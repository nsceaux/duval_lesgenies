OUTPUT_DIR=out
DELIVERY_DIR=/Users/nicolas/Dropbox/CMBV/Duval_LesGenies
#DELIVERY_DIR=delivery
NENUVAR_LIB_PATH:=$(shell pwd)/../../nenuvar-lib
LILYPOND_OPTIONS=--loglevel=WARN -ddelete-intermediate-files -dno-protected-scheme-parsing
LILYPOND_CMD=lilypond -I$(shell pwd) -I$(NENUVAR_LIB_PATH) $(LILYPOND_OPTIONS)

########################################################################
### Matériel version complète
########################################################################
PROJECT=Duval - LesGenies
CONDUCTEUR=$(PROJECT) (PO)
CONDUCTEUR_HC_SOL=$(PROJECT) (PO-hc-sol2)
PARTS = dessus1 dessus2 dessus2-haute-contre taille flute-hautbois parties basse

define PART_template
 $(1):
	$(LILYPOND_CMD) -o "$(OUTPUT_DIR)/$(PROJECT) ($(1))" -dpart=$(1) part.ly
 .PHONY: $(1)
endef

$(foreach part,$(PARTS),$(eval $(call PART_template,$(part))))

define DELIVERY_PART_template
	@if [ -e '$(OUTPUT_DIR)/$(PROJECT) ($(1)).pdf' ]; then mv -fv '$(OUTPUT_DIR)/$(PROJECT) ($(1)).pdf' $(DELIVERY_DIR)/; fi;

endef

delivery:
	@mkdir -p $(DELIVERY_DIR)/
	@if [ -e '$(OUTPUT_DIR)/$(CONDUCTEUR).pdf' ]; then mv -fv '$(OUTPUT_DIR)/$(CONDUCTEUR).pdf' $(DELIVERY_DIR)/; fi
	@if [ -e '$(OUTPUT_DIR)/$(CONDUCTEUR_HC_SOL).pdf' ]; then mv -fv '$(OUTPUT_DIR)/$(CONDUCTEUR_HC_SOL).pdf' $(DELIVERY_DIR)/; fi
	$(foreach part,$(PARTS),$(call DELIVERY_PART_template,$(part)))
	@if [ -e '$(OUTPUT_DIR)/$(PROJECT) (livret).html' ]; then mv -fv "$(OUTPUT_DIR)/$(PROJECT) (livret).html" $(DELIVERY_DIR)/; fi

conducteur:
	$(LILYPOND_CMD) -o "$(OUTPUT_DIR)/$(CONDUCTEUR)" main.ly

conducteur-hc-treble:
	$(LILYPOND_CMD) -o "$(OUTPUT_DIR)/$(CONDUCTEUR_HC_SOL)" -dhaute-contre-treble main.ly

parts: $(PARTS)

clean:
	@rm -f $(OUTPUT_DIR)/$(PROJECT)*

all: check parts conducteur conducteur-hc-treble delivery clean

check:
	@if [ ! -d $(NENUVAR_LIB_PATH) ]; then \
	  echo "Please install nenuvar-lib in parent directory:"; \
	  echo " cd .. && git clone https://github.com/nsceaux/nenuvar-lib.git"; \
	  false; \
	fi
.PHONY: parts conducteur clean all check delivery

########################################################################
### Matériel concert
########################################################################
PROJECT_CONCERT = Duval - LesGenies - concert
PARTS_CONCERT = violon1 violon2 haute-contre taille flute-hautbois basse
CONDUCTEUR_CONCERT=$(PROJECT_CONCERT) (PO)
define PART_CONCERT_template
 $(1)-concert:
	$(LILYPOND_CMD) -o "$(OUTPUT_DIR)/$(PROJECT_CONCERT) ($(1))" -dconcert -dpart=$(1) part-concert.ly
 .PHONY: $(1)-concert
endef

$(foreach part,$(PARTS_CONCERT),$(eval $(call PART_CONCERT_template,$(part))))

define DELIVERY_PART_CONCERT_template
	@if [ -e '$(OUTPUT_DIR)/$(PROJECT_CONCERT) ($(1)).pdf' ]; then mv -fv '$(OUTPUT_DIR)/$(PROJECT_CONCERT) ($(1)).pdf' $(DELIVERY_DIR)/; fi;

endef

parts-concert: $(patsubst %, %-concert, $(PARTS_CONCERT))

conducteur-concert:
	$(LILYPOND_CMD) -dconcert -o "$(OUTPUT_DIR)/$(CONDUCTEUR_CONCERT)" main-concert.ly

delivery-concert:
	@mkdir -p $(DELIVERY_DIR)/
	@if [ -e '$(OUTPUT_DIR)/$(CONDUCTEUR_CONCERT).pdf' ]; then mv -fv '$(OUTPUT_DIR)/$(CONDUCTEUR_CONCERT).pdf' $(DELIVERY_DIR)/; fi
	$(foreach part,$(PARTS_CONCERT),$(call DELIVERY_PART_CONCERT_template,$(part)))
	@if [ -e '$(OUTPUT_DIR)/$(PROJECT_CONCERT) (livret).html' ]; then mv -fv "$(OUTPUT_DIR)/$(PROJECT_CONCERT) (livret).html" $(DELIVERY_DIR)/; fi
	if [ -e '$(OUTPUT_DIR)/$(CONDUCTEUR_CONCERT).midi' ]; then tar zcf '$(DELIVERY_DIR)/$(PROJECT_CONCERT) (midi).tar.gz' $(OUTPUT_DIR)/$(PROJECT)*.midi; fi

all-concert: check parts-concert conducteur-concert delivery-concert clean

.PHONY: parts-concert conducteur-concert all-concert delivery-concert
