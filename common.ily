\header {
  copyrightYear = "2021"
  composer = "Musique de Mademoiselle Duval (1718 - après 1775)"
  poet = "Livret de Jacques Fleury (1730-1775)"
  subtitle = "Ballet"
  date = "(1736)"
  editions = "Composition des parties manquantes Benoît Dratwicki."
}

%% LilyPond options:
%%  urtext  if true, then print urtext score
%%  part    if a symbol, then print the separate part score
#(ly:set-option 'original-layout (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'use-ancient-clef (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'show-ancient-clef (not (symbol? (ly:get-option 'part))))
#(ly:set-option 'use-rehearsal-numbers #t)
#(ly:set-option 'print-footnotes
                (not (symbol? (ly:get-option 'part))))
%% Staff size
#(set-global-staff-size
  (cond ((eqv? 'basse (ly:get-option 'part)) 18)
        ((symbol? (ly:get-option 'part)) 20)
        (else 16)))

%% Line/page breaking algorithm
%%  optimal   for lead sheets
%%  page-turn for instruments and vocal parts
\paper {
  #(define page-breaking (if (eqv? (ly:get-option 'part) #f)
                             ly:optimal-breaking
                             ly:page-turn-breaking))
}

\language "italiano"
\include "nenuvar-lib.ily"
\include "nenuvar-garamond.ily"
\setPath "ly"

#(if (eqv? (ly:get-option 'haute-contre-treble) #t)
     (set! (clef-map 'haute-contre) '(petrucci-c1 . treble)))

\header {
  maintainer = \markup {
    Éditions Nicolas Sceaux – Benoît Dratwicki – Camille Delaforge
    – Il Caravaggio
  }
  license = "Creative Commons Attribution-ShareAlike 4.0 License"
  shortcopyright = \markup { \copyright \license }
  tagline = \markup\sans\abs-fontsize #8 \override #'(baseline-skip . 0) {
    \right-column\bold {
      \with-url #"http://nicolas.sceaux.free.fr" {
        \concat { Éditions \tagline-vspacer }
        \concat { Nicolas \tagline-vspacer }
        \concat { Sceaux \tagline-vspacer }
      }
    }
    \abs-fontsize #9 \with-color #(x11-color 'grey40) \raise #-0.7 \musicglyph #"clefs.petrucci.f"
    \override #'(baseline-skip . 0) \column {
      \vspace#0.4
      %\line { \tagline-vspacer Composition des parties manquantes Benoît Dratwicki. }
      \line { \tagline-vspacer \copyright }
      \line { \tagline-vspacer \license }
    }
  }
}

\opusPartSpecs
#`((dessus "Flûtes, Hautbois, Dessus de violons" ()
           (#:notes "dessus" #:clef "treble"))
   (dessus1 "Premiers dessus de violons" ((dessus #f))
            (#:notes "dessus" #:tag-notes dessus1 #:clef "treble"))
   (dessus2 "Seconds dessus de violons" ((dessus #f))
            (#:notes "dessus" #:tag-notes dessus2 #:clef "treble"))
   (flute-hautbois "Flûtes et Hautbois" ((dessus #f))
                   (#:notes "dessus" #:tag-notes dessus #:clef "treble"))
   (parties "Hautes-contre et tailles de violons" ()
            (#:score-template "score-parties" #:clef "alto"))
   (dessus2-haute-contre
    "Second dessus et hautes-contre de violons"
    ((dessus2 #f))
    (#:notes "haute-contre" #:tag-notes haute-contre #:clef "treble"))
   (taille "Tailles de violons" ((parties #f))
           (#:notes "taille" #:clef "alto"))
   (basse "Basses, Bassons et B.C." ()
          (#:notes "basse" #:clef "basse" #:score-template "score-basse-continue")))

\opusTitle "Les Génies ou les caractères de l'Amour"

\layout {
  largeindent = 25\mm
}

\paper {
  footnote-numbering-function = 
  #(lambda (num)
     #{ \markup\sans\tiny\pad-markup#0.3 $(number->string (+ 1 num)) #})
  footnote-padding = 0\mm
}


#(define-markup-command (tacet layout props) ()
   (interpret-markup layout props #{ \markup {
  \hspace#2 \pad-to-box #'(0 . 0) #'(-2 . 1) \small TACET } #}))

#(define-markup-command (tacet-text layout props text) (markup?)
   (interpret-markup layout props #{ \markup {
  \hspace#2 \pad-to-box #'(0 . 0) #'(-2 . 1) \small TACET \hspace#2 $text } #}))

#(define-markup-command (livretPiece layout props text) (markup?)
   (interpret-markup
    layout props
    #{ \markup\fill-line { $text } #}))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Version :
%%%  - urtext => sans parties ajoutées
%%%  - concert => avec parties ajoutées mais coupures
%%%  - sinon => complète (avec parties ajoutées et sans coupures)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ifFull =
#(define-music-function (parser location music) (ly:music?)
   (if (ly:get-option 'urtext)
       (make-music 'Music 'void #t)
       music))

unlessFull =
#(define-music-function (parser location music) (ly:music?)
   (if (ly:get-option 'urtext)
       music
       (make-music 'Music 'void #t)))

ifComplet =
#(define-music-function (parser location music) (ly:music?)
   (if (ly:get-option 'concert)
       (make-music 'Music 'void #t)
       music))

ifConcert =
#(define-music-function (parser location music) (ly:music?)
   (if (ly:get-option 'concert)
       music
       (make-music 'Music 'void #t)))

%% Version concert : haute-contre du chœur chantée par une mezzo
#(if (eqv? (ly:get-option 'concert) #t)
     (begin
       (set! (clef-map 'haute-contre)  '(petrucci-c1 . treble))
       (set! (clef-map 'taille)        '(petrucci-c2 . treble))))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#(define-markup-command (footnote-style layout props note) (markup-list?)
   (interpret-markup
    layout props
    #{\markup\force-line-width-ratio#1.25  \fontsize#-2 \justify { $note }#}))

#(define-markup-command (note-piece layout props titre) (markup-list?)
   (interpret-markup
    layout props
    #{\markup\column {
  \vspace#1
  \bold\wordwrap $titre

}#}))

#(define-markup-list-command (note-notes layout props notes) (markup-list?)
   (interpret-markup-list
    layout props
    #{ \markuplist\with-line-width-ratio#0.8 \wordwrap-lines { $notes } #}))

#(define-markup-command (stars layout props) ()
   (interpret-markup
    layout props
    #{
\markup\fill-line {
  \center-column {
    \vspace#1
    \wordwrap-center\bold {
      \center-column\typewriter { ✻ \line { ✻ \hspace#6 ✻ } }
    }
    \vspace#1
  }
}#}))

\paper {
  bookTitleMarkup = \markup\when-property #'header:title
  \abs-fontsize#12 \override#'(baseline-skip . 0) \column {
    \vspace#(if (symbol? (*part*)) 10 13)
    \italic\bold\fontsize#7 \fill-line {
      \override#'(baseline-skip . 6) \fromproperty#'header:title
    }
    \vspace#2
    \italic\fontsize#5 \fill-line { \fromproperty#'header:subtitle }
    \vspace#0.8
    \fontsize#1 \fill-line { \fromproperty#'header:creation }
    \vspace#1
    \fontsize#1 \fill-line { \fromproperty#'header:date }
    \vspace#6
    \fontsize#1 \fill-line { \fromproperty#'header:composer }
    \vspace#0.2
    \fontsize#1 \fill-line { \fromproperty#'header:poet }
    \vspace#(if (symbol? (*part*)) 14 20)
    \fontsize#-1 \fill-line { \fromproperty#'header:editions }
    \vspace#2
    \fontsize#2 \fill-line {
      \on-the-fly #(lambda (layout props arg)
                     (if (*part*)
                         (interpret-markup layout props (*part-name*))
                         empty-stencil)) \null
    }
  }
}
