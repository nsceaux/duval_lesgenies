\notesSection "Livret"
\markuplist\abs-fontsize-lines #8 \page-columns-title \act\line { LIVRET } {
\livretAct PROLOGUE
\livretDescAtt\justify {
  Le Théatre représente un désert.
}
\livretScene SCÈNE PREMIÈRE
\livretPers Zoroastre
\livretRef#'AABrecit
\livretVerse#12 { Il est temps que mon Art instruise les Mortels, }
\livretVerse#12 { Dans les secrets des Dieux le premier j’ai su lire : }
\livretVerse#8 { Méritons comme eux des autels, }
\livretVerse#12 { Et montrons mon pouvoir à tout ce qui respire. }
\livretRef#'AACair
\livretVerse#10 { Esprits soumis à mes commandements, }
\livretVerse#8 { Venez remplir mon espérance, }
\livretVerse#10 { Rassemblez-vous des divers Éléments, }
\livretVerse#10 { Et signalez ma gloire et ma puissance. }

\livretScene SCÈNE II
\livretDescAtt\wordwrap-center {
  Zoroastre, et les génies.
}
\livretPers Zoroastre
\livretRef#'ABAair
\livretVerse#12 { Que la Terre, le Feu, que l’Onde, que les Airs }
\livretVerse#12 { Découvrent les trésors que mon Art fait éclore ; }
\livretVerse#12 { Volez, dispersez-vous du Couchant à l’Aurore, }
\livretVerse#10 { De vos bienfaits remplissez l’Univers. }
\livretPers Chœur
\livretVerse#12 { Que la Terre, &c. }
\livretRef#'ABCdanse
\livretDidasPage DANSE POUR LES GENIES.
\livretDidasP\justify {
  On entend un douce harmonie qui annonce la descente de l’Amour.
}
\livretPers Zoroastre
\livretRef#'ABDair
\livretVerse#12 { Quels bruits ! quels doux accords ! quelle clarté nouvelle ! }
\livretVerse#12 { L’horreur des ces déserts disparaît à mes yeux ! }
\livretVerse#10 { Quel Dieu descend de la Cour immortelle, }
\livretVerse#8 { Pour venir embellir ces lieux ? }
\livretVerse#12 { Ah ! je le reconnais à sa douceur extrême, }
\livretVerse#12 { C’est l’Amour ! et quel Dieu se fait sentir de même ! }

\livretScene SCÈNE III
\livretDescAtt\wordwrap-center {
  L’Amour, Zoroastre, et les génies.
}
\livretPers L'Amour
\livretRef#'ACAair
\livretVerse#10 { Tout obéit, tout s’éveille à ta voix ! }
\livretVerse#12 { Tu déchaines les Vents, tu fais trembler la Terre ! }
\livretVerse#12 { Tu soulèves les Flots, tu lances le Tonnerre ; }
\livretVerse#10 { Mais l’Amour seul ne connaît point tes lois. }
\livretPers Zoroastre
\livretVerse#10 { Tout reconnaît votre pouvoir suprême, }
\livretVerse#8 { Régnez, triomphez Dieu charmant ; }
\livretVerse#8 { Il n’est point de plus doux moment, }
\livretVerse#8 { Que l’instant où l’on dit qu’on aime. }
\livretPers L'Amour
\livretVerse#8 { Qui vous amène en ces déserts ? }
\livretVerse#12 { À de nouveaux Sujets je viens donner des fers. }
\livretVerse#12 { Peuples des Éléments, connaissez ma puissance ; }
\livretVerse#8 { Je règne sur tout l’Univers, }
\livretVerse#12 { Éprouvez en ce jour les traits que l’Amour lance. }
\livretVerse#10 { Les maux qu’ils font, doivent être plus chers }
\livretVerse#8 { Que les biens de l’indifférence. }
\livretRef#'ACBair
\livretVerse#12 { Accourez Jeux charmants, volez tendres Amours, }
\livretVerse#8 { Formez les plus galantes Fêtes ; }
\livretVerse#12 { Quand on aime, tout âge est l’âge des beaux jours. }
\livretVerse#12 { Plaisirs, lancez mes traits, étendez mes conquêtes. }

\livretScene SCÈNE IV
\livretDescAtt\wordwrap-center {
  L’Amour, Zoroastre, et les génies, Troupe de Plaisirs & de Jeux.
}
\livretPers L'Amour
\livretRef#'ADAair
\livretVerse#8 { Aimez-tous, cédez à l’Amour, }
\livretVerse#8 { Éprouvez le poids de ses chaînes ; }
\livretVerse#8 { Il vous offre dans ce beau jour }
\livretVerse#8 { Des plaisirs plutôt que des peines. }
\livretVerse#8 { Profitez de l’heureux moment, }
\livretVerse#8 { Il n’est pas toujours favorable ; }
\livretVerse#8 { Le caprice amène l’instant, }
\livretVerse#6 { L’amour le rend aimable. }
\livretPers Chœur
\livretRef#'ADEchoeur
\livretVerse#13 { Du doux bruit de nos chants que ces lieux retentissent, }
\livretVerse#13 { Les Amours & les Jeux, pour nos plaisirs s’unissent ; }
\livretVerse#8 { Aimons, goûtons mille douceurs, }
\livretVerse#8 { L’Amour les promet à nos cœurs. }
\livretFinAct FIN DU PROLOGUE
\sep
\livretAct  PREMIÈRE ENTRÉE
\livretScene\wordwrap { Les Nymphes, ou l’amour indiscret. }
\livretDescAtt\justify {
  Le Théatre représente un agréable jardin sur le bord de la mer.
}
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\wordwrap-center { Léandre, Zerbin. }
\livretPers Léandre
\livretRef#'BABrecit
\livretVerse#12 { Viens être le témoin du bonheur qui m’enchante, }
\livretVerse#12 { C’est dans ces lieux qu’Amour répond à mes désirs ; }
\livretVerse#12 { Sans exiger de moi ni larmes ni soupirs, }
\livretVerse#8 { Il rend ma flamme triomphante. }
\livretPers Zerbin
\livretVerse#8 { Ah ! si ce dieu comble vos vœux }
\livretVerse#8 { Ne le faites jamais paraître ; }
\livretVerse#8 { Un cœur dans l’empire amoureux, }
\livretVerse#8 { Devrait, pour être plus heureux, }
\livretVerse#6 { Douter toujours de l’être. }
\livretPers Léandre
\livretVerse#12 { Les plaisirs dont l’amour sait enchanter les sens, }
\livretVerse#12 { Satisfont les désirs d’un amant qui soupire ; }
\livretVerse#12 { Pour moi, libre du soin de ces tendres amants, }
\livretVerse#7 { Non, non je ne les ressens, }
\livretVerse#8 { Qu’autant que je puis les redire. }
\livretPers Zerbin
\livretVerse#8 { Qui ne sait garder le secret, }
\livretVerse#8 { Goûte peu de douceurs parfaites, }
\livretVerse#8 { Elles n’ont jamais été faites }
\livretVerse#7 { Pour un amant indiscret. }
\livretVerse#12 { Quel objet vous retient dans cet heureux asile ? }
\livretVerse#8 { Venez-vous attendre Lucile ? }
\livretPers Léandre
\livretVerse#12 { Un objet plus charmant m’arrête dans ces lieux, }
\livretVerse#12 { Zerbin, il va bientôt sortir du sein de l’onde }
\livretVerse#12 { Pour me rendre l’amant le plus heureux du monde ; }
\livretVerse#12 { Demeure, son abord va surprendre tes yeux. }
\livretRef#'BACair
\livretVerse#8 { Jamais la reine de Cythère }
\livretVerse#7 { N’a brillé de tant d’appas, }
\livretVerse#8 { L’Amour ne connaît plus sa mère, }
\livretVerse#6 { Depuis qu’il suit les pas }
\livretVerse#8 { De l’aimable objet qui m’enchaîne : }
\livretVerse#8 { Son char conduit par les Zéphirs, }
\livretVerse#8 { Vole sur la liquide plaine ; }
\livretVerse#12 { Les vents à son aspect, retiennent leur haleine, }
\livretVerse#8 { Les ris, les jeux & les plaisirs }
\livretVerse#8 { Folâtrent sans cesse autour d’elle ; }
\livretVerse#8 { On ne saurait voir cette belle, }
\livretVerse#8 { Sans former de tendre désirs. }
\livretRef#'BADrecit
\livretVerse#10 { Lucile vient, j’évite sa présence }
\livretVerse#12 { Elle me croit constant, que je plains son erreur ! }
\livretPers Zerbin
\livretVerse#12 { Dois-je de son amour affermir la constance ? }
\livretPers Léandre
\livretVerse#12 { Ce n’est plus un secret que ma nouvelle ardeur. }

\livretScene SCÈNE II
\livretDescAtt\wordwrap-center { Lucile, Zerbin. }
\livretPers Lucile
\livretRef#'BBAair
\livretVerse#12 { Asile des plaisirs, beau lieu rempli de charmes, }
\livretVerse#12 { Offrez à mes regards l’objet de mon amour. }
\livretVerse#12 { Mon cœur en son absence éprouve des alarmes }
\livretVerse#12 { Que rien ne peut calmer, que son heureux retour. }
\livretVerse#12 { Asile des plaisirs, beau lieu rempli de charmes, }
\livretVerse#12 { Offrez à mes regards l’objet de mon amour. }
\livretPersDidas Zerbin à part
\livretRef#'BBBrecit
\livretVerse#10 { Mérites-tu volage, un cœur si tendre ? }
\livretVerse#12 { Pour qui réserves-tu tes plus funestes coups, }
\livretVerse#12 { Cruel Amour ? }
\livretPers Lucile
\livretVerse#12 { \transparent { Cruel Amour ? } Zerbin. }
\livretPers Zerbin
\livretVerse#12 { \transparent { Cruel Amour ? Zerbin. } Je parle de Léandre. }
\livretVerse#12 { C’est un amant… }
\livretPers Lucile
\livretVerse#12 { \transparent { C’est un amant… } Eh quoi ? }
\livretPers Zerbin
\livretVerse#12 { \transparent { C’est un amant… Eh quoi ? } Trop indigne de vous. }
\livretPers Lucile
\livretVerse#12 { Quoi ? Léandre, Zerbin ! }
\livretPers Zerbin
\livretVerse#12 { \transparent { Quoi ? Léandre, Zerbin ! } Léandre vous adore ; }
\livretVerse#12 { Mais à d’autres qu’à vous, Léandre en dit autant. }
\livretPers Lucile
\livretVerse#12 { Après tous ses serments, l’ingrat me trompe encore. }
\livretPers Zerbin
\livretVerse#8 { Affectez quelque changement }
\livretVerse#8 { Pour vous vanger de cet outrage ; }
\livretVerse#8 { C’est s’assurer de son amant, }
\livretVerse#8 { Que de feindre d’être volage. }
\livretPers Lucile
\livretVerse#8 { Amante infortunée, hélas ! }
\livretVerse#12 { Mes soupirs, mes regards trahiraient ce mystère ; }
\livretVerse#12 { Ma bouche lui dirait que je ne l’aime pas, }
\livretVerse#10 { Et dans mes yeux il lirait le contraire. }
\livretRef#'BBCair
\livretVerse#12 { Venez, juste Dépit, venez à mon secours, }
\livretVerse#12 { Bannissez de mon cœur un amant infidèle ; }
\livretVerse#8 { Que de plus constantes amours }
\livretVerse#12 { Allument dans mon âme une flamme nouvelle. }
\livretVerse#12 { Venez, juste Dépit, venez à mon secours, }
\livretVerse#12 { Bannissez de mon cœur un amant infidèle. }
\livretPers Zerbin
\livretRef#'BBDrecit
\livretVerse#8 { Mais, c’est lui qui vient dans ces lieux : }
\livretVerse#12 { Pour connaître son cœur, cachez-vous à ses yeux. }
\livretPers Lucile
\livretVerse#12 { L’ingrat ! je l’aime encor, malgré son inconstance. }
\livretPers Zerbin
\livretVerse#8 { Venez, évitez sa présence. }

\livretScene SCÈNE III
\livretPers Léandre
\livretRef#'BCAair
\livretVerse#8 { Reviens cher objet de mes vœux ; }
\livretVerse#12 { Déjà l’astre du jour éteint ses feux dans l’onde, }
\livretVerse#12 { Il est temps à mes vœux que ton amour réponde, }
\livretVerse#8 { Viens rendre ton amant heureux. }
\livretRef#'BDAentree
\livretDidasPPage\justify {
  On entend une douce harmonie ; la nymphe paraît sur une conque marine,
  suivie de sa cour.
}

\livretScene SCÈNE IV
\livretDescAtt\wordwrap-center {
  Léandre, la principale Nymphe, & sa suite ; Troupe d’Ondins et de Nymphes.
}
\livretPers Léandre
\livretRef#'BDBduo
\livretVerse#8 { Qu’éloigné de votre présence, }
\livretVerse#8 { J’ai souffert de maux rigoureux ! }
\livretVerse#12 { Mais que ces maux sont doux lorsqu’après votre absence, }
\livretVerse#8 { Je revois encor vos beaux yeux ! }
\livretPers La principale Nymphe
\livretVerse#12 { Ah ! quel aveu charmant, qu’il m’est doux de l’entendre ! }
\livretVerse#8 { Amour, mes vœux sont satisfaits, }
\livretVerse#12 { La gloire de régner sur un cœur aussi tendre }
\livretVerse#8 { Est le plus cher de tes bienfaits. }
\livretPers Ensemble
\livretVerse#12 { Amour, viens nous unir de tes plus douces chaînes, }
\livretVerse#8 { Vole, réponds à nos désirs ; }
\livretVerse#12 { Nos cœurs ne sont point faits pour éprouver tes peines, }
\livretVerse#8 { Ne nous offre que tes plaisirs. }
\livretPers La principale Nymphe
\livretVerse#12 { Nymphes, vous qui formez ma cour la plus brillante, }
\livretVerse#12 { Vous habitants des mers qui vivez sous mes lois, }
\livretVerse#8 { Rassemblez-vous troupe charmante, }
\livretVerse#8 { Venez, accourez à ma voix. }
\livretPers La principale Nymphe
\livretRef#'BDCair
\livretVerse#8 { Chantez dans ce riant boccage, }
\livretVerse#12 { Célébrez de l’Amour les triomphes divers, }
\livretVerse#8 { Il retient sous son esclavage }
\livretVerse#8 { Les cieux, la terre & les enfers ; }
\livretVerse#8 { Qu’il règne autant sur ce rivage, }
\livretVerse#8 { Qu’il règne dans le sein des mers. }
\livretPers Chœur
\livretRef#'BDDchoeur
\livretVerse#8 { Chantons, &c. }
\livretRef#'BDEpassacaille
\livretDidasPPage On danse.
\livretPers La principale Nymphe
\livretRef#'BDHair
\livretVerse#8 { Amour, tu réponds à nos vœux, }
\livretVerse#8 { Triomphe à jamais de nos âmes, }
\livretVerse#8 { Ce n’est qu’en éprouvant tes flammes, }
\livretVerse#8 { Que les cœurs peuvent être heureux. }
\livretVerse#8 { Tous les oiseaux de ces boccages }
\livretVerse#8 { Sous tes lois goûtent des douceurs, }
\livretVerse#8 { Ils ne raniment leurs ramages }
\livretVerse#8 { Que pour célébrer tes faveurs. }
\livretVerse#8 { Triomphe à jamais de nos âmes, }
\livretVerse#8 { Amour, &c. }
\livretDidasPPage On danse.
\livretPers Une Nymphe
\livretRef#'BDKchoeur
\livretVerse#8 { Rions, chantons sous cet ombrage, }
\livretVerse#8 { Tout y répond à nos désirs ; }
\livretVerse#8 { L’Amour y cache les plaisirs }
\livretVerse#8 { Dont notre printemps fait usage. }
\livretPers Chœur
\livretVerse#8 { Rions, chantons, &c. }
\livretPers La Nymphe
\livretVerse#8 { Sans soins, sans crainte des jalous, }
\livretVerse#8 { Nous nous livrons à la tendresse ; }
\livretVerse#8 { Et le tendre amour ne nous blesse, }
\livretVerse#8 { Que pour nous faire un sort plus doux. }
\livretPers Chœur
\livretVerse#8 { Rions, chantons sous cet ombrage, }
\livretVerse#8 { Tout y répond à nos désirs ; }
\livretVerse#8 { L’Amour y cache les plaisirs }
\livretVerse#8 { Dont notre printemps fait usage. }
\livretPersDidas La principale Nymphe à Léandre
\livretRef#'BDNrecit
\livretVerse#8 { Tout prévient ici vos désirs, }
\livretVerse#12 { La sévère sagesse, & la raison cruelle }
\livretVerse#8 { Ne sauraient troubler nos plaisirs ; }
\livretVerse#8 { Mais soyez-moi toujours fidèle. }
\livretPers Ensemble
\livretVerse#12 { Aimons-nous, aimons-nous d’une ardeur éternelle. }

\livretScene SCÈNE V
\livretDescAtt\wordwrap-center {
  La principale Nymphe, & sa suite.
  Léandre, Lucile, Zerbin.
}
\livretPers Lucile
\livretRef#'BEArecit
\livretVerse#12 { Poursuis, ingrat, poursuis volage, amant sans foi, }
\livretVerse#12 { Fais éclater tes feux auprès de cette belle : }
\livretVerse#12 { Va, tu peux lui jurer une ardeur éternelle }
\livretVerse#8 { Que ton cœur n’a promis qu’à moi. }
\livretVerse#12 { Perfide, garde-toi de paraître à ma vue ; }
\livretVerse#12 { C’en est fait, pour jamais mes liens sont rompus. }
\livretPers Léandre
\livretVerse#8 { Hélas ! je vous ai donc perdue, }
\livretVerse#12 { Lucile, vous fuyez ! }
\livretPers Zerbin
\livretVerse#12 { \transparent { Lucile, vous fuyez ! } Vous ne la verrez plus. }
\livretPers La principale Nymphe
\livretVerse#12 { Ah ! puis-je soutenir un si sanglant outrage, }
\livretVerse#10 { Sans immoler un traitre à ma fureur ? }
\livretVerse#12 { Je sens que mon âme s’abandonne à la rage, }
\livretVerse#12 { Perfide, sauve-toi de mon courroux vangeur. }
\livretPersDidas Léandre à Zerbin
\livretVerse#12 { Allons chercher Lucile, & pour fléchir son cœur, }
\livretVerse#12 { Jurons à ses beaux yeux la plus fidèle ardeur. }
\livretPers La principale Nymphe
\livretVerse#8 { Que tout serve ici ma colère }
\livretVerse#12 { Pour punir un ingrat qui m’avait trop su plaire. }
\livretRef#'BEBair
\livretVerse#12 { Venez tyrans des airs, aquilons furieux, }
\livretVerse#12 { Excitez sur ce bord le plus affreux orage ; }
\livretVerse#12 { Que les flots irrités s’élèvent jusqu’aux cieux, }
\livretVerse#8 { Vangez-moi, lavez mon outrage, }
\livretVerse#8 { Innondez pour jamais ces lieux. }
\livretRef#'BECsymphonie
\livretDidasPPage\line { On voit les flots se soulever. }
\livretFinAct FIN DE LA PREMIÈRE ENTRÉE
\sep
\livretAct DEUXIÈME ENTRÉE
\livretScene\wordwrap { Les gnomes, ou l’amour ambitieux. }
\livretDescAtt\justify {
  Le Théatre représente une solitude, bornée par un bosquet.
}
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\wordwrap-center { Zaïre, Zamide. }
\livretPers Zaïre
\livretRef#'CAAair
\livretVerse#8 { Douce erreur, charmante chimère, }
\livretVerse#10 { Pourquoi faut-il que la clarté du jour, }
\livretVerse#10 { Chasse l’espoir dont me flattait l’amour ? }
\livretVerse#12 { Et que tu ne sois plus qu’un bien imaginaire ? }
\livretPers Zamide
\livretVerse#12 { Zaïre, arrêtez-vous ? qui vous guide en ces lieux ? }
\livretVerse#12 { Vos sens sont agités, mille douces alarmes }
\livretVerse#12 { D’un éclat plus brillant embellissent vos yeux ; }
\livretVerse#12 { L’amour veut-il enfin récompenser vos charmes ? }
\livretPers Zaïre
\livretVerse#12 { Quel spectacle à mes yeux s’est offert cette nuit ? }
\livretVerse#12 { Jamais rien de si beau n’avait frappé mon âme ! }
\livretVerse#12 { Malgré l’éclat du jour cette image me suit. }
\livretVerse#12 { Adolphe !… j’ai cru voir cet objet de ma flamme }
\livretVerse#12 { Sur un trône, entouré d’une pompeuse cour : }
\livretVerse#12 { Tout tremblait devant lui dans un humble esclavage, }
\livretVerse#12 { Je me trouvais moi-même en ce charmant séjour, }
\livretVerse#12 { Et lorsque tous les cœurs venaient lui rendre hommage, }
\livretVerse#8 { Je jouissais de l’avantage }
\livretVerse#12 { De le voir à mes pieds, les offrir à l’amour. }
\livretPers Zamide
\livretVerse#8 { Le sommeil par de doux mensonges }
\livretVerse#8 { Quelque fois donne de beaux jours ; }
\livretVerse#8 { Mais le réveil les rend si courts, }
\livretVerse#8 { Qu’ils s’envolent avec les songes. }
\livretPers Zaïre
\livretVerse#12 { Laissez-moi m’occuper des plaisirs que je sens, }
\livretVerse#12 { J’aime à rêver encor dans ce lieu solitaire ; }
\livretVerse#8 { L’amour sait ce qui reste à faire, }
\livretVerse#8 { Pour mieux mériter mon encens. }

\livretScene SCÈNE II
\livretDescAtt\wordwrap-center { Zaïre seule. }
\livretPers Zaïre.
\livretRef#'CBAair
\livretVerse#8 { Je cède à ta voix qui m’appelle, }
\livretVerse#8 { Amour, achève mon bonheur ; }
\livretVerse#12 { Pour prix de tous les biens dont tu flattes mon cœur, }
\livretVerse#8 { Je t’offre une flamme éternelle. }
\livretVerse#10 { Maître des rois, tu conduis l’univers }
\livretVerse#12 { Tu couronnes des cœurs inconnus sur la terre ; }
\livretVerse#8 { Tu forces le dieu du tonnerre, }
\livretVerse#12 { À sortir de son rang, pour être dans tes fers. }
\livretVerse#8 { Je cède, &c. }

\livretScene SCÈNE III
\livretDescAtt\wordwrap-center { Un gnome, sous le nom d’Adolphe, Zaïre. }
\livretPers Adolphe
\livretRef#'CCArecit
\livretVerse#12 { Vous voyez à vos pieds l’amant le plus fidèle, }
\livretVerse#12 { Et je revois l’objet que j’aime tendrement ; }
\livretVerse#8 { Vous ne fûtes jamais si belle, }
\livretVerse#12 { Et jamais mon amour ne fut si violent. }
\livretPers Zaïre
\livretVerse#12 { Je ne puis vous revoir sans une peine extrême, }
\livretVerse#12 { Dans un songe à mes yeux vous aviez mille attraits. }
\livretVerse#8 { Ah ! que ne vous vois-je de même, }
\livretVerse#8 { Tous mes vœux seraient satisfaits ! }
\livretPers Adolphe
\livretVerse#12 { Juste ciel ! est-ce à moi que ce discours s’adresse ? }
\livretPers Zaïre
\livretVerse#12 { Non, non c’est à l’amour qui trahit sa promesse. }
\livretPers Adolphe
\livretVerse#12 { Que vous a-t-il promis qu’il ne puisse tenir ? }
\livretVerse#12 { Parlez, il peut encor contenter votre envie. }
\livretPers Zaïre
\livretVerse#10 { Bannissez-moi de votre souvenir, }
\livretVerse#12 { Et s’il se peut aussi, que mon cœur vous oublie. }
\livretPers Adolphe
\livretVerse#12 { Qui ? moi vous oublier ? Vous voulez donc ma mort, }
\livretVerse#8 { Cruelle, achevez votre ouvrage, }
\livretVerse#12 { Votre bouche et vos yeux ont le même langage, }
\livretVerse#8 { C’est assez pour finir mon sort. }
\livretPers Zaïre
\livretVerse#12 { Je vous aime, il est temps de vous ouvrir mon âme, }
\livretVerse#12 { Que puis-je vous offrir pour répondre à vos vœux, }
\livretVerse#12 { Je n’ai que des soupirs pour prix de votre flamme }
\livretVerse#12 { Et pour mon tendre amour vous n’avez que des feux ; }
\livretVerse#11 { Si le ciel m’eut placé en un rang glorieux, }
\livretVerse#12 { J’aurais fait mon bonheur d’unir mon sort au vôtre, }
\livretVerse#12 { Quelle rigueur, hélas ! plagnez vous-en aux dieux, }
\livretVerse#8 { Nos cœurs étaient faits l’un pour l’autre }
\livretVerse#12 { Et malgré notre amour il faut briser nos nœuds. }
\livretPers Adolphe
\livretVerse#8 { Je n’entends que trop ce langage, }
\livretVerse#12 { Quelque rival caché s’oppose à mon bonheur ; }
\livretVerse#12 { Mais il n’est point encor maître de votre cœur ; }
\livretVerse#12 { Il faut manquer d’amour, ou manquer de courage, }
\livretVerse#8 { Pour souffrir un autre vainqueur. }
\livretPers Zaïre
\livretVerse#8 { Vous m’accusez d’être volage, }
\livretVerse#12 { Et votre cœur se livre à des soupçons jalous ; }
\livretVerse#8 { Ingrat, quand je n’aime que vous, }
\livretVerse#8 { Ai-je mérité cet outrage ? }
\livretPers Adolphe
\livretVerse#12 { Le pouvoir de vos yeux s’étend sur tous les cœurs, }
\livretVerse#12 { Il n’est rien dans les cieux, sur la terre & sur l’onde, }
\livretVerse#8 { Qui ne cède à leurs traits vainqueurs : }
\livretVerse#8 { Jusque dans le centre du monde, }
\livretVerse#12 { Ils savent allumer les plus vives ardeurs. }
\livretPers Zaïre
\livretVerse#12 { À mes faibles appas vous donnez trop d’empire, }
\livretVerse#8 { Ils ne règnent que sur un cœur ; }
\livretVerse#8 { La gloire & le bien où j’aspire }
\livretVerse#8 { Serait de faire son bonheur. }
\livretVerse#12 { Je vois que chaque instant redouble vos alarmes. }
\livretPers Adolphe
\livretVerse#12 { C’est douter trop longtemps du pouvoir de vos charmes, }
\livretVerse#12 { Connaissez où s’étend l’empire de vos yeux. }
\livretPers Zaïre
\livretVerse#8 { Que vois-je ! où suis-je ! ô justes dieux ! }
\livretRef#'CDAmarche
\livretDidasPPage\justify {
  L’on voit paraître un superbe palais. Une troupe de gnomes sous la
  forme de divers peuples orientaux se préparent pour la fête.
}

\livretScene SCÈNE IV
\livretDescAtt\wordwrap-center {
  Adolphe, Zaïre, troupe de gnomes sous la forme
  de divers peuples orientaux.
}
\livretPers Zaïre
\livretRef#'CDBrecit
\livretVerse#12 { Que tout ce que je vois rend mon âme interdite ! }
\livretVerse#12 { Je ne saurais calmer le trouble qui m’agite. }
\livretPers Adolphe
\livretVerse#10 { Rassurez-vous, dissipez votre effroi, }
\livretVerse#12 { Régnez avec Adolphe, en régnant avec moi : }
\livretVerse#12 { Pouvais-je résister de vous rendre les armes, }
\livretVerse#12 { Pour la première fois que j’aperçus vos charmes ? }
\livretVerse#12 { Ce fut dans ce jardin où la mère d’Amour }
\livretVerse#8 { Semble avoir fixé son empire : }
\livretVerse#10 { Vous paraissez, Vénus quitte sa cour, }
\livretVerse#12 { Tout se range vers vous, près de vous tout soupire, }
\livretVerse#12 { Les oiseaux enchantés vous parlaient de leurs feux ; }
\livretVerse#8 { Les ruisseaux par leur doux murmure, }
\livretVerse#8 { Rendaient hommage à vos beaux yeux ; }
\livretVerse#8 { Et le père de la nature }
\livretVerse#12 { Pour vous, du plus beau jour faisait briller ces lieux. }
\livretVerse#10 { Par tant d’attraits, fallait-il me surprendre ? }
\livretVerse#8 { Quel cœur aurait pu s’en défendre ! }
\livretPers Zaïre
\livretVerse#12 { Votre amour me soumet tous ces peuples divers, }
\livretVerse#12 { Et sur vous désormais je règne en souveraine ; }
\livretVerse#12 { Mon destin le plus beau c’est de porter ma chaîne, }
\livretVerse#8 { Et de vous voir porter vos fers. }
\livretPers Ensemble
\livretVerse#8 { Tendre Amour, enchaîne nos âmes, }
\livretVerse#8 { C’est toi seul qui fais mon bonheur ; }
\livretVerse#8 { N’allume jamais dans mon cœur }
\livretVerse#8 { D’autres désirs, ni d’autres flammes. }
\livretPers Adolphe
\livretVerse#12 { Dans ces lieux souterrains où je donne la loi, }
\livretVerse#12 { Vous qui reconnaissez ma puissance suprême, }
\livretVerse#12 { Redoublez vos transports pour plaire à votre roi ; }
\livretVerse#12 { Mais faites encor plus pour plaire à ce que j’aime. }
\livretPers Chœur
\livretRef#'CDCchoeur
\livretVerse#12 { Régnez dans nos climats, jouïssez de la gloire }
\livretVerse#8 { De faire triompher l’amour ; }
\livretVerse#12 { Vos yeux à chaque instant augmentent sa victoire, }
\livretVerse#8 { Qu’il vous enchaîne à votre tour. }
\livretPersDidas Un Indien à Zaïre
\livretRef#'CDEair
\livretVerse#8 { Recevez l’éclatant hommage }
\livretVerse#8 { D’un cœur que vous avez dompté, }
\livretVerse#8 { Triomphez, goûtez l’avantage }
\livretVerse#8 { D’avoir désarmé sa fierté. }
\livretVerse#8 { La gloire, la magnificence }
\livretVerse#8 { Ne font plus sa félicité ; }
\livretVerse#8 { Il ne connaît plus de puissance }
\livretVerse#8 { Que celle de votre beauté. }
\livretRef#'CDFloure
\livretDidasPPage On danse.
\livretPers Un Indien
\livretRef#'CDIair
\livretVerse#4 { Dans nos climats }
\livretVerse#4 { Chacun s’engage, }
\livretVerse#5 { Et la plus sauvage }
\livretVerse#5 { Ne résiste pas. }
\livretVerse#4 { Notre richesse }
\livretVerse#5 { Fait notre tendresse ; }
\livretVerse#5 { Nous savons charmer }
\livretVerse#4 { Un cœur rebelle, }
\livretVerse#5 { Et la plus cruelle }
\livretVerse#5 { Se laisse enflammer. }
\livretVerse#5 { Le dieu des amours }
\livretVerse#5 { Se sert de nos armes, }
\livretVerse#5 { Il n’a point de charmes }
\livretVerse#5 { Sans notre secours. }
\livretPers Chœur
\livretRef#'CDKchoeur
\livretVerse#12 { Régnez dans nos climats, jouissez de la gloire }
\livretVerse#8 { De faire triompher l’amour ; }
\livretVerse#12 { Vos yeux à chaque instant augmentent sa victoire, }
\livretVerse#8 { Qu’il vous enchaîne à votre tour. }
\livretFinAct FIN DE LA DEUXIÈME ENTRÉE
\sep
\livretAct TROISIÈME ENTRÉE
\livretScene\wordwrap { Les salamandres, ou l’amour violent. }
\livretDescAtt\justify {
  Le Théatre représente le palais de Numapire.
}
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\wordwrap-center { Isménide. }
\livretPers Isménide
\livretRef#'DAAair
\livretVerse#8 { Tyran d’un cœur fidèle & tendre, }
\livretVerse#8 { Que t’ai-je fait, cruel amour ? }
\livretVerse#12 { Chaque instant, de mes cris retentit ce séjour, }
\livretVerse#8 { Et tu ne veux pas les entendre. }
\livretVerse#12 { Hélas ! loin de l’objet qui cause ma langueur }
\livretVerse#12 { Tu me laisses gémir sous les fers d’un barbare, }
\livretVerse#8 { Et tu permets qu’il me sépare }
\livretVerse#12 { D’un amant qui faisait mon unique bonheur. }
\livretVerse#8 { Tyran d’un cœur &c. }
\livretRef#'DABrecit
\livretDidasPPage\justify {
  Pircaride sous les traits d'Ismé[ni]de, paraît sur un char de feu un
  poignard à la main.
}
\livretVerse#12 { Que vois-je ? quel objet se présente à mes yeux ? }
\livretVerse#8 { Juste ciel ! quel couroux l’anime. }
\livretDidasPPage\justify {
  Pircaride sort du char.
}
\livretScene SCÈNE II
\livretDescAtt\wordwrap-center { Pircaride, Isménide. }
\livretPers Pircaride
\livretRef#'DBBrecit
\livretVerse#8 { Pour immoler une victime }
\livretVerse#10 { Le désespoir me conduit dans ces lieux, }
\livretVerse#8 { Tu me vois sous ta propre image ; }
\livretVerse#8 { Mais c’est pour mieux servir ma rage. }
\livretPers Isménide
\livretVerse#9 { Qu’entends-je ? }
\livretPers Pircaride
\livretVerse#9 { \transparent { Qu’entends-je ? } À mes transports jaloux }
\livretVerse#6 { Reconnais ta rivale. }
\livretVerse#10 { Pour adoucir ma peine sans égale, }
\livretVerse#12 { C’est sur toi que je vais faire tomber mes coups. }
\livretPers Isménide
\livretVerse#8 { Barbare, achève ta vengeance, }
\livretVerse#8 { Hâte-toi de frapper mon cœur ; }
\livretVerse#8 { Ne respecte dans ta fureur, }
\livretVerse#8 { Ni mes pleurs, ni mon innocence. }
\livretVerse#8 { Unique objet de mes désirs }
\livretVerse#12 { Cher Idas, toi pour qui j’aurais aimé la vie, }
\livretVerse#12 { Reçois avec mon sang, lorsqu’elle m’est ravie, }
\livretVerse#12 { Mes adieux, mon amour, & mes derniers soupirs. }
\livretPersDidas Pircaride à part.
\livretVerse#12 { Elle aime un autre amant ! }
\livretDidasPPage à Isménide.
\livretVerse#12 { \transparent { Elle aime un autre amant ! } Parle, explique tes larmes. }
\livretPers Isménide
\livretVerse#8 { Je touchais au sort le plus doux, }
\livretVerse#10 { Un tendre amant devenait mon époux, }
\livretVerse#10 { Lorsqu’un barbare en vint troubler les charmes ; }
\livretVerse#12 { Il m’enlève, malgré l’effort de mon amant : }
\livretVerse#12 { Votre haine à ce prix, est-elle légitime ? }
\livretPers Pircaride
\livretVerse#12 { Non, je ne te hais plus. }
\livretPers Isménide
\livretVerse#12 { \transparent { Non, je ne te hais plus. } Terminez mon tourment, }
\livretVerse#12 { Que la même fureur contre moi vous anime. }
\livretPers Pircaride
\livretVerse#12 { Impitoyable amour, n’exige rien de moi, }
\livretVerse#12 { Si pour me faire aimer il faut commettre un crime ; }
\livretVerse#12 { Et ne serais-je pas moi-même la victime }
\livretVerse#12 { D’un ingrat que je veux ramener sous ma loi ! }
\livretVerse#12 { C’en est fait, la pitié triomphe de la haine, }
\livretVerse#12 { Moi-même à vos malheurs je donne des soupirs ; }
\livretVerse#8 { C’est trop vous paraître inhumaine, }
\livretVerse#12 { Je vais servir mes feux, en servant vos désirs. }
\livretPers Isménide
\livretVerse#12 { Par quel charme ai-je pu calmer votre colère ? }
\livretPers Pircaride
\livretVerse#12 { Ne craignez rien, je vais vous rendre à votre amant, }
\livretVerse#10 { Et s’il se peut, par mon déguisement, }
\livretVerse#10 { Tromper toujours l’ingrat qui sait me plaire. }
\livretRef#'DBCair
\livretVerse#12 { Vous qui m’obéissez, paraissez à mes yeux, }
\livretVerse#8 { Venez signaler ma puissance ; }
\livretVerse#12 { Ramenez cet objet dans les aimables lieux, }
\livretVerse#12 { Où l’amour doit bientôt couronner sa constance ; }
\livretVerse#12 { Partez, volez, servez ses désirs amoureux. }
\livretDidasPPage\justify {
  Isménide est enlevée par des génies.
}

\livretScene SCÈNE III
\livretDescAtt\wordwrap-center { Pircaride, sous les traits d’Isménide. }
\livretPers Pircaride
\livretRef#'DCAair
\livretVerse#12 { Elle part, & mon cœur n’est point exempt d’alarmes ! }
\livretVerse#12 { C’est sous ses traits qu’amour vient flatter mon ardeur ; }
\livretVerse#12 { Quelle honte ! mes yeux, pour toucher mon vainqueur }
\livretVerse#8 { Vous avez besoin d’autres charmes ! }
\livretVerse#12 { C’est en vain que l’amour veut rassurer mon cœur, }
\livretVerse#12 { Je ne saurais calmer l’ennui qui me dévore ; }
\livretVerse#12 { Je vais m’offrir aux yeux de l’amant que j’adore, }
\livretVerse#12 { J’entendrai des soupirs pour un autre que moi ! }
\livretVerse#8 { Il m’exprimera sa tendresse, }
\livretVerse#8 { Tandis qu’il me manque de foi ; }
\livretVerse#12 { Ô dieux ! il vient, cachons ma honte & ma faiblesse. }

\livretScene SCÈNE IV
\livretDescAtt\wordwrap-center { Numapire, Pircaride, sous les traits d’Isménide. }
\livretPers Numapire
\livretRef#'DDArecit
\livretVerse#12 { Je sens, en vous voyant accroître mon ardeur, }
\livretVerse#8 { Mille feux dévorent mon âme ; }
\livretVerse#12 { Vous avez par vos yeux allumé plus de flamme }
\livretVerse#10 { Que n’en saurait allumer ma fureur. }
\livretVerse#8 { Eh bien, cruelle que vous êtes, }
\livretVerse#12 { N’aurez-vous point pitié des maux que vous me faites ? }
\livretPers Pircaride
\livretVerse#12 { Non, rien n’égale ceux que tu me fais souffrir : }
\livretVerse#12 { Sous ce fatal amour tu sais cacher ta haine, }
\livretVerse#12 { Hélas ! si tu m’aimais, tu finirais ma peine, }
\livretVerse#8 { Mais tu veux me laisser mourir. }
\livretPers Numapire
\livretVerse#12 { Dieux ! pouvez-vous me faire un si sanglant outrage ! }
\livretVerse#12 { Douter de mon amour, lorsque je meurs pour vous ; }
\livretVerse#12 { Qui pourrait me porter de plus sensibles coups ? }
\livretVerse#12 { Mes soupirs, mes transports, ma langueur & ma rage, }
\livretVerse#12 { Si vous ne les croyez, quel témoin croirez-vous ? }
\livretPers Pircaride
\livretVerse#12 { Aime un cœur qui t’adore, & fuis une inhumaine, }
\livretVerse#8 { Fais ton bonheur d’être constant ; }
\livretVerse#8 { Dois-je compter sur un amant }
\livretVerse#8 { Qui brise une si belle chaîne. }
\livretPers Numapire
\livretVerse#8 { Non, je ne l’aimerai jamais, }
\livretVerse#8 { Tout vous en donne l’assurance ; }
\livretVerse#8 { Pour être sur de ma constance, }
\livretVerse#8 { Il fallait avoir vos attrait. }
\livretPers Pircaride
\livretVerse#12 { D’une amante outragée évitez la vangeance. }
\livretPers Numapire
\livretVerse#12 { Pour défendre vos jours j’aurai plus de puissance ; }
\livretVerse#12 { Je vous aime, Isménide, autant que je la hais. }
\livretPersDidas Pircaride à part.
\livretVerse#12 { Le perfide ! aimez-moi s’il se peut davantage, }
\livretVerse#12 { Pour partager les maux de mon triste esclavage. }
\livretVerse#12 { Hélas ! }
\livretPers Numapire
\livretVerse#12 { \transparent { Hélas ! } Vous soupirez, vos yeux versent des pleurs, }
\livretVerse#12 { Ah ! si pour moi, l’amour faisait couler ces larmes ! }
\livretPers Pircaride
\livretVerse#8 { C’est lui qui cause mes alarmes. }
\livretVerse#12 { Je n’ai pu resister à ses attraits vainqueurs, }
\livretVerse#12 { Il triomphe, & toujours sous de feintes rigueurs, }
\livretVerse#8 { J’ai voulu cacher ma tendresse, }
\livretVerse#12 { C’est assez déguiser… c’est pour vous qu’il me blesse… }
\livretPers Numapire
\livretVerse#6 { Que mon sort est heureux ! }
\livretVerse#8 { Je suis au comble de mes vœux. }
% \livretPers Ensemble
% %# Amour, si j'éprouvais des peines,
% %# Je goûte plus les biens que je ressens;
% %# Qu'il est doux de porter tes chaînes,
% %# Lorsque tes plaisirs sont charmants.
\livretPers Numapire
\livretVerse#6 { Vous, que ma voix appelle, }
\livretVerse#12 { Venez, par vos transports me marquer votre zèle, }
\livretVerse#12 { De ces climats brûlants où s’étend mon pouvoir, }
\livretVerse#12 { Accourez, venez tous célébrer votre reine ; }
\livretVerse#12 { Que vos yeux enchantés du plaisir de la voir, }
\livretVerse#12 { Applaudissent au choix que je fais de sa chaîne. }

\livretScene SCÈNE V
\livretDescAtt\wordwrap-center {
  Numapire, Pircaride, troupe de salamandres sous la forme
  de divers peuples d’Afrique.
}
\livretPers Chœur
\livretRef#'DEBchoeur
\livretVerse#8 { Chantons, célébrons notre reine, }
\livretVerse#8 { Portons nos voix jusques aux cieux, }
\livretVerse#12 { Le bonheur d’un amant qui peut porter sa chaîne, }
\livretVerse#8 { Égale le bonheur des dieux. }
\livretPersDidas Une Africaine à Pircaride.
\livretRef#'DEFair
\livretVerse#8 { L’amour a besoin de vos charmes }
\livretVerse#8 { Pour se rendre victorieux, }
\livretVerse#8 { Il triomphe plus par vos yeux }
\livretVerse#8 { Qu’il ne triomphe par ses armes. }
\livretVerse#8 { Lorsque vous soumettez un cœur }
\livretVerse#8 { L’amour est fier de sa victoire, }
\livretVerse#8 { Il ne compte pour rien sa gloire, }
\livretVerse#8 { Quand lui seul en est le vainqueur. }
\livretVerse#8 { L’amour a besoin, &c. }
\livretRef#'DEGair
\livretDidasPPage On danse.
% \livretPers Une Africaine
% %# Ah! quel heureux jour
% %# L'amour nous présage;
% %# Dans notre séjour
% %# Cherchons son esclavage:
% %# Ses vives ardeurs
% %# Ont mille douceurs,
% %# À ses traits vainqueurs
% %# Présentons nos cœurs.
% %# Hâtons-nous d'aimer,
% %# Qui sait nous charmer
% %# Peut-on être *heureux
% %# Sans former de doux nœuds.
% %# Aimons, chantons, ri=ons toujours
% %# C'est dans nos jeux que règnent les amours.
% \livretDidasPPage On danse.
\livretPersDidas Pircaride à sa suite
\livretRef#'DEJrecit
\livretVerse#12 { Finissez ces concerts, votre hommage m’offense. }
\livretPers Numapire
\livretVerse#8 { Qu’entends-je, ô ciel ! }
\livretPers Pircaride
\livretVerse#8 { \transparent { Qu’entends-je, ô ciel ! } Reconnais-moi : }
\livretVerse#12 { En éloignant l’objet dont tu suivais la loi, }
\livretVerse#12 { Sous ses traits empruntés j’ai rempli ma vengeance. }
\livretPers Numapire
\livretVerse#12 { Isménide, grands dieux ! }
\livretPers Pircaride
\livretVerse#12 { \transparent { Isménide, grands dieux ! } Tu ne la verras plus. }
\livretVerse#8 { Auprès de ton rival qu’elle aime, }
\livretVerse#8 { Elle goûte un bonheur extrême, }
\livretVerse#12 { Et laisse à ton amour des regrets superflus. }
\livretPers Numapire
\livretVerse#8 { Suivons la fureur qui me guide, }
\livretVerse#10 { Allons punir & l’amante & l’amant ; }
\livretVerse#8 { Ah ! que ne puis-je aussi, perfide, }
\livretVerse#12 { T’immoler à ma rage en cet affreux moment. }
\livretPersDidas Pircaride sur un char de feu
\livretVerse#8 { Ici je brave ta vengeance, }
\livretVerse#8 { Mon pouvoir égale le tien ; }
\livretVerse#12 { Je vais de ces amants serrer le doux lien, }
\livretVerse#8 { Et c’est moi qui prend leur défense. }
\livretPers Numapire
\livretVerse#12 { La perfide triomphe, & malgré moi je sens }
\livretVerse#12 { Les amoureux transports de la plus vive flamme ; }
\livretVerse#8 { Elle protège ces amants ! }
\livretVerse#12 { Où suis-je ? quelle horreur s’empare de mon âme ! }
\livretVerse#12 { Je ne puis me venger, que je suis malheureux ! }
\livretVerse#12 { Du moins, si je ne puis exercer ma vangeance, }
\livretVerse#12 { Détruisons ce palais, témoin de mon offense ; }
\livretVerse#12 { Que ne puis-je périr pour éteindre mes feux. }
\livretDidasPPage À sa suite.
\livretRef#'DEKair
\livretVerse#8 { Servez les transports de ma rage, }
\livretVerse#12 { Ravagez ce séjour, qu’il perde ses attraits ; }
\livretVerse#12 { Que le feu dévorant le consume à jamais, }
\livretVerse#12 { Et qu’il n’offre aux regards qu’une effrayante image. }
\livretPers Chœur
\livretVerse#8 { Servons les transports de sa rage, }
\livretVerse#12 { Ravageons se séjour, qu’il perde ses attraits ; }
\livretVerse#12 { Que le feu dévorant le consume à jamais, }
\livretVerse#12 { Et qu’il n’offre aux regards qu’une effrayante image. }
\livretDidasPPage\justify {
  Le palais est détruit par le feu.
}
\livretFinAct FIN DE LA TROISIÈME ENTRÉE
\sep
\livretAct QUATRIÈME ENTRÉE
\livretScene\wordwrap { Les Sylphes, ou l’amour léger. }
\livretDescAtt\justify {
  Le Théatre représente un lieu préparé pour y donner une fête
  galante.
}
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\wordwrap-center { Un Sylphe. }
\livretPers Un Sylphe
\livretRef#'EABrecit
\livretVerse#8 { Le ciel a fixé mon empire }
\livretVerse#7 { Entre les cieux & les mers, }
\livretVerse#12 { Je règne en souverain dans l’espace des airs, }
\livretVerse#8 { Mais l’unique bien où j’aspire }
\livretVerse#12 { C’est de charmer l’objet dont je porte les fers. }
\livretVerse#8 { Ces lieux sont ornés pour lui plaire }
\livretVerse#8 { Amour, seconde mes désirs ; }
\livretVerse#12 { Si cet objet charmant demande un cœur sincère, }
\livretVerse#10 { Fixe mes vœux, fais durer mes plaisirs. }

\livretScene SCÈNE II
\livretDescAtt\wordwrap-center { Un Sylphe, une Sylphide. }
\livretPers Le Sylphe
\livretRef#'EBArecit
\livretVerse#12 { Ne dissimulez point, votre cœur est volage, }
\livretVerse#8 { Vous ne vivez plus sous ma loi. }
\livretPers La Sylphide
\livretVerse#8 { Lorsque vous me manquez de foi, }
\livretVerse#12 { Vous offenseriez-vous quand mon cœur se dégage ? }
\livretPers Le Sylphe
\livretVerse#12 { Non, je ne croyais pas que dans le même jour }
\livretVerse#8 { Qu’un aimable nœud nous engage, }
\livretVerse#10 { Qu’en m’apprenant à connaître l’amour, }
\livretVerse#10 { Vous m’apprendriez à devenir volage. }
\livretPers La Sylphide
\livretVerse#12 { Vous devez rendre grâce à ma légèreté, }
\livretVerse#8 { Est-il un plus grand avantage ? }
\livretVerse#12 { Des douceurs de l’amour vous savez faire usage }
\livretVerse#8 { En conservant la liberté. }
\livretPers Le Sylphe
\livretVerse#8 { L’amour brille de moins de charmes, }
\livretVerse#8 { Vous savez toucher tous les cœur ; }
\livretVerse#8 { Sous vos lois il n’est point d’alarmes, }
\livretVerse#8 { On ne goûte que des douceurs. }
\livretVerse#8 { Vous désarmez le plus rebèle, }
\livretVerse#8 { Il est contraint à s’enflammer, }
\livretVerse#8 { Si vous n’étiez point infidèle }
\livretVerse#8 { On voudrait toujours vous aimer. }
\livretPers La Sylphide
\livretVerse#8 { Un amant tel que vous enchante, }
\livretVerse#8 { Vous aimez sans être jaloux : }
\livretVerse#8 { Vous n’exigez point d’une amante, }
\livretVerse#8 { De ne soupirer que pour vous. }
\livretVerse#8 { Vous êtes dans votre tendresse }
\livretVerse#8 { Complaisant, sincère & discret ; }
\livretVerse#8 { Si mon cœur a de la faiblesse, }
\livretVerse#8 { Vous savez garder le secret. }
\livretPers Le Sylphe
\livretVerse#12 { Je sens que mon amour aurait été fidèle, }
\livretVerse#8 { Si le votre eut été constant. }
\livretPers La Sylphide
\livretVerse#10 { Sans le plaisir d’une flamme nouvelle, }
\livretVerse#8 { J’aimerais encor mon amant. }
\livretPers Ensemble
\livretVerse#10 { Lance tes traits, remporte la victoire, }
\livretVerse#8 { Amour, triomphe de mon cœur ; }
\livretVerse#8 { Non, tu n’as jamais tant de gloire }
\livretVerse#8 { Que dans une inconstante ardeur. }
\livretPers La Sylphide
\livretVerse#8 { Je vois ma nouvelle conquête. }
\livretPers Le Sylphe
\livretVerse#12 { La mienne doit se rendre au milieu de la fête. }
\livretPers Ensemble
\livretVerse#7 { Allons préparer des jeux }
\livretVerse#8 { Dignes de nos soins amoureux. }

\livretScene SCÈNE III
\livretDescAtt\wordwrap-center {
  Florise déguisée en cavalier, un masque à la main.
}
\livretPers Florise
\livretRef#'ECAair
\livretVerse#12 { C’est ici que l’amour va m’offrir des hommages, }
\livretVerse#12 { Qui vont faire briller le pouvoir de ses traits ; }
\livretVerse#12 { Sous ce déguisement redoublent mes attraits, }
\livretVerse#8 { Je vais tromper des cœurs volages. }
\livretVerse#8 { Amour, sous tes aimables lois, }
\livretVerse#8 { Tu soumets à jamais mon âme ; }
\livretVerse#12 { Permets que pour ta gloire & l’honneur de mon choix, }
\livretVerse#10 { Je puisse feindre une amoureuse flamme. }

\livretScene SCÈNE IV
\livretDescAtt\wordwrap-center { Florise, la Sylphide. }
\livretPers Florise
\livretRef#'EDArecit
\livretVerse#12 { Belle Nymphe, à l’éclat dont brillent vos beaux yeux, }
\livretVerse#8 { Que de cœurs vont rendre les armes ! }
\livretVerse#12 { Non, non, du dieux d’amour les traits victorieux, }
\livretVerse#8 { Sont moins à craindre que vos charmes. }
\livretPers La Sylphide
\livretVerse#12 { D’une foule d’amants qui vole sur mes pas }
\livretVerse#7 { Je ne crains point le langage ; }
\livretVerse#8 { Il est un amant dont l’hommage }
\livretVerse#7 { Aurait pour moi des appas. }
\livretPers Florise
\livretVerse#12 { Et quel est cet amant ? ah ! que je porte envie }
\livretVerse#8 { Au sort dont vous flatter son cœur ; }
\livretVerse#8 { Le plus doux instant de ma vie, }
\livretVerse#8 { Serait marqué par ce bonheur. }
\livretPers La Sylphide
\livretVerse#12 { La langueur des amants sans cesse me fait rire : }
\livretVerse#12 { Ils m’adressent leurs vœux, je folâtre toujours ; }
\livretVerse#12 { Quand je suis près de vous, je sens que je soupire, }
\livretVerse#8 { Que me demandent les amours ? }
\livretPers Florise
\livretVerse#8 { Ah ! c’en est trop Nymphe charmante, }
\livretVerse#12 { Un aveu si flatteur paie assez mes soupirs. }
\livretPers La Sylphide
\livretVerse#8 { Que notre tendresse s’augmente }
\livretVerse#8 { Par l’espoir de mille plaisirs. }
\livretPers Ensemble
\livretVerse#8 { Formons une chaîne si belle }
\livretVerse#8 { Au milieu des ris & des jeux : }
\livretVerse#8 { Vole amour, viens nous rendre heureux, }
\livretVerse#8 { C’est la constance qui t’appelle. }

\livretScene SCÈNE V
\livretDescAtt\wordwrap-center {
  Le Sylphe, la Sylphide, Florise, troupes de Sylphes & de Sylphides,
  sous divers déguisements.
}
\livretPers Chœur
\livretRef#'EEBchoeur
\livretVerse#8 { Chantons, ne songeons qu’aux plaisirs, }
\livretVerse#8 { Profitons de l’âge des grâces, }
\livretVerse#8 { Pour mieux répondre à nos désirs, }
\livretVerse#8 { Les amours volent sur nos traces. }
\livretRef#'EECcotillon
\livretDidasPPage On danse.
\livretPersDidas Le Sylphe à sa suite.
\livretRef#'EEErecit
\livretVerse#12 { Ce lieu va recevoir la beauté qui m’engage, }
\livretVerse#10 { Vous, qui sous d’aimables déguisements }
\livretVerse#8 { Venez lui rendre votre hommage, }
\livretVerse#10 { Formez des jeux & des concerts charmants. }
\livretRef#'EEFair
\livretVerse#10 { Que de son nom ce séjour retentisse, }
\livretVerse#8 { Applaudissez à mon ardeur ; }
\livretVerse#10 { Qu’à mes transports votre zèle s’unisse, }
\livretVerse#8 { Ne songeons qu’à toucher son cœur. }
% \livretPers Un masque
% %# Un dolce canto di vaga belta.
% %# Puel dar si vanto din cantar la liberta,
% %# Ei rende immorta la dea vagante,
% %# El crin volante porger le fa.
\livretRef#'EEHsarabande
\livretDidasPPage On danse.
\livretPers Le Sylphe
\livretRef#'EEKrecit
\livretVerse#12 { Vous ne paraissez point cher objet que j’adore, }
\livretVerse#12 { Quelque rival jaloux retiendrait-il vos pas ? }
\livretVerse#12 { Sans vous, ce beau séjour est pour moi sans appas. }
\livretVerse#10 { Venez calmer le feu qui me dévore. }
\livretPersDidas Florise masquée.
\livretVerse#12 { Et quelle est la beauté qui cause vos soupirs ? }
\livretPers Le Sylphe
\livretVerse#12 { Je l’ai vue un moment, moment trop redoutable }
\livretVerse#12 { Pour la perte d’un cœur qu’amusaient les plaisirs ! }
\livretVerse#12 { Sans fixer mon amour, les plus tendres désir }
\livretVerse#12 { Semblaient me rendre heureux près d’un objet aimable. }
\livretVerse#8 { Mais, hélas ! depuis cet instant }
\livretVerse#8 { Les soins m’accompagnent sans cesse, }
\livretVerse#8 { Et j’éprouve dans ma tendresse, }
\livretVerse#8 { Que mon plaisir est mon tourment. }
\livretVerse#8 { Florise cause mon martyre. }
\livretPers Florise
\livretVerse#10 { Je la connais. Cette jeune beauté }
\livretVerse#8 { N’aime pas un cœur qui soupire ; }
\livretVerse#8 { L’amant qui folâtre l’attire, }
\livretVerse#12 { Et l’amant qui se plaint est toujours rebuté. }
\livretPers Le Sylphe
\livretVerse#8 { Je sais accommoder ma chaîne }
\livretVerse#12 { Aux caprices d’un cœur dont je suis enchanté ; }
\livretVerse#8 { Et pour vaincre sa cruauté, }
\livretVerse#8 { Je ne compte pour rien la peine. }
\livretPers Florise
\livretVerse#6 { Elle aime un cœur constant ; }
\livretVerse#6 { Quelque fois un volage }
\livretVerse#8 { Pour le plaisir du changement : }
\livretVerse#8 { Pour vous faire à son badinage, }
\livretVerse#8 { Êtes-vous l’un & l’autre amant ? }
\livretPers Le Sylphe
\livretVerse#7 { L’inconstance est mon partage, }
\livretVerse#8 { Je ne suis constant qu’à regret ; }
\livretVerse#8 { Mais pour charmez un bel objet, }
\livretVerse#8 { La constance est mon tendre hommage. }
\livretPers Florise
\livretVerse#12 { Vous êtes ce qu’il faut pour plaire à ses beaux yeux, }
\livretVerse#10 { Mais de son cœur elle n’est plus maîtresse ; }
\livretVerse#8 { Et son amant est dans ces lieux. }
\livretPers Le Sylphe
\livretVerse#12 { Ah ! de quel coup mortel frappez-vous ma tendresse ! }
\livretPersDidas Florise, se démasquant, & parlant à un masque du bal.
\livretVerse#12 { Dorante approchez-vous, digne objet de mes vœux, }
\livretVerse#8 { Florise veut vous rendre heureux. }
\livretPers Le Sylphe et la Sylphide
\livretVerse#10 { Ô ciel ! }
\livretPers Florise
\livretVerse#10 { \transparent { Ô ciel ! } Je vous ai trompé l’un & l’autre, }
\livretVerse#8 { Mais c’est pour mieux serrer vos nœuds ; }
\livretVerse#12 { Aimez, que votre amour puisse imiter le nôtre, }
\livretVerse#8 { Jamais rien n’éteindra vos feux. }
\livretPers Le Sylphe et la Sylphide
\livretVerse#8 { Suivons cet exemple sans peine, }
\livretVerse#8 { Aimons pour ne jamais changer ; }
\livretVerse#8 { Le plaisir de se dégager }
\livretVerse#12 { Ne vaut pas le plaisir de reprendre sa chaîne. }
\livretPers Florise
\livretRef#'EELair
\livretVerse#8 { Triomphe, fais voler tes traits, }
\livretVerse#8 { Tendre amour, règne dans nos fêtes ; }
\livretVerse#8 { Fais ta gloire de nos défaites, }
\livretVerse#8 { Mais laisse-nous aimer en paix. }
\livretDidasPPage On danse.
\livretPers Chœur
\livretVerse#8 { Chantons, ne songeons qu’aux plaisirs, }
\livretVerse#8 { Profitons de l’âge des Grâces : }
\livretVerse#8 { Pour mieux répondre à nos désirs, }
\livretVerse#8 { Les amours volent sur nos traces. }
\livretFinAct FIN
}
