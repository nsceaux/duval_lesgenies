\livretAct PROLOGUE
\livretDescAtt\justify {
  Le Théatre représente un désert.
}
\livretScene SCÈNE PREMIÈRE
\livretPers Zoroastre
\livretRef#'AABrecit
%# Il est temps que mon Art instruise les Mortels,
%# Dans les secrets des Dieux le premier j’ai su lire:
%# Méritons comme eux des autels,
%# Et montrons mon pouvoir à tout ce qui respire.
\livretRef#'AACair
%# Esprits soumis à mes commandements,
%# Venez remplir mon espérance,
%# Rassemblez-vous des divers Éléments,
%# Et signalez ma gloire et ma puissance.

\livretScene SCÈNE II
\livretDescAtt\wordwrap-center {
  Zoroastre, et les génies.
}
\livretPers Zoroastre
\livretRef#'ABAair
%# Que la Terre, le Feu, que l’Onde, que les Airs
%# Découvrent les trésors que mon Art fait éclore;
%# Volez, dispersez-vous du Couchant à l’Aurore,
%# De vos bienfaits remplissez l’Univers.
\livretRef#'ABCdanse
\livretDidasPage DANSE POUR LES GENIES.
\livretDidasP\justify {
  On entend un douce harmonie qui annonce la descente de l’Amour.
}
\livretPers Zoroastre
\livretRef#'ABDair
%# Quels bruits! quels doux accords! quelle clarté nouvelle!
%# L’*horreur des ces déserts disparaît à mes yeux!
%# Quel Dieu descend de la Cour immortelle,
%# Pour venir embellir ces lieux?
%# Ah! je le reconnais à sa douceur extrême,
%# C’est l’Amour! et quel Dieu se fait sentir de même!

\livretScene SCÈNE III
\livretDescAtt\wordwrap-center {
  L’Amour, Zoroastre, et les génies.
}
\livretPers L'Amour
\livretRef#'ACAair
%# Tout obé=it, tout s’éveille à ta voix!
%# Tu déchaines les Vents, tu fais trembler la Terre!
%# Tu soulèves les Flots, tu lances le Tonnerre;
%# Mais l’Amour seul ne connaît point tes lois.
\livretPers Zoroastre
%# Tout reconnaît votre pouvoir suprême,
%# Régnez, tri=omphez Dieu charmant;
%# Il n’est point de plus doux moment,
%# Que l’instant où l’on dit qu’on aime.
\livretPers L'Amour
%# Qui vous amène en ces déserts?
%# À de nouveaux Sujets je viens donner des fers.
%# Peuples des Éléments, connaissez ma puissance;
%# Je règne sur tout l’Univers,
%# Éprouvez en ce jour les traits que l’Amour lance.
%# Les maux qu’ils font, doivent être plus chers
%# Que les biens de l’indifférence.
\livretRef#'ACBair
%# Accourez Jeux charmants, volez tendres Amours,
%# Formez les plus galantes Fêtes;
%# Quand on aime, tout âge est l’âge des beaux jours.
%# Plaisirs, lancez mes traits, étendez mes conquêtes.

\livretScene SCÈNE IV
\livretDescAtt\wordwrap-center {
  L’Amour, Zoroastre, et les génies, Troupe de Plaisirs & de Jeux.
}
\livretPers Chœur
\livretRef#'ADEchoeur
%# Du doux bruit de nos chants que ces lieux retentissent,
%# Les Amours & les Jeux, pour nos plaisirs s'unissent;
%# Aimons, goûtons mille douceurs,
%# L'Amour les promet à nos cœurs.
\livretFinAct FIN DU PROLOGUE
\sep
