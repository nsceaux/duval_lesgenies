\livretAct  PREMIÈRE ENTRÉE
\livretScene\wordwrap { Les Nymphes, ou l’amour indiscret. }
\livretDescAtt\justify {
  Le Théatre représente un agréable jardin sur le bord de la mer.
}
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\wordwrap-center { Léandre, Zerbin. }
\livretPers Léandre
\livretRef#'BABrecit
%# Viens être le témoin du bonheur qui m'enchante,
%# C'est dans ces lieux qu'Amour répond à mes désirs;
%# Sans exiger de moi ni larmes ni soupirs,
%# Il rend ma flamme tri=omphante.
\livretPers Zerbin
%# Ah! si ce dieu comble vos vœux
%# Ne le faites jamais paraître;
%# Un cœur dans l'empire amoureux,
%# Devrait, pour être plus heureux,
%# Douter toujours de l'être.
\livretPers Léandre
%# Les plaisirs dont l'amour sait enchanter les sens,
%# Satisfont les désirs d'un amant qui soupire;
%# Pour moi, libre du soin de ces tendres amants,
%# Non, non je ne les ressens,
%# Qu'autant que je puis les redire.
\livretPers Zerbin
%# Qui ne sait garder le secret,
%# Goûte peu de douceurs parfaites,
%# Elles n'ont jamais été faites
%# Pour un amant indiscret.
%# Quel objet vous retient dans cet heureux asile?
%# Venez-vous attendre Lucile?
\livretPers Léandre
%# Un objet plus charmant m'arrête dans ces lieux,
%# Zerbin, il va bientôt sortir du sein de l'onde
%# Pour me rendre l'amant le plus heureux du monde;
%# Demeure, son abord va surprendre tes yeux.
\livretRef#'BACair
%# Jamais la reine de Cythère
%# N'a brillé de tant d'appas,
%# L'Amour ne connaît plus sa mère,
%# Depuis qu'il suit les pas
%# De l'aimable objet qui m'enchaîne:
%# Son char conduit par les Zéphirs,
%# Vole sur la liquide plaine;
%# Les vents à son aspect, retiennent leur haleine,
%# Les ris, les jeux & les plaisirs
%# Folâtrent sans cesse autour d'elle;
%# On ne saurait voir cette belle,
%# Sans former de tendre désirs.
\livretRef#'BADrecit
%# Lucile vient, j'évite sa présence
%# Elle me croit constant, que je plains son erreur!
\livretPers Zerbin
%# Dois-je de son amour affermir la constance?
\livretPers Léandre
%# Ce n'est plus un secret que ma nouvelle ardeur.

\livretScene SCÈNE II
\livretDescAtt\wordwrap-center { Lucile, Zerbin. }
\livretPers Lucile
\livretRef#'BBAair
%# Asile des plaisirs, beau lieu rempli de charmes,
%# Offrez à mes regards l'objet de mon amour.
%# Mon cœur en son absence éprouve des alarmes
%# Que rien ne peut calmer, que son heureux retour.
%# Asile des plaisirs, beau lieu rempli de charmes,
%# Offrez à mes regards l'objet de mon amour.
\livretPersDidas Zerbin à part
\livretRef#'BBBrecit
%# Mérites-tu volage, un cœur si tendre?
%# Pour qui réserves-tu tes plus funestes coups,
%#- Cru=el Amour?
\livretPers Lucile
%#- Zerbin.
\livretPers Zerbin
%#= Je parle de Lé=andre.
%#- C'est un amant…
\livretPers Lucile
%#- Eh quoi?
\livretPers Zerbin
%#= Trop indigne de vous.
\livretPers Lucile
%#- Quoi? Lé=andre, Zerbin!
\livretPers Zerbin
%#= Lé=andre vous adore;
%# Mais à d'autres qu'à vous, Lé=andre en dit autant.
\livretPers Lucile
%# Après tous ses serments, l'ingrat me trompe encore.
\livretPers Zerbin
%# Affectez quelque changement
%# Pour vous vanger de cet outrage;
%# C'est s'assurer de son amant,
%# Que de feindre d'être volage.
\livretPers Lucile
%# Amante infortunée, hélas!
%# Mes soupirs, mes regards trahiraient ce mystère;
%# Ma bouche lui dirait que je ne l'aime pas,
%# Et dans mes yeux il lirait le contraire.
\livretRef#'BBCair
%# Venez, juste Dépit, venez à mon secours,
%# Bannissez de mon cœur un amant infidèle;
%# Que de plus constantes amours
%# Allument dans mon âme une flamme nouvelle.
%# Venez, juste Dépit, venez à mon secours,
%# Bannissez de mon cœur un amant infidèle.
\livretPers Zerbin
\livretRef#'BBDrecit
%# Mais, c'est lui qui vient dans ces lieux:
%# Pour connaître son cœur, cachez-vous à ses yeux.
\livretPers Lucile
%# L'ingrat! je l'aime encor, malgré son inconstance.
\livretPers Zerbin
%# Venez, évitez sa présence.

\livretScene SCÈNE III
\livretPers Léandre
\livretRef#'BCAair
%# Reviens cher objet de mes vœux;
%# Déjà l'astre du jour éteint ses feux dans l'onde,
%# Il est temps à mes vœux que ton amour réponde,
%# Viens rendre ton amant heureux.
\livretRef#'BDAentree
\livretDidasPPage\justify {
  On entend une douce harmonie ; la nymphe paraît sur une conque marine,
  suivie de sa cour.
}

\livretScene SCÈNE IV
\livretDescAtt\wordwrap-center {
  Léandre, la principale Nymphe, & sa suite ; Troupe d’Ondins et de Nymphes.
}
\livretPers Léandre
\livretRef#'BDBduo
%# Qu'éloigné de votre présence,
%# J'ai souffert de maux rigoureux!
%# Mais que ces maux sont doux lorsqu'après votre absence,
%# Je revois encor vos beaux yeux!
\livretPers La principale Nymphe
%# Ah! quel aveu charmant, qu'il m'est doux de l'entendre!
%# Amour, mes vœux sont satisfaits,
%# La gloire de régner sur un cœur aussi tendre
%# Est le plus cher de tes bienfaits.
\livretPers Ensemble
%# Amour, viens nous unir de tes plus douces chaînes,
%# Vole, réponds à nos désirs;
%# Nos cœurs ne sont point faits pour éprouver tes peines,
%# Ne nous offre que tes plaisirs.
\livretPers La principale Nymphe
%# Nymphes, vous qui formez ma cour la plus brillante,
%# Vous habitants des mers qui vivez sous mes lois,
%# Rassemblez-vous troupe charmante,
%# Venez, accourez à ma voix.
\livretPers La principale Nymphe
\livretRef#'BDCair
%# Chantez dans ce ri=ant boccage,
%# Célébrez de l'Amour les tri=omphes divers,
%# Il retient sous son esclavage
%# Les cieux, la terre & les enfers;
%# Qu'il règne autant sur ce rivage,
%# Qu'il règne dans le sein des mers.
\livretPers Chœur
\livretRef#'BDDchoeur
%#8 Chantons, &c.
\livretRef#'BDEpassacaille
\livretDidasPPage On danse.
\livretPers La principale Nymphe
\livretRef#'BDHair
%# Amour, tu réponds à nos vœux,
%# Tri=omphe à jamais de nos âmes,
%# Ce n'est qu'en éprouvant tes flammes,
%# Que les cœurs peuvent être *heureux.
%# Tous les oiseaux de ces boccages
%# Sous tes lois goûtent des douceurs,
%# Ils ne raniment leurs ramages
%# Que pour célébrer tes faveurs.
%# Tri=omphe à jamais de nos âmes,
%#8 Amour, &c.
\livretDidasPPage On danse.
\livretPers Une Nymphe
\livretRef#'BDKchoeur
%# Ri=ons, chantons sous cet ombrage,
%# Tout y répond à nos désirs;
%# L'Amour y cache les plaisirs
%# Dont notre printemps fait usage.
\livretPers Chœur
%#8 Ri=ons, chantons, &c.
\livretPers La Nymphe
%# Sans soins, sans crainte des jalous,
%# Nous nous livrons à la tendresse;
%# Et le tendre amour ne nous blesse,
%# Que pour nous faire un sort plus doux.
\livretPers Chœur
%# Ri=ons, chantons sous cet ombrage,
%# Tout y répond à nos désirs;
%# L'Amour y cache les plaisirs
%# Dont notre printemps fait usage.
\livretPersDidas La principale Nymphe à Léandre
\livretRef#'BDNrecit
%# Tout prévient ici vos désirs,
%# La sévère sagesse, & la raison cru=elle
%# Ne sauraient troubler nos plaisirs;
%# Mais soy=ez-moi toujours fidèle.
\livretPers Ensemble
%# Aimons-nous, aimons-nous d'une ardeur éternelle.

\livretScene SCÈNE V
\livretDescAtt\wordwrap-center {
  La principale Nymphe, & sa suite.
  Léandre, Lucile, Zerbin.
}
\livretPers Lucile
\livretRef#'BEArecit
%# Poursuis, ingrat, poursuis volage, amant sans foi,
%# Fais éclater tes feux auprès de cette belle:
%# Va, tu peux lui jurer une ardeur éternelle
%# Que ton cœur n'a promis qu'à moi.
%# Perfide, garde-toi de paraître à ma vue;
%# C'en est fait, pour jamais mes li=ens sont rompus.
\livretPers Léandre
%# Hélas! je vous ai donc perdue,
%#- Lucile, vous fuy=ez!
\livretPers Zerbin
%#= Vous ne la verrez plus.
\livretPers La principale Nymphe
%# Ah! puis-je soutenir un si sanglant outrage,
%# Sans immoler un traitre à ma fureur?
%# Je sens que mon âme s'abandonne à la rage,
%# Perfide, sauve-toi de mon courroux vangeur.
\livretPersDidas Léandre à Zerbin
%# Allons chercher Lucile, & pour fléchir son cœur,
%# Jurons à ses beaux yeux la plus fidèle ardeur.
\livretPers La principale Nymphe
%# Que tout serve ici ma colère
%# Pour punir un ingrat qui m'avait trop su plaire.
\livretRef#'BEBair
%# Venez tyrans des airs, aquilons furi=eux,
%# Excitez sur ce bord le plus affreux orage;
%# Que les flots irrités s'élèvent jusqu'aux cieux,
%# Vangez-moi, lavez mon outrage,
%# Innondez pour jamais ces lieux.
\livretRef#'BECsymphonie
\livretDidasPPage\line { On voit les flots se soulever. }
\livretFinAct FIN DE LA PREMIÈRE ENTRÉE
\sep
