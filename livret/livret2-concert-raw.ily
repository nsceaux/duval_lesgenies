\livretAct DEUXIÈME ENTRÉE
\livretScene\wordwrap { Les gnomes, ou l’amour ambitieux. }
\livretDescAtt\justify {
  Le Théatre représente une solitude, bornée par un bosquet.
}
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\wordwrap-center { Zaïre, Zamide. }
\livretPers Zaïre
\livretRef#'CAAair
%# Douce erreur, charmante chimère,
%# Pourquoi faut-il que la clarté du jour,
%# Chasse l'espoir dont me flattait l'amour?
%# Et que tu ne sois plus qu'un bien imaginaire?
\livretPers Zamide
%# Za=ïre, arrêtez-vous? qui vous guide en ces lieux?
%# Vos sens sont agités, mille douces alarmes
%# D'un éclat plus brillant embellissent vos yeux;
%# L'amour veut-il enfin récompenser vos charmes?
%# Le sommeil par de doux mensonges
%# Quelque fois donne de beaux jours;
%# Mais le réveil les rend si courts,
%# Qu'ils s'envolent avec les songes.
\livretPers Zaïre
%# Laissez-moi m'occuper des plaisirs que je sens,
%# J'aime à rêver encor dans ce lieu solitaire;
%# L'amour sait ce qui reste à faire,
%# Pour mieux mériter mon encens.

\livretScene SCÈNE II
\livretDescAtt\wordwrap-center { Zaïre seule. }
\livretPers Zaïre.
\livretRef#'CBAair
%# Je cède à ta voix qui m'appelle,
%# Amour, achève mon bonheur;
%# Pour prix de tous les biens dont tu flattes mon cœur,
%# Je t'offre une flamme éternelle.
%# Maître des rois, tu conduis l'univers
%# Tu couronnes des cœurs inconnus sur la terre;
%# Tu forces le dieu du tonnerre,
%# À sortir de son rang, pour être dans tes fers.
%#8 Je cède, &c.

\livretScene SCÈNE III
\livretDescAtt\wordwrap-center { Un gnome, sous le nom d’Adolphe, Zaïre. }
\livretPers Adolphe
\livretRef#'CCArecit
%# Vous voy=ez à vos pieds l'amant le plus fidèle,
%# Et je revois l'objet que j'aime tendrement;
%# Vous ne fûtes jamais si belle,
%# Et jamais mon amour ne fut si vi=olent.
%# Le pouvoir de vos yeux s'étend sur tous les cœurs,
%# Il n'est rien dans les cieux, sur la terre & sur l'onde,
%# Qui ne cède à leurs traits vainqueurs:
%# Jusque dans le centre du monde,
%# Ils savent allumer les plus vives ardeurs.
\livretPers Zaïre
%# À mes faibles appas vous donnez trop d'empire,
%# Ils ne règnent que sur un cœur;
%# La gloire & le bien où j'aspire
%# Serait de faire son bonheur.
%# Je vois que chaque instant redouble vos alarmes.
\livretPers Adolphe
%# C'est douter trop longtemps du pouvoir de vos charmes,
%# Connaissez où s'étend l'empire de vos yeux.
\livretPers Zaïre
%# Que vois-je! où suis-je! ô justes dieux!
\livretRef#'CDAmarche
\livretDidasPPage\justify {
  L’on voit paraître un superbe palais. Une troupe de gnomes sous la
  forme de divers peuples orientaux se préparent pour la fête.
}

\livretScene SCÈNE IV
\livretDescAtt\wordwrap-center {
  Adolphe, Zaïre, troupe de gnomes sous la forme
  de divers peuples orientaux.
}
\livretPers Adolphe
\livretRef#'CDBrecit
%# Rassurez-vous, dissipez votre effroi,
%# Régnez avec Adolphe, en régnant avec moi:
%# Pouvais-je résister de vous rendre les armes,
%# Pour la première fois que j'aperçus vos charmes?
%# Ce fut dans ce jardin où la mère d'Amour
%# Semble avoir fixé son empire:
%# Vous paraissez, Vénus quitte sa cour,
%# Tout se range vers vous, près de vous tout soupire,
%# Les oiseaux enchantés vous parlaient de leurs feux;
%# Les ruisseaux par leur doux murmure,
%# Rendaient hommage à vos beaux yeux;
%# Et le père de la nature
%# Pour vous, du plus beau jour faisait briller ces lieux.
%# Par tant d'attraits, fallait-il me surprendre?
%# Quel cœur aurait pu s'en défendre!
\livretPers Zaïre
%# Votre amour me soumet tous ces peuples divers,
%# Et sur vous désormais je règne en souveraine;
%# Mon destin le plus beau c'est de porter ma chaîne,
%# Et de vous voir porter vos fers.
\livretPers Ensemble
%# Tendre Amour, enchaîne nos âmes,
%# C'est toi seul qui fais mon bonheur;
%# N'allume jamais dans mon cœur
%# D'autres désirs, ni d'autres flammes.
\livretPers Adolphe
%# Dans ces lieux souterrains où je donne la loi,
%# Vous qui reconnaissez ma puissance suprême,
%# Redoublez vos transports pour plaire à votre roi;
%# Mais faites encor plus pour plaire à ce que j'aime.
\livretPers Chœur
\livretRef#'CDCchoeur
%# Régnez dans nos climats, jou=ïssez de la gloire
%# De faire tri=ompher l'amour;
%# Vos yeux à chaque instant augmentent sa victoire,
%# Qu'il vous enchaîne à votre tour.
\livretPersDidas Un Indien à Zaïre
\livretRef#'CDEair
%# Recevez l'éclatant hommage
%# D'un cœur que vous avez dompté,
%# Tri=omphez, goûtez l'avantage
%# D'avoir désarmé sa fierté.
%# La gloire, la magnificence
%# Ne font plus sa félicité;
%# Il ne connaît plus de puissance
%# Que celle de votre beauté.
\livretFinAct FIN DE LA DEUXIÈME ENTRÉE
\sep
