\livretAct TROISIÈME ENTRÉE
\livretScene\wordwrap { Les salamandres, ou l’amour violent. }
\livretDescAtt\justify {
  Le Théatre représente le palais de Numapire.
}
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\wordwrap-center { Isménide. }
\livretPers Isménide
\livretRef#'DAAair
%# Tyran d'un cœur fidèle & tendre,
%# Que t'ai-je fait, cru=el amour?
%# Chaque instant, de mes cris retentit ce séjour,
%# Et tu ne veux pas les entendre.
%# Hélas! loin de l'objet qui cause ma langueur
%# Tu me laisses gémir sous les fers d'un barbare,
%# Et tu permets qu'il me sépare
%# D'un amant qui faisait mon unique bonheur.
%#8 Tyran d'un cœur &c.
\livretRef#'DABrecit
\livretDidasPPage\justify {
  Pircaride sous les traits d'Ismé[ni]de, paraît sur un char de feu un
  poignard à la main.
}
%# Que vois-je? quel objet se présente à mes yeux?
%# Juste ciel! quel couroux l'anime.
\livretDidasPPage\justify {
  Pircaride sort du char.
}
\livretScene SCÈNE II
\livretDescAtt\wordwrap-center { Pircaride, Isménide. }
\livretPers Pircaride
\livretRef#'DBBrecit
%# Pour immoler une victime
%# Le désespoir me conduit dans ces lieux,
%# Tu me vois sous ta propre image;
%# Mais c'est pour mieux servir ma rage.
\livretPers Isménide
%#- Qu'entends-je?
\livretPers Pircaride
%#= À mes transports jaloux
%# Reconnais ta rivale.
%# Pour adoucir ma peine sans égale,
%# C'est sur toi que je vais faire tomber mes coups.
\livretPers Isménide
%# Barbare, achève ta vengeance,
%# Hâte-toi de frapper mon cœur;
%# Ne respecte dans ta fureur,
%# Ni mes pleurs, ni mon innocence.
%# Unique objet de mes désirs
%# Cher Idas, toi pour qui j'aurais aimé la vie,
%# Reçois avec mon sang, lorsqu'elle m'est ravie,
%# Mes adieux, mon amour, & mes derniers soupirs.
\livretPersDidas Pircaride à part.
%#- Elle aime un autre amant!
\livretDidasPPage à Isménide.
%#= Parle, explique tes larmes.
\livretPers Isménide
%# Je touchais au sort le plus doux,
%# Un tendre amant devenait mon époux,
%# Lorsqu'un barbare en vint troubler les charmes;
%# Il m'enlève, malgré l'effort de mon amant:
%# Votre haine à ce prix, est-elle légitime?
\livretPers Pircaride
%#- Non, je ne te hais plus.
\livretPers Isménide
%#= Terminez mon tourment,
%# Que la même fureur contre moi vous anime.
\livretPers Pircaride
%# Impitoy=able amour, n'exige rien de moi,
%# Si pour me faire aimer il faut commettre un crime;
%# Et ne serais-je pas moi-même la victime
%# D'un ingrat que je veux ramener sous ma loi!
%# C'en est fait, la pitié tri=omphe de la haine,
%# Moi-même à vos malheurs je donne des soupirs;
%# C'est trop vous paraître inhumaine,
%# Je vais servir mes feux, en servant vos désirs.
\livretPers Isménide
%# Par quel charme ai-je pu calmer votre colère?
\livretPers Pircaride
%# Ne craignez rien, je vais vous rendre à votre amant,
%# Et s'il se peut, par mon déguisement,
%# Tromper toujours l'ingrat qui sait me plaire.
\livretRef#'DBCair
%# Vous qui m'obé=issez, paraissez à mes yeux,
%# Venez signaler ma puissance;
%# Ramenez cet objet dans les aimables lieux,
%# Où l'amour doit bientôt couronner sa constance;
%# Partez, volez, servez ses désirs amoureux.
\livretDidasPPage\justify {
  Isménide est enlevée par des génies.
}

\livretScene SCÈNE III
\livretDescAtt\wordwrap-center { Pircaride, sous les traits d’Isménide. }
\livretPers Pircaride
\livretRef#'DCAair
%# Elle part, & mon cœur n'est point exempt d'alarmes!
%# C'est sous ses traits qu'amour vient flatter mon ardeur;
%# Quelle honte! mes yeux, pour toucher mon vainqueur
%# Vous avez besoin d'autres charmes!
%# C'est en vain que l'amour veut rassurer mon cœur,
%# Je ne saurais calmer l'ennui qui me dévore;
%# Je vais m'offrir aux yeux de l'amant que j'adore,
%# J'entendrai des soupirs pour un autre que moi!
%# Il m'exprimera sa tendresse,
%# Tandis qu'il me manque de foi;
%# Ô dieux! il vient, cachons ma honte & ma faiblesse.

\livretScene SCÈNE IV
\livretDescAtt\wordwrap-center { Numapire, Pircaride, sous les traits d’Isménide. }
\livretPers Numapire
\livretRef#'DDArecit
%# Je sens, en vous voy=ant accroître mon ardeur,
%# Mille feux dévorent mon âme;
%# Vous avez par vos yeux allumé plus de flamme
%# Que n'en saurait allumer ma fureur.
%# Eh bien, cru=elle que vous êtes,
%# N'aurez-vous point pitié des maux que vous me faites?
\livretPers Pircaride
%# Non, rien n'égale ceux que tu me fais souffrir:
%# Sous ce fatal amour tu sais cacher ta haine,
%# Hélas! si tu m'aimais, tu finirais ma peine,
%# Mais tu veux me laisser mourir.
\livretPers Numapire
%# Dieux! pouvez-vous me faire un si sanglant outrage!
%# Douter de mon amour, lorsque je meurs pour vous;
%# Qui pourrait me porter de plus sensibles coups?
%# Mes soupirs, mes transports, ma langueur & ma rage,
%# Si vous ne les croy=ez, quel témoin croirez-vous?
\livretPers Pircaride
%# Aime un cœur qui t'adore, & fuis une inhumaine,
%# Fais ton bonheur d'être constant;
%# Dois-je compter sur un amant
%# Qui brise une si belle chaîne.
\livretPers Numapire
%# Non, je ne l'aimerai jamais,
%# Tout vous en donne l'assurance;
%# Pour être sur de ma constance,
%# Il fallait avoir vos attrait.
\livretPers Pircaride
%# D'une amante outragée =évitez la vangeance.
\livretPers Numapire
%# Pour défendre vos jours j'aurai plus de puissance;
%# Je vous aime, Isménide, autant que je la hais.
\livretPersDidas Pircaride à part.
%# Le perfide! aimez-moi s'il se peut davantage,
%# Pour partager les maux de mon triste esclavage.
%#- Hélas!
\livretPers Numapire
%#= Vous soupirez, vos yeux versent des pleurs,
%# Ah! si pour moi, l'amour faisait couler ces larmes!
\livretPers Pircaride
%# C'est lui qui cause mes alarmes.
%# Je n'ai pu resister à ses attraits vainqueurs,
%# Il tri=omphe, & toujours sous de feintes rigueurs,
%# J'ai voulu cacher ma tendresse,
%# C'est assez déguiser… c'est pour vous qu'il me blesse…
\livretPers Numapire
%# Que mon sort est heureux!
%# Je suis au comble de mes vœux.
% \livretPers Ensemble
% %# Amour, si j'éprouvais des peines,
% %# Je goûte plus les biens que je ressens;
% %# Qu'il est doux de porter tes chaînes,
% %# Lorsque tes plaisirs sont charmants.
\livretPers Numapire
%# Vous, que ma voix appelle,
%# Venez, par vos transports me marquer votre zèle,
%# De ces climats brûlants où s'étend mon pouvoir,
%# Accourez, venez tous célébrer votre reine;
%# Que vos yeux enchantés du plaisir de la voir,
%# Applaudissent au choix que je fais de sa chaîne.

\livretScene SCÈNE V
\livretDescAtt\wordwrap-center {
  Numapire, Pircaride, troupe de salamandres sous la forme
  de divers peuples d’Afrique.
}
\livretPers Chœur
\livretRef#'DEBchoeur
%# Chantons, célébrons notre reine,
%# Portons nos voix jusques aux cieux,
%# Le bonheur d'un amant qui peut porter sa chaîne,
%# Égale le bonheur des dieux.
\livretPersDidas Une Africaine à Pircaride.
\livretRef#'DEFair
%# L'amour a besoin de vos charmes
%# Pour se rendre victori=eux,
%# Il tri=omphe plus par vos yeux
%# Qu'il ne tri=omphe par ses armes.
%# Lorsque vous soumettez un cœur
%# L'amour est fier de sa victoire,
%# Il ne compte pour rien sa gloire,
%# Quand lui seul en est le vainqueur.
%#8 L'amour a besoin, &c.
\livretRef#'DEGair
\livretDidasPPage On danse.
% \livretPers Une Africaine
% %# Ah! quel heureux jour
% %# L'amour nous présage;
% %# Dans notre séjour
% %# Cherchons son esclavage:
% %# Ses vives ardeurs
% %# Ont mille douceurs,
% %# À ses traits vainqueurs
% %# Présentons nos cœurs.
% %# Hâtons-nous d'aimer,
% %# Qui sait nous charmer
% %# Peut-on être *heureux
% %# Sans former de doux nœuds.
% %# Aimons, chantons, ri=ons toujours
% %# C'est dans nos jeux que règnent les amours.
% \livretDidasPPage On danse.
\livretPersDidas Pircaride à sa suite
\livretRef#'DEJrecit
%# Finissez ces concerts, votre *hommage m'offense.
\livretPers Numapire
%#- Qu'entends-je, ô ciel!
\livretPers Pircaride
%#= Reconnais-moi:
%# En éloignant l'objet dont tu suivais la loi,
%# Sous ses traits empruntés j'ai rempli ma vengeance.
\livretPers Numapire
%#- Isménide, grands dieux!
\livretPers Pircaride
%#= Tu ne la verras plus.
%# Auprès de ton rival qu'elle aime,
%# Elle goûte un bonheur extrême,
%# Et laisse à ton amour des regrets superflus.
\livretPers Numapire
%# Suivons la fureur qui me guide,
%# Allons punir & l'amante & l'amant;
%# Ah! que ne puis-je aussi, perfide,
%# T'immoler à ma rage en cet affreux moment.
\livretPersDidas Pircaride sur un char de feu
%# Ici je brave ta vengeance,
%# Mon pouvoir égale le tien;
%# Je vais de ces amants serrer le doux li=en,
%# Et c'est moi qui prend leur défense.
\livretPers Numapire
%# La perfide tri=omphe, & malgré moi je sens
%# Les amoureux transports de la plus vive flamme;
%# Elle protège ces amants!
%# Où suis-je? quelle *horreur s'empare de mon âme!
%# Je ne puis me venger, que je suis malheureux!
%# Du moins, si je ne puis exercer ma vangeance,
%# Détruisons ce palais, témoin de mon offense;
%# Que ne puis-je périr pour éteindre mes feux.
\livretDidasPPage À sa suite.
\livretRef#'DEKair
%# Servez les transports de ma rage,
%# Ravagez ce séjour, qu'il perde ses attraits;
%# Que le feu dévorant le consume à jamais,
%# Et qu'il n'offre aux regards qu'une effray=ante image.
\livretPers Chœur
%# Servons les transports de sa rage,
%# Ravageons se séjour, qu'il perde ses attraits;
%# Que le feu dévorant le consume à jamais,
%# Et qu'il n'offre aux regards qu'une effray=ante image.
\livretDidasPPage\justify {
  Le palais est détruit par le feu.
}
\livretFinAct FIN DE LA TROISIÈME ENTRÉE
\sep
