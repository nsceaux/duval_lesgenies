\livretAct QUATRIÈME ENTRÉE
\livretScene\wordwrap { Les Sylphes, ou l’amour léger. }
\livretDescAtt\justify {
  Le Théatre représente un lieu préparé pour y donner une fête
  galante.
}
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\wordwrap-center { Un Sylphe. }
\livretPers Un Sylphe
\livretRef#'EABrecit
%# Le ciel a fixé mon empire
%# Entre les cieux & les mers,
%# Je règne en souverain dans l'espace des airs,
%# Mais l'unique bien où j'aspire
%# C'est de charmer l'objet dont je porte les fers.
%# Ces lieux sont ornés pour lui plaire
%# Amour, seconde mes désirs;
%# Si cet objet charmant demande un cœur sincère,
%# Fixe mes vœux, fais durer mes plaisirs.

\livretScene SCÈNE II
\livretDescAtt\wordwrap-center { Un Sylphe, une Sylphide. }
\livretPers Le Sylphe
\livretRef#'EBArecit
%# Je sens que mon amour aurait été fidèle,
%# Si le votre eut été constant.
\livretPers La Sylphide
%# Sans le plaisir d'une flamme nouvelle,
%# J'aimerais encor mon amant.
\livretPers Ensemble
%# Lance tes traits, remporte la victoire,
%# Amour, tri=omphe de mon cœur;
%# Non, tu n'as jamais tant de gloire
%# Que dans une inconstante ardeur.
\livretPers La Sylphide
%# Je vois ma nouvelle conquête.
\livretPers Le Sylphe
%# La mienne doit se rendre au milieu de la fête.
\livretPers Ensemble
%# Allons préparer des jeux
%# Dignes de nos soins amoureux.

\livretScene SCÈNE III
\livretDescAtt\wordwrap-center { Florise, la Sylphide. }
\livretPers Ensemble
\livretRef#'EDArecit
%# Formons une chaîne si belle
%# Au milieu des ris & des jeux:
%# Vole amour, viens nous rendre *heureux,
%# C'est la constance qui t'appelle.

\livretScene SCÈNE IV
\livretDescAtt\wordwrap-center {
  Le Sylphe, la Sylphide, Florise, troupes de Sylphes & de Sylphides,
  sous divers déguisements.
}
\livretPers La Sylphide
\livretRef#'EELair
%# Tri=omphe, fais voler tes traits,
%# Tendre amour, règne dans nos fêtes;
%# Fais ta gloire de nos défaites,
%# Mais laisse-nous aimer en paix.
\livretPers Chœur
\livretRef#'EEBchoeur
%# Chantons, ne songeons qu'aux plaisirs,
%# Profitons de l'âge des Grâces:
%# Pour mieux répondre à nos désirs,
%# Les amours volent sur nos traces.
\livretFinAct FIN
