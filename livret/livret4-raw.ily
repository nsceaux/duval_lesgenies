\livretAct QUATRIÈME ENTRÉE
\livretScene\wordwrap { Les Sylphes, ou l’amour léger. }
\livretDescAtt\justify {
  Le Théatre représente un lieu préparé pour y donner une fête
  galante.
}
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\wordwrap-center { Un Sylphe. }
\livretPers Un Sylphe
\livretRef#'EABrecit
%# Le ciel a fixé mon empire
%# Entre les cieux & les mers,
%# Je règne en souverain dans l'espace des airs,
%# Mais l'unique bien où j'aspire
%# C'est de charmer l'objet dont je porte les fers.
%# Ces lieux sont ornés pour lui plaire
%# Amour, seconde mes désirs;
%# Si cet objet charmant demande un cœur sincère,
%# Fixe mes vœux, fais durer mes plaisirs.

\livretScene SCÈNE II
\livretDescAtt\wordwrap-center { Un Sylphe, une Sylphide. }
\livretPers Le Sylphe
\livretRef#'EBArecit
%# Ne dissimulez point, votre cœur est volage,
%# Vous ne vivez plus sous ma loi.
\livretPers La Sylphide
%# Lorsque vous me manquez de foi,
%# Vous offenseriez-vous quand mon cœur se dégage?
\livretPers Le Sylphe
%# Non, je ne croy=ais pas que dans le même jour
%# Qu'un aimable nœud nous engage,
%# Qu'en m'apprenant à connaître l'amour,
%# Vous m'apprendriez à devenir volage.
\livretPers La Sylphide
%# Vous devez rendre grâce à ma légèreté,
%# Est-il un plus grand avantage?
%# Des douceurs de l'amour vous savez faire usage
%# En conservant la liberté.
\livretPers Le Sylphe
%# L'amour brille de moins de charmes,
%# Vous savez toucher tous les cœur;
%# Sous vos lois il n'est point d'alarmes,
%# On ne goûte que des douceurs.
%# Vous désarmez le plus rebèle,
%# Il est contraint à s'enflammer,
%# Si vous n'étiez point infidèle
%# On voudrait toujours vous aimer.
\livretPers La Sylphide
%# Un amant tel que vous enchante,
%# Vous aimez sans être jaloux:
%# Vous n'exigez point d'une amante,
%# De ne soupirer que pour vous.
%# Vous êtes dans votre tendresse
%# Complaisant, sincère & discret;
%# Si mon cœur a de la faiblesse,
%# Vous savez garder le secret.
\livretPers Le Sylphe
%# Je sens que mon amour aurait été fidèle,
%# Si le votre eut été constant.
\livretPers La Sylphide
%# Sans le plaisir d'une flamme nouvelle,
%# J'aimerais encor mon amant.
\livretPers Ensemble
%# Lance tes traits, remporte la victoire,
%# Amour, tri=omphe de mon cœur;
%# Non, tu n'as jamais tant de gloire
%# Que dans une inconstante ardeur.
\livretPers La Sylphide
%# Je vois ma nouvelle conquête.
\livretPers Le Sylphe
%# La mienne doit se rendre au milieu de la fête.
\livretPers Ensemble
%# Allons préparer des jeux
%# Dignes de nos soins amoureux.

\livretScene SCÈNE III
\livretDescAtt\wordwrap-center {
  Florise déguisée en cavalier, un masque à la main.
}
\livretPers Florise
\livretRef#'ECAair
%# C'est ici que l'amour va m'offrir des hommages,
%# Qui vont faire briller le pouvoir de ses traits;
%# Sous ce déguisement redoublent mes attraits,
%# Je vais tromper des cœurs volages.
%# Amour, sous tes aimables lois,
%# Tu soumets à jamais mon âme;
%# Permets que pour ta gloire & l'*honneur de mon choix,
%# Je puisse feindre une amoureuse flamme.

\livretScene SCÈNE IV
\livretDescAtt\wordwrap-center { Florise, la Sylphide. }
\livretPers Florise
\livretRef#'EDArecit
%# Belle Nymphe, à l'éclat dont brillent vos beaux yeux,
%# Que de cœurs vont rendre les armes!
%# Non, non, du dieux d'amour les traits victori=eux,
%# Sont moins à craindre que vos charmes.
\livretPers La Sylphide
%# D'une foule d'amants qui vole sur mes pas
%# Je ne crains point le langage;
%# Il est un amant dont l'*hommage
%# Aurait pour moi des appas.
\livretPers Florise
%# Et quel est cet amant? ah! que je porte envie
%# Au sort dont vous flatter son cœur;
%# Le plus doux instant de ma vie,
%# Serait marqué par ce bonheur.
\livretPers La Sylphide
%# La langueur des amants sans cesse me fait rire:
%# Ils m'adressent leurs vœux, je folâtre toujours;
%# Quand je suis près de vous, je sens que je soupire,
%# Que me demandent les amours?
\livretPers Florise
%# Ah! c'en est trop Nymphe charmante,
%# Un aveu si flatteur paie =assez mes soupirs.
\livretPers La Sylphide
%# Que notre tendresse s'augmente
%# Par l'espoir de mille plaisirs.
\livretPers Ensemble
%# Formons une chaîne si belle
%# Au milieu des ris & des jeux:
%# Vole amour, viens nous rendre *heureux,
%# C'est la constance qui t'appelle.

\livretScene SCÈNE V
\livretDescAtt\wordwrap-center {
  Le Sylphe, la Sylphide, Florise, troupes de Sylphes & de Sylphides,
  sous divers déguisements.
}
\livretPers Chœur
\livretRef#'EEBchoeur
%# Chantons, ne songeons qu'aux plaisirs,
%# Profitons de l'âge des grâces,
%# Pour mieux répondre à nos désirs,
%# Les amours volent sur nos traces.
\livretRef#'EECcotillon
\livretDidasPPage On danse.
\livretPersDidas Le Sylphe à sa suite.
\livretRef#'EEErecit
%# Ce lieu va recevoir la beauté qui m'engage,
%# Vous, qui sous d'aimables déguisements
%# Venez lui rendre votre *hommage,
%# Formez des jeux & des concerts charmants.
\livretRef#'EEFair
%# Que de son nom ce séjour retentisse,
%# Applaudissez à mon ardeur;
%# Qu'à mes transports votre zèle s'unisse,
%# Ne songeons qu'à toucher son cœur.
% \livretPers Un masque
% %# Un dolce canto di vaga belta.
% %# Puel dar si vanto din cantar la liberta,
% %# Ei rende immorta la dea vagante,
% %# El crin volante porger le fa.
\livretRef#'EEHsarabande
\livretDidasPPage On danse.
\livretPers Le Sylphe
\livretRef#'EEKrecit
%# Vous ne paraissez point cher objet que j'adore,
%# Quelque rival jaloux retiendrait-il vos pas?
%# Sans vous, ce beau séjour est pour moi sans appas.
%# Venez calmer le feu qui me dévore.
\livretPersDidas Florise masquée.
%# Et quelle est la beauté qui cause vos soupirs?
\livretPers Le Sylphe
%# Je l'ai vue =un moment, moment trop redoutable
%# Pour la perte d'un cœur qu'amusaient les plaisirs!
%# Sans fixer mon amour, les plus tendres désir
%# Semblaient me rendre *heureux près d'un objet aimable.
%# Mais, hélas! depuis cet instant
%# Les soins m'accompagnent sans cesse,
%# Et j'éprouve dans ma tendresse,
%# Que mon plaisir est mon tourment.
%# Florise cause mon martyre.
\livretPers Florise
%# Je la connais. Cette jeune beauté
%# N'aime pas un cœur qui soupire;
%# L'amant qui folâtre l'attire,
%# Et l'amant qui se plaint est toujours rebuté.
\livretPers Le Sylphe
%# Je sais accommoder ma chaîne
%# Aux caprices d'un cœur dont je suis enchanté;
%# Et pour vaincre sa cru=auté,
%# Je ne compte pour rien la peine.
\livretPers Florise
%# Elle aime un cœur constant;
%# Quelque fois un volage
%# Pour le plaisir du changement:
%# Pour vous faire à son badinage,
%# Êtes-vous l'un & l'autre amant?
\livretPers Le Sylphe
%# L'inconstance est mon partage,
%# Je ne suis constant qu'à regret;
%# Mais pour charmez un bel objet,
%# La constance est mon tendre *hommage.
\livretPers Florise
%# Vous êtes ce qu'il faut pour plaire à ses beaux yeux,
%# Mais de son cœur elle n'est plus maîtresse;
%# Et son amant est dans ces lieux.
\livretPers Le Sylphe
%# Ah! de quel coup mortel frappez-vous ma tendresse!
\livretPersDidas Florise, se démasquant, & parlant à un masque du bal.
%# Dorante approchez-vous, digne objet de mes vœux,
%# Florise veut vous rendre *heureux.
\livretPers Le Sylphe et la Sylphide
%#- Ô ciel!
\livretPers Florise
%#= Je vous ai trompé l'un & l'autre,
%# Mais c'est pour mieux serrer vos nœuds;
%# Aimez, que votre amour puisse imiter le nôtre,
%# Jamais rien n'éteindra vos feux.
\livretPers Le Sylphe et la Sylphide
%# Suivons cet exemple sans peine,
%# Aimons pour ne jamais changer;
%# Le plaisir de se dégager
%# Ne vaut pas le plaisir de reprendre sa chaîne.
\livretPers Florise
\livretRef#'EELair
%# Tri=omphe, fais voler tes traits,
%# Tendre amour, règne dans nos fêtes;
%# Fais ta gloire de nos défaites,
%# Mais laisse-nous aimer en paix.
\livretDidasPPage On danse.
\livretPers Chœur
%# Chantons, ne songeons qu'aux plaisirs,
%# Profitons de l'âge des Grâces:
%# Pour mieux répondre à nos désirs,
%# Les amours volent sur nos traces.
\livretFinAct FIN
