\clef "basse" sol,2 r |
r4 r16 sol la si do'4.( si16 la) |
sol4.*5/6 re16 mi fad sol4 fad8. mi16 |
re4.( do8) si,8. la,16 sol,8. fad,16 |
mi,2 la, |
re,4 r16 la si dod' re'4 re |
sol4. la8 si4 sol |
la4. sol8 fad8. mi16 re8. dod16 |
re4~ re16 re mi fad sol4. sol8 |
fad8. mi16 fad8. sol16 la4 la, |
re4. do8 si,8. do16 si,8. la,16 |
sol,4 r16 sol la si do'4.( si16 la) |
re8 r r |
\clef "alto" r re' re' |
sol sol'[ sol'] |
do' do' la |
si la16 sol fad la |
\clef "bass" sol8 red si, |
mi fad sol |
la si do' |
si si si |
mi mi,16 fad, sol,8 |
la, si,4 |
mi8 r r |
r mi' mi' |
la16 sol fad mi fad re |
la8 la la, |
re re' re' |
sol sol, sol |
la la, la |
si si, si |
do' do do' |
si sol fa |
mi fa re |
mi8. re16 do si, |
la,8 la16 si do'8 |
re' mi' mi |
la16 sol la si dod' la |
re' mi' re' do' si la |
si4 sol8 |
do'4 la8 |
re' re16[ mi fad re] |
sol4 r8 |
r8 si16 la sol8 |
la16 si la sol fad8 |
sol16 la sol fad mi re |
do8 re re, |
sol, sol sol |
do16 si, do re mi do |
re4. |
mi4 si,8 |
do re4 |
sol,4.~ |
sol,8 r r |
sol,4. |
