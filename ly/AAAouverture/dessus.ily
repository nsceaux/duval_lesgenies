\clef "dessus" r4 r16 re'' mi'' fad'' sol''4. sol''8 |
sol''4.*5/6 fad''16 mi'' re'' mi''4.*5/6 mi''16 re'' do'' |
si'4.\trill si'8 si'4.*5/6 si'16 la' sol' |
re''4~ re''16 re' re' re' re'4~ re'16 re'' mi'' fad'' |
sol''4. sol''8 sol''4.*5/6 sol''16 fad'' mi'' |
fad''4.*5/6 la''16 la'' la'' la''4.*5/6 la''16 sol'' fad'' |
si''8. la''16 sol''8. fad''16 sol''8. fad''16 mi''8. re''16 |
dod''4\trill~ dod''8 la'64*8/7 si' dod'' re'' mi'' fad'' sol'' la''4. la''8 |
la''4~ la''16 fad'' sol'' la'' si''4~ si''16 sol'' fad'' mi'' |
la''8. dod''16 re''8. mi''16 mi''4.\trill re''8 |
re''4~ re''16 re'' mi'' fad'' sol''4. sol''8 |
sol''4.*5/6 fad''16 mi'' re'' mi''4.*5/6 mi''16 re'' do'' |
re''8 re''[ re''] |
la'8 la'16 si' do''8 |
si'8 re''[ sol''] |
mi''16 red'' mi'' sol'' fad'' mi'' |
red''8\trill fad'' fad'' |
si'' fad''16 sol'' la''8 |
sol'' red'' mi'' |
do''' si'' la'' |
si''8. la''16 sol'' fad'' |
sol''8. la''16 si''8 |
la''16[\trill sol''] fad''8.[ mi''16] |
mi''8 mi'' mi'' |
si' si'16 dod'' re''8 |
dod'' la'' la'' |
mi'' mi''16 fad'' sol''8 |
fad''16 re'' mi'' fad'' sol'' la'' |
si'' mi'' fad'' sol'' la'' si'' |
do''' fad'' sol'' la'' si'' do''' |
re'''8 r r |
r re'' re'' |
sol' sol'' sol'' |
do''' si''16 do''' si'' la'' |
sold''\trill mi'' fad'' sold'' la'' si'' |
do'''4. |
si''8 si''8.\trill la''16 |
la''8 mi'' la'' |
fad''8\trill fad''16 sol'' la''8 |
re'' sol''16 la'' si''8 |
mi'' la''8. si''16 |
fad''4\trill re''8 |
r re'' re'' |
sol'8 sol''16 la'' si''8 |
do''8 fad''16 sol'' la''8 |
si'16 re''[ mi'' fad'' sol''8] |
la'8 sol'' fad'' |
sol''16 fad'' mi'' re'' do'' si' |
mi''4. |
la'8 la'8.\trill sol'16 |
sol' re'' mi'' fad'' sol''8 |
la' sol'' fad'' |
sol''4.~ sol''8 re'' re'' |
sol''4. |
