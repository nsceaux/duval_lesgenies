\key sol \major
\digitTime\time 2/2 \midiTempo#120 s1*2 \bar ".!:"
s1*8 \alternatives s1*2 { \time 3/8 s4. }
\bar ".!:" s4.*39 \alternatives s4.*2 s4. \bar "|."
