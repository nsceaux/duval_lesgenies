\clef "haute-contre" sol'4.*5/6 fad'16 sol' la' si'4. si'8 |
si'4.\trill re''8 do''4. re'8 |
re'4. re'8 re'4~ re'8. mi'16 |
fad'4~ fad'16 fad' fad' fad' sol'8. la'16 si'4~ |
si'4. si'8 la'4. la'8 |
la'2 re''4 do''! |
si'8. do''16 si'8. la'16 sol'8. la'16 sol'8. fad'16 |
mi'4.\trill mi'8 re'8. sol'16 fad'8.\trill mi'16 |
re'4. re'8 re'4~ re'16 dod' re' mi' |
fad'4 la' la' sol' |
fad'4.\trill re''8 re''8. mi''16 re''8. do''16 |
si'4.\trill re''8 do''4. re'8 |
si'8\trill r r |
r re' re' |
sol si' si' |
si' la' do'' |
fad' red'' red'' |
mi'' la'16 si' do'' la' |
si'4 si'8 |
la' fad' fad'~ |
fad'16 fad' sol' fad' mi' red' |
mi'8 si'16 la' sol'8 |
fad' si' si' |
sold'\trill r r |
r sold'16 la' si'8 |
la' re'' re'' |
re'' dod''16 re'' mi''8 |
re''4 do''!8 |
si' re' sol'~ |
sol'4 fad'8 |
fad'4 re'8 |
re' la' la' |
sol' si' si' |
do'' re''16 mi'' re'' do'' |
si'8.\trill mi'16 fad' sold' |
la'4 la'8~ |
la' la' sold' |
la'4 la'8 |
la'4 re'8 |
re'8 re'' re''~ |
re'' do'' do'' |
la'4\trill re'8 |
re'4 r8 |
r si' si'~ |
si'4 la'8~ |
la'4 sol'16 fad' |
mi'8 re' re' |
re'4. |
la'16 sol' la' si' do'' la' |
re'8 sol' fad' |
sol'4 sol'8~ |
sol' si' la' |
sol'4.~ |
sol'8 r r |
sol'4. |
