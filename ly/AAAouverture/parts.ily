\piecePartSpecs
#`((dessus #:instrument ,#{\markup\center-column { [Violons et Hautbois] }#})
   (flute-hautbois #:instrument "[Hautbois]")
   (basse #:instrument ,#{ \markup\center-column { [Basses, Bassons et B.C.] } #})
   (parties)
   (dessus2-haute-contre)
   (taille))
