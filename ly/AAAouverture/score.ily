\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { [Violons et Hautbois] }
    } << \global \includeNotes "dessus" >>
    \ifFull <<
      \new Staff \with {
        instrumentName = \markup\center-column { [Hautes-contre] }
      } << \global \includeNotes "haute-contre" >>
      \new Staff \with {
        instrumentName = "[Tailles]"
      } << \global \includeNotes "taille" >>
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses, Bassons et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*6\break s1*6\pageBreak
        s4.*11\break s4.*10\break s4.*10\pageBreak
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
