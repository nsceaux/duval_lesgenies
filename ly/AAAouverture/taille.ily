\clef "taille" si2 r |
r4 r8 sol' sol'4. sol'8 |
sol'4 re'2~ re'16 la si dod' |
re'4 la si re'~ |
re'4. re'8 re'4 dod'\trill |
re'4.*5/6 dod'16 re' mi' fad'4. re'8 |
re'4 re'2 re'4 |
la4 la~ la8. dod'16 re'8. mi'16 |
fad'8 mi' re'2~ re'16 mi' re' dod' |
re'8. mi'16 re'2 dod'4\trill |
re'4 la' sol' re' |
re'4. sol'8 sol'4. sol'8 |
re'8 r r |
R4. |
r8 sol' sol' |
do' do' la |
si8 la16 sol fad la |
si8 si fad' |
sol' fad' mi' |
mi' red' mi' |
red'4\trill si8 |
si4 mi'8 |
fad'16[ mi'] red'8.\trill mi'16 |
mi'8 r r |
r mi' mi' |
mi' la' la' |
la' la la |
la8. re'16 mi' fad' |
sol'4. |
mi'4 la'8~ |
la' la'16 si' la' sol' |
fad' mi' re' mi' fad' re' |
re'4 sol'8 |
sol'4 la'8 |
mi'4. |
mi'8 do'16 re' mi'8 |
fa' mi' re' |
dod'16 si dod' re' mi' dod' |
re'8 re'16 mi' fad'8 |
sol'8 si'16 la' sol'8~ |
sol' mi' mi' |
re'\noBeam fad'16 mi' re' do' |
si4\trill r8 |
r re' re' |
do' la16 si do'8 |
si4 sol'8~ |
sol' si' la' |
si'16 la' sol' fad' mi' re' |
sol4 sol8 |
sol4 do'8 |
si4 re'8 |
mi' re'8. re'16 |
si4.\trill~ |
si8 r r |
si4.\trill |
