\clef "basse" do1~ |
do4 fa,8 sol, lab,2 |
sol,4 sol fa |
mib2.~ |
mib do8 lab, |
sib,2. |
mib,8 mib mi do fa[ re] |
sol8 fa mib2 |
lab8 fa sol do'16 fa sol8 sol, |
do2. |
