\key sol \minor
\time 2/2 \midiTempo#80 s1*2
\digitTime\time 3/4 \midiTempo#80 \midiTempo#160 \grace s8 s2.*2
\digitTime\time 2/2 s1
\digitTime\time 3/4 \midiTempo#80 s2.*5 \bar "|."
