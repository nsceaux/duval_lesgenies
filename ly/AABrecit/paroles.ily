Il est temps que mon art ins -- trui -- se les mor -- tels,
dans les se -- crets des dieux le pre -- mier j’ai su li -- re ;
mé -- ri -- tons comme eux des au -- tels,
et mon -- trons mon pou -- voir à tout ce qui res -- pi -- re.
