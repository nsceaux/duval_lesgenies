\piecePartSpecs
#`((basse #:instrument "B.C."
          #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{

\markup\fontsize#-2 \column {
  \livretPers Zoroastre
  \livretVerse#12 { Il est tems que mon Art instruise les Mortels, }
  \livretVerse#12 { Dans les secrets des Dieux le premier j’ay sçu lire : }
  \livretVerse#8 { Méritons comme eux des autels, }
  \livretVerse#12 { Et montrons mon pouvoir à tout ce qui respire. }
}
       #}))
