\score {
  \new ChoirStaff <<
    \new Staff \with {
      instrumentName = \markup\character Zoroastre
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "B.C." } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s1*2 s2.*2\break s1 s2.*2\pageBreak }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
