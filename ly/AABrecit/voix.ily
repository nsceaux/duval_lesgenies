\clef "vbasse"
s2. r8 do'16 do' |
mib8 mib16 do lab8 sol do'8. re'16 si8 do' |
\appoggiatura do'8 re'2. |
mib4 mib8 fa sol lab |
sib4 do'8 re' mib'4 mib8 fa |
sol4( fa4. mib8) |
mib16 r sol sol la8. sib16 la8 si16 do' |
si8\trill sol16 sol do'4 re'8. mib'16 |
fa'8 re' si16\trill si do' fa sol4 |
do2. |
