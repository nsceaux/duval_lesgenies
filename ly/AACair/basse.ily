\clef "basse" R1*3 |
r2 r4 r16 do' do' do' |
do'4~ do'8.*5/6 do'32 sib lab sol4~ sol8.*5/6 lab32 sol fa |
mib4~ mib8.*1/6 fa32 mib fa sol fa mib re do4 re32 mib fa sol la sib dod' re' |
mib'2 si4.~ si32 sol sol sol |
sol4.~ sol32 lab sol fa mib4.~ mib32 sol fa mib |
re2 mib4 do |
fa sol lab8. sol16 fa8. lab16 |
sol4 fa8. mib16 re8. do16 si,8. la,16 |
sol,2 sol |
fad sol4 sol, |
do do' sib4. lab8 |
sol2 sol4 sol8 fa mib4. re8 |
do4 mib si, sol, |
do r8 do' do'4 mib' |
lab2 sol4 fa8 mib |
sib4 sib, re sib, |
fa8 fa fa sol lab4 lab8 sib do'4 do'8 re' |
mib'4 mib8 re do4. re8 mib4 do |
re2. sib,8 do re4 re, |
sol,2 r |
sol,2. sol4 si,4. do8 |
lab4 fa re fa sol do |
fa,2 mib,4 fa, sol,2 |
\custosNote do8
