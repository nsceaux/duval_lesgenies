\clef "dessus2" r4 r16 do''16 do'' do'' do''4.~ do''32 do'' sib' lab' |
sol'4.~ sol'32 lab' sol' fa' mib'4~ mib'32[ fa' mib' fa'] sol'[ fa' mib' re'] |
do'8*2[ re'32( mib' fa' sol' la' si' do'' re'')] mib''4~ mib''16 mib'' re'' do'' |
si'4. re''8 sol'4.~ sol'32 lab' sol' fa' |
mib'4~ mib'8.*5/6 mib'32 re' do' re'2 |
mib' \clef "dessus" r4 r16 sol'' sol'' sol'' |
sol''4.~ sol''32 sol'' fa'' mib'' re''4.~ re''32 mib'' re'' do'' |
si'4.~ si'32 do'' si' la' sol'4 la'32( si' do'' re'' mib'' fa'' sol'' la'') |
si''4 sol'' do''' \ficta sib'' |
lab'' sol'' fa''8. mib''16 re''8. do''16 |
si'8. sol'16 si'8. re''16 si'8. sol'16 si'8. re''16 |
si'8. sol'16 si'8. re''16 si'8. sol'16 si'8. sol'16 |
mib''4. re''8 re''4.\trill do''8 |
do''8 do'' mib'' do'' sol'' sol' sol' do'' |
si' sol' si' re'' sol' re' sol' re' mib' do' mib' fa' |
sol' sol' do'' sol' re'' sol' re'' sol'' |
mib'' do'' mib'' do'' sol'' mib'' sol'' mib'' |
lab'' lab' do'' lab' mib'' sib' sib' mib'' |
re'' sib' re'' sib' fa'' sib'' fa'' sol'' |
lab'' fa'' lab'' sib'' do''' lab' do'' re'' mib'' do'' mib'' fa'' |
sol'' sol' sib' sol' do'' re'' mib'' re'' do'' re'' do'' sib' |
la' re'' fad'' re'' la'' re'' re'' sol'' fad''4.\trill sol''8 |
sol''8 sol' sol' la' si' si' si' do'' |
re''4 sol' r8 re'' re'' mib'' fa'' sol'' fa'' mib'' |
re'' fa'' re'' fa'' lab'' sol'' fa'' mib'' re'' fa'' mib'' do'' |
si' re'' fa'' re'' mib'' sol'' fa'' lab'' re''4.\trill do''8 |
\custosNote do''8
