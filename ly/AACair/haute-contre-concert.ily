\clef "haute-contre" r4 r16 mib' mib' mib' mib'4.~ mib'32 lab' sol' fa' |
mib'4.~ mib'32 fa' mib' re' do'4~ do'32[ re' do' re'] mib'[ re' do' si] |
do'4.*13/12 sol'32( la' si') do''4~ do''16 do'' si' do'' |
re''4.~ re''32 lab' sol' fa' mib'4.~ mib'32 fa' mib' re' |
do'4~ do'8.*5/6 mib'32 re' do' si2\trill |
do'4~ do'8.*5/6 si32 do' re' mib'2 |
do''4.~ do''32 mib'' re'' do'' re''4.~ re''32 re' re' re' |
re'4.~ re'32 si si si do'4.~ do'32 re' mib' fa' |
sol'2. mib'4 |
re' mib' re'4. re'8 |
re'2. re'4 |
re' re' re' si |
do'2 do'4 si |
do'8 do'' mib'' do'' sol'' sol' sol' do'' |
si' sol' si' re'' sol' re' sol' re' mib' do' mib' fa' |
sol' sol' do'' sol' re'' sol' re'' sol'' |
mib'' do'' mib'' do'' sol'' mib'' sol'' mib'' |
lab'' lab' do'' lab' mib'' sib' sib' mib'' |
re'' sib' re'' sib' fa'' sib'' fa'' sol'' |
lab'' fa'' lab'' sib'' do''' lab' do'' re'' mib'' do'' mib'' fa'' |
sol'' sol' sib' sol' do'' re'' mib'' re'' do'' re'' do'' sib' |
la' re'' fad'' re'' la'' re'' re'' sol'' fad''4.\trill sol''8 |
sol''8 sol' sol' la' si' si' si' do'' |
re''4 sol' r8 re'' re'' mib'' fa'' sol'' fa'' mib'' |
re'' fa'' re'' fa'' lab'' sol'' fa'' mib'' re'' fa'' mib'' do'' |
si' re'' fa'' re'' mib'' sol'' fa'' lab'' re''4.\trill do''8 |
\custosNote mib'8
