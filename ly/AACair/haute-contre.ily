\clef "haute-contre" r4 r16 mib' mib' mib' mib'4.~ mib'32 lab' sol' fa' |
mib'4.~ mib'32 fa' mib' re' do'4~ do'32[ re' do' re'] mib'[ re' do' si] |
do'4.*13/12 sol'32( la' si') do''4~ do''16 do'' si' do'' |
re''4.~ re''32 lab' sol' fa' mib'4.~ mib'32 fa' mib' re' |
do'4~ do'8.*5/6 mib'32 re' do' si2\trill |
do'4~ do'8.*5/6 si32 do' re' mib'2 |
do''4.~ do''32 mib'' re'' do'' re''4.~ re''32 re' re' re' |
re'4.~ re'32 si si si do'4.~ do'32 re' mib' fa' |
sol'2. mib'4 |
re' mib' re'4. re'8 |
re'2. re'4 |
re' re' re' si |
do'2 do'4 si |
do' r r2 |
R1. R1*4 R1.*3 R1 R1.*3 |
