\piecePartSpecs
#`((dessus #:instrument "Violons")
   (dessus2-haute-contre)
   (taille)
   (parties #:score-template "score-parties-voix")
   (basse #:instrument ,#{\markup\center-column { [Basses, Bassons et B.C.] }#})

   (silence #:on-the-fly-markup , #{

\markup\fontsize#-2 \column {
  \livretPers Zoroastre
  \livretVerse#10 { Esprits soumis à mes commandemens, }
  \livretVerse#8 { Venez remplir mon esperance, }
  \livretVerse#10 { Rassemblez-vous des divers Elements, }
  \livretVerse#10 { Et signalez ma gloire & ma puissance. }
} #}))
