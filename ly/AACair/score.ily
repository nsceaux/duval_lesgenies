\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket \with { \haraKiri } <<
      \new Staff \with { instrumentName = "Violons" } <<
        \global \includeNotes "dessus"
      >>
      \ifFull <<
        \new Staff \with {
          instrumentName = \markup\center-column { [Hautes-contre] }
        } << \global \includeNotes "haute-contre" >>
        \new Staff \with {
          instrumentName = "[Tailles]"
        } << \global \includeNotes "taille">>
      >>
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses et B.C.] }
    } <<
      \global
      \includeNotes "basse"
      \unlessFull { <>_"Violons" \includeNotes "haute-contre" \clef "basse" }
      \includeFigures "chiffres"
      \origLayout {
        s1*4\break s1*4\pageBreak
        s1*5\break s1 s1. s1*2 s2 \bar "" \break s2 s1 s1.*2\pageBreak
        s1. s1 s1. s2 \bar "" \break s1 s1.
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
