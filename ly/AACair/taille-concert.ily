\clef "taille" do'1~ |
do'~ |
do' |
sol'4.~ sol'32 lab'32 sol' fa' mib'4.~ mib'32 fa' mib' re' |
do'2 sol |
sol4~ sol8.*5/6 sol32 la si do'2 |
sol' sol'4.~ sol'32 re' re' re' |
re'4.~ re'32 si si si do'4. do'8 |
re'4 si do'2 |
do' do'4. do'8 |
si!2. sol4 |
sol sol sol sol |
la2 sol4 fa' |
mib'8 do'' mib'' do'' sol'' sol' sol' do'' |
si' sol' si' re'' sol' re' sol' re' mib' do' mib' fa' |
sol' sol' do'' sol' re'' sol' re'' sol'' |
mib'' do'' mib'' do'' sol'' mib'' sol'' mib'' |
lab'' lab' do'' lab' mib'' sib' sib' mib'' |
re'' sib' re'' sib' fa'' sib'' fa'' sol'' |
lab'' fa'' lab'' sib'' do''' lab' do'' re'' mib'' do'' mib'' fa'' |
sol'' sol' sib' sol' do'' re'' mib'' re'' do'' re'' do'' sib' |
la' re'' fad'' re'' la'' re'' re'' sol'' fad''4.\trill sol''8 |
sol''8 sol' sol' la' si' si' si' do'' |
re''4 sol' r8 re'' re'' mib'' fa'' sol'' fa'' mib'' |
re'' fa'' re'' fa'' lab'' sol'' fa'' mib'' re'' fa'' mib'' do'' |
si' re'' fa'' re'' mib'' sol'' fa'' lab'' re''4.\trill do''8 |
\custosNote sol8
