\clef "taille" do'1~ |
do'~ |
do' |
sol'4.~ sol'32 lab'32 sol' fa' mib'4.~ mib'32 fa' mib' re' |
do'2 sol |
sol4~ sol8.*5/6 sol32 la si do'2 |
sol' sol'4.~ sol'32 re' re' re' |
re'4.~ re'32 si si si do'4. do'8 |
re'4 si do'2 |
do' do'4. do'8 |
si!2. sol4 |
sol sol sol sol |
la2 sol4 <<
  \ifComplet { fa | mib }
  \ifConcert { fa' | mib' }
>> r r2 |
R1. R1*4 R1.*3 R1 R1.*3 |
