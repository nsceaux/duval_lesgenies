\clef "vbasse" R1*13 |
r4 do' sib4. lab8 |
sol2 sol4 sol8 fa mib4. re8 |
do2 r |
r4 r8 do' do'4 mib' |
lab2 sol4 fa8 mib |
sib2 sib |
r8 fa fa sol lab4 lab8 sib do'4 do'8 re' |
mib'2 sol4 do'8 sib la4. do'8 |
fad2\trill fad4 sol8 do re2 |
sol, r |
r8 sol sol la si4 si8 do' re'4 re'8 mib' |
fa'2 fa4 sol8 lab si,4 do |
lab2 sol4 lab8 fa sol4( sol,) |
\custosNote do2*1/4
