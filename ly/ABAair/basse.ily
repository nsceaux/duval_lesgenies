\clef "basse" do4.\fortSug do8 sol,8. sol16 sol8. sol16 |
do8. do16 do8. re16 mi8. fa16 sol8. sol,16 |
do4 do' fa lab |
si, do8. sib,?16 lab,2 |
sol,8. do'16 do'8. do'16 do'8. sib16 sib8. sib16 |
sib8. lab16 lab8. lab16 sol4 sol, |
do1~ |
do8. do'16 sib8. lab16 sol8. fa16 mib8. re16 |
do4 si,8. do16 sol,2~ |
sol,8. sol16 fa8. mib16 re8. do16 si,8. la,16 |
sol,2 sol4 fa |
mib fa sol sol, |
do8 re mib fa sol sol, |
do4 sol,2 |
do4 do r8 do' |
re' sib sib, sib do' re' |
mib' sib mib sib, mib, mib |
fa mib fa fa sol lab |
sib fa sib, do re mib |
do lab, do lab, do lab, |
sib,2. |
mib8 sib, sol, sib, mib, mib |
sib la sib do' re' do'16 sib |
do'8 sib do' re' mib' do' |
re'4 re re |
re' re' do' |
sib8 re fad re sol sol, |
mib do mib do mib do |
re re, fad, la, re re, |
sol4 la sib |
do'2 sib8 la |
sib4 la4. sol8 |
re'4 re re |
re' re4. re8 |
sol8 lab sol fa mib re |
mib re do re mib do |
fa mib fa sol la fa |
sib fa re fa sib,4 |
sib sib4. sol8 |
do'2 la8 la |
re4 re'8 sib re' sib |
mib'4 mib mib' |
do' la do' |
fad8 re mi fad sol sib, |
do4.( re16 mib) re4 re, |
sol,2 r8 sol |
sol fa sol la si sol |
do' do mib sol mib do |
sol4 do'4. re'8 |
mib'2 re'8 do' |
si8 do' re' do' si do' |
sol re si, re sol, lab |
lab? fa re fa do mib |
fa mib fa sol lab fa |
sol re si, re sol, sol |
sol sol, sol4 fa |
mib sol sol |
do'8 do do'4 re' |
mib'2 mib'4 |
lab4 lab sol |
fa si do' |
si si do' |
fa2 fa,4 |
fa sol lab |
si, lab mib |
fa8 sol lab fa sol4 sol, |
\ifComplet do
\ifConcert {
  do2 do'4 |
  si4. la8 sol fa |
  mib4 do mib |
  fa2 mib4 |
  fa sol sol, |
  do2. |
}

