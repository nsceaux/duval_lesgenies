\clef "dessus2"
\tag #'dessus2 \startHaraKiri
do''8._\fort do''16 do''8. do''16 do''8. sib'16 sib'8. sib'16 |
sib'8. lab'16 lab'8. lab'16 sol'8. fa'16 mib'8. re'16 |
mib'8. sol''16 sol''8. sol''16 sol''8. fa''16 fa''8. fa''16 |
fa''4 mib''8. re''16 do''8. mib''16 re''8. do''16 |
si'8. mib''16 mib''8. mib''16 re''4 re'' |
mib''8. mib''16 fa''8. fa''16 fa''8. fa''16 mib''8. re''16 |
\origVersion\clef "dessus" mib''8. do'''16 sib''8. lab''16 sol''8. fa''16 mib''8. re''16 |
do''8. sol'16 sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
sol'8. sol''16 fa''8. mib''16 re''8. do''16 si'8. la'16 |
sol'8. re'16 re'8. re'16 re'8. re'16 re'8. re'16 |
re'8. sol'16 la'8. si'16 do''8. re''16 mib''8. fa''16 |
sol''4. fa''16 mib'' re''4.\trill do''8 |
do''4 r r |
r4 \twoVoices #'(dessus1 dessus2 dessus) <<
  { sol''4 fa'' |
    mib''8 re'' mib'' fa'' sol'' mib'' |
    fa'' mib'' fa'' re'' mib'' fa'' |
    sib' mib'' sol'' lab'' sib'' lab''16 sol'' |
    lab''8 fa'' lab'' sib'' do''' lab''16 sol'' |
    fa''4.\trill sib''8 lab'' sol'' |
    lab'' sol'' lab'' sib'' do''' lab'' |
    fa'' sol'' fa''4.\trill mib''8 |
    mib'' re'' mib'' sol'' fa'' mib'' |
    re''4 sib' sib'' |
    sib'' la''8 sib'' la'' sol'' |
    fad'' \ficta  mi'' fad'' sol'' la'' sol'' |
    fad'' \ficta  mi'' fad'' sol'' la'' fad'' |
    sol'' sib'' la'' do''' sib'' re''' |
    re'''4 do'''8 sib'' la'' sol'' |
    fad''8 la'' re''' do''' sib'' la'' |
    sib'' do''' sib'' la'' sol'' fa'' |
    mi''4 fad'' sol''8 la'' |
    re''4 do''4.\trill sib'8 |
    la'8 sol' fad' sol' la' sib' |
    do'' sib' la' sib' do'' la' |
    sib'4. re''8 sol'' si' |
    do''4. sib'8 la' sol' |
    la' do'' fa'' mib'' re'' do'' |
    re'' do'' sib' do'' re'' mib'' |
    fa'' fa'' sib'' fa'' sib'' re''' |
    sol''4. sol''8 do''' do''' |
    do'''4. do'''8 sib'' la'' |
    sol'' fa'' mib'' re'' do'' sib' |
    mib''2 mib''4 |
    mib''2 re''8 sol'' |
    sol''2 fad''4.\trill sol''8 |
    sol''4 r r |
    r si''8 do''' re''' si'' |
    sol'' fa'' mib'' sol'' fa'' mib'' |
    re''4 mib''4. fa''8 |
    sol''4 sol'' si'8 do'' |
    re'' mib'' fa'' mib'' re'' do'' |
    si'4\trill sol' r8 fa'' |
    fa'' sol'' lab'' fa'' sol''4 |
    sol'' fa''8 sol'' fa'' mib'' |
    re''4 sol'8 la' si' do'' |
    re'' do'' re'' mib'' fa'' re'' |
    mib'' sol'' re'' mib'' fa'' re'' |
    mib''4. mib''8 re'' do'' |
    sib'4 sol''8 lab'' sib'' sol'' |
    do'''8 mib'' fa''4 sol'' |
    lab'' lab'' sol'' |
    fa'' fa'' mib'' |
    re''8 mib'' fa'' mib'' re'' do'' |
    si'2.\trill |
    lab''4 re'' mib'' |
    mib''4. re''8 re''4.\trill do''8 |
    \ifComplet do''4 }
  { \stopHaraKiri do''4 si' |
    do''8 sol' do'' re'' mib'' do'' |
    sib'4 lab' sol'8 lab' |
    sol' sib' mib'' fa''16 sol'' mib''8 re'' |
    do'' do'' fa'' sol''16 lab'' fa''8 mib'' |
    re''4.\trill mib''8 fa'' sib' |
    do'' sib' do'' re'' mib'' fa'' |
    re'' mib'' re''4.\trill mib''8 |
    mib'' fa'' sol'' sib'' lab'' sol'' |
    fa''4 re''4. re''8 |
    re''4 mib''8 re'' do'' sib' |
    la' sol' la' sib' do'' sib' |
    la' sol' la' sib' do'' la' |
    sib' re'' do'' mib'' re'' sib'' |
    sib''4 la''8 sol'' do''' sib'' |
    la''4. fad''8 sol'' la'' |
    re'' mib'' re'' do'' sib' la' |
    sol'4 la' sib'8 do'' |
    sib'4 fad'4. sol'8 |
    fad'\trill mi' re' mi' fad' sol' |
    la' sol' fad' sol' la' fad' |
    sol'4. si'8 do'' re'' |
    sol'4 mib''8 fa'' sol''4 |
    do''4 la'8 sib' do''4 |
    fa'8 fa' sol' la' sib' do'' |
    re'' sib' re'' sib' re'' fa'' |
    mi'' do'' mi'' do'' mi'' la'' |
    fad''4. re''8 sol'' re'' |
    re''4 do''8 sib' la' sol' |
    do''2 do''4 |
    do''4. sib'16 la' sib'4 |
    sib'4. la'8 la'4.\trill sol'8 |
    sol'4 sol'8 la' si' do'' |
    re''4 sol''4. re''8 |
    mib'' re'' do'' mib'' re'' do'' |
    si'4 do''4. si'8 |
    do''4 sol'8 do'' re'' mib'' |
    fa''4. sol''8 fa'' mib'' |
    re''2\trill r8 re'' |
    re'' mib'' fa'' re'' mib''4 |
    mib'' re''8 mib'' re'' do'' |
    si' re' mib' fa' sol' la' |
    si' la' si' do'' re'' si' |
    do'' mib'' si' do'' re'' si' |
    do''4. do''8 sib' lab' |
    sol'4 mib''8 fa'' sol'' mib'' |
    lab'' do'' re''4 mi'' |
    fa'' fa'' mib'' |
    re''\trill re'' do'' |
    si'8 do'' re'' mib'' fa'' mib'' |
    re''2.\trill |
    re''4 si' do'' |
    do''4. si'8 si'4.\trill do''8 |
    \ifComplet do''4 }
>>
\ifConcert {
  do''8 si' do'' re'' mib'' fa'' |
  sol'' fa'' sol'' la'' si'' sol'' |
  do''' sib'' lab'' sol'' fa'' mib'' |
  lab''4. si'8 do''4 |
  re'' re''4.\trill do''8 |
  do''2. |
}

