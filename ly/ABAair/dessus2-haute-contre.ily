\clef "haute-contre" mib'8. mib'16 mib'8. mib'16 mib'8. re'16 re'8. re'16 |
re'8. do'16 do'8. do'16 sib8. la16 sol8. sol16 |
sol8. mib''16 mib''8. mib''16 mib''8. re''16 re''8. re''16 |
re''4 do'' sol' fad' |
sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
sol'8. do''16 do''8. re''16 re''8. re''16 do''8. si'16 |
do''8. mib''16 re''8. do''16 sib'8. lab'16 sol'8. fa'16 |
mib'8. mib'16 mib'8. mib'16 mib'8. mib'16 mib'8. mib'16 |
mib'8. mib'16 re'8. mib'16 fa'8. mib'16 re'8. do'16 |
\ru#4 { si8. si16 } |
si8. sol'16 la'8. si'16 do''2~ |
do'' do''4 si'\trill |
do''4 r r |
r do''4 si' |
do''8 sol' do'' re'' mib'' do'' |
sib'4 lab' sol'8 lab' |
sol' sib' mib'' fa''16 sol'' mib''8 re'' |
do'' do'' fa'' sol''16 lab'' fa''8 mib'' |
re''4.\trill mib''8 fa'' sib' |
do'' sib' do'' re'' mib'' fa'' |
re'' mib'' re''4.\trill mib''8 |
mib'' fa'' sol'' sib'' lab'' sol'' |
fa''4 re''4. re''8 |
re''4 mib''8 re'' do'' sib' |
la' sol' la' sib' do'' sib' |
la' sol' la' sib' do'' la' |
sib' re'' do'' mib'' re'' sib'' |
sib''4 la''8 sol'' do''' sib'' |
la''4. fad''8 sol'' la'' |
re'' mib'' re'' do'' sib' la' |
sol'4 la' sib'8 do'' |
sib'4 fad'4. sol'8 |
fad'\trill mi' re' mi' fad' sol' |
la' sol' fad' sol' la' fad' |
sol'4. si'8 do'' re'' |
sol'4 mib''8 fa'' sol''4 |
do''4 la'8 sib' do''4 |
fa'8 fa' sol' la' sib' do'' |
re'' sib' re'' sib' re'' fa'' |
mi'' do'' mi'' do'' mi'' la'' |
fad''4. re''8 sol'' re'' |
re''4 do''8 sib' la' sol' |
do''2 do''4 |
do''4. sib'16 la' sib'4 |
sib'4. la'8 la'4.\trill sol'8 |
sol'4 sol'8 la' si' do'' |
re''4 sol''4. re''8 |
mib'' re'' do'' mib'' re'' do'' |
si'4 do''4. si'8 |
do''4 sol'8 do'' re'' mib'' |
fa''4. sol''8 fa'' mib'' |
re''2\trill r8 re'' |
re'' mib'' fa'' re'' mib''4 |
mib'' re''8 mib'' re'' do'' |
si' re' mib' fa' sol' la' |
si' la' si' do'' re'' si' |
do'' mib'' si' do'' re'' si' |
do''4. do''8 sib' lab' |
sol'4 mib''8 fa'' sol'' mib'' |
lab'' do'' re''4 mi'' |
fa'' fa'' mib'' |
re''\trill re'' do'' |
si'8 do'' re'' mib'' fa'' mib'' |
re''2.\trill |
re''4 si' do'' |
do''4. si'8 si'4.\trill do''8 |
do''4
