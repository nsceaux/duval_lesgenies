\key sol \minor
\tempo "Pesament"
\digitTime\time 2/2 \midiTempo#160 s1*12
\digitTime\time 3/4 s2.*32
\digitTime\time 2/2 s1
\digitTime\time 3/4 s2.*20
\digitTime\time 2/2 s1
\digitTime\time 3/4
\ifComplet { s4 \bar "||" }
\ifConcert { s2.*6 \bar "|." }
