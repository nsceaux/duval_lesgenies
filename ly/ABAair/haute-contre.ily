\clef "haute-contre" mib'8. mib'16 mib'8. mib'16 mib'8. re'16 re'8. re'16 |
re'8. do'16 do'8. do'16 sib8. la16 sol8. sol16 |
sol8. mib''16 mib''8. mib''16 mib''8. re''16 re''8. re''16 |
re''4 do'' sol' fad' |
sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
sol'8. do''16 do''8. re''16 re''8. re''16 do''8. si'16 |
do''8. mib''16 re''8. do''16 sib'8. lab'16 sol'8. fa'16 |
mib'8. mib'16 mib'8. mib'16 mib'8. mib'16 mib'8. mib'16 |
mib'8. mib'16 re'8. mib'16 fa'8. mib'16 re'8. do'16 |
\ru#4 { si8. si16 } |
si8. sol'16 la'8. si'16 do''2~ |
do'' do''4 si'\trill |
do''4 r r |
\ru#31 { \allowPageTurn R2. } R1 R2.*20 R1
\ifComplet r4
\ifConcert s2.*6