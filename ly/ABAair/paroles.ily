- ce.

Que la ter -- re, le feu, que l’on -- de, que les airs
dé -- cou -- vrent les tré -- sors que mon art fait é -- clo -- re ;
vo -- lez, __ dis -- per -- sez- vous du cou -- chant à l’au -- ro -- re,
de vos bien -- faits rem -- plis -- sez l’u -- ni -- vers. __
Vo -- lez, __ dis -- per -- sez- vous du cou -- chant à l’au -- ro -- re,
de vos bien -- faits rem -- plis -- sez l’u -- ni -- vers.
Vo -- lez, __ dis -- per -- sez- vous du cou -- chant à l’au -- ro -- re,
vo -- lez, vo -- lez, __ dis -- per -- sez- vous du cou -- chant à l’au -- ro -- re,
dis -- per -- sez- vous du cou -- chant à l’au -- ro -- re,
de vos bien -- faits rem -- plis -- sez l’u -- ni -- vers.

