\piecePartSpecs
#`((dessus #:indent 0
           #:instrument "Violons"
           #:score-template "score-2dessus")
   (dessus1 #:indent 0
            #:instrument "Violons")
   (dessus2 #:indent 0
            #:instrument "Violons")
   (parties #:score-template "score-parties-voix" #:indent 0)
   (dessus2-haute-contre #:score-template "score-voix"
                         #:indent 0
                         #:notes "dessus2-haute-contre")
   (taille #:score-template "score-voix" #:indent 0)
   (basse #:indent 0
          #:instrument
          ,#{ \markup\center-column { [Basses et B.C.] } #})

   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
