\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket \with { \haraKiriFirst }<<
      \new GrandStaff \with { instrumentName = "Violons" } <<
        \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
        \new Staff <<
          { \startHaraKiri s1*12 s2. \stopHaraKiri }
          \global \keepWithTag #'dessus2 \includeNotes "dessus"
        >>
      >>
      \ifFull <<
        \new Staff \with {
          instrumentName = \markup\center-column { [Hautes-contre] }
        } << \global \includeNotes "haute-contre" >>
        \new Staff \with {
          instrumentName = "[Tailles]"
        } << \global \includeNotes "taille" >>
      >>
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2\pageBreak
        s1*6\break s1*4 s2.*2\pageBreak
        s2.*7\break s2.*7\pageBreak
        s2.*8\break s2.*8\pageBreak
        s1 s2.*6\break s2.*8\pageBreak
        s2.*6 s1
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
