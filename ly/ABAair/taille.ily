\clef "taille" \ru#4 { sol8. sol16 } |
sol8. sol'16 sol'8. fa'16 do'8. do'16 do'8. si16 |
do'8. sol'16 sol'8. sol'16 lab'8. lab'16 lab'8. lab'16 |
sol'2 do' |
re'8. do'16 do'8. do'16 re'8. re'16 re'8. re'16 |
do'8. do'16 fa'8. fa'16 sol'8. sol'16 sol'8. sol'16 |
sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
sol'8. do'16 do'8. do'16 do'8. do'16 do'8. do'16 |
do'8. do'16 sol'8. sol'16 sol'4 sol |
\ru#4 { sol8. sol16 } |
sol2 sol'~ |
sol'4 lab' sol' fa' |
mib' r r |
R2.*31 R1 R2.*20 R1
\ifComplet r4
\ifConcert s2.*6
