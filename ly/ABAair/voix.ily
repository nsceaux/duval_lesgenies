\clef "vbasse" <>^\markup\character Zoroastre do2 r |
R1*11 |
r4 sol4. fa8 |
mib4 mib re |
do2 r8 do' |
re'4 re' do'8 re' |
mib'2 r8 mib |
fa4. fa8 sol lab |
sib2 re8 mib |
do4 do lab, |
sib,2. |
mib2 r8 mib |
sib[\melisma la sib do' re' sib] |
do'[ sib do' re' mib' do']( |
re'2.)\melismaEnd |
re'4 re' do' |
sib fad sol |
mib mib do |
re2 re4 |
sol la sib |
do'2 sib8 la |
sib4 la4. sol8 |
\once\tieDashed re'2.~ |
re'2 r8 re |
sol[\melisma lab sol fa mib re] |
mib[ re do re mib do]( |
fa)[ mib fa sol la fa]( |
sib2.)\melismaEnd |
sib4 sib4. sol8 |
do'2 la8 la |
re4 re' sib |
mib'2 mib'4 |
do' la do' |
fad2 sol8 sib, |
do4.( re16[ mib]) re4. re8 |
sol,2 r8 sol |
sol[ fa sol la si sol]( |
do'2.) |
sol4 do'4. re'8 |
mib'2 re'8 do' |
si4\trill si4. do'8 |
sol4 sol r8 lab |
\ficta lab2 r8 mib |
fa[\melisma mib fa sol lab fa] |
sol2.\melismaEnd |
sol4 sol fa |
mib sol sol |
do' do' re' |
mib'2 mib'4 |
lab4 lab sol |
fa si do' |
re' re' mib' |
fa'2 fa'4 |
fa sol lab |
si, lab mib |
fa4.( sol16[ lab]) sol4. sol8 |
\ifComplet do4
\ifConcert { do2. R2.*5 }

