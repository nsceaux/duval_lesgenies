\clef "basse" sol4 fa |
mib mib re |
do8 sol, mib, sol, do, do' |
re' sib fa re' do' re' |
mib' sib sol sib mib' mib |
fa do fa, fa sol lab |
sib4. sib,8 re mib |
do lab, do lab, do lab, |
sib, lab,? sib, do re sib, |
mib\doux sib, sol, sib, mib, mib |
sib la sib do' re' sib |
do' sib do' re' mib' do' |
re'2. |
re'8 do' re' mib' re' do' |
sib4 fad sol |
mib8 re do re mib do |
re do re mi fad re |
sol re la re sib re |
do' mib' re' do' sib la |
sib4 la4. sol8 |
re'4 re'4. re8\doux |
sol lab sol fa mib re |
mib re do re mib do |
fa mib fa sol la fa |
sib4 sib, sib |
sib sib,8 sib sib sol |
do'4 do8 do' do' la |
re'4 re8 re' re' sib |
mib' re' mib' fa' mib' re' |
do' sib la4. do'8 |
fad re mi fad sol sib, |
do4.( re16 mib) re4. re8 |
sol,2. |
sol2^\doux fa4 |
mib8 fa mib re do4 |
sol4 do'4. re'8 |
mib'4 mib8 mib' re' do' |
si4 si do' |
sol8 re si, re sol, lab^\doux |
lab fa re fa do mib |
fa mib fa sol lab fa |
sol4 sol, sol, |
sol sol,8 sol sol fa |
mib fa sol re sol, sol |
do' do do sol do' re' |
mib' mib mib fa sol mib |
lab lab, lab sib lab sol |
fa4 re do |
si, si, do |
fa2 fa4 |
fa4 sol lab |
si, lab4 mib |
fa4. sol16 lab sol4 sol, |
do2 r8 sol,^\doux |
do si, do re mib fa |
sol fa sol la si sol |
do'4 do do |
do'8 si do' re' si sol |
do' do do' do' sol mib |
lab4 sol fa |
sol8 sol, fa, sol, mib, mib |
mib re mib fa sol la |
sib la sib do' re' mib' |
fa'4 fa fa |
lab8 sol lab sib lab sol |
fa mib fa sol fa mib |
re4 re do |
sol,8 sol^\doux la si do' re' |
mib' re' do' sib lab sol |
do' sib lab sol fa mib |
lab sol fa mib re do |
sol4 sol, sol, |
\clef "alto" sol'4 sol' fa' |
mib' mib' re' |
do' si do' |
lab'4 lab' \clef "bass" r8 mib^\doux |
fa mib fa sol lab fa |
sol fa sol la si sol |
la si do' re' mib' do' |
re' do' re' mib' fa' re' |
mib'4 mib mib |
mib re do |
lab lab mib |
fa4.( sol16 lab) sol4 sol, |
do2 do'4 |
si4. la8 sol fa |
mib4 do mib |
fa2 mib4 |
fa sol sol, |
do2. |
