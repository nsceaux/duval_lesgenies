\clef "dessus" sol'4 sol' |
do'' do'' re'' |
mib''2 r8 mib'' |
fa''4. fa''8 sol'' lab'' |
sol''2\trill r8 sol'' |
sol''4. fa''8 fa'' mib'' |
re''4\trill fa''4. sib'8 |
mib''4 mib'' mib'' |
mib''2( re''4) |
<>^"Violons" mib''8\doux re'' mib'' sol'' fa'' mib'' |
re''4.\trill fa''8 sib'' sib'' |
sib''4 la''8 sib'' la'' sol'' |
fad'' sol'' fad'' mib'' re''4 |
<>^"[Tous]" la'4 la'8 sib' do'' la' |
re''4 do'' sib' |
do'' do'' la' |
fad'2\trill \appoggiatura mi'?8 re'4 |
sib' do'' re'' |
mib''2 re''8 do'' |
re''4 do''4.\trill sib'8 |
la'4.\trill <>^"Violons" sib'8\doux do'' la' |
sib'4 sol'8 si' do'' re'' |
sol'4 mib''8 re'' do'' sib' |
la' do'' fa'' mib'' re'' do'' |
re'' do'' sib' do'' re'' mib'' |
<>^"Tous" fa''4 re''4. sol''8 |
mi''2\trill mi''8 la'' |
fad''4 fad'' re'' |
sol''2 sol''4 |
sol' do'' do'' |
do''2 sib'8 sib' |
sib'4.( la'8) la'4. re''8 |
si'2\trill r8 <>^"Violons" re''\doux |
sol' la' si' do'' re'' si' |
do'' si' do'' re'' mib'' fa''
<>^"[Tous]" re''4 mib'' fa'' |
sol''2 fa''8 mib'' |
re''4 re'' mib'' |
si'8 re'' si' sol' <>^"Violons" r fa''\doux |
fa'' sol'' lab'' fa'' sol''4 |
sol'' fa''8 sol'' fa'' mib'' |
re'' mib'' re'' do'' si'4 |
<>^"[Tous]" re''4 re'' re'' |
sol'' sol'' fa'' |
mib'' mib''4. si'8 |
do''2 sol'4 |
do'' re'' mi'' |
fa'' fa'' mib'' |
re'' re'' do'' |
si'2\trill si'4 |
re'' re'' re'' |
fa'' re'' mib'' |
mib''4.( re''8) re''4.\trill do''8 |
<>^"Violons" do''8\doux sol' la' si' do'' re'' |
mib'' fa'' mib'' re'' do'' re'' |
si'4.\trill sol''8 sol'' fa'' |
mib'' fa'' mib'' re'' do''4 |
<>^"[Tous]" sol'4 do'' re'' |
mib'' mib'' sol'' |
do'' do'' re'' |
<>^"Violons" si'8\trill\doux re'' si' re'' sol' sol'' |
sol''4. fa''8 mib'' fa'' |
re''4.\trill sib''8 sib'' sib'' |
lab'' sib'' lab'' sol'' fa''4 |
<>^"Tous" fa''4 fa'' sol'' |
lab'' lab'' sol'' |
fa'' fa'' mib'' |
re''\trill re'' r |
r4 r <>^"Violons" r8 re''8\doux |
mib'' re'' do'' sib' lab' sol' |
do'' sib' lab' sol' fa' mib' |
re'2.\trill |
\twoVoices #'(dessus1 dessus2 dessus) <<
  { re''4 re'' re'' |
    mib'' mib'' si' |
    do'' re'' mib'' |
    fa'' fa'' }
  { si'4 si' si' |
    do'' do'' re'' |
    mib'' fa'' mib'' |
    re''\trill re'' }
>> <>^"Violons" r8 sol''\doux |
sol''4 lab''8 sol'' fa'' mib'' |
re''4. do''8 re'' si' |
do'' re'' mib'' fa'' sol'' mib'' |
fa'' mib'' fa'' sol'' lab'' fa'' |
sol''2. |
<>^"Tous" do''4 re'' mib'' |
fa'' re'' mib'' |
mib''4.( re''8) re''4.\trill do''8 |
<>^"Violons" do''8 si' do'' re'' mib'' fa'' |
sol'' fa'' sol'' la'' si'' sol'' |
do''' sib'' lab'' sol'' fa'' mib'' |
lab''4. si'8 do''4 |
re'' re''4.\trill do''8 |
do''2. |

