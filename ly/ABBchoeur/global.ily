\set Score.currentBarNumber = 68
\key sol \minor \midiTempo#160
\digitTime\time 3/4 \partial 2 s2 s2.*30
\digitTime\time 2/2 s1
\digitTime\time 3/4 s2.*20
\digitTime\time 2/2 s1
\digitTime\time 3/4 s2.*30
\digitTime\time 2/2 s1
\digitTime\time 3/4 s2.*6 \bar "|."

