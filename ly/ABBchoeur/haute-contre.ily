\clef "haute-contre" re'4 re' |
sol' sol' fa' |
mib'8 sol do' mib' sol' do'' |
lab' fa' re' sib' sib' sib' |
sib' sol' mib' sol' sib' sol' |
lab' fa' do' lab' lab' lab' |
lab'4 lab'4. sol'8 |
sol'4 lab' lab' |
sol'( fa'2) |
mib'8\doux fa' sol' sol' sol' sol' |
fa'4.\trill re''8 re'' re'' |
re''4 do'' do''8 sib' |
la' sib' la' sol' fad'4 |
fad' fad'8 sol' la' fad' |
sol'4 la' sol' |
sol' sol' mib' |
re'2 re'4 |
sol' fad' sib' |
la'2\trill sol'8 fad' |
sol'4 fad'4.\trill sol'8 |
fad'4.\trill sol'8\doux la' fad' |
sol'4. sol'8 la' si' |
do'' sib'! la' sib' la' sol' |
fa'4 la'8 sol' fa' mib' |
fa' mib' re' la' sib' do'' |
re''4 re''4. re''8 |
do''2 do''8 do'' |
la'4 la' sib' |
sib'8 la' sol' la' sol' fa' |
mib'4 la'4. la'8 |
la'2 sol'8 sol' |
sol'4.( fad'8) fad'4.\trill sol'8 |
sol'2 r8 si'\doux |
si'8 la' sol'4 re' |
mib'8 re' mib' fa' sol'4 |
sol' sol'4. sol'8 |
sol'4 sol'8 la' si' do'' |
re''4 sol' sol' |
sol' re' r8 lab'\doux |
lab'8 sib' do'' lab' sol'4 |
lab'2 re'4 |
re'2. |
si'8 la' si' do'' re'' si' |
do''4 si' si' |
do'' do''4. sol'8 |
sol'2 sol'4 |
do''4 do'' sib' |
lab' lab' sol' |
sol' sol' sol' |
lab'2 lab'4 |
lab' sol' fa' |
lab' fa' sol' |
sol'2 sol'4. sol'8 |
sol'8\doux mib' fa' sol' la' si' |
do'' re'' do'' si' do'' sol' |
sol'2 sol'4 |
sol'2 \appoggiatura fa'8 mib'4 |
sol' sol' sol' |
sol' sol' sib' |
lab' lab' lab' |
sol'2 sib'!4\doux |
sib'4. la'8 sol' fa' |
fa'4. re''8 re'' re'' |
do'' re'' do'' sib' lab'4 |
do''4 do'' sib' |
lab'4 do'' do'' |
si' si' do'' |
si'\trill si' r8 si'\doux |
do''4 do'8 re' mib' fa' |
sol'4. mib'8 fa' sol' |
mib'4 fa' si8 do' |
si2.\trill |
sol'4 sol' fa' |
mib' mib' re' |
do' si do' |
lab' lab' r8 sol'\doux |
lab'4 fa' fa' |
sol' sol' sol' |
fa'4 mib'8 re' do' mib' |
re'4 si'4. re''8 |
do''2. |
la'4 si' do'' |
si' re'' do'' |
do''4.( si'8) si'4.\trill do''8 |
do''4 sol'8 fa' mib'4 |
re' si'8 do'' re'' si' |
do''4. sib'!8 lab' sol' |
fa'4. re'8 mib' re' |
do'4 do' si\trill |
do'2. |
