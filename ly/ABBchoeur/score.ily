\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with { instrumentName = "Violons" } <<
        \global \includeNotes "dessus"
      >>
      \ifFull <<
        \new Staff \with {
          instrumentName = \markup\center-column { [Hautes-contre] }
        } << \global \includeNotes "haute-contre" >>
        \new Staff \with {
          instrumentName = "[Tailles]"
        } << \global \includeNotes "taille" >>
      >>
    >>
    \new ChoirStaff <<
      \new Staff \with { instrumentName = "[Dessus]" } \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \unlessFull\new Staff \with {
        \haraKiriFirst
        instrumentName = \markup\center-column { [Hautes-contre] }
      } \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \ifFull <<
        \new Staff \with {
          instrumentName = \markup\center-column { [Hautes-contre] }
        } \withLyrics <<
          \global \keepWithTag #'vhaute-contre \includeNotes "voix"
        >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
        \new Staff \with { instrumentName = "[Tailles]" } \withLyrics <<
          \global \keepWithTag #'vtaille \includeNotes "voix"
        >> \keepWithTag #'vtaille \includeLyrics "paroles"
      >>
      \new Staff \with { instrumentName = "[Basses]" } \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'zoroastre \includeNotes "voix"
    >> \keepWithTag #'zoroastre \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2 s2.*5\pageBreak
        s2.*7\break s2.*7\pageBreak
        s2.*8\break s2.*3 s1 s2.*3\pageBreak
        s2.*7\break s2.*8\pageBreak
        s2.*2 s1 s2.*4\break s2.*7\pageBreak
        s2.*8\break s2.*8\pageBreak
        s2.*3 s1 s2.*2\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
