\clef "taille" si4 si |
do' do' si |
do'8 sol do' mib' sol' sol' |
fa' re' fa' lab' sol' fa' |
mib' sol' mib' sol' mib' sib |
lab fa' do' do' do' do' |
sib4 sib4. sib8 |
sib4 do' do' |
sib2( lab4) |
sol4. sib8\doux sib sib |
sib8 do' re' mib' fa' re' |
mib' re' mib' re' do'4 |
re'2 re'4 |
re' re' re' |
re' la re' |
do'8 sib la4 do' |
la2\trill \appoggiatura sol8 fad4 |
re' re' sol' |
fad'2\trill re'8 re' |
re'4 re'4. re'8 |
re'2 r8 re'\doux |
re'4 sol sol |
sol2 do'4 |
do'2 do'4 |
sib2 fa'4 |
fa' fa'4. sol'8 |
sol'2 mi'8 mi' |
fad'4 re' fa' |
mib'8 fa' sol' la' sol' fa' |
mib'4 mib'4. mib'8 |
re'8 fad' sol' la' sib' re' |
mib'4.( re'16 do') re'4. re'8 |
re'2 r8 sol'\doux |
sol'4 sol sol |
sol2 sol4 |
si do'4. si8 |
do'4 sol'8 la' sol' sol' |
sol'4 re' do' |
re'4 re' r8 do'\doux |
do'2 mib'4 |
mib' re'8 mib' re' do' |
si do' si la sol4 |
sol' sol' sol' |
sol' sol' sol' |
sol' sol'4. sol'8 |
sol'2 mib'4 |
mib' mib' do' |
do' re' mib' |
fa' fa' mib' |
fa'2 fa'4 |
fa' fa' fa' |
re' re' do' |
do'4.( si8) si4.\trill do'8 |
do'4. si'8\doux la' sol' |
sol'4. fa'8 mib' re' |
re'4 si8 do' re' si |
do' re' do' si do'4 |
mib' mib' re' |
do' mib' mib' |
mib' do' do' |
re'2 mib'4\doux |
mib'2 sib4 |
sib4. fa'8 fa' fa' |
fa'2 fa'4 |
fa'4 fa' mib' |
fa'4 fa' sol' |
sol' sol' sol' |
sol' sol' r8 sol'\doux |
sol' fa' mib' re' do' sib |
do'4 do' re' |
mib' do' re' |
re'2. |
sol'4 sol' fa' |
mib' mib' re' |
do' si do' |
lab' lab' r8 mib'\doux |
mib'4 re'8 mib' re' do' |
si2 re'4 |
do'4 sol' sol' |
fa' lab'8 sol' fa' lab' |
sol'2. |
la'!4 sol' sol' |
lab' fa' sol' |
sol'2 sol'4. sol'8 |
mib'8 re' mib' fa' sol' lab' |
sol'4 re' sol' |
sol'2 do'4 |
si2 sol4 |
lab sol sol |
mib2. |

