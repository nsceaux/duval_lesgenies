<<
  \tag #'(vdessus basse) {
    \clef "vdessus" <>^\markup\character Chœur sol'4 sol' |
    do'' do'' re'' |
    mib''2 r8 mib'' |
    fa''4. fa''8 sol'' lab'' |
    sol''2\trill r8 sol'' |
    sol''4. fa''8 fa'' mib'' |
    re''4\trill fa''4. sib'8 |
    mib''4 mib'' mib'' |
    mib''2( re''4) |
    mib''2 r4 |
    R2.*3 |
    la'4 la'8[ sib'] do''[ la'] |
    re''4 do'' sib' |
    do'' do'' la' |
    fad'2\trill \appoggiatura mi'?8 re'4 |
    sib' do'' re'' |
    mib''2 re''8 do'' |
    re''4 do''4.\trill sib'8 |
    la'2\trill r4 |
    R2.*4 |
    fa''4 re''4. sol''8 |
    mi''2\trill mi''8 la'' |
    fad''4 fad'' re'' |
    sol''2 sol''4 |
    sol' do'' do'' |
    do''2 sib'8 sib' |
    sib'4.( la'8) la'4. re''8 |
    si'2\trill r4 |
    R2.*2 |
    re''4 mib'' fa'' |
    sol''2 fa''8 mib'' |
    re''4 re'' mib'' |
    si'4 sol' r |
    R2.*3 |
    re''4 re'' re'' |
    sol'' sol'' fa'' |
    mib'' mib''4. si'8 |
    do''2 sol'4 |
    do'' re'' mi'' |
    fa''4 fa'' mib''! |
    re'' re'' do'' |
    si'2\trill si'4 |
    re''4 re'' re'' |
    fa'' re'' mib'' |
    mib''4.( re''8) re''4.\trill do''8 |
    do''2 r4 |
    R2.*3 |
    sol'4 do'' re'' |
    mib'' mib'' sol'' |
    do'' do'' re'' |
    si'\trill \appoggiatura la'8 sol'4 r |
    R2.*3 |
    fa''4 fa'' sol'' |
    lab'' lab'' sol'' |
    fa'' fa'' mib'' |
    re''\trill re'' r |
    R2.*4 |
    <<
      { \voiceOne re''4 re'' re'' |
        mib'' mib'' si' |
        do'' re'' mib'' |
        fa'' fa'' \oneVoice }
      \new Voice {
        \voiceTwo si'4 si' si' |
        do'' do'' re'' |
        mib'' fa'' mib'' |
        re''\trill re''
      }
    >> r4 |
    R2.*5 |
    do''4 re'' mib'' |
    fa'' re'' mib'' |
    mib''4.( re''8) re''4.\trill do''8 |
    do''2 r4 |
    R2.*5 |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" sol'4 sol' |
    sol' sol' sol' |
    sol'2 r8 mib' |
    lab'4. lab'8 sol' fa' |
    mib'2 r8 sol' |
    lab'4. lab'8 lab' lab' |
    lab'4 lab'4. sol'8 |
    sol'4 lab' lab' |
    sol'4( fa'2) |
    mib'2 r4 |
    R2.*3 |
    fad'4 fad'8[ sol'] la'[ fad'] |
    sol'4 la' sol' |
    sol' sol' mib' |
    re'2 re'4 |
    sol' fad' sol' |
    fad'2\trill sol'8 la' |
    sol'4 fad'4.\trill sol'8 |
    fad'2\trill r4 |
    R2.*4 |
    fa'4 fa'4. sol'8 |
    sol'2 mi'8 mi' |
    fad'4 la' sib' |
    sib'2 sib'4 |
    la' la'4. la'8 |
    la'2 sol'8 sol' |
    sol'4.( fad'8) fad'4.\trill sol'8 |
    sol'2 r4 |
    R2.*2 |
    sol'4 sol'4. sol'8 |
    sol'2 sol'8 sol' |
    sol'4 sol' sol' |
    sol' re' r |
    R2.*3 |
    sol'4 sol' sol' |
    sol' sol' sol' |
    sol' sol'4. sol'8 |
    sol'2 sol'4 |
    mib' mib' do' |
    do' lab' sol' |
    sol' sol' sol' |
    lab'2 lab'4 |
    lab' sol' fa' |
    lab' fa' sol' |
    sol'2 sol'4. sol'8 |
    mib'2 r4 |
    R2.*3 |
    sol'4 sol' sol' |
    sol' sol' sib' |
    lab' mib' lab' |
    sol' sol' r |
    R2.*3 |
    fa'4 fa' mib' |
    fa' fa' sol' |
    sol' sol' sol' |
    sol' sol' r4 |
    R2.*4
    sol'4 sol' fa' |
    mib' mib' re' |
    do' si do' |
    lab' lab' r4 |
    R2.*5 |
    la'4 sol' sol' |
    lab' fa' sol' |
    sol'2 sol'4. sol'8 |
    mib'2 r4 |
    R2.*5 |
  }
  \tag #'vtaille {
    \clef "vtaille" si4 si |
    do' do' si |
    do'2 r8 do' |
    sib4. sib8 mib' re' |
    mib'2 r8 mib' |
    mib'4 do' do'8 do' |
    sib4 sib4. sib8 |
    sib4 do' do' |
    sib2( lab4) |
    sol2 r4 |
    R2.*3 |
    re'4 re' re' |
    re' la re' |
    do'8[ sib] la4 do' |
    la2\trill \appoggiatura sol8 fad4 |
    re' re' re' |
    do'2 re'8 re' |
    re'4 re'4. re'8 |
    re'2 r4 |
    R2.*4 |
    re'4 re'4. re'8 |
    do'2 do'8 do' |
    la4 re' fa' |
    mib'2 mib'4 |
    mib' mib'4. mib'8 |
    re'2 re'8 re' |
    mib'4.( re'16[ do']) re'4. re'8 |
    re'2 r4 |
    R2.*2 |
    si4 do'4. si8 |
    do'2 si8 do' |
    re'4 re' do' |
    re' si r |
    R2.*3 |
    si4 si si |
    do' si si |
    do' mib'4. re'8 |
    mib'2 mib'4 |
    do' do' sib |
    lab re' mib' |
    fa' fa' mib' |
    fa'2 fa'4 |
    fa' fa' fa' |
    re' re' do' |
    do'4.( si8) si4.\trill do'8 |
    do'2 r4 |
    R2.*3 |
    mib'4 mib' re' |
    do' mib' mib' |
    mib' do' do' |
    \appoggiatura do'8 re'4 \appoggiatura do'8 si4 r |
    R2.*3 |
    do'4 do' sib |
    lab do' do' |
    si si do' |
    si\trill si r |
    R2.*13 |
    la4 si do' |
    si re' do' |
    do'4.( si8) si4.\trill do'8 |
    do'2 r4 |
    R2.*5 |
  }
  \tag #'vbasse {
    \clef "vbasse" sol4 fa |
    mib mib re |
    do2 r8 do' |
    re'4. re'8 do' re' |
    mib'2 r8 mib |
    fa4. fa8 sol lab |
    sib4 re4. mib8 |
    do4 do lab, |
    sib,2. |
    mib2 r4 |
    R2.*3 |
    re'4 re' do' |
    sib fad sol |
    mib mib do |
    re2 re4 |
    sol la sib |
    do'2 sib8 la |
    sib4 la4.\trill sol8 |
    re'2 r4 |
    R2.*4 |
    sib4 sib4. sol8 |
    do'2 la8 la |
    re4 re' sib |
    mib'2 mib'4 |
    do' la4. do'8 |
    fad2 sol8 sib, |
    do4.( re16[ mib]) re4. re8 |
    sol,2 r4 |
    R2.*2 |
    sol4 do'4. re'8 |
    mib'2 re'8 do' |
    si4 si do' |
    sol4 sol r |
    R2.*3 |
    sol4 sol fa |
    mib sol sol |
    do'4 do'4. re'8 |
    mib'2 mib'4 |
    lab lab sol |
    fa si do' |
    re' re' mib' |
    fa'2 fa'4 |
    fa4 sol lab |
    si, lab mib |
    fa4.( sol16[ lab]) sol4. sol8 |
    do2 r4 |
    R2.*3 |
    do'4 do' si |
    do' do' sol |
    lab sol fa |
    sol sol r |
    R2.*3 |
    lab4 lab sol |
    fa fa mib |
    re re do |
    sol sol r |
    R2.*13 |
    mib4 re do |
    lab lab mib |
    fa4.( sol16[ lab]) sol4. sol8 |
    do2. |
    R2.*5 |
  }
  \tag #'zoroastre {
    \clef "vbasse" r4 r |
    R2.*8 |
    r4 r <>^\markup\character Zoroastre r8 mib |
    sib[\melisma la sib do' re' sib] |
    do'[ sib do' re' mib' do']( |
    re'2.)\melismaEnd |
    re'4 re' do' |
    sib fad sol |
    mib mib do |
    re2 re4 |
    sol la sib |
    do'2 sib8 la |
    sib4 la4.\trill sol8 |
    re'2 r8 re |
    sol[\melisma lab sol fa mib re] |
    mib[ re do re mib do] |
    fa[ mib fa sol la fa]( |
    sib2.)\melismaEnd |
    sib4 sib4. sol8 |
    do'2 la8 la |
    re4 re' sib |
    mib'2 mib'4 |
    do' la4. do'8 |
    fad2 sol8 sib, |
    do4.( re16[ mib]) re4. re8 |
    sol,2 r8 sol |
    sol8[ fa sol la si sol]( |
    do'2.) |
    sol4 do'4. re'8 |
    mib'2 re'8 do' |
    si4 si do' |
    sol sol r8 lab |
    lab2 r8 mib |
    fa[ mib fa sol lab fa]( |
    sol2.) |
    sol4 sol fa |
    mib sol sol |
    do' do'4. re'8 |
    mib'2 mib'4 |
    lab lab sol |
    fa si do' |
    re' re' mib' |
    fa'2 fa'4 |
    fa sol lab |
    si, lab mib |
    fa4.( sol16[ lab]) sol4. sol8 |
    do2 r8 sol, |
    do[\melisma si, do re mib fa] |
    sol[ fa sol la si sol]( |
    do'2.)\melismaEnd |
    do'4 do' si |
    do' do' sol |
    lab sol fa |
    sol sol r8 mib |
    mib[\melisma re mib fa sol la] |
    sib[ la sib do' re' mib']( |
    fa'2.)\melismaEnd |
    lab4 lab sol |
    fa fa mib |
    re re do |
    sol sol r8 re' |
    mib'[\melisma re' do' sib lab sol] |
    do'[ sib lab sol fa mib] |
    lab[ sol fa mib re do]( |
    sol2.)\melismaEnd |
    R2.*3 |
    r4 r r8 mib |
    fa[\melisma mib fa sol lab fa] |
    sol[ fa sol la si sol] |
    la[ si do' re' mib' do'] |
    re'[ do' re' mib' fa' re'] |
    mib'2.~ |
    mib'\melismaEnd |
    r4 lab mib |
    fa4.( sol16[ lab]) sol4. sol8 |
    do2. |
    R2.*4 |
  }
>>
