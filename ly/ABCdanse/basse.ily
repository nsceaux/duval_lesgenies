\clef "basse"
\setMusic #'rondeau {
  do8*2/3( re mib) do( re mib) do( re mib) |
  lab,4 do lab, |
  re8*2/3( mib fa) re( mib fa) re( mib fa) |
  sib,4 re sib, |
  mib8*2/3( fa sol) mib( fa sol) mib( fa sol) |
  do4 mib do |
  fa8*2/3( sol lab) mib8[ fa] sol[ sol,] |
  do sol, mib, sol, do,4 |
  do8*2/3( re mib) do( re mib) do( re mib) |
  lab,4 do lab, |
  re8*2/3( mib fa) re( mib fa) re( mib fa) |
  sib,4 re sib, |
  mib8*2/3( fa sol) mib( fa sol) mib( fa sol) |
  do4 mib do |
  fa8*2/3( sol lab) mib8[ fa] sol[ sol,] |
}
\keepWithTag #'() \rondeau
do2. do'4 |
fa4 mib re sol |
do sol, do, do8 re |
mib4. re8 do4 lab, |
sol,4. sol8 do'4 fa |
sol8 fa mib re do re mib do |
fa4 mib re do |
sol sol,8 la, si,4 sol, |
mib,1 |
mib2 sol8 fa mib4 |
sib2 fa4 lab |
mib2 fa |
sol4 sol, r2 |
sol4 fad sol re |
sib,4. la,8 sol, la, sib, sol, |
do4 la, re re, |
sol,1 |
\rondeau |
do2 |
do8. re16 mib8. do16 |
sol8. la16 si8. sol16 |
do'8. mib'16 re'8. si16 |
mib'4 mib |
lab8. sol16 fa8. re16 |
mib8. fa16 sol8. sol,16 |
do8. mib16 re8. fa16 |
mib4 re8. do16 |
sol4 fa |
mib8. do16 mib8. do16 |
sol8. la16 si8. sol16 |
do'8. mib'16 re'8. si16 |
mib'4 sol |
lab8. sol16 fa8. re16 |
mib8. fa16 sol8. sol,16 |
do8. mib16 re8. fa16 |
mib4 re8. do16 |
sol2 |
\rondeau do2.
