\setMusic #'rondeau {
  s2. <7>2\figExtOn <7>4\figExtOff <7 5/>2. <7->2\figExtOn <7->4\figExtOff
  <7>2. <7>2\figExtOn <7>4\figExtOff <_->4 <6>4 <7 _+> s2.
  s2. <7>2\figExtOn <7>4\figExtOff <7 5/>2. <7->2\figExtOn <7->4\figExtOff
  <7>2. <7>2\figExtOn <7>4\figExtOff <_->4 <6> <_+>
}
\keepWithTag #'() \rondeau
s1 <_->4 <6> <7 5/> <7 _+> s1 <6>4. <6+>8 s4 <6> <_+>4 <6 4>8 <_+> s4 <6 5 _-> <_+>4 <6>8 <6+> s2
<_->4 <6> <6+>2 <_+>4.\figExtOn <_+>8\figExtOff s2 <5>1 <"">8*5\figExtOn <"">8\figExtOff s4 s2 <_->4\figExtOn <_->\figExtOff <6>2 <6 5 _-> <_+>1 s4 <5/>2 <_+>4
<6>2 <"">4.\figExtOn <"">8 <6 5>4 <6>\figExtOff <_+> <7> <_+>1
\rondeau
s2*2 <_+>4\figExtOn <_+> <"">8. <"">16 <5/>8. <5/>16\figExtOff
s2 <9>4 <_->8. <6 5>16 <6>8.\figExtOn <6>16\figExtOff <6 4>8. <_+>16 s4 <6+ 5/> <6> <6+> <_+>\figExtOn <_+>\figExtOff <6>2 <_+>4\figExtOn <_+> <"">8. <"">16 <5/>8. <5/>16\figExtOff
s4 <6> <9> <_->8. <6 5>16 <6>8.\figExtOn <6>16\figExtOff <6 4>8. <_+>16 s4 <6+ 5/> <6> <6+> <_+>2
\rondeau
