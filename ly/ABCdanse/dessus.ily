\clef "dessus"
\setMusic #'rondeau {
  sol'2 do''4 |
  mib''8*2/3( re'' do'') mib''( re'' do'') mib''( re'' do'') |
  lab'2 re''4 |
  fa''8*2/3( mib'' re'') fa''( mib'' re'') fa''( mib'' re'') |
  sib'2 mib''4 |
  sol''8*2/3( fa'' mib'') sol''( fa'' mib'') sol''( fa'' mib'') |
  lab''( sol'' fa'') sol''( fa'' mib'') fa'' re'' sol'' |
  mib''4.\trill re''8 do''4 |
  sol'2 do''4 |
  mib''8*2/3( re'' do'') mib''( re'' do'') mib''( re'' do'') |
  lab'2 re''4 |
  fa''8*2/3( mib'' re'') fa''( mib'' re'') fa''( mib'' re'') |
  sib'2 mib''4 |
  sol''8*2/3( fa'' mib'') sol''( fa'' mib'') sol''( fa'' mib'') |
  lab''( sol'' fa'') sol''( fa'' mib'') re''( do'' si') |
}
\keepWithTag #'() \rondeau
do''4 do''' do'''8 sib'' lab'' sol'' |
lab''4 sol'' fa''8 lab'' sol'' fa'' |
mib''4.\trill re''8 do''4 mib''8 fa'' |
sol'' sol' la' si' do'' re'' mib'' fa'' |
sol'' sol' do'' re'' mib'' fa'' sol'' lab'' |
si' sol' do'' re'' mib'' fa'' sol'' do'' |
lab''4 sol'' fa'' mib'' |
re''1\trill |
mib''2 sol''8 fa'' mib''4 |
sib''1 |
fa''2 lab''8 sol'' fa''4 |
do'''4 mib''' re''' do''' |
si'' re''' sol'' fad'' |
sol'' re'' sol' fad' |
sol'8 la' sib' do'' re''4 sol' |
la'2 fad'\trill |
sol'1 |
\rondeau |
do''2 |
sol''4 do''' |
si''8.\trill la''16 sol''8. fa''16 |
mib''4 lab'' |
sol''8. fa''16 mib''8. re''16 |
do''4 fa'' |
mib''8. re''16 do''8. si'16 |
do''4 lab'' |
sol''8. do'''16 si''8. do'''16 |
re'''8. sol''16 re''4 |
sol'' do''' |
si''8.\trill la''16 sol''8. fa''16 |
mib''4 lab'' |
sol''8. fa''16 mib''8. re''16 |
do''4 fa'' |
mib''8. re''16 do''8. si'16 |
do''4 lab'' |
sol''8. do'''16 si''8. do'''16 |
re'''2 |
\rondeau do''2.
