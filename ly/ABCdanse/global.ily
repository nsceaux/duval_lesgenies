\key sol \minor
\digitTime\time 3/4 \midiTempo#120 s2.*15
\digitTime\time 2/2 \midiTempo#160 \tempo "Gai" s1*8
\tempo "Lentement" s1*9
\digitTime\time 3/4 \midiTempo#120 s2.*15
\time 2/4 \midiTempo#120 s2 \tempo "Gai" s2*18
\digitTime\time 3/4 \midiTempo#120 s2.*16 \bar "|."
