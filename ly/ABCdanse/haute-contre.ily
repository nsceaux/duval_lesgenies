\clef "haute-contre"
\setMusic #'rondeau {
  sol'2 sol'4 |
  sol'8*2/3( fa' mib') sol'( fa' mib') sol'( fa' mib') |
  fa'2 lab'4 |
  lab'8*2/3( sol' fa') lab'( sol' fa') lab'( sol' fa') |
  sol'2 sib'4 |
  sib'8*2/3( lab' sol') sib'( lab' sol') sib'( lab' sol') |
  fa'4 mib'8*2/3( re' do') sol'4 |
  sol'4. fa'8 mib'4 |
  sol'2 sol'4 |
  sol'8*2/3( fa' mib') sol'( fa' mib') sol'( fa' mib') |
  fa'2 lab'4 |
  lab'8*2/3( sol' fa') lab'( sol' fa') lab'( sol' fa') |
  sol'2 sib'4 |
  sib'8*2/3( lab' sol') sib'( lab' sol') sib'( lab' sol') |
  fa'4 mib'8*2/3( re' do') sol'4 |
}
\keepWithTag #'() \rondeau
mib'4 mib'' mib''8 re'' do'' sib' |
do''2. si'4 |
do''2. sol'4 |
sol'2. do''4 |
si' sol'2 re'4 |
re'4 sol'8 fa' mib' re' do'4~ |
do' do'' si' do'' |
si'1\trill |
sol'2 r |
sol' sib'8 lab' sol'4 |
re'2 do''8 sib' lab'4 |
sol'2 fa'4 mib' |
re'2 r |
sib'4 la' sib' re' |
re'8 fad' sol' la' sib'4 re' |
mib'2 re'4 do' |
si1\trill |
\rondeau |
sol'2 |
mib''8. re''16 do''8. mib''16 |
re''8. do''16 re''8. si'16 |
do''4 re'' mib'' sol'~ |
sol' lab' |
sol'8. fa'16 mib'8. re'16 |
mib'4 si' |
do''8. sol'16 sol'8. do''16 |
si'2\trill |
do''4 do''8. mib''16 |
re''8. do''16 re''8. si'16 |
do''4 re'' mib'' sol'~ |
sol' lab' |
sol'8. fa'16 mib'8. re'16 |
mib'4 si' |
do''8. sol'16 sol'8. do''16 |
si'2\trill |
\rondeau |
sol'2. |
