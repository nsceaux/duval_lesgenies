\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { [Violons et Hautbois] }
    } << \global \includeNotes "dessus" >>
    \ifFull <<
      \new Staff \with {
        instrumentName = \markup\center-column { [Hautes-contre] }
      } << \global \includeNotes "haute-contre" >>
      \new Staff \with {
        instrumentName = "[Tailles]"
      } << \global \includeNotes "taille" >>
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses, Bassons et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion {
        s2.*15\break s1*8 s1*9\break
        s2.*15 s2\break s2*18\break
      }
      \origLayout {
        s2.*7\break s2.*7\break s2. s1*6\pageBreak
        s1*8\break s1*3 s2*4\break s2*9\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}