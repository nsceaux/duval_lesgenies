\clef "taille"
\setMusic #'rondeau {
  mib'2 mib'8*2/3( re' do') |
  do'2 do'4 |
  do'2 fa'8*2/3( mib' re') |
  re'2 re'4 |
  re'2 sol'8*2/3( fa' mib') |
  mib'2. |
  do'2 si4\trill |
  do'4. si8 do'4 |
  mib'2 mib'8*2/3( re' do') |
  do'2 do'4 |
  do'2 fa'8*2/3( mib' re') |
  re'2 re'4 |
  re'2 sol'8*2/3( fa' mib') |
  mib'2. |
  do'2 si4\trill |
}
\keepWithTag #'() \rondeau
do'4 sol' sol'2 |
fa'4 sol' lab' re' |
mib'2. mib'8 re' |
do'4. re'8 mib' fa' sol' lab' |
sol' fa' mib' re' do'4 do' |
si4 sol'~ sol'8 fa' mib' sol' |
fa'4 sol'2 sol'4 |
sol'1 |
sib2 r |
mib'2 mib'8 fa' sol'4 |
re'2 do'4 do' |
do'1 |
re'2 r |
re'2. la4 |
sol4. do'8 sib la sol4 |
sol2 re' |
re'1 |
\rondeau |
mib'2 |
do'4 sol' |
sol' sol' |
sol' si' |
do'' do'8. re'16 |
mib'4 do' |
do' sol |
sol8. sol'16 fa'8. re'16 |
mib'4 fa'8. mib'16 |
re'4 sol'~ |
sol' sol' |
sol' sol' |
sol' si' |
do'' do'8. re'16 |
mib'4 do' |
do' sol |
sol8. sol'16 fa'8. re'16 |
mib'4 fa'8. mib'16 |
re'2 |
\rondeau |
mib'2. |
