\clef "basse" do'4 do4. do'8 |
si do' re' do' si la |
sol la fa sol mi4 |
fa sol sol, |
do2~ do8 do' |
do'4 do4. do'8 |
si2 do'4 |
sol8 la fa sol mi4 |
re re2\trill |
do4 si, do |
sol fa mi |
re2. |
do4 si, do |
sol,2. |
do4 do,2 |
do2.~ |
do~ |
do |
do'4 re'8 do' si la |
sol, sol si,4. do8 |
sol, sol si,4. do8 |
sol, sol sol4 fad |
sol2.~ |
sol~ |
sol | \allowPageTurn
do2 fa,4 fad, |
sol,8 sol do'4. la8 |
re' si sol4 fad |
sol2. |
fad4. sol8 re4 re, |
sol,2 sol4 |
sol2.~ |
sol~ |
sol~ | \allowPageTurn
sol8. fa16 mi8. re16 do2 |
fa,4. la,8 sol,4 |
do,2 do4 |
fa4 fad2 |
sol2 fa4 |
mi8. fa16 sol4 sol, |
do2 do'4 |
si2 la4 |
sol fa mi |
fa sol sol, |
do2 do4 |
si,2 la,4 |
sol,2. |
mi'4. re'8 do'4 |
re'4 sol do' |
si2 do'4 |
fa2 fa,4 |
do2. |
sol2 sol,4 |
sol sol sol |
sol sol, sol8 fa |
mi sol fa mi re do |
si,2 sol,4 |
do8 re mi4. fa8 |
sol2. |
sol |
fa |
mi2 fa4 |
sol2. |
fa |
mi8 fa sol4 sol, |
do2
