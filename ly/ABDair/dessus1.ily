\clef "dessus" <>^"Flûtes seules"
mi''4. re''8 do''4 |
re'' sol' r8 do'' |
si'2\trill do''4 |
re''8 do'' re'' mi'' do'' re'' |
mi''4 \appoggiatura re''8 do''4 mi''8 fa'' |
sol''2.~ |
sol''4 fa'' mi'' |
re''2\trill mi''4 |
fa''8 mi'' fa'' sol'' mi'' fa'' |
sol''4 fa'' mi'' |
re''2\trill mi''4 |
fa''8 mi'' fa'' sol'' mi'' fa'' |
sol''4 fa'' mi'' |
re''8 do'' re'' mi'' fa'' sol'' |
mi''2\trill <>^"Violons seuls" mi''4 |
do'' sol' sol'8 fa' |
mi' sol' do'' mi'' re'' fa'' |
mi'' re'' mi'' fa'' sol'' fa'' |
mi''2\trill <>^"Flûtes seules" mi''8 fa'' |
sol''4 fa''8( mi'') re''( do'') |
si'4\trill re''8 do'' re'' mi'' |
re''4 r r |
r re''8 do'' re'' mi'' |
fa'' mi'' fa'' sol'' fa'' mi'' |
re'' do'' re'' mi'' re'' si' |
do''2 r  |
r8 <>^"Violons" sol'' mi'' sol'' do'' mi'' |
la' re'' re''4 re'' |
re''4 si'\trill r |
re''4. sol''8 fad''4.(\trill mi''16 fad'') |
<< { sol''2*1/4  } \\ { sol'8 <>^"Flûtes seules" si' re''4 do'' } >>
si'8 re'' fa''4 mi'' |
re''4.\trill do''16 si' do''4~ |
do''8 re''16 mi'' fa''4( mi''8.\trill re''16) |
re''4 r r2 | \allowPageTurn \grace s8
R2.*5 |
<>^"Violons" mi''4. re''8 do''4 |
re'' sol' do'' |
si'2\trill do''4 |
re''8 do'' re'' mi'' fa'' re'' |
mi''4 do'' sol'' |
sol''2 fad''4 |
sol''8 fa''16 mi'' re''8 do'' si' re'' |
do'' re'' mi'' fa'' sol''4 |
fa''2\trill mi''4 |
re''2\trill mi''4 |
<>^"Flûtes seules" fa''8 mi'' fa'' sol'' la'' fa'' |
sol''4. fa''8 mi''4 |
re''8 do'' re'' mi'' fa'' mi'' |
re'' sol'' re''4. mi''8 |
si'8 la' si' do'' re'' si' |
do''8^"Violons" re'' mi'' fa'' sol''4 |
sol''2.~ |
sol''8 fa'' mi''4. re''16 do'' |
si'4.\trill re''8 mi'' re'' |
mi'' re'' do''4. re''8 |
si'4.\trill do''8 re'' si' |
do'' mi''16 fa'' sol''4 la'' |
re''8 mi'' fa''4 mi'' |
re''8 do'' si' do'' re'' si' |
do'' re'' re''4.\trill do''8 |
do''2
