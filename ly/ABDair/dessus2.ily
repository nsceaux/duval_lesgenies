\clef "dessus" R2.*5 |
<>^\markup\whiteout "Flûtes seules" mi''4. re''8 do''4 |
re'' sol' r8 do'' |
si'2\trill do''4 |
re''8 do'' re'' mi'' do'' re'' |
mi''4 re'' do'' |
si'2\trill do''4 |
re''8 do'' re'' mi'' do'' re'' |
mi''4 re'' do'' |
do''( si'2)\trill |
do''2 <>^\markup\whiteout "Violons seuls" sol'4 |
sol' mi'' mi''8 re'' |
do'' si' do'' la' si' re'' |
<<
  { s2 <>^\markup\whiteout "Flûtes seules" mi''8 fa'' |
    sol''4 fa''8 mi'' re'' do'' } \\
  { do'' si' do'' re'' mi'' fa'' |
    sol''4 sol'\rest sol'\rest }
>>
si'4 re''8 do'' re'' mi'' |
re'' sol'' fa'' mi'' re'' do'' |
si'4\trill r r |
r si'8 la' si' do'' |
re'' do'' re'' mi'' re'' mi'' |
fa'' mi'' fa'' sol'' fa'' sol'' |
mi''2\trill r |
r8 <>^"Violons" re''8 sol' sol' mi' do'' |
fad' sol' si' si' la'\trill la' |
sol'4 re' r |
la'4. sol'8 la'4.( si'16 do'') |
<>^\markup\whiteout "Flûtes seules" si'8\trill re'' fa''4 mi'' |
re''8 si' re''4 do'' |
si'4.\trill do''16 re'' mi''4~ |
mi''8 fa''16 mi'' re''4( do''8.\trill si'16) |
si'4 r r2 |
R2.*5 |
R2.*4 |
<>^"Violons" mi''4. re''8 do''4 |
re'' sol' do'' |
si'8 la' si' do'' re''4 |
sol'2 do''4 |
si'2\trill do''4 |
fa''2 mi''4 |
<>^\markup\whiteout "Flûtes seules" re''8 do'' re'' mi'' fa'' re'' |
mi''4. re''8 do''4 |
si'8 la' si' do'' re'' do'' si' sol' si'4. do''8 |
re'' do'' re'' mi'' fa'' re'' |
sol''4. <>^\markup\whiteout "Violons" sol''8 fa'' mi'' |
re'' do'' re'' mi'' fa'' sol'' |
mi'' fa'' sol''4.\trill fa''16 mi'' |
re''4.\trill si'8 do'' si' |
do'' re'' mi''4. fa''8 |
sol''2.~ |
sol''8. fa''16 mi''4. re''16 do'' |
si'8 do'' re''4 do'' |
si'8 la' sol' la' si' sol' |
do''4 si'4.\trill do''8 |
do''2
