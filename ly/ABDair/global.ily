\key do \major
\digitTime\time 3/4 \midiTempo#120 s2.*25
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.*3
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#120 s2.*4
\digitTime\time 2/2 \midiTempo#80 s1
\digitTime\time 3/4 \midiTempo#80 \grace s8 s2.*5 \midiTempo#120 s2.*25
\digitTime\time 2/2 \midiTempo#160 s2 \bar "|."
