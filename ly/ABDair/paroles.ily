Quels bruits ! quels doux ac -- cords ! __ quel -- le clar -- té nou -- vel -- le !
L’hor -- reur des ces dé -- serts dis -- pa -- raît à mes yeux !
Quel Dieu des -- cend de la cour im -- mor -- tel -- le,
pour ve -- nir em -- bel -- lir ces lieux ?
Ah ! je le re -- con -- nais à sa dou -- ceur ex -- trê -- me,
c’est l’A -- mour ! et quel Dieu se fait sen -- tir de mê -- me !
