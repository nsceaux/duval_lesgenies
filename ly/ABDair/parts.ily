\piecePartSpecs
#`((dessus #:instrument
           ,#{\markup\center-column { Flûtes Violons }#}
           #:score-template "score-2dessus-bis")
   (dessus1 #:notes "dessus1"
            #:instrument ,#{\markup\center-column { Flûtes Violons }#})
   (dessus2 #:notes "dessus2"
            #:instrument ,#{\markup\center-column { Flûtes Violons }#})
   (dessus2-haute-contre #:notes "dessus2"
                         #:instrument ,#{\markup\center-column { Flûtes Violons }#})
   (basse #:instrument
          ,#{\markup\center-column { [Basses et B.C.] }#}
          #:score-template "score-basse-continue-voix")

   (silence #:on-the-fly-markup , #{

\markup\fontsize#-2 \column {
  \livretPers Zoroastre
  \livretVerse#12 { Quels bruits ! quels doux accords ! quelle clarté nouvelle ! }
  \livretVerse#12 { L’horreur des ces déserts disparaît à mes yeux ! }
  \livretVerse#10 { Quel Dieu descend de la Cour immortelle, }
  \livretVerse#8 { Pour venir embellir ces lieux ? }
  \livretVerse#12 { Ah ! je le reconnais à sa douceur extrême, }
  \livretVerse#12 { C’est l’Amour ! et quel Dieu se fait sentir de même ! }
}
#}))
