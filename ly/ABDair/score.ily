\score {
  \new ChoirStaff <<
    \new GrandStaff \with {
      instrumentName = \markup\center-column { Flûtes Violons }
    } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Zoroastre
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*10\break s2.*9\pageBreak
        s2.*6\break s1 s2.*2\pageBreak
        s2. s1 s2.*4\break s1 s2.*2\pageBreak
        \grace s8 s2.*3\break s2.*8\pageBreak
        s2.*9\break s2.*8
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
