\clef "vbasse" R2.*14 |
r4 r do' |
mi'2. |
R2.*2 |
do'4 re'8[ do'] si[ la] |
sol2.~ |
sol |
sol8 la16 si do'8. la16 re'4 |
si2\trill r4 |
R2. |
r4 r r8 sol |
do'4 mi8 fa16 sol \appoggiatura sol8 la fa16 la re8 re16 re |
re4 r r8 la |
re'8 si sol la16 si do'8 la16 re' |
si4\trill \appoggiatura la8 sol4 si8 do' |
re'4 la8 si la4.\trill sol8 |
sol2 r4 |
R2.*3 |
r4 do'2 mi16 mi fa sol |
\appoggiatura sol8 la8. sol16 fa mi re do sol4 |
mi2\trill r8 sol16 do' |
\appoggiatura si8 la2\trill re'8. mi'16 |
si2\trill si8 do'16 re' |
sol8 do' do'4.( si8) |
do'2 r4 |
R2.*24 |
r2
