\clef "basse" do2~ |
do4 si, fad,2 |
sol,2.~ sol,8*1/4 sol,32 la, si, do re mi fad |
sol16 si re' si sol si re' si sol8 sol, r re |
fa16 fa fa fa fa, fa fa fa do4~ do8*1/4 do32 re mi fa sol la si |
do'4~ do'8*1/4 do'32 si la sol fa mi re do16 mi sol mi do mi sol mi |
do do do do do, do do do sol, sol, sol, sol, sol, sol, sol, sol, |
sol,4 r8 do re16 re re re re re re re |
re4~ re8*1/4 re32 mi fad sol la si do' re'16 la re' la fad la fad re |
sol4~ sol8*1/4 sol,32 la, si, do re mi fad sol16 re sol re si, re si, sol, |
do4~ do8*1/4 do'32 si la sol fa mi re do16 do do do do do do do |
sol8*1/4~ sol32 fad mi re do si, la, sol,16 sol si re' sol16 fad!32 mi re do si, la, sol,16 sol si re' |
sol16 sol sol sol sol, sol, sol, sol, sol sol sol sol sol, sol, sol, sol, |
re8*1/2 dod32 si, la, sol, fa, mi, re,16 re fa la re8*1/2 dod32 si, la, sol, fa, mi, re,16 re fa la |
re4~ re8*1/4 re32 mi fa sol la si dod' re'4 r8 sol |
la16 la la la la, la, la, la, re re re re do do do do |
si, sol, si, re sol sol, sol, sol, sol sol sol sol fa fa fa fa |
mi do mi sol do' do do do sib sib sib sib la la la la |
sol re sol re si, re si, sol, do do, do, do, fa,8 sol, |
do,1 do2 |
fa1 mi4 do |
sol2 sol, |
do2. | \allowPageTurn
<>^"[B.C.]" si,2 la, |
sol,8 fad, sol, la, si, sol, |
do4 la, re8 do |
si,4. la,8 si,4 |
do8 re mi do re re, |
sol, re sol fa mi do |
fa mi re2 |
mi4. fad8 sold mi |
la sold la do' si la |
sold2 mi4 |
la8 re mi4 mi, |
la,2. |
re4 re, re |
mi2 do4 |
fa8 sol fa mi re do |
sol4 sol,2 |
do8 si, la,2 |
mi la,4 |
mi,2 mi4 |
la4. sol8 fa8. mi16 |
re4 sib8 sol la la, |
re2 re8 mi |
fa2. sol4 la4. re8 |
la,4 la sol |
fa2 fad |
sol2 mi4 |
fa do' do |
fa4 la8 sib la sol |
fad mi re mi fad re |
sol2. |
sib2 sol4 |
la2 r16 la, si, dod |
re2. |
re'4 re2 |
la2 dod'4 |
re' sib2 |
la8 sol fad mi fad re |
sol la sol fa sol mi |
la2 dod4 |
re8 mi fa sol la fa |
sib2 fa8 sol |
la4 la,2 |
re4. re'8 dod' la |
re'4 sib2 |
la8 sol fad mi fad re |
sol la sol fa sol mi |
la2 dod4 |
re8 mi fa sol la fa |
sib2 fa8 sol |
la4 la,2 |
re4. do8 si, la, |
\once\set Staff.whichBar = "|"
sol,2
