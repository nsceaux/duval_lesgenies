\clef "dessus"
<<
  \tag #'dessus1 {
    sol'4. sol'8 |
    la'4 si' do''2 |
    si'16 re'' si' re'' sol' si' sol' re' si si do' re' mi' fad' sol' la' |
    si' re'' sol'' re'' si' sol' si' sol' re'' re'' re'' re'' re'' re'' re'' re'' |
    do'' do'' fa'' do'' la' fa' la' fa' do'' do'' do'' do'' do'' do'' do'' do'' |
    do'' sol'' do''' sol'' mi'' do'' mi'' do'' sol' sol' sol' sol' sol' sol' sol' sol' |
    sol'4~ sol'8*1/4 sol'32 la' si' do'' re'' mi'' fa'' sol''16 si'' re''' si'' sol'' re'' sol'' re'' |
    re'' re'' re'' re'' mi'' mi'' mi'' mi'' re'' re'' re'' re'' re'' re'' re'' re'' |
    re'' la' re'' la' fad' la' re'' la' la' la' la' la' do'' do'' do'' do'' |
    si' re'' sol'' re'' si' re'' sol'' re'' re'' re'' re'' re'' fa'' fa'' fa'' fa'' |
    mi''4~ mi''8*1/4 mi''32 re'' do'' si' la' sol' fa' mi'16 sol' do'' sol' mi' sol' do'' sol' |
    re' re' re' re' re' re' re' re' re' re'' sol'' re'' si' re'' sol'' re'' |
    re'' re'' re'' re'' re' re' re' re' re'' sol'' re'' sol'' si' re'' si' re'' |
    la' re'' re'' la' fa' la' re'' la' fa' re'' re'' la' fa' la' re'' la' |
    fa'4~ fa'8*1/4 fa'32 sol' la' si' do'' re'' mi'' fa''16 re'' fa'' re'' sib'' sol'' sib'' sol'' |
    mi'' la'' mi'' la'' dod'' mi'' dod'' mi'' la' la'' fad'' la'' re'' fad'' re'' fad'' |
    sol''4~ sol''16 sol' si' re'' sol'' re''' si'' re''' sol'' si'' la'' si'' |
    do'''4~ do'''16 do'' mi'' sol'' do''' do''' do''' do''' do''' do''' do''' do''' |
    re''' do''' si'' la'' sol'' fa'' mi'' re'' do'' do'' do'' do'' do'' do'' do'' si' |
    do''4 do'' do'' do'' do'' do'' |
    re'' re'' re'' re'' mi'' mi'' |
    re''2 re''4.\trill do''8 |
    do''2 r4 |
  }
  \tag #'dessus2 {
    mi'4. mi'8 |
    fad'4 sol' la'2 |
    re'4~ re'8*1/4 re'32 mi' \ficta fad' sol' la' si' do'' re''16 sol'' re'' si' sol' si' sol' si' |
    re'' re'' re'' re'' re'' re'' re'' re'' si'8 sol' r la' |
    la'16 la' la' la' la' la' la' la' sol'4~ sol'8*1/4 mi'32 fa' sol' la' si' do'' re'' |
    mi''16 mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' do'' mi'' sol'' do''' sol'' do''' sol'' |
    mi'' do'' mi'' sol'' mi'' do'' mi'' sol'' re''4~ re''8*1/2 mi''32 fad'' sol'' la'' si'' do''' |
    re'''16 si'' re''' si'' do''' sol'' do''' do''' la'' la'' re''' la'' la'' fad'' re'' la' |
    fad'4~ fad'8*1/4 fad'32 sol' la' si' do'' re'' mi'' fad''16 fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    sol'' sol' sol' sol' sol' sol' sol' sol' sol' si' re'' sol'' re'' si' sol' si' |
    do'' do' do' do' do' do' do' do' do' mi' sol' do'' do' mi' sol' do'' |
    si' re'' sol'' re'' si' re'' sol'' re'' si' re'' si' sol' re'( re') re'( mi'32 fad') |
    sol'16 re' re' re' sol' si' re'' si' sol' si' sol' si' re'' re'' re'' re'' |
    re'' la' la' la' re'' fa'' la'' fa'' re'' la' la' la' re'' fa'' la'' fa'' |
    re'' la'' re''' la'' fa'' la'' re''' la'' la'' fa'' la'' fa'' sol'' mi'' sol'' mi'' |
    dod'' mi'' dod'' mi'' la' dod'' la' dod'' re'' re' re' re' re' re' re' re' |
    re' re'' sol'' re'' si' re'' si' sol' re' re'' re'' re'' re'' re'' re'' re'' |
    mi'' sol'' do''' sol'' mi'' sol'' mi'' do'' sol' mi'' do'' mi'' fa'' fa'' fa'' fa'' |
    fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' mi'' mi'' mi'' mi'' re'' re'' re'' re'' |
    do''4 do'' mi' mi' mi' mi' |
    re' re' si si do' do' |
    re'2 re'4.( mi'16 fa') |
    mi'2\trill r4 |
  }
>> \allowPageTurn
R1 R2.*21 R1. R2. R1 R2.*26
