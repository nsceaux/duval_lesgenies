\key do \major
\once\omit TimeSignature \midiTempo#80 \partial 2 s2
\time 4/4 s1*18
\time 3/2 \midiTempo#160 s1.*2
\digitTime\time 2/2 s1
\digitTime\time 3/4 \midiTempo#80 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.*21
\time 3/2 s1.
\digitTime\time 3/4 \grace s8 s2.
\digitTime\time 2/2 s1
\digitTime\time 3/4 s2.*26 \bar "|."
