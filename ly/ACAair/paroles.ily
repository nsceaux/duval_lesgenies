\tag #'complet {
  Tout o -- bé -- it, tout s’é -- veille à ta voix !
  Tu dé -- chai -- nes les vents, tu fais trem -- bler __ la ter -- re !
  Tu sou -- lè -- ves les flots, tu lan -- ces le ton -- ner -- re ;
}
mais l’A -- mour seul ne con -- naît point __ tes lois.

Tout re -- con -- naît vo -- tre pou -- voir su -- prê -- me,
ré -- gnez, __ tri -- om -- phez Dieu char -- mant ;
il n’est point de plus doux mo -- ment,
que l’ins -- tant où l’on dit qu’on ai -- me.
Il n’est point de plus doux mo -- ment,
que l’ins -- tant où l’on dit qu’on ai -- me.

Qui vous a -- mène en ces dé -- serts ?
À de nou -- veaux su -- jets je viens don -- ner des fers.
Peu -- ples des é -- lé -- ments, con -- nais -- sez ma puis -- san -- ce ;
je rè -- gne sur tout l’u -- ni -- vers,
é -- prou -- vez en ce jour les traits que l’A -- mour lan -- ce ;
les maux qu’ils font, doi -- vent ê -- tre plus chers
que les biens de l’in -- dif -- fé -- ren -- ce,
les maux qu’ils font, doi -- vent ê -- tre plus chers
que les biens de l’in -- dif -- fé -- ren -- ce.
Ac -- cou -- rez Jeux char -- mants, vo -- lez ten -- dres A -- mours,
for -- mez les plus ga -- lan -- tes Fê -- tes ;
quand on ai -- me, tout âge est l’â -- ge des beaux jours.
Plai -- sirs, lan -- cez mes traits, é -- ten -- dez mes con -- quê -- tes.
