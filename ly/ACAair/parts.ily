\piecePartSpecs
#(let ((livret #{
\markup\fontsize#-2 \fill-line {
  \column {
    \livretPers L'Amour
    \livretVerse#10 { Tout obéit, tout s’éveille à ta voix ! }
    \livretVerse#12 { Tu déchaines les Vents, tu fais trembler la Terre ! }
    \livretVerse#12 { Tu soulèves les Flots, tu lances le Tonnerre ; }
    \livretVerse#10 { Mais l’Amour seul ne connaît point tes lois. }
    \livretPers Zoroastre
    \livretVerse#10 { Tout reconnaît votre pouvoir suprême, }
    \livretVerse#8 { Régnez, triomphez Dieu charmant ; }
    \livretVerse#8 { Il n’est point de plus doux moment, }
    \livretVerse#8 { Que l’instant où l’on dit qu’on aime. }
  }
  \column {
    \livretPers L'Amour
    \livretVerse#8 { Qui vous amène en ces déserts ? }
    \livretVerse#12 { À de nouveaux Sujets je viens donner des fers. }
    \livretVerse#12 { Peuples des Éléments, connaissez ma puissance ; }
    \livretVerse#8 { Je règne sur tout l’Univers, }
    \livretVerse#12 { Éprouvez en ce jour les traits que l’Amour lance. }
    \livretVerse#10 { Les maux qu’ils font, doivent être plus chers }
    \livretVerse#8 { Que les biens de l’indifférence. }
  }
}
#}))
       `((dessus #:instrument "Violons" #:score-template "score-2dessus")
         (dessus1 #:instrument "Violons")
         (dessus2 #:instrument "Violons")
         (dessus2-haute-contre #:notes "dessus"
                               #:instrument "Violons"
                               #:tag-notes dessus2)
         (basse #:score-template "score-basse-continue-voix"
                #:instrument
                ,#{ \markup\center-column { [Basses et B.C.] } #})
         (flute-hautbois #:on-the-fly-markup ,livret)
         (silence #:on-the-fly-markup ,livret)))
   