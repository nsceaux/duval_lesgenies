\score {
  \new StaffGroupNoBar <<
    \new GrandStaff \with { instrumentName = "Violons" \haraKiri } <<
      \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
      \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character L'Amour
    } \withLyrics <<
      \global \keepWithTag #'complet \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion {
        s2 s1*18 s1.*2 s1 s2.\break
      }
      \origLayout {
        s2 s1*3\break s1*2\break s1*2\pageBreak
        s1*2\break s1*2\break s1*2\break s1*2\pageBreak
        s1*2\break s1 s1. s1 \bar "" \break s2 s1 s2. s1\pageBreak
        s2.*6\break s2.*6 s2 \bar "" \break s4 s2.*5\pageBreak
        s2.*3 s2. \bar "" \break s2. s2. s1 s2.*2\break s2.*6\pageBreak
        s2.*7\break \grace s8 s2.*6 s2 \bar "" \break s4 s2.*4
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
