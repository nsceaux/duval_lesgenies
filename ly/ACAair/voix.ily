\clef "vbas-dessus" <<
  \tag #'basse { r2 R1*18 }
  \tag #'complet {
    do''4 mi'8. sol'16 |
    re'4 r8 sol'16 sol' re''4. re''16 re'' |
    re''2 r4 r8 sol'16 sol' |
    re'2~ re'16[\melisma mi' re' mi'] fa'[ sol' fa' sol'] |
    la'[ si' la' si'] do''[ re'' do'' re''] mi''4\melismaEnd mi''8 mi'' |
    mi''1 |
    r4 r8 sol' si'4 re'' |
    si'8.[\melisma re''16] do''[ si' la' sol']( fad'4.)\trill\melismaEnd la'8 |
    re'2 re' |
    R1*2 |
    r4 sol'8. sol'16 si'8.[\melisma sol'16 si'8. re''16] |
    si'8.[ sol'16 si'8. re''16] si'16[ sol' si' re''] si'[ sol' si' sol']( |
    fa''4)\melismaEnd fa''8 fa'' fa''2 |
    R1 |
    r2 r4 r8 re'' |
    sol''4~ sol''8*1/2[\melisma fa''32 mi'' re'' do'' si' la'] sol'4~ sol'8*1/2[ la'32 si' do'' re'' mi'' fa''] |
    sol''4~ sol''16\melismaEnd sol'' sol'' sol'' sol''4\melisma la''32[ sol'' fa'' mi'' re'' do'' si' la']( |
    si'4)\trill\melismaEnd \appoggiatura la'8 sol'4 r2 |
  }
>>
r2 do''2 mi''4 sol'' |
si'1 sol'4 sol'8 do'' |
do''2( si'4.)\trill do''8 |
do''2
\ffclef "vbasse" <>^\markup\character Zoroastre mi8 re16 do |
sol4 sol8 la16 si do'8. la16 re'4 |
si2\trill r8 re' |
mi'8[ re' do' si la si16 do']( |
re'4.) fad8 sol4 |
mi do re |
sol,2 r8 si16 do' |
\appoggiatura si8 la4 si8 do' si la |
sold4.\trill la8 si4 |
do'2 re'8 mi' |
\appoggiatura mi'8 re'2 r8 mi' |
do'4( si2*1/2)(\trill \afterGrace s4 la8) |
la2 do'8 mi |
fa4 fa8 sol la si |
\appoggiatura si8 do'2 re'8 mi' |
la4 la8 la si do' |
do'2( si4) |
do'2 r16 mi mi la |
sold4.\trill sold8 sold la |
\appoggiatura la8 si2. |
\ffclef "vbas-dessus" <>^\markup\character L'Amour
r8 dod'' dod'' dod'' re''8. mi''16 |
\appoggiatura mi''8 fa''8. la'16 re''8. mi''16 dod''8.\trill re''16 |
re''2. |
la'4 la'16 fa' la' do'' fa''4 mi''8 sol'' dod''4\trill dod''8 re'' |
\appoggiatura re''8 mi''4 mi'' r8 la' |
re''4~ re''32[\melisma mi'' fa'' mi'' re'' do'' si' la'] re''16[ do'' sib' la' sol' fa' mi' re']( |
sib'2)\melismaEnd la'8 sol' |
la'4 sol'4.\trill fa'8 |
fa'2 la'8 si' |
\appoggiatura si'8 do''4 la'4. re''8 |
si'2\trill r8 sol' |
mi''4~ mi''8. fa''16 sol''[ fa''] mi''[ re''] |
dod''[\melisma re'' dod'' re''] mi''[ re'' dod'' si'] la'[ si' la' sol'] |
fa'4. sol'16[ la' si' do'' re'' mi'']( |
fa''4 fa''2\trill)\melismaEnd |
mi''2 r8 la' |
fa'8.[ sol'16] sol'4.\trill( fa'16[ sol']) |
la'2 la'8 re'' |
\appoggiatura do''8 si'4 dod''4. re''8 |
dod''2 mi''8 sol'' |
fa''2. |
\appoggiatura fa''8 mi''4. \appoggiatura re''8 dod'' re'' mi'' |
fa''4( mi''2)\trill |
re''2 r8 la' |
fa'8.[ sol'16] sol'4.(\trill fa'16[ sol']) |
la'2 la'8 re'' |
\appoggiatura do''8 si'4 dod''4. re''8 |
dod''2\trill mi''8 sol'' |
fa''2. |
\appoggiatura fa''8 mi''4. \appoggiatura re''8 dod'' re'' mi'' |
fa''4( mi''2)\trill |
re''2. |
