\clef "basse" sol8 la |
si4 do'2 |
re'4 do'8 si la sol |
fad re mi fad sol4 |
do re re, |
sol8 re sol,4 sol8 la |
si4 do'2 |
re'4 do'8 si la sol |
fad4. sol8 fad mi |
re la re' do' si la |
sol4. fad16 mi re8 do |
si,2 r4 |
\clef "alto" r4 r r8 re' |
sol'4. fad'16 mi' re'8 do' |
\clef "bass" si la sol fa mi re |
do si, do re mi do |
re2 fad,4 |
sol, sol2 |
la8 si fad4. sol8 |
re do re mi fad re |
sol la sol fa mi re |
do4 do' si |
la8 sol fad4. sol8 |
re4 re8 dod re mi |
fad mi re mi fad re |
sol4. fad8 sol la |
si la sol la si sol |
do'4 r do' |
si8 la sol la si sol |
do' sol mi sol do4 |
do'8 si la si do' la |
re'4 r re' |
dod'2 la4 |
re8 la re' do' si sol |
do'4. re'8 mi'4 |
re'8 re sol la si sol |
do'2 si4 |
la la, r |
la la, la |
re' re re' |
dod'2 la4 |
re2.~ |
re |
do2 si,8 sol, |
do si, la,4 sol, |
re re,2 |
sol,2. |
