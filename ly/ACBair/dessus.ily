\clef "dessus" si'8 do'' |
re''4 la'4. si'8 |
fad'2\trill r8 sol' |
do''4. si'16 la' si'4 |
do'' la'4.\trill sol'8 |
sol'2 r4 |
R2.*4 |
r4 r r8 re'' |
sol''4. fad''16 mi'' re''8 do'' |
si'4. la'16 sol' fad'8 la' |
sol'2 r8 re'' |
sol''4. sol'8 la' si' |
do'' re'' do'' si' la' sol' |
fad'4 fad''8 sol'' la''4 |
re''2 sol''4 |
fad''8 sol'' la''4. sol''8 |
fad'' sol'' fad'' sol'' la''4 |
re''4. sol'8 la' si' |
do'' re'' mi'' fad'' sol''4 |
fad''8 sol'' do''' re''' do''' si'' |
la'' sol'' fad'' mi'' fad'' sol'' |
la''4. sol''8 fad'' mi'' |
re'' mi'' re'' do'' si' la' |
sol'4. re''8 sol'' sol'' |
sol''2~ sol''16 la'' sol'' la'' |
si''4 si''8 do''' re''' si'' |
do'''2 do'''8 do''' |
do'''4 do''~ do''16 mi'' do'' mi'' |
la'4 r \twoVoices #'(dessus1 dessus2 dessus) <<
  { la''4 |
    la'' sol''8 fad'' sol'' la'' |
    la''4 fad''16 re'' mi'' fad'' }
  { la''4 |
    mi''~ mi''16 fad'' mi'' re'' dod'' re'' mi'' dod'' |
    re''4 re'16 re'' mi'' fad'' }
>> sol''8 sol'' |
sol'' fad'' mi''4.( fad''16 sol'') |
fad''4\trill \appoggiatura mi''8 re''4 r8 si' |
do'' re'' mi'' fad'' sol''4~ |
sol''16 fad'' mi'' re'' do'' re'' do'' si' la' si' la' si' |
do''4~ do''16 si' do'' re'' mi''4 |
la' r \twoVoices #'(dessus1 dessus2 dessus) <<
  { la''4 |
    la'' sol''8 fad'' sol'' fad'' |
    la'' sol'' fad''4 }
  { la''4 |
    mi''~ mi''16 fad'' mi'' re'' dod'' re'' mi'' dod'' |
    re''4 la' }
>> r4 |
r8 \twoVoices #'(dessus1 dessus2 dessus) <<
  { re''8 fad''4 la'' |
    re'''2.~ |
    re'''4 do''' si'' |
    si''( la''2)\trill |
    sol''2. | }
  { fad'8 la'4 re'' |
    fad''2 sol''8 sol'' |
    sol'' sol' do''4 re'' |
    re''2. |
    si'\trill | }
>>
