Ac -- cou -- rez jeux char -- mants, vo -- lez ten -- dres a -- mours,
vo -- lez, vo -- lez, __ vo -- lez, __
vo -- lez, vo -- lez, vo -- lez __ ten -- dres a -- mours,
for -- mez les plus ga -- lan -- tes fê -- tes ;
quand on ai -- me, tout âge est l’â -- ge des beaux jours.
Plai -- sirs, lan -- cez mes traits,
plai -- sirs, lan -- cez __ mes traits,
plai -- sirs, lan -- cez __ mes traits,
é -- ten -- dez mes con -- quê -- tes.
Plai -- sirs, lan -- cez __ mes traits,
lan -- cez mes traits, __
é -- ten -- dez mes con -- quê -- tes.
