\piecePartSpecs
#`((dessus #:instrument ,#{ \markup\center-column { [Violons, Hautbois] } #})
   (dessus1 #:instrument ,#{ \markup\center-column { [Violons, Hautbois] } #})
   (dessus2 #:instrument ,#{ \markup\center-column { [Violons, Hautbois] } #})
   (dessus2-haute-contre
    #:notes "dessus" #:tag-notes dessus2
    #:instrument ,#{ \markup\center-column { [Violons, Hautbois] } #})
   (basse #:instrument ,#{ \markup\center-column { [Basses, Bassons et B.C.] } #})

   (silence #:on-the-fly-markup , #{
\markup\fontsize#-2 \column {
  \livretPers L'Amour
  \livretVerse#12 { Accourez Jeux charmants, volez tendres Amours, }
  \livretVerse#8 { Formez les plus galantes Fêtes ; }
  \livretVerse#12 { Quand on aime, tout âge est l’âge des beaux jours. }
  \livretVerse#12 { Plaisirs, lancez mes traits, étendez mes conquêtes. }
}#}))
