\score {
  \new ChoirStaff <<
    \new Staff \with {
      instrumentName = \markup\center-column { [Violons, Hautbois] }
    } << \global \includeNotes "dessus" >>
    \new Staff \with {
      instrumentName = \markup\character L'Amour
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses, Bassons et B.C.] }
    } <<
      \global
      \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s2.*7 s2 \bar "" \break s4 s2.*6\pageBreak
        s2.*7\break s2.*7\pageBreak
        s2.*7\break s2.*6\pageBreak
        s2.*4
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
