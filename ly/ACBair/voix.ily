\clef "vdessus" r4 |
R2.*4 |
r4 r si'8 do'' |
re''4 la'4. si'8 |
fad'2\trill r8 sol' |
do''4. si'8 la' sol' |
la'4. la'8 si' do'' |
re''2.~ |
re''2\trill r8 re'' |
sol''4.\melisma fad''16[ mi''] re''8[ do'']( |
si'4.)\trill\melismaEnd la'8 si' do'' |
re''8. re''16 si'16[\melisma la' sol' la'] si'[ do'' re'' si']( |
mi''4.)\melismaEnd re''8 do'' si' |
la'2\trill re''4 |
si'2\trill si'4 |
do''8 re'' do''4.\trill si'8 |
la'4\trill la' r8 re''16 do'' |
si'2\trill do''8 re'' |
\appoggiatura re''8 mi''2 re''4 |
do''8 si' la'4. sol'8 |
fad'2\trill r8 la' |
re''4 la' si'8 do'' |
si'2\trill r8 re'' |
sol''2 re''4 |
mi''8.[\melisma re''16] do''[ re'' do'' re''] mi''[ fa'' mi'' fa'']( |
sol''2)\melismaEnd re''4 |
mi''2 r8 mi'' |
la''2 mi''4 |
fad''8.[\trill\melisma mi''16] re''[ mi'' re'' mi''] fad''[ sol'' fad'' sol'']( |
la''2)\melismaEnd mi''4 |
fad''2 re''8 si' |
mi''([ re'']) do''([ si']) la'([ sol']) |
re''4 si'\trill r8 re'' |
mi''2 re''4 |
do''16[\melisma re'' do'' si'] la'[ si' la' si'] do''[ re'' do'' re''] |
mi''4~ mi''16[ fa'' mi'' re''] do''[ mi'' re'' mi''] |
fad''[ sol'' fad'' mi''] re''[ mi'' re'' mi''] fad''[ sol'' fad'' sol'']( |
la''2)\melismaEnd mi''4 |
fad''8\trill la' re''4 fad'' |
la''2.~ |
la''2 re''8 re'' |
mi''4 fad'' sol'' |
sol''2( fad''4) |
sol''2. |
