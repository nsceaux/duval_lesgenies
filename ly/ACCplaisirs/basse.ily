\clef "basse" sol,4 sol4. la8 |
si4 sol do' |
si la sol |
re' do'8 si la sol |
fad4 re2 |
sol sol,4 |
sold2 mi4 |
la fad re |
sol2 sol4 |
fad mi re |
sol, la,2 |
re8 do re mi fad re |
re2. |
re'4 re'4. si8 |
do' si la si do' la |
si2. |
la4 sol fad |
mi4. fad8 sol4 |
la si si, |
mi4 mi8 red mi fad |
sol4 sol4. la8 |
si4 do' la |
mi fad2 |
sol4. la8 si4 |
do'4 la sol |
re' re si, |
mi fad sol |
re re' do' |
si sol4. la8 |
si4 sol do' |
si la4. sol8 |
re'4 do'8 si la sol |
fad2 re4 |
sol si sol |
do' dod' la |
re' do' si |
do' re' re |
\once\tieDashed sol2.~ |
\once\set Staff.whichBar = "|" sol4. fa8 mib re |
