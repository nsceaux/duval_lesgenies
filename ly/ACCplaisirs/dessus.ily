\clef "dessus" sol'4 si'4.\trill do''8 |
re''4 si' mi'' |
re'' do''4.\trill si'8 |
la'2 re''4 |
do'' si' la' |
si'4.\trill do''8 re''4 |
mi''4. fad''8 \appoggiatura mi'' re''4 |
dod''\trill la'' \appoggiatura sol''8 fad''4 |
si'' \appoggiatura la''8 sol''4 \appoggiatura fad''8 mi''4 |
la'' dod''\trill re'' |
mi'' mi''4.\trill re''8 |
re''2. |
re'' |
fad''4 fad''4. sol''8 |
mi''4\trill la'' sol'' |
fad''8 si'' la'' sol'' fad'' mi'' |
red''4\trill mi'' fad'' |
sol''4. la''8 si''4 |
la''8\trill sol'' fad''4.\trill mi''8 |
mi''2. |
si'4 si'4.\trill do''8 |
re''4. mi''8 fad''4 |
sol'' fad''8 mi'' re'' do'' |
si'4.\trill do''8 re''4 |
mi'' fad'' sol'' |
la'' \appoggiatura sol''8 fad''4 si'' |
\appoggiatura la''8 sol''4 do'''4. si''8 |
la''2\trill re'''4 |
sol' si'4.\trill do''8 |
re''4 \appoggiatura do''8 si'4 mi'' |
re'' do''4.\trill si'8 |
la'2 re''4 |
do'' si' la' |
si' sol''4. re''8 |
mi''4 la''4. mi''8 |
fad''4.\trill( mi''16 fad'') sol''4 |
la'' la''4. sol''8 |
sol''2. |
