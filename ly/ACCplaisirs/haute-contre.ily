\clef "haute-contre" re'4 sol'4. fad'8 |
sol'4 si' la' |
si' fad'4. sol'8 |
fad'2\trill la'4 |
la' sol' fad' |
sol'4. la'8 si'4 |
si'2 si'4 |
la'2 \appoggiatura si'8 la'4 |
sol' \appoggiatura fad'8 mi'4 \appoggiatura re'8 dod'4 |
re' mi' fad' |
si' la' sol' |
fad'2.\trill |
fad'\trill |
re''4 re''4. re''8 |
do''4 do'' mi' |
fad'2. |
la'2 si'4 |
si'2 si'4 |
do'' si' la' |
sol'2. |
sol'4 sol'4. fad'8 |
sol'2 fad'4 |
mi' re'8 do' si la |
sol2 sol'4 |
sol' fad'8 mi' re' mi' |
fad'4 \appoggiatura mi'8 re'4 sol'~ |
sol' la' re'' |
re''2 fad'4 |
sol' sol'4. fad'8 |
sol'4 si' la' |
si' fad'4. sol'8 |
fad'2\trill la'4 |
la' sol' fad' |
sol' re''4. si'8 |
do''4 mi'' mi'' |
re''2 re''4 |
mi'' re'' do'' |
si'2.\trill |
