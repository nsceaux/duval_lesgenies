\clef "taille" si4 re'4. re'8 |
re'2 sol'4 |
sol' la' re' |
re' mi'8 re' do' si |
la4 re' re' |
re'2. |
si2 mi'4 |
mi' re' do' |
si mi la |
la2 re'4 |
re'2 dod'4 |
re'8 mi' fad' mi' re' do'! |
re'2. |
la'4 la'4. sol'8 |
sol'4 mi' mi' |
red'4.\trill dod'8 red' mi' |
fad'4 dod' red' |
mi'2 mi'4 |
mi'2 red'4\trill |
mi'2. |
re'4 re'4. re'8 |
re'4 do' do' |
do' la re' |
re'2 re'4~ |
re' do' si |
la2 re'4 |
re'2 sol'4 |
fad'2\trill re'4 |
re' re'4. re'8 |
re'2 sol'4 |
sol' la'4. re'8 |
re'4 mi'8 re' do' si |
la4 re' re' |
re'2 sol'4 |
sol' mi' la' |
la'2 sol'4 |
sol'2 fad'4\trill |
sol'2. |
