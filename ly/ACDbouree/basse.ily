\clef "basse"
\setMusic #'rondeau {
  r do8 re mib fa sol do |
  lab4 do sol do |
  do' sib lab fa |
  sol8 fa mib re do4 r |
  r do8 re mib fa sol do |
  lab4 do sol do |
  fa,2 sol, |
  do
}
do2 do,4 r |
\keepWithTag #'() \rondeau do,4 r |
do2 r4 mib' |
mib2 r4 mib8 fa |
sol lab sol lab sib4 sib, |
sib, sib,8 do re mib re mib |
fa4 fa,8 sol, lab, sib, lab, sib, |
do4 do' do' si |
do' do2 si,4 |
do mib fa lab |
sol fa mib8 fa sol sol, |
do2 do,4 r |
\rondeau
