\clef "dessus"
\setMusic #'rondeau {
  do''8 re'' mib'' fa'' sol'' do'' |
  lab''4 do'' sol'' do'' |
  do''' do''8 re'' mib'' fa'' sol'' do'' |
  lab''4 sol''8 lab'' fa'' sol'' mib'' fa'' |
  re'' sol' do'' re'' mib'' fa'' sol'' do'' |
  lab''4 do'' sol'' do'' |
  do''' do''8 re'' mib'' fa'' sol'' mib'' |
  lab'' sol'' fa'' mib'' re''4.\trill do''8 |
}
r4 \keepWithTag #'() \rondeau
do''4 do''8 re'' mib'' fa'' sol'' do'' |
do''2 r4 mib''8 fa'' |
sol''4 sol' sol' sol''8 lab'' |
sib''4 sib' sib' re''8 mib'' |
fa''4 fa' fa' fa''8 sol'' |
lab''4 lab' lab' do''8 re'' |
mib'' sol'' fa'' mib'' fa'' lab'' sol'' fa'' |
mib'' sol'' fa'' mib'' fa'' lab'' sol'' fa'' |
mib'' sol'' fa'' mib'' re'' mib'' do'' re'' |
si' do'' re'' si' sol'4 sol' |
r4 \rondeau do''2
