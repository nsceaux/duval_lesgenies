\clef "haute-contre"
\setMusic #'rondeau {
  mib'8 fa' sol' fa' mib' sol' |
  fa'4 sol' do'' mib' |
  fa' mib'8 fa' sol' fa' mib' sol' |
  fa'4 sib' do'' do'' |
  si'4\trill mib'8 fa' sol' fa' mib' sol' |
  fa'4 sol' do'' mib' |
  fa' mib'8 fa' sol' fa' mib' sol' |
  fa' mib' re' do' do'4 si\trill |
}
r4 \keepWithTag #'() \rondeau
do'4 mib'8 fa' sol' fa' mib' sol' |
do'2 r4 sib' |
sib'2 r4 sib'8 lab' |
sol'2 r4 sib'8 do'' |
re''4 r r re'' |
do''2 r |
r4 sol' fa' sol'~ |
sol' re'2 sol'4~ |
sol' sol' lab' re' |
re'8 do' si re' mib'4 re'\trill |
do' \rondeau do'2
