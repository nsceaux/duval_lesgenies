\clef "taille"
\setMusic #'rondeau {
  r4 mib8 fa sol fa mib sol |
  fa4 do' do' do' |
  do' re' do' lab |
  sol r r2 |
  r4 mib8 fa sol fa mib sol |
  fa4 do'2 do'4~ |
  do' lab sol fa |
}
R1 |
\keepWithTag #'() \rondeau
mib4 r r2 |
mib2 r4 sol'8 fa' |
mib'2 r4 mib' |
mib'2 r4 fa'8 mib' |
re'4 re'8 mib' fa' sol' lab' sol' |
fa'2 r |
r4 mib' re' re' |
do' lab2 re'4 |
do' do' do' do' |
re'8 mib' fa' re' do'4 si\trill |
do'4 r r2 |
\rondeau |
mib2
