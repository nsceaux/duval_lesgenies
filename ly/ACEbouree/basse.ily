\clef "basse" do'4 do |
si si, la la, |
sol sol, mi8 re do re |
mi4 do fa8 sol la fa |
sol la si sol do'4 do |
si si, la la, |
sol sol, sol8 la si do' |
si4 la8 sol do4 re |
sol,2 sol4 do |
sol8 la sol fa mi fa mi re |
do sol la si do'4 fa |
do'4. sib8 la4 fa |
sol8 la sol fa mi4 r |
fa r mi r |
fa r mi r |
fa r sol r |
la mi fa r |
mi r fa do |
fa,2 sol, |
do2
