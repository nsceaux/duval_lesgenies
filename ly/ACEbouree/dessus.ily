\clef "dessus" mi''8 sol'' fa'' mi'' |
re'' fa'' mi'' re'' do'' mi'' re'' do'' |
re''4 sol' do''8 re'' mi'' fa'' |
sol''4 fa''8 mi'' la'' sol'' fa'' mi'' |
re''2 mi''8 sol'' fa'' mi'' |
re'' fa'' mi'' re'' do'' mi'' re'' do'' |
re''4 sol' si'8 do'' re'' mi'' |
re'' mi'' fad'' sol'' la'' sol'' fad'' la'' |
sol''2 si'8 do'' re'' mi'' |
re'' do'' si' la' sol'4 la'8 si' |
do''4 do' mi''8 fa'' sol'' la'' |
sol''4 fa''8 mi'' fa'' mi'' re'' do'' |
si'4\trill sol' do''8 mi'' re'' do'' |
re'' fa'' mi'' re'' do'' mi'' re'' do'' |
re'' fa'' mi'' re'' do'' sol'' fa'' mi'' |
la'' sol'' fa'' re'' si'' la'' sol'' mi'' |
do''' si'' la'' sol'' la'' si'' la'' sol'' |
do''' si'' la'' sol'' la'' si'' la'' sol'' |
la'' sol'' fa'' mi'' re''4.\trill do''8 |
do''2
