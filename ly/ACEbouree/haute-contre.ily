\clef "haute-contre" sol'4 la'~ |
la' sol'2 fad'4\trill |
sol'2 sol'4 sol' |
sol' la'8 sol' la' si' do'' la' |
si'2 sol'4 la'~ |
la' sol'2 fad'4\trill |
sol'2 re''8 do'' si' la' |
sol'4 la'8 si' do'' si' la' do'' |
si'2\trill sol'4 sol' |
sol'2 sol' |
sol'4 mi' sol' fa' |
mi'8 mi'' re'' do'' re'' do'' si' la' |
sol'2 sol'4 r |
fa'4 r sol' r |
fa' r sol' r |
re' r re' mi' |
fa' sol' fa' r |
sol' r do'8 re' do' si |
do'4 la' sol' fa' |
mi'2\trill
