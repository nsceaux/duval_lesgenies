\piecePartSpecs
#`((dessus #:instrument
           ,#{\markup\center-column { [Violons et Hautbois] }#})
   (dessus2-haute-contre)
   (taille)
   (parties)
   (basse #:instrument
          , #{ \markup\center-column { [Basses, Bassons et B.C.] } #})
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
