\clef "taille" mi'2 |
fa'4 sol'8 fa' mi' re' do'4 |
si2 do'4 do' |
do'2 re'4 re' |
re' sol mi'2 |
fa'4 sol'8 fa' mi' re' do'4 |
si2 re'4 re' |
re'2 mi'4 la |
sol2 sol8 la si do' |
si4 sol sol do'8 re' |
mi' si do' re' do'4 do' |
do'2 do'4 re' |
re'2 do'4 r |
do'4 r do' r |
do' r do' r |
do' r si do' |
do' do' re' r |
do' fa'8 mi' re'4 mi' |
fa'8 mi' re' do' si4.\trill do'8 |
do'2
