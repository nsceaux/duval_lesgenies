\clef "basse" do1 do'2 |
mi2. re4 mi2 |
fa sol sol, |
do1. |
do' |
si2. si,4 la,2 |
sol, re re, |
sol,2. la,4 si, sol, |
sol,1. |
R1. |
sol2 fa mi |
re do1 |
sol,2 sol fa |
mi fa sol |
si, sol, la, |
fa, sol,1 |
do1. |
