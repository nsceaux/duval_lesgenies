\clef "dessus" r2 mi''2. fa''4 |
sol''2. si'4 do''2 |
re'' \appoggiatura re''8 mi''2. fa''4 |
mi''1\trill \appoggiatura re''8 do''2 |
r mi''2. fad''4 |
sol''2. re''4 do''2\trill~ |
do''4 si' la'2.\trill sol'4 |
sol'1. |
sol' |
sol''2 re''2. mi''4 |
si'2\trill \appoggiatura la'8 sol'2 do'' |
re'' mi''2. fa''4 |
re''1.\trill |
sol''2 re''2. mi''4 |
\appoggiatura mi''8 fa''2. sol''4 mi''2\trill~ |
mi''4 re'' re''2.\trill do''4 |
do''1. |
