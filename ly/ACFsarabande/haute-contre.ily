\clef "haute-contre" sol'2 do''1~ |
do''2 sol'1 |
la'2 sol'2. sol'4 |
sol'1 \appoggiatura fa'8 mi'2 |
sol' sol'2. la'4 |
sol'1 la'2~ |
la'4 sol' sol'2 fad'\trill |
sol'1. |
sol' |
R1. |
sol'1 sol'2 |
fa'2 mi' sol' |
sol'1. |
do''1 do''2 |
re''1 do''2~ |
do'' do'' si'\trill |
do''1. |
