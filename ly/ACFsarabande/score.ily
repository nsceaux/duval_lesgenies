\score {
  \new StaffGroup <<
    \ifConcert <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
    \ifComplet <<
      \new Staff \with {
        instrumentName = \markup\center-column { [Violons et Hautbois] }
      } << \global \includeNotes "dessus" >>
      \ifFull <<
        \new Staff \with {
          instrumentName = \markup\center-column { [Hautes-contre] }
        } << \global \includeNotes "haute-contre" >>
        \new Staff \with {
          instrumentName = "[Tailles]"
        } << \global \includeNotes "taille" >>
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { [Basses, Bassons et B.C.] }
      } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \origLayout { s1.*11\break }
      >>
    >>
  >>
  \layout {
    indent = #(if (ly:get-option 'concert)
                  smallindent
                  largeindent)
  }
  \midi { }
}
