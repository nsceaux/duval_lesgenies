\clef "taille" mi'1 sol'2~ |
sol'1 do'2~ |
do' do'2. si4 |
\appoggiatura si8 do'1 sol2 |
mi'1 mi'4 re' |
re'1 fad2 |
sol re'1 |
si1.\trill |
si\trill |
R1. |
re'1 do'2 |
si2 do'4 re' mi' re' |
si1.\trill |
sol'2 la' sol'~ |
sol' si' la'~ |
la' sol' fa' |
mi'1.\trill |
