\clef "basse" do4 do'4. sol8 |
do4 re2 |
mi8 fa sol fa mi re |
do4 fa, sol, |
do,4. sol,8 la, si, |
do4 do'4. sol8 |
do4 re2 |
mi8 fa sol fa mi re |
do2 si,8 do |
re4 re,2 |
sol,4 do'4. sol8 |
sol8 la sib2 |
la8 si! do'2 |
si sib4 |
la8 sol fa mi re do |
sol4 sol4. la8 |
sib2 sib4 |
la8 si! do'2 |
si4. sol8 do' si |
la fa sol4 sol, |
do8 si, do re mi fa |
do2. |
