Ai -- mez- tous, cé -- dez à l’A -- mour,
é -- prou -- vez le poids de ses chaî -- nes ;
il vous of -- fre dans ce beau jour
des plai -- sirs plu -- tôt que des pei -- nes.
Ai -- mez- -nes.
Pro -- fi -- tez de l’heu -- reux mo -- ment,
il n’est pas tou -- jours fa -- vo -- ra -- ble ;
le ca -- price a -- mè -- ne l’ins -- tant,
l’a -- mour le rend ai -- ma -- ble. -ble.
