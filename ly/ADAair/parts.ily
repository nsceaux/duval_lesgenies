\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix"
          #:instrument "[B.C.]")
   (silence #:on-the-fly-markup , #{
\markup\fontsize#-2 \column {
\livretPers L'Amour
\livretVerse#8 { Aimez-tous, cédez à l’Amour, }
\livretVerse#8 { Éprouvez le poids de ses chaînes ; }
\livretVerse#8 { Il vous offre dans ce beau jour }
\livretVerse#8 { Des plaisirs plutôt que des peines. }
\livretVerse#8 { Profitez de l’heureux moment, }
\livretVerse#8 { Il n’est pas toujours favorable ; }
\livretVerse#8 { Le caprice amène l’instant, }
\livretVerse#6 { L’amour le rend aimable. }
}
       #}))
