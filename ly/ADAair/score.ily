\score {
  \new ChoirStaff <<
    \new Staff \with {
      instrumentName = \markup\character L'Amour
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "[B.C.]" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*3\break s2.*6\pageBreak
        s2.*7\break s2.*6
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
