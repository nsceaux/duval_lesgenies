\clef "vdessus" r4 do''4. re''8 |
mi''4 re''8 do'' si' la' |
sol'4 mi''4. fa''8 |
sol''4 fa''8 mi'' fa'' re'' |
\appoggiatura re''8 mi''2 \appoggiatura re''8 do''4 |
r do''4. re''8 |
mi''4 re''8 do'' si' la' |
sol'2 la'8 si' |
do''4 si'8 la' re'' mi'' |
si'4( la'2)\trill |
sol'4 do''4. re''8 |
sol'4 re''4. mi''8 |
fa''4 mi''8 re'' mi''\trill fa'' |
sol''4 fa''8 mi'' re'' mi'' |
\appoggiatura re''8 do''4 fa''4. mi''8 |
re''2\trill sol'4 |
r re''4. mi''8 |
fa''4 mi''8 re'' mi''\trill fa'' |
sol''4 r8 fa'' mi'' re'' |
do'' re'' re''4.\trill( do''8) |
do''2. |
do'' |
