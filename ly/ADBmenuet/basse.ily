\clef "basse" do2 do4 |
sol fa mi |
mi8 fa sol4 sol, |
do2. |
do2 do4 |
sol sol,8 la, si,4 |
do re re, |
sol, sol8 fa mi re |
sol,2. |
sol2 r4 |
do'2 r4 |
fad2 re4 |
sol2 r4 |
sol, si, sol, |
do2 do'4 |
fa sol sol, |
do2. |
