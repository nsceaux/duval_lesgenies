\clef "dessus" do''8 re'' mi'' re'' do'' mi'' |
re''4 sol' do'' |
sol'8 la' sol' fa' mi' re' |
mi'4 re'8 mi' do'4 |
do''8 re'' mi'' re'' do'' mi'' |
re'' do'' si' do'' re''4 |
do''8 si' la'4.\trill sol'8 |
sol'2. |
sol' |
re''8 mi'' fa'' mi'' re'' fa'' |
mi'' re'' do'' re'' mi'' do'' |
re'' mi'' re'' do'' si' la' |
si'4 la'8 si' sol'4 |
re''8 mi'' fa'' mi'' re'' fa'' |
mi'' re'' do'' re'' mi'' do'' |
re''4 sol' si'\trill |
do''2. |
