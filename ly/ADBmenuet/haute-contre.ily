\clef "haute-contre" mi'8 fa' sol' fa' mi' sol' |
sol'2 sol'4 |
sol'8 fa' mi' re' do' si |
si2 do'4 |
mi'8 fa' sol' fa' mi' sol' |
sol'4 re''8 do'' si'4 |
la'8 sol' sol'4 fad'\trill |
sol'8 la' si' la' sol' fa'! |
sol'2. |
si'8 do'' re'' do'' si' re'' |
do'' si' la' si' do''4 |
la'8 sol' si' la' sol' fad' |
fad'2 sol'4 |
si'8 do'' re'' do'' si' re'' |
do'' si' la' si' do''4 |
la' sol' fa' |
mi'2.\trill |
