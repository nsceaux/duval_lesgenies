\clef "taille" sol2 do'4 |
si8 do' re'4 do' |
do'2 sol4 |
sol fa8 sol mi4 |
sol2 do'4 |
si si'8 la' sol' fa' |
mi'4 re' do' |
si2.\trill |
si\trill |
sol'2 r4 |
sol'2 r4 |
re'2 re'4 |
re' do'8 re' si4 |
sol'2 sol'4 |
sol'2 do'4 |
do' mi' re' |
do'2. |
