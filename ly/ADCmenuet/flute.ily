\clef "dessus" sol''2 lab''4 |
sol''2 do'''4 |
sol''8 lab'' sol'' fa'' mib'' re'' |
mib''4\trill re''8 mib'' do''4 |
sol''2 lab''4 |
sol''2 do'''4 |
mib''8 fa'' fa''4.(\trill mib''16 fa'') |
sol''2. |
sol'' |
sib''2 do'''4 |
sib'' sib' mib'' |
lab'' lab'' sol'' |
fa''8 mib'' fa'' sol'' lab'' fa'' |
sib''2 do'''4 |
sib''2 sib'4 |
mib'' fa'' re''\trill |
mib''2. |
sol''2 lab''4 |
sol''2 do'''4 |
sol''8 lab'' sol'' fa'' mib'' re'' |
mib''4\trill re''8 mib'' do''4 |
sol''2 lab''4 |
sol''2 do'''4 |
mib'' fa''8 mib'' re'' mib'' |
do''2. |
do'' |
