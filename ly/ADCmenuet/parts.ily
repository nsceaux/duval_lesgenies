\piecePartSpecs
#`((dessus #:score "score")
   (flute-hautbois #:notes "flute" #:instrument "Flûtes")
   (dessus1 #:notes "violon")
   (dessus2 #:notes "violon")
   (dessus2-haute-contre #:notes "violon")
   (basse #:score-template "score"
          #:notes "violon" #:instrument "Violons"
          #:clef "alto")
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
