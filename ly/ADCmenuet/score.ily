\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Flûtes" } <<
      \global \includeNotes "flute"
    >>
    \new Staff \with { instrumentName = "Violons" } <<
      \global \includeNotes "violon"
      \origLayout { s2.*3\break s2.*11\break }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
