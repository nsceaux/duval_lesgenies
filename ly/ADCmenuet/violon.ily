\clef "dessus2" mib'8 re' do'4 re' |
mib'8 re' do' si do' re' |
mib' fa' sol'4 sol |
do'2. |
mib'8 re' do'4 re' |
mib'8 fa' mib' re' do' si |
do' sib lab2\trill |
sol sol'8 fa' |
sol2. |
mib'2 lab4 |
mib'2 do'4 |
re' re' mib' |
sib' sib' lab' |
sol' mib' lab |
mib'2. |
sol'4 lab' sib' |
mib' mib' re' |
do'8 si do'4 re' |
mib'8 re' do' si do' re' |
mib' fa' sol'4 sol |
do'2. |
mib'8 re' do'4 re' |
mib'8 fa' mib' re' do' si |
do'4 sol' sol |
do'4. sib8 do' re' |
sol2. |
