\clef "basse" sol,4 re sol la |
si2 si4 sol |
do' do do do' |
do' si la sol |
re' re' re re |
sol2 sol4 sol |
mi mi, mi mi |
la2. sol4 |
fad4. fad8 mi4. re8 |
la4 la la, la, |
re r r2 |
re,2 re4 re |
sol sol, sol sol |
re' re re re' |
do' do do8 do' re' do' |
si4 sol sol, sol |
fa1 |
mi |
si, |
do2 do'4 la |
mi'2 re'4 do' |
si2. do'4 |
re' re' re re |
do2 do4 do |
si,2. si,4 |
mi mi fad re |
sol1~ |
sol~ |
sol~ |
sol4 sol sol, sol, |
do1 |
la,4 la, re2 |
si,4 si, mi re |
do si, la, sol, |
re re re, re, |
re re, re, sol, |
fad, fad, re, re, |
sol,2. r8 si, |
mi2 fad4. sol8 |
do2 re |
sol, r |
sol4 sol do' do'8 si |
la si do' la re'4 re'8 do' |
si do' si la sol4 sol |
do' do8 re mi4 do |
re2 r |
la,4 la,8 si, do2 |
sol, sol |
re4 re8 mi fa2 |
do4 do8 re mi2 |
si, do |
re4 sol, re,2 |
sol,4 sol sol la |
si si, si sol |
do' do do do' |
do' si la sol |
re' re' re re |
sol8 la sol fad mi fad sol mi |
la si la sol fad sol la fad |
si4 si, si mi |
si, si, si, si |
red red red si, |
mi1 |
mi |
re2 do |
si,1 |
la, |
sol, |
fad, |
mi,2 mi |
re1 |
do |
si,2. mi4 |
la,2 la |
sol1 |
fad |
mi2. la4 |
re2 re' |
do'1 |
si |
la2. re'4 |
sol2 sol |
sol1 |
sol |
sol, |
do |
la,2 re |
si,2. re4 |
do si, la, sol, |
re2 re, |
re,4 re re re |
fad4 fad re re |
sol sol, sol, si, |
mi2 fad4. sol8 |
do2 re |
sol,1 |

