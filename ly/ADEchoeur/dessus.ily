\clef "dessus" si'4 la' si' do'' |
re'' re' re'' si' |
mi''2. mi''4 |
mi'' re'' do'' si' |
la' re''8 mi'' fad'' sol'' la'' fad'' |
sol''4 sol'8 la' si' do'' re'' si' |
mi''4 mi'' si' mi'' |
dod'' dod'' dod'' la' |
re''4. re''8 mi''4. fad''8 |
fad''2( mi''4.)\trill re''8 |
re''4 \twoVoices #'(dessus1 dessus2 dessus) <<
  { la'4 la' si' |
    do'' do'' si' la' |
    si' }
  { fad'4 fad' sol' |
    la' la' sol' fad' |
    sol' }
>> sol' sol' si' |
la' la' fad' sol' |
la' la' sol' fad' |
sol' sol' sol' sol' |
re'' re'' re'' re'' |
sol'' sol' sol' sol'' |
fa'' fa'' fa'' sol'' |
mi'' mi'' \twoVoices #'(dessus1 dessus2 dessus) <<
  { sol'4 la' |
    si'2\trill si'4 do'' |
    re''2 do''4 si' |
    la'\trill la' }
  { sol'4 fad' |
    sol'2 sol'4 la' |
    si'2 la'4 sol' |
    fad'4\trill fad' }
>> re'4 re' |
la'2 la'4 la' |
re''4 re' re' re'' |
do'' do'' do'' re'' |
si' si' sol' sol' |
re'' re' re' re'' |
sol'' sol' sol' sol'' |
fa'' fa'' fa'' sol'' |
mi''\trill mi'' sol' sol' |
do'' do'' la' la' |
re'' re'' si' si' |
mi'' re'' do'' si' |
la'2\trill la' |
r4 la' la' si' |
do'' do'' si'\trill la' |
si' la'8 si' sol' la' si' re'' |
re''4. re''8 do''4. si'8 |
si' la' sol' la' la'4.\trill sol'8 |
sol'2 re''4 re'' |
sol'' sol''8 fad'' mi'' fad'' sol'' mi'' |
la''4 la''8 sol'' fad''4 la'' |
re''2 sol''4 sol''8 fa'' |
mi'' fa'' mi'' re'' do'' si' la' sol' |
fad'4\trill re' la' la'8 si' |
do''2 sol'4 sol'8 la' |
si'4 sol'8 la' si' do'' re'' mi'' |
fa''2 do''4 do''8 re'' |
mi''2 si'4 si'8 do'' |
re''4 sol''8 fad'' mi'' re'' do'' si' |
la'4 si' la'4.\trill sol'8 |
sol'2 si'4 do'' |
re''8 mi'' re'' do'' si' do'' re'' si' |
mi''4 mi'' mi'' mi'' |
mi'' re'' do'' si' |
la'\trill la' re'' re'' |
si' si' si' mi'' |
dod'' dod'' dod'' fad'' |
red''2\trill red''4 mi'' |
fad''4 fad'' fad'' si' |
fad' sol' la' fad' |
sol'2 sol' |
r4 do'' do'' do'' |
do'' si' si' la' |
si' si8 si si4 red'' |
red'' red'' red'' red'' |
mi'' mi' mi'' mi'' |
mi''2 red''4.\trill mi''8 |
mi''4 mi'' mi' mi' |
si'2 si'4 si' |
mi''2. mi''4 |
re'' re'' re'' mi'' |
dod''\trill dod'' la' la' |
mi''2 mi''4 mi'' |
la'' la' la' la'' |
sol'' sol'' sol'' la'' |
fad''4 fad'' re'' re'' |
la'' la' la' la'' |
re''' re'' re'' re''' |
do''' do'' do''8 do''' re''' do''' |
si''4 si'' sol' sol' |
re'' re' re' re'' |
sol'' sol' sol' sol'' |
fa''4 fa' fa'8 fa'' sol'' fa'' |
mi''4\trill mi'' sol' sol' |
do'' do'' la' la' |
re'' re'' si' si' |
mi''4 re'' do'' si' |
la'2\trill la' |
r4 re' la' si' |
do'' do'' si'\trill la' |
si'\trill si' si' r8 re'' |
re'' do'' re'' si' do'' si' la' si' |
si' la' sol' la' la'4.\trill sol'8 |
sol'1 |
