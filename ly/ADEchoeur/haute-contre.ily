\clef "haute-contre" sol'4 la' sol' fad' |
sol'2 sol'4 sol' |
sol'4. re'8 mi' fad' sol' la' |
sol'4 sol' fad' sol' |
fad'\trill fad'8 sol' la' si' do'' la' |
si'4 si'8 la' sol' la' si' sol' |
sol'4 si' sol' sol' |
mi'4 mi' mi' la' |
la'2 la' |
la'2. la'4 |
fad'\trill la' la' si' |
do'' do'' si' la' |
si' sol' sol' si' |
la' la' fad' sol' |
la' la' sol' fad' |
sol' si si si |
si si' re'' si' |
do'' mi' mi' do'' |
re'' re'' re'' re'' |
sol'2 do'4 la |
mi'2 re'4\trill do' |
si2. do'4 |
re'2 fad'4 sol' |
fad'2\trill fad'4 fad' |
sol' si si sol' |
sol'2 la'4 fad' |
sol' re' si sol' |
fa' fa' fa' sol' |
mi' mi' mi' mi' |
re' re'' re'' si' |
do''2 mi'4 mi' |
mi'2 fad'4 fad' |
sol' sol' sol' sol' |
sol' sol' fad' sol' |
fad'2\trill fad' |
r4 fad' fad' sol' |
la' la' sol'\trill fad' |
sol'2. r8 si' |
si'4. si'8 la'4. sol'8 |
sol' fad' mi' fad' fad'4.\trill sol'8 |
sol'2 do''4 do'' |
si'2 sol'4 sol' |
sol'4 fad'8 mi' re'4 fad' |
sol'2 re'4 sol' |
sol'8 la' sol' fa' mi' re' do' si |
la2 fad'!4 fad'8 sol' |
la'2 mi'4 mi'8 fad' |
sol'2~ sol'8 la' si' do'' |
re''2 la'4 la'8 si' |
do''2 sol'4 sol'8 la' |
si'4. re''8 do'' si' la' sol' |
fad'4 sol' fad'4.\trill sol'8 |
sol'2 sol'4 fad' |
sol'2 sol'4 sol' |
sol' sol' sol' sol' |
sol' sol' fad' sol' |
fad'\trill fad' fad' fad' |
sol' sol' sol' sol' |
mi' mi' la' la' |
fad'2\trill fad'4 sol' |
red' red' red' si' |
fad' sol' la' fad' |
sol'2 sol' |
r4 sol' sol' sol' |
fad' sol' fad'4.\trill sol'8 |
red'2.\trill fad'4 |
fad' fad' fad' fad' |
sol' sol' mi' mi' |
fad'2 fad'4. fad'8 |
si'2 si'4 si' |
si'2 si'4 si' |
do''2. la'4 |
la' la' la' sold' |
la' la' dod'' dod'' |
dod''2 dod''4 dod'' |
re''2 re''4 re'' |
re''2.( dod''4) |
re''2 fad'4 fad' |
fad'2 fad'4 fad' |
sol'1~ |
sol'2 sol'4 fad' |
sol'2. sol'4 |
fa' fa' fa' sol' |
mi' mi' mi' mi' |
re'2 re'4 si |
do'2 mi'4 mi' |
mi' mi' fad' fad' |
sol' sol' sol' sol' |
sol' sol' fad' sol' |
fad'2\trill fad' |
r4 fad' fad' sol' |
la' la' sol'\trill fad' |
sol' sol' sol' r8 si' |
si' la' si' sol' la' sol' fad' sol' |
sol' fad' mi' fad' fad'4.\trill sol'8 |
sol'1 |
