Du doux bruit de nos chants que ces lieux re -- ten -- tis -- sent,
les a -- mours, les a -- mours et les jeux, pour nos plai -- sirs s’u -- nis -- sent ;
\tag #'vdessus {
  ai -- mons, goû -- tons mil -- le dou -- ceurs,
  l’a -- mour les pro -- met à nos cœurs.
  Ai -- mons, ai -- mons, goû -- tons mil -- le dou -- ceurs,
  l’a -- mour les pro -- met à nos cœurs.

  Du doux bruit de nos chants que ces lieux re -- ten -- tis -- sent,
  du doux bruit de nos chants que ces lieux re -- ten -- tis -- sent,
}
\tag #'vhaute-contre {
  \ifConcert { ai -- mons, goû -- tons mil -- le dou -- ceurs, }
  Du doux bruit de nos \ifComplet { chants } \ifConcert { chants __ }
  que ces lieux re -- ten -- tis -- sent,
  \ifComplet {
    les a -- mours et les jeux, pour nos plai -- sirs s’u -- nis -- sent ;
  }
  \ifConcert { l’a -- mour les pro -- met à nos cœurs. }

  Du doux bruit de nos chants,
  \ifConcert { du doux bruit }
  de nos chants
  que ces lieux re -- ten -- tis -- sent,
  les a -- mours et les jeux,
}
\tag #'vtaille {
  Du doux bruit de nos \ifComplet { chants __ } \ifConcert { chants }
  que ces lieux re -- ten -- tis -- sent ;
  \ifConcert { les a -- mours et les jeux, pour nos plai -- sirs s’u -- nis -- sent ; }
  \ifComplet { Du doux bruit }
  de nos chants que ces lieux re -- ten -- tis -- sent,
  du doux bruit de nos chants que ces lieux re -- ten -- tis -- sent,
}
\tag #'vbasse {
  Du doux bruit de nos chants que ces lieux re -- ten -- tis -- sent,
  les a -- mours et les jeux, pour nos plai -- sirs s’u -- nis -- sent ;

  Du doux bruit de nos chants que ces lieux re -- ten -- tis -- sent,
}
les a -- mours et les jeux,
\tag #'vbasse { et les jeux, }
pour nos plai -- sirs s’u -- nis -- sent ;
ai -- mons, goû -- tons mil -- le dou -- ceurs,
l’a -- mour les pro -- met à nos cœurs.

Du doux bruit de nos chants que ces lieux re -- ten -- tis -- sent,
les a -- mours et les jeux,
les a -- mours et les jeux, pour nos plai -- sirs s’u -- nis -- sent ;
ai -- mons, goû -- tons mil -- le dou -- ceurs,
\tag #'(vdessus vhaute-contre vtaille) {
  l’a -- mour, l’a -- mour les pro -- met à nos cœurs.
}
Du doux bruit de nos chants que ces lieux re -- ten -- tis -- sent,
\tag #'vdessus {
  que ces lieux, que ces lieux re -- ten -- tis -- sent,
  du doux bruit de nos chants que ces lieux re -- ten -- tis -- sent,
  du doux bruit de nos chants que ces lieux re -- ten -- tis -- sent,
}
\tag #'vhaute-contre {
  du doux bruit de nos chants que ces lieux re -- ten -- tis -- sent,
  du doux bruit __ de nos chants que ces lieux re -- ten -- tis -- sent,
  les a -- mours et les jeux,
}
\tag #'vtaille {
  que ces lieux, que ces lieux re -- ten -- tis -- sent,
  du doux bruit de nos chants,
  du doux bruit de nos chants que ces lieux re -- ten -- tis -- sent,
}
\tag #'vbasse {
  du doux bruit de nos chants que ces lieux re -- ten -- tis -- sent,
  que ces lieux re -- ten -- tis -- sent,
  du doux bruit de nos chants que ces lieux re -- ten -- tis -- sent,
  du doux bruit de nos chants que ces lieux re -- ten -- tis -- sent,
  les a -- mours,
}
les a -- mours et les jeux,
\tag #'vbasse { et les jeux, }
pour nos plai -- sirs s’u -- nis -- sent ;
ai -- mons, goû -- tons mil -- le dou -- ceurs,
l’a -- mour les pro -- met à nos cœurs.