\piecePartSpecs
#`((dessus #:instrument ,#{ \markup\center-column { Hautbois Violons } #})
   (dessus1 #:instrument ,#{ \markup\center-column { Hautbois Violons } #})
   (dessus2 #:instrument ,#{ \markup\center-column { Hautbois Violons } #})
   (parties)
   (dessus2-haute-contre)
   (taille)
   (basse #:instrument ,#{ \markup\center-column { [Basses, Bassons et B.C.] } #})

   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
