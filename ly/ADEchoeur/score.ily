\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with {
        instrumentName = \markup\center-column { Hautbois Violons }
      } << \global \includeNotes "dessus" >>
      \ifFull <<
        \new Staff \with {
          instrumentName = \markup\center-column { [Hautes-contre] }
        } << \global \includeNotes "haute-contre" >>
        \new Staff \with {
          instrumentName = "[Tailles]"
        } << \global \includeNotes "taille" >>
      >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \ifComplet\instrumentName "[Dessus]"
        \ifConcert\instrumentName "[Dessus I]"
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \ifComplet\instrumentName\markup\center-column { [Hautes-contre] }
        \ifConcert\instrumentName "[Dessus II]"
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \ifComplet\instrumentName "[Tailles]"
        \ifConcert\instrumentName\markup\center-column { [Hautes-contre] }
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \ifComplet\instrumentName "[Basses]"
        \ifConcert\instrumentName "[Basse]"
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses, Bassons et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*8\pageBreak
        s1*8\pageBreak
        s1*8\pageBreak
        s1*7\pageBreak
        s1*7\pageBreak
        s1*7\pageBreak
        s1*9\pageBreak
        s1*8\pageBreak
        s1*8\pageBreak
        s1*7\pageBreak
        s1*7\pageBreak
        s1*7\pageBreak
        s1*5
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
