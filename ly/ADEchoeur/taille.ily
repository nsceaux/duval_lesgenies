\clef "taille" re'2 re'4 do' |
si2 re'4 re' |
do'2. do'4 |
do' re' re' re' |
re'2 re'4 re' |
re'2 si4 si |
si sol' mi' si |
la2 mi' |
fad'4. fad'8 dod'4. re'8 |
re'2. dod'4 |
re' fad' fad' sol' |
la' la' sol' fad' |
sol' si si sol' |
fad'2.\trill re'4 |
re'2 re'4 re' |
sol'2 sol'4 sol' |
sol'2. sol4 |
sol2. sol'4 |
sol' sol' sol' sol' |
sol'2 do'4 la |
mi'2 re'4\trill do' |
si2. do'4 |
re'2 la4 la |
la2 la4 la |
si sol sol si |
sol sol re' re' |
re' re' si si |
si2 si4 si |
do' do' do' do' |
si si si re' |
sol2 do'4 do' |
do'2 re'4 la |
si re' mi' si |
do' re' re' re' |
re'1 |
re'4 re' re' re' |
re'2 re'4 re' |
re'2. r8 sol' |
sol'2 re'4. re'8 |
re'2 do'4( si8) do' |
si2\trill r2 |
re'4 re' mi'2 |
do'4 do' la' re' |
re'8 mi' re' do' si4 si |
do'2 sol4 la |
la2 r |
mi'4 mi'8 re' do'4 mi' |
re'1 |
la'4 la'8 sol' fa'4 la' |
sol' sol'8 fa' mi'4 sol' |
sol'1 |
re'2 re'4 do' |
si2\trill re'4 do' |
si2 re'4 re' |
do' do' do' do' |
do' re' re' re' |
re' re' re' re' |
re' re' mi' mi' |
mi' mi' fad' fad' |
fad'2 si4 si |
si si si si' |
fad' sol' la' fad' |
sol'2 sol' |
r4 mi' mi' mi' |
la si do'4. do'8 |
fad'2. si4 |
si si si si |
si si si si |
do'2 si4. si8 |
si2 sold'4 sold' |
sold'2 sold'4 sold' |
la'2. mi'4 |
fa' fa' fa' mi' |
mi' mi' la' la' |
la'2 mi'4 mi' |
fad'2 la4 la |
si si si la |
la2 re'4 re' |
re'2 re'4 re' |
re'2 re'4 re' |
mi'2 mi'4 re' |
re'2 si4 si |
si si si si |
do' do' do' do' |
si si si re' |
sol sol sol sol |
la la la la |
si si si si |
do' sol do' re' |
re'2 re' |
r4 re' re' re' |
re' re' re' re' |
re' re' re' r8 re' |
sol4. si8 la4. re'8 |
mi'2 do'4. si8 |
si1\trill |
