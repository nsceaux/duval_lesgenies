<<
  \tag #'vdessus {
    \clef "vdessus" r2 si'4 do'' |
    re''2 re''4 si' |
    mi''2. mi''4 |
    mi'' re'' do'' si' |
    la'\trill la' la' re'' |
    si'2\trill si'4 si' |
    mi''2 si'4 mi'' |
    dod''2.\trill la'4 |
    re''4. re''8 mi''4. fad''8 |
    fad''2( mi'')\trill |
    re''4 <<
      \ifComplet <<
        { \voiceOne re''4^"Trio" la' si' |
          do'' do'' si' la' |
          si'2. si'4 | \oneVoice }
        \new Voice \with { autoBeaming = ##f } {
          \voiceTwo fad'4 fad' sol' |
          la' la' sol' fad' |
          sol'2. si'4 | }
      >>
      \ifConcert {
        re''4^"Trio" la' si' |
        do'' do'' si' la' |
        si'2. si'4 |
      }
    >>
    la'2\trill fad'4 sol' |
    la'2 sol'4 fad' |
    sol'2 r |
    r4 si' re'' si' |
    do''2. do''4 |
    re'' re'' re'' re'' |
    sol'2. <<
      \ifComplet <<
        { \voiceOne la'4 |
          si'2 si'4 do'' |
          re''2 do''4 si' |
          la'2\trill \oneVoice }
        \new Voice \with { autoBeaming = ##f } {
          \voiceTwo fad'4 |
          sol'2 sol'4 la' |
          si'2 la'4 sol' |
          fad'2\trill
        }
      >>
      \ifConcert {
        la'4 |
        si'2 si'4 do'' |
        re''2 do''4 si' |
        la'2\trill
      }
    >> re'4 re' |
    la'2 la'4 la' |
    re''2. re''4 |
    do'' do'' do'' re'' |
    si' si' sol' sol' |
    re''2 re''4 re'' |
    sol''2. sol''4 |
    fa'' fa'' fa'' sol'' |
    mi''4\trill mi'' sol' sol' |
    do''2 la'4 la' |
    re''2 r4 si' |
    mi'' re'' do'' si' |
    la'2\trill la' |
    r4 la' la' si' |
    do'' do'' si' la' |
    si'2.\trill r8 re'' |
    re''2 do''4. si'8 |
    si'2 la'4.\trill sol'8 |
    sol'2 r |
    R1*11 |
    r2 si'4 do'' |
    re''2 re''4 si' |
    mi''2. mi''4 |
    mi'' re'' do'' si' |
    la'\trill la' re'' re'' |
    si'2\trill si'4 mi'' |
    dod''2\trill dod''4 fad'' |
    red''2\trill red''4 mi'' |
    fad''2. si'4 |
    fad' sol' la' fad' |
    sol'2 sol' |
    r4 do'' do'' do'' |
    do'' si' \footnoteHere #'(0 . 0) \markup {
      Source : \raise#1 \score {
        <<
          \new Staff {
            \tinyQuote \key sol \major do''4 si' si'8([ la'16\trill sol']) la'16*4 |
            si'2.
          } \addlyrics { -tons mil -- le dou -- ceurs }
        >>
        \layout { \quoteLayout }
      }
    } si'8([ la'\trill sol']) la' |
    si'2. red''4 |
    red''2. red''4 |
    mi''2 mi''4. mi''8 |
    mi''2 red''4.\trill mi''8 |
    mi''2 mi'4 mi' |
    si'2 si'4 si' |
    mi''2. mi''4 |
    re'' re'' re'' mi'' |
    dod'' dod'' dod'' dod'' |
    dod''2 dod''4 dod'' |
    re''2 re''4 re'' |
    re''2.( dod''4) |
    re''2 re'4 re' |
    la'2 la'4 la' |
    re''2. re''4 |
    do'' do'' do'' re'' |
    si' si' sol' sol' |
    re''2 re''4 re'' |
    sol''2. sol''4 |
    fa''4 fa'' fa'' sol'' |
    mi'' mi'' sol' sol' |
    do''2 la'4 la' |
    re''2. si'4 |
    mi'' re'' do'' si' |
    la'2\trill la' |
    r4 re' la' si' |
    do''4 do'' si'\trill la' |
    si'2.\trill r8 re'' |
    re''2 do''4. si'8 |
    si'2 la'4.\trill sol'8 |
    sol'1 |
  }
  \tag #'vhaute-contre {
    \ifComplet\clef "vhaute-contre"
    \ifConcert\clef "petrucci-c3/treble"
    r2 sol'4 fad' |
    sol'2 sol'4 sol' |
    sol'2. sol'4 |
    sol' sol' fad' sol' |
    fad' fad' fad' fad' |
    sol'2 re'4 re' |
    sol'2 sol'4 sol' |
    mi'2. mi'4 |
    fad'4 fad' dod'4. re'8 |
    re'2.( dod'4) |
    re'4 <<
      \ifComplet {
        r4 r2 |
        R1*4 |
        r2 sol4 sol |
        re'2 re'4 re' |
        sol'2.
      }
      \ifConcert {
        fad'4 fad' sol' |
        la' la' sol' fad' |
        sol'2. r4 |
        R1 |
        r2 re'4 re' |
        sol'2 sol'4 sol' |
        sol'1~ |
        sol'2.
      }
    >> sol'4 |
    fa' fa' fa' sol' |
    mi' mi' <<
      \ifComplet {
        do'4 la |
        mi'2 re'4\trill do' |
        si2. do'4 |
        re' re' fad' sol' |
        fad'\trill fad' fad'4 fad' |
      }
      \ifConcert {
        r4 fad'4 |
        sol'2 sol'4 la' |
        si'2 la'4 sol' |
        fad'2\trill la4 la
        la2 fad'4 fad' |
      }
    >>
    sol'2 sol'4 sol' |
    sol'2 la'4 fad' |
    sol'2. sol'4 |
    fa' fa' fa' sol' |
    mi' mi' mi' mi' |
    re'2 re'4 si |
    do'2 mi'4 mi' |
    mi'2 fad'4 fad' |
    sol'2 r4 sol' |
    sol' sol' fad' sol' |
    fad'2\trill fad' |
    r4 fad' fad' sol' |
    la' la' sol' fad' |
    sol'2. r8 si' |
    si'2 la'4. sol'8 |
    sol'2 fad'4.\trill sol'8 |
    sol'2 r |
    R1*11 |
    r2 sol'4 fad' |
    sol'2 sol'4 sol' |
    sol'2. sol'4 |
    sol' sol' fad' sol' |
    fad' fad' fad' fad' |
    sol'2 sol'4 sol' |
    mi'2 la'4 la' |
    fad'2 fad'4 sol' |
    red'2.\trill <<
      \ifComplet {
        si4 |
        fad sol la fad |
        sol2 sol |
      }
      \ifConcert {
        si'4 |
        fad' sol' la' fad' |
        sol'2 sol' |
      }
    >>
    r4 sol' sol' sol' |
    fad' sol' mi'4. fad'8 |
    red'2.\trill fad'4 |
    fad'2. fad'4 |
    sol'2 mi'4. mi'8 |
    fad'2 fad'4. fad'8 |
    si2 si4 si |
    si2 si4 si |
    do'2. mi'4 |
    fa' fa' fa' mi' |
    mi' mi' la la |
    mi'2 mi'4 mi' |
    la'2. la'4 |
    sol' sol' sol' la' |
    fad'2\trill fad' |
    r fad'4 fad' |
    sol'1~ |
    sol'2 sol'4 fad' |
    sol'2. sol'4 |
    fa' fa' fa' sol' |
    mi' mi' mi' mi' |
    re'2 re'4 si |
    do'2 mi'4 mi' |
    mi'2 fad'4 fad' |
    sol'2. sol'4 |
    sol' sol' fad' sol' |
    fad'2\trill fad' |
    r4 fad' fad' sol' |
    la'4 la' sol'\trill fad' |
    sol'2. r8 si' |
    si'2 la'4. sol'8 |
    sol'2 fad'4.\trill sol'8 |
    sol'1 |
  }
  \tag #'vtaille {
    \clef "vtaille" r2 re'4 do' |
    si2 re'4 re' |
    do'2. do'4 |
    do' re' re' re' |
    re' re' re' re' |
    re'2 si4 si |
    si2 si4 si |
    la2. la4 |
    la4 la la4. la8 |
    la1 |
    fad4 r r2 |
    <<
      \ifComplet {
        R1*3 |
        r2 re'4 re' |
        sol'2 sol'4 sol' |
        sol'1~ |
        sol'2.
      }
      \ifConcert {
        R1*4 |
        r2 sol4 sol |
        re'2 re'4 re' |
        sol'2.
      }
    >> sol4 |
    sol sol sol sol |
    sol sol <<
      \ifComplet {
        r2 |
        R1*2 |
        r2 la4 la |
        la2 la4 la |
      }
      \ifConcert {
        do'4 la |
        mi'2 re'4\trill do' |
        si2. do'4 |
        re' re' fad' sol' |
        fad'\trill fad' la4 la |
      }
    >>
    si2. si4 |
    sol4 sol re' re' |
    re' re' si si |
    si2 si4 si |
    do'2. do'4 |
    si si si re' |
    sol sol do' do' |
    do'2 re'4 la |
    si2 r4 si |
    do' re' re' re' |
    re'1 |
    re'4 re' re' re' |
    re' re' re' re' |
    re'2. r8 sol' |
    sol'2 re'4. re'8 |
    re'2 do'4( si8) do' |
    si2\trill r |
    R1*11 |
    r2 re'4 do' |
    si2 re'4 re' |
    do'2. do'4 |
    do' re' re' re' |
    re' re' re' re' |
    re'2 mi'4 mi' |
    mi'2 fad'4 fad' |
    fad'2 si4 si |
    si2. si4 |
    fad sol la fad |
    sol2 sol |
    r4 mi' mi' mi' |
    la si do'4. do'8 |
    fad2. si4 |
    si2. si4 |
    si2 si4. si8 |
    do'2 si4. si8 |
    si2 sold4 sold |
    sold2 sold4 sold |
    la2. la4 |
    la la la sold |
    la2 la |
    r mi'4 mi' |
    fad'2 fad'4 fad' |
    si2 si4 la |
    la2 la |
    R1 |
    r2 re'4 re' |
    mi'2 mi'4 re' |
    re'2 si4 si |
    si2 si4 si |
    do'2. do'4 |
    si4 si si re' |
    sol sol sol sol |
    la2 la4 la |
    si2. si4 |
    do' sol do' re' |
    re'2 re' |
    r4 re' re' re' |
    re'4 re' re' re' |
    re'2. r8 re' |
    sol2 la4. re'8 |
    mi'2 do'4. si8 |
    si1\trill |
  }
  \tag #'vbasse {
    \clef "vbasse" r2 sol4 la |
    si2 si4 sol |
    do'2. do'4 |
    do' si la sol |
    re' re' re re |
    sol2 sol4 sol |
    mi2 mi4 mi |
    la2. sol4 |
    fad4. fad8 mi4. re8 |
    la2( la,) |
    re4 r r2 |
    r2 re4 re |
    sol2 sol4 sol |
    re'2. re'4 |
    do' do' do' re' |
    si sol sol sol |
    fa2 fa4 sol |
    mi2. mi4 |
    si, si, si, sol, |
    do do r2 |
    R1*2 |
    r2 re4 re |
    do2 do4 do |
    si,2. si,4 |
    mi mi fad re |
    sol1~ |
    sol~ |
    sol |
    sol2 sol,4 sol, |
    do1 |
    la,4 la, re2 |
    si,4 si, mi re |
    do si, la, sol, |
    re1 |
    re4 re re re |
    re re re re |
    sol2. r8 si, |
    mi2 fad4. sol8 |
    do2 re4. re8 |
    sol,2 r |
    R1*11 |
    r2 sol4 la |
    si2 si4 sol |
    do'2. do'4 |
    do' si la sol |
    re' re' re re |
    sol2 mi4 mi |
    la2 la4 fad |
    si2 si4 mi |
    si,2. si4 |
    red red red si, |
    mi2 mi |
    r4 mi mi mi |
    re re do4. do8 |
    si,2 si,4 si, |
    fad2 fad4 fad |
    si2. si4 |
    la la la si |
    sold mi mi mi |
    re2 re4 re |
    do2. do4 |
    si, si, si, mi |
    la,2 la,4 la |
    sol sol sol la |
    fad fad fad fad |
    mi2 mi4 la |
    re2. re'4 |
    do' do' do' re' |
    si si si si |
    la2 la4 re' |
    sol2. sol4 |
    sol sol sol sol |
    sol sol sol sol |
    sol2 sol,4 sol, |
    do2 do4 do |
    la,2 re4 re |
    si,2. re4 |
    do si, la, sol, |
    re2 re |
    r4 re re re |
    fad4 fad re re |
    sol2. r8 si, |
    mi2 fad4. sol8 |
    do2 re4. re8 |
    sol,1 |
  }
>>
