\clef "basse" <>^"Tous" re,4 re2 la4~ |
la sib2 la4~ |
la dod'2 re'4~ |
re' mi4 fa8 sol fa mi |
re4 la,2 mi4~ |
mi fa2 mi4~ |
mi sold2 la4~ |
la si, do8 re do si, |
la,4 r fa2 |
fa4 mi2 re4 |
sib2. sol4 |
la2 la, |
re2 la,4 la |
sold2 la |
re2. do4 |
re fa mi mi, |
la,2 r4 re' |
dod'2 re' |
la4 la fa fa |
mi mi re re |
dod2 fa |
sib4 sol la la, |
re2 r4 sol |
fad2 sol |
re'4 re' sib sib |
la la sol sol |
re2 sib, |
do re4 re, |
sol,2 sol4 mi |
la4 fa2 sol4 |
la2 la, |
re,4 re2 la4~ |
la sib2 la4~ |
la dod'2 re'4~ |
re' mi fa8 sol fa mi |
re2 r4 la |
sold2 la |
re2. do4 |
re fa mi mi, |
la,2 r4 re' |
sol2 r |
r sib |
la4 fa mi re |
sol fa mi sol |
fa2 re |
dod re |
la4 sol fa2 |
sol4 sib la la, |
re1 |
