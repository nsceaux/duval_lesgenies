\clef "dessus" r4 re'2 la'4~ |
la' sib'2 la'4~ |
la' dod''2 re''4~ |
re'' mi' fa'8 sol' fa' mi' |
re'4 do'' do'' do'' |
si' si' la' la' |
sold'4\trill si'2 do''4~ |
do'' si' la'4.\trill sold'8 |
<<
  \tag #'dessus1 {
    la'2 r4 re''~ |
    re'' dod''2 re''4~ |
    re'' mi''2 fa''4~ |
    fa'' mi'' re''4.\trill dod''8 |
    re''4 la'2 mi''4~ |
    mi'' fa''2 mi''4~ |
    mi'' sold''2 la''4~ |
    la'' si' do''8 re'' do'' si' |
    la'4 la' fa'' fa'' |
    mi'' mi'' re'' re'' |
    dod'' dod'' re'' re'' |
    dod''2\trill re''4 la'~ |
    la' mi''2 re''4~ |
    re'' re'' dod''4.\trill re''8 |
    re''4 re'' re'' re'' |
    do''4 do'' sib' sib' |
    la' la' re'' re'' |
    do'' do'' sib' sib' |
    la' la' re' re'' |
    sib'4.\trill la'8 la'4.\trill sol'8 |
    sol'4 si'2 dod''4~ |
    dod'' re''2 mi''4~ |
    mi'' re'' dod''4.\trill re''8 |
    re''4 r la'2 |
    sol'4 fa' mi' dod'' |
    mi'' mi' la'2~ |
    la'4 sol' fa'8 mi' re' dod' |
    re'2 r4 do'' |
    si'2 la' |
    sold'4 si'2 la'4~ |
    la'2 la'4.\trill sold'8 |
    la'4 la'' sol'' fa'' |
    sib'' sib'' la'' sol'' |
    dod''2 mi'' |
    fa''4 la'' la' la'' |
    dod'' re'' mi'' dod'' |
    re'' fa'' fa'' fa'' |
    mi'' mi'' re'' re'' |
    dod''\trill dod'' re'' re'' |
    re''2 dod''4.\trill re''8 |
    re''1 |
  }
  \tag #'dessus2 {
    la'4 re'2 la'4~ |
    la' sib'2 la'4~ |
    la' dod''2 re''4~ |
    re'' mi' fa'8 sol' fa' mi' |
    re'4 r do'' do'' |
    si'2 la' |
    sold'4 si'2 la'4 |
    la'2 la'4.\trill sold'8 |
    la'4 la' la'' la'' |
    sol'' sol'' fa'' fa'' |
    mi'' mi'' la'' la'' |
    sol'' sol'' fa'' fa'' |
    mi'' sol''2 fa''4~ |
    fa''4. mi''8 mi''4.\trill re''8 |
    re''4 la' sib' sib' |
    la' la' sol' sol' |
    fad' fad' sol' sol' |
    fad' re' sol' sol' |
    fad' fad' sol' sol' |
    sol'4. fad'8 fad'4.\trill sol'8 |
    sol'4 re''2 mi''4~ |
    mi'' la' la'' sol''~ |
    sol'' fa'' mi''4.\trill re''8 |
    re''4 r fa''2 |
    mi''4 re'' dod'' mi'' |
    sol''2. fa''4 |
    fa'' sib'' la''4. sol''8 |
    fa''4 la'2 mi''4~ |
    mi'' fa''2 mi''4~ |
    mi'' sold''2 la''4~ |
    la'' si' do''8 re'' do'' si' |
    la'2. la''4 |
    dod''2 mi''~ |
    mi'' dod'' |
    re''4 re'' dod'' re'' |
    mi'' fa'' dod'' mi'' |
    la' la'' la'' la'' |
    sol'' sol'' fa'' fa'' |
    mi''2 la'4 la'' |
    fa''4.\trill mi''8 mi''4.\trill re''8 |
    re''1 |
  }
>>