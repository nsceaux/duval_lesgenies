\clef "basse" r4 |
re2 dod4 |
re2. |
re'4 re'8 do' si la |
sold4 la8 sol fa2 |
mi4. re8 do si, |
la,4 la8 sol fa mi |
re2 re4 |
la2 la4 |
sib2 sol4 |
dod mi8 sol fa4 mi8 re |
la4 la,2 |
\ifComplet {
  re1~ |
  re4 fa sol, la, sib, sol, |
  la,2. la8 sol |
  fad4 re sol8 fa? mi re |
  la4 la8 sol fa sol fa mi |
  la2. la4 |
  la sol fa re |
  mi re do4. si,8 |
  do4 sold, la, la |
  re2. red4 |
  mi2 mi, |
  la,4 la8 sol fa2 fad4 re |
  sol4. fa8 mi4. fad8 |
  sold4 mi la2 |
  dod4 la, la8 sol fa sol |
  la2 la, |
  re2 \allowPageTurn
}
\ifConcert { re2 }
re'8 dod' |
re'4 sol4. fad8 |
mi2 mi4 |
si la sol |
fad8 mi re dod re mi |
fad la sol fad mi re |
dod2 la,4 |
re2 re'8 mi' |
fad'4 dod' re' |
la8 si la sol fad mi |
re4 sol4. fad8 |
la2 la4 |
mi4. re8 mi fad |
sol fad mi fad sol mi |
la si la sol fad mi |
re4. dod8 si,4\trill |
la,8 sol, la, si, dod la, |
re2 sol,4 |
la,2 la4 sol2. |
fad4 mi re |
dod2 re4 |
la,4. la8 sol fad |
mi4. re8 mi fad |
sol fad mi fad sol mi |
la si la sol fad mi |
re2 re,4 |
la,8 sol, la, si, dod la, |
re4 re' sol |
la sol8 fad mi re |
dod si, la,4 sol, |
fad, mi, re, |
la,2. |
re8 fad la re' dod' la |
re dod re mi fad re |
sol2. |
la |
sol |
fad4 dod re |
la la,2 |
\ifComplet {
  re2~ re4 re4 |
  sol,4 la, sib,2 |
  la,4 la dod2. la,4 |
  re2 re,4 re8 do |
  si,4. la,8 sold,4 mi, |
  la,2 la4 sol |
  fa re mi mi, |
  la, mi, la,, la8 sol |
  fad4 re sol8 fa? mi re |
  la4 dod re mi8 fa |
  sib,4 sol, la,2 |
  re8 mi fa sol la4 la, |
  re1~ | \allowPageTurn
  re4 sol~ sol8. fad16 mi8. re16 |
  la4. re8 mi dod fad re16 mi |
  la,4 la dod |
  re si,2 dod8. re16 |
  la,4. sol,8 fad,4 mi, |
  re, re8 mi fad4 sol |
  la2 re4 |
  la,2 la4 |
  mi2. | \allowPageTurn
  sol4 fad8 mi re4 sol, la,2 |
  %re2*3/4~ \hideNotes re8
}
\ifConcert { re2*3/4~ \hideNotes re8 }
