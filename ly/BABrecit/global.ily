\key la \minor
\digitTime\time 3/4 \midiTempo#80 \partial 4 s4 s2.*3
\time 4/4 s1
\digitTime\time 3/4 \midiTempo#120 s2.*5
\digitTime\time 2/2 s1
\digitTime\time 3/4 s2.
\ifComplet {
  \digitTime\time 2/2 \midiTempo#160 s1
  \time 3/2 \bar ".|:" s1.
  \digitTime\time 2/2 s1*2 \alternatives s1 s1 s1*5
  \time 3/2 s1.
  \digitTime\time 2/2 s1*4
  \digitTime\time 3/4 \midiTempo#120
}
s2 \bar "||"
\beginMarkSmall "Air" \key re \major s4 s2. \bar ".|:" s2.*7 \alternatives s2.*2 s2.
s2.*21 \tempo "Lentement" s2.*7
\digitTime\time 2/2 \midiTempo#160 s2 \bar "||"
\ifComplet {
  \key la \minor s2 s1
  \time 3/2 s1.
  \digitTime\time 2/2 s1*9 \bar "||"
  \key re \major s1
  \time 4/4 \midiTempo#80 s1*2
  \digitTime\time 3/4 s2.
  \time 4/4 \grace s8 s1*3
  \digitTime\time 3/4 s2.*3
  \time 3/2 \midiTempo#160 s1.
  %\digitTime\time 2/2 s2 \bar "||"
}
