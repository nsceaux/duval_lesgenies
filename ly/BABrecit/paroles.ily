Viens ê -- tre le té -- moin du bon -- heur qui m’en -- chan -- te,
c’est dans ces lieux qu’A -- mour ré -- pond à mes __ dé -- sirs ;
sans ex -- i -- ger de moi ni lar -- mes ni sou -- pirs,
il rend ma flam -- me tri -- om -- phan -- te.

\ifComplet {
  Ah ! __ si ce dieu com -- ble vos vœux
  ne le fai -- tes ja -- mais pa -- raî -- tre ;
  Ah ! -raî -- tre ;

  un cœur dans l’em -- pire a -- mou -- reux,
  de -- vrait, pour ê -- tre plus heu -- reux,
  dou -- ter __ tou -- jours de l’ê -- tre.
  Un cœur dans l’em -- pire a -- mou -- reux,
  de -- vrait, pour ê -- tre plus heu -- reux,
  dou -- ter __ tou -- jours de l’ê -- tre.
}

Les plai -- sirs dont l’a -- mour sait en -- chan -- ter les sens,
sa -- tis -- font les dé -- sirs d’un a -- mant qui sou -- pi -- re ;
Les plai -- sirs dont l’a-  -pi -- re ;
pour moi, li -- bre du soin de ces ten -- dres a -- mants,
non, non je ne les res -- sens,
qu’au -- tant que je puis les re -- di -- re.
Pour moi, li -- bre du soin de ces ten -- dres a -- mants,
non, non je ne les res -- sens,
qu’au -- tant que je puis les re -- di -- re.
Non, non __ je ne les res -- sens,
qu’au -- tant que je puis les re -- di -- re.

\ifComplet {
  Qui ne sait gar -- der le se -- cret,
  goû -- te peu de dou -- ceurs par -- fai -- tes ;
  el -- les n’ont ja -- mais é -- té fai -- tes
  pour un a -- mant in -- dis -- cret.
  El -- les n’ont ja -- mais é -- té fai -- tes
  pour un a -- mant __ in -- dis -- cret.
  Quel ob -- jet vous re -- tient dans cet heu -- reux a -- si -- le ?
  Ve -- nez- vous at -- ten -- dre Lu -- ci -- le ?
  
  Un ob -- jet plus char -- mant m’ar -- rê -- te dans ces lieux,
  Zer -- bin, il va bien -- tôt sor -- tir du sein de l’on -- de
  pour me ren -- dre l’a -- mant le plus heu -- reux __ du mon -- de ;
  de -- meu -- re, son a -- bord va sur -- pren -- dre tes yeux.
}
