\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix"
          #:instrument "[B.C.]")
   (parties #:on-the-fly-markup , #{

\markup\fontsize#-2 \fill-line {
  \column {
    \livretPers Léandre
    \livretVerse#12 { Viens être le témoin du bonheur qui m’enchante, }
    \livretVerse#12 { C’est dans ces lieux qu’Amour répond à mes désirs ; }
    \livretVerse#12 { Sans exiger de moi ni larmes ni soupirs, }
    \livretVerse#8 { Il rend ma flamme triomphante. }
    \livretPers Zerbin
    \livretVerse#8 { Ah ! si ce dieu comble vos vœux }
    \livretVerse#8 { Ne le faites jamais paraître ; }
    \livretVerse#8 { Un cœur dans l’empire amoureux, }
    \livretVerse#8 { Devrait, pour être plus heureux, }
    \livretVerse#6 { Douter toujours de l’être. }
    \livretPers Léandre
    \livretVerse#12 { Les plaisirs dont l’amour sait enchanter les sens, }
    \livretVerse#12 { Satisfont les désirs d’un amant qui soupire ; }
    \livretVerse#12 { Pour moi, libre du soin de ces tendres amants, }
    \livretVerse#7 { Non, non je ne les ressens, }
    \livretVerse#8 { Qu’autant que je puis les redire. }
    \livretPers Zerbin
    \livretVerse#8 { Qui ne sait garder le secret, }
    \livretVerse#8 { Goûte peu de douceurs parfaites, }
    \livretVerse#8 { Elles n’ont jamais été faites }
    \livretVerse#7 { Pour un amant indiscret. }
    \livretVerse#12 { Quel objet vous retient dans cet heureux asile ? }
    \livretVerse#8 { Venez-vous attendre Lucile ? }
  }
  \column {
    \livretPers Léandre
    \livretVerse#12 { Un objet plus charmant m’arrête dans ces lieux, }
    \livretVerse#12 { Zerbin, il va bientôt sortir du sein de l’onde }
    \livretVerse#12 { Pour me rendre l’amant le plus heureux du monde ; }
    \livretVerse#12 { Demeure, son abord va surprendre tes yeux. }
    \livretVerse#8 { Jamais la reine de Cythère }
    \livretVerse#7 { N’a brillé de tant d’appas, }
    \livretVerse#8 { L’Amour ne connaît plus sa mère, }
    \livretVerse#6 { Depuis qu’il suit les pas }
    \livretVerse#8 { De l’aimable objet qui m’enchaîne : }
    \livretVerse#8 { Son char conduit par les Zéphirs, }
    \livretVerse#8 { Vole sur la liquide plaine ; }
    \livretVerse#12 { Les vents à son aspect, retiennent leur haleine, }
    \livretVerse#8 { Les ris, les jeux & les plaisirs }
    \livretVerse#8 { Folâtrent sans cesse autour d’elle ; }
    \livretVerse#8 { On ne saurait voir cette belle, }
    \livretVerse#8 { Sans former de tendre désirs. }
    \livretVerse#10 { Lucile vient, j’évite sa présence }
    \livretVerse#12 { Elle me croit constant, que je plains son erreur ! }
    \livretPers Zerbin
    \livretVerse#12 { Dois-je de son amour affermir la constance ? }
    \livretPers Léandre
    \livretVerse#12 { Ce n’est plus un secret que ma nouvelle ardeur. }
  }
}#})
   (silence #:on-the-fly-markup , #{
\markup\fontsize#-2 \fill-line {
  \column {
    \livretPers Léandre
    \livretVerse#12 { Viens être le témoin du bonheur qui m’enchante, }
    \livretVerse#12 { C’est dans ces lieux qu’Amour répond à mes désirs ; }
    \livretVerse#12 { Sans exiger de moi ni larmes ni soupirs, }
    \livretVerse#8 { Il rend ma flamme triomphante. }
    \livretPers Zerbin
    \livretVerse#8 { Ah ! si ce dieu comble vos vœux }
    \livretVerse#8 { Ne le faites jamais paraître ; }
    \livretVerse#8 { Un cœur dans l’empire amoureux, }
    \livretVerse#8 { Devrait, pour être plus heureux, }
    \livretVerse#6 { Douter toujours de l’être. }
    \livretPers Léandre
    \livretVerse#12 { Les plaisirs dont l’amour sait enchanter les sens, }
    \livretVerse#12 { Satisfont les désirs d’un amant qui soupire ; }
    \livretVerse#12 { Pour moi, libre du soin de ces tendres amants, }
  }
  \column {
    \livretVerse#7 { Non, non je ne les ressens, }
    \livretVerse#8 { Qu’autant que je puis les redire. }
    \livretPers Zerbin
    \livretVerse#8 { Qui ne sait garder le secret, }
    \livretVerse#8 { Goûte peu de douceurs parfaites, }
    \livretVerse#8 { Elles n’ont jamais été faites }
    \livretVerse#7 { Pour un amant indiscret. }
    \livretVerse#12 { Quel objet vous retient dans cet heureux asile ? }
    \livretVerse#8 { Venez-vous attendre Lucile ? }
    \livretPers Léandre
    \livretVerse#12 { Un objet plus charmant m’arrête dans ces lieux, }
    \livretVerse#12 { Zerbin, il va bientôt sortir du sein de l’onde }
    \livretVerse#12 { Pour me rendre l’amant le plus heureux du monde ; }
    \livretVerse#12 { Demeure, son abord va surprendre tes yeux. }
  }
}#}

\markup\fontsize#-2 \fill-line {
  \column {
    \livretPers Léandre
    \livretVerse#12 { Viens être le témoin du bonheur qui m’enchante, }
    \livretVerse#12 { C’est dans ces lieux qu’Amour répond à mes désirs ; }
    \livretVerse#12 { Sans exiger de moi ni larmes ni soupirs, }
    \livretVerse#8 { Il rend ma flamme triomphante. }
    \livretPers Zerbin
    \livretVerse#8 { Ah ! si ce dieu comble vos vœux }
    \livretVerse#8 { Ne le faites jamais paraître ; }
    \livretVerse#8 { Un cœur dans l’empire amoureux, }
    \livretVerse#8 { Devrait, pour être plus heureux, }
    \livretVerse#6 { Douter toujours de l’être. }
    \livretPers Léandre
    \livretVerse#12 { Les plaisirs dont l’amour sait enchanter les sens, }
    \livretVerse#12 { Satisfont les désirs d’un amant qui soupire ; }
    \livretVerse#12 { Pour moi, libre du soin de ces tendres amants, }
  }
  \column {
    \livretVerse#7 { Non, non je ne les ressens, }
    \livretVerse#8 { Qu’autant que je puis les redire. }
    \livretPers Zerbin
    \livretVerse#8 { Qui ne sait garder le secret, }
    \livretVerse#8 { Goûte peu de douceurs parfaites, }
    \livretVerse#8 { Elles n’ont jamais été faites }
    \livretVerse#7 { Pour un amant indiscret. }
    \livretVerse#12 { Quel objet vous retient dans cet heureux asile ? }
    \livretVerse#8 { Venez-vous attendre Lucile ? }
    \livretPers Léandre
    \livretVerse#12 { Un objet plus charmant m’arrête dans ces lieux, }
    \livretVerse#12 { Zerbin, il va bientôt sortir du sein de l’onde }
    \livretVerse#12 { Pour me rendre l’amant le plus heureux du monde ; }
    \livretVerse#12 { Demeure, son abord va surprendre tes yeux. }
  }
}#}))
