\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "[B.C.]" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s2.*3\break s1 s2.*4\break \grace s8 s2. s1 s2. s1 s2 \bar "" \pageBreak
        s1*5 s2 \bar "" \break s2 s1*4\break s1. s1*2 s2 \bar ""\pageBreak
        s2 s1 s2.*5\break s2.*8\break s2.*7\pageBreak
        s2.*8\break s2.*7\break s2.*5 s1\pageBreak
        s1 s1. s1*2\break s1*5\break s1*4\pageBreak
        \grace s8 s1 s2. s2. \bar "" \break s4 s1 s2 \bar "" \break s2 s2.*3 s2 \bar "" \pageBreak
        \grace s8 s1
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
