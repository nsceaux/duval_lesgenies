\ffclef "vhaute-contre" <>^\markup\character Léandre r8 la |
re'16 re' re' mi' \appoggiatura mi'8 fa' mi'16 fa' sol'8 fa'16 mi' |
\appoggiatura mi'8 fa'2 \appoggiatura mi'8 re'4 |
fa'4 fa'8 mi' re' do' |
si8.\trill si16 \appoggiatura si8 do'8. si16 \appoggiatura si8 la4\trill\melisma sold8.\melismaEnd la16 |
sold2.\trill |
mi'4 mi'8 dod' re' mi' |
\appoggiatura mi'8 fa'2 r8 fa' |
mi'4.\trill re'8 mi' fad' |
\appoggiatura fad'8 sol'2 r8 mi' |
la'4. la8 re' re' mi' fa' |
fa'4( mi'4.\trill re'8) |
re'2
\ifComplet {
  \ffclef "vtaille" <>^\markup\character Zerbin
  re'2~ |
  re'4 la8 fa sib4 la sol4. fa8 |
  mi2.\trill la8 si |
  \appoggiatura si8 do'2 si8 si dod' re' |
  dod'4\trill la re'2*3/4~ \hideNotes re'8 | \unHideNotes
  dod'2\trill dod'4 la |
  do'!4 si8 do' la4\trill sold8 la |
  sold4\trill r8 si mi'4. fa'8 |
  mi' re' mi' si do'4 r8 la |
  fa'2~ fa'8 re' si do' |
  do'2( si4.\trill la8) |
  la4 la re' mi'8 fa' la4 si8 do' |
  si4\trill r8 re' sol'4. fa'8 |
  mi' si dod' re' dod'4\trill r8 la |
  mi'2~ mi'8 dod' re' mi' |
  fa'2( mi'4.\trill re'8) |
  re'2
  \ffclef "vhaute-contre" <>^\markup\character Léandre
}
re'8 mi' |
fad'4 mi'4.\trill re'8 |
sol'4. fad'8 mi'4 |
re' dod' si |
la2.~ |
la2 la'8 mi' |
sol'4 fad'4.\trill mi'8 |
\grace mi'8 fad'2 fad'8 sol' |
la'4 sol'4.\trill fad'8 |
mi'4\trill mi' re'8 mi' |
fad'4 mi'4.\trill re'8 |
mi'4 mi' mi' |
sol'2. |
si2 si8 mi' |
dod'2\trill re'8 mi' |
fad'4 \appoggiatura sol'8 fad'4\trill \appoggiatura mi'8 re'4 |
mi'2 la'4 |
fad'4. sol'16 fad' mi'8. fad'16 |
dod'2 r8 la |
mi'2 re'8 dod' |
re'4 mi'4. fad'8 |
sol'2( fad'8\trill[ mi']) |
mi'2 mi'4 |
sol'2. |
si2 si8 mi' |
dod'2\trill re'8 mi' |
fad'4 \appoggiatura sol'8 fad'4 \appoggiatura mi'8 re'4 |
mi'2\trill la'4 |
fad'4.\trill sol'16 fad' mi'8. fad'16 |
dod'2\trill r8 mi' |
la'2 dod'8 la |
re'4 mi'4. fad'8 |
fad'4( mi'2)\trill |
re'4 r mi' |
fad'2.~ |
fad'4. sol'16 fad' mi'8. fad'16 |
dod'2\trill r8 mi' |
la'2 r8 dod'16 la |
re'4 mi'4. fad'8 |
fad'4( \afterGrace mi'2)\trill re'8 |
re'2
\ifComplet {
  \ffclef "vtaille" <>^\markup\character Zerbin
  r4 re'8 la |
  sib4 la re' mi'8 fa' |
  dod'4\trill mi'8 fa' mi'4 fa'8 sol' fa'4\trill mi' |
  fa' \appoggiatura mi'8 re'4 r fa'8 mi' |
  re'4. mi'8 re'4 do'8 si |
  do'4 \appoggiatura si8 la4 do' re'8 mi' |
  la2 sold4.\trill la8 |
  la2 r4 la8 si |
  \appoggiatura si8 do'4. re'8 \appoggiatura do'8 si4 dod'8 re' |
  dod'4\trill \appoggiatura si8 la4 fa' mi'8 re' |
  sol'4.( fa'16\trill[ mi']) mi'4.\trill re'8 |
  re'2 r |
  r2 r4 r8 la16 la |
  re'8 re'16 fad' si4 si8 dod'16 re' dod'8.\trill re'16 |
  \appoggiatura re'8 mi'4 mi'8 si16 si si8. dod'16 \appoggiatura si8 la la16 sold |
  la4 la8
  \ffclef "vhaute-contre" <>^\markup\character Léandre
  mi'16 fad' sol'8 fad'16 mi' |
  \appoggiatura mi'8 fad'8. la'16 la'4( sol'8.)\trill fad'16 mi'8. fad'16 |
  dod'8 r16 mi' la la si dod' re'8 re' mi'16 mi' fad' sol' |
  fad'4\trill fad'8 fad'16 mi' re'8 re'16 dod' si si dod' re' |
  dod'2\trill~ dod'8 re' |
  \appoggiatura re'8 mi'4 mi' r8 mi' |
  sol'4 sol' r8 si16 mi' |
  dod'4\trill re'8 mi' \appoggiatura mi'8 fad'2 mi'4. re'8 |
  %re'2
}
