\clef "basse" re2~ re4. re'8 |
fa'4 mi' re' do' |
sib la sol8 la sib sol |
la4 sol fa dod |
re8. fa16 mi8. sol16 fa8. la16 sol8. sib16 |
la4 sol8 fa mi4 re |
la8. mi16 dod8. mi16 la,4. re8 |
dod8. la,16 si,8. dod16 re8. mi16 fa8. re16 |
sol4. fa8 mi4. re8 |
la,2 la4. la,8 |
la,8. si,16 dod8. re16 mi8. re16 dod8. si,16 |
la,4 la8 sol fa4. mi8 |
re1~ |
re~ |
re |
do4 si, la,4. si,8 |
do2 re4 mi |
fa re mi mi, |
la, r r fa |
mi fa8 mib re4 do |
sib,2 sib |
la r |
r la4 sib |
fa2 r |
fa2 r |
R1 | \allowPageTurn
si,2 si4 sol |
do' r r2 |
sol2 si,4 do |
sol,2. sol8 fa |
mi4 re8 do si,4 sol, |
do do' si sol |
do do' si sib |
la8 sol la fa do'4 si8 sol |
do' mi fa sol do sol mi re |
do8 mi re do si,4 do |
sol8 sol, la, si, do2 |
si,4 do re re, |
mi4 do re re, |
sol,2 r4 r8 la |
re4. re'8 sol la sib sol |
la4 sol8 fa mi re dod re |
la,4 r la,2 |
la,4 r re8 sib, sol, la, |
re,2 re8 mi fa4 |
sold,4. mi,8 fad,4. sold,8 |
la,4. la8 do' la sold mi |
la2 sold |
la4 re mi2 |
fa8 fa mi re mi4 mi, |
la,2 la |
dod2. la,4 |
re2. re'4 |
dod' do' si sib |
la2 la, |
sib,8 fa, mi, re, sol, fa, mi, re, |
dod, re, sib, sol, la,2 |
re,
