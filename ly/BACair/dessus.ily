\clef "dessus" <>^\markup\whiteout Violons r2 r4 r8 <<
  \tag #'dessus1 {
    re''8 |
    la'4 sol' fa' mi' |
    sol' fa' mi'4. re'8 |
    dod'4.( si16 dod') re'8. fa'16 mi'8. sol'16 |
    fa'8. la'16 sol'8. sib'16 la'2~ |
    la'4 si' \appoggiatura si'8 dod''4. re''8 |
    dod''4.\trill si'8 la'4.
  }
  \tag #'dessus2 {
    fa''8 |
    la''4 dod'' re'' la' |
    dod' re' sib'8 la' sol' fa' |
    mi'4. la'8 la'4 r |
    r dod'' re''4. mi''8 |
    dod''4 re'' sol''4. fa''8 |
    mi''4.\trill re''8 dod''4.\trill
  }
>> fa''8\tresdoux |
mi'' dod'' re'' mi'' fa''4. la''8 |
re''2 dod''4.\trill re''8 |
<<
  \tag #'dessus1 {
    dod''8 la' dod'' mi'' re''8. fa''16 dod''8. re''16 |
    mi''8. fa''16 mi''8. re''16 dod'' re'' dod'' re'' mi'' fa'' mi'' fa'' |
    sol''4. mi''8 la'4~ la'16 mi'' fa'' sol'' |
    la''8 fa'' la'' fa'' re''
  }
  \tag #'dessus2 {
    mi''4 la'8 sol' fa'4 mi' |
    sol'4. fa'8 mi'4. re'8 |
    dod'4. mi'8 re'4. dod'8 |
    re'2~ re'8
  }
>> <>^\markup\whiteout Flûtes la'8[ re'' mi''] |
fa''4. sol''8 fa'' mi'' re'' do'' |
<<
  \tag #'dessus1 { si'8 do'' si' la' sold' mi' sold' si' | }
  \tag #'dessus2 { si' do'' si' do'' si' mi'' si' sold' | }
>>
mi'8 fad' sold' mi' la'4. sold'8 |
<<
  \tag #'dessus1 {
    la'4 si'8 do'' si'4\trill dod'' |
    re''8 fa'' si' re''
  }
  \tag #'dessus2 {
    la'4. la'8 la'4. la'8 |
    la'8 re'' si' re''
  }
>> sold'4.\trill la'8 |
la'4 r r <<
  \tag #'dessus1 {
    la'4 |
    sol' fa' fa''8 fa' sol' la' |
    sib' do'' re'' mi'' fa''4 fa'8 sol' |
    la'4. sol'8 fa' sol' la' sib' |
    do'' re'' mib'' re'' do'' re'' mib'' re'' |
    do'' re'' do'' sib' la' sol' la' sib' |
    do''8 sib' la' sol' fa'4 r |
    r r8 do'' fa''4 fa''8 fa'' |
    re''4 re''8 re'' sol''4 sol''8 sol'' |
    sol''2. fa''8 mi'' |
    re''4 re''8 mi'' fa'' sol'' fa'' mi'' |
    re''2 si'4.\trill re''8 |
    sol'4. sol''8 sol''4. sol''8 |
    sol''8 mi'' re'' mi'' fa'' mi'' fa'' re'' |
    mi''4 do''
  }
  \tag #'dessus2 {
    do''4 |
    do'' do'' re'' mib'' |
    re''8 mi'' fa''2 r8 do'' |
    do''2 do''4 do''8 sib' |
    la' sib' do'' sib' fa'4 fa' |
    la'8 sib' la' sib' do'' sib' la' sol' |
    la' sib' do'' sib' la' sib' la' sol' |
    fa'4. la'8 re''4 re''8 re'' |
    re''4 fa''8 fa'' fa'' re'' si' re'' |
    do'' re'' si' do'' re'' mi'' fa'' mi'' |
    re''4 si'8 do'' re'' mi'' re'' do'' |
    si'2\trill sol'4. sol''8 |
    sol''4 fa''8 mi'' fa'' mi'' re'' si' |
    do'' sol'' fa'' mi'' re'' do'' re'' si' |
    do''4 sol'
  }
>> r4 r8 sol'' |
la'' si'' do''' la'' sol'' <<
  \tag #'dessus1 { mi''8 fa'' re'' | mi'' do'' re'' si' do'' }
  \tag #'dessus2 { do''8 re'' si' | do'' sol' fa' re' mi' }
>> si'8 do'' re'' |
mi''2 re''4.\trill do''8 |
si'8 re'' do'' re'' <<
  \tag #'dessus1 {
    sol'4 la' |
    si'8 re'' mi'' do'' la'4. re''8 |
    si' sol'' la'' sol'' fad''4.\trill sol''8 |
    sol''2 r4 r8 mi'' |
    fa'' la'' sol'' la'' sib'' la'' sol'' fa'' |
    mi'' re'' mi'' fa'' sol'' fa'' mi'' re'' |
    dod'' mi'' re'' mi'' fa'' mi'' re'' mi'' |
    dod'' mi'' re'' dod'' re'' sol'' fa'' mi'' |
    fa''4 \appoggiatura mi''8 re''4 la''8 do''' si'' la'' |
    si''4. sold''8 la''4. si''8 |
    mi'' re'' do'' mi'' la' do'' si' re'' |
    do''4. re''16 do'' si'8 sold' la' si' |
    mi'4 la' sold'4.\trill la'8 |
    la' re'' do'' si' si'4.\trill la'8 |
    la'2 dod''8 si' dod'' re'' |
    mi''2 re''4.\trill dod''8 |
    re'' dod'' re'' mi'' fa''4. sol''16 fa'' |
    mi''4~ mi''16 sol'' fa'' mi'' re''4 mi'' |
    fa''4. mi''16 re'' dod''4.\trill re''8 |
    re'' la'' sol'' fa'' sib'' la'' sol'' sib'' |
    la''4 sol''8 fa'' mi''4.\trill re''8 |
    re''2
  }
  \tag #'dessus2 {
    do''8 mi'' re''4 |
    re''8 re'' do'' mi'' la'4. re''8 |
    si'8 si' mi'' la' la'4.\trill sol'8 |
    sol'2 r4 r8 la'' |
    la'' fa'' sib'' la'' sib'' la'' sol'' fa'' |
    mi'' fa'' mi'' re'' dod'' re'' mi'' fa'' |
    mi'' dod'' la' dod'' re'' mi'' fa'' mi'' |
    mi'' dod'' re'' mi'' re'' re'' re'' dod'' |
    re'' mi'' fa'' sol'' la'' do''' si'' la'' |
    mi''4. mi''8 mi''4. re''8 |
    do''4. mi''8 mi''4. mi''8 |
    mi''4 do'' re''8 sold' la' si' |
    mi'4 la' sold'4.\trill la'8 |
    la' si' la' la' sold'4.\trill la'8 |
    la'2 dod''8 si' dod'' re'' |
    mi''2 re''4.\trill dod''8 |
    re'' mi'' fa'' sol'' la''4. la''8 |
    la''4 la' re'' sol'' |
    fa''4. mi''16 re'' dod''4.\trill re''8 |
    re''4 dod''8 re'' mi'' fa'' dod'' re'' |
    mi'' fa'' mi'' re'' dod''4.\trill re''8 |
    re''2
  }
>>
