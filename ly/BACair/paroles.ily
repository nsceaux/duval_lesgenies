yeux.

Ja -- mais la rei -- ne de Cy -- thè -- re
n’a bril -- lé __ de tant d’ap -- pas,
l’A -- mour ne con -- naît plus sa mè -- re,
de -- puis qu’il suit les pas
de l’ai -- mable ob -- jet qui m’en -- chaî -- ne :
son char con -- duit par les Zé -- phirs,
vo -- le sur la li -- qui -- de plai -- ne ;
les vents à son as -- pect, re -- tien -- nent leur ha -- lei -- ne,
les ris, les jeux et les plai -- sirs
fo -- lâ -- trent sans cesse au -- tour d’el -- le ;
on ne sau -- rait voir cet -- te bel -- le,
sans for -- mer de ten -- dre dé -- sirs.
Les ris, les jeux et les plai -- sirs
fo -- lâ -- trent sans cesse au -- tour d’el -- le ;
on ne sau -- rait voir cet -- te bel -- le,
sans for -- mer de ten -- dre dé -- sirs.
On ne sau -- rait voir cet -- te bel -- le,
sans for -- mer de ten -- dre dé -- sirs.
