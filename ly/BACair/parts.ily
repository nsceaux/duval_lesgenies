\piecePartSpecs
#`((basse #:music ,#{ <>_\markup\center-align { Basses de violons } #}
          #:indent 0)
   (dessus #:score-template "score-2dessus"
           #:instrument
           , #{ \markup\center-column { Violons Flûtes } #})
   (dessus1 #:indent 0)
   (dessus2 #:indent 0)
   (parties #:on-the-fly-markup ,#{\markup\null#})
   (silence #:on-the-fly-markup , #{

\markup\fontsize#-2 \column {
  \livretPers Léandre
  \livretVerse#8 { Jamais la reine de Cythère }
  \livretVerse#7 { N’a brillé de tant d’appas, }
  \livretVerse#8 { L’Amour ne connaît plus sa mère, }
  \livretVerse#6 { Depuis qu’il suit les pas }
  \livretVerse#8 { De l’aimable objet qui m’enchaîne : }
  \livretVerse#8 { Son char conduit par les Zéphirs, }
  \livretVerse#8 { Vole sur la liquide plaine ; }
  \livretVerse#12 { Les vents à son aspect, retiennent leur haleine, }
  \livretVerse#8 { Les ris, les jeux & les plaisirs }
  \livretVerse#8 { Folâtrent sans cesse autour d’elle ; }
  \livretVerse#8 { On ne saurait voir cette belle, }
  \livretVerse#8 { Sans former de tendre désirs. }
}#}))
