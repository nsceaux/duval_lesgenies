\score {
  \new ChoirStaff <<
    \new GrandStaff \with {
      instrumentName = \markup\center-column { Violons Flûtes }
    } <<
      \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
      \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
    >>
    \new Staff \with { instrumentName = \markup\character Léandre } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = \markup\center-column { Basses de violons } } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*7\break s1*6\pageBreak
        s1*5\break s1*6\pageBreak
        s1*6 s2 \bar "" \break s2 s1*4\pageBreak
        s1*5 s2 \bar "" \break s2 s1*4 s2 \bar "" \pageBreak
        s2 s1*5\break s1*6
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
