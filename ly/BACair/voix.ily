\clef "vhaute-contre" re'2 r |
R1*5 |
r2 r4 r8 re' |
la2~ la4. fa8 |
sib4. la8 sol4. fa8 |
mi4\trill mi r la8. si16 |
dod'4.\melisma si8 la16[ si la si] dod'[ re' dod' re']( |
mi'4.)\melismaEnd dod'8 re'4. mi'8 |
\appoggiatura mi'8 fa'2. r8 mi' |
re'4. mi'8 re' do' si la |
sold2\trill \appoggiatura fa8 mi4 r8 mi |
la4 si do'4. re'8 |
mi'4 re'8 do' fa'4 mi' |
re' mi'8 fa' si4.(\trill la8) |
la4 la sol fa |
do' la sib do' |
re' sib8[\melisma do'] re'[ mi' re' mi'] |
fa'2.\melismaEnd do'8 re' |
mib'[ re'] do'[ re'] mib'[ re'] do'[ sib] |
la2\trill \appoggiatura sol8 fa4 r |
r2 r4 r8 do' |
fa'2 re'4 re'8 re' |
sol'2. r8 fa' |
mi'[ fa'] re'[ mi'] fa'[ mi'] re'[ do'] |
si2\trill \appoggiatura la8 sol4 r |
r2 r4 r8 sol |
do'4. mi'8 re' mi' fa' re' |
mi'2 r |
r4 r8 sol' sol'4 re'8 mi' |
fa'4 do'8 re' mi'4 \appoggiatura re'8 do'4 |
r2 mi'8 re' mi' fa' |
sol'2 fa'4.\trill mi'8 |
re'4\trill re' mi' fad' |
sol'4. la'8 fad'4\trill mi'8 fad' |
sol'2 r |
r4 r8 re' fa'([ mi']) re'([ dod']) |
re'4 mi'8([ fa']) sol'([ fa']) mi'([ re']) |
dod'2 r4 r8 la' |
la'([ sol']) fa'([ sol']) la'([ sol']) fa'([ sol']) |
la'[ sol'] fa'[ mi'] fa' \appoggiatura mi' re' r4 |
r2 fa'8 mi' re' do' |
re'2 do'4.\trill si8 |
do'4 \appoggiatura si8 la4 r2 |
mi'4 la' re'( do'8)\trill si |
\appoggiatura si8 do'2 si4.\trill la8 |
la2 r |
r mi'8 re' mi' fa' |
sol'2 fa'4.\trill mi'8 |
fa'4 \appoggiatura mi'8 re'4 r2 |
la'4 mi' sol' r8 dod' |
re'4. mi'16[ fa'] mi'4.\trill re'8 |
re'2 r |
R1 |
r2
