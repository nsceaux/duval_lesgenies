\clef "basse" <>^"[B.C.]" re4 fad, |
sol,8 sol si,2 |
do dod4 |
re2 mi8 do re re, |
sol,2. |
\ifComplet {
  sol4. sol8 fa mi |
  re2 fad |
  sol2. |
  sib4 la4. sol16 fa sol8 sol, |
  do2
}
