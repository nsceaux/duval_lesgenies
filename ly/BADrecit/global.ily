\ifComplet\set Score.currentBarNumber = 149
\ifConcert\set Score.currentBarNumber = 53
\key do \major
\once\omit Staff.TimeSignature
\digitTime\time 2/2 \midiTempo#80 \partial 2 s2
\digitTime\time 3/4 s2.*2
\digitTime\time 2/2 s1
\digitTime\time 3/4 \grace s8
\ifConcert { s2. \bar "|." }
\ifComplet {
  s2.*2
  \digitTime\time 2/2 \midiTempo#160 \grace s8 s1
  \digitTime\time 3/4 \midiTempo#80 s2.
  \time 4/4 s1
  \digitTime\time 3/4 s2 \bar "|."
}
