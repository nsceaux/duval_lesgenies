Lu -- ci -- le vient, j’é -- vi -- te sa pré -- sen -- ce,
el -- le me croit cons -- tant, que je plains son er -- reur !

\ifComplet {
  Dois- je de son a -- mour af -- fer -- mir la cons -- tan -- ce ?
  
  Ce n’est plus un se -- cret que ma nou -- velle ar -- deur.
}
