\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix"
          #:indent 0)
   (parties #:on-the-fly-markup ,#{\markup\null#})
   (silence #:on-the-fly-markup , #{

\markup\fontsize#-2 \column {
  \livretPers Léandre
  \livretVerse#10 { Lucile vient, j’évite sa présence }
  \livretVerse#12 { Elle me croit constant, que je plains son erreur ! }
  \livretPers Zerbin
  \livretVerse#12 { Dois-je de son amour affermir la constance ? }
  \livretPers Léandre
  \livretVerse#12 { Ce n’est plus un secret que ma nouvelle ardeur. }
}#}))
