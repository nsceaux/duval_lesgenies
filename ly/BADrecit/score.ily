\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s2 s2.*2 s1\break \grace s8 s2. s1 s2.\break }
    >>
  >>
  \layout {
    indent = \noindent
    ragged-last = ##f
  }
  \midi { }
}
