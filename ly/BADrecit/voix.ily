\clef "vhaute-contre" \ifComplet <>^\markup\character Léandre
r8 fa' la re' |
si8.\trill re'16 sol'8 re' mi'8. fa'16 |
mi'8\trill mi' r16 mi' mi' mi' la'8. la'16 |
fad'4.\trill re'16 re' sol'4. sol'16 fad' |
\appoggiatura fad'8 sol'2. |
\ifComplet {
  \ffclef "vtaille" <>^\markup\character Zerbin
  re'4 re'8 re' re' mi' |
  \appoggiatura mi'8 fa'4 la8 la re'4 re'8 re' |
  si4\trill si
  \ffclef "vhaute-contre" <>^\markup\character Léandre
  sol'8 sol' |
  re' re'16 mi' \appoggiatura mi'8 fa'4 fa'8 sol'16 la' re'8.\trill do'16 |
  do'2
}
