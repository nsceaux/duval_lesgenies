\clef "basse" do8 re |
mi fa sol4. do8 |
sol,4 sol8 fa mi do |
fa mi fa sol la fa |
sol2 fa4 |
mi si, sol, |
do4. sol,8 do,4 |
do8 si, la, si, do la, |
re si, mi do re re, |
sol,4. sol8 re mi |
fa4. mi8 si, do |
sol4 la8 si do' fa |
do mi fa sol la sol |
fa la sol4 sol,8 sol |
si re' sol4 si |
do'8 fa sol4 sol, |
do2. |
do8 fa, sol,4 sol8 do |
sol,4. la,8 si, sol, |
do4. re8 mi do |
fa do fa, do fa mi |
re la re' do' si la |
sol re sol,4 r |
r r8 sol, si, sol, |
do8. sol,16 do,4 r |
r4 r8 mi re do |
fa do fa, re fa re |
sol fa mi fa sol sol, |
do4. do8 re mi |
fa sol fa re mi fa |
sol la sol mi fa sol |
la si la fa sol la |
si do' si sol la si |
do' fa sol4 sol, |
do do, do' |
sold8 si mi'4 mi |
la fa re |
mi re8 do si, la, |
sold, mi, la,4 la |
re2 si,4 |
do8 re mi4 mi, |
la,8 fa mi\trill re do4~ |
do si, sol, |
do do,8 sol, la, si, |
do fa, sol,4 sol8 do |
sol,4. la,8 si, sol, |
do4. re8 mi do |
fa do fa, do fa mi |
re la re' do' si la |
sol re sol,4 r |
r r8 sol, si, sol, |
do8. sol,16 do,4 r |
r4 r8 mi re do |
fa do fa, re fa re |
sol fa mi fa sol sol, |
do4*3/4~ \hideNotes do16
