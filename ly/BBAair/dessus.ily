\clef "dessus" r8 sol' |
do'' re'' si'4.\trill do''8 |
re'' si' sol'4 sol'' |
la''4. sol''8 fa'' mi'' |
re'' si' sol'4 re'' |
sol''4. fa''8 mi'' re'' |
mi'' do'' sol'4 mi'' |
la''8 si'' do''' mi'' la'' sol'' |
fad'' sol'' sol''4.\trill fad''8 |
sol''2.~ |
sol''4. sol''8 re'' mi'' |
si'4 do''8 fa'' mi'' la'' |
sol'' sol' la' do'' fa'' la' |
re''8 mi''16 fa'' si'4. sol''8 |
sol''4. fa''8 mi'' re'' |
mi'' fa'' re''4.\trill do''8 |
do''4. si'8 do'' re'' |
mi''8 fa''16 mi'' re''4.\trill do''8 |
si'4\trill sol''4. sol''8 |
sol''4. fa''8 mi'' sol'' |
do'' la' do'' la' la''4 |
la''4. la'8 si' do'' |
si' sol' si' re'' mi'' fad'' |
sol''4 re''8 do'' si' re'' |
sol'8. sol'16 do''8. mi''16 sol''4 |
sol''4. mi''8 fa'' sol'' |
do''4 la'8 fa'' la''4 |
re'' sol''4. fa''8 |
mi'' do'' mi'' sol'' mi'' do'' |
la''4. la'8 re'' fa'' |
si'4 do''8 mi'' re''\trill do'' |
fa'' mi'' re'' mi'' fa'' re'' |
sol''2~ sol''16 fa'' mi'' re'' |
mi''8 re''16 do'' si'4.\trill do''8 |
do''4. mi''8 la'' do''' |
si''4 la''8( sold'') la'' si'' |
mi''4 re''4. do''8 |
si'2\trill r8 si' |
si' mi'' mi''4.\trill( re''16 mi'') |
\appoggiatura mi''4 fa''2 r8 fad'' |
sold'' la'' sold''4.\trill la''8 |
la''8 la'16\fort si' do''4. re''16 mi'' |
re''8 mi''16 fa'' sol''4. fa''8 |
mi''4 \appoggiatura re''8 do'' si'\doux do'' re'' |
mi'' fa''16 mi'' re''4.\trill do''8 |
si'4\trill sol''4. sol''8 |
sol''4. fa''8 mi'' sol'' |
do'' la' do'' la' la''4 |
la''4. la'8 si' do'' |
si' sol' si' re'' mi'' fad'' |
sol''4 re''8 do'' si' re'' |
sol' sol' do'' mi'' sol''4~ |
sol''4. mi''8 fa'' sol'' |
do''4 la'8 fa'' la''4 |
re'' sol''4. fa''8 |
mi''4\trill
