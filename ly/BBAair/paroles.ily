A -- si -- le des plai -- sirs, beau lieu rem -- pli de char -- mes,
of -- frez à mes re -- gards l’ob -- jet de mon a -- mour,
of -- frez à mes re -- gards l’ob -- jet de mon a -- mour.
Mon cœur en son ab -- sence é -- prou -- ve des a -- lar -- mes
que rien ne peut cal -- mer, que son heu -- reux re -- tour.
A -- si -- le des plai -- sirs, beau lieu rem -- pli de char -- mes,
of -- frez à mes re -- gards l’ob -- jet de mon a -- mour,
of -- frez à mes re -- gards l’ob -- jet de mon a -- mour.
