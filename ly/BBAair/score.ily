\score {
  \new ChoirStaff <<
    \new Staff \with { instrumentName = "Violons" } <<
      \global \includeNotes "dessus"
    >>
    \new Staff \with { instrumentName = \markup\character Lucile } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s2.*10\break s2.*8\break s2.*6\pageBreak
        s2.*8\break s2.*6\pageBreak
        \grace s4 s2.*7\break s2.*6\pageBreak
        s2.*3
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
