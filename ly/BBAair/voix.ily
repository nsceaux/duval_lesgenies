\clef "vbas-dessus" r4 |
R2.*15 |
r4 r r8 sol' |
do'' re'' si'4.\trill do''8 |
\appoggiatura do''8 re''2 r8 re'' |
mi''4. re''8 do'' si' |
la'4\trill la' r8 do'' |
fa''4. mi''8 re'' do'' |
\appoggiatura do'' re''2 do''4 |
si'4.\trill la'8 sol' fa' |
mi'2 r8 mi'' |
mi''4. do''8 re'' mi'' |
la'2 r8 fa'' |
si'4 do''4. do''16 si' |
\appoggiatura si'8 do''2 r4 |
R2.*5 |
r4 r mi'' |
re''\trill re'' do''8 si' |
do'' mi'' la' sold' la'8. si'16 |
sold'4\trill \appoggiatura fad'8 mi'4 r8 si' |
mi''8. fa''16 dod''4(\trill si'8) dod'' |
re''2 re''4 |
mi''8 fa'' si'4.\trill la'8 |
la'2 r4 |
R2. |
r4 r r8 sol' |
do'' re'' si'4.\trill do''8 |
\appoggiatura do''8 re''2 r8 re'' |
mi''4. re''8 do'' si' |
la'4\trill la' r8 do'' |
fa''4. mi''8 re'' do'' |
\appoggiatura do''8 re''2 do''4 |
\appoggiatura do''8 si'4.\trill la'8 sol' fa' |
mi'2\trill r8 mi'' |
mi''4. do''8 re'' mi'' |
la'2 fa''4 |
si' do''4. do''16 si' |
\appoggiatura si'8 do''4