\clef "basse" do4\repeatTie do |
sol4. fa8 mi re |
la2. |
dod2 la,4 |
re2 \appoggiatura { do16 si, la, } sol,4. sol8 |
sold4 sold2 mi4 |
la2 \appoggiatura { si16 la sol } fad2 |
sol8 mi la sol fad sol la la, |
\ifComplet {
  re2 mi4 |
  fad mi re4. dod8 |
  si,2 la,4 |
  sol,4. mi,8 fad,2 |
  si,4. fad8 si la sol fad |
  mi2 sold, |
  la,4 la8 sol fad mi |
  re4 dod re |
  la,8 la sol fad mi re |
  dod4. si,8 dod la, |
  re mi fad4 re |
  sol8 fad mi fad sol mi |
  la si dod' la si dod' |
  re'4 dod' si |
  la8 sol fad mi fad re |
  sol fad mi fad sol mi |
  la si dod' la si dod' |
  re4 mi fad |
  sol la la, |
}
re2~ re4 | \allowPageTurn
mi2. |
fa |
r4 r la, |
sib,2 r4 |
r r si,! |
do2 mi,4. fa,8 |
do2 r4 |
r4 fad4. re8 |
sol4. la8 sib sol |
la la, dod mi la4 |
la,8 la sol fa mi re |
sib4 sol la8 sol |
fa2 mi8 re |
la4 la,2 |
re2 mi4 |
fa2. |
