s2 s <6+>4 s2. <6>4 <5/> <7> s1
s4 <6> <5/> <_+> s2 <6> s8 <7> <"">\figExtOn <"">\figExtOff <6> <6 5> <4> <3>
\ifConcert { s2 }
\ifComplet {
  s4 <6> <6 5> <_+>\figExtOn <_+>\figExtOff <6>4. <6+>8 <_->2 <4+>4
  <6>4. <6 5>8 <4>4 <_+> <_->2 <_+>8\figExtOn <_+>\figExtOff <6> <6+> <_->2 <6>4 <5/>
  s4 <"">8\figExtOn <"">\figExtOff <6> <6> s4 <5/>2 s8 <"">\figExtOn <"">\figExtOff <6> <6 4> <4+> <6>2\figExtOn <6>4\figExtOff s <6>
  <7>4 <"">2\figExtOn <"">4\figExtOff <"">4\figExtOn <"">\figExtOff <6>8 <5/> s4 <6> <6+> s <6>2 <"">2\figExtOn <"">4\figExtOff
  <"">4\figExtOn <"">\figExtOff <6>8 <5/> s4 <6> <6> <6 5> <4> <3> s2
}
<_->4 <7>2 <6>8 <5/> s2. s2 <6 5/>4
s2. s2 <6 5/>4 s2 <5/> s2.
s4 <6>4. <7 _+>8 s4 <_-> <4 3>8 <6 5 _-> <_+>4.\figExtOn <_+>\figExtOff <_+>2\figExtOn <_+>4\figExtOff <6> <6 5 _-> <_+>
<6>2 <6+>4 <6 4> <_+>2 s <5/>4
