\set Score.currentBarNumber = 56
\key do \major
\once\omit Staff.TimeSignature
\digitTime\time 3/4 \partial 2 \midiTempo#80 s2 s2. \key re \major s2.*2
\time 4/4 s1*3
\time 4/4 s1
\digitTime\time 3/4 \grace s8
\ifComplet {
  s2.
  \digitTime\time 2/2 \midiTempo#160 s1
  \digitTime\time 3/4 \midiTempo#80 \grace s8 s2.
  \digitTime\time 2/2 \grace s8 s1*3
  \digitTime\time 3/4 s2.*13
}
s2 \bar "||"
\key la \minor s4 s2.*5
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.*10 \bar "|."
