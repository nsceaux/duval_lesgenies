Mé -- ri -- tes- tu vo -- lage, un cœur si ten -- dre ?
Pour qui ré -- ser -- ves- tu tes plus fu -- nes -- tes coups,
cru -- el A -- mour ?

Zer -- bin.

Je par -- le de Lé -- an -- dre.
C’est un a -- mant…

Eh quoi ?

Trop in -- di -- gne de vous.

\ifComplet {
  Quoi ? Lé -- an -- dre, Zer -- bin !
  
  Lé -- an -- dre vous a -- do -- re ;
  mais à d’au -- tres qu’à vous, Lé -- andre en dit au -- tant.
  
  A -- près tous ses ser -- ments, l’in -- grat me trompe en -- co -- re.
  
  Af -- fec -- tez quel -- que chan -- ge -- ment
  pour vous van -- ger de cet ou -- tra -- ge ;
  c’est s’as -- su -- rer de son a -- mant,
  que de fein -- dre d’ê -- tre vo -- la -- ge,
  c’est s’as -- su -- rer de son a -- mant,
  que de fein -- dre d’ê -- tre vo -- la -- ge.
}

A -- mante in -- for -- tu -- né -- e, hé -- las ! hé -- las !
mes sou -- pirs, mes re -- gards tra -- hi -- raient ce mys -- tè -- re ;
ma bou -- che lui di -- rait que je ne l’ai -- me pas,
et dans mes yeux il li -- rait le con -- trai -- re.
