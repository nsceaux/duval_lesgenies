\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix"
          #:indent 0)
   (parties #:on-the-fly-markup ,#{\markup\null#})
   (silence #:on-the-fly-markup , #{

\markup\fontsize#-2 \fill-line {
  \column {
    \livretPersDidas Zerbin à part
    \livretVerse#10 { Mérites-tu volage, un cœur si tendre ? }
    \livretVerse#12 { Pour qui réserves-tu tes plus funestes coups, }
    \livretVerse#12 { Cruel Amour ? }
    \livretPers Lucile
    \livretVerse#12 { \transparent { Cruel Amour ? } Zerbin. }
    \livretPers Zerbin
    \livretVerse#12 { \transparent { Cruel Amour ? Zerbin. } Je parle de Léandre. }
    \livretVerse#12 { C’est un amant… }
    \livretPers Lucile
    \livretVerse#12 { \transparent { C’est un amant… } Eh quoi ? }
    \livretPers Zerbin
    \livretVerse#12 { \transparent { C’est un amant… Eh quoi ? } Trop indigne de vous. }
    \livretPers Lucile
    \livretVerse#12 { Quoi ? Léandre, Zerbin ! }
  }
  \column {
    \livretPers Zerbin
    \livretVerse#12 { \transparent { Quoi ? Léandre, Zerbin ! } Léandre vous adore ; }
    \livretVerse#12 { Mais à d’autres qu’à vous, Léandre en dit autant. }
    \livretPers Lucile
    \livretVerse#12 { Après tous ses serments, l’ingrat me trompe encore. }
    \livretPers Zerbin
    \livretVerse#8 { Affectez quelque changement }
    \livretVerse#8 { Pour vous vanger de cet outrage ; }
    \livretVerse#8 { C’est s’assurer de son amant, }
    \livretVerse#8 { Que de feindre d’être volage. }
    \livretPers Lucile
    \livretVerse#8 { Amante infortunée, hélas ! }
    \livretVerse#12 { Mes soupirs, mes regards trahiraient ce mystère ; }
    \livretVerse#12 { Ma bouche lui dirait que je ne l’aime pas, }
    \livretVerse#10 { Et dans mes yeux il lirait le contraire. }
  }
}#}))
