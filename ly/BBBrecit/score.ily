\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2 s2.\break s2.*2 s1 s4 \bar "" \break s2. s1*2\pageBreak
        \grace s8 s2. s1 s2.\break \grace s8 s1*3\break s2.*4 s2 \bar "" \pageBreak
        s4 s2.*5\break s2.*7\break s2.*2 s1 s2.\pageBreak
        s2.*5\break s2.*4
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
