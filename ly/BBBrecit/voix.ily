\ffclef "vtaille" <>^\markup\character-text Zerbin à part
r16 sol sol sol do'8. mi'16 |
si4. si8 dod' re' |
dod'4\trill dod'8. mi'16 mi' re' dod' si |
la4 la8 la16 sol sol8. fad16 |
fad4\trill r16 re' mi' fad' \appoggiatura fad'8 sol'4.
\ffclef "vbas-dessus" <>^\markup\character Lucile
si'8 | mi''4
\ffclef "vtaille" <>^\markup\character Zerbin
r8 si re' re' re' dod' |
dod'4\trill dod' r8 re' mi' fad' |
si8.
\ffclef "vbas-dessus" <>^\markup\character Lucile
sol''16 dod''8
\ffclef "vtaille" <>^\markup\character Zerbin
mi'16 fad' \appoggiatura mi'8 re'4. re'16 dod' |
\ifConcert {
  \appoggiatura dod'8 re'2
}
\ifComplet {
  \appoggiatura dod'8 re'4
  \ffclef "vbas-dessus" <>^\markup\character Lucile
  fad''8. fad''16 dod''8 dod''16 re'' |
  lad'4\trill
  \ffclef "vtaille" <>^\markup\character Zerbin
  r8 fad si si si dod' |
  \appoggiatura dod' re'4 re'8 re'16 re' red'8\trill mi'16 fad' |
  \appoggiatura fad'8 sol'4 r8 mi' dod'4\trill dod'8 dod'16 re' |
  \appoggiatura dod'8 si4
  \ffclef "vbas-dessus" <>^\markup\character Lucile
  r8 re''16 re'' red''4\trill mi''8 fad'' |
  \appoggiatura fad''8 sol''4 r8 si' mi''4 re''8 re''16 dod'' |
  dod''4\trill dod''
  \ffclef "vtaille" <>^\markup\character Zerbin
  r8 re'16 mi' |
  fad'4 sol'8 fad' mi' re' |
  mi'4. re'8 dod' si |
  la4.\trill si8 la sol |
  fad4\trill \appoggiatura mi8 re4 r16 re' mi' fad' |
  si4 mi' fad'8 sol' |
  dod'4\trill la8 la re' mi' |
  fad'4 sol'8[ fad'] mi'[ re'] |
  mi'4 la r16 re' mi' fad' |
  si4 mi' fad'8 sol' |
  dod'4\trill la8 la re' mi' |
  fad'4 dod'4. re'8 |
  re'([ mi'] \afterGrace mi'2\trill re'8) |
  re'2
}
\ffclef "vbas-dessus" <>^\markup\character Lucile
r8 fa'' |
mi''4\trill re'' do''8 sib' |
la'4\trill la' r8 do'' |
mib''4 \afterGrace mib''2( re''8) |
re''2\trill r8 re''16 mi'' |
\appoggiatura mi''8 fa''2 r8 re''16 sol'' |
mi''4\trill do''8 re'' sib'4\trill sib'8 la' |
sol'4\trill sol' r8 fa'' |
la'4 la' si'8 do'' |
si'4\trill sol'' sol''16 fa'' mi'' fa'' |
dod''2 r4 |
mi''2 mi''8 fa'' |
sol''2 \appoggiatura fa''8 mi'' \appoggiatura re'' dod'' |
re''2 mi''8 fa'' |
fa''4( mi''2)\trill |
re''2. |
R2. |
