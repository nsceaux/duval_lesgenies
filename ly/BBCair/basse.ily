\clef "basse" fa,8^"notes égales" fa fa fa mi re do sib, |
la, la, la, la, sib, re do do, |
fa, fa fa fa mi re do sib, |
la, la, la, fa, sib, sib, sib, sib, |
si,! si, do sib, la, la sol fa |
do' do' do' do' mi mi mi do |
fa fa fa fa fad fad fad re |
sol sol si sol do' sib la mi |
fa mi fa re sol fa sol sol, |
do do do do do' do' do' do' |
mi8. do16 fa8. fa16 sib8 sib sib sol |
re' re re re fad fad fad re |
sol sol sol sol si sol do' sib |
la la sol fa mi do fa fa, |
do do do' do' mi do fa fa |
la, la, la, la, la, fa, fa, fa, |
sib, sib sol sol si,? si, sol, sol, |
do do' do' do' sib sib la fa |
mib mib re do sib, do re do |
sib, la, sol, fa, do sib, do do, |
fa, fa sib sib la la sol fa16 mi |
fa8 do' re' re' do' do' sib la16 sol |
la4 fa16 sol la sib do'8 sib do' do |
fa mi fa sol fa sol fa mi |
re4 do sib,8 la, sib, sol, |
la, la sol16 fa mi re dod8 si, dod la, |
re re mi mi fa fa sol re |
la sol la la, re re' do' sib |
la la la la la fa la fa |
sib sol sol sol si, si, si, sol, |
do do fa fa mi re do sib, |
la, la, la, fa, sib, sib, sib, sib, |
si,! sol, do sib, la, la sol fa |
do' do' do' do' mi mi mi do |
fa fa fa fa la, la, la, fa, |
sib, sib sib sib sol sol sol sol |
si! si si sol do' sib la fa |
sib la sol fa do' sib do' do |
fa do' re' re' do' do' sib la16 sol |
la8 fa sib sib la la sol fa16 mi |
fa8 fa, fa16 sol la sib do'8 sib do' do |
fa1 |
