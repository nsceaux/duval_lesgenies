\clef "dessus" r8 fa'' fa''8. fa''16 do'''8. do''16 do''8. do''16 |
fa''8. fa'16 fa'8. fa'16 re''8. sib'16 sol'8. do''16 |
la'8. do''16 fa'8. do''16 mi''8. sol''16 do'''8. sol''16 |
la''8. do'''16 la''8. do'''16 fa''8. sib''16 fa''8. sib''16 |
sol''8. sol''16 sol''8. mi''16 fa''8. fa''16 sol''8. do''16 |
do''8. do'''16 sib''8. la''16 sol''8. sol''16 sol''8. mi''16 |
fa''8. re'''16 do'''8. sib''16 la''8. la''16 la''8. fad''16 |
sol''8. re''16 si'8. re''16 sol'8. sol''16 la''8. sol''16 |
fa''8. sol''16 la''8. fa''16 sol''4. sol''8 |
sol''4 sol'' r8 do'' mi''8. sol''16 |
sib''8. sib''16 la''8. do'''16 do'''8. do'''16 sib''8. la''16 |
la''8. la''16 la''8. la''16 la''8. fad''16 fad''8. la''16 |
re''8. re''16 re''8. re''16 sol''8. fa''16 mi''8. sol''16 |
do''8. do'''16 sib''8. la''16 sol''8. sol''16 sol''8. fa''16 |
mi''8. sol''16 mi''8. sol''16 do''8. do''16 do''8. do''16 |
do''8. do''16 do''8. do''16 do''8. la'16 la'8. la'16 |
sib'8. re''16 re''8. re''16 re''8. si'16 si'8. si'16 |
do''8. sol'16 do''8. mi''16 sol''8. mi''16 fa''8. fa''16 |
fa''8. fa''16 fa''8. mib''16 re''8. mib''16 fa''8. mib''16 |
re''8. do''16 sib'8. la'16 sol'4.\trill fa'8 |
fa'8 fa'' re'' mi''16 fa'' do''8 do'' sib' la'16 sol' |
la'8 fa' sib' sib' la' la' sol' fa'16 mi' |
fa'16 mi' fa' sol' la'8 do''16 sib' la'8 sib' sol' do'' |
la'2\trill r8 la' si'!8. dod''16 |
re''4 la''2 sol''8 fa'' |
mi''4. mi''8 mi''4. mi''8 |
la'8. la'16 sol'8. sol'16 fa'8. fa''16 dod''8. re''16 |
re''4 dod''8. re''16 re''8. fa''16 mi''8. re''16 |
do''8. do''16 do''8. do''16 do''8. la'16 la'8. la'16 |
sib'8. re''16 re''8. re''16 re''8. si'16 si'8. si'16 |
do''8. do''16 la'8. do''16 mi''8. sol''16 do'''8. sol''16 |
la''8. do'''16 la''8. do'''16 fa''8. sib''16 fa''8. sib''16 |
sol''8. sol''16 sol''8. mi''16 fa''8. fa''16 sol''8. do''16 |
do''8. do'''16 sib''8. la''16 sol''8. sol''16 sol''8. mi''16 |
fa''8. do''16 do''8. la'16 fa'8. fa'16 fa'8. fa'16 |
fa'8. sib'16 sib'8. sib'16 sib'8. re''16 re''8. sib'16 |
sol'8. sol'16 sol'8. sol'16 sol'8. sol''16 la''8. do'''16 |
fa''8. fa''16 sol''8. la''16 sib''4. do'''8 |
la''16 la' do'' la' fa' fa'' re'' fa'' fa'' do'' fa'' do'' mi'' sol'' do''' do'' |
fa'8 do'' re'' re'' do'' do'' sib' la'16 sol' |
la'4. do''16 sib' la'8 sib' sol' do'' |
la'2. la'4 |
