\clef "dessus" do''8. do'''16 la''8. do'''16 sol''8. do'''16 mi''8. do'''16 |
do''8. la''16 do''8. la''16 fa''8. re''16 sol''8. mi''16 |
fa''8. la''16 do'''8. la''16 sol''8. do'''16 mi''8. sol''16 |
do''8. fa''16 do''8. la''16 sib'8. re''16 sib'8. re''16 |
re''8. re''16 do''8. sol''16 la''8. la''16 mi''8. fa''16 |
mi''8. mi''16 mi''8. fa''16 sol''8. sib''16 mi''8. sol''16 |
do''8. fa''16 fa''8. sol''16 la''8. do'''16 fad''8. la''16 |
re''8. sol''16 re''8. si'16 do''8. do''16 do''8. sib'16 |
la'8. sol'16 fa'8. la'16 si'!8. re''16 do''8. si'16 |
do''8. sol'16 do''8. mi''16 sol''8. mi''16 sol''8. sib''16 |
sol''8. mi''16 fa''8. la''16 la''8. la''16 sol''8. sol''16 |
fa''8. la'16 re''8. re''16 re''8. re''16 re''8. fad'16 |
sol'8. sol''16 si'8. re''16 sol'8. sol''16 sol''8. mi''16 |
la''8. la''16 mi''8. fa''16 do''8. do''16 do''8. do''16 |
do''8. mi''16 do''8. mi''16 sol''8. mi''16 la''8. do'''16 |
fa''8. fa''16 fa''8. fa''16 fa''8. fa''16 fa''8. fa''16 |
fa''8. re''16 sol''8. sol''16 sol''8. sol''16 sol''8. sol''16 |
sol''8. mi''16 sol''8. mi''16 do''8. do''16 do''8. do''16 |
do''8. do''16 re''8. la'16 sib'4. la'8 |
sib'8. fa'16 sol'8. la'16 sib'4. do''8 |
la'8. do''16 fa'8. fa'16 fa'8 fa'' mi'' fa''16 sol'' |
do''8. la'16 fa'8. re''16 fa'8. do''16 mi'8. sib'16 |
la'16 do'' re'' mi'' fa''8. fa''16 fa''8. sol''16 mi''8.\trill fa''16 |
fa''2 r8 dod'' re''8. mi''16 |
fa''4 mi'' re''8. do''16 sib'4 |
la'4. la'8 la'4. sol'8 |
fa'8. fa'16 mi'8. mi'16 re'8. la'16 sib'8. la'16 |
la'4 la'8. sol'16 fa'4. fa''8 |
fa''8. fa''16 fa''8. fa''16 fa''8. fa''16 fa''8. fa''16 |
fa''8. sib''16 sol''8. sol''16 sol''8. sol''16 sol''8. sol''16 |
sol''8. do'''16 do'''8. la''16 sol''8. do'''16 sol''4 |
do''8. fa''16 do''8. la'16 sib'8. re''16 sib'8. re''16 |
re''8. re''16 do''8. sol''16 la''8. la''16 mi''8. fa''16 |
mi''8. mi''16 mi''8. fa''16 sol''8. sib''16 mi''8. sol''16 |
do''8. fa''16 la''8. fa''16 do''8. do''16 do''8. la'16 |
sib'8. re''16 fa''8. re''16 sol''8. sol''16 sib''8. sol''16 |
re''8. re''16 re''8. si'16 do''4. do''8 |
do''8. do''16 sib'8. la'16 sol'4. fa'8 |
fa'8 fa' sib' sib' la' la' sol' fa'16 mi' |
fa'8. do'''16 fa''8. re'''16 fa''8. do'''16 mi''8. do''16 |
fa''8. do'''16 fa''8. fa''16 fa''8. sol''16 mi''8.\trill fa''16 |
fa''8.\trill do''16 do''8. do''16 do''2 |
