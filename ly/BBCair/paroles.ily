Ve -- nez, ve -- nez, jus -- te dé -- pit, ve -- nez, ve -- nez à mon se -- cours,
ban -- nis -- sez de mon cœur un a -- mant in -- fi -- dè -- le,
ban -- nis -- sez de mon cœur un a -- mant in -- fi -- dè -- le.
Ve -- nez, ve -- nez, jus -- te dé -- pit, ve -- nez à mon se -- cours,
ban -- nis -- sez de mon cœur un a -- mant in -- fi -- dè -- le,
ve -- nez, ve -- nez, jus -- te dé -- pit, ve -- nez, ve -- nez à mon se -- cours,
ban -- nis -- sez de mon cœur un a -- mant in -- fi -- dè -- le.

Que de plus cons -- tan -- tes a -- mours
al -- lu -- ment dans mon âme u -- ne flam -- me nou -- vel -- le.
Ve -- nez, ve -- nez, jus -- te dé -- pit, ve -- nez, ve -- nez à mon se -- cours,
ve -- nez, ve -- nez, jus -- te dé -- pit, ve -- nez, ve -- nez à mon se -- cours,
ban -- nis -- sez de mon cœur un a -- mant in -- fi -- dè -- le,
ban -- nis -- sez de mon cœur un a -- mant in -- fi -- dè -- le.
