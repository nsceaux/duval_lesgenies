\piecePartSpecs
#`((basse #:instrument
          ,#{ \markup\center-column { [Basses et B.C.] } #})
   (dessus #:score-template "score-2dessus-bis"
           #:instrument "Violons")
   (dessus1 #:notes "dessus1" #:instrument "Violons")
   (dessus2 #:notes "dessus2" #:instrument "Violons")
   (parties #:on-the-fly-markup , #{

\markup\fontsize#-2 \fill-line {
  \column {
    \livretPers Lucile
    \livretVerse#12 { Venez, juste Dépit, venez à mon secours, }
    \livretVerse#12 { Bannissez de mon cœur un amant infidèle ; }
    \livretVerse#8 { Que de plus constantes amours }
    \livretVerse#12 { Allument dans mon âme une flamme nouvelle. }
    \livretVerse#12 { Venez, juste Dépit, venez à mon secours, }
    \livretVerse#12 { Bannissez de mon cœur un amant infidèle. }
  }
  \column {
    \livretPers Zerbin
    \livretVerse#8 { Mais, c’est lui qui vient dans ces lieux : }
    \livretVerse#12 { Pour connaître son cœur, cachez-vous à ses yeux. }
    \livretPers Lucile
    \livretVerse#12 { L’ingrat ! je l’aime encor, malgré son inconstance. }
    \livretPers Zerbin
    \livretVerse#8 { Venez, évitez sa présence. }
  }
}#})

   (silence #:on-the-fly-markup , #{

\markup\fontsize#-2 \column {
  \livretPers Lucile
  \livretVerse#12 { Venez, juste Dépit, venez à mon secours, }
  \livretVerse#12 { Bannissez de mon cœur un amant infidèle ; }
  \livretVerse#8 { Que de plus constantes amours }
  \livretVerse#12 { Allument dans mon âme une flamme nouvelle. }
  \livretVerse#12 { Venez, juste Dépit, venez à mon secours, }
  \livretVerse#12 { Bannissez de mon cœur un amant infidèle. }
}#}))
