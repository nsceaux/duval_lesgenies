\score {
  \new ChoirStaff <<
    \new GrandStaff \with { instrumentName = "Violons" } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \with { instrumentName = \markup\character Lucile } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*3\break s1*2 s2 \bar "" \pageBreak
        s2 s1*3\break s1*4\pageBreak
        s1*4\break s1*5\pageBreak
        s1*4\break s1*4 s2 \bar "" \pageBreak
        s2 s1*3 s2 \bar "" \break s2 s1*4\pageBreak
        s1*3
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
