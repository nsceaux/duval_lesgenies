\clef "vdessus" R1*2 |
r4 r8 fa' do''4. do''8 |
fa''2 re''4 re''8 re'' |
re''8. sol''16 mi''8. sol''16 do''8. do''16 sib'8. la'16 |
sol'4\trill r8 sol'16 la' sib'4 sib'8 do'' |
la'4. la'16 la' re''4 re''8. re''16 |
si'8\trill si' r sol''16 sol'' mi''4 fa''8 sol'' |
do''4 do''8. fa''16 re''4\trill mi''8. fa''16 |
mi''4\trill mi''8 r16 sol' sib'4. sol'8 |
do''2 re''4 re''8. mi''16 |
fa''4. la'8 re''8. la'16 si'8. do''16 |
si'4\trill r8 re''16 re'' re''4 mi''8 do'' |
fa''4 do''8 re'' sib'4\trill sib'8. la'16 |
sol'2\trill sol'8. do''16 la'8. fa'16 |
mib''2 do''4 re''8. mib''16 |
re''8. sol''16 si'!8. sol'16 fa''8. re''16 mi''8. fa''16 |
mi''2\trill r4 do''8. fa''16 |
la'4 sib'8. do''16 re''4 r8 sib'16 do'' |
re''4 mi''8. fa''16 fa''4.( mi''8) |
fa''2 r |
R1*3 |
la'8. si'16 do''8 la' re''4 re''8. mi''16 |
dod''4.\trill mi''8 sol''8. sol''16 fa''8.\trill mi''16 |
fa''4 dod''8. dod''16 re''4 mi''8. fa''16 |
fa''4( mi'')\trill re''8. fa''16 la'8. fa'16 |
mib''2 do''4 re''8. mib''16 |
re''8. sol''16 si'!8 sol' fa''8. re''16 mi''8. fa''16 |
mi''4\trill r8 fa' do''4 r8 do'' |
fa''2 re''4 re''8. re''16 re''8. sol''16 mi''8. sol''16 do''8. do''16 sib'8. la'16 |
sol'4 r8 sol'16 la' sib'4 sib'8. do''16 |
la'4.\trill do''16 re'' mib''4 mib''8. fa''16 |
re''2\trill re''4 r8 re''16 mi'' |
fa''4 re''8. sol''16 mi''4 fa''8. la'16 |
re''4 mi''8. fa''16 fa''4.( mi''8) |
fa''2 r |
R1*3 |
