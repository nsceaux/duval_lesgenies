\clef "basse" <>^"[B.C.]" fa2.~ |
fa2 fa~ |
fa mi8 fa sol sol, |
\ifConcert { \custosNote do8 \stopStaff }
\ifComplet {
  do2.~ |
  do~ |
  do4 si,2 |
  do2. |
  fa8 mib re do sib,2 do4 do, |
  fa,4. fa8 mi re |
  \once\set Staff.whichBar = "|"
}
