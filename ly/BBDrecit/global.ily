\set Score.currentBarNumber = 43 \bar ""
\key do \major
\digitTime\time 3/4 \midiTempo#80 s2.
\digitTime\time 2/2 s1*2
\digitTime\time 3/4
\ifConcert { s2 \bar "|." }
\ifComplet {
  s2.*4
  \time 3/2 \midiTempo#160 s1.
  \digitTime\time 3/4 \midiTempo#80 s2. \bar "|."
}
