Mais, c’est lui qui vient dans ces lieux :
pour con -- naî -- tre son cœur, ca -- chez- vous à ses yeux.
\ifComplet {
  L’in -- grat ! je l’aime en -- cor, mal -- gré son in -- cons -- tan -- ce.
  
  Ve -- nez, ve -- nez, é -- vi -- tez sa pré -- sen -- ce.
}
