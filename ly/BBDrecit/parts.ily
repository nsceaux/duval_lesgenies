\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix"
          #:indent 0)
   (parties #:on-the-fly-markup ,#{\markup\null#})

   ,(if (ly:get-option 'concert)
        `(silence #:on-the-fly-markup , #{
\markup\fontsize#-2 \column {
  \livretPers Zerbin
  \livretVerse#8 { Mais, c’est lui qui vient dans ces lieux : }
  \livretVerse#12 { Pour connaître son cœur, cachez-vous à ses yeux. }
}
       #})
        `(silence #:on-the-fly-markup , #{
\markup\fontsize#-2 \column {
  \livretPers Zerbin
  \livretVerse#8 { Mais, c’est lui qui vient dans ces lieux : }
  \livretVerse#12 { Pour connaître son cœur, cachez-vous à ses yeux. }
  \livretPers Lucile
  \livretVerse#12 { L’ingrat ! je l’aime encor, malgré son inconstance. }
  \livretPers Zerbin
  \livretVerse#8 { Venez, évitez sa présence. }
}
#})))
