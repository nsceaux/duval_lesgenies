\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s2. s1*2 s2.\break }
    >>
  >>
  \layout {
    indent = \noindent
    ragged-last = ##f
  }
  \midi { }
}
