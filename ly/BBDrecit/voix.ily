\ffclef "vtaille" <>^\markup\character Zerbin
do'8. do'16 la8 la sib sib16 do' |
re'4 r8 sib16 do' re'4 mi'8 fa' |
si!4 sol8 sol do'4 do'8 si |
do'2
\ifComplet {
  \ffclef "vdessus" <>^\markup\character Lucile
  r8 sol'' |
  mi''4\trill r16 do'' re'' mi'' fa''8. fa''16 |
  \appoggiatura mi''8 re''4. re''8 mi'' fa'' |
  mi''4\trill mi''
  \ffclef "vtaille" <>^\markup\character Zerbin
  r8 do' |
  la4 r8 la re'4 sib8 re' sol4 la8 sib |
  la4\trill la r |
}

