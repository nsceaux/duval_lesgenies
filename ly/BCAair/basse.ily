\clef "basse" do4. sol8 la si |
do' do mi do mi do |
sol sol, si, sol, si, sol, |
do la, dod la, dod la, |
re do si,4 sol, |
do8 la, re4 re, |
sol,8 sol si, sol, si, sol, |
do si, do re mi fa |
sol4 fa mi |
fa mi re |
do si, la, |
sol, sol8 fa mi4 |
fa sol sol, |
do4. sol8 la si |
do' do mi4 do |

<<
  \tag #'basse { sol,8 fa mi re do si, | do mi }
  \tag #'bc { sol,4 r r | do4 }
>> sol4 do' |
si8 la sol la si sol |
do'4 do8 re mi do |
fa mi fa sol fa mi |
<<
  \tag #'basse {
    re8 do re mi fa re |
    sol mi re do si, la, |
    sol, mi re do si, la, |
    sol,4 sol8 la si sol |
  }
  \tag #'bc {
    re2 re4 |
    sol sol sol |
    sol, sol sol |
    sol, sol sol |
  }
>>
do'4. si8 la4 |
sol sol sol |
sol, sol sol |
si,2 do4 |
la, fad, re, |
<<
  \tag #'basse {
    sol,4. sol8 re mi |
    fa mi re do si, la, |
    sol,2. |
  }
  \tag #'bc {
    sol,2.~ |
    sol, |
    sol,4. la,8 si, sol, |
  }
>>
do8 si, la,4. sol,8 |
re re' do' si la sol |
fad4. mi8 re si, |
do la, re4 re, |
<<
  \tag #'basse {
    sol,8 mi re do si, la, |
    sol, mi re do si, la, |
    sol,4 sol8 fa mi4 |
    fa8 mi fa
  }
  \tag #'bc {
    sol,2. |
    sol~ |
    sol4. fa8 mi4 |
    fa4.
  }
>> sol8 la fa |
sol sol, si,4 sol, |
do8 si, do re mi4 |
fa sol sol, |
do2.~ |
<<
  \tag #'basse {
    do4. mi8 re fa |
    mi8 sol fa4. sol16 la |
    sol8 do' si4. do'16 re' |
    do'8 si16 la sol8 fa
  }
  \tag #'bc {
    do2.~ |
    do~ |
    do~ |
    do4. re8
  }
>> mi4 |
fa sol sol, |
do4. re8 do si, |
la,2. |
