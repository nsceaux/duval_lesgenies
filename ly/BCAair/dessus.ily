\clef "dessus" r4 r sol' |
mi'8 sol' do'' mi'' re'' do'' |
si' re'' sol''4~ sol''16 fa'' mi'' fa'' |
mi''8 mi'' la'' mi'' fad'' sol'' |
fad'' re'' sol''4 si' |
mi'' la'4.\trill sol'8 |
sol' re'' sol''4. fa''8 |
mi'' sol'' do'' mi'' re'' do'' |
si'4\trill \appoggiatura la'8 sol'4 sol''8 mi'' |
la'' fa'' sol'' mi'' fa'' re'' |
mi'' do'' re'' si' do'' la' |
si'4. sol'8 sol''4 |
la'' re''4.\trill do''8 |
do''4 sol'8 la' sol' fa' |
mi' re' do' re' mi' fa' |
sol' la' sol' fa' mi' re' |
mi'4 re' do' |
sol'8 la' si' do'' re'' si' |
do'' re'' mi'' fa'' sol'' mi'' |
fa''4 do'' r8 la' |
re''4. do''8 do'' re'' |
si' sol'' fa'' mi'' re'' do'' |
re'' sol'' fa'' mi'' re'' do'' |
si'4. re''8 do'' si' |
do''4 sol' do'' |
si'8 do'' re'' do'' si' la' |
si' do'' re'' do'' si' la' |
sol'4. sol''8 \appoggiatura fa''8 mi''4~ |
mi''8 do''' \appoggiatura si''8 la''4 \appoggiatura sol''8 fad''4 |
sol''8 re''16 mi'' fa''8 mi'' re'' do'' |
re'' sol'' fa'' mi'' re'' do'' |
si' la' si' do'' re''4 |
sol'8 sol'' do'''4. si''8 |
la''4. re'''8 do''' si'' |
la'' sol'' fad'' sol'' la''8. si''16 |
si''8. la''16 la''4.\trill sol''8 |
sol'' sol'' fa'' mi'' re'' do'' |
si' sol'' fa'' mi'' re'' do'' |
si'4\trill \appoggiatura la'8 sol'4 sol''~ |
sol''8 do'' fa''4. mi''8 |
re'' mi'' re'' do'' si' re'' |
sol' sol'' sol'' fa'' mi'' sol'' |
la'' si'' si''4.\trill do'''8 |
do''' sol'' do'' sol'' la'' si'' |
do'''4. do''8 fa'' re'' |
sol''8 mi'' la''4.\trill sol''16 fa'' |
mi''8 do'' fa''4. mi''16 re'' |
sol''4. sol''8 do''4 |
fa'' re''4.\trill do''8 |
do''2. |
R2. |
