\piecePartSpecs
#`((basse #:score "score-basse")
   (dessus #:instrument
           ,#{\markup\center-column { Violons, Hautbois } #})
   (dessus2 #:instrument
            ,#{\markup\center-column { Violons, Hautbois } #})
   (silence #:on-the-fly-markup , #{

\markup\fontsize#-2 \column {
  \livretPers Léandre
  \livretVerse#8 { Reviens cher objet de mes vœux ; }
  \livretVerse#12 { Déjà l’astre du jour éteint ses feux dans l’onde, }
  \livretVerse#12 { Il est temps à mes vœux que ton amour réponde, }
  \livretVerse#8 { Viens rendre ton amant heureux. }
}#}))
