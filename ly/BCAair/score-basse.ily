\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses, Bassons] }
    } <<
      \global \keepWithTag #'basse \includeNotes "basse"
    >>
    \new Staff \with { instrumentName = "[B.C.]" } <<
      \global \keepWithTag #'bc \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { indent = \largeindent }
}
