\score {
  \new StaffGroupNoBar <<
    \new Staff \with {
      instrumentName = \markup\center-column { [Violons, Hautbois] }
    } << \global \includeNotes "dessus" >>
    \new Staff \with { instrumentName = \markup\character Léandre } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new StaffGroupNoBracket <<
      \new Staff \with {
        instrumentName = \markup\center-column { [Basses, Bassons] }
      } <<
        \global \keepWithTag #'basse \includeNotes "basse"
      >>
      \new Staff \with { instrumentName = "[B.C.]" } <<
        \global \keepWithTag #'bc \includeNotes "basse"
        \includeFigures "chiffres"
        \origLayout {
          s2.*9\break s2.*9\pageBreak
          \grace s8 s2.*6 s2 \bar "" \break s4 s2.*7\pageBreak
          s2.*7\break s2.*6\pageBreak
          s2.*6
        }
      >>
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
