\clef "vhaute-contre" R2.*12 |
r4 r r8 sol |
mi2.\trill |
r4 sol do' |
si2\trill do'8 re' |
sol2. |
r4 r r8 re' |
\appoggiatura re'8 mi'4. re'8 do' si |
la2\trill r8 do' |
fa'4. fa'8 mi' fa' |
re'4 re' r |
r r re'8 mi' |
fa'2 mi'8 re' |
mi' re' do' re' mi' fad' |
\appoggiatura fad' sol'2 sol'4 |
r4 r re' |
fa'4 \appoggiatura mi'8 re'4 r8 do'~ |
do' si16([ la]) re'4. mi'8 |
si2\trill r4 |
R2. |
r4 r sol'8 fa' |
mi'4 fad'4. sol'8 |
fad'2.\trill |
re'4 re'8 mi' fad' sol' |
sol'2~ sol'8[ fad'] |
sol'2. |
re'2 fa'4 |
\appoggiatura mi'8 re'4 \appoggiatura do'8 si4 do'~ |
do'8 la re'4. mi'8 |
si2\trill sol'4 |
\appoggiatura fa'8 mi'4.\trill re'8 do'4~ |
do'8 re' re'4.\trill do'8 |
do'2 r4 |
R2.*7 |
