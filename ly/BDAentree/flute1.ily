\clef "dessus" mi''4 re'' r8 fa'' |
\appoggiatura fa'' mi''2 r8 mi' |
la' do'' si' do'' re'' si' |
do''8.*5/6 la'32 si' dod'' dod''4.\trill( si'16 dod'') |
re''2 re''8 do'' |
si' do'' re'' do'' si' la' |
sold'\trill mi' sold' si' mi'' sol''! |
dod'' la' dod'' mi'' la''4 |
\appoggiatura la''8 sol''4.\trill fa''8 sol'' mi'' |
\appoggiatura mi'' fa''4. la'8 re'' fa'' |
si' sol' si' re'' sol''4 |
\appoggiatura sol''8 fa''4.\trill mi''8 fa'' re'' |
mi'' sol'' fa'' mi'' re'' do'' |
la'' sol'' fa'' mi'' re'' do'' |
fa''4. mi''16 re'' mi''4~ |
mi''8 fa'' re''4.\trill do''8 |
do'' mi'' mi''4.(\trill re''16 mi'') |
fa''4 r8 fad'' sold'' la'' |
sold''4.\trill mi''8 la'' mi'' |
fa'' mi'' re'' do'' si' la' |
re''4 sold'8 mi'' do''4\trill~ |
do''8 si' si'4.\trill la'8 |
la'2
