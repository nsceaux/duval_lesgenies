\clef "dessus" do''4 si' r8 re'' |
do''2 si'8 mi'' |
do'' la' sold' la' si' sold' |
la'8.*5/6 dod''32 re'' mi'' mi''4.(\trill re''16 mi'') |
fa''2 fa''8 mi'' |
re'' mi'' fa'' mi'' re'' do'' |
si'4 mi'' si' |
mi'4. la'8 dod'' mi'' |
mi''4 dod''4.(\trill si'16 dod'') |
re''4. fa''8 la''4 |
re''8 si' sol' si' re''4 |
re'' si'4.(\trill la'16 si') |
do''8 mi'' re'' do'' si' do'' |
fa'' mi'' re'' do'' si' la' |
si' sol' la' si' do''4 |
do'' si'4.\trill do''8 |
do''16 la' si' do'' dod''4.(\trill si'16 dod'') |
re''16 si' dod'' re'' red''4.(\trill dod''16 red'') |
mi''4 si' do'' |
la' fa''8 mi'' re'' do'' |
si'4 mi'' la'~ |
la'8 sold' sold'4.\trill la'8 |
la'2
