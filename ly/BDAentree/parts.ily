\piecePartSpecs
#`((dessus #:score "score")
   (dessus1 #:notes "violon")
   (dessus2 #:notes "violon")

   (flute-hautbois #:score "score-flutes")
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
