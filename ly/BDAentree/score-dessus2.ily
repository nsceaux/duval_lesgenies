\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Flûtes" } <<
      \global \includeNotes "flute2"
    >>
    \new Staff \with { instrumentName = "Violons" } <<
      \global \includeNotes "violon"
    >>
  >>
  \layout { indent = \largeindent }
}
