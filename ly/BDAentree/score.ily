\score {
  \new StaffGroup <<
    \new GrandStaff \with { instrumentName = "Flûtes" } <<
      \new Staff << \global \includeNotes "flute1" >>
      \new Staff << \global \includeNotes "flute2" >>
    >>
    \new Staff \with { instrumentName = "Violons" } <<
      \global \includeNotes "violon"
      \origLayout {
        s2.*8\break \grace s8 s2.*8\pageBreak
        s2.*6
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
