\clef "dessus2" la'2 sold'4 |
la'8 la do' re' mi'4 |
la mi' r8 mi' |
la'2 la'4 |
re'2 re'8 mi' |
fa'4 re'2 |
mi'2 mi'4 |
la'2 la'4 |
dod'4 la2 |
re'4 fa' re' |
sol'2. |
si4 sol2 |
do'4 r r |
R2. |
sol'4. fa'8 mi'4 |
fa' sol'2 |
do'4 la' la |
re' si' si |
mi'4. re'8 do' la |
re' do' re' mi' fa' re' |
sold2 la4 |
re' mi'2 |
la
