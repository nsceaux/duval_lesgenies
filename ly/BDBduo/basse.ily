\clef "basse"
\ifComplet {
  la8 si |
  do'4 si la |
  sold mi la8 sol? |
  fa4~ fa8 mi fa re |
  mi4. fa8 mi re |
  do4. si,8 do4 la, |
  si, si red si, |
  mi4. red8 mi fad |
  sol fad sol la si sol |
  do' la si4 si, |
  mi sold mi |
  la si8 do' sold4 la |
  mi4 re8 do si,4 mi |
  la,8 mi la sol fa mi |
  re do re mi fa re |
  mi4 mi,2 |
  la,2~ la,8 sol, |
  fa,2 re, |
  mi,2 mi8 re |
  dod2. |
  re4. mi8 fa2 re |
  mi r4 | \allowPageTurn
  r r r8 do' |
  si2 la8 sold |
  la sol fa mi fa re |
  mi4 mi,4. mi8 |
  la sol fa4. mi8 |
  re mi fa2 |
  mi r4 | \allowPageTurn
  r r r8 mi' |
  fa'4. mi'8 re' dod' |
  re'4 si sol |
  do'4. sol8 mi sol |
  do4 re2 |
  mi8 la, mi,2 |
}
la,4. si,8 dod la, |
mi4. fad8 mi re |
dod2 re4 |
mi2 mi,4 |
la,8 sold, la, si, dod si, |
la,4 la2~ |
la sold8 la |
mi4. re8 dod si, |
la,2 mi4 |
si,2 fad4 |
dod la, la |
sold4. fad8 sold mi |
la2. |
fad4 si si, |
mi mi,8 mi sol4 |
re2 mi4 |
fad4. mi8 re dod |
si,4 lad,2 |
si,4. fad8 sold lad |
si4 si,8 dod re si, |
sol2. |
mi4 fad fad, |
si, mi8 re dod si, |
la,2 r8 la |
sold2 mi4 |
la8 dod' la mi dod mi |
la,4 la2~ |
la2. | \allowPageTurn
sold8 la si4 si, |
mi8 fad sold mi la la, |
mi2 la,4 |
mi, mi8 fad mi re |
dod2 re4 |
mi mi,2 |
la, la4 |
la la, la |
sold4. fad8 mi4 |
la,2 la4 |
sold4. fad8 mi4 |
la dod' la |
re8 dod si,4. la,8 |
mi mi sold si mi mi' |
dod' si la si dod' re'16 dod' |
si8 la sold la si dod'16 si |
la4 la, la |
mi mi, mi |
la,2 la4 |
re2 re4 |
la2 la,4 |
re8 dod si,4. la,8 |
mi mi re dod si, dod |
re2.~ |
re2 dod4 |
re mi mi, |
la, la la, |
la,8 si, dod re mi re |
dod si, la, si, dod4 |
si, si,2\trill |
la,8 mi fad sold la si |
dod' si la si dod' re' |
mi'2 mi4 |
re2 dod4 |
re mi mi, |
\ifConcert { la,2. }
\ifComplet {
  la,2 sol,4 |
  fa,2. |
  fa8 mi fa sol la sib |
  do'4 do8 re mi do |
  fa mi re do sib, la, |
  re do sib, la, sol, fa, |
  do4 do,2 fa,2. la,4 |
  sib,2 sib,4 |
  la,4. sol,8 fa,4 |
  sib, fa8. sol16 la8. sib16 do'8 do |
  fa2 fad4 |
  sol2 sold4 |
  la2 la,4 |
  re2 sol4 |
  fa mi4. re8 |
  la,2. |
}

