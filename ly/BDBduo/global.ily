\ifComplet {
  \key la \minor
  \once\omit Staff.TimeSignature
  \digitTime\time 3/4 \partial 4 \midiTempo#120 s4 s2.*4
  \digitTime\time 2/2 s1*2
  \digitTime\time 3/4 \grace s8 s2.*4
  \digitTime\time 2/2 s1*2
  \digitTime\time 3/4 \grace s8 s2.*3 s2.
  \digitTime\time 2/2 s1
  \digitTime\time 3/4 s2.*2
  \time 3/2 \grace s8 s1.
  \digitTime\time 3/4 s2.*14 \bar "||"
}
\ifConcert { \digitTime\time 3/4 \midiTempo#120 }
\key la \major
\beginMarkSmall "Duo"
\ifConcert { s2.*64 \bar "|." }
\ifComplet {
  s2.*65 \bar "||"
  \key do \major \digitTime\time 3/4 s2.*5
  \time 4/4 \midiTempo#80 s1
  \digitTime\time 3/4 s2.*2
  \time 4/4 s1
  \digitTime\time 3/4 s2.*6 \bar "|."
}
