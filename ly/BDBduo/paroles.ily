\ifComplet {
  \tag #'(leandre basse) {
    Qu’é -- loi -- gné de vo -- tre pré -- sen -- ce,
    j’ai souf -- fert de maux ri -- gou -- reux !
    Mais que ces maux sont doux lors -- qu’a -- près votre ab -- sen -- ce,
    je re -- vois en -- cor vos beaux yeux !
    Mais que ces maux sont doux lors -- qu’a -- près votre ab -- sen -- ce,
    je re -- vois en -- cor vos beaux yeux !
  }
  \tag #'(nymphe basse) {
    Ah ! quel a -- veu char -- mant, qu’il m’est doux de l’en -- ten -- dre !
    A -- mour, a -- mour, mes vœux sont sa -- tis -- faits,
    la gloi -- re de ré -- gner sur un cœur aus -- si ten -- dre
    est le plus cher de tes bien -- faits,
    la gloi -- re de ré -- gner sur un cœur aus -- si ten -- dre
    est le plus cher de tes bien -- faits.
  }
}
A -- mour, viens nous u -- nir de tes plus dou -- ces chaî -- nes,
\tag #'(nymphe basse) {
  vo -- le, ré -- ponds à nos dé -- sirs ;
}
vo -- le, vo -- le, ré -- ponds à nos dé -- sirs ;
nos cœurs ne sont point faits pour é -- prou -- ver tes pei -- nes,
\tag #'leandre { ne nous of -- fre, }
ne nous of -- fre que tes plai -- sirs.

Nos cœurs ne sont point faits pour é -- prou -- ver tes pei -- nes,
ne nous of -- fre que tes plai -- sirs.
A -- mour, \tag #'(leandre basse) { a -- mour, }
viens nous u -- nir de tes plus dou -- ces chaî -- nes,
\tag #'(nymphe basse) {
  vo -- le,
}
\tag #'leandre {
  a -- mour, a -- mour, viens nous u -- nir de tes plus dou -- ces chaî -- nes,
}
ré -- ponds à nos dé -- sirs ;

\tag #'basse { Vo -- }
Vo -- le, vo -- le, ré -- ponds à nos dé -- sirs,
vo -- le, ré -- ponds à nos dé -- sirs
vo -- le, vo -- le, ré -- ponds à nos dé -- sirs.

\ifComplet\tag #'(nymphe basse) {
  Nym -- phes, vous qui for -- mez ma cour la plus bril -- lan -- te,
  vous ha -- bi -- tants des mers qui vi -- vez sous mes lois,
  ras -- sem -- blez- vous trou -- pe char -- man -- te,
  ve -- nez, ac -- cou -- rez __ à ma voix.
}
