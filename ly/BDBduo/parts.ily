\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix"
          #:instrument "B.C.")
   (silence #:on-the-fly-markup , #{

\markup\fontsize#-3 \fill-line {
  \column {
    \livretPers Léandre
    \livretVerse#8 { Qu’éloigné de votre présence, }
    \livretVerse#8 { J’ai souffert de maux rigoureux ! }
    \livretVerse#12 { Mais que ces maux sont doux lorsqu’après votre absence, }
    \livretVerse#8 { Je revois encor vos beaux yeux ! }
    \livretPers La principale Nymphe
    \livretVerse#12 { Ah ! quel aveu charmant, qu’il m’est doux de l’entendre ! }
    \livretVerse#8 { Amour, mes vœux sont satisfaits, }
    \livretVerse#12 { La gloire de régner sur un cœur aussi tendre }
    \livretVerse#8 { Est le plus cher de tes bienfaits. }
  }
  \column {
    \livretPers Ensemble
    \livretVerse#12 { Amour, viens nous unir de tes plus douces chaînes, }
    \livretVerse#8 { Vole, réponds à nos désirs ; }
    \livretVerse#12 { Nos cœurs ne sont point faits pour éprouver tes peines, }
    \livretVerse#8 { Ne nous offre que tes plaisirs. }
    \livretPers La principale Nymphe
    \livretVerse#12 { Nymphes, vous qui formez ma cour la plus brillante, }
    \livretVerse#12 { Vous habitants des mers qui vivez sous mes lois, }
    \livretVerse#8 { Rassemblez-vous troupe charmante, }
    \livretVerse#8 { Venez, accourez à ma voix. }
  }
}#}))
