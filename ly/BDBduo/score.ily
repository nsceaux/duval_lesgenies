\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \with {
      instrumentName = \markup\center-column\smallCaps { La principale Nymphe }
    } \withLyrics <<
      \global \keepWithTag #'nymphe \includeNotes "voix"
    >> \keepWithTag #'nymphe \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\character Léandre
    } \withLyrics <<
      \global \keepWithTag #'leandre \includeNotes "voix"
    >> \keepWithTag #'leandre \includeLyrics "paroles"
    \new Staff \with { instrumentName = "B.C." } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \ifComplet\modVersion { 
        s4 s2.*4 s1*2 s2.*4 s1*2 s2.*3 s2. s1 s2.*2 s1. s2.*14\break
        s2.*65\break
      }
      \origLayout {
        s4 s2.*4\pageBreak
        s1*2 s2. s2 \bar "" \break s4 s2.*2 s1*2\break \grace s8 s2.*4 s1\pageBreak
        s2.*2 s1. s2.*2\break s2.*6\break s2.*6\pageBreak
        s2.*5\break s2.*7\pageBreak
        s2.*5\break \grace s8 s2.*6\pageBreak
        s2.*5 s4 \bar "" \break s2 s2.*6\pageBreak
        s2.*7\break s2.*7\pageBreak
        s2.*7\break s2.*9\pageBreak
        s2.*5 s1 s2.\break \grace s8 s2. s1 s2.\break s2.*5
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
