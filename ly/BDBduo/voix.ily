\ifComplet {
  <<
    \tag #'(leandre basse) {
      \clef "vhaute-contre" \tag #'basse <>^\markup\character Léandre
      do'8 re' |
      mi'4 fa'8 mi' re' do' |
      si4\trill si do'8 mi' |
      la si do'4 si8 la |
      sold2.\trill |
      mi'4 fad'8 sol' fad'4.\trill mi'8 |
      red'4 si8 si fad'4 mi'8 fad' |
      \appoggiatura fad'8 sol'2 \appoggiatura fad'!8 mi'4 |
      r4 si8 dod' red' si |
      mi'4 red'4.\trill mi'8 |
      mi'2. |
      do'4 re'8 mi' \appoggiatura mi'8 re'4.\trill do'8 |
      si4 mi'8 fa' \appoggiatura mi'8 re'4\trill do'8 si |
      \appoggiatura si8 do'2 \appoggiatura si8 la4 |
      r4 fa'8 mi' re' \appoggiatura do' si |
      \appoggiatura si do'4 si4.\trill la8 |
      la4
    }
    \tag #'nymphe { r4 R2.*4 R1*2 R2.*4 R1*2 R2.*3 r4 \ffclef "vdessus" }
  >>
  <<
    \tag #'(nymphe basse) {
      \tag #'basse \ffclef "vdessus"
      <>^\markup\character La principale Nymphe
      mi''8 do''16 do'' re''8 mi'' |
      la'4 re''8 re'' \appoggiatura do'' si'4\trill si'8 si' |
      sold'4\trill sold' r8 si' |
      mi''2( re''8) mi'' |
      \appoggiatura mi''8 fa''4 r8 fa'' \appoggiatura mi'' re''2\trill re''4 re''8 re'' |
      si'2\trill r8 mi'' |
      \appoggiatura mi'' re''4.\trill do''8 si' la' |
      re''2 do''8 si' |
      do''2 si'8 la' |
      sold'2\trill \appoggiatura fad'8 mi'4 |
      do'' re''4. mi''8 |
      fa''8 mi'' re''4.\trill mi''8 |
      mi''2 r8 mi'' |
      fa''4. mi''8 re'' dod'' |
      re''2 re''8 mi'' |
      fa''4 mi''4.\trill re''8 |
      \appoggiatura re'' mi''2 \appoggiatura re''8 do''4 |
      mi'' \appoggiatura re''8 do''4 \appoggiatura si'8 la'4 |
      sold'8\trill do'' si'4.\trill la'8 |
      la'2
    }
    \tag #'leandre { r4 r R1 R2.*2 R1. R2.*14 r4 r }
  >>
}
\ifConcert {
  \tag #'(nymphe basse) { \ffclef "vdessus" r4 r }
  \tag #'leandre { \clef "vhaute-contre" r4 r }
}
<<
  \tag #'(nymphe basse) {
    r8 dod'' |
    si'2.\trill |
    mi''2 re''8 dod'' |
    si' la' si' dod'' re'' \appoggiatura dod'' si' |
    \appoggiatura si' dod''2 \appoggiatura si'8 la'4 |
    mi''2 re''8 dod'' |
    fad'' mi'' \appoggiatura mi'' re''4. dod''8 |
    si'2\trill r4 |
    mi''8[\melisma fad'' mi'' re''16 dod'' si'8 dod''] |
    re''[ mi'' re'' dod''16 si' la'8 si'] |
    dod''8.[ re''16 dod''8 si' dod''8. la'16]( |
    si'2)\melismaEnd mi'4 |
    mi''2 re''8 dod'' |
    fad'' mi'' red''4.\trill mi''8 |
    mi''2 r8 si' |
    re''4. dod''8 si' dod'' |
    lad'2\trill r4 |
    fad''4 mi''8 re'' mi'' \appoggiatura re'' dod'' |
    \appoggiatura dod'' re''2 \appoggiatura dod''8 si'4 |
    r4 re''4. fad''8 |
    \appoggiatura fad''8 mi''2\trill \appoggiatura re''8 dod''4~ |
    dod''8 re'' dod''4.\trill si'8 |
    si'2 r8 mi'' |
    mi''4. fad''8 re''\trill dod'' |
    re'' mi'' re'' dod'' re'' si' |
    \appoggiatura si' dod''2 \appoggiatura si'8 la'4 |
    r dod''4. dod''8 |
    fad''2 \appoggiatura mi''8 red''4~ |
    red''8 mi'' red''4.\trill mi''8 |
    mi''2 <<
      \tag #'basse { s4 s2 \ffclef "vdessus" <>^\markup\character La Nymphe }
      \tag #'nymphe { r4 | r r }
    >> r8 dod'' |
    si'2.\trill |
    mi''2 re''8 dod'' |
    si'\trill la' si' dod'' re'' \appoggiatura dod'' si' |
    \appoggiatura si' dod''2 \appoggiatura si'8 la'4 |
    dod''8[\melisma si' dod'' re'' mi'' fad''16 mi''] |
    re''8[ dod'' si' dod'' re'' mi''16 re''] |
    dod''8[ si' la' si' dod'' re''] |
    mi''4 mi''2\trill~ |
    mi''4\melismaEnd mi'' r8 dod'' |
    fad''8. mi''16 re''4.\trill dod''8 |
    si'2.\trill |
    <<
      \tag #'basse { s2.*2 \ffclef "vdessus" <>^\markup\character La Nymphe }
      \tag #'nymphe R2.*2
    >>
    mi''8[\melisma re'' dod'' re'' mi'' fad''16 mi''] |
    re''8[ dod'' si' dod'' re'' mi''16 re'']( |
    dod''2)\trill\melismaEnd \appoggiatura si'8 la'4 |
    fad''8[ mi'' re'' mi'' fad'' mi''16 re'']( |
    mi''4) dod''\trill r8 mi'' |
    la'8 \appoggiatura sold' fad' \appoggiatura fad' sold'4. la'8 |
    sold'2.\trill |
    si'4 si' r8 re'' |
    \appoggiatura dod''8 \once\tieDashed si'2~ si'8 la' |
    la'4( sold'4.\trill) la'8 |
    la'4 dod''2~ |
    dod''\trill si'4 |
    mi''8[\melisma re'' dod'' re'' mi'' fad''16 mi''] |
    re''8[ dod'' si' dod'' re'' mi''16 re''] |
    dod''8[ si' la' si' dod'' re''] |
    mi''2.~ |
    mi''4\melismaEnd si' r8 re'' |
    \appoggiatura dod'' si'2~ si'8 la' |
    \once\tieDashed la'4( sold'4.)\trill la'8 |
    la'2. |
  }
  \tag #'(leandre basse) {
    <<
      \tag #'basse { s4 s2.*28 s2 \ffclef "vhaute-contre" <>^\markup\character Léandre }
      \tag #'leandre {
        r8 la |
        sold2.\trill |
        dod'2 si8 la |
        sold fad sold la si \appoggiatura la sold |
        la2 la4 |
        dod'8[\melisma si la dod' si la] |
        re'[ dod' si dod' si la] |
        \appoggiatura la8 sold2\melismaEnd \appoggiatura fad8 mi4 |
        dod'8[\melisma re' dod' si16 la sold8 la] |
        si[ dod' re' mi'16 re' dod'8 re'] |
        mi'8.[ fad'16 mi'8 re' dod' mi'] |
        re'[ dod' si dod' re' si]( |
        dod'4)\melismaEnd \appoggiatura si8 la4 r8 mi' |
        la' sold' fad'4.\trill mi'8 |
        mi'2 r8 re' |
        fad'4. mi'8 re' mi' |
        dod'\trill lad si dod' re' mi' |
        \appoggiatura mi'8 re'4 \grace dod'8 dod'4\trill dod'8 fad' |
        fad'2 \appoggiatura mi'8 re'4 |
        r4 fad'4. re'8 |
        \appoggiatura re'8 dod'2\trill \appoggiatura si8 lad4~ |
        lad8 si lad4. si8 |
        si2 r8 dod' |
        dod'4. re'8 si\trill la |
        si dod' si la si sold |
        \appoggiatura sold la2 la4 |
        r mi'4. mi'8 |
        la'2 \appoggiatura sold'8 fad'4~ |
        fad'8 sold' fad'4.\trill mi'8 |
        mi'2
      }
    >> r8 dod' |
    si2\trill <<
      \tag #'basse { s4 s2.*11 \ffclef "vhaute-contre" <>^\markup\character Léandre }
      \tag #'leandre {
        r8 la |
        sold2.\trill |
        dod'2 si8 la |
        sold\trill fad sold la si \appoggiatura la sold |
        la2 la8 dod' |
        mi'2 r8 dod' |
        si2.\trill |
        mi'2 re'8 dod' |
        si\trill la si dod' re' si |
        dod'4 \appoggiatura si8 la4 r8 mi' |
        la' \appoggiatura sold' fad' \appoggiatura fad' sold'4. la'8 |
        sold'2.\trill |
      }
    >>
    mi'8[\melisma re' dod' re' mi' fad'16 mi'] |
    re'8[ dod' si dod' re' mi'16 re'] |
    <<
      \tag #'basse { \melismaEnd s2.*5 }
      \tag #'leandre {
        dod'8[ si la si dod' re'16 dod']( |
        si2)\melismaEnd \appoggiatura la8 sold4 |
        mi'8[\melisma re' dod' re' mi' fad'16 mi'] |
        re'8[ dod' si dod' re' dod'16 si]( |
        dod'4)\melismaEnd \appoggiatura si8 la4 r8 mi' |
        fad'8. mi'16 \appoggiatura mi'8 re'4.\trill dod'8 |
        si2.\trill |
        sold'4 sold' r8 sold' |
        sold'2~ sold'8 la' |
        dod'4( si4.)\trill la8 |
        la4 la8[\melisma si dod' re'] |
        mi'2.~ |
        mi'2\trill~ mi'8[ re'16 mi'] |
        fad'8[ mi' re' dod' si dod'16 re']( |
        mi'2)\melismaEnd mi'4 |
        la'2~ la'16[ sold' fad' sold']( |
        sold'4\trill) sold' r8 sold' |
        sold'2~ sold'8 la' |
        dod'4( si4.)\trill la8 |
        la2. |
      }
    >>
  }
>>
\ifComplet {
  R2.*6 |
  <<
    \tag #'(nymphe basse) {
      <>^\markup\character La principale Nymphe
      do''4 do''8 r8 r16 la' la' sib' do''8. do''16 |
      re''4. re''8 re'' mi'' |
      \appoggiatura mi''8 fa''2 fa''4 |
      re''8 re''16 re'' la'8. sib'16 do''8 sib'16 la' sol'8 la'16 sib' |
      la'4\trill r8 la' la' re'' |
      si'4\trill si' dod''8 re'' |
      dod''4\trill dod'' r8 la' |
      fa''2 mi''8 sol'' |
      si'16[ do'' si' do''] dod''4.\trill re''8 |
      \appoggiatura re''8 mi''2. |
    }
    \tag #'leandre { R1 R2.*2 R1 R2.*6 }
  >>
}
