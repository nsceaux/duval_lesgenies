\clef "dessus" re'2.~ |
re'~ |
re' |
fa'4 sol'8 la' sib' sol' |
la'4 sol'8 fa' mi' re' |
dod'2 la'8 sol' |
fa'4 re'2 |
sol'4 r r |
la' r r |
sib'8 la' sol' la' sib' sol' |
la'2 fa''8 mi'' |
re'' mi'' re'' do'' si' la' |
mi''4 mi'8 mi'' re''4 |
do'' la' r8 la' |
re''2. |
mi''4 mi'' mi' |
la'2 la'4 |
re''8 sol'' la'4 la |
re'8 re'' do'' sib' la' sol' |
fad'4 re'' re' |
sol'2 sib'8 la' |
sol' la' sol' fa' mi' re' |
la'4 la'4. re''8 |
\appoggiatura do''8 si'2 dod''8 re'' |
mi'' fa'' sol'' fa'' mi'' re'' |
dod''2\trill dod''8 re'' |
la'4 la dod''8 re'' |
la'2 la'4 |
re'' re'' re' |
sol'8 la' sib' la' sol' fa' |
do'' sib' do'' re'' mi'' do'' |
fa''4 fa'' r8 la' |
si'2. |
si'8 re'' dod'' la' re'' re' |
la'2 fa'4 |
do''8 sib' la' sib' do'' la' |
re'' do'' sib' do'' re'' mi'' |
fa''2 fa''8 mi'' |
re''4. re''8 sol'' sol' |
do''4 mi'' do'' |
fa''8 mi'' re'' do'' sib' la' |
re'' do'' sib' la' sol' fa' |
do''4. sib'8 la'4 |
sib' do'' do' |
fa'2. |
fa'2 fa''4 |
re''2 re'4 |
sol'2 mi''8 mi' |
la' si' dod''4 la' |
re''4. do''8 sib'4 |
la' sol'4.(\trill fa'16 sol') |
la'4 la' sol' |
fa'2 r4 |
r r la' |
re''2 re'4 |
sol'2 mi''8 mi' |
la' si' dod''4 la' |
re''4. do''8 sib'4 |
sol' la'4. la'8 |
re'2. |
re'2 la'4 |
re''8 mi'' fa'' re'' mi'' dod'' |
re'' mi'' fa'' re'' mi'' dod'' |
re''4. do''8 sib'4 |
sol' la' la |
re'4

