\clef "dessus" r8 re'' fa'' mi'' re'' dod'' |
re'' fa'' la'' sol'' fa'' mi'' |
fa'' la'' re'' mi'' fa'' sol'' |
la''4~ la''8 la'' fa'' sol'' |
mi''4.\trill fa''8 sol'' la'' |
sib'' la'' sol'' fa'' mi''8.(\trill re''32 mi'') |
fa''16 re'' mi'' fa'' fad''4.(\trill mi''16 fad'') |
sol''4. fa''8 mi'' re'' |
dod'' si' dod'' re'' mi'' fa'' |
sol'' la'' sib'' la'' sol'' fa'' |
mi''4\trill mi'' la''8 la'' |
la''4. la''8 si'' do''' |
si'' la'' si'' do''' re''' si'' |
do''' la'' do''' mi''' do''' mi''' |
mi'''4 re'''4.\trill do'''8 |
do'''4( si''4.\trill) la''8 |
la'' sol'' fa'' mi'' re'' dod'' |
re'' si' dod''4.\trill re''8 |
re''4 r r |
r r la'' |
re'' sib' re''8 re'' |
re''4. re''8 mi'' fa'' |
dod''4\trill la'4. la''8 |
la''2 sol''8 fa'' |
mi''4.\trill re''8 mi'' fa'' |
sol'' la'' sib'' la'' sol'' fa'' |
mi''2\trill mi''8 fa'' |
dod''2\trill dod''4 |
re'' fad''4. fad''8 |
sol''4. la''8 sib''8 la'' |
sol'' mi'' do'' mi'' sol'' do''' |
la'' fa'' la'' do''' fa'' la'' |
la''4. sol''8 sol'' sol'' |
sol''4. la''8 fa''4 |
mi''\trill mi'' r |
r r do''' |
la'' re'''4. re'''8 |
do'''4 la'' do''' |
do'''4. do'''8 sib'' sib'' |
sib'' la'' sol'' la'' sib'' do''' |
la''2\trill r4 |
r r8 fa'' sib'' la'' |
sol'' sib'' la'' sib'' do''' la'' |
re''' sol'' sol''4.\trill fa''8 |
fa''8 do'' la' do'' fa' do'' |
fa'' sol'' la''4 r |
r4 r r8 la'' |
re''2 r8 si'' |
mi''2 r8 la'' |
fad''4. fad''8 sol''4 |
la'' sib''8 la'' sol'' fa'' |
mi''2\trill la'4 |
re''8 mi'' fa'' sol'' la'' re'' |
dod'' re'' mi'' fa'' sol'' mi'' |
fa''2 r8 la'' |
re''2 r8 si'' |
mi''2 mi''4 |
fa''4. fa''8 sol'' la'' |
sib''4 mi''4.\trill re''8 |
re''8 re' fa' la' re'' mi'' |
fa'' sol'' la'' fa'' sol'' mi'' |
fa'' sol'' la'' fa'' sol'' mi'' |
fa'' sol'' la'' fa'' sol'' mi'' |
fa''4. fa''8 sol'' la'' |
sib'' mi'' mi''4.\trill re''8 |
re''4

