\clef "dessus" R2. |
r8 re'' fa'' mi'' re'' dod'' |
re'' fa'' la'' sol'' fa'' mi'' |
re'' fa'' mi'' fa'' re'' mi'' |
dod''4.\trill re''8 mi'' fa'' |
sol'' fa'' mi'' re'' dod''8.(\trill si'32 dod'') |
re''4 re''4. re''8 |
re''4 sol''8 la'' sol'' fa'' |
mi''\trill re'' mi'' fa'' sol'' fa'' |
mi'' fa'' sol'' fa'' mi'' re'' |
dod''\trill la' dod'' mi'' re'' mi'' |
fa''4. fad''8 sold'' la'' |
sold'' fad'' sold'' la'' si'' sold'' |
la'' mi'' la'' do''' la'' do''' |
do'''4 si''4.\trill la''8 |
la''4( sold''4.)\trill la''8 |
la'' dod'' re'' mi'' fa'' sol'' |
fa''\trill mi'' mi''4.\trill re''8 |
re''4 r r |
r r fad''4 |
sol'' re'' sol''8 la'' |
sib'' do''' sib'' la'' sol'' fa'' |
mi''4\trill mi''4. fa''8 |
fa''2 mi''8 re'' |
dod''4.\trill si'8 dod'' re'' |
mi'' fa'' sol'' fa'' mi'' re'' |
dod''2\trill r4 |
r r mi'' |
la' la''4. la''8 |
re''4. re''8 mi''8 fa'' |
mi'' do'' mi'' sol'' do''' mi'' |
fa'' do'' la' fa' la' do'' |
re'' fa'' re'' si' sol' sol'' |
re'' mi''16 fa'' mi''4\trill re'' |
dod''4\trill \appoggiatura si'8 la'4 r |
r r la'' |
fa''4 fa''4. sib''8 |
la''4 \appoggiatura sol''8 fa''4. la''8 |
la''4. la''8 sol'' sol'' |
sol'' fa'' mi'' fa'' sol'' mi'' |
fa''2 r8 do'' |
fa'' mi'' re'' re'' mi'' fa'' |
mi'' sol'' fa'' sol'' la'' fa'' |
fa''4 mi''4.\trill fa''8 |
fa''4. do''8 la' do'' |
fa' do'' fa''4 r |
r4 r r8 fad'' |
sol''2 r8 sold'' |
la''2 r8 dod'' |
re''4. re''8 mi''4 |
fad'' sol''8 \ficta fa'' mi'' re'' |
dod''2\trill r4 |
r r re' |
la'8 si' dod'' re'' mi'' dod'' |
re''2 r8 fad'' |
sol''2 r8 sold'' |
la''2 dod''4 |
re''4. re''8 mi''4~ |
mi''8 fa'' dod''4. re''8 |
re''4. re'8 fa' la' |
re'' mi'' fa'' re'' mi'' dod'' |
fa'4 re''8 fa'' dod'' mi'' |
re'' dod'' re'' fa'' dod'' mi'' |
re''4. la'8 re''4 |
mi''8 re'' dod''4.\trill re''8 |
re''4
