\piecePartSpecs
#`((dessus #:score "score-dessus")
   (dessus1)
   (dessus2)
   (flute-hautbois #:score "score-flutes")
   (silence #:on-the-fly-markup , #{

\markup\fontsize#-2 \column {
  \livretPers La principale Nymphe
  \livretVerse#8 { Chantez dans ce riant boccage, }
  \livretVerse#12 { Célébrez de l’Amour les triomphes divers, }
  \livretVerse#8 { Il retient sous son esclavage }
  \livretVerse#8 { Les cieux, la terre & les enfers ; }
  \livretVerse#8 { Qu’il règne autant sur ce rivage, }
  \livretVerse#8 { Qu’il règne dans le sein des mers. }
}#}))
