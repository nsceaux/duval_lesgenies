\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Flûtes" } <<
      \global \includeNotes "flute1"
    >>
    \new Staff \with { instrumentName = "Violons" } <<
      \global \includeNotes "dessus"
    >>
  >>
  \layout { indent = \largeindent }
}
