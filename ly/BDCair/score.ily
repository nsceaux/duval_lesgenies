\score {
  \new StaffGroupNoBar <<
    \new GrandStaff \with { instrumentName = "Flûtes" } <<
      \new Staff << \global \includeNotes "flute1" >>
      \new Staff << \global \includeNotes "flute2" >>
    >>
    \new Staff \with {
      instrumentName = \markup\center-column\smallCaps { La principale Nymphe }
    } \withLyrics <<
      \global \keepWithTag #'nymphe \includeNotes "voix"
    >> \keepWithTag #'nymphe \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Violons" } <<
      \global \includeNotes "dessus"
      \origLayout {
        s2.*7\break s2.*7\pageBreak
        s2.*8\break s2.*7 s2 \bar "" \pageBreak
        s4 s2.*8\break s2.*8\pageBreak
        s2.*9\break s2.*10
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
