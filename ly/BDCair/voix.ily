\clef "vdessus" R2.*2 |
r8 re'' fa''[ mi''] re''[ dod''] |
re''2. |
la'4 sib'8 la' sol' fa' |
mi'4\trill mi' la'8 la' |
re''4 la'4. re''8 |
si'2\trill dod''8 re'' |
mi''[ fa'' sol'' fa'' mi'' re'']( |
dod''2)\trill dod''8 re'' |
la'2 fa''8 mi'' |
re''4 re''8 do'' si' la' |
mi''4 mi'' r8 mi'' |
la''2 r8 la' |
re''2~ re''8[ mi''16 fa''] |
mi''4 mi'' mi' |
la' r r |
r r la' |
fad'2. |
la'4 la'8 sib' do'' la' |
sib'4 \appoggiatura la'8 sol'4 sib'8 la' |
sol'[ la' sol' fa' mi' re']( |
la'4) la'4. re''8 |
\appoggiatura do''8 si'2 dod''8 re'' |
mi''[ fa'' sol'' fa'' mi'' re'']( |
dod''2)\trill dod''8 re'' |
\appoggiatura re''8 mi''2 r4 |
r r la'8 sib' |
fad'4 re'' la' |
sib' sib'8 la' sol' fa' |
do''4 do'' r8 do'' |
fa''2 r8 la' |
si'2. |
si'4 dod'' re'' |
la'2 fa'4 |
do''8[\melisma sib' la' sib' do'' la'] |
re''[ do'' sib' do'' re'' mi''] |
fa''2~ fa''8\melismaEnd mi'' |
re''4.\trill re''8 mi'' fa'' |
mi''4\trill mi'' do'' |
fa''8[\melisma mi'' re'' do'' sib' la'] |
re''[ do'' sib' la' sol' fa']( |
do''4.)\melismaEnd sib'8 la'4 |
sib' do''4. do'8 |
fa'2 r4 |
r r fa'' |
re''8[\melisma mi'' fa'' la' si' do''] |
si'[ la' sol' re'' sol'' si'] |
dod''[ si' la' si' dod'' la']( |
re''4.)\melismaEnd do''8 sib'4 |
la' sol'4.(\trill fa'16) sol' |
la'2 r4 |
R2. |
r4 r la' |
re''8[\melisma mi'' fa'' la' si' do''] |
si'[ la' sol' re'' sol'' si'] |
dod''[ si' la' si' dod'' la']( |
re''4.)\melismaEnd do''8 sib'4 |
sol' la'4. la'8 |
re'2 r4 |
r r la' |
re''8[\melisma mi'' fa'' re'' mi'' dod''] |
re''[ mi'' fa'' re'' mi'' dod'']( |
re''4.)\melismaEnd do''8 sib'4 |
sol'8[ la'16 sib'] la'4. la'8 |
re'4
