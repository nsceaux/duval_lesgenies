\clef "basse" re'4 sol la |
re2. re'4~ |
re'2 dod' |
re'2. sold4 |
la2. mi4 |
la, si, do la, |
mi2 re |
do2. dod4 |
re2. la,4 |
re, mi, fa, re, |
la,2. la,4 |
re2. do8 si, |
la,4 la do'8 si la sold |
la4 mi la, do' |
si do' re' si |
do'2 la4 la8 sol |
fa2 fa4. mi8 |
re4 la, re,4. re8 |
la4 mi la,4. la,8 |
re2. do8 re |
mi4 la, mi,2 |
la,4 mi, la,, la, |
mi la, mi, mi, |
la, la dod' la |
re' re fa8 mi re dod |
re4 la, re, fa |
mi fa sol mi |
fa2 re4 mi8 re |
do re mi re do4. si,8 |
la,2 la4. la8 |
sold2 sold4. mi8 |
la4 mi la, la8 sol |
fa2 re4. re8 |
mi4 mi, mi, mi8 re |
do2 re4. mi8 |
la,2 la |
la la, |
la,2. la4 |
la2. sib8 la |
sol2. la8 sol |
fa2 fa4 fa |
fa2. sol8 fa |
mi2 mi\trill |
re2. re'4 |
sol la sib sol |
la2 la4 la |
la2. sol4 |
la re la, la, |
re2 re4 re' |
re'2. mi'8 re' |
do'2. re'8 do' |
sib2( sib\trill) |
la4 mi la, la |
fa2. mi8 fa |
sol2. fa8 mi |
fa4 fa fa re |
dod2.\trill la,4 |
re la, re, fa |
fa2. sol8 fa |
mi2. fa8 mi |
re4 re re do |
sib,2. sib,4 |
la, la re mi |
la,2. la4 |
la2 sold |
la2. la4 |
la2 sol |
fa4 mi8 re dod4 la, |
re re' do'2 |
sib fad |
sol2. mi4 |
la2 la, |
re r |
re' fa'4 re' |
dod' re' mi' dod' |
re'2 re |
la2. mi4 |
la, si, do la, |
mi2 re |
do2. dod4 |
re2. la,4 |
re, mi, fa, re, |
la,2. la,4 |
re2. do8 si, |
la,4 la do'8 si la sold |
la2. do'4 |
si do' re' si |
do'2 la4 la8 sol |
fa2 fa4. mi8 |
re2 re4. re8 |
la2 la4. la,8 |
re2. do8 re |
mi4 la, mi,2 |
la,4 la,, la,, la, |
mi2. mi,4 |
la, la8 si dod'4 la |
re' re fa8 mi re dod |
re4 la, re, fa |
mi fa sol mi |
fa2 fa4 mi8 re |
do2 do4. si,8 |
la,2 la4. la8 |
sold2 sold4. mi8 |
la4 mi la, la8 sol |
fa2 fa4. re8 |
mi2 mi4. re8 |
do2 re4. mi8 |
la,2. la4 |
la la, la, la |
sol sol sol sol |
fa2 fa4 re' |
re'2 re'4 re' |
sol sol sol sol |
sol sol sol mi |
la2 la, |
re1 |
