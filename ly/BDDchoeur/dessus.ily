\clef "dessus" r4 r2 |
R1*61 |
r4 la'4 do''8 si' la' sold' |
la'2. do''4 |
si' do'' re'' si' |
do'' la' do'' mi'' |
dod'' la' dod'' mi'' |
la'' sol''8 fa'' mi'' fa'' sol'' mi'' |
fa''4 re'' fad'' la'' |
re''' do'''8 sib'' la'' sib'' do''' la'' |
sib'' do''' sib'' la'' sol'' la'' sol'' fa'' |
mi'' fa'' sol'' fa'' mi''4.\trill re''8 |
re''4 r r2 |
R1*43 |
