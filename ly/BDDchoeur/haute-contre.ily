\clef "haute-contre" r4 r2 |
R1*61 |
r4 mi' mi'8 re' do' si |
do'2. mi'4 |
re' mi' fa' re' |
mi' mi' la' do'' |
la' mi' la' dod'' |
re'' dod''8 re'' mi'' re'' dod'' mi'' |
re''4 la' la'2 |
sol' la' |
sol'4 re''2 mi''8 re'' |
dod'' re'' mi'' re'' dod''4.\trill re''8 |
re''4 r r2 |
R1*43 |
