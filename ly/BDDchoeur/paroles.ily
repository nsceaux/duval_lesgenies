\tag #'vdessus {
  Chan -- tons, chan -- tons dans ce ri -- ant boc -- ca -- ge,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  il re -- tient sous son es -- cla -- va -- ge
  les cieux, la terre et les en -- fers.
  Chan -- tons, chan -- tons, chan -- tons dans ce ri -- ant boc -- ca -- ge,
  chan -- tons, chan -- tons dans ce ri -- ant boc -- ca -- ge,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  il re -- tient sous son es -- cla -- va -- ge
  les cieux, la terre et les en -- fers ;
  chan -- tons, chan -- tons,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  qu’il rè -- gne,
  qu’il règne au -- tant sur ce ri -- va -- ge,
  qu’il rè -- gne,
  qu’il rè -- gne dans le sein des mers,
  qu’il rè -- gne,
  qu’il règne au -- tant sur ce ri -- va -- ge,
  qu’il rè -- gne dans le sein des mers.
  
  Chan -- tons, chan -- tons dans ce ri -- ant boc -- ca -- ge,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  il re -- tient sous son es -- cla -- va -- ge
  les cieux, la terre et les en -- fers.
  Chan -- tons, chan -- tons, chan -- tons dans ce ri -- ant boc -- ca -- ge,
  chan -- tons, chan -- tons dans ce ri -- ant boc -- ca -- ge,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  il re -- tient sous son es -- cla -- va -- ge
  les cieux, la terre et les en -- fers ;
  qu’il règne au -- tant sur ce ri -- va -- ge,
  qu’il rè -- gne,
  qu’il rè -- gne dans le sein des mers.
}
\tag #'vhaute-contre {
  Chan -- tons, chan -- tons dans ce ri -- ant boc -- ca -- ge,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  il re -- tient sous son es -- cla -- va -- ge
  les cieux, la terre et les en -- fers.
  Chan -- tons, chan -- tons, chan -- tons,
  chan -- tons dans ce ri -- ant boc -- ca -- ge,
  cé -- lé -- brons, cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  chan -- tons, cé -- lé -- brons,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  chan -- tons, chan -- tons,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  qu’il rè -- gne,
  qu’il rè -- gne,
  qu’il règne au -- tant sur ce ri -- va -- ge,
  qu’il rè -- gne,
  qu’il rè -- gne,
  qu’il rè -- gne dans le sein des mers,
  qu’il rè -- gne dans le sein des mers.
  
  Chan -- tons, chan -- tons dans ce ri -- ant boc -- ca -- ge,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  il re -- tient sous son es -- cla -- va -- ge
  les cieux, la terre et les en -- fers.
  Chan -- tons, chan -- tons, chan -- tons,
  chan -- tons dans ce ri -- ant boc -- ca -- ge,
  cé -- lé -- brons, cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  chan -- tons, cé -- lé -- brons,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  qu’il règne au -- tant sur ce ri -- va -- ge,
  qu’il rè -- gne dans le sein des mers.
}
\tag #'vtaille {
  Chan -- tons, chan -- tons dans ce ri -- ant boc -- ca -- ge,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  il re -- tient sous son es -- cla -- va -- ge
  les cieux, la terre et les en -- fers.
  Chan -- tons, chan -- tons dans ce ri -- ant boc -- ca -- ge,
  chan -- tons, cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  chan -- tons, chan -- tons,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  chan -- tons,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  chan -- tons, chan -- tons,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  qu’il rè -- gne,
  qu’il rè -- gne,
  qu’il rè -- gne,
  qu’il rè -- gne dans le sein,
  dans le sein des mers,
  qu’il rè -- gne dans le sein des mers,
  qu’il rè -- gne,
  qu’il rè -- gne,
  qu’il rè -- gne dans le sein des mers,
  qu’il rè -- gne dans le sein des mers.
  
  Chan -- tons, chan -- tons dans ce ri -- ant boc -- ca -- ge,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  il re -- tient sous son es -- cla -- va -- ge
  les cieux, la terre et les en -- fers.
  Chan -- tons, chan -- tons dans ce ri -- ant boc -- ca -- ge,
  chan -- tons, cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  chan -- tons, chan -- tons,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  chan -- tons,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  qu’il règne au -- tant sur ce ri -- va -- ge,
  qu’il rè -- gne dans le sein des mers.
}
\tag #'vbasse {
  Chan -- tons, chan -- tons dans ce ri -- ant boc -- ca -- ge,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  les tri -- om -- phes di -- vers,
  chan -- tons, chan -- tons, chan -- tons, chan -- tons, chan -- tons,
  chan -- tons dans ce ri -- ant boc -- ca -- ge,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  qu’il rè -- gne,
  qu’il rè -- gne,
  qu’il rè -- gne dans le sein,
  dans le sein des mers,
  qu’il rè -- gne,
  qu’il rè -- gne,
  qu’il rè -- gne dans le sein des mers,
  qu’il rè -- gne dans le sein des mers.
  
  Chan -- tons, chan -- tons dans ce ri -- ant boc -- ca -- ge,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  les tri -- om -- phes di -- vers,
  chan -- tons, chan -- tons, chan -- tons, chan -- tons, chan -- tons,
  chan -- tons dans ce ri -- ant boc -- ca -- ge,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  cé -- lé -- brons de l’a -- mour les tri -- om -- phes di -- vers,
  qu’il règne au -- tant sur ce ri -- va -- ge,
  qu’il rè -- gne, qu’il rè -- gne,
  qu’il rè -- gne dans le sein des mers.
}

