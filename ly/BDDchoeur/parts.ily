\piecePartSpecs
#`((dessus #:instrument ,#{ \markup\center-column { [Violons, Hautbois] } #})
   (parties #:indent 0)
   (taille #:indent 0)
   (dessus2-haute-contre #:indent 0)
   (basse #:instrument ,#{ \markup\center-column { [Basses, Bassons et B.C.] } #})
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
