\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "dessus" >>
      \ifFull\new Staff << \global \includeNotes "haute-contre" >>
      \ifFull\new Staff <<
        \global \includeNotes "taille"
      >>
    >>
    \new ChoirStaff <<
      \new Staff \with { instrumentName = "[Dessus]" } \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \ifFull\new Staff \with {
        instrumentName = \markup\center-column { [Hautes-contre] }
      } \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \ifFull\new Staff \with { instrumentName = "[Tailles]" } \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \with { instrumentName = "[Basses]" } \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses, Bassons et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2. s1*4\break s1*7\break s1*7\pageBreak
        s1*7\break s1*7\pageBreak
        s1*6\break s1*7\pageBreak
        s1*7\break s1*7 s2 \bar "" \pageBreak
        s2 s1*7\break s1*7\pageBreak
        s1*7\break s1*7\pageBreak
        s1*6 s2 \bar "" \break s2 s1*6\pageBreak
        s1*7\break s1*7
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
