<<
  \tag #'vdessus {
    \clef "vdessus" re''4 fa''8[ mi''] re''[ dod''] |
    re''2. fa''4 |
    mi'' fa'' sol'' mi'' |
    fa''2 \appoggiatura mi''8 re''4 mi''8 re'' |
    do''2 mi''4. mi''8 |
    mi''2 mi''4. la''8 |
    sold''4.\melisma fad''8 mi''[ fad'' sold'' mi'']( |
    la''2)\melismaEnd la''4. la''8 |
    la''2 re''4 mi'' |
    fa''1 |
    dod''4 re'' mi'' dod'' |
    re''2 la'4 r8 re'' |
    mi''2. r8 re'' |
    do''1 |
    sold'2 sold'4 si' |
    mi'2. la'4 |
    re' la' re'' mi'' |
    fa''2. re''4 |
    dod'' re'' mi'' dod'' |
    re''2 re''4 r |
    r la' do''8([ si']) la'([ sold']) |
    la'2. do''4 |
    si' do'' re'' si' |
    do''2 la'4 la'8 sol' |
    fa'2 re''4. mi''8 |
    fa''2 re''4. re''8 |
    dod''4.\trill\melisma re''8 mi''[ re'' dod'' mi''] |
    re''2\melismaEnd re''8([ mi'']) fa''([ sol'']) |
    la''2 mi''4 mi'' |
    mi''1 |
    si'4 la' sold' si' |
    mi'2 mi'4 r8 mi'' |
    fa''2. r8 re'' |
    si'1 |
    mi''2 re''4 re'' |
    dod'' dod'' dod'' dod'' |
    dod''2. dod''8 re'' |
    mi''2. fa''8 sol'' |
    la''2 re''4. re''8 |
    dod''2 dod''4. re''8 |
    re''2. la''4 |
    la''2.\melisma sib''8[ la'']( |
    sol''2.) la''8[ sol'']( |
    fa''2.) sol''8[ fa'']( |
    mi''2)~ mi''\trill\melismaEnd |
    mi''2. fa''4 |
    fa''2. sol''4 |
    mi'' fa'' sol'' mi'' |
    fa''2 re''4 la' |
    la'2 sib'4 sib' |
    sib' la' la' la' |
    la'2. sol'4 |
    la'2. la'4 |
    re''2.\melisma dod''8([ re'']) |
    mi''2\melismaEnd mi''4 la' |
    re''2. fa''4 |
    mi'' fa'' sol'' mi'' |
    fa''2 re''4 re'' |
    re''2.\melisma mi''8([ re'']) |
    do''2. re''8([ do''])( |
    sib'4)\melismaEnd sib'4 sib' la' |
    la'2. sol'4 |
    la'2 r |
    R1*9 |
    r4 re'' fa''8([ mi'']) re''([ dod'']) |
    re''2. fa''4 |
    mi'' fa'' sol'' mi'' |
    fa''2 \appoggiatura mi''8 re''4 mi''8 re'' |
    do''2 mi''4. mi''8 |
    mi''2 mi''4. la''8 |
    sold''4.\melisma fad''8 mi''[ fad'' sold'' mi'']( |
    la''2)\melismaEnd la''4. la''8 |
    la''2 re''4 mi'' |
    fa''1 |
    dod''4 re'' mi'' dod'' |
    re''2 re''4 r8 re'' |
    mi''2. r8 re'' |
    do''1 |
    sold'2\trill sold'4 si' |
    mi'2. la'4 |
    re' la' re'' mi'' |
    fa''2. re''4 |
    dod'' re'' mi'' dod'' |
    re''2 re'' |
    r4 la' do''8([ si']) la'([ sold']) |
    la'2. do''4 |
    si' do'' re'' si' |
    do''2 la'4 la'8 sol' |
    fa'2 re''4. mi''8 |
    fa''2 re''4. re''8 |
    dod''4.\melisma re''8 mi''[ re'' dod'' mi''] |
    re''2\melismaEnd re''8([ mi'']) fa''([ sol'']) |
    la''2 mi''4 mi'' |
    mi''1 |
    si'4 la' sold' si' |
    mi'2 mi'4 r8 mi'' |
    fa''2. r8 re'' |
    si'1 |
    mi''2 re''4 re'' |
    dod''2. dod''4 |
    dod''2. re''4 |
    dod'' re'' mi'' dod'' |
    re''2 re''4 fa'' |
    la''2 la''4 la' |
    re''4.\melisma mi''8 fa''[ mi'' re'' do''] |
    sib'4\melismaEnd sib' sib' sol' |
    mi'2 mi'\trill |
    re'1 |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" r4 r2 |
    R1*7 |
    r4 re' fa'8[ mi'] re'[ dod'] |
    re'2. re'4 |
    mi'4 fa' sol' mi' |
    fa'2 \appoggiatura mi'8 re'4 mi'8 re' |
    do'2 mi'4. mi'8 |
    mi'2 mi'4. la'8 |
    sold'4.\melisma fad'8 mi'[ fad' sold' mi']( |
    la'2)\melismaEnd la'4. la'8 |
    la'2 la'4 la' |
    la'1 |
    mi'4 fa' sol' mi' |
    fa'2 fa'4 r8 mi' |
    mi'2. r8 re' |
    do'1 |
    mi'2 la'4 sold' |
    la'2. mi'4 |
    fa' la' la'8[ sol'] fa'[ mi'] |
    re'4 fa' fa' fa' |
    sol' fa' mi' sol' |
    fa'2 fa'4 sol'8 fa' |
    mi'4 la'8 la' la'4 la'8 sold' |
    la'2 mi'4 mi' |
    mi'2 mi'4. mi'8 |
    mi'4 do' mi' la'8 la' |
    la'4 la'8 la' la'4 si'8 la' |
    sold'2\trill sold'4. sold'8 |
    la'2 si'4 mi' |
    mi' mi' mi' mi' |
    mi'2. mi'8 re' |
    dod'2.\trill fa'8 mi' |
    re'2 fa'4. fa'8 |
    mi'2 mi'4. fa'8 |
    fa'4 fa' la'2 |
    la'4 re' fa'\melisma mi'8[ fa']( |
    sol'2.) fa'8[ mi']( |
    fa'2.) mi'8[ fa']( |
    sol'4) fa' mi'2\melismaEnd |
    dod'2.\trill la'4 sib'2. sib'4 |
    la'4 la' mi' dod' |
    re'2 re'4 fa' |
    fa'2.\melisma mi'8([ fa']) |
    sol'2. re'8([ mi']) |
    fa'2 re'\melismaEnd |
    mi'2. dod'4 |
    re'2.\melisma mi'8([ re']) |
    dod'2\melismaEnd dod'4 dod' |
    re' re' re' re' |
    mi'2. dod'4 |
    re'2 r4 re' |
    re'2.\melisma mi'8([ fa']) |
    sol'2. re'8([ mi'])( |
    fa'4)\melismaEnd fa' fa' mi' |
    fa'2. sol'4 |
    mi'2\trill r |
    R1*17 |
    r4 re' fa'8[ mi'] re'[ dod'] |
    re'2. re'4 |
    mi'4 fa' sol' mi' |
    fa'2 \appoggiatura mi'8 re'4 mi'8 re' |
    do'2 mi'4. mi'8 |
    mi'2 mi'4. la'8 |
    sold'4.\melisma fad'8 mi'[ fad' sold' mi']( |
    la'2)\melismaEnd la'4. la'8 |
    la'2 la'4 la' |
    la'1 |
    mi'4 fa' sol' mi' |
    fa'2 fa'4 r8 mi' |
    mi'2. r8 re' |
    do'1 |
    mi'2 la'4 sold' |
    la'2. mi'4 |
    fa' la' la'8[ sol'] fa'[ mi'] |
    re'4 fa' fa' fa' |
    sol' fa' mi' sol' |
    fa'2 fa'4 sol'8 fa' |
    mi'4 la'8 la' la'4 la'8 sold' |
    la'2 mi'4 mi' |
    mi'2 mi'4. mi'8 |
    mi'4 do' mi' la'8 la' |
    la'4 la'8 la' la'4 si'8 la' |
    sold'2\trill sold'4. sold'8 |
    la'2 si'4 mi' |
    mi'2. la'4 |
    la'2 la' |
    la' la' |
    la' la' |
    fa'2 fa'4 fa' |
    fa'4.( mi'8) re'4.( mi'8) |
    fa'2 mi' |
    re' dod'\trill |
    re'1 |
  }
  \tag #'vtaille {
    \clef "vtaille" r4 r2 |
    R1*3 |
    r4 la do'8[ si] la[ sold] |
    la2. do'4 |
    si do' re' si |
    do'2 \appoggiatura si8 la4 la8 sol |
    fa2 la4. la8 |
    la2 la4. fa'8 |
    mi'4.\melisma re'8 dod'[ si la sol]( |
    fa2)\melismaEnd fa4 la |
    la2 do'4 do' |
    do'1 |
    re'4 mi' fa' re' |
    mi'2 do'4 r8 dod' |
    re'2. r8 dod' |
    re'1 |
    la2 la4 la |
    la2. do'4 |
    do'2. mi'4 |
    mi'2. la4 |
    sold la si mi' |
    mi'2 la4 dod' |
    re' fa'8 mi' re'4 la8 la |
    la2 la4. la8 |
    la2 la4. la8 |
    la4 la la4. la8 |
    la4 do'8 re' mi'4 mi'8 re' |
    do'2 do'4 do' |
    re'2 re'4. re'8 |
    do'4 la dod' dod'8 dod' |
    re'2 si4. si8 |
    si2 si4. si8 |
    la2 la4. sold8 |
    la4 la la la |
    la2. la8 la |
    la2. re'8 dod' |
    re'2 la4. la8 |
    la2 la4. la8 |
    la4 re' re'2 |
    re'4 la re'2 |
    re'4 si dod'2 |
    re'4 la la2 |
    sib re'4 sib |
    la2 dod'4 re' |
    re'2 re' |
    dod'4\trill re' dod' la |
    la4 la la re' |
    re'2. sol'8([ fa']) |
    mi'2.\melisma fa'8([ mi']) |
    re'2~ re'\trill\melismaEnd |
    dod'2. la4 |
    la1~ |
    la2 la4 la |
    la la la la |
    la2. la4 |
    la2 r4 la |
    la\melisma fa8([ sol]) la2 |
    sol4 mi8([ fa]) sol2 |
    fa4\melismaEnd fa re' mi' |
    re'2 re' |
    dod'\trill r |
    R1*13 |
    r4 la do'8[ si] la[ sold] |
    la2. do'4 |
    si do' re' si |
    do'2 \appoggiatura si8 la4 la8 sol |
    fa2 la4. la8 |
    la2 la4. fa'8 |
    mi'4.\melisma re'8 dod'[ si la sol]( |
    fa2)\melismaEnd fa4 la |
    la2 do'4 do' |
    do'1 |
    re'4 mi' fa' re' |
    mi'2 do'4 r8 dod' |
    re'2. r8 dod' |
    re'1 |
    la2 la4 la |
    la2. do'4 |
    do'2. mi'4 |
    mi'2. la4 |
    sold la si mi' |
    mi'2 la4 dod' |
    re' fa'8 mi' re'4 la8 la |
    la2 la4. la8 |
    la2 la4. la8 |
    la4 la la4. la8 |
    la4 do'8 re' mi'4 mi'8 re' |
    do'2 do'4 do' |
    re'2 re'4. re'8 |
    do'4 la dod' dod'8 dod' |
    re'2 si4. si8 |
    si2 si4. si8 |
    la2 la4. sold8 |
    la2. mi'4 |
    mi'2 mi' |
    mi' mi' |
    re' re' |
    re' re'4 re' |
    sib2 sib |
    re' re' |
    la sol |
    fa1 |
  }
  \tag #'vbasse {
    \clef "vbasse" r4 r2 |
    R1*11 |
    r4 la do'8[ si] la[ sold] |
    la2. do'4 |
    si do' re' si |
    do'2 la4 la8 sol |
    fa2 fa4. mi8 |
    re2 re4. re8 |
    la2 la4. la,8 |
    re2. do8 re |
    mi2 mi4. mi8 |
    la,2. la,4 |
    mi2. mi4 |
    la, la dod' la |
    re' re fa8([ mi]) re([ dod]) |
    re2. fa4 |
    mi fa sol mi |
    fa2 re4 mi8 re |
    do2 do4. si,8 |
    la,2 la4 la |
    sold2\trill sold4. mi8 |
    la2. la8 sol |
    fa2 re4. re8 |
    mi2 mi4. re8 |
    do2 re4. mi8 |
    la,2 r |
    R1 |
    r2 r4 la |
    la2.\melisma sib8[ la] |
    sol2. la8[ sol]( |
    fa2)\melismaEnd fa4 fa |
    fa2.\melisma sol8[ fa]( |
    mi2)~ mi\trill\melismaEnd |
    re2. re'4 |
    sol la sib sol |
    la2 la4 la |
    la2. sol4 |
    la2. la,4 |
    re2 re4 re' |
    re'2.\melisma mi'8([ re']) |
    do'2. re'8([ do']) |
    sib2~ sib\trill\melismaEnd |
    la2. la4 |
    fa2.\melisma mi8([ fa]) |
    sol2. fa8([ mi])( |
    fa4)\melismaEnd fa fa re |
    dod2.\trill la,4 |
    re2 r4 fa |
    fa2.\melisma sol8([ fa]) |
    mi2. fa8([ mi])( |
    re4)\melismaEnd re re do |
    sib,2. sib,4 |
    la,2 r |
    R1*21 |
    r4 la do'8([ si]) la([ sold]) |
    la2. do'4 |
    si do' re' si |
    do'2 la4 la8 sol |
    fa2 fa4. mi8 |
    re2 re4. re8 |
    la2 la4. la,8 |
    re2. do8 re |
    mi2 mi4. mi8 |
    la,2. la,4 |
    mi2. mi4 |
    la, la dod' la |
    re' re fa8[ mi] re[ dod] |
    re2. fa4 |
    mi fa sol mi |
    fa2 fa4 mi8 re |
    do2 do4. si,8 |
    la,2 la4. la8 |
    sold2 sold4. mi8 |
    la2. la8 sol |
    fa2 fa4. re8 |
    mi2 mi4. re8 |
    do2 re4. mi8 |
    la,2. la4 |
    la2. la4 |
    sol sol sol sol |
    fa2 fa4 re' |
    re'2 re'4 re' |
    sol2 sol4 sol |
    sol sol sol mi |
    la2 la, |
    re1 |
  }
>>

