\clef "basse" re4 re'2 |
dod' si4 |
la2 dod4 |
re sol, la, |
re re'2 |
dod' si4 |
la2 dod4 |
re sol, la, |
re2 r4 |
r re' dod' |
si sold la |
mi mi' re' |
dod' si la |
re'8 dod' si4 la |
sold la fad |
re mi mi, |
la, re'2 |
dod' si4 |
la2 dod4 |
re sol, la, |
re re'2 |
dod' si4 |
la2 dod4 |
re sol, la, |
re, \clef "alto" re' si |
do' do' la |
si8 do' si la sol4 |
la si2 |
mi2 mi4 |
la2 la4 |
re2 r4 |
re' dod' re' |
la \clef "bass" re'2 |
dod' si4 |
la2 dod4 |
re sol, la, |
re re'2 |
dod' si4 |
la2 dod4 |
re sol, la, |
re,
%%
re'2~ |
re'4 dod'2 |
re'8 do' sib la sib sol |
la4 la sol |
fa8 sol fa mi re4~ |
re dod2 |
re4 mi fa |
sol la la, |
re r re |
mi4. re8 do4 |
fa r do |
fa mi re |
do la fa |
sib2 sol4 |
do' sib la |
sib do' do |
fa re'2~ |
re'4 dod'2 |
re'8 do' sib la sib sol |
la4 la sol |
fa8 sol fa mi re4~ |
re dod2 |
re4 mi fa |
sol la la, |
re4 \clef "alto" re'4 mi' |
fa' re'2 |
mi' do'4 |
re' mi'2 |
la8 si do' si la do' |
re'4 mi'2 |
la'4 sold' la' |
re' mi'2 |
la4 \clef "bass" re'2~ |
re'4 dod'2 |
re'8 do' sib la sib sol |
la4 la sol |
fa8 sol fa mi re4~ |
re dod2 |
re4 mi fa |
sol la la, |
%%
re4 re'2 |
dod'2 si4 |
la2 dod4 |
re sol, la, |
re re'2 |
dod' si4 |
la2 dod4 |
re sol, la, |
re2. |
