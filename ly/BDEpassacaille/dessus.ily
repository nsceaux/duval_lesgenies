\clef "dessus" r4 fad'' re'' |
la'' la' re'' |
dod''4. re''8 mi''4 |
fad'' sol'' \appoggiatura fad''8 mi''4 |
fad'' fad'' re'' |
la'' la' re'' |
dod''4. re''8 mi''4 |
fad'' mi''4.\trill re''8 |
re''4 la'' sol'' |
fad'' fad'' mi'' |
re'' re'' dod'' |
si'2\trill si'4 |
mi'' re'' dod'' |
fad'' sold'' la'' |
mi'' \appoggiatura re''8 dod''4 \appoggiatura si'8 la'4 |
si' si'4.\trill la'8 |
la'4 fad'' \appoggiatura mi''8 re''4 |
la'' la' re'' |
dod''4.\trill re''8 mi''4 |
fad'' sol'' \appoggiatura fad''8 mi''4 |
fad'' fad'' \appoggiatura mi''8 re''4 |
la'' la' re'' |
dod''4.\trill re''8 mi''4 |
fad'' mi''4.\trill re''8 |
re''4 \cesureInstr <>^\markup\whiteout Flûtes \twoVoices #'(dessus1 dessus2 dessus) <<
  { la''4 si'' |
    \appoggiatura la''8 sol''4 sol'' la'' |
    fad''\trill fad'' si'' |
    la''8 sol'' fad'' sol'' la'' fad'' |
    sol''4 \appoggiatura fad''8 mi''4 s8 sol'' |
    sol''4 fad''8 sol'' mi'' sol'' |
    fad'' mi'' fad'' sol'' la'' si'' |
    la''4 sol'' fad'' |
    mi''4\trill }
  { fad''4 sol'' |
    \appoggiatura fad''8 mi''4 mi'' fad'' |
    red''\trill red'' sol'' |
    fad''8 mi'' red'' mi'' fad'' red'' |
    mi''4 si' s8 mi'' |
    dod''4.\trill si'8 dod''4 |
    re''8 dod'' re'' mi'' fad'' sol'' |
    fad''4 mi'' re'' |
    dod''\trill }
  { s2 | s2.*3 | s2 r8 s | }
>> \cesureInstr <>^"Tous" fad''4 \appoggiatura mi''8 re''4 |
la'' la' re'' |
dod''4.\trill re''8 mi''4 |
fad'' sol'' \appoggiatura fad''8 mi''4 |
fad'' fad'' \appoggiatura mi''8 re''4 |
la'' la' re'' |
dod''4.\trill re''8 mi''4 |
fad'' mi''4.\trill re''8 |
re''4
%%
re''4 fa'' |
mi'' la''8 sol'' fa'' mi'' |
fa''4 sol''8 fa'' mi'' re'' |
dod''4 re'' mi'' |
la' re'' fa'' |
mi'' la''8 sol'' fa'' mi'' |
fa''4 dod'' re'' |
mi''4 mi''4.\trill re''8 |
re''4 fa'' la'' |
sol'' do'' r8 sib'' |
la''4 \appoggiatura sol''8 fa''4 sol'' |
la'' sol'' fa'' |
sol'' do'' mib'' |
re'' sol''4. sib''8 |
mi''4\trill \appoggiatura re''8 do''4 fa'' |
sol'' sol''4.\trill fa''8 |
fa''4 re'' fa'' |
mi'' la''8 sol'' fa'' mi'' |
fa''4 sol''8 fa'' mi'' re'' |
dod''4\trill re'' mi'' |
la' re'' fa'' |
mi'' la''8 sol'' fa'' mi'' |
fa''4 dod'' re'' |
mi'' mi''4.\trill re''8 |
re''4 \cesureInstr <>^\markup\whiteout Flûtes \twoVoices #'(dessus1 dessus2 dessus) <<
  { re'''4 do''' |
    si''8 do''' re''' do''' si'' la'' |
    sold''4 mi'' do''' |
    si''8 la'' sold'' la'' si'' sold'' |
    la''4 mi''4. do'''8 |
    si''4 la''8 si'' sold'' si'' |
    la'' do''' si'' re''' do'''4 |
    si''8 la'' sold''4.\trill la''8 |
    la''4 }
  { fa''4 mi'' |
    re''8 mi'' fa'' mi'' re'' do'' |
    si'4 si' mi'' |
    re''8 do'' si' do'' re'' si' |
    do''4 la' mi'' |
    re'' do''8 re'' si' re'' |
    do'' mi'' re'' fa'' mi''4 |
    re''8 do'' si'4. la'8 |
    la'4 }
>> \cesureInstr <>^"[Tous]" re''4 fa'' |
mi'' la''8 sol'' fa'' mi'' |
fa''4 sol''8 fa'' mi'' re'' |
dod''4.\trill re''8 mi''4 |
la' re'' fa'' |
mi'' la''8 sol'' fa'' mi'' |
fa''4 dod''\trill re'' |
mi''4 mi''4.\trill re''8 |
%%
re''4 fad'' \appoggiatura mi''8 re''4 |
la'' la' re'' |
dod''4. re''8 mi''4 |
fad'' sol'' \appoggiatura fad''8 mi''4 |
fad'' fad'' \appoggiatura mi''8 re''4 |
la'' la' re'' |
dod''4.\trill re''8 mi''4 |
fad'' mi''4.\trill re''8 |
re''2. |
