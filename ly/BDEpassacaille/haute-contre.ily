\clef "haute-contre"
\setMusic #'repriseI {
  la'2 |
  la'2 sold'4 |
  la'2 la'4 |
  la' si' dod'' |
  re'' la'2 |
  la' sold'4 |
  la'2 la'4 |
  la' si' la'8 sol' |
}
r4 \keepWithTag #'() \repriseI |
fad'4 fad' sol' |
la'2 la'4 |
si' si' la' |
sold'2\trill sold'4 |
la' si' mi' |
mi'2 mi'4~ |
mi' \appoggiatura fad'8 mi'4 \appoggiatura re'8 dod'4 |
re' la' sold'\trill |
la' \repriseI |
fad'4 r r |
R2.*7 | \allowPageTurn
r4 \repriseI |
fad'4
%%
\setMusic #'repriseII {
  la'2 |
  la' la'4 |
  la' sib'4. sib'8 |
  la'4 fa' mi' |
  fa'8 mi' fa' sol' la'4 |
  la'2 la'4 |
  la'2 la'4 |
  sib' la' la' |
}
\keepWithTag #'() \repriseII |
fa' r fa' |
do'4. re'8 mi'4 |
fa' r mi' |
fa' sol'4. sol'8 |
sol'4 fa' do''~ |
do'' mi'' sol' |
sol'\trill \appoggiatura fa'8 mi'4 do'' |
sib' sib'4.\trill la'8 |
la'4 \repriseII |
fa'4 r r |
R2.*7 | \allowPageTurn
r4 \repriseII |
%%
fad'4 \repriseI |
fad'2. |
