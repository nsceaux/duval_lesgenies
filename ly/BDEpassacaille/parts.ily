\piecePartSpecs
#`((dessus #:instrument ,#{ \markup\center-column { [Violons, Flûtes] } #})
   (dessus1 #:instrument ,#{ \markup\center-column { [Violons, Flûtes] } #})
   (dessus2 #:instrument ,#{ \markup\center-column { [Violons, Flûtes] } #})
   (parties)
   (dessus2-haute-contre)
   (taille)
   (basse #:instrument ,#{ \markup\center-column { [Basses et B.C.] } #})

   (flute-hautbois #:instrument "Flûtes")
   (taille)
   
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
