\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { [Violons et Flûtes] }
    } << \global \includeNotes "dessus" >>
    \ifFull <<
      \new Staff \with {
        instrumentName = \markup\center-column { [Hautes-contre] }
      } << \global \includeNotes "haute-contre" >>
      \new Staff \with {
        instrumentName = "[Tailles]"
      } << \global \includeNotes "taille" >>
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*10\break s2.*10\break s2.*9\pageBreak
        s2.*9\break s2.*11\break s2.*11\pageBreak
        s2.*10\break s2.*10\break s2.*9
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}