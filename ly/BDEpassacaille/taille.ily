\clef "taille"
\setMusic #'repriseI {
  la'4 fad' |
  mi'2 mi'4 |
  mi'4. fad'8 mi'4 |
  re'2 la'4 |
  la' la' fad' |
  mi'2 mi'4 |
  mi'4. fad'8 mi'4 |
  re'4. re'8 dod'4\trill |
}
r4 \keepWithTag #'() \repriseI |
re'4 re' mi' |
fad' re' mi' |
mi'2 mi'4 |
mi'2 mi'4~ |
mi' sold' la' |
la' si' dod'' |
si' la'4. la'8 |
la'4 mi'4. mi'8 |
dod'4\trill \repriseI |
re'4 r r |
R2.*7 |
r4 \repriseI |
re'4
%%
\setMusic #'repriseII {
  fa'2 |
  sol' sol'4 |
  fa'8 mi' re'4. mi'8 |
  mi'4 la4. la8 |
  re' dod' re' mi' fa'4 |
  sol'2 sol'4 |
  fa' mi' re' |
  re'4. re'8 dod'4\trill |
}
\keepWithTag #'() \repriseII |
re'4 r re' |
do' do'4. do'8 |
do'4 \appoggiatura sib8 la4 do' |
do'2 re'4 |
mi' fa' la'~ |
la' sol'4. re'8 |
do'4. sol'8 fa' mi' |
re'4 do' do' |
do' \repriseII |
re'4 r r |
R2.*7 |
r4 \repriseII |
re' \repriseI |
re'2. |
