\clef "basse" re8 |
re4 la8 |
re'4 re8 |
la, la sol |
fad fad mi |
re4 la8 |
re'4 re8 |
re dod re |
la,4 la8 |
dod4 la,8 |
re4 re'8 |
dod'4 si8 |
la mi16 fad sol8 |
fad dod la, |
re8. mi16 fad8 |
sol la la, |
re4
