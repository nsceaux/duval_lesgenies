\clef "dessus" fad''8 |
fad'' re'' mi'' |
fad'' re''16 mi'' fad''8 |
\appoggiatura fad''8 mi'' \appoggiatura re'' dod'' \appoggiatura si' la' |
re'' re''16 dod'' re'' mi'' |
fad''8 re'' mi'' |
fad''16 mi'' re'' mi'' fad'' sol'' |
la''8 sol'' fad'' |
mi''4\trill mi''8 |
la'' mi''16 fad'' sol''8 |
fad'' fad''16 sol'' la''8 |
mi'' sol''16 fad'' mi'' re'' |
mi''8 dod'' mi'' |
la'' mi''16 fad'' sol''8 |
fad'' fad''16 sol'' la''8 |
re'' dod''16 re'' mi'' dod'' |
re''4
