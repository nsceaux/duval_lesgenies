\clef "haute-contre" re''8 |
re''4 dod''8 |
re'' la' la' |
la'4 la'8 |
la'8. la'16 si' dod'' |
re''4 dod''8 |
re''8 la' la' |
la'4 la'8 |
la'4 la'8 |
la'4 la'8 |
la'16 si' la' sol' fad' sol' |
la'4 sold'8 |
la'4 la'8 |
la'8 sol'16 fad' mi'8 |
re'4 re'8 |
mi' fad' mi' |
re'4
