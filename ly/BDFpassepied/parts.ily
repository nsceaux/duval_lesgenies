\piecePartSpecs
#`((dessus #:instrument
           ,#{\markup\center-column { [Violons et Hautbois] }#})
   (parties)
   (taille)
   (dessus2-haute-contre)
   (basse #:instrument
          , #{ \markup\center-column { [Basses, Bassons et B.C.] } #})
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
