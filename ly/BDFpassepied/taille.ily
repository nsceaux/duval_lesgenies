\clef "taille" la'8 |
la' fad' mi' |
re' fad'16 mi' re'8 |
dod' mi' dod' |
re' la la' |
la' fad' mi' |
re'8. dod'16 re' mi' |
fad'8 mi' re' |
dod'4\trill mi'8 |
mi' dod'16 re' mi'8 |
re' re'16 mi' fad'8 |
mi'4 mi'8 |
mi'8 mi'16 re' dod'8 |
re' mi'16 re' dod' si |
la4. |
si8 la la |
fad4\trill
