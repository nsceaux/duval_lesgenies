\clef "dessus2" re''8 |
sol'4 do''8 |
fa'4 sib'8 |
mi' la' la |
re'4 re''8 |
sol'4 do''8 |
fa'4 fa'8 fa' mi' re' |
la4 r8 |
r r la' |
sib' sib'16 do'' re''8 |
sol'4 sol'8 |
do''4 r8 |
do''8 r fa'' |
do'' sib' la' |
sib' do'' do' |
fa'4 r8 |
r r re'' |
sol''4 r8 |
sol'8 sib'4 |
la' fa'8 |
dod'4 la8 |
re' mi' fa' |
sol' la' la |
re'4
