\clef "dessus" la''8 |
sib'' sol''16 la'' sib''8 |
la''4 fa''8 |
sol'' mi'' la'' |
fa'' re'' la'' |
sib'' sol''16 la'' sib''8 |
la''4 la'8 |
re'' dod'' re'' |
mi''4\trill mi''8 |
fa'' fa''16 sol'' la''8 |
re''4 re''8 |
sib''16 la'' sol'' fa'' mi'' re'' |
mi''8 do'' do''' |
sol'' sib''16 la'' sol'' fa'' |
sol''8 \appoggiatura fa'' mi'' fa'' |
sol'' sol''8.\trill fa''16 |
fa''4 la''8 |
re''' la''16 sib'' do''' la'' |
sib''8 sol'' la'' |
sib''16 la'' sol'' fa'' mi'' re'' |
mi''8 dod'' re'' |
mi''16 re'' mi'' fa'' sol'' mi'' |
fa'' mi'' fa'' sol'' la''8 |
mi'' mi''8.\trill re''16 |
re''4
