\piecePartSpecs
#`((dessus #:score "score")
   (dessus1)
   (dessus2)
   (flute-hautbois #:notes "flute"
                   #:instrument "Flûtes")
   (silence #:on-the-fly-markup , #{ \markup\tacet-text \italic "On reprend le premier passepied" #}))
