\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Flûtes" } <<
      \global \includeNotes "flute"
    >>
    \new Staff \with { instrumentName = "Violons" } <<
      \global \includeNotes "dessus"
      \origLayout { s8 s4.*12\break }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
