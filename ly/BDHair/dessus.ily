\clef "dessus2" r8 |
r4 r r8 la |
re'4 mi' fad' |
sol'2 fad'4 |
dod' re'2 |
la'4 la'8 sol' fad' mi' |
re'4 dod' re' |
la la la |
la la'8 sol' fad' re' |
sol'4 r r8 mi' |
la'4 r r8 fad' |
si'4 r r8 sol' |
dod''4 r r8 la' |
fad'4 dod' re' |
sol'8 la' si'4 sol' |
la'4 la' la' |
la' la' la' |
re'4. mi'8 fad'4 |
mi' mi'8 re' dod' la |
re'2. |
mi' |
la'4 sol'2 |
fad'4 mi' re' |
la'8 si' la' sol' fad' mi' |
re'4 dod'4. re'8 |
la4 la la |
la'8 sol' fad'4 re' |
sol'2 r4 |
sol'2 r4 |
sol'2 r4 |
fad'4 dod' re' |
sol'8 la' si'4 sol' |
la'8 la dod'4 la |
re'4. mi'8 fad'4 |
mi' re' dod'8 la |
re'8 dod' re' mi' fad' re' |
mi'2. |
la4 la' sol' |
fad'8 re'' dod'' la' re'' re' |
sol'4 la' r8 fad' |
sol'2 fad'8 mi' |
re'2 r4 |
re''2 do''4 |
si'2 sol'4 |
la' la2 |
re'8 fad' sol' fad' sol' mi' |
si' dod'' dod''4.(\trill si'16 dod'') |
re''8 mi'' mi''4.(\trill re''16 mi'') |
fad''8 sol'' fad'' mi'' re''4 |
sol' la'2 |
re'2. |
re''4 mi''4. dod''8 |
re'' dod'' re'' mi'' re'' dod'' |
si'8 fad' si fad' si' dod'' |
re'' re' mi'4~ mi' |
fad' fad' fad' |
fad'4. mi'8 re' dod' |
si dod' re' dod' si lad |
si2. |
si' |
si'4 si' si' |
si' lad'2 |
si'8 la' sol'2 |
fad'8. mi''16 red''8. mi''16 red''16 si' dod'' red'' |
mi''8. fad''16 mi''8. fad''16 mi'' dod'' re'' mi'' |
fad''4 si'2 |
fad'4 fad' mi' |
re' red'2 |
mi'2. |
mid' |
fad' |
fad' |
fad'16 lad' si' dod'' dod''4.(\trill si'16 dod'') |
re''2~ re''16 re' mi' fad' |
sol'8 mi' fad' fad'' fad'' mi'' |
re''4. dod''8 si' lad' |
si'4 r fad' |
si' r fad' |
sol' r mi' |
fad' fad' fad' |
fad' fad' fad' |
si'8 mi' fad'2 |
si4 r r |
r r r8 re'' |
re''4 sol'2 |
re'8 mi' fad' sol' la' la |
re'2 r8 re''16 mi'' |
fad''8. sol''16 sol''4.\trill( fad''16 sol'') |
la''4. sol''16 fad'' mi''8. sol''16 |
fad''8 sol'' fad'' mi'' re''4 |
re'2 re''4 |
sol'2. |
re''4 re' re'' |
sol''8 fad'' mi''4. re''8 |
la' si' dod'' la' si' dod'' |
re''4 fad' re' |
