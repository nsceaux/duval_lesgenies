\clef "dessus" re''8 |
la'' si'' la'' sol'' fad'' mi'' |
fad'' la'' sol'' fad'' mi'' re'' |
dod''4.(\trill si'16 dod'') re''4 |
mi''4 \appoggiatura mi''8 fad''4. sol''8 |
mi''4\trill dod''8 mi'' fad'' sol'' |
la''4 sol''8 fad'' mi'' re'' |
dod'' la'' sol'' fad'' mi'' re'' |
dod''4. si'8 la'4 |
r r r8 sol'' |
mi''2\trill r8 la'' |
fad''2\trill r8 si'' |
sol''4 la''8 sol'' fad'' mi'' |
fad''4 sol''4. fad''8 |
mi''4 re''~ re''16 dod'' si' dod'' |
dod''8 la'' la'' mi'' fad'' sol'' |
dod'' la' dod'' mi'' dod'' la' |
fad'4 fad''8 sold'' la''4 |
sold''8 la'' si'' mi'' la''4 |
la''4. fad''8 sold'' la'' |
la''4( sold''4.)\trill la''8 |
la'' la'' mi'' sol'' dod''8.\trill( si'32 dod'') |
re''4 mi''8 fad''16 sol'' fad''8.\trill sol''16 |
mi''2 fad''8 sol'' |
la''4 sol''8 fad'' mi'' re'' |
dod'' la'' sol'' fad'' mi'' re'' |
dod'' si' la' sol' fad' fad'' |
\appoggiatura mi''8 re''2 r4 |
si'2 r4 |
dod''4 re''8 dod'' si' la' |
la''4 sol''8 fad'' mi'' re'' |
mi''4 re''~ re''16 dod'' si' dod'' |
dod''8 la'' la'' mi'' fad'' sol'' |
la''4 la''4. la''8 |
sold''4\trill \appoggiatura fad''8 mi''4. la''8 |
la''2. |
la''4 sold''4.\trill la''8 |
la'' la'16 dod'' si'8 re'' dod'' mi'' |
re'' fad'' mi'' sol'' fad''4~ |
fad''16 mi'' fad'' sol'' mi''4.\trill la''8 |
re'' dod'' re'' mi'' fad'' sol'' |
la'' si'' la'' sol'' fad''4 |
r16 fad'' sol'' la'' la''4.(\trill sol''16 la'') |
si''2~ si''8.*1/2 la''32 sol'' fad'' mi'' re'' |
re''4( dod''4.)\trill re''8 |
re''8 la' si' la' si' dod'' |
re'' mi'' mi''4.(\trill re''16 mi'') |
fad''8 sol'' sol''4.(\trill fad''16 sol'') |
la''2. |
si''8 dod'''16 re''' mi''4.\trill re''8 |
re''2. |
R2. |
si''4 si''4. lad''8 |
si''4 fad''8 mi'' re'' dod'' |
si'4 sol''8 fad'' mi'' re'' |
dod'' fad'' mi'' re'' dod'' si' |
lad'\trill dod'' si' dod'' re'' mi'' |
fad''4. mi''8 re''\trill dod'' |
re''4. mi''8 re''8. mi''16 |
re''8. mi''16 re''8. mi''16 re'' mi'' re'' mi'' |
re''4\trill si'8 dod'' re'' mi'' |
fad''8. sol''16 fad''8. sol''16 fad'' sol'' fad'' sol'' |
fad''8 sold''16 lad'' si''8 dod'''16 re''' dod''' re''' dod''' si'' |
lad'' fad'' sold'' lad'' si''4. lad''8 |
sold''8 lad''16 si'' dod'''4~ dod'''16 re''' dod''' si'' |
lad''8 si'' si''4.\trill( lad''16 si'') |
dod'''4. lad''8 si'' dod''' |
fad''4 fad''2 |
si' r4 |
r4 r sold'' |
dod''8 re''16 mi'' mi''4.(\trill re''16 mi'') |
fad'' sol'' fad'' sol'' fad''4. si''8 |
lad''16 dod'' re'' mi'' mi''4.(\trill re''16 mi'') |
fad''2~ fad''16 fad' sold' lad' |
si'8 dod'' lad' lad'' si'' dod''' |
fad''8. sol''16 fad''8. sol''16 fad''8. sol''16 |
fad''16 sol'' fad'' sol'' fad''4.\trill mi''8 |
re''16 fad'' mi'' fad'' fad''4.\trill mi''8 |
re''16 fad'' mi'' fad'' sol''8. la''16 sol'' fad'' mi'' re'' |
dod''4 fad''8 mi'' re'' dod'' |
re''4 re''8 dod'' si' lad' |
si' dod'' lad'4.\trill si'8 |
si'8. re''16 re''8. mi''16 mi''8.(\trill re''32 mi'') |
fad''4 \appoggiatura mi''8 re''4 r8 fad'' |
fad'' sol'' sol''4.(\trill fad''16 sol'') |
la''4. sol''16 fad'' mi''8 sol'' |
fad''4 \appoggiatura mi''8 re''4 r8 re'' |
re''8. mi''16 mi''4.(\trill re''16 mi'') |
fad''4. mi''16 re'' dod''8. mi''16 |
re''8 la' re'' mi'' fad'' sol'' |
la''4 sol''4.\trill fad''8 |
si''4 la''4.\trill sol''8 |
la''4 \appoggiatura sol''8 fad''4 r8 la'' |
si''4 dod'''4. re'''8 |
dod'''\trill si'' la'' sol'' fad'' mi'' |
fad''4\trill \appoggiatura mi''8 re''4 fad''8 sol'' |
