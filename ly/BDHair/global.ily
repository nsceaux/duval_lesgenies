\key re \major \midiTempo#120
\digitTime\time 3/4 \partial 8 s8 s2.*22 \bar "||" \segnoMark
s2.*28 \bar "|." \fineMark
s2.*45 \bar "|."
\ifComplet\endMark\markup\override #'(baseline-skip . 4) \right-column {
  \line { Reprise jusqu’au mot fin. \raise#1 \musicglyph#"scripts.segno" }
  \line { On reprend les passepieds. }
}
\ifConcert\endMark\markup {
  Reprise jusqu’au mot fin. \raise#1 \musicglyph#"scripts.segno"
}
