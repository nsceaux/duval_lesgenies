A -- mour, tu ré -- ponds à nos vœux,
tri -- om -- phe à ja -- mais de nos â -- mes,
ce n’est qu’en é -- prou -- vant tes flam -- mes,
que les cœurs peu -- vent être heu -- reux.
A -- mour, tu ré -- ponds à nos vœux,
tri -- om -- phe à ja -- mais de nos â -- mes,
ce n’est qu’en é -- prou -- vant tes flam -- mes,
que les cœurs peu -- vent être heu -- reux.
Ce n’est qu’en é -- prou -- vant tes flam -- mes,
que les cœurs peu -- vent être heu -- reux.

Tous les oi -- seaux de ces boc -- ca -- ges
sous tes lois goû -- tent des dou -- ceurs,
ils ne ra -- ni -- ment leurs ra -- ma -- ges
que pour cé -- lé -- brer tes fa -- veurs.
Ils ne ra -- ni -- ment leurs ra -- ma -- ges,
ils ne ra -- ni -- ment leurs ra -- ma -- ges
que pour cé -- lé -- brer tes fa -- veurs.
Tri -- om -- phe tri -- omphe à ja -- mais de nos â -- mes,
A-
