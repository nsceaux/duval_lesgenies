\piecePartSpecs
#`((dessus #:score "score-dessus")
   (dessus1)
   (dessus2)
   (flute-hautbois #:instrument "Flûte" #:notes "flute")
   
   (silence #:on-the-fly-markup , #{

\markup\fontsize#-2 \column {
  \livretPers La principale Nymphe
  \livretVerse#8 { Amour, tu réponds à mes vœux, }
  \livretVerse#8 { Triomphe à jamais de nos âmes, }
  \livretVerse#8 { Ce n’est qu’en éprouvant tes flammes, }
  \livretVerse#8 { Que les cœurs peuvent être heureux. }
  \livretVerse#8 { Tous les oiseaux de ces boccages }
  \livretVerse#8 { Sous tes lois goûtent des douceurs, }
  \livretVerse#8 { Ils ne raniment leurs ramages }
  \livretVerse#8 { Que pour célébrer tes faveurs. }
  \livretVerse#8 { Triomphe à jamais de nos âmes, }
  \livretVerse#8 { Amour, &c. }
}#}))
