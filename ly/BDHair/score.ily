\score {
  \new ChoirStaff <<
    \new Staff \with { instrumentName = "Flûtes" } <<
      \global \includeNotes "flute"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column\smallCaps { La principale Nymphe }
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Violons" } <<
      \global \includeNotes "dessus"
      \includeFigures "chiffres"
      \origLayout {
        s8 s2.*7\break s2.*7\pageBreak
        s2.*5\break s2.*6\pageBreak
        s2.*5\break s2.*4\pageBreak
        s2.*6\break \grace s8 s2.*6\pageBreak
        s2.*7\break s2.*5\pageBreak
        s2.*5\break s2.*5\pageBreak
        s2.*6\break s2.*5\pageBreak
        s2.*5\break s2.*5\pageBreak
        s2.
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
