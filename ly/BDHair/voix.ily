\clef "vdessus" r8 |
R2.*3 |
r4 r r8 re'' |
la'2 re''8 mi'' |
fad''4 sol''4. fad''8 |
mi''2 r4 |
r r r8 la' |
si'4.\melisma dod''16[ si'] la'8[ si'] |
dod''4. re''16[ dod''] si'8[ dod''] |
re''4. mi''16[ re''] dod''8[ re'']( |
mi''4)\melismaEnd fad''8[ mi''] re''[ dod''] |
re''4 mi''4. fad''8 |
sol''4( \afterGrace fad''2\trill mi''8) |
mi''4\trill r r |
r r r8 mi'' |
fad'' mi'' re'' dod'' si' la' |
si'4 sold'\trill mi''8 dod'' |
fad''2 si'8 dod'' |
dod''4( si'4.)\trill la'8 |
la'2 r4 |
r r r8 re'' |
la'2 re''8 mi'' |
fad''4 sol''4. fad''8 |
mi''2\trill r4 |
r4 r r8 la' |
si'16[\melisma dod'' si' la'] sol'[ la' sol' la'] si'[ dod'' si' dod''] |
re''[ mi'' re'' dod''] si'[ dod'' si' dod''] re''[ dod'' re'' si']( |
mi''4)\melismaEnd fad''8[ mi''] re''[ dod''] |
re''4 mi''4. fad''8 |
sol''2( fad''8\trill[ mi'']) |
mi''2 r8 mi'' |
fad'' mi'' re'' dod'' si' la' |
si'4 sold'\trill mi''8 dod'' |
fad''2 si'8 dod'' |
dod''4( si'4.\trill) la'8 |
la'4 r r |
R2. |
r4 r r8 la' |
si' la' si' dod'' re'' mi'' |
\appoggiatura mi''8 fad''2 \appoggiatura mi''8 re''4 |
r r fad''8 re'' |
sol''2 mi''8 fad'' |
fad''4( mi''4.)\trill re''8 |
re''2 r4 |
R2.*5 |
fad''4 sol''4. \appoggiatura fad''8 mi'' |
fad''4. sol''8 fad'' mi'' |
re''4\trill \appoggiatura dod''8 si'4 re''8 mi'' |
fad''4 mi''8 re'' dod'' re'' |
lad'2.\trill |
r4 r8 fad' si' dod'' |
re''4. mi''8 fad'' sol'' |
fad''4.\melisma sol''8 fad''8.[ sol''16] |
fad''8.[ sol''16] fad''8.[ sol''16] fad''[ sol'' fad'' sol'']( |
fad''2)\trill\melismaEnd \appoggiatura mi''8 re''4 |
r mi''8 mi'' re'' dod'' |
re''[ mi''] mi''4.\trill fad''8 |
fad''2 r4 |
R2.*2 |
r4 r8 fad' sold' lad' |
si'4. dod''8 la' si' |
sold'8[\melisma la'16 si'] si'4.\trill la'16[ si'] |
dod''4. re''16[ dod''] si'8.[ dod''16] |
lad'8[ si'16 dod''] dod''4.(\trill si'16[ dod'']) |
re''[ mi'' re'' mi'']( re''2)\trill\melismaEnd |
dod''4 r r |
R2. |
r4 r8 fad' sold' lad' |
si'4. lad'8 si' dod'' |
re''16[\melisma mi'' re'' mi''] re''4.\trill dod''8 |
si'16[ re'' dod'' re''] re''4.\trill dod''8 |
si'16[ re'' dod'' re''] mi''8.[ fad''16] mi''[ re'' dod'' si']( |
lad'2)\trill\melismaEnd \appoggiatura sold'8 fad'4 |
r4 fad''8 mi'' re'' dod'' |
re''[ mi''] dod''4.\trill si'8 |
si'2 r4 |
r r r8 re'' |
re''[\melisma mi''] mi''4.(\trill re''16[ mi'']) |
fad''4. mi''16[ re''] dod''8[ mi'']( |
re''4)\melismaEnd la' r |
R2.*2 |
r4 r r8 re'' |
fad''4 mi''4.\trill re''8 |
sol''4 fad''4.\trill mi''8 |
fad''4 \appoggiatura mi''8 re''4 r |
R2.*2 |
r4 r r8 re'' |
