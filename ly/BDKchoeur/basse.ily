\clef "basse" re8 |
la4 sol8 |
fad mi re |
dod si, la, |
re4 re,8 |
la,4 re'8 |
dod' la sol |
fad mi re |
dod si, la, |
re4
\clef "alto" re'8 |
la'4 fad'8 |
sol'4 fad'8 |
sol' mi' la' |
re'4. |
la4 re'8 |
la'4 fad'8 |
sol'4 fad'8 |
sol' mi' la' |
re'4
\clef "bass" <>^"B.C." re'8 |
re' dod' si |
la4 sold8 |
la4 si8 |
dod'4 dod'8 |
dod' si la |
re4. |
mi8 mi,4 |
la,4 la8 |
dod si, la, |
re mi fad |
dod si, la, |
re mi fad |
sol4 sol8 |
sol fad sol |
la la,4 |
re4
\clef "alto" <>^"B.C." la'8 |
la'4 la'8 |
la'4 la'8 |
la'4 la'8 |
la'4 la'8 |
sol' fad' mi' |
fad' re' sol |
la4. |
re'4
\clef "bass" si8 |
si4 r8 |
si4 r8 |
mi4 r8 |
si,4 r8 |
mi4 r8 |
si4 r8 |
lad si dod' |
si4 si,8 |
fad4 mi8 |
re4 r8 |
sol,8 la,4 |
re8 mi fad |
sol4 r8 |
dod4 fad8 |
si,4 sol,8 |
mi, fad,4 |
si,4
