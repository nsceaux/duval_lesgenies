\clef "dessus" r8 |
R4.*8 |
r8 r fad'' |
mi''4 la''8 |
re''4 fad''8 |
mi'' fad'' sol'' |
fad''4.\trill |
mi''4 fad''8 |
mi''4 la''8 |
re''4 fad''8 |
mi'' fad'' sol'' |
fad''4\trill r8 |
R4.*15 |
r8 r mi'' |
mi'' fad'' sol'' |
fad'' sol'' la'' |
mi'' fad'' sol'' |
fad'' sol'' la'' |
mi'' fad'' sol'' |
dod'' re''8. mi''16 |
fad''8( mi''4)\trill |
re''4 r8 |
R4.*16 |
r4
