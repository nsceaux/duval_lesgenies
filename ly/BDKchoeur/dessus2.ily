\clef "dessus" r8 |
R4.*8 |
r8 r re'' |
dod''4 do''8 |
si'4 re''8 |
re''4 dod''8 |
re''4. |
dod''4 re''8 |
dod''4 do''8 |
si'4 re''8 |
re''4 dod''8 |
re''4 r8 |
R4.*15 |
r8 r dod'' |
dod'' re'' mi'' |
re'' mi'' fad'' |
dod'' re'' mi'' |
re'' mi'' fad'' |
dod'' re'' mi'' |
la' la'8. re''16 |
re''4( dod''8) |
re''4 r8 |
R4.*16 |
r4