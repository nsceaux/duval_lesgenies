\key re \major \midiTempo#92 \segnoMark
\time 3/8 \partial 8 s8 s4.*8 s4 \bar ""
\beginMarkSmall "Petit chœur" s8 s4.*8 s4
s8 s4.*15 s4 \bar ""
\beginMarkSmall "Petit chœur" s8 s4.*7 s4 \bar "||" \endMark "[Fin.]"
s8 s4.*16 s4 \bar "|."
\endMark\markup { Tout entier jusqu’au mot fin \raise#1 \musicglyph#"scripts.segno" }
