\tag #'(nymphe basse) {
  Ri -- ons, chan -- tons sous cet om -- bra -- ge,
  tout y ré -- pond à nos dé -- sirs ;
}
\tag #'(vdessus1 vdessus2 vhaute-contre) {
  Ri -- ons, chan -- tons sous cet om -- bra -- ge,
  tout y ré -- pond à nos dé -- sirs ;
}
\tag #'(nymphe basse) {
  L’a -- mour y ca -- che les plai -- sirs
  dont no -- tre prin -- temps fait u -- sa -- ge.
  L’a -- mour y ca -- che les plai -- sirs
  dont no -- tre prin -- temps fait u -- sa -- ge.
}
\tag #'(vdessus1 vdessus2 vhaute-contre) {
  L’a -- mour y ca -- che les plai -- sirs
  dont no -- tre prin -- temps fait u -- sa -- ge.
}
\tag #'(nymphe basse) {
  Sans soins, sans crain -- te des ja -- lous,
  nous nous li -- vrons à la ten -- dres -- se ;
  et le tendre a -- mour ne nous bles -- se,
  que pour nous faire un sort plus doux.
}
