\piecePartSpecs
#`((dessus #:score "score-dessus")
   (dessus1 #:notes "dessus1"
            #:instrument "Violons"
            #:score-template "score-voix")
   (dessus2 #:notes "dessus2"
            #:instrument "Violons"
            #:score-template "score-voix")
   (basse #:instrument "B.C." #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{

\markup\fontsize#-2 \fill-line {
  \column {
    \livretPers Une Nymphe
    \livretVerse#12 { Rions, chantons sous cet ombrage, }
    \livretVerse#12 { Tout y répond à nos désirs ; }
    \livretVerse#12 { L’Amour y cache les plaisirs }
    \livretVerse#12 { Dont notre printemps fait usage. }
    \livretPers Chœur
    \livretVerse#12 { Rions, chantons, &c. }
  }
  \column {
    \livretPers La Nymphe
    \livretVerse#12 { Sans soins, sans crainte des jalous, }
    \livretVerse#12 { Nous nous livrons à la tendresse ; }
    \livretVerse#12 { Et le tendre amour ne nous blesse, }
    \livretVerse#12 { Que pour nous faire un sort plus doux. }
    \livretPers Chœur
    \livretVerse#12 { Rions, chantons sous cet ombrage, }
    \livretVerse#12 { Tout y répond à nos désirs ; }
    \livretVerse#12 { L’Amour y cache les plaisirs }
    \livretVerse#12 { Dont notre printemps fait usage. }
  }
}#}))
