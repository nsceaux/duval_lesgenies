\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiri \tinyStaff } \withLyrics <<
      \global \keepWithTag #'nymphe \includeNotes "voix"
    >> { \set fontSize = -2 \keepWithTag #'nymphe \includeLyrics "paroles" }
    \new GrandStaff \with { instrumentName = "Violons" \haraKiri } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
  >>
  \layout { indent = \largeindent }
}
