\score {
  \new ChoirStaff <<
    \new GrandStaff \with { instrumentName = "Violons" \haraKiri } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new ChoirStaff \with { \haraKiri } <<
      \new Staff \with {
        instrumentName = \markup\center-column { [Premiers Dessus] }
      } \withLyrics <<
        \global \keepWithTag #'vdessus1 \includeNotes "voix"
      >> \keepWithTag #'vdessus1 \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\center-column { [Seconds Dessus] }
      } \withLyrics <<
        \global \keepWithTag #'vdessus2 \includeNotes "voix"
      >> \keepWithTag #'vdessus2 \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\center-column { [Hautes-contre] }
      } \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
    >>
    \new Staff \with {
      \haraKiri
      instrumentName = \markup\character "Une Nymphe"
    } \withLyrics <<
      \global \keepWithTag #'nymphe \includeNotes "voix"
    >> \keepWithTag #'nymphe \includeLyrics "paroles"
    \new Staff \with { instrumentName = "[B.C.]" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s8 s4.*8 s4 \bar "" \pageBreak
        s8 s4.*9\break s4.*10\break s4.*10\pageBreak
        s4.*9\break s4.*9\break
      }
      \modVersion {
        s8 s4.*8\break s4.*10\break s4.*15\break s4.*8 s4\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
