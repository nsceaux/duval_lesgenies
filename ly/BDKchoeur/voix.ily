<<
  \tag #'(nymphe basse) {
    \clef "vdessus" fad''8 |
    mi''4\trill la''8 |
    re''4 fad''8 |
    mi''([ fad'']) sol'' |
    \grace sol''^( \afterGrace fad''4.\trill mi''8) |
    mi''4 fad''8 |
    mi''4 la''8 |
    re''4 fad''8 |
    mi''([ fad'']) sol'' |
    fad''4\trill r8 |
    R4.*8 |
    r8 r fad''8 |
    fad''([ mi''])\trill re'' |
    dod''4\trill si'8 |
    dod''4\trill re''8 |
    mi''4 mi''8 |
    mi'' re'' dod'' |
    fad'' si'8. dod''16 |
    dod''8( si'4)\trill |
    la'4 mi''8 |
    mi''([ fad'']) sol'' |
    fad''([ sol'']) la'' |
    mi''([ fad'']) sol'' |
    fad''([ sol'']) la'' |
    mi'' fad'' sol'' |
    dod'' re''8. mi''16 |
    fad''8( mi''4) |
    re''4 r8 |
    R4.*7 |
    r8 r fad'' |
    fad''4 mi''8 |
    fad''4 sol''8 |
    sol''([ fad''])\trill mi'' |
    fad''4 sol''8 |
    sol''([ fad'']) mi'' |
    fad''4 sol''8 |
    sol''([ fad''])\trill mi'' |
    re''4. |
    dod''8.\trill re''16 mi''8 |
    fad''4 mi''8 |
    sol'' fad'' mi'' |
    fad''4. |
    \appoggiatura mi''8 re''4 mi''8 |
    mi''([ re''\trill]) dod'' |
    re''4 mi''8 |
    dod''4\trill lad'8\trill |
    si'4
  }
  \tag #'vdessus1 {
    \clef "vdessus" r8 |
    R4.*8 |
    r8 r fad''8 |
    mi''4 la''8 |
    re''4 fad''8 |
    mi''[ fad''] sol'' |
    fad''4. |
    mi''4 fad''8 |
    mi''4 la''8 |
    re''4 fad''8 |
    mi''[ fad''] sol'' |
    fad''4\trill r8 |
    R4.*15 |
    r8 r mi'' |
    mi''[ fad''] sol'' |
    fad''[ sol''] la'' |
    mi''[ fad''] sol'' |
    fad''[ sol''] la'' |
    mi'' fad'' sol'' |
    dod''\trill re''8. mi''16 |
    fad''8( mi''4)\trill |
    re''4 r8 |
    R4.*16 |
    r4
  }
  \tag #'vdessus2 {
    \clef "vdessus" r8 |
    R4.*8 |
    r8 r re'' |
    dod''4 do''8 |
    si'4 re''8 |
    re''4 dod''8 |
    re''4. |
    dod''4 re''8 |
    dod''4 do''8 |
    si'4 re''8 |
    re''4 dod''8 |
    re''4 r8 |
    R4.*15 |
    r8 r dod'' |
    dod''[ re''] mi'' |
    re''[ mi''] fad'' |
    dod''[ re''] mi'' |
    re''[ mi''] fad'' |
    dod'' re'' mi'' |
    la' la'8. re''16 |
    re''4( dod''8) |
    re''4 r8 |
    R4.*16 |
    r4
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" r8 |
    R4.*8 |
    r8 r re'8 |
    la'4 fad'8 |
    sol'4 fad'8 |
    sol'[ mi'] la' |
    re'4. |
    la4 re'8 |
    la'4 fad'8 |
    sol'4 fad'8 |
    sol'[ mi'] la' |
    re'4 r8 |
    R4.*15 |
    r8 r la'8 |
    la'4 la'8 |
    la'4 la'8 |
    la'4 la'8 |
    la'4 la'8 |
    sol' fad' mi' |
    fad' re' sol |
    la4. |
    re'4 r8 |
    R4.*16 |
    r4
  }
>>
