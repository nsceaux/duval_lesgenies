\clef "basse"
\setMusic #'rondeau {
  re4 re |
  re re |
  re16 dod re mi re dod re mi |
  re4 re |
  fa16 mi fa sol fa mi fa sol |
  fa8 re16 mi fa8 re |
  dod la, re dod |
  re sol, la, la,, |
  re4 re |
  re re |
  re16 dod re mi re dod re mi |
  re4 re |
  fa16 mi fa sol fa mi fa sol |
  fa8 re16 mi fa8 re |
  dod la, re la, |
  re,4 re, |
}
\keepWithTag #'() \rondeau
fa4 fa |
fa fa |
fa8 fa, do, do |
fa16 mi fa sol mi re mi do |
fa4 fa |
fa16 mi fa sol fa mi fa sol |
fa8 fa,16 sol, la, sib, do8 |
fa,4 fa, |
\rondeau
do16 si, do re mi8 mi, |
la,4 r |
re8 re'16 do' si8 si |
do'16 si la sold la8 la,16 si, |
do si, do re mi8 mi, |
la, la sold la |
mi'4 mi |
la, la, |
\rondeau
