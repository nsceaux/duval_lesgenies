\clef "basse"
\setMusic #'rondeau {
  re4 re |
  re16 dod re mi re dod re mi |
  re4 re |
  fa16 mi fa sol fa mi fa sol |
  fa4 fa |
  r8 re16 mi fa8 re |
  dod la, re dod |
  re sol, la, la16 sol |
  fa4 fa |
  re16 dod re mi re dod re mi |
  re4 re |
  fa16 mi fa sol fa mi fa sol |
  fa4 fa |
  r8 re16 mi fa8 re |
  dod la, re la, |
  re4 re |
}
\keepWithTag #'() \rondeau
fa4 fa |
fa16 mi fa sol fa mi fa sol |
fa mi fa sol mi re mi do |
fa4 sib |
la16 sol la sib la sol la sib |
la sol la sib do' sib la sol |
fa8 fa,16 sol, la, sib, do8 |
fa,4 fa, |
\rondeau
la4 la8 sold\trill |
la16 sold la si sold fad sold la |
re8 re'16 do' si8 si |
do'16 si la sold la8 mi16 re |
do re do si, la,8 sold, |
la, la sold la |
mi'4 mi |
la la |
\rondeau
