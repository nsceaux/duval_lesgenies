\setMusic #'rondeau {
  s2*4 <6>8.\figExtOn <6>16\figExtOff <6>8.\figExtOn <6>16\figExtOff
  <6>8.\figExtOn <6>16\figExtOff s8 <6- 3> <5/>4. <5/>8 s8 <7 5> <4> \new FiguredBass <_+> s2
  s2 <"">8\figExtOn <"">\figExtOff <"">8\figExtOn <"">\figExtOff s2
  <6>8.\figExtOn <6>16\figExtOff
  <6>8.\figExtOn <6>16\figExtOff
  <6>8.\figExtOn <6>16\figExtOff s8 <6- 3> <5/>4. <_+>8 s2
}
\keepWithTag #'() \rondeau
<5/>2 s2*2 <"">8.\figExtOn <"">16 <5/>8. <5/>16\figExtOff s2
<"">8.\figExtOn <"">16\figExtOff <"">8.\figExtOn <"">16\figExtOff s2*2
\rondeau
<6>4 <6 4>8 <_+> s2 s4 <6+ 5/> <6>8. <5/>16 s4 <6 5>8.\figExtOn <6>16\figExtOff <6 4>8 <_+> s4 <5/> <6 4> <_+> s <_+>
\rondeau

