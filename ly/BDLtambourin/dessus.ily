\clef "dessus"
\setMusic #'rondeau {
  re''16 dod'' re'' mi'' re'' dod'' re'' mi'' |
  re''4 re'' |
  fa''16 mi'' fa'' sol'' fa'' mi'' fa'' sol'' |
  fa''4 fa'' |
  la''16 sol'' la'' sib'' la'' sol'' la'' sib'' |
  la''8 re''' la'' sib'' |
  la'' sol'' fa''\trill mi'' |
  fa''4 mi''\trill |
  re''16 dod'' re'' mi'' re'' dod'' re'' mi'' |
  re''4 re'' |
  fa''16 mi'' fa'' sol'' fa'' mi'' fa'' sol'' |
  fa''4 fa'' |
  la''16 sol'' la'' sib'' la'' sol'' la'' sib'' |
  la''8 re''' la'' sib'' |
  la'' sol'' fa'' mi'' |
  re''4 re'' |
}
\keepWithTag #'() \rondeau
fa''16 mi'' fa'' sol'' fa'' mi'' fa'' sol'' |
fa''4 fa'' |
la''16 sol'' la'' sib'' do''' re''' do''' sib'' |
la''4 sol''\trill |
fa''16 mi'' fa'' sol'' fa'' mi'' fa'' sol'' |
fa''4 fa'' |
la''16 sol'' la'' sib'' do''' sib'' la'' sol'' |
fa''4 fa'' |
\rondeau
la''16 sold'' la'' si'' do'''8 si''\trill |
la''4 mi'' |
fa''16 mi'' fa'' sol'' fa'' mi'' re'' fa'' |
mi'' re'' do'' si' la'4 |
la''16 sold'' la'' si'' do'''8 si'' |
la''8 do''' si'' la'' |
mi''4 sold''\trill |
la''4 la'' |
\rondeau