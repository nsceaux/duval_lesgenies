\clef "dessus"
\setMusic #'rondeau {
  re'4 re' |
  re'16 dod' re' mi' re' dod' re' mi' |
  re'4 re' |
  fa'16 mi' fa' sol' fa' mi' fa' sol' |
  fa'4 fa' |
  r8 re'16 mi' fa'8 re' |
  dod' la re' dod' |
  re' sol la la'16 sol' |
  fa'4 fa' |
  re'16 dod' re' mi' re' dod' re' mi' |
  re'4 re' |
  fa'16 mi' fa' sol' fa' mi' fa' sol' |
  fa'4 fa' |
  r8 re'16 mi' fa'8 re' |
  dod' la re' la |
  re'4 re' |
}
\keepWithTag #'() \rondeau
fa'4 fa' |
fa'16 mi' fa' sol' fa' mi' fa' sol' |
fa' mi' fa' sol' mi' re' mi' do' |
fa'4 sib' |
la'16 sol' la' sib' la' sol' la' sib' |
la' sol' la' sib' do'' sib' la' sol' |
fa'8 fa'16 sol' la' sib' do''8 |
fa'4 fa' |
\rondeau
la'4 la'8 sold'\trill |
la'16 sold' la' si' sold' fad' sold' la' |
re'8 re''16 do'' si'8 si' |
do''16 si' la' sold' la'8 mi'16 re' |
do' re' do' si la8 sold |
la la' sold' la' |
mi''4 mi' |
la' la' |
\rondeau
