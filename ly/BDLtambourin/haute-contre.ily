\clef "haute-contre"
\setMusic #'rondeau {
  fa'16 mi' fa' sol' fa' mi' fa' sol' |
  fa'4 fa' |
  la' la' |
  la' la' |
  re'' re'' |
  re'' re''8 fa' |
  mi' mi' fa'16 sol' la'8 |
  la' sib' mi' la' |
  la'4 la' |
  la' la' |
  la' la' |
  la' la' |
  re'' re'' |
  re'' re''8 fa' |
  mi' mi' re' dod'\trill |
  re'4 re' |
}
\keepWithTag #'() \rondeau
la'16 sol' la' sib' la' sol' la' sib' |
la'4 la' |
la' sol' |
fa' sol' |
do' do' |
do' do' |
fa' fa'8 mi' |
fa'4 fa' |
\rondeau |
mi'4 mi' |
mi' r |
la' sold'\trill |
sold' la' |
mi'' mi'' |
mi'' mi'' |
do'' si' |
la' la' |
\rondeau |
