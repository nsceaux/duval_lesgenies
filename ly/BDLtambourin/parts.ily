\piecePartSpecs
#`((dessus #:instrument
           ,#{\markup\center-column { [Violons et Hautbois] }#})
   (parties)
   (dessus2-haute-contre)
   (taille)
   ,(if (ly:get-option 'concert)
        `(basse #:instrument ,#{\markup\center-column { [Basses, Bassons] }#})
        `(basse #:score "score-basse"))

   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
