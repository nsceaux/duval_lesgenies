\clef "taille"
\setMusic #'rondeau {
  la4 la |
  la la |
  la la |
  la la |
  re' re' |
  re'8 fa16 sol la8 re |
  mi la la mi' |
  re' re' re' dod'\trill |
  re'4 re' fa' fa' |
  la la |
  la la |
  re' re' |
  re'8 fa16 sol la8 re |
  mi la la la |
  fa4 fa
}
\keepWithTag #'() \rondeau
do'4 do' |
do' do' |
do' do' |
do' do' |
do' do' |
do' do' |
do' do'8 sib |
la4 la |
\rondeau |
mi'4 la8 re' |
do'4 r |
la'16 sol' fa' mi' re' mi' fa' re' |
mi'8 la16 si do' si do' re' |
mi' fa' mi' re' do'8 re' |
do'16 si la8 si do'16 re' |
mi'4 re' |
dod'\trill dod' |
\rondeau |
