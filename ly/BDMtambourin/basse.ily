\clef "basse" re4 re |
re re |
sol16 fad sol la si8 dod' |
re' re dod16 la, si, dod |
re4 re |
re re |
sol16 fad sol la si8 sol |
la4 la |
la la |
la, la, |
la, la, |
la, la, |
la, la, |
la, la, |
la, la8 sol |
fad sol la la, |
re4 re |
