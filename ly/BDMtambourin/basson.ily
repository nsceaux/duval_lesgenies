\clef "basse" re16 mi fad sol la8 la, |
re16 dod re mi fad4 |
sol16 fad sol la si8 dod' |
re' re la16 la, si, dod |
re mi fad sol la8 la, |
re16 dod re mi fad4 |
sol16 fad sol la si8 re' |
dod'8. si16 la sol fad mi |
la2 |
dod'16 si dod' re' dod' si dod' sold |
la sold la si la si la sol |
fad8 la16 sol fad mi re dod |
re4 dod |
dod'16 si dod' re' dod' si dod' sold |
la sold la si la8 sol |
fad sol la la, |
re4 re |
