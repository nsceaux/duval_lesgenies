\clef "dessus" re''16 dod'' re'' mi'' fad''8 mi''\trill |
re''4 re''8 la'' |
si''16 la'' si'' dod''' re'''8 la'' |
fad''4\trill mi''\trill |
re''16 dod'' re'' mi'' fad''8 mi''\trill |
re''4 re''8 la'' |
si''16 la'' si'' dod''' re'''8 fad'' |
mi''4\trill mi'' |
mi''4\trill mi'' |
la''16 sold'' la'' si'' la'' sold'' la'' si'' |
la''4 la'' |
re'''8 dod'''16 si'' la'' sol'' fad'' mi'' |
fad''4\trill mi''8 la' |
la''16 sold'' la'' si'' la'' sold'' la'' si'' |
la''4 la''16 si'' dod''' la'' |
re'''8 dod'''16 si'' la'' sol'' fad'' mi'' |
re''4 re'' |
