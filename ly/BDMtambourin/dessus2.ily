\clef "dessus" re'16 mi' fad' sol' la'8 la |
re'16 dod' re' mi' fad'4 |
sol'16 fad' sol' la' si'8 dod'' |
re'' re' la'16 la si dod' |
re' mi' fad' sol' la'8 la |
re'16 dod' re' mi' fad'4 |
sol'16 fad' sol' la' si'8 re'' |
dod''8. si'16 la' sol' fad' mi' |
la'2 |
dod''16 si' dod'' re'' dod'' si' dod'' sold' |
la' sold' la' si' la' si' la' sol' |
fad'8 la'16 sol' fad' mi' re' dod' |
re'4 dod' |
dod''16 si' dod'' re'' dod'' si' dod'' sold' |
la' sold' la' si' la'8 sol' |
fad' sol' la' la |
re'4 re' |
