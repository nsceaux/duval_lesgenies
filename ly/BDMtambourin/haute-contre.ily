\clef "haute-contre" fad'16 sol' fad' mi' re'8 dod'\trill |
re' la re' re''16 dod'' |
si' dod'' si' la' sol'8 mi' |
la'4. la'16 sol' |
fad' sol' fad' mi' re'8 dod'\trill |
re' la re' re''16 dod'' |
si' dod'' si' la' sol'8 si' |
dod''8. re''16 dod'' si' la' sol' |
dod''4 dod'' |
mi' mi'8 mi'16 re' |
dod' si dod' re' dod' re' dod' si |
la8 re' re' re'16 mi' |
re'4 mi' |
mi'4. mi'16 re' |
dod' si dod' re' dod' re' mi' dod' |
re'8 la'16 sol' fad' mi' fad' sol' |
fad'4\trill fad' |
