\score {
  \new StaffGroup <<
    \ifConcert <<
      \new GrandStaff <<
        \new Staff \with {
          instrumentName = \markup\center-column { [Violons I Hautbois] }
        } << \global \includeNotes "dessus" >>
        \new Staff \with { instrumentName = "[Violons II]" } <<
          \global \includeNotes "dessus2"
        >>
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { [Basses, Bassons] }
      } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
    \ifComplet <<
      \new Staff \with {
        instrumentName = \markup\center-column { [Violons et Hautbois] }
      } << \global \includeNotes "dessus" >>
      \ifFull\new Staff \with {
        instrumentName = \markup\center-column { [Hautes-contre] }
      } << \global \includeNotes "haute-contre" >>
      \ifFull\new Staff \with { instrumentName = "[Tailles]" } <<
        \global \includeNotes "taille"
      >>
      \new Staff \with {
        instrumentName = "[Bassons]"
      } << \global \includeNotes "basson" >>
      \new Staff \with { instrumentName = "[Basses]" } <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \origLayout { s2*9\break }
      >>
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
