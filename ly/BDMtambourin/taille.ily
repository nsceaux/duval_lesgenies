\clef "taille" la4 la8 sol |
fad16 mi fad sol la8 la' |
sol'16 la' sol' fad' mi'8 mi' |
re'4\trill dod'\trill |
la4 la8 sol |
fad16 mi fad sol la4 |
sol'16 la' sol' fad' mi'8 re' |
la4 la |
la4 la |
mi' mi' |
mi' mi' |
fad'4. fad'16 sol' |
fad'4 la |
mi' mi' |
mi' mi' |
re'8 re' re' dod'\trill |
re'4 re' |
