\clef "basse" si,4 fad sol |
fad2 si8 la sol fad |
mi2 sold mi |
la4 sol fad8. mi16 |
re4 la la, |
re r dod |
si, sol,8 mi, fad,2 |
si,4 si4. re'8 |
lad4 si4. fad8 |
sol2 fad8 mi |
re4 mi8 fad sol mi |
fad2
