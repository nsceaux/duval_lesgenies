\tag #'(nymphe basse) {
  Tout pré -- vient i -- ci vos dé -- sirs,
  la sé -- vè -- re sa -- gesse, et la rai -- son cru -- el -- le
  ne sau -- raient trou -- bler nos plai -- sirs ;
  mais soy -- ez- moi tou -- jours fi -- dè -- le.

  Ai -- mons- nous, ai -- mons- nous d’une ar -- deur é -- ter -- nel -- le.
}
\tag #'leandre {
  Ai -- mons- nous d’une ar -- deur é -- ter -- nel -- le.
}
