\piecePartSpecs
#`((basse #:instrument "[B.C.]"
          #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{

\markup\fontsize#-2 \column {
  \livretPersDidas La principale Nymphe à Léandre
  \livretVerse#8 { Tout prévient ici vos désirs, }
  \livretVerse#12 { La sévère sagesse, & la raison cruelle }
  \livretVerse#8 { Ne sauraient troubler nos plaisirs ; }
  \livretVerse#8 { Mais soyez-moi toujours fidèle. }
  \livretPers Ensemble
  \livretVerse#12 { Aimons-nous, aimons-nous d’une ardeur éternelle. }
}#}))
