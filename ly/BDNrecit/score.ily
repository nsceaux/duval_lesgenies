\score {
  \new ChoirStaff <<
    \new Staff \with {
      instrumentName = \markup\center-column\smallCaps { La principale Nymphe }
    } \withLyrics <<
      \global \keepWithTag #'nymphe \includeNotes "voix"
    >> \keepWithTag #'nymphe \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'leandre \includeNotes "voix"
    >> \keepWithTag #'leandre \includeLyrics "paroles"
    \new Staff \with { instrumentName = "[B.C.]" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2. s1 s1.\break s2.*3 s1 s4 \bar "" \break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
