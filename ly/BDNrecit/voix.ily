<<
  \tag #'(nymphe basse) {
    \clef "vdessus" <>^\markup\italic à Léandre
    r8 re''16 mi'' dod''8.\trill re''16 si'8\trill lad'16 si' |
    lad'4\trill r8 fad''16 sol'' red''4\trill mi''8 fad'' |
    sol''2 si'4 si'8 si' mi''4. mi''8 |
    dod''4\trill dod'' la'16 la' re'' mi'' |
    \appoggiatura mi''8 fad''4 mi''4.\trill re''8 |
    re''4 fad''4. lad'16 lad' |
    si'8. si'16 dod''8. re''16 dod''4.(\trill si'8) |
    si'4 re''4. fad''8 |
    dod''4 re''4. dod''8 |
    si'2 si'8 lad' |
    si'8.([ dod''32 re'']) dod''4.\trill si'8 |
    lad'4\trill lad'
  }
  \tag #'leandre {
    \clef "vhaute-contre" R2. R1 R1. R2.*3 R1 R2. |
    r4 <>^\markup\character Léandre fad'4. mi'8 |
    re'2 re'8 dod' |
    re'8.([ mi'32 fad']) mi'4.\trill re'8 |
    dod'4\trill dod'
  }
>>