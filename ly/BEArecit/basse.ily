\clef "basse" si8 si, |
mi dod fad4 mi re8 lad, |
si,2 mi4 |
sold,2 la,4 lad, |
si,2 si,4 |
dod4. la,8 sold,8. fad,16 dod8 dod, |
fad,4 fad2 |
si4 mi sold16 mi fad sold |
la la sol fad mi re dod si, la,2 |
sol,4 fad,8 sol, la,4 |
re2 do4 |
si, si8 la sol fad |
mi2. |
sold2 mi4 |
la2 la,4 |
re re2 |
sol la8. sib16 |
do'1~ |
do'2 si |
do'4. do'8 sol8. la16 sib8 sol |
re'2. re8 mi |
fa1 |
fa8 la, sib, sol, do4 do, |
fa, fa8 mib re do |
sib,2.~ |
sib, |
la,2 fa,4 |
sib,2 do4 do, |
fa,8 fa mi4 re |
do4 sib,8 la, sol,8. la,16 |
sib,4. la,8 sol, fa, |
do4 do16 do' sib la sol fa mi re |
\once\set Staff.whichBar = "|"
