\key si \minor
\once\omit Staff.TimeSignature
\digitTime\time 3/4 \partial 4 \midiTempo#80 s4
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.*2
\digitTime\time 2/2 s1
\digitTime\time 3/4 s2.*6 s4 \bar "||"
\key la \minor s2 s2.
\digitTime\time 2/2 s1*2
\time 4/4 s1
\digitTime\time 2/2 \midiTempo#160 s1*2
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.*4
\digitTime\time 2/2 s1
\digitTime\time 3/4 s2.*4 \bar "|."
