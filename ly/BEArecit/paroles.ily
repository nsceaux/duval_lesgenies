Pour -- suis, in -- grat, pour -- suis vo -- lage, a -- mant sans foi,
fais é -- cla -- ter tes feux au -- près de cet -- te bel -- le :
va, tu peux lui ju -- rer une ar -- deur é -- ter -- nel -- le,
que ton cœur n’a pro -- mis qu’à moi.
Per -- fi -- de, gar -- de- toi de pa -- raître à ma vu -- e ;
c’en est fait, pour ja -- mais mes li -- ens sont rom -- pus.

Hé -- las ! je vous ai donc per -- du -- e,
Lu -- ci -- le, vous fuy -- ez !

Vous ne la ver -- rez plus.

Ah ! puis- je sou -- te -- nir un si san -- glant ou -- tra -- ge,
sans im -- mo -- ler un traitre à ma fu -- reur ?
Je sens que mon â -- me s’a -- ban -- donne à la ra -- ge,
per -- fi -- de, sau -- ve- toi de mon cour -- roux van -- geur.

Al -- lons cher -- cher Lu -- cile, et pour flé -- chir son cœur,
ju -- rons à ses beaux yeux la plus fi -- dèle ar -- deur.

Que tout serve i -- ci ma co -- lè -- re,
pour pu -- nir un in -- grat qui m’a -- vait trop su plai -- re.
