\piecePartSpecs
#`((basse #:instrument "[B.C.]"
          #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{

\markup\fontsize#-2 \fill-line {
  \column {
    \livretPers Lucile
    \livretVerse#12 { Poursuis, ingrat, poursuis volage, amant sans foi, }
    \livretVerse#12 { Fais éclater tes feux auprès de cette belle : }
    \livretVerse#12 { Va, tu peux lui jurer une ardeur éternelle }
    \livretVerse#8 { Que ton cœur n’a promis qu’à moi. }
    \livretVerse#12 { Perfide, garde-toi de paraître à ma vue ; }
    \livretVerse#12 { C’en est fait, pour jamais mes liens sont rompus. }
    \livretPers Léandre
    \livretVerse#8 { Hélas ! je vous ai donc perdue, }
    \livretVerse#12 { Lucile, vous fuyez ! }
    \livretPers Zerbin
    \livretVerse#12 { \transparent { Lucile, vous fuyez ! } Vous ne la verrez plus. }
  }
  \column {
    \livretPers La principale Nymphe
    \livretVerse#12 { Ah ! puis-je soutenir un si sanglant outrage, }
    \livretVerse#10 { Sans immoler un traitre à ma fureur ? }
    \livretVerse#12 { Je sens que mon âme s’abandonne à la rage, }
    \livretVerse#12 { Perfide, sauve-toi de mon courroux vangeur. }
    \livretPersDidas Léandre à Zerbin
    \livretVerse#12 { Allons chercher Lucile, & pour fléchir son cœur, }
    \livretVerse#12 { Jurons à ses beaux yeux la plus fidèle ardeur. }
    \livretPers La principale Nymphe
    \livretVerse#8 { Que tout serve ici ma colère }
    \livretVerse#12 { Pour punir un ingrat qui m’avait trop su plaire. }
  }
}#}))
