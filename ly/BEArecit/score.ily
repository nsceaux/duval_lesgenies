\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "[B.C.]" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s1 s2.\break s1 s2. s1\break s2.*2 s1 s4 \bar "" \pageBreak
        s2 s2.*4\break s2.*3\break s1*3\pageBreak
        s1*3 s2.\break s2.*3 s1 s4 \bar "" \break s2 s2.*3\pageBreak
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
