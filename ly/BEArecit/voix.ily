\clef "vdessus" <>^\markup\character Lucile
r16 fad'' re'' si' |
sol''8. mi''16 dod''8. dod''16 fad''8. lad'16 si'8 dod'' |
fad'8 r16 fad'' re'' dod'' si' la' sold'8. si'16 |
mi''4 re''8 re''16 dod'' dod''8\trill dod''16 r fad''8. mi''16 |
re''8 re''16 dod'' si'8 si'16 la' sold'8 sold'16 si' |
mid'4\trill mid'16 r dod'' re'' si'8\trill si'16 la' sold'8. la'16 |
fad'2 r8 fad'' |
red''16\trill red'' mi'' fad'' sol''8 si'16 si' mi''8 mi''16 mi'' |
dod''4\trill dod'' r r8 mi''16 la'' |
dod''8 dod''16 la' re''8 re''16 sol'' mi''8 fad''16 sol'' |
<>^\markup\right-align\italic Elle sort fad''2\trill
\ffclef "vhaute-contre" <>^\markup\character Léandre
r8 fad' |
red'4\trill r16 red' red' red' mi'8. fad'16 |
sol'2 sol'8. si16 |
mi'4 re' r8 re'16 dod' |
dod'4\trill
\ffclef "vtaille" <>^\markup\character Zerbin
r8 mi' mi'16 mi' mi' fad' |
\appoggiatura mi'8 re'4
\ffclef "vdessus" <>^\markup\character La principale Nymphe
fa''4 re''16 re'' re'' fa'' |
sib'8. sib'16 sib'8. sib'16 do''8. re''16 |
sol'2 sol'4 r16 do'' re'' mi'' |
fa''4 fa'' re''8. re''16 mi''8. fa''16 |
mi''8.\trill do''16 sol'8 sol'16 la' sib'16 sib' sib' do'' re''8 re''16 mi'' |
fa''2 fa''4 r8 fa'' |
la'2 la'4 r8 la'16 fa' |
do''4 re'' mi''8. fa''16 fa''8. mi''16 |
fa''8
\ffclef "vhaute-contre" <>^\markup\character Léandre
do'8 la la sib do' |
re' re' re' re' mi' fa' |
\appoggiatura fa' sol'2 r8 do' |
fa'4. fa'8 sol' la' |
re' sol' la' sib' mi'4.\trill fa'8 |
fa'
\ffclef "vdessus" <>^\markup\character La principale Nymphe
la'16 fa' do''8 do'' fa'' fa''16 re'' |
sol''4 sol''8 sol'16 la' sib'8 sib'16 do'' |
re''8. re''16 re''8 re'' mi''8. fa''16 |
mi''4\trill mi'' r |
