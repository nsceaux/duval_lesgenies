\clef "basse" do8. do16 do8. do16 do4 |
\ru#57 { R2.\allowPageTurn } |
<>^"[Basses, B.C.]" \ru#16 do16 |
do8 mi16 do do8 mi16 do do8 mi16 do do8 mi16 do |
do do do do do do do do dod dod dod dod dod dod dod dod |
re8 fad16 re re8 fad16 re re8 fad16 re re8 fad16 re |
re re re re re re re re sol sol sol sol sol sol sol sol |
do' do' do' do' do' do' do' do' fa4~ fa32 fa, sol, la, sib, do re mi |
fa8 la16 fa fa8 la16 fa fa8 la16 fa fa8 la16 fa |
re,4~ re,32 re, mi, fa, sol, la, si, do re8. re16 re8. re16 |
sol8. sol16 sol8. sol16 mi4~ mi32 do re mi fa sol la si |
do'8 mi'16 do' do'8 mi'16 do' do'8 mi'16 do' do'8 mi'16 do' |
fad16 fad fad fad fad fad fad fad fad8. fad16 fad8. fad16 |
sol16 sol, sol, sol, sol, sol, sol, sol, do do do do do do do do |
do do si, la, sol, fa, mi, re, do, do, do, do, do, do, do, do, |
do8. do'16 do'8. do'16 do'8. do'16 do'8. do'16 |
do16 do re mi fa sol la si do' do' si la sol fa mi re |
do1 |
