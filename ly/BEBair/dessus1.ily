\clef "dessus" r8 sol'' do''' sol'' mi'' sol'' |
si' la' si' do'' re'' si' |
do'' re'' mi'' fa'' sol'' mi'' |
fa''4 fa'' r |
fa''8 sol'' fa'' mi'' re''4 |
r fad''4. fad''8 |
sol''4. sol''8 sol''4 |
fad''8 re'' sol'' si'' re'' sol'' |
sol''4 fad''4.\trill( mi''16 fad'') |
sol''8 re'' re'' mi'' fa'' mi'' |
re'' do'' si' do'' re'' mi'' |
fa''8. la''16 fa''8. la''16 fa''8. sol''16 |
mi''8. la''16 fa''8. la''16 fa''8. la''16 |
do''8. fa''16 do''8. fa''16 do''8. fa''16 |
mi''8. sol''16 mi''8. sol''16 mi''8. sol''16 |
do'''8. la''16 do'''8. la''16 do'''8. la''16 |
fad''8. la'16 fad'8. la'16 re''8. la'16 |
si'4 sol' r8 sol'' |
sol''4 do''' r8 do''' |
la''4 re'''4. re'''8 |
si''4 do''' si'' |
la''8. fad''16 la''8. fad''16 la''8. fad''16 |
si''8. si''16 sol''8. si''16 sol''8. re''16 |
mi''8. sol''16 mi''8. sol''16 mi''8. do''16 |
mi''8. do''16 re''8. do''16 si'8. la'16 |
sold'8. mi'16 sold'8. mi'16 si'8. mi'16 |
do''8. mi'16 mi''8. do''16 la'8. do''16 |
si'8. re''16 si'8. re''16 si'8. re''16 sold'8. si'16 sold'8. si'16 sold'8. si'16 |
la'8 la''16 sib'' la''8 sol'' fa'' mi'' |
fa''8. la''16 fa''8. la''16 fa''8. la''16 |
dod''8. mi''16 dod''8. mi''16 la'8. dod''16 |
re''8. fa''16 la''8. fa''16 la''8. fa''16 |
mi''8. dod''16 mi''8. dod''16 re''8. fa''16 |
mi''8. sol''16 mi''8. sol''16 mi''8. sol''16 |
dod''8 re'' dod''4.\trill re''8 |
re''8. re'16 fad'8. la'16 re''8. fad''16 |
la''8. fad''16 la''8. fad''16 la''8. fad''16 |
sol''8. sol'16 do''8. mi''16 sol''8. mi''16 |
fa''2 fa''8 fa'' |
mi''4 re''4.\trill do''8 |
si'8. sol'16 si'8. re''16 sol''8. re''16 |
mi''8. sol''16 mi''8. sol''16 mi''8. do''16 |
fa''8. mi''16 re''8. do''16 si'8. la'16 |
re''8. fa''16 re''8. fa''16 re''8. fa''16 |
fa''8. re''16 fa''8. re''16 fa''8. si'16 |
do''8. mi''16 mi''8. sol''16 mi''8. sol''16 |
la''8. fa''16 fa''8. la''16 la''8. fa''16 |
re''8. sol'16 si'8. re''16 fa''8. re''16 |
si'8. sol'16 si'8. re''16 sol'8. sol''16 |
sol''8. mi''16 sol''8. mi''16 do''8. mi''16 |
fa''8. la''16 si''8. re'''16 do'''4 |
si''8. sol'16 si'8. re''16 si'8. sol'16 |
do''8. sol'16 do''8. mi''16 sol''8. sib''16 |
la''8. fa''16 la''8. la''16 re'''8. do'''16 |
si''8 sol'' do''' sol'' do''' do''' |
do''' do''' si''4. do'''8 |
do'''8. do'''16 do'''8. do'''16 do'''8. do'''16 |
%
do'''8 do'16 do' mi'8 do'16 do' sol'8 do'16 do' mi'8 do'16 do' |
mi''8. sol''16 mi''8. sol''16 mi''8. sol''16 mi''8. sol''16 |
do''8. do'16 re' mi' fa' sol' la'8. la''16 la'' la'' la'' la'' |
fad''8. la''16 fad''8. la''16 fad''8. la''16 fad''8. la''16 |
re''8. re'16 mi' fad' sol' la' si'8. sol''16 sol''8. sol''16 |
sol''8. do''16 mi''8. do''16 la'8. do''16 la'8. do''16 |
fa'4~ fa'32 fa' sol' la' si' do'' re'' mi'' fa''4~ fa''32 fa'' fa'' fa'' fa'' fa'' fa'' fa'' |
fa''8. fa''16 re''8. fa''16 re''8. fa''16 re''8. fa''16 |
si'4~ si'32 sol' la' si' do'' re'' mi'' fa'' sol''4~ sol''32 sol'' sol'' sol'' sol'' sol'' sol'' sol'' |
sol''4~ sol''32 do''' do''' do''' do''' do''' do''' do''' do'''8. sol''16 mi''8. do''16 |
la''4~ la''32 re''' re''' re''' re''' re''' re''' re''' re'''8. la''16 fad''8. re''16 |
si''4~ si''32 sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol''8. sol''16 mi''8. sol''16 |
do''8. mi''16 sol'8. do''16 mi'8. sol'16 mi'8. sol'16 |
do'4~ do'32 do' re' mi' fa' sol' la' si' do''4~ do''32 do'' re'' mi'' fa'' sol'' la'' si'' |
do'''8. do'''16 do'''8. do'''16 do'''8. do'''16 do'''8. do'''16 |
do'''1 |
