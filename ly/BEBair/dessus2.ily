\clef "dessus" r8 do'' mi'' sol'' do'' mi'' |
re'' do'' re'' mi'' fa'' re'' |
mi'' fa'' sol'' la'' sib'' sol'' |
la''4 la'' r |
la''8 sib'' la'' sol'' fa''4 |
la''8 do''' la'' do''' la''4 |
re''4. re''8 do''4~ |
do''8 re'' si'4.\trill la'8 |
si'4( la'4.)\trill sol'8 |
sol' si' si' do'' re'' do'' |
si' la' sol' la' si' dod'' |
re''4 la'4. si'8 |
do''8. fa''16 do''8. fa''16 do''8. fa''16 |
la'4 la''4. la''8 |
sol''8. do'''16 sol''8. do'''16 sol''8. do'''16 |
r8 do''' la''8. do'''16 la''8. do'''16 |
la''8. fad''16 la''8. fad''16 la''8. fad''16 |
sol''4. si'8 do'' re'' |
mi'' re'' do'' re'' mi'' do'' |
fad'' mi'' re'' mi'' fad'' re'' |
sol'' fad'' mi'' fad'' sol'' mi'' |
fad''8. la''16 fad''8. la''16 fad''8. la''16 |
sol''4 si'8. re''16 si'8. re''16 |
do''8. mi''16 do''8. mi''16 sol''8. mi''16 |
la''8. mi''16 fa''8. mi''16 re''8. do''16 |
si'8 sold' si' sold' mi' sold' |
la'8. do''16 la'8. mi''16 do''8. la'16 |
re''8. fa''16 re''8. fa''16 re''8. fa''16 |
si'8. mi''16 si'8. mi''16 si'8. mi''16 |
dod''8 fa''16 sol'' fa''8 mi'' re'' dod'' |
re''8. fa''16 la''8. fa''16 re''8. fa''16 |
sol''8. mi''16 sol''8. mi''16 dod''8. la''16 |
la''8. la'16 re''8. la'16 fa''8. re''16 |
dod''8. mi''16 sol''8. mi''16 fa''8. la''16 |
sol''8. sib''16 sol''8. sib''16 sol''8. sib''16 |
mi''8 fa'' mi''4.\trill re''8 |
re''8. fad'16 la'8. re''16 fad''8. la''16 |
do'''8. la''16 do'''8. la''16 do'''8. la''16 |
si''8. sol''16 sol''8. sol''16 do'''8. sol''16 |
la''4. la''8 si''4 |
do'''8 sol'' fa''4.\trill mi''8 |
re''8. si'16 re''8. sol''16 si''8. sol''16 |
sol''8. mi''16 do''8. mi''16 do''8. mi''16 |
la''8. sol''16 fa''8. mi''16 re''8. do''16 |
fa''8. la''16 fa''8. la''16 fa''8. la''16 |
re''8. sol'16 re''8. sol'16 re''8. sol''16 |
mi''8. sol''16 sol''8. mi''16 do''8. mi''16 |
fa''8. re''16 re''8. fa''16 fa''8. re''16 |
si'8. si'16 re''8. si'16 sol'8. si'16 |
re''8. fa''16 re''8. fa''16 re''8. fa''16 |
mi''8. do''16 mi''8. sol''16 do'''8. do'''16 |
la''4 la''8 la'' sol''4 |
sol''8. sol''16 sol''8. sol''16 re''8. fa''16 |
mi''8. do''16 mi''8. do''16 mi''8. sol''16 |
fa''8. la''16 fa''8. la''16 fa''8. la''16 |
re''8. sol''16 sol''8. mi''16 sol''8. mi''16 |
mi''8. fa''16 re''8. mi''16 fa''8. sol''16 |
mi''8. mi''16 mi''8. mi''16 mi''8. mi''16 |
%%
mi''8 mi'16 mi' sol'8 mi'16 mi' do'8 mi'16 mi' sol'8 mi'16 mi' |
do'8. do''16 sol'8. do''16 sol'8. do''16 sol'8. do''16 |
mi'8. mi'16 mi'8. mi'16 mi'8. mi''16 mi''8. mi''16 |
la'8 re''16 re'' la'8 re''16 re'' la'8 re''16 re'' la'8 re''16 re'' |
fad'16 fad' fad' fad' fad'8. re'16 re''8. re''16 re''8. fa''16 |
mi''8. mi''16 sol''8. mi''16 fa''8. do''16 fa''8. do''16 |
la'8. la'16 la'8. do''16 la'8 do''16 do'' la'8 do''16 do'' |
la'8. la''16 fa''8. la''16 fa''8. la''16 fa''8. la''16 |
re'' sol'' sol'' sol'' sol'' fa'' mi'' re'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
mi''8 sol''16 mi'' mi''8 sol''16 mi'' mi''8 do'' sol' do''16 mi'' |
re''8. la'16 re''8. la'16 re''4~ re''32 re'' re'' re'' re'' re'' re'' re'' |
re''16 re'' si' re'' si' re'' si' re'' mi''8. mi''16 do''8. sol'16 |
mi'8. sol'16 mi'8. sol'16 do''8. mi''16 sol''8. do'''16 |
mi''8. do''16 mi''8. sol''16 mi''8. do''16 mi''8. sol''16 |
mi''8. mi''16 mi''8. mi''16 mi''8. mi''16 mi''8. mi''16 |
mi''1 |
