\clef "dessus" r8 do'' do'' do'' do'' do'' |
sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
do''8. do''16 do''8. do''16 do''8. do''16 |
fa''8. fa''16 fa''8. fa''16 fa''8. fa''16 |
re''4 re'' re''8. mi''16 |
fa''8. fa''16 la'8. la'16 la'8. re''16 |
si'8. si'16 si'8. si'16 do''8. do''16 |
re''8. re''16 sol'8. sol'16 sol'8. do'16 |
re'8. re'16 re'8. re'16 re'8. re'16 |
sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
re'8. re'16 re'8. re'16 re'8. re'16 |
la'8. la'16 la'8. la'16 la'8. fa'16 |
fa'8. fa'16 fa'8. sol'16 la'8. si'16 |
do''8. do''16 do''8. do''16 do''8. do''16 |
la'8. la'16 la'8. la'16 la'8. la'16 |
re''8. re'16 re'8. re'16 re'8. re'16 |
sol'8 la' si' sol' la' si' |
do'' si' la' si' do'' la' |
re'' do'' si' do'' re'' si' |
mi'' re'' do'' re'' mi'' do'' |
re''8. re''16 re''8. re''16 re''8. re''16 |
sol''8. sol'16 sol'8. sol'16 sol'8. sol'16 |
do''8. do''16 do''8. do''16 do''8. mi''16 |
do''4 sold'4. la'8 |
mi'8. mi'16 mi'8. mi'16 mi'8. mi'16 |
la'8. la'16 la'8. la'16 la'8. la'16 |
fa'8. fa'16 fa'8. fa'16 fa'8. re'16 |
mi'8. mi'16 mi'8. mi'16 mi'8. mi'16 |
la'8. la'16 la'8. la'16 la'8. la'16 |
re''8. re''16 re''8. re''16 re''8. la'16 |
mi''4. re''8 mi''4 |
fa''8. fa''16 fa''8. fa''16 re''8. re''16 |
la'8. la'16 la'8. la'16 re''8. re''16 |
sib'8. sib'16 sib'8. sib'16 sib'8. sol'16 |
la'8. la'16 la'8. la'16 la4 |
re'8. re'16 re'8. re'16 re'8. re'16 |
re'8. re'16 re'8. re'16 re'8. re''16 |
sol''8. sol''16 mi''8. mi''16 do''8. do''16 |
fa''8. fa''16 fa''8. mi''16 re''8. re''16 |
do''8. do''16 si'8. si'16 si'8. do''16 |
sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
do''8. do''16 do''8. do''16 do''8. do''16 |
fa'8. fa'16 la'8. fa'16 la'8. fa'16 |
re'8. re'16 re'8. re'16 re''8. re''16 |
si'8. si'16 si'8. si'16 si'8. sol'16 |
do''8. do''16 do''8. do''16 do''8. do''16 |
fa''8. fa''16 fa''8. re''16 mi''8. fa''16 |
sol''8. sol'16 sol'8. sol'16 sol'8. sol'16 |
sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
do''8. do''16 do''8. do''16 do''8. la'16 |
re''4 re''8. si'16 do''4 |
sol'8. sol'16 sol'8. sol'16 sol'4 |
r4 mi''8. mi''16 do''8. do''16 |
fa''8. fa''16 fa''8. fa''16 re''8. re''16 |
sol''8. sol''16 mi''8. mi''16 do''8. do''16 |
sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
do''8. do''16 do''8. do''16 do''8. do''16 |
%%
do''8 do'16 do' mi'8 do'16 do' sol'8 do'16 do' mi'8 do'16 do' |
mi''8. sol''16 mi''8. sol''16 mi''8. sol''16 mi''8. sol''16 |
do''8. do'16 re' mi' fa' sol' la'8. la''16 la'' la'' la'' la'' |
fad''8. la''16 fad''8. la''16 fad''8. la''16 fad''8. la''16 |
re''8. re'16 mi' fad' sol' la' si'8. sol''16 sol''8. sol''16 |
sol''8. do''16 mi''8. do''16 la'8. do''16 la'8. do''16 |
fa'4~ fa'32 fa' sol' la' si' do'' re'' mi'' fa''4~ fa''32 fa'' fa'' fa'' fa'' fa'' fa'' fa'' |
fa''8. fa''16 re''8. fa''16 re''8. fa''16 re''8. fa''16 |
si'4~ si'32 sol' la' si' do'' re'' mi'' fa'' sol''4~ sol''32 sol'' sol'' sol'' sol'' sol'' sol'' sol'' |
sol''4~ sol''32 do''' do''' do''' do''' do''' do''' do''' do'''8. sol''16 mi''8. do''16 |
la''4~ la''32 re''' re''' re''' re''' re''' re''' re''' re'''8. la''16 fad''8. re''16 |
si''4~ si''32 sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol''8. sol''16 mi''8. sol''16 |
do''8. mi''16 sol'8. do''16 mi'8. sol'16 mi'8. sol'16 |
do'4~ do'32 do' re' mi' fa' sol' la' si' do''4~ do''32 do'' re'' mi'' fa'' sol'' la'' si'' |
do'''8. do'''16 do'''8. do'''16 do'''8. do'''16 do'''8. do'''16 |
do'''1 |
