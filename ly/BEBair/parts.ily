\piecePartSpecs
#`((basse #:score-template "score-voix"
          #:indent 0)
   (dessus #:score "score-dessus")
   (dessus1 #:score "score-dessus1")
   (dessus2 #:score "score-dessus2")
   (flute-hautbois #:score "score-vents")

   (silence #:on-the-fly-markup , #{
\markup\fontsize#-2 \column {
  \livretPers La principale Nymphe
  \livretVerse#12 { Venez tyrans des airs, aquilons furieux, }
  \livretVerse#12 { Excitez sur ce bord le plus affreux orage ; }
  \livretVerse#12 { Que les flots irrités s’élèvent jusqu’aux cieux, }
  \livretVerse#8 { Vangez-moi, lavez mon outrage, }
  \livretVerse#8 { Innondez pour jamais ces lieux. }
}#}))
