\score {
  \new GrandStaff \with { instrumentName = "Violons" } <<
    \new Staff << \global \includeNotes "dessus1" >>
    \new Staff << \global \includeNotes "dessus2" >>
    \new Staff << \global \includeNotes "dessus3" >>
  >>
  \layout { indent = \largeindent }
}
