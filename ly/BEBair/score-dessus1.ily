\score {
  \new GrandStaff <<
    \new Staff \with { instrumentName = "Violons I" } <<
      \global \includeNotes "dessus1"
      { s2.*58\break }
    >>
    \new Staff \with { instrumentName = "Violons III" \haraKiri } <<
      \global \includeNotes "dessus3"
    >>
  >>
  \layout { indent = \largeindent }
}
