\score {
  \new GrandStaff <<
    \new Staff \with { instrumentName = "Violons II" } <<
      \global \includeNotes "dessus2"
      { s2.*58\break }
    >>
    \new Staff \with { instrumentName = "Violons III" \haraKiri } <<
      \global \includeNotes "dessus3"
    >>
  >>
  \layout { indent = \largeindent }
}
