\score {
  \new GrandStaff \with { instrumentName = "Violons" } <<
    \new Staff << \global \includeNotes "dessus1" >>
    \new Staff <<
      \global \includeNotes "dessus2"
      { s2.*58\allowPageTurn }
    >>
  >>
  \layout { indent = \largeindent }
}
