\score {
  \new ChoirStaff <<
    \new GrandStaff \with { instrumentName = "Violons" } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff \with { \haraKiri } << \global \includeNotes "dessus3" >>
    >>
    \new Staff \with {
      instrumentName = \markup\center-column\smallCaps { La principale Nymphe }
      \haraKiri
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "[B.C.]" \haraKiri } <<
      \global \includeNotes "basse"
      \origLayout {
        s2.*6\break s2.*6\pageBreak
        s2.*4\break s2.*5\pageBreak
        s2.*5\break s2.*6\pageBreak
        \grace s8 s2.*8\break s2.*8\pageBreak
        s2.*8\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
