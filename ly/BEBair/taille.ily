\clef "dessus" r8 do'' do'' do'' do'' do'' |
sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
do''8. do''16 do''8. do''16 do''8. do''16 |
fa''8. fa''16 fa''8. fa''16 fa''8. fa''16 |
re''4 re'' re''8. mi''16 |
fa''8. fa''16 la'8. la'16 la'8. re''16 |
si'8. si'16 si'8. si'16 do''8. do''16 |
re''8. re''16 sol'8. sol'16 sol'8. do'16 |
re'8. re'16 re'8. re'16 re'8. re'16 |
sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
re'8. re'16 re'8. re'16 re'8. re'16 |
la'8. la'16 la'8. la'16 la'8. fa'16 |
fa'8. fa'16 fa'8. sol'16 la'8. si'16 |
do''8. do''16 do''8. do''16 do''8. do''16 |
la'8. la'16 la'8. la'16 la'8. la'16 |
re''8. re'16 re'8. re'16 re'8. re'16 |
sol'8 la' si' sol' la' si' |
do'' si' la' si' do'' la' |
re'' do'' si' do'' re'' si' |
mi'' re'' do'' re'' mi'' do'' |
re''8. re''16 re''8. re''16 re''8. re''16 |
sol''8. sol'16 sol'8. sol'16 sol'8. sol'16 |
do''8. do''16 do''8. do''16 do''8. mi''16 |
do''4 sold'4. la'8 |
mi'8. mi'16 mi'8. mi'16 mi'8. mi'16 |
la'8. la'16 la'8. la'16 la'8. la'16 |
fa'8. fa'16 fa'8. fa'16 fa'8. re'16 |
mi'8. mi'16 mi'8. mi'16 mi'8. mi'16 |
la'8. la'16 la'8. la'16 la'8. la'16 |
re''8. re''16 re''8. re''16 re''8. la'16 |
mi''4. re''8 mi''4 |
fa''8. fa''16 fa''8. fa''16 re''8. re''16 |
la'8. la'16 la'8. la'16 re''8. re''16 |
sib'8. sib'16 sib'8. sib'16 sib'8. sol'16 |
la'8. la'16 la'8. la'16 la4 |
re'8. re'16 re'8. re'16 re'8. re'16 |
re'8. re'16 re'8. re'16 re'8. re''16 |
sol''8. sol''16 mi''8. mi''16 do''8. do''16 |
fa''8. fa''16 fa''8. mi''16 re''8. re''16 |
do''8. do''16 si'8. si'16 si'8. do''16 |
sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
do''8. do''16 do''8. do''16 do''8. do''16 |
fa'8. fa'16 la'8. fa'16 la'8. fa'16 |
re'8. re'16 re'8. re'16 re''8. re''16 |
si'8. si'16 si'8. si'16 si'8. sol'16 |
do''8. do''16 do''8. do''16 do''8. do''16 |
fa''8. fa''16 fa''8. re''16 mi''8. fa''16 |
sol''8. sol'16 sol'8. sol'16 sol'8. sol'16 |
sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
do''8. do''16 do''8. do''16 do''8. la'16 |
re''4 re''8. si'16 do''4 |
sol'8. sol'16 sol'8. sol'16 sol'4 |
r4 mi''8. mi''16 do''8. do''16 |
fa''8. fa''16 fa''8. fa''16 re''8. re''16 |
sol''8. sol''16 mi''8. mi''16 do''8. do''16 |
sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
do''8. do''16 do''8. do''16 do''8. do''16 |
%%
do''8 mi'16 mi' sol'8 mi'16 mi' do'8 mi'16 mi' sol'8 mi'16 mi' |
do'8. do''16 sol'8. do''16 sol'8. do''16 sol'8. do''16 |
mi'8. mi'16 mi'8. mi'16 mi'8. mi''16 mi''8. mi''16 |
la'8 re''16 re'' la'8 re''16 re'' la'8 re''16 re'' la'8 re''16 re'' |
fad'16 fad' fad' fad' fad'8. re'16 re''8. re''16 re''8. fa''16 |
mi''8. mi''16 sol''8. mi''16 fa''8. do''16 fa''8. do''16 |
la'8. la'16 la'8. do''16 la'8 do''16 do'' la'8 do''16 do'' |
la'8. la''16 fa''8. la''16 fa''8. la''16 fa''8. la''16 |
re'' sol'' sol'' sol'' sol'' fa'' mi'' re'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
mi''8 sol''16 mi'' mi''8 sol''16 mi'' mi''8 do'' sol' do''16 mi'' |
re''8. la'16 re''8. la'16 re''4~ re''32 re'' re'' re'' re'' re'' re'' re'' |
re''16 re'' si' re'' si' re'' si' re'' mi''8. mi''16 do''8. sol'16 |
mi'8. sol'16 mi'8. sol'16 do''8. mi''16 sol''8. do'''16 |
mi''8. do''16 mi''8. sol''16 mi''8. do''16 mi''8. sol''16 |
mi''8. mi''16 mi''8. mi''16 mi''8. mi''16 mi''8. mi''16 |
mi''1 |
