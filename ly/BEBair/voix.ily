\clef "vdessus" r4 r r8 do'' |
sol''4. sol'8 sol' sol' |
do''2 do''8 do'' |
fa''2 fa''8 fa'' |
re''2\trill re''8 mi'' |
fa''4 la'4. re''8 |
si'4.\trill si'8 do''4 |
re'' sol'4. do'8 |
re'2. |
sol'2 r4 |
r r sol'8 sol' |
re'4 re'4. re'8 |
la'2 r8 fa' |
fa'4. sol'8 la' si' |
do''2 do''8 do'' |
la'4 la'4. la'8 |
re''2 r8 re' |
sol'[\melisma la' si' sol' la' si'] |
do''[ si' la' si' do'' la'] |
re''[ do'' si' do'' re'' si'] |
mi''[ re'' do'' re'' mi'' do'']( |
re''4.)\melismaEnd re''8 re'' re'' |
sol''2 sol'8 sol' |
do''2 r8 mi'' |
do''4 sold'4.\trill la'8 |
mi'2 mi'4 |
r4 la' la' |
fa' fa'4. re'8 |
mi'2 mi'4 |
la'2 la'8 la' |
re''2 r8 la' |
mi''4.\trill re''8 mi''4 |
\appoggiatura mi''8 fa''2 \appoggiatura mi''8 re''4 |
r la' re'' |
sib' sib'4. sol'8 |
la'2 la'4 |
re'2. |
r4 r re''8 re'' |
sol''4 mi'' do'' |
fa''4. mi''8 re''4 |
do'' si'4.\trill do''8 |
sol'2 sol'4 |
r r do''8 do'' |
fa'4 la'4. fa'8 |
\appoggiatura mi'8 re'2 re''8 re'' |
si'4\trill si'4. sol'8 |
do''2 r8 do'' |
fa''4. re''8 mi'' fa'' |
sol''2.~ |
sol''2 sol'8 sol' |
do''2 r8 la' |
fa''4 fa'' mi'' |
re''2\trill re''4 |
r mi'' do'' |
fa'' fa'' re'' |
sol'' mi'' do'' |
sol'2 r8 sol' |
do''2. |
R1*16 |
