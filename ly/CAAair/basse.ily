\clef "basse" mi2.~ |
mi4 red si, |
mi sol mi |
si2 r8 sol |
do' si la4 sol |
re'8 re fad4 re |
sol mi8 fad sol mi |
la4 fad8 sol la fad |
si4 red' si |
mi'8 re' do'2 |
si r4 |
r mi2~ |
mi si,4 |
sol, fad, mi, |
si,2 r4 |
r sol8 la si sol |
do' si la si do' la |
re'4 do'8 si la sol |
fad2 sol4 |
re8 la re' mi' re' do' |
si la sol fa mi re |
do si, la, si, do la, |
re do re mi re re, |
sol,4. re8 mi fad |
sol fad mi4. re8 |
la sol fad sol la fad |
si4 la8 sol fad mi |
red dod si, dod red si, |
mi2 re4 |
do8 re do si, do la, |
sold,4 la,8 si, do la, |
si,8 fad si do' si la |
sold fad mi fad sold mi |
la sol fad sol la fad |
si4. sol8 fad mi |
si4 si,2 |
do4 la, si, |
<< { s4 <>^"[B.C.]" } mi2.~ >> | \allowPageTurn
mi2 si,4 do |
re fad, sol, sol |
fad2. re4 |
sol sol, sol |
sold8 fad mi fad sold mi |
la2. |
lad8 si lad sold lad fad |
si2 la4 |
sold mi la,2 |
si,4 si la |
sol8 la si la sol fad |
\ifComplet {
  <>^"[Tous]" mi2 mi, |
  si, red4 si, |
  mi2 sol4 mi |
  si la sol fad8 mi red4 si, |
  mi2 sol8 mi fad16. fad32 mi re dod si, |
  lad,2 fad,4 |
  si,2.~ |
  si,2 r4 | \allowPageTurn
  r4 sol fad2. re4 |
  sol4 sol,8 la, si, do |
  re2~ re16 re' do' si |
  la4 sol8 fad16 mi red8 si, mi16 re? do si, |
  la,4. la8 lad2 |
  si1 la2 |
  sol2 red si, |
  mi2 mi8 re do4 |
  si,~ si,16 do si, la, sol,4 sol |
  fad1 re2 |
  sol2 mi8 fad |
  sold la sold fad sold mi |
  la si lad sold lad fad |
  si la si do' si la |
  sol4 mi si,8 la, sol, fad, |
  mi,4~ mi,16 mi fad sol la sol la si |
  do'4~ do'16 fad sol la si4~ si16 mi fad sol |
  la4 si si, |
}
mi2 mi8 fad |
sol4 la8 si do' la |
si la sol fad mi re |
do4 do2\trill |
si,4 si8 la sol fad |
sol4 la8 si do' la |
si la sol fad mi re |
do4 do2\trill |
si,2. |
R2. | \allowPageTurn
sol4 sol4. la8 |
si4. la8 si do' |
re' re16 mi fad sol fad sol la si la si |
do'4 dod'2 |
re'4 do' si |
do'8 la sold fad sold mi |
la si lad sold lad fad |
si la sol4 red |
mi sol mi |
red mi2 |
si,4 si la |
sol16 fad sol la sol la sol fad mi re do si, |
la,4~ la,8 si, do la, |
si,2. |
mi2 fad4 |
sol2. fad4 |
mi2. re8 do |
si,4.\trill la,8 sol,4 |
re4. la,16 si, do4 dod |
re4. re,8 sol,4 |
sold,4. mi,8 la, fad, |
si, fad si do' si la |
sol4. fad8 mi la, si,4 |
mi2. fad4 |
\once\set Staff.whichBar = "|"
