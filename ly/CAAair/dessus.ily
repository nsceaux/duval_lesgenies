\clef "dessus" r4 mi'' si' |
sol'8 la' si' la' sol' fad' |
sol'4 \appoggiatura fad'8 mi'4 r8 <<
  \tag #'dessus1 {
    sol''8 |
    \appoggiatura sol''8 fad''4 si'8 re'' do'' si' |
    mi'' re'' do'' re'' si' do'' |
    la'4 re''8 do'' si' la' |
    si'4 sol''8 fad'' mi'' re'' |
    dod''4 la''8 sol'' fad'' mi'' |
    red''4 si''8 la'' sol'' fad'' |
    sol'' la'' la''4.\trill( sol''16 la'') |
    si''8 fad'' red''4 mi''8( fad''16 sol'') |
    fad''4
  }
  \tag #'(dessus2 dessus2-part) {
    mi''8 |
    red''4 fad''4. si''8 |
    sol'' mi'' fad''4 sol'' |
    fad''8 sol'' la''4. la''8 |
    re''8 sol''16 la'' si''8 la'' sol'' fad'' |
    mi''8 la''16 si'' do'''8 si'' la'' sol'' |
    fad''4. red''8 mi'' fad'' |
    si' dod''16 red'' mi''4. fad''8 |
    red''4 si'8 la' sol' fad'16 mi' |
    red'4 \tag #'dessus2 \startHaraKiri
  }
>> r4 r |
R2.*2 |
r4 si'' fad'' |
si'8. do''16 re''4. re''8 |
sol'8 la'16 si' do''8 si' la' sol' |
fad'4.\trill mi'8 fad' sol' |
la'4. re''8 do'' si' |
la' sol' fad' sol' la' fad' |
sol' la' si' sol' la' si' |
do'' re'' do'' si' la' sol' |
fad'4. fad'8 sol' la' |
re' re'' sol'' fad'' sol'' la'' |
si'' la'' sol'' la'' sol'' fad'' |
mi''\trill mi'' la'' si'' la'' sol'' |
fad''4\trill si''4. si''8 |
si'' si'' la'' sol'' fad'' mi''16 red'' |
mi''8 mi'' fad'' sold'' sold''8.(\trill fad''32 sold'') |
la''4. sold''8 la''8\trill sold''16 la'' |
si''4 la''8 sol'' fad'' mi'' |
red''4\trill red''8 mi'' fad'' red'' |
mi''4 si' r8 si'' |
mi''4 la''8 si'' la'' sol'' |
fad''4.\trill sol''8 red'' mi'' |
mi''4( red''4.)\trill mi''8 |
mi'' fad'' fad''4. mi''8 |
mi''4 r r | \allowPageTurn
R1*3 R2.*5 R1 R2.*2 | \allowPageTurn
\ifComplet {
  r2 <>^"Violons" mi''4\fort sol'' |
  red''4. mi''16 fad'' si'4.~ si'32 la' sol' fad' |
  sol'16 la' sol' la' si' la' sol' fad' mi'2\doux |
  fad' sol'8[ mi'] red'8.\trill mi'16 fad'2 |
  si8 mi' si'8. mi''16 dod''4\trill lad'8. si'16 |
  dod''4. dod''8 si' lad' |
  si'8._\markup\italic "un peu fort" fad''16 la''8. sol''16 fad''8. si''16 |
  red''8. fad''16 si'4 r |
  r si'\doux la'2. la'4 |
  re'8. re''16 re''4~ re''16. re''32( do'' si' la' sol') |
  fad'2\trill~ fad'8 fad'16 sold' |
  la'8 do'' si'8. si'16 si'4. si'8 |
  do'' la' fad'4 fad'4. fad'8 |
  fad'2.~ fad'16\fort fad' sol' la' si'4.~ si'32 si' dod'' red'' |
  mi''4 si'\doux fad''1 |
  si'2. mi''8 fad'' |
  red''2.\trill si'4 |
  la'4. re''8 do'' si' la' sol' fad'4.(\trill mi'16 fad') |
  sol'8 re'' mi'' fad'' sol'' la'' |
  si''2 r8 si'' |
  mi''4 dod''4. dod''8 |
  fad'4 fad''4. fad''8 |
  si'2~ si'8 si' dod'' red'' |
  mi''8. fad''16 sol'' sol'' la'' si'' do'''8. do'''16 |
  fad''4. fad''8 red''4\trill r8 sol'' |
  sol'' fad'' fad''4.\trill mi''8 |
  mi''2 r4 |
} \allowPageTurn
\ifConcert R2.
R2.*24 R1*2 R2. R1 R2.*3 R1*2 |
