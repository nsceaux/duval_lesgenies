Douce er -- reur, char -- man -- te chi -- mè -- re,
pour -- quoi faut- il que la clar -- té du jour,
chas -- se l’es -- poir dont me flat -- tait l’a -- mour,
et que tu ne sois plus qu’un bien i -- ma -- gi -- nai -- re ?
Pour -- quoi faut- il que la clar -- té __ du jour,
chas -- se l’es -- poir dont me flat -- tait l’a -- mour,
et que tu ne sois plus qu’un bien i -- ma -- gi -- nai -- re ?

Za -- ï -- re, ar -- rê -- tez- vous ? qui vous guide en ces lieux ?
Vos sens sont a -- gi -- tés, mil -- le dou -- ces a -- lar -- mes
d’un é -- clat plus bril -- lant em -- bel -- lis -- sent vos yeux ;
l’a -- mour veut- il en -- fin ré -- com -- pen -- ser vos char -- mes ?

\ifComplet {
  Quel spec -- tacle à mes yeux s’est of -- fert cet -- te nuit ?
  Ja -- mais rien de si beau n’a -- vait frap -- pé mon â -- me !
  Mal -- gré l’é -- clat du jour cette i -- ma -- ge me suit.
  A -- dol -- phe !… j’ai cru voir cet ob -- jet de ma flam -- me
  sur un trône, en -- tou -- ré d’u -- ne pom -- peu -- se cour :
  tout trem -- blait de -- vant lui dans un humble es -- cla -- va -- ge,
  je me trou -- vais moi- même en ce char -- mant sé -- jour,
  et lors -- que tous les cœurs ve -- naient lui rendre hom -- ma -- ge,
  je jou -- is -- sais de l’a -- van -- ta -- ge
  de le voir à mes pieds, les of -- frir à l’a -- mour.
}

Le som -- meil par de doux men -- son -- ges
quel -- que fois don -- ne de beaux jours.
Le som -- meil par de doux men -- son -- ges
quel -- que fois don -- ne de beaux jours ;
mais le ré -- veil les rend si courts,
qu’ils s’en -- vo -- lent a -- vec les son -- ges.
Mais le ré -- veil les rend si courts,
qu’ils s’en -- vo -- lent a -- vec les son -- ges.

Lais -- sez- moi m’oc -- cu -- per des plai -- sirs que je sens,
j’aime à rê -- ver en -- cor dans ce lieu so -- li -- tai -- re ;
l’a -- mour sait ce qui reste à fai -- re,
pour mieux mé -- ri -- ter mon en -- cens.
