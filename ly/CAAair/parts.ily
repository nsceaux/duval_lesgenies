\piecePartSpecs
#`((dessus #:score-template "score-2dessus-voix"
           #:instrument
           ,#{ \markup\center-column { [Violons et Hautbois] } #})
   (dessus1 #:score-template "score-voix"
            #:tag-notes dessus1
            #:instrument ,#{ \markup\center-column { [Violons et Hautbois] } #})
   (dessus2 #:score-template "score-voix"
            #:tag-notes dessus2-part
            #:instrument ,#{ \markup\center-column { [Violons et Hautbois] } #})
   
   (basse #:score-template "score-basse-continue-voix"
          #:instrument
          ,#{ \markup\center-column { [Basses, Bassons et B.C.] } #})
   
   (silence #:on-the-fly-markup , #{
\markup\fontsize#-2 \fill-line {
  \column {
    \livretPers Zaïre
    \livretVerse#8 { Douce erreur, charmante chimère, }
    \livretVerse#10 { Pourquoi faut-il que la clarté du jour, }
    \livretVerse#10 { Chasse l’espoir dont me flattait l’amour ? }
    \livretVerse#12 { Et que tu ne sois plus qu’un bien imaginaire ? }
    \livretPers Zamire
    \livretVerse#12 { Zaïre, arrêtez-vous ? qui vous guide en ces lieux ? }
    \livretVerse#12 { Vos sens sont agités, mille douces alarmes }
    \livretVerse#12 { D’un éclat plus brillant embellissent vos yeux ; }
    \livretVerse#12 { L’amour veut-il enfin récompenser vos charmes ? }
    \livretPers Zaïre
    \livretVerse#12 { Quel spectacle à mes yeux s’est offert cette nuit ? }
    \livretVerse#12 { Jamais rien de si beau n’avait frappé mon âme ! }
    \livretVerse#12 { Malgré l’éclat du jour cette image me suit. }
    \livretVerse#12 { Adolphe !… j’ai cru voir cet objet de ma flamme }
    \livretVerse#12 { Sur un trône, entouré d’une pompeuse cour : }
  }
  \column {
    \livretVerse#12 { Tout tremblait devant lui dans un humble esclavage, }
    \livretVerse#12 { Je me trouvais moi-même en ce charmant séjour, }
    \livretVerse#12 { Et lorsque tous les cœurs venaient lui rendre hommage, }
    \livretVerse#8 { Je jouissais de l’avantage }
    \livretVerse#12 { De le voir à mes pieds, les offrir à l’amour. }
    \livretPers Zamire
    \livretVerse#8 { Le sommeil par de doux mensonges }
    \livretVerse#8 { Quelque fois donne de beaux jours ; }
    \livretVerse#8 { Mais le réveil les rend si courts, }
    \livretVerse#8 { Qu’ils s’envolent avec les songes. }
    \livretPers Zaïre
    \livretVerse#12 { Laissez-moi m’occuper des plaisirs que je sens, }
    \livretVerse#12 { J’aime à rêver encor dans ce lieu solitaire ; }
    \livretVerse#8 { L’amour sait ce qui reste à faire, }
    \livretVerse#8 { Pour mieux mériter mon encens. }
  }
  
}#}))
