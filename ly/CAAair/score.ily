\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new GrandStaff \with {
      instrumentName = \markup\center-column { [Violons, Hautbois] }
    } <<
      \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
      \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses, Bassons et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*8\pageBreak
        s2.*6\break \grace s8 s2.*5\pageBreak
        s2.*6\break s2.*6\pageBreak
        s2.*5\break s2.*2 s1 s2. \bar "" \pageBreak
        s4 s1 s2.*2\break s2.*3 s2 \bar "" \break s2 s2.*2\pageBreak
        s1*3 s1.\break \grace s8 s1 s2.*3 s2 \bar "" \pageBreak
        s1 s2.*2 s1 \break s1 s1.*2\pageBreak
        s1*2 s1. s2.\break s2.*3 s1\pageBreak
        \grace s8 s2. s1 s2.*2 s2 \bar "" \break s4 s2.*4 s2 \bar "" \pageBreak
        s4 s2.*5\break s2.*5\break s2.*5\pageBreak
        s2.*3 s1*2 s2.\break s1 s2.*2\break s2. s1*2
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
