\ffclef "vdessus" R2.*11 |
r4 <>^\markup\character Zaïre mi'' si' |
\appoggiatura la'8 sol'2 si'4 |
mi'' fad''4. sol''8 |
\appoggiatura sol''8 fad''4 si'8 r16 si' si'8. re''16 |
sol'8. la'16 si'8 do'' re''8. si'16 |
mi''4 do'' do''8 mi'' |
la'2. |
re''4 do''8 si' la' si' |
fad'2.\trill |
re''4 re''8 si' do'' re'' |
\appoggiatura re'' mi''2 do''4 |
la'4.\trill la'8 si' do'' |
si'4\trill \appoggiatura la'8 sol'4 r |
r8 si' dod''4. re''8 |
dod''4\trill fad'' fad''8 sol'' |
red''2\trill~ red''8 mi'' |
\appoggiatura mi''8 fad''2. |
si'4 do'' si' |
mi''2. |
re''4 do''8 si' la' si' |
fad'2.\trill |
si'4 si'8 si' dod'' re'' |
dod''2\trill fad''4 |
red''4.\trill mi''8 fad'' sol'' |
sol''4(\melisma fad''4.)\trill mi''8\melismaEnd |
mi''4 r r |
\ffclef "vdessus" <>^\markup\character Zamide
r8 si' mi''4 mi''16 sol' la' si' |
mi'4 r8 si'16 do'' re''4 do''8 si' |
la'4\trill r8 re'' si'4\trill si'8 la'16 sol' |
re''4 la'8 si' \appoggiatura si'8 do''4 la'8 re'' |
si'4\trill \appoggiatura la'8 sol'4 r8 si'16 dod'' |
re''4 si'4. mi''8 |
dod''2\trill re''8 mi'' |
fad''2 sol''8 \appoggiatura fad'' mi'' |
red''2\trill r8 si' |
mi''4~ mi''16 re'' do'' si' do''8. do''16 do''16 si' la' sol' |
fad'2\trill fad'4 |
R2. |
\ifComplet {
  \ffclef "vdessus" <>^\markup\character Zaïre
  R1*2 |
  r2 mi''4 sol'' |
  red''4 mi''8 fad'' si'4 fad'8 sol' la'4 sol'8 fad' |
  \appoggiatura fad'8 sol'4 r8 si' mi''4 dod''8 re''16 mi'' |
  \appoggiatura mi''8 fad''8. fad''16 dod''8 dod'' red'' mi'' |
  red''4\trill red'' r |
  r r sol' |
  si' si'8 la'16 sol' re''4 la'8 si' \appoggiatura si' do''4 do''8 si' |
  si'4\trill r r8 si' |
  la'4\trill la' r8 la'16 si' |
  do''8 do''16 re'' mi''8 fad''16 sol'' fad''8\trill fad'' r sol''16 mi'' |
  la''8 fad''16 fad'' dod''4 dod''8 dod''16 dod'' red''8. mi''16 |
  red''2\trill r r |
  mi''2 si' red'4 mi'8 fad' |
  sol'4 r8 mi'16 fad' sol'4 sol'8 la' |
  \appoggiatura la'8 si'2 si'8 r16 sol' sol' la' si' do'' |
  re''2 la'4 la'8 la' si'4. do''8 |
  si'2\trill r8 si' |
  mi''4. re''8 mi'' si' |
  dod''8. dod''16 fad''8 mi'' fad'' dod'' |
  red''4\trill \appoggiatura dod''8 si'4 r16 si' dod'' red'' |
  mi''2 red''4 mi''8 fad'' |
  \appoggiatura fad''8 sol''4 \appoggiatura fad''8 mi''4 r8 do''16 mi'' |
  la'4 si'8 do'' fad'4 r8 si'16 mi'' |
  dod''8[ red''16 mi''] red''4.\trill mi''8 |
  mi''2
  \ffclef "vdessus" <>^\markup\character Zamide
}
\ifConcert { r4 r }
mi''8 red'' |
mi''4 fad''8 sol'' fad'' mi'' |
red''4\trill \appoggiatura dod''8 si'4 do''8 si' |
la'4\trill la'8 sol' fad'\trill mi' |
si'2 mi''8 red'' |
mi''4 fad''8 sol'' fad'' mi'' |
red''4\trill \appoggiatura dod''8 si'4 do''8 si' |
la'4\trill la'8 sol' fad' mi' |
si'2. |
sol'4 sol'4. la'8 |
si'4.\trill la'8 si' do'' |
re''4 do'' si' |
la'16[\melisma sol' fad' sol'] la'[ si' la' si'] do''[ re'' do'' re'']( |
mi''4)\melismaEnd mi''8 mi'' fad'' sol'' |
fad''2\trill \appoggiatura mi''8 re''4 |
mi''2 si'8 re'' |
dod''4. fad''8 dod'' fad'' |
red''4\trill mi'' si' |
sol'16[\melisma la' sol' fad'] mi'[ fad' mi' fad'] sol'[ la' sol' la'] |
si'[ do'' si' la'] sol'[ la' sol' la'] si'[ do'' si' dod''] |
red''[ mi'' red'' dod''] si'[ dod'' si' dod''] red''[ dod'' red'' si'] |
mi''2.~ |
mi''4\melismaEnd mi''8 mi'' fad'' sol'' |
sol''4( fad''2)\trill |
mi'' r4 |
\ffclef "vdessus" <>^\markup\character Zaïre
r4 r8 si'16 do'' la'4\trill la'8 re'' |
sol'4 sol'8 la' si'4\trill si'8 do'' |
\appoggiatura do''8 re''2 r4 |
la'4 la'16 si' do'' re'' mi''8 do''16 re'' mi''8 fad''16 sol'' |
fad''4\trill fad'' r8 sol'' |
\appoggiatura fad''8 mi''4 re'' do''16 si' la' sol' |
fad'4\trill fad' r8 si' |
si'4 dod''8 red'' mi''4. mi''16 red'' |
\appoggiatura red''8 mi''2. r4 |
