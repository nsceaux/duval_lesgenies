\clef "basse" sol1~ |
sol2. sol,4 |
si,2 si,4. sol,8 |
re4. mi8 fad4 re |
sol1~ |
sol2. sol4 |
si2 si4 sol |
re' do' si8 la16 sol |
do'2 si4 |
do'8 si la sol re' re fad re |
sol fa mi re do4~ do16 si, la, sol, |
re4. si,8 do2 si,8 do re re, |
sol,2. |
sol~ |
sol | \allowPageTurn
sol2 la si4 si8 mi |
si,4 sol8 fad mi2 |
fad4 mi re dod8 si, |
fad mi16 re dod si, lad, sol, fad,2 |
si,2 r4 r8 sol, si,4 si,8 si, |
si,2 dod4. la,8 |
re4~ re16 re mi fad sol8 fad sol mi |
la sol fad mi re4. mi8 |
fad4 fad, fad |
si8 do' si la sol2~ | \allowPageTurn
sol1~ |
sol2. sol4 |
si2 si4. sol8 |
re'8 re re' do' si sol |
do'2 si4 |
do'2 si4 |
do'8 si la re' sol2 |
la sol4 la |
si2 r4 r8 mi |
do'2 la si4 si, |
mi2
