\clef "dessus" r2 r4 sol' |
si'2 si'4. sol'8 |
re''2 re''4. sol''8 |
fad''4.\trill mi''8 re'' do'' si' la' |
si'2\trill \appoggiatura la'8 sol'4 re'\tresdoux |
sol'2~ sol'4. sol'8 |
sol'4 re'' sol'' si'' |
la''8.*5/6 fad''32 sol'' la'' la''8.(\trill sol''32 la'') si''4 |
la''2 si''4 |
\appoggiatura la''8 sol'' sol'' fad'' sol'' fad''4.\trill fad''8 |
sol''4 sol'8 la'16 si' do''4~ do''16 sol' la' si' |
la'4\trill la'8 si' \appoggiatura la'8 sol'4. sol'16 la' si'4 la'8. re''16 |
si'2\trill \appoggiatura la'8 sol'4 |
sol'_\markup\italic "un peu fort" si' re'' |
\afterGrace sol''2.~ { sol''16\doux la'' } |
si''2. la''8 sol'' fad''4\trill fad''8 sol'' |
red''4\trill mi''8 fad'' sol''4. mi''8 |
dod''4.\trill dod''8 re''4 lad'8 si' |
si'2( lad'4.\trill) si'8 |
si'4._\markup\italic "un peu fort" si'8 re''4 re''8 si' sol''4. sol''8\doux |
sol''16 la'' si'' la'' sol'' la'' sol'' fad'' mi'' fad'' mi'' re'' dod'' re'' dod'' si' |
la'8 la'' la''2 sol''8. fad''16 |
mi''8 la' si' dod'' re''4. dod''16 si' |
si'4( lad'4.)\trill si'8 |
si'2. sol'4_\markup\italic "un peu fort" |
si'2 si'4. sol'8 |
re''2 re''4.\doux sol''8 |
sol''4 re'' sol''4. si''8 |
la''8.*5/6 fad''32( sol'' la'') la''8.(\trill sol''32 la'') si''4 |
la''2 sol''4 |
\appoggiatura sol''8 fad''2 si''4 |
\appoggiatura la''8 sol''4 sol''8 fad'' \appoggiatura fad'' sol''4 r8 sol'' |
red''4\trill~ red''16 si' dod'' red'' mi''4. fad''16 sol'' |
fad''4. si''8 si''4 do'''8 si'' |
la''4. sol''16( fad'') sol''4. fad''16( mi'') red''4.\trill mi''8 |
mi''2
