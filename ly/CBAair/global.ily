\key sol \major
\digitTime\time 2/2 \midiTempo#160 s1*7
\digitTime\time 3/4 \midiTempo#80 s2.*2
\time 4/4 \grace s8 s1*2
\time 3/2 \midiTempo#160 s1.
\digitTime\time 3/4 \grace s8 s2.*3
\time 3/2 s1.
\digitTime\time 2/2 \grace s8 s1*3
\time 3/2 s1.
\digitTime\time 2/2 \midiTempo#160 s1*3
\digitTime\time 3/4 s2.
\digitTime\time 2/2 s1*4
\digitTime\time 3/4 \midiTempo#80 s2.*3
\digitTime\time 2/2 \grace s8 s1*3
\time 3/2  \midiTempo#160 s1.
\digitTime\time 2/2 s2 \bar "|."
