Je cède à ta voix qui m’ap -- pel -- le,
a -- mour, a -- chè -- ve mon bon -- heur ;
pour prix de tous les biens dont tu flat -- tes mon cœur,
je t’offre u -- ne flamme é -- ter -- nel -- le.
Maî -- tre des rois, tu con -- duis l’u -- ni -- vers,
tu cou -- ron -- nes des cœurs in -- con -- nus sur la ter -- re ;
tu for -- ces le dieu du ton -- ner -- re,
à sor -- tir de son rang, pour ê -- tre dans tes fers.
Je cède à ta voix qui m’ap -- pel -- le,
a -- mour, a -- chè -- ve mon bon -- heur ;
pour prix de tous les biens dont tu flat -- tes mon cœur,
je t’offre u -- ne flamme é -- ter -- nel -- le.
