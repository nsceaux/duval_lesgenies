\piecePartSpecs
#`((dessus #:instrument "Violons")
   (dessus2 #:instrument "Violons")
   (basse #:score-template "score-basse-continue-voix"
          #:instrument
          ,#{ \markup\center-column { [Basses et B.C.] } #})

   (silence #:on-the-fly-markup , #{
\markup\fontsize#-2 \column {
  \livretPers Zaïre
  \livretVerse#8 { Je cède à ta voix qui m’appelle, }
  \livretVerse#8 { Amour, achève mon bonheur ; }
  \livretVerse#12 { Pour prix de tous les biens dont tu flattes mon cœur, }
  \livretVerse#8 { Je t’offre une flamme éternelle. }
  \livretVerse#10 { Maître des rois, tu conduis l’univers }
  \livretVerse#12 { Tu couronnes des cœurs inconnus sur la terre ; }
  \livretVerse#8 { Tu forces le dieu du tonnerre, }
  \livretVerse#12 { À sortir de son rang, pour être dans tes fers. }
  \livretVerse#8 { Je cède, &c. }
}#}))
