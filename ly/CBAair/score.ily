\score {
  \new ChoirStaff <<
    \new Staff \with { instrumentName = "Violons" } <<
      \global \includeNotes "dessus"
    >>
    \new Staff \with {
      instrumentName = \markup\character Zaïre
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*7 s2.\break \grace s8 s2. s1*2 s2 \bar "" \pageBreak
        \grace s8 s1 s2.*3 s1.\break \grace s8 s1*3 s1.\pageBreak
        s1*3 s2.\break s1*4 s2.*3\pageBreak
        \grace s8 s1*2\break s1 s2. s2
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
