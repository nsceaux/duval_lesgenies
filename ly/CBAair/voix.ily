\clef "vdessus" R1*4 |
r2 r4 sol' |
si'2 si'4. sol'8 |
re''2 re''4. sol''8 |
fad''4\trill fad'' r8 sol'' |
\appoggiatura sol''8 fad''2 sol''4 |
\appoggiatura fad''8 mi'' re'' do''8. si'16 la'4\trill r8 re'' |
\appoggiatura do''8 si'4\trill si'8 do''16 re'' \appoggiatura re''8 mi'' mi''16 mi'' mi''8 fad''16 sol'' |
fad''4\trill r8 re'' mi''4. mi''16 fad'' sol''4 sol''8 fad'' |
\appoggiatura fad''8 sol''2 sol''4 |
R2. |
sol'4 si' re'' |
sol''2. fad''8 sol'' red''4\trill red''8 mi'' |
\appoggiatura mi''8 fad''4 mi''8 re'' dod''4 re''8 mi'' |
lad'4\trill fad'8 fad' si'4 dod''8 re'' |
re''2( dod''4.\trill si'8) |
si'4 r r2 r4 si' |
re''4 re''8 si' sol''4 mi''8 la'' |
fad''16[\melisma sol'' fad'' mi''] re''[ mi'' re'' do'']\melismaEnd si'4 si'8 mi'' |
dod''4 re''8 mi'' \appoggiatura mi'' fad''4. sol''8 |
dod''4. dod''8 red'' mi'' |
red''2\trill r |
r r4 sol' |
si'2 si'4. sol'8 |
re''2 re''4. sol''8 |
fad''4\trill fad'' r8 sol'' |
\appoggiatura sol'' fad''2 r4 |
r r sol'' |
\appoggiatura fad''8 mi'' re'' do''8.\trill si'16 si'4 r8 si' |
fad''4 red''8 mi''16 fad'' si'8 dod''16 red'' dod''8 red''!16 mi'' |
red''4 r8 si' sol''2 |
fad''4. \appoggiatura mi''8 red'' mi''4 fad''8 sol'' sol''4( \afterGrace fad''\trill) mi''8 |
mi''2
