\clef "basse" si,8 si la si |
sol fad sol mi fad4 si, |
fad,2 si,4 la, |
sol,8 sol fad mi red4 si, |
mi2 mi8 re do si, |
la,4 sol, fad,2 |
sol,4 sol8 fad mid2 fad4. si,8 |
fad,4 fad8 mi re4 si, |
mi,2 fad, |
si, la, sol,4. fad,8 |
mi,2 mi |
red4 re do8 la, si,4 |
\ifComplet {
  mi1~ | \allowPageTurn
  mi4 re do |
  si, si8 la sold4 |
  la2 re'4 fad |
  sol re' re |
  sol4. la8 sol fad |
  mi4 la2 |
  si4 si8 mi si,4 si la2 |
  sol8 fad mi re do2 |
  si,4 mi la, do |
  si, la, sol,8. fad,16 |
  mi,2 mi4 |
  fad2 re4 |
  sol mi la fad |
  si2 si,4 |
  mi4. re8 do si, |
  la,4 re8 si, mi do re re, |
  sol, sol fad mi re4 do8 si, |
  la,4. si,8 do2 |
  si, mi,8 mi |
  fad2. |
  sol2 mi4 |
  la4. sol8 fad si |
  mi4 mi, si,~ si,16 la, sol, fad, |
  mi,4 mi8 re do si, |
  la, sol, fad,4 re, |
  sol,2 sol8 mi |
  si16 la sol mi fad8 mi re dod16 si, fad8 fad, |
  si,2 do4. la,8 |
  si,1 |
  mi~ | \allowPageTurn
  mi2 fad4 re |
  sol2.~ | \allowPageTurn
  sol2 fa4 mi8 do16 re |
  sol,4 r r8 mi |
  fa2 re4 |
  mi4. re8 do si, |
  la, re, mi,2 |
  la,8 la16 si do'8 la si la sol mi |
  si4. mi8 si, si la sol |
  fad16 la sol fad mi4 fad8 sol |
  re' re mi fad sol fa mi re |
  do re do si, la, sol, |
  fad, sol, re4 re, |
  sol,2. sol8 mi |
  la4 lad2 |
  si4. la8 sol mi |
  si4 si,2 |
  mi4 fad2 | \allowPageTurn
  sol4 sold la8 fad si mi |
  si,4 <>^"[Basses et B.C.]" si8 si la4 sol |
  re'2 re'4 re |
  sol2 sol |
  si4 si8 si do'4 re' |
  mi' re' do' si la sol |
  re'2 re4 re8 re mi4 fad |
  sol2 mi4 mi8 mi fad4. sol8 |
  la2. la4 |
  fad4 re sol4. la8 |
  si2 si4 sol |
  do'2 do'4. la8 |
  si4 si red mi |
  do4. la,8 si,2 si,4 si, |
  mi2. mi4 |
  la4 la fad4. re8 |
  sol2 sol4 sol |
  do'2 do'4. la8 |
  re'4 re' fad sol |
  mi4. si,8 do2 la,4 re |
  sol,2 \bar "" \allowPageTurn <>^"B.C." sol |
  fad4 re8 mi fad4 sol |
  re' do' si la8 sol |
  fad4 re sol8 fad mi re |
  la2 la, |
  re8 re mi fad sol2 |
  re2. re'8 do' |
  si4 la8 sol fad4 re |
  sol8 fad sol la si4 sol |
  do'8 si la sol fad4 sol |
  re'2 re4 re'8 do' |
  si4 la8 sol fad4 re |
  sol8 fad sol la si la sol fad |
  mi4 do re re, |
  sol,
}
\ifConcert { mi4 }
mi8 red mi4 re?8 do |
si,4 sold, la,4. si,8 |
do4 la, si,8 la, sol, fad, |
mi,4. mi8 la4 sol |
fad re8 mi fad4 sol |
re8 mi re do si,4 si8 sol |
do'4 si8 do' re'4 re |
sol2 sol, |
do4 la, si,4. mi8 |
red mi red dod si,4 si8 la |
sol fad mi red mi4 fad8 sol |
la4. si8 do' si do' la |
si4 si, si4. si8 |
la sol fad mi red4 mi |
la,2 si,4. mi8 |
red mi red dod si, dod red si, |
mi mi fad sol la sol la si |
do'2 la4 si |
mi2 mi4 |
la, re re, |
sol,2 sol4 |
fad sol mi |
si4. sol8 la si |
do' la si do' re' sol |
re2. |
sol8 si la sol fad mi |
red2 si,4 |
mi2 mi4 |
la2. | \allowPageTurn
sol8 la si4 si, |
mi2. |
sol4. fa8 mi re |
do2 re4 |
sol,2 do4 |
la,2 si,~ |
si,4. la,8 sol, fad, |
mi, mi do'4 la8 si |
mi4 la,2 |
si,4 si8 la sol4 fad8 mi |
si4 si,8 dod red si, |
\once\set Staff.whichBar = "|"
