\clef "dessus" r2 R1*3 R1 R1\allowPageTurn R1. R1\allowPageTurn
R1\allowPageTurn R1. R1\allowPageTurn R1
\ifComplet {
  R1\allowPageTurn
  R2.*2\allowPageTurn R1 R2.*3 R1.\allowPageTurn R1*2\allowPageTurn
  R2.*3\allowPageTurn R1\allowPageTurn R2.*2 R1*2 R1 R2.*4 R1
  R2.*3\allowPageTurn R1\allowPageTurn R1*3\allowPageTurn
  R1\allowPageTurn R2.\allowPageTurn R1\allowPageTurn
  R2.*4\allowPageTurn R1\allowPageTurn R1\allowPageTurn
  R2.\allowPageTurn R1 R2.*2\allowPageTurn R1\allowPageTurn
  R2.*4\allowPageTurn R1
  r4 <>^"Violons" <<
    \tag #'dessus1 {
      re''8 mi'' fad''4 sol'' |
      fad''8 mi'' fad'' sol'' fad'' sol'' la'' fad'' |
      sol''2 re'' |
      sol''4 sol''8 fad'' mi''4 re'' |
      do'' re'' mi'' re'' do''4.\trill si'8 |
      la'2\trill fad''4 fad''8 fad'' sol''4 la'' |
      re''2 sol''4 sol''8 sol'' la''4 si'' |
      mi''4.\trill re''8 mi'' fad'' sol'' mi'' |
      fad''4.\trill la''8 sol''4 fad''8 mi'' |
      red''2\trill red''4 si'' |
      si'' la''8 sol'' la''2~ |
      la''4 la'' la'' sol'' |
      sol''4. la''8 sol''4.\trill fad''8 fad''4.\trill mi''8 |
      mi''2 r |
      r4 mi'' la'' la'' |
      re'' re'' sol''8 la'' sol'' fad'' |
      mi''2.\trill mi''4 |
      fad''4. sol''8 la''4 re'' |
      sol''2 sol''( fad''4.)\trill sol''8 |
      sol''2
    }
    \tag #'dessus2 {
      sol'8 sol' do''4 si' |
      la'8 sol' la' si' la' si' do'' re'' |
      si'2\trill si'8 la' si' do'' |
      re''4 mi''8 re'' do''4 si' |
      la' si' do'' sol' fad'4. sol'8 |
      fad'2\trill la'4 la'8 re'' do''4. re''8 |
      si'2\trill si'4. mi''8 re''4. mi''8 |
      dod''\trill si' dod'' re'' dod'' re'' mi'' dod'' |
      re''4 re''8 do'' si'4 la'8 sol' |
      fad'2\trill fad'4 sol'' |
      mi'' fad''8 sol'' fad''4.\trill mi''8 |
      red''4\trill fad'' si'2 |
      mi'' mi''4. red''8 red''4.\trill mi''8 |
      mi''4 si' mi'' mi'' |
      dod''4 do'' do''4. re''8 |
      si'2 si'4 re'' |
      sol' sol' do''8 re'' do'' si' |
      la'4. si'8 do''4 si' |
      do''4. re''8 mi''4 la' la'4.\trill sol'8 |
      sol'2
    }
  >> r2 | \allowPageTurn
  R1*4\allowPageTurn R1\allowPageTurn R1\allowPageTurn
  R1*7\allowPageTurn
}
r4 r4 r2\allowPageTurn
R1*17\allowPageTurn R2.*16\allowPageTurn R1
R2.*3\allowPageTurn R1\allowPageTurn R2.
