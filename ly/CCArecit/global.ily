\key sol \major
\once\omit Staff.TimeSignature
\digitTime\time 2/2 \partial 2 \midiTempo#160 s2 s1*3
\time 4/4 \midiTempo#80 \grace s8 s1
\digitTime\time 2/2 \midiTempo#160 \grace s8 s1
\time 3/2 s1.
\digitTime\time 2/2 \grace s8 s1
\time 4/4 \midiTempo#80 s1
\time 3/2 \midiTempo#160 s1.
\digitTime\time 2/2 \grace s8 s1
\time 4/4 \midiTempo#80
\ifConcert { s1 \digitTime\time 2/2 \grace s8 s4 }
\ifComplet {
  s1*2
  \digitTime\time 3/4 s2.*2
  \digitTime\time 2/2 \midiTempo#160 s1
  \digitTime\time 3/4 \midiTempo#80 \grace s8 s2.*3
  \time 3/2 \midiTempo#160 s1.
  \digitTime\time 2/2 s1*2
  \digitTime\time 3/4 \midiTempo#80 s2.*3
  \digitTime\time 2/2 \midiTempo#160 s1
  \digitTime\time 3/4 \midiTempo#80 s2.*2
  \digitTime\time 2/2 \grace s8 s1*2
  \digitTime\time 2/2 \midiTempo#160 \grace s8 s1
  \digitTime\time 3/4 \midiTempo#80 \grace s8 s2.*4
  \time 4/4 \grace s8 s1
  \digitTime\time 3/4 \grace s8 s2.*3
  \time 4/4 s1
  \digitTime\time 2/2 s1*3
  \time 4/4 s1
  \digitTime\time 3/4 s2.
  \time 4/4 s1
  \digitTime\time 3/4 s2.*4
  \time 4/4 s1
  \digitTime\time 2/2 s1
  \digitTime\time 3/4 s2.
  \digitTime\time 2/2 \midiTempo#160 s1
  \digitTime\time 3/4 \midiTempo#80 \grace s8 s2.*2
  \digitTime\time 2/2 s1
  \digitTime\time 3/4 s2.*4
  \time 4/4 s1
  \digitTime\time 2/2 \midiTempo#160 \grace s8 s1*4
  \time 3/2 s1.*3
  \digitTime\time 2/2 s1*5
  \time 3/2 s1.
  \digitTime\time 2/2 s1*5
  \time 3/2 s1.
  \digitTime\time 2/2 s2
  \beginMarkSmall "Air" s2 \bar ".!:" s1*4 \alternatives s1 s1 s1*7 s4
}
\beginMarkSmall "Air" s2. s1*17
\digitTime\time 3/4 \midiTempo#120 s2.*15 \midiTempo#80 s2.
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 \grace s8 s2.*3
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2. \bar "|."
