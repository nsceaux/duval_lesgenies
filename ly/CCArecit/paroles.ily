Vous voy -- ez à vos pieds l’a -- mant le plus fi -- dè -- le,
et je re -- vois l’ob -- jet que j’ai -- me ten -- dre -- ment ;
vous ne fû -- tes ja -- mais si bel -- le,
et ja -- mais mon a -- mour ne fut si vi -- o -- lent.
Vous ne fû -- tes ja -- mais si bel -- le,
et ja -- mais mon a -- mour ne fut si vi -- o -- lent.

\ifComplet {
  Je ne puis vous re -- voir sans u -- ne peine ex -- trê -- me,
  dans un songe, à mes yeux vous a -- viez mille at -- traits.
  Ah ! ah ! que ne vous vois- je de mê -- me,
  tous mes vœux se -- raient sa -- tis -- faits !
  
  Jus -- te ciel ! est-ce à moi que ce dis -- cours s’a -- dres -- se ?
  
  Non, non c’est à l’a -- mour qui tra -- hit sa pro -- mes -- se.
  
  Que vous a- t-il pro -- mis qu’il ne puis -- se te -- nir ?
  Par -- lez, il peut en -- cor con -- ten -- ter votre en -- vi -- e.
  
  Ban -- nis -- sez- moi de vo -- tre sou -- ve -- nir,
  et s’il se peut aus -- si, que mon cœur vous ou -- bli -- e.
  
  Qui ? moi vous ou -- bli -- er ? Vous vou -- lez donc ma mort,
  cru -- elle, a -- che -- vez votre ou -- vra -- ge,
  vo -- tre bouche et vos yeux ont le mê -- me lan -- ga -- ge,
  c’est as -- sez, c’est as -- sez pour fi -- nir mon sort.
  
  Je vous aime, __ il est temps de vous ou -- vrir mon â -- me,
  que puis- je vous of -- frir pour ré -- pondre à vos vœux,
  je n’ai que des sou -- pirs pour prix de vo -- tre flam -- me
  et pour mon tendre a -- mour vous n’a -- vez que des feux ;
  si le ciel m’eut pla -- cé en un rang glo -- ri -- eux,
  j’au -- rais fait mon bon -- heur d’u -- nir mon sort au vô -- tre,
  quel -- le ri -- gueur, hé -- las ! pla -- gnez vous- en aux dieux,
  nos cœurs é -- taient faits l’un pour l’autre
  et mal -- gré notre a -- mour il faut bri -- ser nos nœuds.
  
  Je n’en -- tends que trop ce lan -- ga -- ge,
  quel -- que ri -- val ca -- ché s’op -- pose à mon bon -- heur ;
  mais il n’est point en -- cor maî -- tre de vo -- tre cœur ;
  il faut man -- quer d’a -- mour, ou man -- quer de cou -- ra -- ge,
  pour souf -- frir un au -- tre vain -- queur.
  Il faut man -- quer d’a -- mour, ou man -- quer de cou -- ra -- ge,
  pour souf -- frir un au -- tre vain -- queur.
  
  Vous m’ac -- cu -- sez d’ê -- tre vo -- la -- ge,
  et vo -- tre cœur se livre à des soup -- çons ja -- lous ;
  Vous m’ac -- cu-  lous ;
  in -- grat, quand je n’ai -- me que vous,
  ai- je mé -- ri -- té cet ou -- tra -- ge ?
  In -- grat, quand je n’ai -- me que vous,
  ai- je mé -- ri -- té cet ou -- tra -- ge ?
}

Le pou -- voir de vos yeux s’é -- tend sur tous les cœurs,
il n’est rien dans les cieux, sur la terre et sur l’on -- de,
qui ne cède à leurs traits vain -- queurs :
jus -- que dans le cen -- tre du mon -- de,
ils sa -- vent al -- lu -- mer les plus vi -- ves ar -- deurs.
Jus -- que dans le cen -- tre du mon -- de,
ils sa -- vent al -- lu -- mer les plus vi -- ves ar -- deurs.

À mes fai -- bles ap -- pas vous don -- nez trop d’em -- pi -- re,
ils ne rè -- gnent que sur un cœur ;
la gloire et le bien où j’as -- pi -- re,
se -- rait de fai -- re son bon -- heur.
Je vois que chaque ins -- tant re -- dou -- ble vos a -- lar -- mes.

C’est dou -- ter trop long -- temps du pou -- voir de vos char -- mes,
con -- nais -- sez où s’é -- tend l’em -- pi -- re de vos yeux.

que vois-je ! où suis- je ! ô jus -- tes dieux !

