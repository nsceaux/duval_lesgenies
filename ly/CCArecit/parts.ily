\piecePartSpecs
#`((dessus #:score-template "score-2dessus-voix")
   (dessus1 #:score-template "score-voix")
   (dessus2 #:score-template "score-voix")
   (basse #:score-template "score-basse-continue-voix"
          #:instrument "[B.C.]")
   (silence #:on-the-fly-markup , #{
\markup\fontsize#-3 \fill-line {
  \column {
    \livretPers Adolphe
    \livretVerse#12 { Vous voyez à vos pieds l’amant le plus fidèle, }
    \livretVerse#12 { Et je revois l’objet que j’aime tendrement ; }
    \livretVerse#8 { Vous ne fûtes jamais si belle, }
    \livretVerse#12 { Et jamais mon amour ne fut si violent. }
    \livretPers Zaïre
    \livretVerse#12 { Je ne puis vous revoir sans une peine extrême, }
    \livretVerse#12 { Dans un songe à mes yeux vous aviez mille attraits. }
    \livretVerse#8 { Ah ! que ne vous vois-je de même, }
    \livretVerse#8 { Tous mes vœux seraient satisfaits ! }
    \livretPers Adolphe
    \livretVerse#12 { Juste ciel ! est-ce à moi que ce discours s’adresse ? }
    \livretPers Zaïre
    \livretVerse#12 { Non, non c’est à l’amour qui trahit sa promesse. }
    \livretPers Adolphe
    \livretVerse#12 { Que vous a-t-il promis qu’il ne puisse tenir ? }
    \livretVerse#12 { Parlez, il peut encor contenter votre envie. }
    \livretPers Zaïre
    \livretVerse#10 { Bannissez-moi de votre souvenir, }
    \livretVerse#12 { Et s’il se peut aussi, que mon cœur vous oublie. }
    \livretPers Adolphe
    \livretVerse#12 { Qui ? moi vous oublier ? Vous voulez donc ma mort, }
    \livretVerse#8 { Cruelle, achevez votre ouvrage, }
    \livretVerse#12 { Votre bouche et vos yeux ont le même langage, }
    \livretVerse#8 { C’est assez pour finir mon sort. }
    \livretPers Zaïre
    \livretVerse#12 { Je vous aime, il est temps de vous ouvrir mon âme, }
    \livretVerse#12 { Que puis-je vous offrir pour répondre à vos vœux, }
    \livretVerse#12 { Je n’ai que des soupirs pour prix de votre flamme }
    \livretVerse#12 { Et pour mon tendre amour vous n’avez que des feux ; }
    \livretVerse#11 { Si le ciel m’eut placé en un rang glorieux, }
    \livretVerse#12 { J’aurais fait mon bonheur d’unir mon sort au vôtre, }
  }
  \column {
    \livretVerse#12 { Quelle rigueur, hélas ! plagnez vous-en aux dieux, }
    \livretVerse#8 { Nos cœurs étaient faits l’un pour l’autre }
    \livretVerse#12 { Et malgré notre amour il faut briser nos nœuds. }
    \livretPers Adolphe
    \livretVerse#8 { Je n’entends que trop ce langage, }
    \livretVerse#12 { Quelque rival caché s’oppose à mon bonheur ; }
    \livretVerse#12 { Mais il n’est point encor maître de votre cœur ; }
    \livretVerse#12 { Il faut manquer d’amour, ou manquer de courage, }
    \livretVerse#8 { Pour souffrir un autre vainqueur. }
    \livretPers Zaïre
    \livretVerse#8 { Vous m’accusez d’être volage, }
    \livretVerse#12 { Et votre cœur se livre à des soupçons jalous ; }
    \livretVerse#8 { Ingrat, quand je n’aime que vous, }
    \livretVerse#8 { Ai-je mérité cet outrage ? }
    \livretPers Adolphe
    \livretVerse#12 { Le pouvoir de vos yeux s’étend sur tous les cœurs, }
    \livretVerse#12 { Il n’est rien dans les cieux, sur la terre & sur l’onde, }
    \livretVerse#8 { Qui ne cède à leurs traits vainqueurs : }
    \livretVerse#8 { Jusque dans le centre du monde, }
    \livretVerse#12 { Ils savent allumer les plus vives ardeurs. }
    \livretPers Zaïre
    \livretVerse#12 { À mes faibles appas vous donnez trop d’empire, }
    \livretVerse#8 { Ils ne règnent que sur un cœur ; }
    \livretVerse#8 { La gloire & le bien où j’aspire }
    \livretVerse#8 { Serait de faire son bonheur. }
    \livretVerse#12 { Je vois que chaque instant redouble vos alarmes. }
    \livretPers Adolphe
    \livretVerse#12 { C’est douter trop longtemps du pouvoir de vos charmes, }
    \livretVerse#12 { Connaissez où s’étend l’empire de vos yeux. }
    \livretPers Zaïre
    \livretVerse#8 { Que vois-je ! où suis-je ! ô justes dieux ! }    
  }
}#}))
