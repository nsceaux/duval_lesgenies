\score {
  \new ChoirStaff <<
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
      \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = "[B.C.]"
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2 s1*3 s2 \bar "" \break s2 s1 s1.\pageBreak
        \grace s8 s1*2 s1.\break \grace s8 s1*3\break s2.*2 s1 s2.\pageBreak
        s2.*2 s1. s1 s4 \bar "" \break s2. s2.*3\break s1 s2.*2 s1\pageBreak
        \grace s8 s1*2 s2.\break \grace s8 s2.*3 s2 \bar "" \break s2 s2.*2 s2 \bar "" \pageBreak
        s4 s1*3\break s1*2 s2.\break s1 s2.*2\pageBreak
        \grace s8 s2.*2 s1*2\break s2. s1 s2.\break s2. s1 s2.*3 s4 \bar "" \pageBreak
        s2 s1 s4 \bar "" \break s2. s1*3 s1.\pageBreak
        s1.*2 s1*2\break s1*3 s1. s1\pageBreak
        s1*4 s1. s2 \bar "" \break s2 s1*4\pageBreak
        s1*4\break s1*4\break s1*3\pageBreak
        s1*4 s2 \bar "" \break s2 s1*4\break s1*5 s2 \bar "" \pageBreak
        s2 s1 s2.*4 s2 \bar "" \break s4 s2.*6\break s2.*5\pageBreak
        s1 s2.*2\break s2. s1 s2.
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
