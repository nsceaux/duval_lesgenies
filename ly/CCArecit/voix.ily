\ffclef "vbasse" r2 |
R1 |
<>^\markup\character Adolphe |
r4 fad8 sol red4\trill mi8 fad |
si,4. mi8 la4 la8 sol16 fad |
\appoggiatura fad8 sol4 \appoggiatura fad8 mi4 sol8 sol16 sol la8 si |
\appoggiatura si do'4. la8 re' la si do' |
si4\trill r8 si16 dod' re'4 dod'8 re' lad4.\trill si8 |
\appoggiatura si8 dod'2 fad4 r8 si16 la |
sol4 sol16 fad mi re dod4\trill dod8 dod16 re |
si,4 r8 fad16 sol red4\trill red8 red mi4. fad8 |
\appoggiatura fad8 sol2 \appoggiatura fad8 mi4 r8 sol16 la |
fad8 sol16 la \appoggiatura la8 si sol \appoggiatura fad mi4\trill mi8 mi16 red |
\appoggiatura red8 mi4
\ifComplet {
  \ffclef "vdessus" <>^\markup\character Zaïre
  r8 sol'16 sol' sol'4 la'8 si' |
  fad'4 si'8 si'16 si' mi''8 fad'' |
  red''4\trill red'' r8 mi''16 re'' |
  do''4 do''8 si' la'4\trill re''8 la' |
  \appoggiatura la'8 si'4 la'4.\trill sol'8 |
  sol'4 si'2 |
  sol''4 r8 fad'' fad'' sol'' |
  red''4\trill red''8 mi'' \appoggiatura mi''8 fad''2 si' |
  mi''8 re'' do'' si' la'4. sol'16 la' |
  \appoggiatura la'8 si'4
  \ffclef "vbasse" <>^\markup\character Adolphe
  r8 sol16 mi do'4 fad8 la |
  red4\trill red8 red16 red mi8. fad16 |
  \appoggiatura fad8 sol4 sol
  \ffclef "vdessus" <>^\markup\character Zaïre
  r8 do'' |
  la'4\trill re'' la'8 re'' |
  si'4\trill si'8 mi'' dod''4 red''8 mi'' |
  red''2\trill red''4 |
  \ffclef "vbasse" <>^\markup\character Adolphe
  sol4 sol8 sol la si |
  \appoggiatura si do'4 la8 re' sol4. sol16 fad |
  \appoggiatura fad8 sol4 r8 sol re'4 r16 la la si |
  \appoggiatura si8 do'4 la8 la red4\trill red8 mi |
  \appoggiatura mi fad4 fad
  \ffclef "vdessus" <>^\markup\character Zaïre
  r16 si' si' do'' |
  \appoggiatura si'8 la'4 la'8 la'16 la' si'8 do'' |
  si'4 mi''8 mi''16 mi'' fad''8 sol'' |
  dod''8 dod''16 dod'' red''8 si'16 mi'' mi''8.[ red''16] |
  \appoggiatura red''8 mi''4
  \ffclef "vbasse" <>^\markup\character Adolphe
  r8 si red4 red8 mi16 fad |
  \appoggiatura fad8 sol4 r16 sol sol sol la8. si16 |
  \appoggiatura si8 do' r16 la re'8 la16 si do'8 do'16 si |
  si4\trill si r8 si16 dod' |
  re'8 dod'16 mi' lad8 fad16 fad si8 dod'16 re' dod'4\trill |
  si r8 fad16 sol la4 r8 si16 do' |
  fad4 fad8 sol fad4.\trill mi8 |
  mi4
  \ffclef "vdessus" <>^\markup\character Zaïre
  r8 sol'16 mi' si'2~ |
  si'8 mi''16 si' \appoggiatura si'8 do''4 la'8 la'16 la' si'8. do''16 |
  si'4\trill si' r8 sol' |
  si'4 si'8 si'16 do'' \appoggiatura do''8 re''8 si'16 re'' sol'8 sol'16 fad' |
  sol'8 r16 si' si'8. do''16 do''([ si']) la'([ sold']) |
  \appoggiatura sold'8 la'2 re''4 |
  \appoggiatura do''8 si'4. \appoggiatura la'8 sold' la' si' |
  do''4( si'4.\trill la'8) |
  la'16 do'' do'' si' la'8. sol'16 fad'4 r8 si'16 mi'' |
  red''4\trill red''8 mi'' \appoggiatura mi'' fad''4 r8 si'16 \ficta dod'' |
  red''8\trill mi''16 fad'' sol''8 fad''16 mi'' \ficta re''8 do''16 si' |
  la'4\trill re''8 do'' si'4\trill do''8 re'' |
  \appoggiatura re''8 mi''4. mi''8 fad'' sol'' |
  do'' si' la'4.(\trill sol'8) |
  sol'2 r4 si'8 dod''16 re'' |
  dod''4\trill fad''2 |
  red''\trill r16 mi'' fad'' sol'' |
  sol''4( fad''4.)\trill mi''8 |
  mi''8 r16 do'' la'8 la'16 si' do''8 do''16 re'' |
  si'8\trill si'16 dod'' re''8 si'16 mi'' dod''\trill dod'' red'' mi'' red''8.\trill mi''16 |
  \appoggiatura mi''8 fad''4
  \ffclef "vbasse" <>^\markup\character Adolphe
  si8 si la4 sol |
  re'2 re'4 re |
  sol2 sol |
  si4 si8 si do'4 re' |
  mi' re' do' si la sol |
  re'2 re4 re8 re mi4 fad |
  sol2 mi4 mi8 mi fad4. sol8 |
  la2. la4 |
  fad4 re sol4. la8 |
  si2 si4 sol |
  do'2 do'4. la8 |
  si4 si red4. mi8 |
  do4. la,8 si,2 si,4 si, |
  mi2. mi4 |
  la4 la fad4. re8 |
  sol2 sol4 sol |
  do'2 do'4. la8 |
  re'4 re' fad sol |
  mi4. si,8 do2 la,4 re |
  sol,2
  \ffclef "vdessus" <>^\markup\character Zaïre
  r8 si' do'' re'' |
  la'4\trill re'' do''16([ si'8.]) la'16([ sol'8.]) |
  la'4 fad'\trill sol' la'8 si' |
  \appoggiatura si' do''4. re''8 si' si' dod'' re'' |
  re''2( dod''4.)\trill re''8 |
  re''2 r8 si' do'' re'' |
  re''2. re''4 |
  sol''4 fad''8 mi'' re''4 do''8 re'' |
  si'2\trill re''8 do'' si' la' |
  sol'4 la'8 si' do''4.( si'16[\trill la']) |
  la'2. re''4 |
  sol'' fad''8 mi'' re''4 do''8 re'' |
  si'2\trill re''8 do'' si' la' |
  sol'4 la'8 si' \afterGrace la'2\trill( sol'8) |
  sol'4
  \ffclef "vbasse" <>^\markup\character Adolphe
}
mi8 fad sol4 fad8 mi |
si4. si8 do'4. si8 |
la8([ sol]) fad([ mi]) red4\trill mi8 fad |
sol4 la8 si do'4 si8 la |
re'2 do'8([ si]) la([ sol]) |
la2 re4 si8 sol |
do'4 si8 do' re'4 re |
sol2 si4. si8 |
la[ sol] fad[ mi] red4\trill red8 mi |
si,2 si,4 si, |
mi8 red mi fad sol4 la8 si |
do'2 do'4 \appoggiatura si8 la4 |
si2 r |
r si4. si8 |
la[ sol] fad[ mi] red4 red8 mi |
si,2 si,4 si |
sol8 sol fad mi la4 la8 si |
do'2 la4 si |
mi2
\ffclef "vdessus" <>^\markup\character Zaïre
r8 mi''16 si' |
do''4 la'4.(\trill sol'16) la' |
si'2 si'8 dod'' |
re''4 \appoggiatura do''8 si'4 mi'' |
red''\trill \appoggiatura dod''8 si'4 sol''8 fad'' |
mi''2 la'8 si' |
si'4( la'4.)\trill sol'8 |
sol'2 r8 si' |
fad''2 mi''8 fad'' |
sol''2 fad''8 mi'' |
fad''4 fad''8 si' \ficta dod'' \ficta red'' |
mi'' fad'' red''4.\trill mi''8 |
mi''2 r8 si' |
si'4. si'8 do'' re'' |
mi'' re'' do'' si' la' re'' |
si'4\trill \appoggiatura la'8 sol'
\ffclef "vbasse" <>^\markup\character Adolphe
si16 sol mi8 fad16 sol |
la4 fad8 la red4\trill red8 mi |
\appoggiatura mi fad fad r sol16 la si8 dod'16 red' |
mi'8 mi do'8. do'16 la8 si |
mi
\ffclef "vdessus" <>^\markup\character Zaïre
r16 si' fad''4 r8 fad'' |
red''4\trill red'' r8 mi'' fad'' sol'' |
fad''2\trill r4 |
