\clef "basse"
\setMusic #'rondeau {
  mi2 sol4 mi |
  la,2 do |
  si,4 si8 la sol4 fad |
  mi2 red |
  mi4 mi, si, la, |
  sol,8 la, sol, fad, mi,4 mi |
  la,2 do |
  si,4 si8 la sol4 fad |
  mi la si si, |
  mi1 |
}
\keepWithTag #'() \rondeau
sol2 sol4. la8 |
si2. sol8 fad |
mi4 sol fad re |
sol sol, re8 mi fad re |
sol2 sol4. la8 |
si2. sol8 fad |
mi4 sol fad mi |
re sol, fad, sol, |
do2 re |
sol,4 sol8 la si la sol fad |
\rondeau
mi'2 mi'4 si |
do'4. re'8 do'4 si |
la si do' la |
si la sol fad |
mi fad sol mi |
la, si, do la, |
si,2 si4 la |
sol fad sol mi |
fad2 mi |
re4 dod si, lad, |
si, mi, fad,2 |
si, si8 la sol fad |
\rondeau
