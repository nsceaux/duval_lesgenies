\setMusic #'rondeau {
  s2 <6> <6 5> <4 3> <_+>4.\figExtOn <_+>8\figExtOff <6>4 <6+> s2 <5> s <_+>4\figExtOn <_+>
  <6>4. <6>8\figExtOff s2 <6 5> <4 3> <_+>4.\figExtOn <_+>8\figExtOff <6>4 <6+> s <6 5> <_+> <7> s1
}
\keepWithTag #'() \rondeau
s1 <6> <7>4 <5> <6 5/> <7> s2 <"">4.\figExtOn <"">8\figExtOff s2 <"">4.\figExtOn <"">8\figExtOff <6>1 <7>4\figExtOn <7>\figExtOff <6> <6+> s2 <5/> <6 5>1 <"">2.\figExtOn <"">4\figExtOff
\rondeau
s2. <6>4 <"">2\figExtOn <""> <7 5>4 <7> <4 3> <4> <_+> <_+>\figExtOff <6> <6+>
<"">\figExtOn <"">\figExtOff <6> <7> <6 5>\figExtOn <6>2\figExtOff <7 5>4
<_+>2 <"">4\figExtOn <""> <6> <6>\figExtOff <4+ 3>2 <_+>2\figExtOn <_+>\figExtOff
<6>4 <6+>2 <6 5/>4 s <7 5> <_+>2 <_+>2 <"">4.\figExtOn <"">8\figExtOff
\rondeau
