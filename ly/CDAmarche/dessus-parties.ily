\tag #'(dessus1 dessus2 dessus) \clef "dessus"
\tag #'haute-contre \clef "haute-contre"
\tag #'taille \clef "taille"
\setMusic #'rondeau <<
  \tag #'(dessus1 dessus2) {
    \tag #'dessus2 \startHaraKiri
    mi''2 si'4.(\trill la'16 si') |
    do''2 la'4.(\trill sol'16 la') |
    si'4 dod''8 red'' mi''4 fad'' |
    sol'' fad''8 sol'' la''4 si'' |
    sol''2\trill fad'' |
    mi'' si'4.(\trill la'16 si') |
    do''2 la'4.(\trill sol'16 la') |
    si'4 dod''8 red'' mi''4 fad'' |
    sol'' fad''8 mi'' red''4.\trill mi''8 |
    mi''1 |
    \tag #'dessus2 \stopHaraKiri
  }
  \tag #'(haute-contre haute-contre-concert) {
    sol'2 sol'4. fad'16 sol' |
    la'2 fad'4. mi'16 fad' |
    red'4 mi'8 fad' sol'4 la' |
    si' la'8 sol' fad'4 fad' |
    si'2 si' |
    si' sol'4. fad'16 sol' |
    la'2 fad'4. mi'16 fad' |
    red'4 mi'8 fad' sol'4 la' |
    si'4 la'8 sol' fad'4. mi'8 |
    mi'1 |
  }
  \tag #'(taille taille-concert) {
    si2 mi' |
    fad' mi' |
    fad' mi'4 red' |
    mi'2 si |
    mi' red' |
    mi' mi' |
    fad' mi' |
    fad' mi'4 red' |
    mi'2 si |
    si1 |
  }
>>
%% Rondeau
\keepWithTag #'(dessus1 dessus2 haute-contre haute-contre-concert taille taille-concert) \rondeau
%% Couplet 1
<<
  \tag #'(dessus1) {
    si'2 si'4. do''8 |
    re''2. mi''8 fad'' |
    sol''4 fad''8 mi'' re''4 do'' |
    si'4. do''8 la'2 |
    si'2 si'4.\trill do''8 |
    re''2. mi''8 fad'' |
    sol''4 si'' la'' sol'' |
    la'' si'' la'' sol'' |
    la''2 fad''\trill |
    sol''1 |
  }
  \tag #'(dessus2 haute-contre-concert taille-concert) {
    re'2 re' |
    sol'2. sol'8 la' |
    si'2 do''4 la' |
    sol'2 fad' |
    re' re' |
    sol'2. sol'8 la' |
    si'4 re'' re'' dod'' |
    re''4 si' do''! si' |
    la'2 re'' |
    si'1 |
  }
  \tag #'(haute-contre taille) R1*10
>>
%% Rondeau
\rondeau
%% Couplet 2
<<
  \tag #'(dessus1) {
    sol''2 sol''4. fad''8 |
    mi''2~ mi''4. red''8 |
    mi''4. fad''16 sol'' fad''4.\trill mi''8 |
    red''4 fad'' si'' la'' |
    sol'' fad'' mi'' re'' |
    do'' si' la' sol' |
    fad'4 sol'8 la' si'4 dod''8 red'' |
    mi''4 re'' dod'' si' |
    lad' si' dod'' fad' |
    si'4. lad'8 si'4 mi'' |
    re''2 dod''\trill |
    si'1 |
  }
  \tag #'(dessus2 haute-contre-concert taille-concert) {
    si'2 si' |
    sol' sol' |
    sol'4. fad'16 mi' mi'2 |
    fad'2 mi'4 red' |
    mi'2 si' |
    do'' do'' |
    si'4 si'8 la' sol'4 fad' |
    mi'2 mi'4 sol' |
    fad' sold' lad' fad' |
    fad' mi' re' dod'' |
    si'2 lad' |
    si'1 |
  }
  \tag #'(haute-contre taille) R1*12
>>
\rondeau
