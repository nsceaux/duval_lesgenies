\clef "haute-contre"
\setMusic #'rondeau {
  sol'2 sol'4. fad'16 sol' |
  la'2 fad'4. mi'16 fad' |
  red'4 mi'8 fad' sol'4 la' |
  si' la'8 sol' fad'4 fad' |
  si'2 si' |
  si' sol'4. fad'16 sol' |
  la'2 fad'4. mi'16 fad' |
  red'4 mi'8 fad' sol'4 la' |
  si'4 la'8 sol' fad'4. mi'8 |
  mi'1 |
}
\keepWithTag #'() \rondeau
re'2 re' |
sol'2. sol'8 la' |
si'2 do''4 la' |
sol'2 fad' |
re' re' |
sol'2. sol'8 la' |
si'4 re'' re'' dod'' |
re''4 si' do''! si' |
la'2 re'' |
si'1 |
\rondeau |
si'2 si' |
sol' sol' |
sol'4. fad'16 mi' mi'2 |
fad'2 mi'4 red' |
mi'2 si' |
do'' do'' |
si'4 si'8 la' sol'4 fad' |
mi'2 mi'4 sol' |
fad' sold' lad' fad' |
fad' mi' re' dod'' |
si'2 lad' |
si'1 |
\rondeau |
