\piecePartSpecs
#`((dessus #:score "score-dessus"
           #:instrument
           ,#{\markup\center-column { [Violons et Hautbois] }#})
   (dessus1 #:notes "dessus-parties"
            #:instrument ,#{\markup\center-column { [Violons et Hautbois] }#})
   (dessus2 #:notes "dessus-parties"
            #:instrument ,#{\markup\center-column { [Violons et Hautbois] }#})
   (parties #:score "score-parties")
   (basse #:instrument
          ,#{ \markup\center-column { [Basses, Bassons et B.C.] } #})
   (dessus2-haute-contre)
   (taille #:tag-notes taille #:notes "dessus-parties")
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
