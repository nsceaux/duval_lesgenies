\score {
  \new GrandStaff \with {
    instrumentName = \markup\center-column { [Violons et Hautbois] }
    \haraKiriFirst
  } <<
    \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus-parties" >>
    \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus-parties" >>
  >>
  \layout { indent = \largeindent }
}
