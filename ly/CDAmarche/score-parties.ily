\score {
  \new StaffGroup <<
    \new Staff << \global \keepWithTag #'haute-contre \includeNotes "dessus-parties" >>
    \new Staff << \global \keepWithTag #'taille \includeNotes "dessus-parties" >>
  >>
  \layout { }
}
