\score {
  \new StaffGroup \with { \haraKiriFirst }<<
    \new GrandStaff \with {
      instrumentName = \markup\center-column { [Violons et Hautbois] }
    } <<
      \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus-parties" >>
      \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus-parties" >>
    >>
    \ifFull <<
      \new Staff \with {
        instrumentName = \markup\center-column { [Hautes-contre] }
      } << \global \keepWithTag #'haute-contre \includeNotes "dessus-parties" >>
      \new Staff \with {
        instrumentName = "[Tailles]"
      } << \global \keepWithTag #'taille \includeNotes "dessus-parties" >>
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses, Bassons et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion {
        s1*10\break s1*10\break s1*10\break s1*12\break
      }
      \origLayout {
        s1*10\break s1*10\break s1*10\break s1*12\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
