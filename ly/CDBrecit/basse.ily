\clef "basse"
\ifComplet {
  sol2 do4 dod |
  re1 |
  mi8 do re4 re, |
}
sol,1 |
re8. do16 re mi re do si,8 do re re, |
sol,4 sol2 fad4 |
sol2 sol,4 |
fad,16 la, sol, fad, mi re do si, la,8 re |
sol,4. re,8 mi, fad, |
sol,4 la,8 si, do la, |
si,4 dod red si, mi  mi, |
si,2 r | \allowPageTurn
<>^"[Basses et B.C.]" mi2. r8 si |
mi'4 do' la4.(\trill sol16 la) |
si4 si, red si, |
mi4. fad8 sol4. la8 si4 sol |
do2 re4 re, |
sol8 la sol fad mi4 |
red8 re dod4. do8 |
si,4 si mi'8 re' |
do' re' mi' re' do' si |
la sol fad mi fad re |
sol4 si,8 do re re, |
sol,4 sol8 la sol fad |
mi fad sol la si si, |
mi4 red mi |
si si, si8 la |
sol fad mi4. mi8 |
la si la sol fad mi |
red4 si,8 dod red si, |
mi4 r r |
r4 la,8 si, dod la, |
re4 r r |
sol,4 fad,4. sol,8 |
re2 do4 |
si,4 mi mi, |
la,8 mi la sol la fad |
sol4 fad sol |
re' do'8 si la sol |
fad mi re mi fad re |
sol2 re4 |
sol8 la sol fa mi re |
do2 do'4 |
si4.\trill la8 sol la |
si re' do' si la sol |
fad4 sol sol, |
re4. mi8 fad re |
sol4 do re |
sol, sol re |
sol8 la sol fa mi re |
do'4 do do' |
si4. la8 sol la |
si re' do' si la sol |
fad4. sol8 la4 |
si si, do |
re4. re8 mi4 |
do re re, |
<< { s2 <>^"[B.C.]" } { sol,2. la,4 } >> |
si,2 red4 si, |
mi2. mi4 |
fad2. re4 |
sol2 red4 |
mi2 la,4. si,8 do2 |
si, sold |
la8 fad si la sol fad16 mi |
si8 si, mi4 la,2 |
si,2 red4 |
mi4 fad4.\trill mi16 fad |
sol4 sol, sol |
la2 si4 |
do'8 la si4 si, |
mi2 mi4 |
si2 mi4 |
si,2 si4 |
do' si4. la8 |
si2 sol8 mi |
la2 si4 |
do'4 do'4. si8 |
si4. si,8 mi re |
do2 r8 sol |
do'4 do r8 sol |
do'4. re'8 mi' re' |
do' re' do' si la sol |
re'4. re8 mi fad |
sol4. sol8 la si |
do'4. fad8 sol4 |
do re re, |
sol,4. re8 mi fad |
sol la sol fad sol mi |
la4 la4. si8 |
do'4 do' la |
si4 dod' red' |
mi'2 mi4 |
la2 sol8 si |
do4 do'8 si do' la |
si4 si,2 |
mi mi'4 |
sold2 mi4 |
la,2 r8 mi |
la2. |
si4 dod' red' |
mi'2 mi4 |
la2 sol8 si |
do4 do'8 si do' la |
si4 si,2 |
mi2. | \allowPageTurn
<>^"[Basses et B.C.]" sol,1~ |
sol, |
sol,4 sol8 fad mi4 mi8 re |
la4 la la,8 la, |
re4 mi fad |
sol2 sol4 |
la4. la8 la la |
si2. |
sol4 sol8 sol fad mi |
red2 mi4 |
do'4. do'8 do' la |
si4 si,2 |
mi4 dod la, |
re4. mi8 fad4 |
sol2 sol4 |
do'4. la8 si do' |
re'2 re4 |
sol4 sol8 sol la si |
do'4. re'8 do' si |
la4 la8 la si do' |
fad2 sol4 |
mi4. mi8 fad sol |
re4 re,2 |
sol,2.
