\clef "dessus" \tag#'dessus2 \startHaraKiri
\ifComplet { R1*2 R2. }
R1*2 R1 R2.*4 R1. | \allowPageTurn
<>^"Violons" r4 r8 si'\doux mi''4 si' |
sol'8 fad' sol' la' si' la' sol' fad' |
sol'4 la'8 si' do'' si' la' sol' |
fad'4 red''8 mi'' fad''4 mi''8 red'' |
mi''4. re''16 do'' si'4.\trill do''8 re''4 si' |
mi''~ mi''16 fad''( mi'' fad'') fad''4.(\trill mi''16 fad'') |
sol''4 re'' r |
R2. | \allowPageTurn
r4 si''8 la'' sol'' fad'' |
mi'' fad'' sol'' fad'' mi'' re'' |
do'' si' la' sol' la' fad' |
sol'4. sol'8 sol' fad' |
sol'4 re' si'8 la' |
sol'4. fad'8 mi' red' |
mi'4 la' sol' |
fad' fad''8 mi'' red'' dod'' |
si'4 si''8 la'' sol'' fad'' |
mi''4 la''8 si'' la'' sol'' |
fad''4\trill si''8 la'' sol'' fad'' |
sol''4 r r |
r4 la''8 sol'' fad'' mi'' |
fad''4 r r |
sol''4 la''4. si''8 |
fad'' sol'' fad'' mi'' re''4 |
si''4 do'''8 si'' la'' sold'' |
la''2. |
sol''4 la''4. si''8 |
fad'' mi'' re'' mi'' fad'' sol'' |
la'' sol'' fad'' sol'' la'' fad'' |
sol''2 fad''4\trill |
sol'' la''4. si''8 |
do'''4 sol''4. sol''8 |
sol''8 fad''16 mi'' re''8 fad'' mi'' fad'' |
sol''4. re''8 do'' si' |
la'4 re'4. sol'8 |
fad' sol' la' re' la' re'' |
si'4 la'8 sol' fad' la' |
sol'2 fad'4\trill |
sol'4 la'4. si'8 |
do'' re'' mi'' fad'' sol''4 |
sol''8 fad''16 mi'' re''8 fad'' mi'' fad'' |
sol''4. re''8 do'' si' |
la'4. sol'8 fad'4 |
sol'4. sol'8 do''4 |
do''4. re''8 si'4 |
mi''16 fad'' mi'' fad'' fad''4.\trill sol''8 |
sol''2 r | \allowPageTurn
R1*3 R2. R1. R1 R2. R1 R2.*40 | \allowPageTurn
<>^"Violons" <>\doux <<
  \tag #'dessus1 {
    re''2. re''4 |
    re''2 si'4~ si'16 la' si' do'' |
    re''4. re''8 sol''4. fad''8 |
    mi''2\trill la''8 sol'' |
    fad''4 sol'' la'' |
    re''2 si''4 |
    si'' la''8 si'' la'' sol'' |
    fad'' sol'' fad'' red'' mi'' fad'' |
    sol''4. sol''8 la'' si'' |
    la''4.\trill si''8 \appoggiatura la'' sol''4 |
    sol''4. sol''8 sol'' fad'' |
    sol''4( fad''4.\trill) mi''8 |
    mi''2 dod''8 mi'' |
    la'2 fad''8 la'' |
    re''2\trill re''8 sol'' |
    mi''8 fa'' mi'' re'' do'' si' |
    la' sol' la' fad' sol' la' |
    si' do'' re'' do'' si' la' |
    sol'4. sol'8 la' si' |
    do'' re'' do'' si' la' sol' |
    do''4. re''8 \appoggiatura do''8 si'4 |
    sol''8 fad'' mi'' re'' do'' si' |
    si'4( la'4.)\trill sol'8 |
    sol'2.
  }
  \tag #'(dessus2 dessus2-part) {
    \tag #'dessus2 \stopHaraKiri si'2. si'4 |
    sol'2 sol'4~ sol'16 fad' sol' la' |
    si'4. si'8 dod''4. re''8 |
    dod''2\trill dod''8 dod'' |
    re''4 re'' do'' |
    si'2\trill sol''4 |
    sol'' fad''8 sol'' fad'' mi'' |
    red'' dod'' red'' si' dod'' red'' |
    mi''4. mi''8 fad'' sol'' |
    fad''4.\trill sol''8 \appoggiatura fad'' mi''4 |
    mi''4. mi''8 mi''4 |
    mi''4( red''4.)\trill mi''8 |
    mi''2 r4 |
    r r la'8 re'' |
    si'4. do''8 re''4 |
    sol'4 do''8 si' la' sol' |
    fad' mi' fad' re' mi' fad' |
    sol' la' si' la' sol' fad' |
    mi'4. mi'8 fad' sol' |
    la' si' la' sol' fad' mi' |
    la'2 sol'4 |
    do''8 re'' do'' si' la' sol' |
    sol'4( fad'4.)\trill sol'8 |
    sol'2.
  }
>>
