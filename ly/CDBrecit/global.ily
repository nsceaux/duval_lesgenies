\key sol \major
\ifComplet {
  \time 4/4 \midiTempo#80 s1*2
  \digitTime\time 3/4 s2.
}
\digitTime\time 2/2 \midiTempo#160 s1*2
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.*4
\time 3/2 \midiTempo#160 s1.
\digitTime\time 2/2 s1*4
\time 3/2 \grace s8 s1.
\digitTime\time 2/2 s1
\digitTime\time 3/4 \midiTempo#80 s2.*2 \midiTempo#120 s2.*38
\digitTime\time 2/2 \midiTempo#160 s1*4
\digitTime\time 3/4 \midiTempo#80 s2.
\time 3/2 \midiTempo#160 \grace s8 s1.
\digitTime\time 2/2 \grace s8 s1
\digitTime\time 3/4 \midiTempo#80 s2.
\time 4/4 s1
\digitTime\time 3/4 \midiTempo#120 s2.*5 s2
\beginMarkSmall "Duo" s4 s2.*34
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 s2.*20
\digitTime\time 2/2 s2. \bar "|."
