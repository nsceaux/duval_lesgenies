\tag #'(voix1 basse) {
  \ifComplet {
    Que tout ce que je vois rend mon âme in -- ter -- di -- te !
    Je ne sau -- rais cal -- mer le trou -- ble qui m’a -- gi -- te.
  }
  Ras -- su -- rez- vous, dis -- si -- pez votre ef -- froi,
  ré -- gnez a -- vec A -- dolphe, en ré -- gnant a -- vec moi :
  pou -- vais- je ré -- sis -- ter de vous ren -- dre les ar -- mes,
  pour la pre -- miè -- re fois que j’a -- per -- çus vos char -- mes ?
  Ce fut dans ce jar -- din où la mè -- re d’A -- mour
  semble a -- voir fi -- xé son em -- pi -- re :
  vous pa -- rais -- sez, Vé -- nus quit -- te sa cour,
  tout se ran -- ge vers vous, près de vous tout sou -- pi -- re,
  les oi -- seaux en -- chan -- tés __ vous par -- laient de leurs feux ;
  les ruis -- seaux par leur doux mur -- mu -- re,
  ren -- daient hom -- mage à vos beaux yeux ;
  et le pè -- re de la na -- tu -- re
  pour vous, du plus beau jour fai -- sait bril -- ler __ ces lieux.
  Et le pè -- re de la na -- tu -- re
  pour vous, du plus beau jour fai -- sait bril -- ler, __ 
  fai -- sait bril -- ler ces lieux.
  Par tant d’at -- traits, fal -- lait- il me sur -- pren -- dre ?
  Quel cœur au -- rait pu s’en dé -- fen -- dre !
  
  Votre a -- mour me sou -- met tous ces peu -- ples di -- vers,
  et sur vous dé -- sor -- mais je règne en sou -- ve -- rai -- ne ;
  mon des -- tin le plus beau c’est de por -- ter ma chaî -- ne,
  et de vous voir por -- ter vos fers.
}
Tendre A -- mour, en -- chaî -- ne nos â -- mes,
c’est toi seul qui fais mon bon -- heur ;
\tag #'(voix1 basse) {
  n’al -- lu -- me ja -- mais,
  n’al -- lu -- me ja -- mais dans mon cœur,
}
\tag #'voix2 {
  n’al -- lu -- me ja -- mais __ dans mon cœur,
  d’au -- tres dé -- sirs,
}
d’au -- tres dé -- sirs, ni d’au -- tres flam -- mes.
\tag #'(voix1 basse) {
  N’al -- lu -- me ja -- mais,
  n’al -- lu -- me ja -- mais dans mon cœur,
}
\tag #'voix2 {
  N’al -- lu -- me ja -- mais dans mon cœur,
  dans mon cœur,
}
d’au -- tres dé -- sirs, ni d’au -- tres flam -- mes.
\tag #'voix2 {
  N’al -- lu -- me ja -- mais,
  n’al -- lu -- me ja -- mais dans mon cœur,
}
\tag #'(voix1 basse) {
  N’al -- lu -- me ja -- mais dans mon cœur,
  dans mon cœur,
}
d’au -- tres dé -- sirs, ni d’au -- tres flam -- mes.
\tag #'(voix2 basse) {
  Dans ces lieux sou -- ter -- rains où je don -- ne la loi,
  vous qui re -- con -- nais -- sez ma puis -- san -- ce su -- prê -- me,
  re -- dou -- blez vos trans -- ports pour plaire à vo -- tre roi ;
  mais fai -- tes en -- cor plus pour plaire à ce que j’ai -- me.
  Re -- dou -- blez vos trans -- ports pour plaire à vo -- tre roi ;
  mais fai -- tes en -- cor plus,
  mais fai -- tes en -- cor plus pour plaire à ce que j’ai -- me.
}
