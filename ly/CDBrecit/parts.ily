\piecePartSpecs
#`((dessus #:score-template "score-2dessus-voix")
   (dessus1 #:score-template "score-voix")
   (dessus2 #:score-template "score-voix" #:tag-notes dessus2-part)
   (basse #:score-template "score-basse-continue-voix"
          #:instrument "[B.C.]")

   (silence #:on-the-fly-markup , #{
\markup\fontsize #-2 \fill-line {
  \column {
    \livretPers Zaïre
    \livretVerse#12 { Que tout ce que je vois rend mon âme interdite ! }
    \livretVerse#12 { Je ne saurais calmer le trouble qui m’agite. }
    \livretPers Adolphe
    \livretVerse#10 { Rassurez-vous, dissipez votre effroi, }
    \livretVerse#12 { Régnez avec Adolphe, en régnant avec moi : }
    \livretVerse#12 { Pouvais-je résister de vous rendre les armes, }
    \livretVerse#12 { Pour la première fois que j’aperçus vos charmes ? }
    \livretVerse#12 { Ce fut dans ce jardin où la mère d’Amour }
    \livretVerse#8 { Semble avoir fixé son empire : }
    \livretVerse#10 { Vous paraissez, Vénus quitte sa cour, }
    \livretVerse#12 { Tout se range vers vous, près de vous tout soupire, }
    \livretVerse#12 { Les oiseaux enchantés vous parlaient de leurs feux ; }
    \livretVerse#8 { Les ruisseaux par leur doux murmure, }
    \livretVerse#8 { Rendaient hommage à vos beaux yeux ; }
    \livretVerse#8 { Et le père de la nature }
    \livretVerse#12 { Pour vous, du plus beau jour faisait briller ces lieux. }
    \livretVerse#10 { Par tant d’attraits, fallait-il me surprendre ? }
    \livretVerse#8 { Quel cœur aurait pu s’en défendre ! }
  }
  \column {
    \livretPers Zaïre
    \livretVerse#12 { Votre amour me soumet tous ces peuples divers, }
    \livretVerse#12 { Et sur vous désormais je règne en souveraine ; }
    \livretVerse#12 { Mon destin le plus beau c’est de porter ma chaîne, }
    \livretVerse#8 { Et de vous voir porter vos fers. }
    \livretPers Ensemble
    \livretVerse#8 { Tendre Amour, enchaîne nos âmes, }
    \livretVerse#8 { C’est toi seul qui fais mon bonheur ; }
    \livretVerse#8 { N’allume jamais dans mon cœur }
    \livretVerse#8 { D’autres désirs, ni d’autres flammes. }
    \livretPers Adolphe
    \livretVerse#12 { Dans ces lieux souterrains où je donne la loi, }
    \livretVerse#12 { Vous qui reconnaissez ma puissance suprême, }
    \livretVerse#12 { Redoublez vos transports pour plaire à votre roi ; }
    \livretVerse#12 { Mais faites encor plus pour plaire à ce que j’aime. }
  }
}#}))
