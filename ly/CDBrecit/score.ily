\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new GrandStaff <<
      \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
      \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix1 \includeNotes "voix"
    >> \keepWithTag #'voix1 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix2 \includeNotes "voix"
    >> \keepWithTag #'voix2 \includeLyrics "paroles"
    \new Staff \with { instrumentName = "[B.C.]" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2 s2.\break s1*3\break s2.*4 s1.\pageBreak
        s1*4 s1.\break s1 s2.*4\pageBreak
        s2.*7\break s2.*7\pageBreak
        s2.*7\break s2.*7\pageBreak
        s2.*6 s2 \bar "" \break s4 s2. s1*3\pageBreak
        s1 s2. s1.\break \grace s8 s1 s2. s2 \bar "" \break \grace s8 s2 s2.*4 s4 \bar "" \pageBreak
        s2 s2.*6\break \grace s8 s2.*7\pageBreak
        s2.*7\break s2.*6\pageBreak
        s2.*7\break s2.*2 s1*2\pageBreak
        s1 s2.*5\break s2.*7\pageBreak
        s2.*6\break s2.*2 s2.
      }
      \ifComplet\modVersion {
        s1*2 s2. s1*2 s1 s2.*4 s1. s1*4 s1. s1 s2.*2 s2.*38 s1*4 s2. s1. s1
        s2. s1 s2.*5 s2 s4 s2.*34\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
