<<
  \tag #'(voix1 basse) {
    \ifComplet {
      \ffclef "vdessus" <>^\markup\character Zaïre
      r8 si' si'16 si' do'' re'' \appoggiatura re''8 mi'' do''16 mi'' la'8 la'16 la' |
      fad'4\trill fad' r16 la' la' la' re''8 fad' |
      sol' do'' la'\trill la' si' do'' |
      si'4\trill si'
    }
    \ffclef "vbasse" <>^\markup\character Adolphe
    \ifConcert r2
    r8 si si re' |
    fad4 re8 re sol4 sol8 fad |
    sol8. si16 sol sol la si do'8 la16 la re'8 re'16 re' |
    si2\trill r8 si |
    red16 red mi fad sol8 la16 si do'8 si16 la |
    \appoggiatura la8 si2 \appoggiatura la8 sol4 |
    si la8 sol fad mi |
    red4.\trill mi8 fad sol la si la4( sol8\trill[ fad]) |
    fad4\trill r r2 |
    r r4 r8 si |
    mi'4 do' la4.(\trill sol16) la |
    si4 fad8 sol la4 sol8 fad |
    \appoggiatura fad8 sol2 sol4. la8 si4 sol |
    do'2 la4 re' |
    si\trill \appoggiatura la8 sol4 r16 sol la si |
    fad8 si mi'4 dod'8 red'16 mi' |
    red'2\trill mi'8 \ficta re' |
    do'([ re']) mi'([ re']) do'([ si]) |
    la2 re'8 do' |
    si([ do']) re'([ do']) si([ la]) |
    si4 \appoggiatura la8 sol4 r |
    r r si8 la |
    sol4 fad\trill mi |
    si2.~ |
    si2 mi'8 re' |
    dod'4 red' mi' |
    si r r |
    r mi'8([ re']) do'([ si]) |
    la4 r r |
    r re'8([ do']) si([ la]) |
    si4( do'8[ si]) la([ sol]) |
    la([ si] la4) fad\trill |
    re'4 mi'8[ re'] do'[ si] |
    \appoggiatura si8 do'2. |
    si4 do'8([ si]) la([ sol]) |
    re'2.~ |
    re'2 la8 re' |
    si2\trill la4 |
    si do'4. re'8 |
    mi'4 \appoggiatura re'8 do'4 mi'4 |
    re'4.\trill do'8 si la |
    re'4. sol8 fad sol |
    do'2( si8) do' |
    la2\trill r4 |
    r r la8 re' |
    si2\trill la4 |
    si4 do'4. re'8 |
    mi'4 \appoggiatura re'8 do'4 mi' |
    \appoggiatura mi'8 re'4. do'8 si la |
    re'4. sol8 fad sol |
    do'8[\melisma si la si do' si16 la] |
    re'4. do'16[ si] la8[ si16 sol]( |
    fad4.)\melismaEnd fad8 sol4~ |
    sol16 la[ sol la] la4.\trill sol8 |
    sol2 r8 si fad la |
    red4 r8 fad16 sol la4 sol8 fad |
    \appoggiatura fad sol2 sol4 r8 do' |
    la4\trill la8 si do'4 do'8 si |
    si4\trill si
    \ffclef "vdessus" <>^\markup\character Zaïre
    r8 si'16 si' |
    \appoggiatura la'8 sol'4 la'8 si' \appoggiatura si' do''4 do''8 si' la'4\trill sol'8 la' |
    \appoggiatura la'8 si'4 r8 si'16 dod'' re''4 si'8 mi'' |
    dod''\trill fad'' red''4\trill mi''8 fad''16 sol'' |
    fad''16\trill fad'' si' do'' sold'8\trill la'16 si' \appoggiatura si'8 do''8. do''16 do'' si' la' \ficta sol' |
    fad'8[\melisma mi' fad' sol' la' fad'] |
    sol'16[ la' sol' la'] la'4.(\trill sol'16[ la']) |
    si'2\melismaEnd si'4 |
    sol''2 fad''8 \appoggiatura mi'' red'' |
    mi'' fad'' fad''4.\trill mi''8 |
    mi''2 mi''8 fad'' |
    red''2\trill sol''4 |
    fad''8[ sol'' fad'' mi'' fad'' red'']( |
    mi''4) fad''4. sol''8 |
    \appoggiatura sol''8 fad''4 si' mi''8 re'' |
    do''2 si'4 |
    \appoggiatura si'8 la'2\trill sol'8 la' |
    \appoggiatura la'8 si'2 r8 sol' |
    do''4 do''4. re''8 |
    \appoggiatura re''8 mi''2 r8 re'' |
    do''4 do''4. re''8 |
    mi''4 fad''4. sol''8 |
    fad''2.\trill |
    re''4 do''4.\trill si'8 |
    mi''4. la'8 si'4~ |
    si'8 do'' la'4.(\trill sol'8) |
    sol'2 r8 si' |
    mi''2 re''8 mi'' |
    dod''2\trill r8 dod'' |
    fad''2 mi''8 fad'' |
    red''4\trill mi'' fad'' |
    sol''2. |
    fad''2 sol''8 red'' |
    mi''4. mi''8 fad'' sol'' |
    sol''4( fad''2)\trill |
    mi''2 r4 |
    r4 r r8 si' |
    mi''2 re''8 mi'' |
    dod''4 red'' mi'' |
    red'' mi'' fad'' |
    sol''2. |
    \appoggiatura sol''8 fad''2\trill sol''8 red'' |
    mi''4. mi''8 fad'' sol'' |
    sol''4( fad''2)\trill |
    mi''2. |
  }
  \tag #'voix2 {    
    \clef "vbasse" \ifComplet { R1*2 R2. }
    R1*2 R1 R2.*4 R1. R1*4 R1. R1 R2.*40 R1*4
    R2. R1. R1 R2. R1 R2.*6 |
    <>^\markup\character Adolphe r4 r mi'8 fad' |
    red'2\trill si4 |
    do'4 si4. la8 |
    si4 si sol8 mi |
    la2 si4 |
    do' do'4. si8 |
    si2 r4 |
    r r r8 sol |
    do'4 do'4. re'8 |
    mi'2.~ |
    mi'8[ re'] do'[ si] la[ sol] |
    re'4. re8 mi fad |
    sol4 sol la8 si |
    do'4. fad8 sol4 |
    do re2 |
    sol,2 r4 |
    r r r8 mi |
    la4 la4. si8 |
    do'4 do' la |
    si4 dod' red' |
    mi'2. |
    red'2 mi'8 si |
    do'4. si8 do' la |
    si4( si,2) |
    mi2 r8 si |
    mi'2 re'8 mi' |
    dod'2\trill r8 mi |
    la4 si \ficta dod' |
    si dod' red' |
    mi'2. |
    \appoggiatura mi'8 red'2\trill mi'8 si |
    do'4. si8 do' la |
    si4( si,2) |
    mi2. |
  }
>>
<<
  \tag #'(voix2 basse) {
    \tag #'basse { \ffclef "vbasse" <>^\markup\character Adolphe }
    r8 si16 re' sol8 re16 re sol,8 re16 re sol8 sol16 la |
    \appoggiatura la8 si2 re'4 re'16 do' si la |
    sol4 sol8 fad mi4 mi8 re |
    la4 la la,8 la, |
    re4 mi fad |
    sol2 sol4 |
    la4. la8 la la |
    si2. |
    sol4 sol8 sol fad mi |
    red2 mi4 |
    do'4. do'8 do' la |
    si2. |
    mi2 mi8 la |
    fad4.\trill mi8 re4 |
    sol2 sol4 |
    do'4. la8 si do' |
    re'2. |
    sol4 sol8 sol la si |
    do'2. |
    la4 la8 la si do' |
    fad2\trill sol4 |
    mi4. mi8 fad sol |
    re2. |
    sol,2.
  }
  \tag #'voix1 { R1*3 R2.*20 r2 r4 }
>>
