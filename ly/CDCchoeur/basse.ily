\clef "basse" r4 |
R1 |
r2 r4 r8 sol |
sol fad sol la si re' do' re' |
sol si la sol fad4. re8 |
sol4 sol,2 sol8 la |
si8 la si do' si4 sol |
re'2~ re'8 re mi fad |
sol4 sol sol mi |
la la la la |
la2. fad4 |
si si dod' re' |
la2 la, |
re4 re, re, re |
sol sol sol sol |
mi mi, mi, mi |
do' si la do' |
si si, si, si |
si si dod' red' |
mi' mi mi mi, |
mi, sol la si |
do'2 la si4 si, |
mi1 |
r4 si sol mi |
si8 la si do' si2 |
r4 re' si sol |
re8 do re mi fad4 re |
sol la si do' |
re'2 re |
mi4 do re re, |
sol, re mi fad |
sol, mi fad sol |
la, fad sol la |
si, sol la si |
do do'8 si la si do' la |
si2 si,4 si |
mi'2 red'4. mi'8 |
si4 si, si, si8 la |
sol4 sol, sol mi |
fad fad, fad re |
mi re dod si, |
lad,2. si,4 |
fad fad, fad re |
sol sol, sol mi |
fad2 fad, |
si,4 si si si |
sol4. fad8 mi4 do |
si, si, si, la, |
sol, mi, r mi |
la si do' la |
si la sol fad |
mi4. red8 mi fad sol mi |
la2 la, |
re,4 re mi fad |
sol2 do re4. re8 |
sol,4 sol fad re |
sol2. si,4 |
do si, la, sol, |
re si, mi do |
re re' re' do' |
si la sol4. sol8 |
re'2. r8 re |
sol4 sol la si |
do'2. dod'8 la |
re'2 re'4 sol |
fad4. mi8 re mi fad re |
sol4 sol, sol4. sol8 |
sol4 re si, re |
sol, sol fad4. re8 |
sol4 sol,2 sol8 la |
si2 si4 sol |
re'2 re'4 re |
sol sol sol mi |
la la, la la |
la la, la fad |
si si dod' re' |
la2 la, |
re2 r | \allowPageTurn
\clef "alto" r2 r4 sol |
do' do' do' si |
la2. la4 |
fa' mi' re' fa' |
mi' mi mi mi |
mi mi' fad'! sold' |
la' la la la |
la do' re' mi' |
fa'2 re' mi'4. mi'8 |
la2. \clef "bass" r8 la |
la2. la,4 |
re2. r8 re' |
re'2. re4 |
sol4 sol8 la si4 sol |
do'4 do2 do'8 si |
la2 fad4 sol |
re re, re, re |
sol la si do' |
re'2. sol4 |
re2. \clef "alto" r8 re' |
re'2. sol4 |
re2 sol |
re'4 re re re'8 mi' |
fad'2 fad'4 re' |
sol'2 sol'4 sol |
do' do' do' la |
re' re re re' |
re'2. si4 |
mi' mi' fad' sol' |
re'2 re |
sol2. \clef "bass" re4 |
sol sol sol si, |
do2. la,4 |
mi mi mi dod |
re2 re,4 r |
r re' si sol |
sol, sol fad re |
sol4. fad8 sol la si sol |
do'4. si8 la si do' la |
re'4 re re do |
si, si la sol |
fad2 sol |
fad sol4 sol, |
re, re mi fad |
sol, mi fad sol |
la, fad sol la |
si, sol la si |
do'2. r8 la |
re'4 re re re' |
re' sol fad re |
sol4. fad8 sol la si sol |
do'4. si8 la si do' la |
re'4 re sol sol, re re, |
sol,1 |
