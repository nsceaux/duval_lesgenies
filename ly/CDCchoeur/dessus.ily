\clef "dessus" r8 re'' |
re''4 si' sol' re' |
re''2 si'4. sol'8 |
si' la' si' do'' re''4 si'8 si' |
si'4 sol'8 sol' la'4 re'' |
si'2\trill sol'4 sol''8 fad'' |
sol''2 sol''4 sol'' |
sol''2 fad''4 re'' |
si' si' si' mi'' |
dod''4.\trill si'8 la' si' dod'' re'' |
mi''2. la''4 |
la'' sol'' sol'' fad'' |
fad''2( mi''4.)\trill re''8 |
re''4 re' re' r |
r2 r4 re'' |
sol'' sol'' sol'' sol'' |
mi''2.\trill fad''4 |
red''\trill red'' red'' mi'' |
fad''2 fad'' |
r4 si' dod'' red'' |
mi''1~ |
mi''2 mi'' red''4.\trill mi''8 |
mi''4 si' sol' mi' |
si'8 la' si' do'' si'2 |
r4 re'' si' sol' |
re''8 do'' re'' mi'' re''4 sol'' |
fad'' mi'' re'' do'' |
si'8 la' si' do'' si'4 sol'' |
fad'' mi'' re'' do'' |
si'2 la'\trill |
sol'4 re'' si' sol' |
si'2 si'4. si'8 |
si'2. la''8 la'' |
la''2 sol''4 sol'' |
sol'' fad''8 sol'' la'' sol'' fad'' mi'' |
red''2\trill red''4 si' |
sol' mi' si' mi'' |
red''2~ red''4. fad''8 |
si'2. si'8 dod'' |
lad'2\trill lad'4 si' |
dod''8 si' lad' si' dod'' re'' dod'' re'' |
mi'' fad'' sol'' fad'' mi'' re'' dod'' si' |
lad'2.\trill fad''4 |
fad'' mi'' mi'' re'' |
re''2( dod''4.)\trill si'8 |
si'4 r r2 |
R1*2 | \allowPageTurn
r4 si' mi'' sol'' |
fad''4.\trill fad''8 fad''4 mi'' |
red''\trill si' dod'' red'' |
mi''4. fad''8 sol'' fad'' mi'' re'' |
dod''4.\trill re''8 mi'' fad'' sol'' la'' |
fad''4.\trill mi''8 re'' do'' si' la' |
si'2 do'' la'4.\trill re''8 |
si'4\trill r r2 |
<>^"Violons" r4 re'' si' sol' |
mi'' re'' do'' si' |
la' re'' sol' do'' |
fad' re' mi' fad' |
sol' la' si'4. sol''8 |
fad''4\trill mi'' re'' do'' |
si'2.\trill do''8 re'' |
mi''2 fad''4 sol'' |
fad''2\trill \appoggiatura mi''8 re''4. re''8 |
re''1 |
re''2 si'4. sol'8 |
re''2. re''8 do'' |
si'2 la'4. re''8 |
si'2\trill sol'4 sol''8 sol'' |
sol''2 sol''4 sol'' |
fad''2.\trill re''4 |
si' si' si' mi'' |
dod''4. si'8 la' si' dod'' re'' |
mi''2. la''4 |
la'' sol'' sol'' fad'' |
fad''2( mi''4.)\trill re''8 |
re''2. \twoVoices #'(dessus1 dessus2 dessus) <<
  { re''4 |
    sol'' sol'' sol'' sol'' |
    mi''2. mi''4 |
    mi''2 mi''4 mi'' |
    do''4. do''8 fa''4 re'' |
    si'2\trill si' |
    << \tag #'dessus si''1\rest \tag #'dessus1 R1 >> |
    r4 mi'' fad'' sold'' |
    la''4. sol''!8 fa'' mi'' re'' do'' |
    si'2 do'' si'4.\trill la'8 | }
  { re''4 |
    si' si' do'' re'' |
    sol'2. sol'4 |
    do''2 do''4 do'' |
    la'4. la'8 si'4 la' |
    sold'2\trill sold' |
    r4 mi'' mi'' re'' |
    do''4. si'8 do'' re'' mi'' re'' |
    do'' re'' do'' si' la' do'' si' la' |
    sold'2 la' sold'4.\trill la'8 | }
>>
la'1 |
r4 la' si' dod'' |
re''2 re'' |
r4 re'' mi'' fad'' |
sol'' sol'' sol'' fa'' |
mi'' sol' la' si' |
do''4. si'8 do'' re'' si' do'' |
la'4.\trill sol'8 la' si' do'' re'' |
si'4 la' sol'4. la'8 |
fad'4.\trill re''8 re''4. sol''8 |
fad''2.\trill \twoVoices #'(dessus1 dessus2 dessus) <<
  { re''8 mi'' |
    fad''2 fad''4 sol'' |
    la'' sol''8 fad'' sol''2 |
    fad''2.\trill sol''4 |
    la'' la'' la'' fad'' |
    re''4. re''8 re''4 sol''8 fad'' |
    mi''2 do''4 mi'' |
    la'4. sol'8 la' si' do'' si' |
    la'2\trill la'4 re'' |
    re'' re'' do'' si' |
    si'2( la'4.\trill) sol'8 |
    sol'2. }
  { fad'8 sol' |
    la'2 la'4 si' |
    do''4 si'8 la' si'2 |
    la'2.\trill re''4 |
    re'' do'' do'' re'' |
    si'4.\trill si'8 si'4 si'8 re'' |
    sol'2 mi'4 la' |
    fad'4. mi'8 fad' sol' la' sol' |
    fad'2\trill fad'4 si' |
    sol' si' la' sol' |
    sol'2( fad'4.)\trill sol'8 |
    sol'2. }
>> re''4 |
si' re'' sol'' re'' |
mi''2. fad''4 |
sol'' sol'' mi'' la'' |
fad''2.\trill <>^"Violons" la'4 |
re'' la' si' re'' |
sol'' re'' fad'' la'' |
re'' re'' si' sol' |
mi''4. re''8 mi'' fad'' sol'' la'' |
fad''4.\trill mi''8 re'' do'' si' la' |
si'4 re'' do'' si' |
la'\trill r r r8 si' |
la'2.\trill r8 re'' |
re''1 |
si'2 si'4. si'8 |
si'2. la'8 la' |
la'2 sol'4 sol' |
sol'2 mi'4 do'' |
la'\trill la' si' do'' |
re''2. re''4 |
re'' re'' si' sol' |
mi''4. re''8 mi'' fad'' sol'' la'' |
fad''2 sol'' fad''4.\trill sol''8 |
sol''1 |
