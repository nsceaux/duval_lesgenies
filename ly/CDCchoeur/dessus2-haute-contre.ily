\clef "haute-contre" r4 |
r2 r4 r8 sol' |
sol'4 sol' re' si |
sol2 sol'4. sol'8 |
sol'4 si'8 si' la'4 fad'8 fad' |
sol'2 sol'4 si' |
si' do'' re'' si' |
la'4.\trill la'8 la'4 fad' |
sol' sol' sol' sol' |
mi'4.\trill re'8 dod' re' mi' fad' |
mi'2.\trill fad'4 |
fad' si' la' la' |
la'2~ la'4. la'8 |
fad'2\trill r |
r2 r4 si' |
si' si' si' si' |
sol'2. sol'4 |
fad' fad' fad' sol' |
fad'2 fad' |
r4 sol' mi' fad' |
sol'1~ |
sol'2 fad' fad'4.\trill sol'8 |
sol'4 r r sol' |
sol'8 fad' sol' la' sol'2 |
r2 r4 si' |
si'8 la' si' do'' si'4 si' |
la' sol' fad' la' |
sol'8 fad' sol' la' sol'4 mi'' |
re'' do'' si' la' |
sol'2 fad'\trill |
sol'4 r r r8 re' |
re'4 re' re'4. sol'8 |
sol'2. r8 sol' |
sol'2 r4 r8 re' |
mi'2 fad' |
fad'2. si'4 |
si'2 si'4. si'8 |
si'2. red'8 red' |
mi'2 mi'4 sol' |
fad'2 fad'4 fad' |
mi' fad' mi' fad' |
sol'2. fad'4 |
fad'2. re''4 |
re'' dod'' dod'' si' |
si'2( lad'4.)\trill si'8 |
si'4 r r2 |
R1*2 | \allowPageTurn
r4 sol' sol' sol' |
do''4. do''8 do''4 do'' |
si' red' mi' fad' |
sol'2 si' |
la' la' |
la'4 fad' sol' la' |
sol'2 sol' fad'4.\trill sol'8 |
sol'4 r r2 |
r4 si' sol' sol' |
sol' sol' fad' sol' |
la' sol' mi' mi' |
re' re' re' re' |
re' fad' sol'4. si'8 |
la'4 sol' fad' la' |
sol'2. sol'8 sol' |
sol'2 la'4 la' |
la'2 \appoggiatura sol'8 fad'4. si'8 |
la'4. sol'8 fad' sol' la' fad' |
sol'2 re'4. re'8 |
re'1 |
re'2 re'4. re'8 |
re'2. si'4 |
si' do'' re'' si' |
la'4. la'8 la'4 fad' |
sol' sol' sol' sol' |
mi'4. re'8 dod' re' mi' fad' |
mi'2. dod''4 |
re'' si' la' la' |
la'2. la'4 |
la'2. re''4 |
si' si' do'' re'' |
sol'2. sol'4 |
do''2 do''4 do'' |
la'4. la'8 si'4 la' |
sold'2\trill sold' |
r4 mi'' mi'' re'' |
do''4. si'8 do'' re'' mi'' re'' |
do'' re'' do'' si' la' do'' si' la' |
sold'2 la' sold'4.\trill la'8 |
la'1 |
r4 dod' re' mi' |
fad'2 fad' |
r4 fad' sol' la' |
si' re'' re'' re'' |
do'' mi' fad' sol' |
la'4. sol'8 la' si' sol' la' |
fad'4.\trill mi'8 fad' sol' la' si' |
sol'4 fad' sol'4. la'8 |
la'4. fad'8 fad'4 re' |
re'2. fad'8 sol' |
la'2 la'4 si' |
do''4 si'8 la' si'2 |
la'2.\trill re''4 |
re'' do'' do'' re'' |
si'4.\trill si'8 si'4 si'8 re'' |
sol'2 mi'4 la' |
fad'4. mi'8 fad' sol' la' sol' |
fad'2\trill fad'4 si' |
sol' si' la' sol' |
sol'2( fad'4.)\trill sol'8 |
sol'2. fad'4 |
sol' sol' si' sol' |
sol'2. fad'4 |
mi' mi' sol' mi' |
re'2. la'4 |
la' la' sol' si' |
re''2 r |
R1 |
r4 sol' mi' do' |
la'4. sol'8 fad' mi' re' do' |
re'4 sol' la' re' |
re'2 re' |
re' re' |
re'4 r r r8 re' |
re'4 re' re'4. sol'8 |
sol'2. mi'8 mi' |
re'2 mi'4 re' |
mi'2 mi'4 mi' |
fad' fad' sol' la' |
si'2 la' |
sol'4 re'' si' sol' |
mi'' sol' do'' mi'' |
re''2 si' la'4.\trill sol'8 |
sol'1 |
