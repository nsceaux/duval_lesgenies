\tag #'vdessus {
  Ré -- gnez dans nos cli -- mats, jou -- ïs -- sez de la gloi -- re
  de fai -- re tri -- om -- pher l’a -- mour,
  de fai -- re tri -- om -- pher, __
  de fai -- re tri -- om -- pher l’a -- mour ;
  vos yeux à chaque ins -- tant aug -- men -- tent sa vic -- toi -- re,
  qu’il vous en -- chaî -- ne à vo -- tre tour.
  
  Ré -- gnez dans nos cli -- mats, jou -- ïs -- sez de la gloi -- re
  de fai -- re tri -- om -- pher l’a -- mour,
  jou -- ïs -- sez de la gloi -- re
  de fai -- re tri -- om -- pher l’a -- mour ;
  qu’il vous en -- chaî -- ne,
  qu’il vous en -- chaîne à vo -- tre tour,
  qu’il vous en -- chaî -- ne à vo -- tre tour.

  Ré -- gnez dans nos cli -- mats, jou -- ïs -- sez de la gloi -- re,
  ré -- gnez dans nos cli -- mats, jou -- ïs -- sez de la gloi -- re
  de fai -- re tri -- om -- pher l’a -- mour,
  de fai -- re tri -- om -- pher, __
  de fai -- re tri -- om -- pher l’a -- mour ;
  vos yeux à chaque ins -- tant aug -- men -- tent,
  aug -- men -- tent sa vic -- toi -- re,
  qu’il vous en -- chaî -- ne à vo -- tre tour.
  
  Qu’il vous en -- chaî -- ne,
  qu’il vous en -- chaîne à vo -- tre tour,
  qu’il vous en -- chaî -- ne à vo -- tre tour.

  Ré -- gnez, ré -- gnez, jou -- ïs -- sez de la gloi -- re
  de fai -- re tri -- om -- pher l’a -- mour,
  jou -- ïs -- sez de la gloi -- re
  de fai -- re tri -- om -- pher l’a -- mour.

  Vos yeux à chaque ins -- tant aug -- men -- tent sa vic -- toi -- re,
  qu’il vous en -- chaî -- ne à vo -- tre tour.
  Ré -- gnez, ré -- gnez dans nos cli -- mats, jou -- ïs -- sez de la gloi -- re
  de fai -- re tri -- om -- pher l’a -- mour,
  qu’il vous en -- chaî -- ne à vo -- tre tour.
}
\tag #'vhaute-contre {
  Ré -- gnez dans nos cli -- mats, jou -- ïs -- sez de la gloi -- re,
  jou -- ïs -- sez de la gloi -- re
  de fai -- re tri -- om -- pher l’a -- mour,
  de fai -- re tri -- om -- pher l’a -- mour ;
  vos yeux à chaque ins -- tant aug -- men -- tent sa vic -- toi -- re,
  qu’il vous en -- chaî -- ne à vo -- tre tour.

  Ré -- gnez dans nos cli -- mats,
  ré -- gnez, ré -- gnez, ré -- gnez dans nos cli -- mats,
  jou -- ïs -- sez de la gloi -- re
  de fai -- re tri -- om -- pher l’a -- mour,
  de fai -- re tri -- om -- pher l’a -- mour ;
  qu’il vous en -- chaîne à vo -- tre tour,
  qu’il vous en -- chaîne à vo -- tre tour,
  qu’il vous en -- chaîne à vo -- tre tour.

  Ré -- gnez dans nos cli -- mats, jou -- ïs -- sez de la gloi -- re,
  ré -- gnez dans nos cli -- mats,
  dans nos cli -- mats, jou -- ïs -- sez de la gloi -- re
  de fai -- re tri -- om -- pher, __
  de fai -- re tri -- om -- pher l’a -- mour ;
  %% Trio
  \ifComplet {
    vos yeux à chaque ins -- tant aug -- men -- tent sa vic -- toi -- re,
    qu’il vous en -- chaî -- ne,
    qu’il vous en -- chaîne à vo -- tre tour.
  }
  \ifConcert {
    vos yeux à chaque ins -- tant aug -- men -- tent,
    aug -- men -- tent sa vic -- toi -- re,
    qu’il vous en -- chaî -- ne à vo -- tre tour.
  }

  Qu’il vous en -- chaî -- ne,
  qu’il vous en -- chaîne à vo -- tre tour,
  qu’il vous en -- chaî -- ne à vo -- tre tour.
  %% Trio
  \ifComplet {
    Ré -- gnez dans nos cli -- mats, jou -- ïs -- sez de la gloi -- re
    de fai -- re tri -- om -- pher l’a -- mour,
    de fai -- re tri -- om -- pher l’a -- mour.
  }
  \ifConcert {
    jou -- ïs -- sez de la gloi -- re
    de fai -- re tri -- om -- pher l’a -- mour,
    jou -- ïs -- sez de la gloi -- re
    de fai -- re tri -- om -- pher l’a -- mour.
  }
  Vos yeux à chaque ins -- tant aug -- men -- tent sa vic -- toi -- re,
  qu’il vous en -- chaî -- ne à vo -- tre tour.
  Ré -- gnez dans nos cli -- mats, jou -- ïs -- sez de la gloi -- re
  de fai -- re tri -- om -- pher l’a -- mour,
  qu’il vous en -- chaî -- ne à vo -- tre tour.
}
\tag #'vtaille {
  Ré -- gnez dans nos cli -- mats,
  jou -- ïs -- sez de la gloi -- re
  de fai -- re tri -- om -- pher l’a -- mour,
  de fai -- re tri -- om -- pher l’a -- mour,
  de fai -- re tri -- om -- pher l’a -- mour ;
  vos yeux aug -- men -- tent sa vic -- toi -- re,
  sa vic -- toi -- re,
  qu’il vous en -- chaî -- ne à vo -- tre tour.

  Qu’il vous en -- chaîne à vo -- tre tour,
  qu’il vous en -- chaîne à vo -- tre tour.
  Ré -- gnez dans nos cli -- mats,
  jou -- ïs -- sez, jou -- ïs -- sez de la gloi -- re
  de fai -- re tri -- om -- pher l’a -- mour ;
  qu’il vous en -- chaîne à vo -- tre tour,
  qu’il vous en -- chaî -- ne,
  qu’il vous en -- chaîne à vo -- tre tour.
  
  Ré -- gnez, ré -- gnez, jou -- ïs -- sez, jou -- ïs -- sez de la gloi -- re,
  ré -- gnez, jou -- ïs -- sez de la gloi -- re,
  jou -- ïs -- sez de la gloi -- re
  de fai -- re tri -- om -- pher l’a -- mour,
  de fai -- re tri -- om -- pher l’a -- mour ;

  \ifConcert {
    vos yeux à chaque ins -- tant aug -- men -- tent sa vic -- toi -- re,
    qu’il vous en -- chaî -- ne,
    qu’il vous en -- chaîne à vo -- tre tour.
  }
  Qu’il vous en -- chaî -- ne,
  qu’il vous en -- chaîne à vo -- tre tour,
  jou -- ïs -- sez de la gloi -- re
  de fai -- re tri -- om -- pher l’a -- mour.
  \ifConcert {
    Ré -- gnez dans nos cli -- mats, jou -- ïs -- sez de la gloi -- re
    de fai -- re tri -- om -- pher l’a -- mour,
    de fai -- re tri -- om -- pher l’a -- mour.
  }
  Vos yeux à chaque ins -- tant aug -- men -- tent sa vic -- toi -- re,
  qu’il vous en -- chaî -- ne à vo -- tre tour.
  Qu’il vous en -- chaî -- ne à vo -- tre tour,
  qu’il vous en -- chaî -- ne à vo -- tre tour,
  qu’il vous en -- chaî -- ne à vo -- tre tour.
}
\tag #'vbasse {
  Ré -- gnez dans nos cli -- mats, jou -- ïs -- sez de la gloi -- re
  de fai -- re tri -- om -- pher l’a -- mour,
  de fai -- re tri -- om -- pher l’a -- mour ;
  vos yeux à chaque ins -- tant aug -- men -- tent sa vic -- toi -- re,
  qu’il vous en -- chaî -- ne,
  qu’il vous en -- chaîne à vo -- tre tour.
  
  Qu’il vous en -- chaîne à vo -- tre tour,
  qu’il vous en -- chaîne à vo -- tre tour.
  Ré -- gnez dans nos cli -- mats, jou -- ïs -- sez de la gloi -- re
  de fai -- re tri -- om -- pher l’a -- mour,
  de fai -- re tri -- om -- pher l’a -- mour ;
  qu’il vous en -- chaîne à vo -- tre tour,
  qu’il vous en -- chaî -- ne,
  qu’il vous en -- chaîne à vo -- tre tour.
  
  Ré -- gnez, ré -- gnez dans nos cli -- mats, jou -- ïs -- sez de la gloi -- re,
  ré -- gnez dans nos cli -- mats, jou -- ïs -- sez de la gloi -- re
  de fai -- re tri -- om -- pher l’a -- mour,
  de fai -- re tri -- om -- pher l’a -- mour ;
  
  Ré -- gnez, ré -- gnez dans nos cli -- mats, jou -- ïs -- sez de la gloi -- re
  de fai -- re tri -- om -- pher l’a -- mour ;

  Vos yeux à chaque ins -- tant aug -- men -- tent sa vic -- toi -- re,
  qu’il vous en -- chaî -- ne,
  qu’il vous en -- chaîne à vo -- tre tour,
  qu’il vous en -- chaîne à vo -- tre tour,
  qu’il vous en -- chaîne à vo -- tre tour,
  ré -- gnez, __
  qu’il vous en -- chaî -- ne à vo -- tre tour.
}
