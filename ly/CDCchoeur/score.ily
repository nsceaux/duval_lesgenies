\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with {
        \override VerticalAxisGroup.remove-empty = #(eqv? (ly:get-option 'urtext) #t)
        \override VerticalAxisGroup.remove-first = #(eqv? (ly:get-option 'urtext) #t)
        instrumentName = \markup\center-column { [Violons, Hautbois] }
      } << \global \keepWithTag #'dessus \includeNotes "dessus" >>
      \ifFull\new Staff \with {
        instrumentName = \markup\center-column { [Hautes-contre] }
      } << \global \includeNotes "haute-contre" >>
      \ifFull\new Staff \with { instrumentName = "[Tailles]" } <<
        \global \includeNotes "taille"
      >>
    >>
    \new ChoirStaff <<
      \new Staff = "vdessus" \withLyrics <<
        \ifComplet\instrumentName "[Dessus]"
        \ifConcert\instrumentName "[Dessus I]"
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \with {
        \override VerticalAxisGroup.remove-empty = #(eqv? (ly:get-option 'urtext) #t)
        \override VerticalAxisGroup.remove-first = #(eqv? (ly:get-option 'urtext) #t)
      } \withLyrics <<
        \ifComplet\instrumentName\markup\center-column { [Hautes-contre] }
        \ifConcert\instrumentName "[Dessus II]"
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \ifFull\new Staff \withLyrics <<
        \ifComplet\instrumentName "[Tailles]"
        \ifConcert\instrumentName\markup\center-column { [Haute- contre] }
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \ifComplet\instrumentName "[Basses]"
        \ifConcert\instrumentName "[Basse]"
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses, Bassons et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s1*4\pageBreak
        s1*7\break s1*7\pageBreak
        s1*2 s1. s1*3\break s1*8\pageBreak
        s1*6\break s1*6\pageBreak
        s1*5\break s1*4 s1. s1\pageBreak
        s1*7\break s1*6\pageBreak
        s1*6\break s1*5\pageBreak
        s1*5\break s1 s1. s1*3\pageBreak
        s1*5\break s1*6\pageBreak
        s1*5 s2 \bar "" \break s2 s1*5\pageBreak
        s1*6 s2 \bar "" \break s2 s1*5\pageBreak
        s1*5\break s1 s1. s1
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
