\clef "taille" r4 |
r2 r4 r8 si |
si1 |
re'2 re'4. re'8 |
re'4 si8 si do'4 la8 re' |
re'2 re'4 re' |
re' do' si re' |
re'4. re'8 re'4 re' |
re' re' re' sol |
la2. re'4 |
dod'2.\trill dod'4 |
re' re' mi' re' |
re'2( dod'4.)\trill re'8 |
re'2 r |
r r4 re' |
mi'2. mi'4 |
mi' re' do' mi' |
fad' fad' fad' mi' |
red'2\trill red' |
r4 si si si |
si si do' re' |
mi'2 do' si4. si8 |
si4 r r si |
si1 |
r2 r4 re' |
re'2. re'4 |
fad' sol' la' re' |
re'2. la'4 |
la' re' mi' fad' |
sol' mi' re' do' |
si la sol la |
si sol la si |
do' la si do' |
re' si do' re' |
do'2 do' |
si2. red'4 |
mi' sol' fad'4. sol'8 |
fad'2.\trill si8 si |
si2. mi'8 mi' |
dod'2\trill dod'4 si |
lad4. sold8 lad si lad si |
dod' re' mi' fad' sol' fad' mi' re' |
dod'2.\trill si4 |
si dod' dod' re' |
fad'2. mi'4 |
red'\trill r r2 |
R1*2 | \allowPageTurn
r4 mi' mi' mi' |
mi'4. mi'8 mi'4 mi' |
si red' mi' fad' |
sol' si si mi' |
mi'2 \appoggiatura re'8 dod'2 |
r4 fad' mi' re' |
re'2 mi' re'4. re'8 |
re'4 r r2 |
r4 si' sol' re' |
do' re' re' re' |
fad' sol' si la |
la re' re' re' |
re' do' si4. re'8 |
re'2. r8 re' |
re'2. mi'8 re' |
do'2. mi'8 mi' |
fad'2. r4 |
r2 r4 r8 do'' |
si'4 si' sol'4. si8 |
si2. si8 la |
sol2 la4. fad8 |
sol2. re'8 re' |
re'2 re'4 re' |
re'2 re'4 re' |
re' re' re' re' |
dod'2. mi'4 |
dod'2. fad'4 |
fad' mi' mi' re' |
re'2( dod'4.)\trill re'8 |
re'2. r4 |
r2 r4 sol |
do' do' do' si |
la2. la4 |
fa' mi' re' fa' |
mi'2 mi' |
r4 mi' fad'! sold' |
la'2 la' |
r4 do' re' mi' |
fa'2 re' mi'4. mi'8 |
la'1 |
r4 mi' re' la |
re'2 re' |
r4 la' sol' re' |
sol' sol' sol' sol' |
sol' r r mi'8 mi' |
mi'2 re'4 re' |
re'2 re'4 fad' |
re' re' re' mi' |
re'4. la8 la4 si |
la2. r8 re' |
re'2. sol4 |
re'2 si4. sol8 |
re'2. re'8 mi' |
fad'2 fad'4 re' |
sol'2 sol'4 sol |
do' do' do' la |
re'2. re'4 |
re'2. si4 |
mi' mi' fad' sol' |
re'2. re'4 |
sol2. fad'4 |
sol' sol' si' sol' |
do'2. re'4 |
si si si la |
la2. sol'4 |
fad' fad' re' re' |
si2\trill r |
R1*2 |
r4 mi' mi' re' |
si si do' sol |
la2 re' |
re' re' |
re'4 la sol la |
si sol la si |
do' la si do' |
re' si do' re' |
do'2 mi'4 mi' |
fad' fad' sol' la' |
si' si do' do' |
si4. la8 si do' re' si |
do'4. re'8 mi'2 |
re' re' re'4. re'8 |
si1\trill |
