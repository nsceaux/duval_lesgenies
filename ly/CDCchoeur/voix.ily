<<
  \tag #'vdessus {
    \clef "vdessus" r8 re'' |
    re''1 |
    re''2 si'4. sol'8 |
    re''2. si'8 si' |
    si'2 la'4 re'' |
    si'2\trill sol'4 si' |
    si' do'' re'' si' |
    la'4.\trill la'8 la'4 re'' |
    si' si' si' mi'' |
    dod''4.\trill\melisma si'8 la'[ si' dod'' re'']( |
    mi''2.)\melismaEnd la''4 |
    la'' sol'' sol'' fad'' |
    fad''2( mi''4.)\trill re''8 |
    re''2 r |
    r r4 re'' |
    sol'' sol'' sol'' sol'' |
    mi''2.\trill fad''4 |
    red''\trill red'' red'' mi'' |
    fad''2 fad'' |
    r4 si' dod'' red'' |
    mi''1~ |
    mi''2 mi'' red''4.\trill mi''8 |
    mi''4 r r2 |
    R1*6 |
    r2 r4 r8 re'' |
    re''1 |
    si'2 si'4. si'8 |
    si'2. la'8 la' |
    la'2 sol'4 sol' |
    sol'\melisma fad'8[ sol'] la'[ sol' fad' mi']( |
    red'2)\trill\melismaEnd red'4 si' |
    sol' mi' si' mi'' |
    red''2~ red''4. fad''8 |
    si'2. si'8 dod'' |
    lad'2\trill lad'4 si' |
    dod''8[\melisma si' lad' si'] dod''[ re'' dod'' re''] |
    mi''[ fad'' sol'' fad''] mi''[ re'' dod'' si']( |
    lad'2)\trill\melismaEnd lad'4 fad'' |
    fad'' mi'' mi'' re'' |
    re''2( dod''4.)\trill si'8 |
    si'4 fad'' red'' si' |
    mi''4.\melisma red''8 mi''[ sol'' fad'' mi'']( |
    red''2)\trill\melismaEnd \appoggiatura dod''8 si'2 |
    r4 si' mi'' sol'' |
    fad''4.\trill fad''8 fad''4 mi'' |
    red''\trill si' dod'' red'' |
    mi''4.\melisma fad''8 sol''[ fad'' mi'' re''] |
    dod''4.\trill re''8 mi''[ fad'' sol'' la''] |
    fad''4.\trill mi''8 re''[ do'' si' la']( |
    si'2)\melismaEnd do'' la'4.\trill re''8 |
    si'4\trill r r2 |
    R1*4 |
    r2 r4 r8 sol'' |
    fad''4\trill mi'' re'' do'' |
    si'2.\trill do''8 re'' |
    mi''2 fad''4 sol'' |
    fad''2\trill \appoggiatura mi''8 re''4. re''8 |
    re''1 |
    re''2 si'4. sol'8 |
    re''2. re''8 do'' |
    si'2 la'4. re''8 |
    si'2\trill sol'4 si' |
    si' do'' re'' si' |
    la'4. la'8 la'4 re'' |
    si' si' si' mi'' |
    dod''4.\melisma si'8 la'[ si' dod'' re'']( |
    mi''2.)\melismaEnd la''4 |
    la'' sol'' sol'' fad'' |
    fad''2( mi''4.\trill) re''8 |
    re''2. <<
      \ifComplet {
        <<
          \new Voice \with { autoBeaming = ##f } {
            \voiceOne re''4 |
            sol'' sol'' sol'' sol'' |
            mi''2. mi''4 |
            mi''2 mi''4 mi'' |
            do''4. do''8 fa''4 re'' |
            si'2\trill si' |
          }
          { \voiceTwo re''4 |
            si' si' do'' re'' |
            sol'2. sol'4 |
            do''2 do''4 do'' |
            la'4. la'8 si'4 la' |
            sold'2 sold' | }
        >>
        <<
          \new Voice = "dessus1" \with { autoBeaming = ##f } {
            \voiceOne sol''1\rest |
            r4 mi'' fad'' sold'' |
            la''4.\melisma sol''!8 fa''[ mi'' re'' do'']( |
            si'2)\melismaEnd do'' si'4.\trill la'8 |
            la'1
          }
          \new Lyrics \with { alignAboveContext = "vdessus" } \lyricsto "dessus1" {
            qu’il vous en -- chaî -- ne à vo -- tre tour.
          }
          { \voiceTwo sol'4\rest mi'' mi'' re'' |
            do''4.\melisma si'8 do''[ re'' mi'' re''] |
            do''[ re'' do'' si'] la'[ do'' si' la']( |
            sold'2)\melismaEnd la' sold'4.\trill la'8 |
            \hideNotes la'1 \oneVoice\unHideNotes | }
        >>
      }
      \ifConcert {
        re''4 |
        sol'' sol'' sol'' sol'' |
        mi''2. mi''4 |
        mi''2 mi''4 mi'' |
        do''4. do''8 fa''4 re'' |
        si'2\trill si' |
        R1 |
        r4 mi'' fad'' sold'' |
        la''4.\melisma sol''!8 fa''[ mi'' re'' do'']( |
        si'2)\melismaEnd do'' si'4.\trill la'8 |
        la'1
      }
    >>
    r4 la' si' dod'' |
    re''2 re'' |
    r4 re'' mi'' fad'' |
    sol'' sol'' sol'' fa'' |
    mi'' sol' la' si' |
    do''4.\melisma si'8 do''[ re'' si' do''] |
    la'4.\trill sol'8 la'[ si' do'' re'']( |
    si'4)\melismaEnd la' sol'4. la'8 |
    fad'4.\trill re''8 re''4. sol''8 |
    fad''2.\trill <<
      \ifComplet <<
        { \voiceOne re''8 mi'' |
          fad''2 fad''4 sol'' |
          la''\melisma sol''8[ fad''] sol''2\melismaEnd |
          fad''2.\trill sol''4 |
          la''4 la'' la'' fad'' |
          re''4. re''8 re''4 sol''8 fad'' |
          mi''2 do''4 mi'' |
          la'4.\melisma sol'8 la'[ si' do'' si'] |
          la'2\trill\melismaEnd la'4 re'' |
          re'' re'' do'' si' |
          si'2( la'4.)\trill sol'8 |
          sol'2. \oneVoice
        }
        \new Voice \with { autoBeaming = ##f } {
          \voiceTwo fad'8 sol' |
          la'2 la'4 si' |
          do''4 si'8[ la'] si'2 |
          la'2.\trill re''4 |
          re''4 do'' do'' re'' |
          si'4.\trill si'8 si'4 si'8 re'' |
          sol'2 mi'4 la' |
          fad'4. mi'8 fad'[ sol' la' sol'] |
          fad'2\trill fad'4 si' |
          sol' si' la' sol' |
          sol'2( fad'4.)\trill sol'8 |
          sol'2.
        }
      >>
      \ifConcert {
        re''8 mi'' |
        fad''2 fad''4 sol'' |
        la''\melisma sol''8[ fad''] sol''2\melismaEnd |
        fad''2.\trill sol''4 |
        la''4 la'' la'' fad'' |
        re''4. re''8 re''4 sol''8 fad'' |
        mi''2 do''4 mi'' |
        la'4.\melisma sol'8 la'[ si' do'' si'] |
        la'2\trill\melismaEnd la'4 re'' |
        re'' re'' do'' si' |
        si'2( la'4.)\trill sol'8 |
        sol'2.
      }
    >> <>^"Tous" re''4 |
    si' re'' sol'' re'' |
    mi''2. fad''4 |
    sol'' sol'' mi'' la'' |
    fad''2\trill fad'' |
    R1*2 |
    r4 re'' si' sol' |
    mi''4.\melisma re''8 mi''[ fad'' sol'' la''] |
    fad''4.\trill mi''8 re''[ do'' si' la']( |
    si'4)\melismaEnd re'' do'' si' |
    la'\trill r r r8 si' |
    la'2.\trill r8 re'' |
    re''1 |
    si'2 si'4. si'8 |
    si'2. la'8 la' |
    la'2 sol'4 sol' |
    sol'2 mi'4 do'' |
    la'\trill la' si' do'' |
    re''2. re''4 |
    re'' re'' si' sol' |
    mi''4.\melisma re''8 mi''[ fad'' sol'' la'']( |
    fad''2)\melismaEnd sol'' fad''4.\trill sol''8 |
    sol''1 |
  }
  \tag #'vhaute-contre {
    \ifComplet\clef "vhaute-contre"
    \ifConcert\clef "petrucci-c3/treble"
    r4 |
    r2 r4 r8 sol' |
    sol'1 |
    sol'2 sol'4. sol'8 |
    sol'4 sol'8 sol' la'4 fad'8 fad' |
    sol'2 sol'4 sol'8 fad' |
    sol'2 sol'4 sol' |
    sol'2 fad'4 fad' |
    sol' sol' sol' sol' |
    mi'2. fad'4 |
    mi'2.\trill fad'4 |
    fad' si' la' la' |
    la'2~ la'4. la'8 |
    fad'2\trill r |
    r2 r4 si' |
    si' si' si' si' |
    sol'2. sol'4 |
    fad' fad' fad' sol' |
    fad'2 fad' |
    r4 sol' mi' fad' |
    sol'1~ |
    sol'2 fad' fad'4.\trill sol'8 |
    sol'4 r4 r2 |
    R1*7 |
    r2 r4 r8 re' |
    re'4 re' re'4. sol'8 |
    sol'2. r8 fad' |
    fad'2 r4 r8 re' |
    mi'2 fad' |
    fad'1 |
    si'2 si'4. si'8 |
    si'2. red'8 red' |
    mi'2 mi'4 sol' |
    fad'2 fad'4 fad' |
    mi' fad' mi' fad' |
    sol'2. fad'4 |
    fad'2. <<
      \ifComplet {
        re'4 |
        re' dod' dod' re' |
        fad'2. mi'4 |
        red'4\trill
      }
      \ifConcert {
        re''4 |
        re'' dod'' dod'' si' |
        si'2( lad'4.)\trill si'8 |
        si'4
      }
    >> r4 r2 |
    R1*2 |
    r4 <<
      \ifComplet {
        sol'4 mi' mi' |
        mi'4. mi'8 mi'4 mi' |
        si
      }
      \ifConcert {
        sol'4 sol' sol' |
        do''4. do''8 do''4 do'' |
        si'
      }
    >> red'4 mi' fad' |
    sol'2 si' |
    la' la' |
    la'4 fad' sol' la' |
    sol'2 sol' fad'4.\trill sol'8 |
    sol'4 r4 r2 |
    R1*4 |
    r2 r4 r8 si'8 |
    la'4 sol' fad' la' |
    sol'2. sol'8 sol' |
    sol'2 la'4 la' |
    la'2 \appoggiatura sol'8 fad'4 r |
    r2 r4 r8 do'' |
    si'4 si' re'4. re'8 |
    re'1 |
    re'2 re'4. re'8 |
    re'2. sol'8 sol' |
    sol'2 sol'4 sol' |
    fad'2\trill fad'4 fad' |
    sol' sol' sol' sol' |
    mi'4.\melisma re'8 dod'[ re' mi' fad']( |
    mi'2.)\melismaEnd <<
      \ifComplet {
        fad'4 |
        fad' mi' mi' re' |
        re'2( dod'4.)\trill re'8 |
        re'2.
      }
      \ifConcert {
        dod''4 |
        re'' si' la' la' |
        la'2. la'4 |
        la'2.
      }
    >>
    %% Trio
    <<
      \ifComplet {
        r4 |
        r2 r4 sol |
        do' do' do' si |
        la2. la4 |
        fa' mi' re' fa' |
        mi'2 mi' |
        r4 mi' fad' sold' |
        la'2 la' |
        r4 do' re' mi' |
        fa'2 re' mi'4. mi'8 |
        la1 |
      }
      \ifConcert {
        re''4 |
        si' si' do'' re'' |
        sol'2. sol'4 |
        do''2 do''4 do'' |
        la'4. la'8 si'4 la' |
        sold'2 sold' |
        r4 mi'' mi'' re'' |
        do''4.\melisma si'8 do''[ re'' mi'' re''] |
        do''[ re'' do'' si'] la'[ do'' si' la']( |
        sold'2)\melismaEnd la' sold'4.\trill la'8 |
        la'1 |
      }
    >>
    %%
    r4 dod' re' mi' |
    fad'2 fad' |
    r4 <<
      \ifComplet {
        la'4 sol' re' |
        re' sol' sol' sol' |
        sol'
      }
      \ifConcert {
        fad'4 sol' la' |
        si' re'' re'' re'' |
        do''
      }
    >> mi'4 fad' sol' |
    la'4.\melisma sol'8 la'[ si' sol' la'] |
    fad'4.\trill mi'8 fad'[ sol' la' si']( |
    sol'4)\melismaEnd fad' sol'4. la'8 |
    la'4. r8 r2 |
    r2 r4
    %% Trio
    <<
      \ifComplet {
        r8 re' |
        re'1 |
        re'2 si4. sol8 |
        re'2. re'8 mi' |
        fad'2 fad'4 re' |
        sol'2 sol'4 sol |
        do' do' do' la |
        re'2. re'4 |
        re'2. si4 |
        mi' mi' fad' sol' |
        re'2. re'4 |
        sol2.
      }
      \ifConcert {
        fad'8 sol' |
        la'2 la'4 si' |
        do''4\melisma si'8[ la'] si'2\melismaEnd |
        la'2.\trill re''4 |
        re''4 do'' do'' re'' |
        si'4.\trill si'8 si'4 si'8 re'' |
        sol'2 mi'4 la' |
        fad'4.\melisma mi'8 fad'[ sol' la' sol'] |
        fad'2\trill\melismaEnd fad'4 si' |
        sol' si' la' sol' |
        sol'2( fad'4.)\trill sol'8 |
        sol'2.
      }
    >> fad'4 |
    sol' sol' si' sol' |
    sol'2. fad'4 |
    mi' mi' sol' mi' |
    re'2 re' |
    R1*3 |
    r4 sol' mi' do' |
    la'4.\melisma sol'8 fad'[ mi' re' do']( |
    re'4)\melismaEnd  sol' la' re' |
    re' r r2 |
    R1 |
    r2 r4 r8 re' |
    re'4 re' re'4. sol'8 |
    sol'2. mi'8 mi' |
    re'2 mi'4 re' |
    mi'2 mi'4 mi' |
    fad' fad' sol' la' |
    si'2 la' |
    sol'4 r r2 |
    r4 <<
      \ifComplet {
        sol'4 do' mi' |
        re'2 re' re'4. re'8 |
        si1\trill |
      }
      \ifConcert {
        sol'4 do'' mi'' |
        re''2 si' la'4.\trill sol'8 |
        sol'1 |
      }
    >>
  }
  \tag #'vtaille {
    \clef "vtaille" r4 |
    r2 r4 r8 si |
    si1 |
    si2 re'4. re'8 |
    re'4 si8 si do'4 la8 re' |
    re'2 re'4 re' |
    re' do' si re' |
    re'4. re'8 re'4 re' |
    re' re' re' sol |
    la2. re'4 |
    dod'2.\trill dod'4 |
    re' re' mi' re' |
    re'2( dod'4.)\trill re'8 |
    re'2 r |
    r r4 re' |
    mi'2. mi'4 |
    mi' re' do' mi' |
    fad' fad' fad' mi' |
    red'2\trill red' |
    R1 |
    r4 si do' re' |
    mi'2 do' si4. si8 |
    si4 r4 r2 |
    R1*7 |
    r4 fad sol la |
    si sol la si |
    do' la si do' |
    re' si do' re' |
    do'2 do' |
    si1 |
    sol'2 fad'4. sol'8 |
    fad'2.\trill si8 si |
    si2. mi'8 mi' |
    dod'2\trill dod'4 si |
    lad8[\melisma sold fad sold] lad[ si lad si] |
    dod'[ re' mi' fad'] sol'[ fad' mi' re']( |
    dod'2)\trill\melismaEnd dod'4 <<
      \ifComplet {
        si4 |
        si dod' dod' si |
        si2( lad4.)\trill si8 |
        si4
      }
      \ifConcert {
        si4 |
        si dod' dod' re' |
        fad'2. mi'4 |
        red'\trill
      }
    >> r4 r2 |
    R1*2 |
    r4 <<
      \ifComplet {
        si4 si si |
        do'4. do'8 do'4 do' |
        si
      }
      \ifConcert {
        mi'4 mi' mi' |
        mi'4. mi'8 mi'4 mi' |
        si
      }
    >> r4 r2 |
    r4 si si mi' |
    mi'2 \appoggiatura re'8 dod'2 |
    r4 fad' mi' re' |
    re'2 mi' re'4. re'8 |
    re'4 r4 r2 |
    R1*4 |
    r2 r4 r8 re'8 |
    re'2. r8 re' |
    re'2. mi'8 re' |
    do'2. mi'8 mi' |
    fad'2 re'4. si8 |
    la4.\melisma sol8 fad[ sol la fad]( |
    sol2)\melismaEnd sol4. si8 |
    si2. si8 la |
    sol2 la4. fad8 |
    sol2 sol4 re'8 re' |
    re'2 re'4 re' |
    re'2 re'4 re' |
    re' re' re' re' |
    dod'2. re'4 |
    dod'2. <<
      \ifComplet {
        dod'4 |
        re' si la la |
        la2. la4 |
        fad2.\trill
      }
      \ifConcert {
        fad'4 |
        fad' mi' mi' re' |
        re'2( dod'4.)\trill re'8 |
        re'2.
      }
    >> r4 |
    %% Trio
    <<
      \ifComplet { R1*8 R1. R1 | }
      \ifConcert {
        r2 r4 sol |
        do' do' do' si |
        la2. la4 |
        fa' mi' re' fa' |
        mi'2 mi' |
        r4 mi' fad' sold' |
        la'2 la' |
        r4 do' re' mi' |
        fa'2 re' mi'4. mi'8 |
        la1 |
      }
    >>
    %%
    r4 mi' re' la |
    re'2 re' |
    r4 <<
      \ifComplet {
        fad4 sol la |
        sol re' re' re' |
        do'
      }
      \ifConcert {
        la'4 sol' re' |
        sol' sol' sol' sol' |
        sol'
      }
    >> r4 r mi'8 mi' |
    mi'2 re'4 re' |
    re'2 re'4 fad' |
    re' re' re' mi' |
    re'2. si4 |
    la2.
    %% Trio
    <<
      \ifComplet { r4 | R1*10 | r2 r4 }
      \ifConcert {
        r8 re' |
        re'1 |
        re'2 si4. sol8 |
        re'2. re'8 mi' |
        fad'2 fad'4 re' |
        sol'2 sol'4 sol |
        do' do' do' la |
        re'2. re'4 |
        re'2. si4 |
        mi' mi' fad' sol' |
        re'2. re'4 |
        sol2.
      }
    >> re'4 |
    re' re' re' re' |
    do'2. re'4 |
    si si si la |
    la2 la |
    R1*4 |
    r4 re' re' do' |
    si4 si do' sol |
    la r r2 |
    R1 |
    r4 fad sol la |
    si sol la si |
    do' la si do' |
    re' si do' re' |
    do'2. r4 |
    R1 |
    r4 si do' do' |
    si4.\melisma la8 si[ do' re' si] |
    do'4. re'8 mi'2( |
    re')\melismaEnd <<
      \ifComplet { si2 la4.\trill sol8 | sol1 | }
      \ifConcert { re'4 re'4. re'8 | si1\trill | }
    >>
  }
  \tag #'vbasse {
    \clef "vbasse" r4 |
    R1 |
    r2 r4 r8 sol |
    sol1 |
    sol2 fad4. re8 |
    sol2. sol8 la |
    si2 si4 sol |
    re'2 re'4 re |
    sol4 sol sol mi |
    la2. la4 |
    la2. fad4 |
    si si dod' re' |
    la2~ la4. la8 |
    re2. re4 |
    sol sol sol sol |
    mi2. mi4 |
    do' si la do' |
    si2 si |
    r4 si dod' red' |
    mi'2 mi' |
    r4 sol la si |
    do'2 la si4. si8 |
    mi4 r r2 |
    R1*7 |
    r4 re mi fad |
    sol mi fad sol |
    la fad sol la |
    si4 sol la si |
    do'2. r8 la |
    si1 |
    mi'2 red'4. mi'8 |
    si2. si8 la |
    sol2 sol4 mi |
    fad2 fad4 re |
    mi re dod si, |
    lad,2. si,4 |
    fad2. re4 |
    sol sol sol mi |
    fad2. fad4 |
    si,4 r r2 |
    R1*2 |
    r4 si sol mi |
    la si do' la |
    si la sol fad |
    mi4.\melisma red8 mi[ fad sol mi]( |
    la2)\melismaEnd la |
    r4 re mi fad |
    sol2 do re4. re8 |
    sol,4 r r2 |
    R1*4 |
    r2 r4 r8 sol |
    re'2. r8 re' |
    sol4 sol la si |
    do'2. dod'8 la |
    re'2 re'4. sol8 |
    fad4.\melisma mi8 re[ mi fad re]( |
    sol2)\melismaEnd sol4. sol8 |
    sol1 |
    sol2 fad4. re8 |
    sol2. sol8 la |
    si2 si4 sol |
    re'2 re'4 re |
    sol sol sol mi la2. la4 |
    la2. fad4 |
    si si dod' re' |
    la2. la4 |
    re2 r |
    R1*8 R1. |
    r2 r4 r8 la |
    la1 |
    r2 r4 r8 re' |
    re'1 |
    si2 si4. sol8 |
    do'2. do'8 si |
    la2 fad4 sol |
    re2 re4 re |
    sol la si do' |
    re'2. sol4 |
    re2. r4 |
    R1*10 |
    r2 r4 re |
    sol sol sol si, |
    do2. la,4 |
    mi mi mi dod |
    re2 re |
    R1 |
    r4 sol fad re |
    sol4.\melisma fad8 sol[ la si sol] |
    do'4. si8 la[ si do' la]( |
    re'2)\melismaEnd re' |
    r4 si la sol |
    fad2 sol |
    fad sol |
    re'4 re mi fad |
    sol mi fad sol |
    la fad sol la |
    si sol la si |
    do'2. r8 la |
    re'1~ |
    re'4 sol fad re |
    sol4.\melisma fad8 sol[ la si sol] |
    do'4. si8 la[ si do' la]( |
    re'2)\melismaEnd sol re4. re8 |
    sol,1 |
  }
>>
