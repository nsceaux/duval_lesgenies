\clef "basse" sol,1 |
la4~ la16 sol la sib do'4 do' |
re1 |
sib4~ sib16 la sib do' re'4 sib |
mib re mib do |
re re,16 re mib re do4 do,16 do re do |
sib,1 |
la4~ la16 sol la sib do'4 do' |
re1 |
sib4~ sib16 la sib do' re'4 sib |
mib re do sib,8 la, |
sib,4 do re re, |
sol,1 |
r2 sib4~ sib16 sib do' sib |
la2 r |
la, r |
sib,4~ sib,16 la, sib, do re4 sib, |
sib1 |
sib,2~ sib,8 sib,16 do re mib? fa sol |
lab2.~ lab16 lab sib lab |
sol4 fa8 mib re4 sib, |
mib2~ mib16 mib, fa, sol, lab, sib, do re |
mib1~ |
mib4 lab, sib,2 |
mib4 sol8 fa mib4 re |
do do8 re mib4 do |
re sol fad sol |
re4~ re16 do re mi fad4 re |
sol sol re4~ re16 do re mib |
fa4 fa do~ do16 sib, do re |
mib8 fa sol fa mib2 |
re4 re,16 re mib re do4 do,16 do re do |
sib,1 |
la4~ la16 sol la sib do'4 do' |
re1 |
sib4~ sib16 la sib do' re'4 sib |
mib re mib do |
re re,16 re mib re do4 do,16 do re do |
sib,1 |
la4~ la16 sol la sib do'4 do' |
re1 |
sib4~ sib16 la sib do' re'4 sib |
mib re do sib,8 la, |
sib,4 do re re, |
sol,1 |
