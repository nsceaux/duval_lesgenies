\clef "dessus" sol'4~ sol'16 \ficta fad' sol' la' sib'4 sib' |
do'1 |
la'4~ la'16 sol' la' sib' do''4 do'' |
re'2.~ re'16 re'' mib'' re'' |
do''4 sib' la' sol' |
fad'2\trill re' |
sol'4~ sol'16 fad' sol' la' sib'4 sib' |
do'1 |
la'4~ la'16 sol' la' sib' do''4 do'' |
re'2.~ re'16 re'' mib'' re'' |
do''4 sib' la' sol'8 fad' |
sol'4. la'8 la'4.\trill sol'8 |
sol'1 |
sib'4~ sib'16 la' sib' do'' re''4 fa'' |
do''2 la'4 fa' |
do''4~ do''16 sib' do'' re'' mib''4 re''8 do'' |
re''2 sib' |
re''4~ re''16 do'' re'' mib'' fa''4 fa'' |
lab''1 |
sib'4~ sib'16 do'' re'' mib'' fa''4 fa'' |
sib''4 lab''8 sol'' fa'' sol'' lab'' fa'' |
sol''4. fa''8 mib''2 |
mib'4. re'8 mib' fa' sol' lab' |
sib'4 do'' fa'4.\trill mib'8 |
mib'4 r r2 |
r4 do''8 sib' la'4 sol' |
fad' sol' la' sib' |
la'2 re' |
sib'4~ sib'16 la' sib' do'' re''4 re'' |
la'4~ la'16 sol' la' sib' do''4 do'' |
sol'~ sol'16 fad' sol' la' sib'4 do''\trill |
re''8 la' fad' la' re'2 |
sol'4~ sol'16 fad' sol' la' sib'4 sib' |
do'1 |
la'4~ la'16 sol' la' sib' do''4 do'' |
re'2.~ re'16 re'' mib'' re'' |
do''4 sib' la' sol' |
fad'2\trill re' |
sol'4~ sol'16 fad' sol' la' sib'4 sib' |
do'1 |
la'4~ la'16 sol' la' sib' do''4 do'' |
re'2.~ re'16 re'' mib'' re'' |
do''4 sib' la' sol'8 fad' |
sol'4. la'8 la'4.\trill sol'8 |
sol'1 |
