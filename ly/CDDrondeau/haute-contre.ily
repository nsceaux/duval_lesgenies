\clef "haute-contre"
\setMusic #'rondeau {
  re'2 sol'4 sol' |
  fad'1 |
  fad'4~ fad'16 mi' fad' sol' la'4 la' |
  sol'4~ sol'16 fad' sol' la' sib'4~ sib'16 sib' do'' sib' |
  la'4 sib' do'' mib' |
  re'2.~ re'16 re' mi'! fad' |
  sol'2 sol'4 sol' |
  fad'1 |
  fad'4~ fad'16 mi' fad' sol' la'4 la' |
  sol'4~ sol'16 fad' sol' la' sib'4~ sib'16 sib' do'' sib' |
  la'4 sib' do'' re'' |
  re'' mib' re' do' |
  sib1 |
}
\keepWithTag #'() \rondeau
re'4~ re'16 do' re' mib' fa'4 fa' |
fa'2 r |
fa'4~ fa'16 sol' la' sib' do''4 sib'8 la' |
sib'2 sib'4 sib' |
sib'2 re''4 re'' |
re''2 re' |
re'4~ re'16 mib' fa' mib' re'4 re' |
mib' sib'2 sib'4~ |
sib'4. lab'8 sol'2 |
sib2. mib'4 |
mib'2 re'4.\trill mib'8 |
mib'4 si do' re' |
mib'4 mib'8 re' do'4 mib' |
re' sib' la' sol' |
fad'2\trill re' |
re' fa'4 fa' |
do'2 mib'4 mib' |
mib'4. re'8 re'4 do' |
la2 re'4 re' |
\rondeau |
