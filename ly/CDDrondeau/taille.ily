\clef "taille"
\setMusic #'rondeau {
  sib~ sib16 la sib do' re'4 re' |
  mib'1 |
  re'2 re'4 re' |
  re'2 sol'4 sol' |
  sol'2 sol' |
  la' la4 re' |
  re'~ re'16 la sib do' re'4 re' |
  mib'1 |
  re'2 re'4 re' |
  re'2 sol'4 sol' |
  sol'2. re'4 |
  sol'2 fad'4.\trill sol'8 |
  sol'1 |
}
\keepWithTag #'() \rondeau
r2 re'4~ re'16 re' mib' re' |
do'2 r |
mib' r |
re'4~ re'16 do' re' mib' fa'4 fa' |
fa'2 sib'4 sib' |
sib'1 |
fa'2.~ fa'16 fa' sol' fa' |
mib'2 fa'8 mib' re' fa' |
mib'4. fa'8 sol'2 |
sol2. sol4 |
sol lab lab4. sol8 |
sol4 sol sol si |
do' sol2 sol4 |
re' re'2 re'4 |
re'2 la4~ la16 la sib la |
sol2 sib4 sib |
la2 sol4 sol |
sib2 sol' |
fad'\trill la4 la |
\rondeau |
