\clef "dessus" r4 r sol''8 fad'' |
sol'' fad'' mi'' re'' do'' si' |
la'4 la'' si'' |
sol''8 fad'' mi'' re'' mi'' dod'' re''4. fad''8 sol''4 |
la''8 sol'' fa'' mi'' re'' do'' |
si'4\trill si' mi'' |
do''4. do''8 do''4 |
si' si'4.\trill la'8 |
la' si' dod'' re'' mi'' dod'' |
re''4 do''8 si' la' re'' |
sol' la' si' do'' re'' si' |
mi''4. mi''8 fad''4 |
sol''4 sol'' fad'' |
sol''2 sol'4\fort |
si'8 la' sol' la' si' do'' |
re''4\doux re'' do''8 si' |
la'4.\trill do''8 re''4 |
sol'4 mi''4. mi''8 |
re''4. si''8 la'' sol'' |
la'' si'' do''' si'' la'' sol'' |
fad'' sol'' la'' sol'' fad'' mi'' |
red''4\trill red'' sol'' |
mi''4. mi''8 mi''4 |
mi''4 red''4.\trill mi''8 |
mi''4 mi'' dod'' |
re''8 re''16 mi'' fad'' mi'' fad'' sol'' la'' sol'' la'' fad'' |
sol''8 si'16 do'' re'' re'' mi'' fad'' sol'' fad'' sol'' la'' |
si'' la'' si'' do''' re'''8 do''' si'' re''' |
sol''4 sol'' do''' |
do'''4. do'''8 \tuplet 3/2 { si''8 la'' sol'' } |
sol''4 la'' fad'' |
sol''16 re'' mi'' fad'' sol'' sol'' la'' si'' do''' la'' si'' do''' |
re''' mi''' re''' do''' si'' do''' si'' la'' sol'' fad'' sol'' la'' |
fad''4\trill r16 re'' mi'' fad'' sol'' mi'' fad'' sol'' |
la'' si'' la'' sol'' fad'' mi'' fad'' sol'' la'' sol'' la'' fad'' |
si'' re'' mi'' fad'' sol'' fad'' sol'' la'' si'' la'' si'' do''' |
re''' mi''' re''' do''' si'' do''' si'' la'' sol'' la'' sol'' fad'' |
mi''4. mi''8 la'' la'' |
la''4. la''8 re''4 |
sol'' sol'' fad''\trill |
sol''2. |
