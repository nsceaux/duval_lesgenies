\clef "dessus" r4 r sol''8 la'' |
si'' la'' sol'' sol'' fad'' sol'' |
fad''4 fad'' sol'' |
dod''8 re'' mi'' fad'' sol'' mi'' |
fad''4. do''8 re''4 |
mi'' la'8 la'' sold'' la'' |
sold'' fad'' sold'' la'' si'' do''' |
la''4. la''8 la''4 |
la''4 sold''4.\trill la''8 |
la''4 mi''8 re'' dod'' mi'' |
la'' sol'' fad'' sol'' la'' fad'' |
si'' la'' sol''4 re'' |
re''4. re''8 do''4 |
si'4 do'' la' |
si'8\fort la' sol' la' si' do'' |
re'' do'' si' la' sol' fad' |
sol'\doux la' si'4 la'8 sol' |
fad'4.\trill fa'8 fa' sol' |
mi'4\trill do''4. do''8 |
si'8 si'' la'' sol'' fad'' mi'' |
red''4\trill red''4. mi''8 |
la'' si'' do''' si'' la'' sol'' |
fad''4\trill si'' si'' |
si''4. si''8 la''4 |
sol'' fad''4.\trill mi''8 |
mi''4 mi'' mi'' |
la''16 si'' la'' sol'' fad'' sol'' fad'' mi'' re'' mi'' re'' do'' |
si'8 sol'16 la' si' si' do'' re'' mi'' re'' mi'' fad'' |
sol'' fad'' sol'' la'' si''8 la'' sol'' fad'' |
mi''4 mi'' mi'' |
la''4. la''8 \tuplet 3/2 { re'' do'' si' } |
do''4 do'' la' |
si'4 r16 si'' la'' sol'' fad''8\trill fad'' |
sol'' sol'16 la' si' la' si' do'' re''8 mi'' |
la'16 la' si' dod'' re'' fad'' mi'' re'' dod''8 dod'' |
re'' la' la''16 si'' la'' sol'' fad'' sol'' fad'' mi'' |
re'' si' do'' re'' mi'' re'' mi'' fad'' sol'' fad'' sol'' la'' |
si''16 do''' si'' la'' sol'' la'' si'' do''' re'''4 |
re'''4. sol''8 do''' do''' |
do'''4. re'''8 si''4\trill~ |
si''8 la'' la''4.\trill sol''8 |
sol''2. |
