\tag #'violon _
Re -- ce -- vez l’é -- cla -- tant hom -- ma -- ge
d’un cœur que vous a -- vez domp -- té,
tri -- om -- phez, goû -- tez l’a -- van -- ta -- ge
d’a -- voir dé -- sar -- mé sa fier -- té,
tri -- om -- phez, goû -- tez l’a -- van -- ta -- ge
d’a -- voir dé -- sar -- mé sa fier -- té.

La gloi -- re, la ma -- gni -- fi -- cen -- ce
ne font plus sa fé -- li -- ci -- té ;
il ne con -- naît plus de puis -- san -- ce
que cel -- le de vo -- tre beau -- té.

Tri -- om -- phez, __ goû -- tez l’a -- van -- ta -- ge
d’a -- voir dé -- sar -- mé sa fier -- té.
Tri -- om -- phez, __ tri -- om -- phez, __ goû -- tez l’a -- van -- ta -- ge
d’a -- voir dé -- sar -- mé sa fier -- té.
