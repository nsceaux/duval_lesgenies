\piecePartSpecs
#`((dessus #:score "score-dessus")
   (dessus1 #:notes "violon" #:clef "treble")
   (dessus2 #:notes "violon" #:clef "treble")
   (parties #:notes "violon" #:clef "alto")
   (taille #:notes "violon")
   (basse #:score "score-basse")
   (flute-hautbois #:score "score-flutes"))
