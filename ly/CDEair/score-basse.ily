\score {
  \new Staff \with {
    instrumentName = "[Clavecin]"
    \override BassFigureAlignmentPositioning.direction = #UP
  } <<
    \new Voice = "basse" << \global \includeNotes "violon" >>
    \includeFiguresInStaff "chiffres"
    \new Lyrics \lyricsto "basse" { \keepWithTag #'violon \includeLyrics "paroles" }
  >>
  \layout { indent = \largeindent }
}
