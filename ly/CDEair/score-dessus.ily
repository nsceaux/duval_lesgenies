\score {
  \new ChoirStaff <<
    \new GrandStaff \with { instrumentName = "Flûtes" } <<
      \new Staff << \global \includeNotes "flute1" >>
      \new Staff << \global \includeNotes "flute2" >>
    >>
    \new Staff \with { instrumentName = "Violons" } <<
      \global \includeNotes "violon" \clef "treble"
    >>
  >>
  \layout { indent = \largeindent }
}
