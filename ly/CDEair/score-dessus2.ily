\score {
  \new ChoirStaff <<
    \new Staff \with { instrumentName = "Flûtes" } <<
      \global \includeNotes "flute2"
    >>
    \new Staff \with { instrumentName = "Violons" } <<
      \global \includeNotes "violon" \clef "treble"
    >>
  >>
  \layout { indent = \largeindent }
}
