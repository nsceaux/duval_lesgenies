\score {
  \new Staff \with {
    instrumentName = \markup\center-column { Htes-contre, et Tailles }
  } <<
    \new Voice = "parties" << \global \includeNotes "violon" >>
    \new Lyrics \lyricsto "parties" { \keepWithTag #'violon \includeLyrics "paroles" }
  >>
  \layout { indent = \largeindent }
}
