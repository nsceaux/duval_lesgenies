\score {
  \new ChoirStaff <<
    \new GrandStaff \with { instrumentName = "Flûtes" } <<
      \new Staff << \global \includeNotes "flute1" >>
      \new Staff << \global \includeNotes "flute2" >>
    >>
    \new Staff \with { instrumentName = \markup\character Un Indien } \withLyrics <<
      \global \includeNotes "voix"
    >> \keepWithTag #'voix \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { Violons, Htes-contre, et Tailles }
    } <<
      \global \includeNotes "violon"
      \includeFigures "chiffres"
      \origLayout {
        s2.*5 s4 \bar "" \break s2 s2.*7\pageBreak
        s2.*6\break s2.*7\pageBreak
        s2.*6\break s2.*5\pageBreak
        s2.*5
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
