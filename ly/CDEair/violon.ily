\clef "quinte" sol2 sol'8 re' |
si4 do'8 si la sol |
re'4 re' sol' |
mi'8 re' dod' re' sol la |
re'4. la8 si4 |
do' re'8 do' si la |
mi'4 mi' r8 do' |
fa'4. fa'8 re'4 |
mi'4 mi' mi |
la4. la'8 sol'4 |
fad' re'8 mi' fad' re' |
sol'4 sol' r8 si do'4. do'8 la4 |
mi'4 do' re' |
sol2.~ |
sol2 sol4 |
si8 la sol la si do' |
re'4 re'8 la si4 |
do' do'8 re' mi' fad' |
sol'2. |
r4 fad'8 sol' fad' mi' |
red'4\trill red'4. mi'8 |
si4 si mi' |
do'4. do'8 do'4 |
la si4. si8 |
mi'4. mi'8 la'4 |
fad'16\melisma sol' fad' mi' re' do' re' mi' fad' mi' fad' re' |
\once\tieDashed sol'2.~ |
sol'4\melismaEnd sol'8 la' si' sol' |
do''4 do'' r8 la' |
fad'4. fad'8 sol'4 |
mi' do' re' |
sol4. sol8 la4 |
si16\melisma do' si la sol la sol la si do' si do' |
re'4.\melismaEnd re'8 mi'4 |
fad'16\melisma sol' fad' mi' re' do' re' mi' fad' mi' fad' re' |
sol'2.~ |
sol'4\melismaEnd sol'8 la' si' sol' |
do''4 do'' r8 la' |
fad'4. fad'8 sol'4 |
mi' do' re' |
sol2. |
