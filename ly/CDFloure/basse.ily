\clef "basse" sol,2 r4 r2*3/2 |
r4 r8 sol sol4 fad mi re |
sol sol, re sol fad mi |
re dod si, la, dod la, |
re4. mi8 fad re la4. la8 sol4 |
fad4. fad8 mi4 re dod re |
la, sol, fad, sol, la,2 |
re,4. re,8 mi,4 fad, mi, re, |
re,2 r4 r2*3/2 |
r4 r8 re' re'4 dod' si la |
re' re la re' sol la |
si red si, mi fad sol |
la4. sol8 fad mi red4 mi do |
si, dod red mi fad sol |
la si do' la si si, |
mi sol si mi'4. re'8 do'4 |
si4. si8 do'4 re'4. do'8 si4 |
la4. la8 si4 do'4. si8 la4 |
sol2 sol,4 re2 do4 |
si, la, sol, mi re do |
do'2 si4 do' re' re |
sol,4. la,8 si,4 do re re, |
sol,4. sol,8 la,4 si, la, sol, |
re,2 r4 r2*3/2 |
sol,2.~ sol, |
