\clef "dessus" r4 r8 re'' do''4 si' la' sol' |
re''2.~ re''~ |
re''4. re''8 do''4 si' la' sol' |
fad'4. mi'8 re'4 dod' la' mi' |
fad'4. dod'8 re'4 mi' fad' sol' |
la'4. si'8 dod''4 re'' mi'' fad'' |
dod''4.\trill la'8 re''4 re''4. mi''8 dod''4 |
re''2.~ re'' |
re''4. la'8 sol'4 fad' mi' re' |
la'2.~ la'~ |
la'4. la'8 sol'4 fad' sol' mi' |
red'4.\trill si'8 fad'4 sol' la' si' |
do''2.~ do''4 si' mi'' |
red''4.\trill si'8 la'4 sol' fad' mi' |
do'' si' mi'' mi''4. fad''8 red''4 |
mi''2.~ mi''4. si'8 do''4 |
re''2.~ re''4. la'8 si'4 |
do''2.~ do''4. sol'8 la'4 |
si'4. do''8 re''4 fad' mi' re' |
sol'4. la'8 si'4 do'' re'' mi'' |
fad'' re'' sol'' la''4.\trill sol''8 la''4 |
si''4. fad''8 sol''4 la'' re'' fad''\trill |
sol''2.~ sol'' |
r4 r8 la'8 sol'4 fad' mi' re' |
sol''2.~ sol'' |
