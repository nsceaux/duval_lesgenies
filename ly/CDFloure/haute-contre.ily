\clef "haute-contre" sol'2 r4 r2*3/2 |
r4 r8 si' si'4 la' sol' fad' |
sol'2 re'4 re'2 la4 |
la2. mi'2 dod'4 |
re' la re' dod' re' mi' |
re'2 la'4 la'2 la'4 |
la'4. la'8 la'4 sol' fad' sol' |
fad'4.\trill fad'8 sol'4 la' sol' fad' |
fad'2\trill r4 r2*3/2 |
r4 r8 fad' fad'4 mi' re' dod' |
re'4. re'8 mi'4 re'2 fad'4 |
fad'2 red'4 mi' red' mi' |
mi'2. la4 sol la |
si2 si4 si la sol |
fad fad' fad' do' si la |
sol2.~ sol4. si'8 la'4 |
sol'4. sol'8 sol'4 fad'4. re'8 re'4 |
mi'4. la'8 sol'4 sol'4. sol'8 fad'4 |
sol'4. la'8 si'4 la' sol' fad' |
sol'2 re''4 do'' fad' sol' |
la'2 sol'4 sol' sol' fad'\trill |
sol'2 sol'4 sol'2 do''4 |
si'2.\trill~ si' |
R1. |
si'2.\trill~ si' |
