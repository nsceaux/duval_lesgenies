\piecePartSpecs
#`((dessus #:instrument
           ,#{\markup\center-column { [Violons, Hautbois] } #})
   (parties)
   (dessus2-haute-contre)
   (taille)
   (basse #:instrument
          , #{\markup\center-column { [Basses, Bassons et B.C.] } #})
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
