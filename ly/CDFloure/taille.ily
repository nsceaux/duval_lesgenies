\clef "taille" si2 r4 r2*3/2 |
r4 r8 re' re'4 re' mi' la |
si4. si8 la4 sol la dod' |
re'4. mi'8 fad'4 la2 la4 |
la2. la2 dod'4 |
re'4. re'8 mi'4 fad' mi' re' |
mi'2 re'4 mi' fad' mi' |
re'2.~ re' |
re'2 r4 r2*3/2 |
r4 r8 la' la'4 la'2 la'4 |
fad'2\trill sol'4 la' si' dod' |
si2 si4 si2 si4 |
la4. si8 la sol fad4 sol fad |
fad2 fad4 sol la si |
la red' mi' fad' sol' fad' |
mi'2.~ mi'4. fad'8 fad'4 |
sol'4. re'8 mi'4 fad'4. fad'8 sol'4 |
do'4. do'8 re'4 mi'4. re'8 re'4 |
re'2 re'4 re'2 re'4 |
re'2 sol'4 sol' fad' mi' |
re'2 re'4 do' si la |
sol4. re'8 re'4 do' si la |
sol2.~ sol |
R1. |
sol2.~ sol |
