\clef "basse" sol,4. sol8 sol4 la |
si4. si8 la4 sol |
re' dod' re' fad |
sol mi si8 sol la la, |
re4. sol8 sol4 la |
si4. si8 la4 sol |
re' dod' re' fad |
sol mi si8 sol la la, |
re2 re'4 sol |
re2 re'4 sol |
fad8 re re' do' si sol fad re |
sol4 si, la, sol, |
re re, sol8 mi la fad |
si sol do' la si4 si, |
mi2 r4 mi |
fad sol do8 si, la, sol, |
re4 re'8 do' si la sol do' |
si la sol fad mi4 do |
re8 la, si, do fad,4 re, |
re'8 la si do' fad4 sol8 do |
re do re mi fad4 re |
sol8 la si do' re'4 re |
sol8 la si do'
sol2
