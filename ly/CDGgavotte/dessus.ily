\clef "dessus" r4 r8 re'' si'4 do'' |
re''4. sol''8 fad''4 sol'' |
la'' sol'' fad''8 mi'' re'' do'' |
si' la' si' dod'' re''4 dod'' |
re''4. re''8 si'4 do''\trill |
re''4. sol''8 fad''4 sol'' |
la'' sol'' fad''8 mi'' re'' do'' |
si' la' si' dod'' re''4 dod'' |
re''2 la'4 si'8 sol' |
la'4 re' la'8 do'' si' sol' |
la'4 re' sol'8 si' la' do'' |
si'4 re'' do''4.\trill si'8 |
la'4\trill la' si'8 re'' dod'' mi'' |
red'' si' mi'' fad'' fad''4.\trill mi''8 |
mi''2 sol''8 fad'' mi'' re'' |
do''4\trill si' mi''8 re'' do'' si' |
si'4\trill la' re''8 do'' si' mi'' |
re'' do'' si' la' do''4. si'8 |
la'2\trill re''8 la' si' do'' |
re''4 re' re''8 la' si' do'' |
fad'4\trill re' re''8 la' si' do'' |
si'\trill la' sol' la' la'4.\trill sol'8 |
sol'2
sol'2

