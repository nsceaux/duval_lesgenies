\clef "haute-contre" re'4. si'8 si'4 la' |
sol'4. si'8 do''4 si' |
re'' mi'' re''8 do'' si' la' |
sol' fad' sol' la' sol'4 sol' |
fad'4.\trill si'8 si'4 la' |
sol'4. si'8 do''4 si' |
re'' mi'' re''8 do'' si' la' |
sol' fad' sol' la' sol'4 sol' |
fad'2\trill fad'4 sol'8 mi' |
fad'2 fad'8 la' sol' mi' |
fad'4 fad'8 mi' re'4 la' |
sol' sol' fad'8 la' si' sol' |
fad'4\trill fad' si' la' |
fad' mi' mi' red'\trill |
mi'2 mi''8 re'' do'' si' |
la'4\trill sol' sol' fad'8 sol' |
sol'4\trill fad' sol' sol' |
sol'2 sol'4 sol' |
fad'2\trill la'4. sol'8 |
fad' sol' fad' mi' re'4. mi'8 |
re' mi' fad' sol' la'4. la'8 |
sol' fad' sol'4 fad'4.\trill sol'8 |
sol'2 sol' |
