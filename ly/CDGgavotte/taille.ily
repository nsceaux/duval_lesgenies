\clef "taille" si4. re'8 re'4 re' |
re'4. re'8 re'4 sol' |
fad' la' la' re' |
mi' mi' re' la |
la4. re'8 re'4 re' |
re'4. re'8 re'4 sol' |
fad' la' la' re' |
mi' mi' re' la |
la2 re'4 re' |
re'2 re'4 re' |
re' fad'8 mi' re'4 re' |
re' re' re' re' |
re'2 re'4 la |
si do' si la |
sol2 r4 do' |
re' re' do'8 re' re'4 |
re'2 re'4 re'8 do' |
re'2 do'4 mi' |
re'2 re' |
re'4. la8 la4 sol8 la |
la4 fad'8 mi' re'4 re'~ |
re' re'2 do'4 |
si2\trill si\trill |
