\clef "basse" sol4 fad |
sol fa mib do |
re re'8 do' sib4 fad |
sol4 fa mib2 |
re8 mi fad re sol4 fad |
sol4 fa mib do |
re re'8 do' sib4 fad |
sol fa mib2 |
re sib8 do' sib do' |
re'4 r mib' r |
re' r mib' r |
re'8 do re mib fa4 fa, |
sib,2 r4 sol8 fa |
mib4 re do8 re mib fa |
sol4 si, do8 sol la si |
do' sol mib sol do re mib do |
re do sib,4 la, sol, |
fad, sol, sol fad |
sol fa mib do |
re re'8 do' sib4 fad |
sol2 do4 re |
sol8 fad sol la
sol,2
