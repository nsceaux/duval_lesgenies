\clef "dessus" sol'8\tresdoux sib' la' re'' |
mib''4 re'' sol''8 sib'' la'' sol'' |
fad''4\trill re'' sol'8 sib' la' re'' |
mib''4 re'' sol''8 la'' fad'' sol'' |
\appoggiatura sol''8 la''2 sol'8 sib' la' re'' |
mib''4 re'' sol''8 sib'' la'' sol'' |
fad''4\trill re'' sol'8 sib' la' re'' |
mib''4 re'' sol''8 la'' fad'' sol'' |
\appoggiatura sol''8 la''2 re''8 mib'' re'' mib'' |
fa''4 sib' sol''8 sib' sol'' sib' |
fa''4 sib' sol''8 sib' la' sib' |
fa'' la' sib' do'' do''4.\trill sib'8 |
sib'2 re''8 mib'' re'' si' |
do''8 re'' mib'' fa'' sol''4 fa''8 mib'' |
re'' mib'' fa'' re'' mib''2 |
\appoggiatura re''8 do''2 mib''8 re'' do'' sib' |
la'4 sol' fad'\trill sol' |
re'' re' sol'8 sib' la' re'' |
mib''4 re'' sol''8 sib'' la'' sol'' |
fad''4\trill \appoggiatura mi''8 re''4 sol'8 sib' la' re'' |
mib''4 re''8 sol'' la''4 fad''\trill |
sol''2
sol''
