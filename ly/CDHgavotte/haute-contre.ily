\clef "haute-contre" re' la' |
sol' la' sib'8 re'' do''\trill sib' |
la'2\trill re'4 la' |
sol' la' sib'8 do'' la' sol' |
fad'2\trill re'4 la' |
sol' la' sib'8 re'' do''\trill sib' |
la'2\trill re'4 la' |
sol' la' sib'8 do'' la' sol' |
fad'2\trill sib'4 sib' |
sib' r do'' r |
re'' r do'' r |
sib'8 do'' sib'2 la'4\trill |
sib'2 sol'4 sol' |
sol' sol'8 fa' mib' re' do' la |
si do' re'4 do'2 |
sol do''8 sib' la' sol' |
fad'4 sol' re' re' |
re'2 re'4 la' |
sol' la' sib'8 re'' do''\trill sib' |
la'2\trill re'4 la' |
sol'4. re''8 mib''4 re'' |
re''2 re'' |
