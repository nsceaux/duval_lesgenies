\piecePartSpecs
#`((dessus #:instrument
           ,#{\markup\center-column { Flûtes et Violons } #})
   (parties)
   (dessus2-haute-contre)
   (taille)
   (basse #:instrument
          , #{\markup\center-column { [Basses et B.C.] } #})
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
