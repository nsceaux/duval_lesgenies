\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { Flûtes et Violons }
    } << \global \includeNotes "dessus" >>
    \ifFull <<
      \new Staff \with {
        instrumentName = \markup\center-column { [Hautes-contre] }
      } << \global \includeNotes "haute-contre" >>
      \new Staff \with {
        instrumentName = "[Tailles]"
      } << \global \includeNotes "taille" >>
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2 s1*7\break \grace s8 s1*8\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}