\clef "taille" sib4 do' |
sib la sol mib' |
re' fad'8 mi'! re'4 do' |
sib re'2 do'4 |
re'2 sib4 do' |
sib la sol mib' |
re' fad'8 mi'! re'4 do' |
sib re'2 do'4 |
re'2 fa'8 mib' fa' mib' |
re'4 r sib' r |
sib' r sib' r |
sib'8 la' fa' sol' fa'4 mib' |
re'2\trill si8 do' si re' |
do'4 si\trill do' sol |
sol sol sol2 |
mib sol4 sol |
la re'2 sib4 |
do' sib sib do' |
sib la sol mib' |
re' fad'8 mi'! re'4 do' |
do' sib8 sib do'4 la |
sib8 la sib do' sib2 |
