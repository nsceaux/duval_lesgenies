\clef "basse" sol8 sol4 la |
si4. si8 la4 sol |
re' dod' re' fad |
sol mi si8 sol la la, |
re4. sol8 sol4 la |
re2 re4 sol, |
re, r re sol, |
re, re8 do si, sol fad re |
sol la si sol fad4 sol |
re8 mi fad re sol mi la fad |
si la sol la si4 si, |
mi2 r |
mi8 fa mi re do si, la, sol, |
re mi re do si, la, sol,4 |
sol, sol fad sol |
re8 la, si, do fad,4 sol, |
re8 la, si, do fad,4 sol, |
re8 mi re do si, la, sol, fad, |
sol, la, si, do re4 re, |
sol,8 la, si, do
sol,2
