\piecePartSpecs
#`((basse #:instrument "[B.C.]"
          #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{

\markup\fontsize#-2 \fill-line {
  \column {
    \livretPers Un Indien
    \livretVerse#11 { Dans nos climats }
    \livretVerse#11 { Chacun s’engage, }
    \livretVerse#12 { Et la plus sauvage }
    \livretVerse#12 { Ne résiste pas. }
    \livretVerse#11 { Notre richesse }
    \livretVerse#12 { Fait notre tendresse ; }
  }
  \column {
    \livretVerse#12 { Nous savons charmer }
    \livretVerse#11 { Un cœur rebelle, }
    \livretVerse#12 { Et la plus cruelle }
    \livretVerse#12 { Se laisse enflammer. }
    \livretVerse#12 { Le dieu des amours }
    \livretVerse#12 { Se sert de nos armes, }
    \livretVerse#12 { Il n’a point de charmes }
    \livretVerse#12 { Sans notre secours. }
  }
}#}))
