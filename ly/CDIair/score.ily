\score {
  \new ChoirStaff <<
    \new Staff \with { instrumentName = \markup\character Un Indien } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "[B.C.]" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s8 s2 s1*4 s2 \break s2 s1*3 s2 \bar "" \break s2 s1*3\pageBreak
        s1*4\break s1*3
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
