\clef "vhaute-contre" re'8 si4 do' |
re'4. sol'8 fad'4\trill sol' |
la' sol' fad'8 mi' re' do' |
si\trill la si dod' re'4 dod'\trill |
re'4. re'8 si4 do' |
re'2 la4 si8 sol |
la4 fad\trill la8 do' si sol |
la4 fad\trill sol8 si la do' |
si4 re' do'4.\trill si8 |
la4\trill la si8 re' dod' mi' |
red'\trill si mi' fad' fad'4.\trill mi'8 |
mi'2 sol'8 fad' mi' re' |
do'2 mi'8 re' do' si |
si4\trill la re'8 do' si mi' |
re' do' si la do'4. si8 |
la2\trill re'8 la si do' |
re'2 re'8 la si do' |
fad4\trill fad re'8 la si do' |
si la sol la la4.\trill sol8 |
sol2
sol2
