\clef "basse" re8 re' sib4 sol |
la4. sol8 fa mi |
re re' fad4 re |
sol8 la sib2 |
la8 la sol fa mi re |
dod2 la,4 |
re do sib, |
la, la8 sib la sol |
fa4 dod2 |
re8 re' do' sib do' la |
sib la sol la sib sol |
do'4 mi do |
fa la fa |
sib2 sol4 |
do' sib la |
sib do' do |
fa r r |
fad re sol |
sold mi la |
la fa sib |
si! sol do' |
dod' la dod' |
re'8 sol la4 la, |
re8 re' sib4 sol |
la4. sol8 fa mi |
re8 re' fad4 re |
sol8 la sib2 |
la la4 |
sol4. sol8 fa mi |
re4. la8 si dod' |
re'2 do'4 |
sib la2 |
sol4 dod la, |
re2. |
sol2 la4 |
sib2 fa8 sol |
la4 la,2 |
re4 sol8 la sib sol |
la4 dod la, |
re8 sol, la,2 |
re4 re8 do re mi |
fa2 fa,4 |
do2 do'4 |
mi2~ mi8 fa |
sib,2 do |
fa mi8 fa re4 |
mi re8 do si, la, |
sold,2 mi,4 |
la,1~ |
la,4 la8 si do'4 do'8 re' mi'4 mi |
la, la8 sol fa mi |
re re' sib4 sol |
la4. sol8 fa mi |
