<<
  \tag #'dessus1 {
    \clef "dessus" r8 la' re''4. mi''8 |
    dod''4.\trill la'8 re'' mi'' |
    \appoggiatura mi'' fa''4 \appoggiatura mi''8 re'' la' sol' la' |
    sib' la' sol'4.\trill( fa'16 sol') |
    \appoggiatura sol'8 la'4. mi''8 mi'' fa'' |
    \appoggiatura fa'' sol''4. mi''8 la'' mi'' |
    fa'' la'' mi'' la'' re'' mi'' |
    dod''4\trill mi'' mi'' |
    la''4 sol''8 fa'' sol'' mi'' |
    fa''16 re'' mi'' fa'' fad''4.\trill( mi''16 fad'') |
    sol''8 la'' sib'' la'' sol'' fa'' |
    mi''4\trill sol'' do''' |
    la'' do'' fa'' |
    \appoggiatura mi''8 re''4 sol'' la''8 sib'' |
    mi''4.\trill re''16 do'' fa''4~ |
    fa''8 sol'' sol''4.\trill fa''8 |
    fa''4 la'4. la'8 |
    re''4 do''8 re'' si'4 |
    mi''4 re''8 mi'' dod''4 |
    fa''4 mi''8 fa'' re''4 |
    sol'' fa''8 sol'' mi''4 |
    la''8 sib'' la'' sol'' fa'' mi'' |
    fa'' sol'' mi''4.\trill re''8 |
    re''4
  }
  \tag #'dessus2 {
    \clef "dessus2" r8 fa' fa'4. sol'8 |
    mi'4.\trill mi'8 fa' dod' |
    \appoggiatura dod'8 re'4 re'4. fad'8 |
    sol' fa' mi'4. fa'16 re' |
    dod'4.\trill la'8 dod'' re'' |
    \appoggiatura re''8 mi''2 dod''4 |
    re'' r8 la' la' sol' |
    \appoggiatura sol' la'4 dod'' dod'' |
    re''8 mi''16 fa'' mi''4. mi''8 |
    la'4 la'4. do''8 |
    sib' do'' re'' do'' sib' la' |
    sol'4 mi'' sol'' |
    do'' la' do'' |
    fa' sib' do''8 re'' |
    sol'4. do''8 do'' do'' |
    re'' mi'' mi''4.\trill fa''8 |
    fa''4 r r |
    r la' re' |
    si si' mi' |
    do' do'' fa' |
    re' re'' sol' |
    mi' mi''4. mi''8 |
    la' re'' dod''4. re''8 | \startHaraKiri
    re''4
  }
>> r4 r |
r r8 mi'' fa'' dod'' |
re''4 la' r8 fad' |
sol' fa' \appoggiatura fa' mi'4. fa'16 re' |
dod'2\trill mi'4 |
mi'4. mi'8 fa' dod' |
re' la' si' dod'' re'' mi'' |
fa''4. sol''8 la''4 |
la''8. sol''16 la''8. mi''16 mi''4 |
dod''8 mi'' sol'' dod'' mi'' re''16 dod'' re''4. fa'8 sol' la' |
sib'4. sib'8 la' sol' |
mi'2 la'8 re'' |
re''4( dod''4.)\trill re''8 |
re'' fa'' mi'' sol'' fa'' mi'' |
dod'' la'' sol''4.\trill \appoggiatura fa''8 mi'' |
\appoggiatura mi'' fa'' sol'' mi''4.\trill re''8 |
re''2. |
r4 la'2 |
sol'\trill mi''4 |
sol''2~ sol''8 do'' |
fa''4. fa''8 fa''4. mi''8 |
fa''4 r8 do'' mi'' do'' fa'' re''16 re'' |
si'8 mi' mi' fad' sold' la' |
si'2 sold'4 |
la'2~ la'4. la'8 |
la'2. la'4 la' sold' |
la'2 la''8 sol'' |
fa'' la'' sol''4. sib''8 |
mi''4.\trill mi''8 fa'' dod'' |
