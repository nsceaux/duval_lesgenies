\key la \minor \midiTempo#100
\digitTime\time 3/4 s2.*25 \bar "||"
\segnoMark \grace s8 s2.*15 s4 \fineMark s2 \bar "|." s2.*3
\digitTime\time 2/2 s1
\time 4/4 s1
\digitTime\time 3/4 s2.*2
\digitTime\time 2/2 \grace s8 s1
\time 3/2 s1.
\digitTime\time 3/4 \grace s8 s2.*3 \bar "|."
\endMark\markup { jusqu’au mot fin. \raise#1 \musicglyph#"scripts.segno" }
