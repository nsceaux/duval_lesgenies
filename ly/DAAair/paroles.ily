Ty -- ran d’un cœur fi -- dèle et ten -- dre,
que t’ai- je fait, cru -- el __ a -- mour ?
Chaque ins -- tant, de mes cris re -- ten -- tit ce sé -- jour,
et tu ne veux pas les en -- ten -- dre.
Chaque ins -- tant, de mes cris re -- ten -- tit ce sé -- jour,
et tu ne veux pas les en -- ten -- dre.
Hé -- las ! loin de l’ob -- jet qui cau -- se ma lan -- gueur
tu me lais -- ses gé -- mir sous les fers d’un bar -- ba -- re,
et tu per -- mets qu’il me sé -- pa -- re
d’un a -- mant qui fai -- sait mon u -- ni -- que bon -- heur.
Ty -- ran d’un cœur fi -- dèle et
