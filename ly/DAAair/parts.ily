\piecePartSpecs
#`((dessus #:score-template "score-2dessus"
           #:music ,#{ s2.*24\break #}
           #:instrument
           ,#{ \markup\center-column { [Violons et Hautbois] } #})
   (dessus1 #:instrument ,#{ \markup\center-column { [Violons et Hautbois] } #})
   (dessus2 #:instrument ,#{ \markup\center-column { [Violons et Hautbois] } #})
   (basse #:score-template "score-basse-continue-voix"
          #:instrument
          ,#{ \markup\center-column { [Basses, Bassons et B.C.] } #})
           
   (silence #:on-the-fly-markup , #{
\markup\fontsize#-2 \column {
  \livretPers Isménide
  \livretVerse#8 { Tyran d’un cœur fidèle & tendre, }
  \livretVerse#8 { Que t’ai-je fait, cruel amour ? }
  \livretVerse#12 { Chaque instant, de mes cris retentit ce séjour, }
  \livretVerse#8 { Et tu ne veux pas les entendre. }
  \livretVerse#12 { Hélas ! loin de l’objet qui cause ma langueur }
  \livretVerse#12 { Tu me laisses gémir dans les fers d’un barbare, }
  \livretVerse#8 { Et tu permets qu’il me sépare }
  \livretVerse#12 { D’un amant qui faisait mon unique bonheur. }
  \livretVerse#8 { Tyran d’un cœur &c. }
}#}))
