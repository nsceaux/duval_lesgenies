\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new GrandStaff \with {
      instrumentName = \markup\center-column { [Violons, Hautbois] }
    } <<
      \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
      \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses, Bassons et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s2.*24\break }
      \origLayout {
        s2.*8\pageBreak
        s2.*9\break s2.*8\pageBreak
        \grace s8 s2.*4\break \grace s8 s2.*3 s2 \bar "" \pageBreak
        s4 s2.*6\break \grace s8 s2.*5 s1\pageBreak
        s1 s2.*2 s1\break s1. s2.*3
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
