\clef "vdessus" R2.*23 |
<>^\markup\character Isménide r8 la' re''4 mi'' |
dod''4.\trill la'8 re'' mi'' |
\appoggiatura mi''8 fa''4 \appoggiatura mi''8 re'' la' sol' la' |
sib' la' \appoggiatura la' sol'4.\trill( fa'16) sol' |
\appoggiatura sol'8 la' la'16 la' mi''8 mi''16 fa'' \appoggiatura fa''8 sol'' mi''16 sol'' |
dod''2 re''8 mi'' |
\appoggiatura mi''8 fa''2. |
r4 la'8 si' do'' la' |
re'' re''16 mi'' dod''4 dod''8 la'16 la' |
mi''8 mi''16 mi'' << { \voiceOne la''8 \oneVoice } \new Voice { \voiceTwo mi''8 } >> mi''16 fa'' sol''8 fa''16 mi'' |
\appoggiatura mi''8 fa''2. |
mi''4. sol''8 fa'' mi'' |
dod''2 re''8. mi''16 |
fa''4( mi''4.\trill re''8) |
re''4 r r |
R2.*3 |
r4 fa''2 |
mi''2.\trill |
do''2 sib'8 la' |
re''4. sib'8 sol'4\trill sol'8 la'16 sib' |
la'16 r fa' sol' la'8 la'16 fa' do''8 la'16 la' re''8 si'16 si' |
sold'4\trill sold'8 r si' si'16 si' |
mi''4. mi''8 mi'' si' |
\appoggiatura si'8 do''2 do''4 r8 la'16 si' |
do''4 do''8 re'' \appoggiatura re'' mi''4 mi''8 fa'' si'4\trill si'8 do'' |
\appoggiatura si'8 la'4 r r |
r8 la' re''4 mi'' |
dod''4.\trill la'8 re'' mi'' |
