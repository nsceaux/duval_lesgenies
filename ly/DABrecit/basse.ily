\clef "basse" re4 re' re' la |
re re fa la |
re re' re' la |
re re fa la |
re fa2 mi4 |
fa2 fa, |
fa4 la,8 si, do4 do, |
fa,2 fa4 do |
fa sib8 la sol4 sib |
<< { s4 <>^"[B.C.]" } la2. >> sib8 fad |
sol4 mi8 fa do4 do'8 sib |
la2. r16 fa sol la |
sib2 sib,4
