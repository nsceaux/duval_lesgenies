\clef "dessus" r4 re'' re'' la' |
re' re' fa' la' |
re' la'8 sol' fa' mi' re' dod' |
re'4 fa' fa' fa' |
la' fa' la' do'' |
fa' do''8 re'' do''4 fa'' |
la'2 sol'\trill |
fa'4 la'8 sib' la'4 sol' |
fa' sol'8 fa' mi'4 re' |
dod'4 r r2 |
R1*2 |
r2 r4
