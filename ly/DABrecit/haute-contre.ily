\clef "haute-contre" r4 re'' re'' la' |
re' re' fa' la' |
re' re'8 mi' fa' sol' fa' mi' |
fa'4 re' re' re' |
fa' fa'2 sol'4 |
la' la'8 sib' la'4 la' |
fa'2. mi'4\trill |
fa' fa'8 sol' fa'4 mi' |
fa' re' mi' mi' |
mi' r r2 |
R1*2 |
r2 r4
