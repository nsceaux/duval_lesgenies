\piecePartSpecs
#`((dessus #:instrument "Violons"
           #:score-template "score-voix")
   (parties)
   (taille #:score-template "score-voix")
   (dessus2-haute-contre #:score-template "score-voix")
   (basse #:instrument
          ,#{ \markup\center-column { [Basses et B.C.] } #}
          #:score-template "score-basse-continue-voix")

   (silence #:on-the-fly-markup , #{

\markup\fontsize#-2 \column {
  \livretPers Pircaride
  \livretVerse#12 { Que vois-je ? quel objet se présente à mes yeux ? }
  \livretVerse#8 { Juste ciel ! quel couroux l’anime. }
}
       #}))
