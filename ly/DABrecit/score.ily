\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with { instrumentName = "Violons" } <<
        \global \includeNotes "dessus"
      >>
      \ifFull <<
        \new Staff \with {
          instrumentName = \markup\center-column { [Hautes-contre] }
        } << \global \includeNotes "haute-contre" >>
        \new Staff \with {
          instrumentName = "[Tailles]"
        } << \global \includeNotes "taille" >>
      >>
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s1*9\break }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}