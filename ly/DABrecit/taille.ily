\clef "taille" r4 re'' re'' la' |
re' re' fa' la' |
re' fa' re' la |
la la la la |
la la do' sib |
la fa'2 do'4 |
do'2. sib4 |
la do' do' do' |
la sib sib sib |
la r r2 |
R1*2 |
r2 r4
