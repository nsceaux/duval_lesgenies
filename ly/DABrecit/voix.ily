\clef "vdessus" R1*9 |
<>^\markup\character Isménide r8 la' mi'' mi'' r mi''16 fa'' \appoggiatura mi''8 re'' do''16 re'' |
sib'4\trill sib'8 la' sol'4\trill r8 do''16 do'' |
mib''4. mib''16 mib'' mib''4. re''8 |
re''2\trill re''4
