\clef "basse" r4 |
r2 r4 sol |
do'2 r4 fa |
sib sol sib sol |
do' la re' sib |
do' fa do2 |
fa r4 re |
re re' sol sib |
la2 la,4 r |
r la la la |
re' re r2 |
re'4 re r2 |
re'4 re sol8 fa mi re |
sol la sib sol la4 la, |
re re' re' la |
re8 sib la sol fa mi re dod |
re4 re8 mi fa4 mi |
fa fa fa do |
la, fa, fad re |
sol sib sol re |
sib, sol, si! sol |
do' mi' do' la |
sib do' re' do' |
sib8 do' re' sib do'4 do |
fa2 fad |
sol sold |
la la |
sib si |
do' dod' |
re' la |
sib4 sol8 la sib4 sol |
la8 sol fa sol la4 la, |
re1 |
