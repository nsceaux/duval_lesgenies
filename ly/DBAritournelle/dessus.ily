\clef "dessus" re''4 |
sol'' sol'' sol'' sib'' |
mi'' do'''8 sib'' la'' sol'' fa'' mi'' |
re''4 sib''8 la'' sol'' fa'' mi'' re'' |
mi''4 do''' fa'' sib''~ |
sib'' la''8 sib'' sol''4 do''' |
la'' la' la' la' |
re'' re''8 fa'' mi''4 re'' |
dod'' mi'' mi'' mi'' |
la'' sib''8 la'' sol'' fa'' sol'' mi'' |
fa'' la' si' dod'' re'' mi'' fa'' sol'' |
la'' re'' mi'' fa'' sol'' la'' si'' dod''' |
re''' do''' sib'' la'' sib'' la'' sol'' fa'' |
sib'' la'' sol'' fa'' mi''4.\trill re''8 |
re''4 r r2 |
r4 re'' re'' la' |
re' fa' la' do'' |
fa' fa' la' do'' |
fa'' fa''8 mi'' re'' do'' sib' la' |
sib'4 sol' sib' re'' |
sol''4 sol''8 la'' sol'' fa'' mi'' re'' |
mi''4 do''2 fa''8 mi'' |
re''4 do'' sib' la' |
re''8 do'' sib' la' sol'4.\trill fa'8 |
fa'4 la'2 re''4~ |
re'' si'2 mi''4~ |
mi'' dod''2 fa''4~ |
fa'' re''2 sol''4~ |
sol'' mi''2 la''8 sol'' |
fa''4 mi'' re'' dod'' |
re''8 fa'' sib'' la'' sol'' la'' fa'' sol'' |
mi'' dod'' re'' mi'' mi''4.\trill re''8 |
re''1 |
