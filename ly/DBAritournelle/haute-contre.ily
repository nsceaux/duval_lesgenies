\clef "haute-contre" sol'4 |
sib' sib' sib' re'' |
do'' r r fa' |
fa' re' re' sol' |
sol' sol' fa' sol'~ |
sol' do''2 do''4 |
do''2 r4 fa' |
fa'4 fa'8 la' sib'4 sol' |
la'2. r4 |
r sol'8 fa' mi' re' mi' dod' |
re'4 re' r2 |
la'4 la' r2 |
la'4 la' r2 |
r4 sib' la' sol' |
fa' r r2 |
R1 |
r4 re' do' do' |
do' la do' fa' |
fa'2 re'4 re' |
re' sol' sol' fad' |
sol'2 sol'4 sol' |
sol' sol'2 fa'4 |
fa'2. fa'4 |
sol'4. fa'8 fa'4 mi' |
fa'2 la |
sib si |
dod' dod' |
re' re' |
mi' mi'4 la' |
la'2 re'4 mi' |
fa'8 la' re'' do'' sib'4. sib'8 |
la'4 la'2 la'4 |
fa'1 |
