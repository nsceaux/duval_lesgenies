\piecePartSpecs
#`((dessus #:instrument ,#{\markup\center-column { [Violons et Hautbois] }#})
   (parties)
   (dessus2-haute-contre)
   (taille)
   (basse #:system-count 5
          #:instrument ,#{ \markup\center-column { [Basses, Bassons et B.C.] } #})

   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
