\clef "taille" sib4 |
re' re' re' sol' |
sol' la'8 sol' fa' mi' re' do' |
sib4 sol'8 fa' mi' re' do' sib |
do'4 mi' re' re' |
mi' fa'8 sol' fa'4 mi' |
fa'2 r4 la |
la re' mi' mi' |
mi'2. r4 |
r dod' dod' mi' |
re' re' r2 |
fa'4 fa' r2 |
fa'4 fa' r2 |
r4 re' re' dod' |
re' r r2 |
R1 |
r4 la la sol |
la do' la la |
do'2 la4 la |
sol re' re' re' |
re'2 re'4 re' |
do' mi'2 do'4 |
re' la' sib' fa' |
fa'8 mi' re'4 do' do' |
la2\trill la |
sib si |
dod' dod' |
re'2 re' |
mi' mi' |
fa'4 sol' fa' mi' |
re'2. re'4 |
dod'8 mi' re'4 re' dod'\trill |
re'1 |
