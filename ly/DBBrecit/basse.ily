\clef "basse" re2 sol~ |
sol1 |
la4 fad2 |
sol2.~ |
sol2 sol8 la sib sol la4 la,8 sol, |
fa,2~ fa,8 sol, fa, mi, re,2 |
re2. la4 |
fad sol2. |
la8 re la,4. la8 re'4 |
sib4 sol la8 la, |
re2 re4 |
re2. | \allowPageTurn
sol2. sol16 fad mi re |
la8 la, re8 do si, la, |
sol,2 sol4 |
sol2. fad8 mi16 re |
la4 la,2 |
re4. la8 la re' |
\appoggiatura dod'8 si si la sol fad re |
sol4. la8 si sol |
la4 dod re2~ |
re4 dod8 si, la, sold, |
la,2 fad,4 |
sol,8 sol red4 si, |
mi2 mi,4 |
mi8 fad mi re dod si, |
fad2. |
mi |
re2 re4 |
dod8 si, fad4 fad, |
si,2.~ |
si,1 | \allowPageTurn
mi2. |
sold2 la4 sold |
fad4 re2 si,4 |
mi mi'8 re' dod'4 si8 la |
sold4 mi la sol? |
fad8 re sol fad mi re la la, |
re2.~ |
re2 fad,8 re, |
sol,4 sold, la,8 la16 sol |
fad4. fad8 mi re |
la2 la,4 |
re2. |
re4 fad8 re sol2 |
fad2 si4 la |
sol4. la8 sol fad |
mi4. la,8 si,4 |
mi, \ifComplet {
  mi4 re |
  do2 si,4. mi,8 |
  si,2. | \allowPageTurn
  r2 sold |
  la4. sol8 fad4 re |
  sol8 la sol fad mi fad sol mi |
  la si do'4 la |
  si2 si4 |
  la2 sol fad4. mi8 |
  si4 si,8 dod red si, |
  mi re mi fad sol mi |
  la si do'4 la |
  si2. |
  la2 sol4 fad8 mi si4 si, |
  mi2.~ |
  mi2 sold |
  la2.~ |
  la~ | \allowPageTurn
  la2 lad4 fad |
  si8 la sol fad mi2 |
  red8 dod si,4 la,4. fad,8 |
  si,8 mi, fad,2 |
  si,4
}
sold,2~ |
sold,2. |
la,2 la,4 |
re4 si, mi8 re dod si, |
la,2 la~ |
la2. | \allowPageTurn
sold4 mi la2 |
fad si4 si, |
mi2. |
