\key do \major
\time 4/4 \midiTempo#80 s1
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.*2
\time 3/2 \midiTempo#160 s1.*2
\time 4/4 \midiTempo#80 s1*3
\digitTime\time 3/4 s2. s2 \bar "||"
\key re \major s4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.*2
\digitTime\time 2/2 \grace s8 s1
\digitTime\time 3/4 s2.*4
\time 4/4 s1
\digitTime\time 3/4 s2.*10
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.
\digitTime\time 2/2 s1*5
\digitTime\time 3/4 \midiTempo#80 s2.*6
\time 4/4 s1
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2. \bar "||"
\key mi \minor s2.*2
\ifComplet {
  \digitTime\time 2/2 \midiTempo#160 s1
  \digitTime\time 3/4 \midiTempo#80 \grace s8 s2.
  \digitTime\time 2/2 \midiTempo#160 s1*3
  \digitTime\time 3/4 \midiTempo#80 s2.*2
  \time 3/2 \midiTempo#160 s1.
  \digitTime\time 3/4 \midiTempo#80 \grace s8 s2.*4
  \time 3/2 \midiTempo#160 s1.
  \digitTime\time 3/4 \midiTempo#80 s2.
  \time 4/4 s1
  \digitTime\time 3/4 s2.*2
  \digitTime\time 2/2 \midiTempo#160 \grace s8 s1
  \time 4/4 \midiTempo#80 s1
  \digitTime\time 2/2 \midiTempo#160 s1
  \digitTime\time 3/4 \midiTempo#80 s2.*4
}
\ifConcert s2.*2
\time 4/4 s1
\digitTime\time 2/2 \midiTempo#160 \grace s8 s1
\digitTime\time 3/4 \midiTempo#80 \grace s8 s2.
\digitTime\time 2/2 \midiTempo#160 s1*2
\digitTime\time 3/4 \midiTempo#80 s2. \bar "|."
