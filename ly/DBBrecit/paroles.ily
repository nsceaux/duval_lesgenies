Pour im -- mo -- ler u -- ne vic -- ti -- me
le dé -- ses -- poir me con -- duit dans ces lieux,
tu me vois sous ta propre i -- ma -- ge ;
mais c’est pour mieux ser -- vir ma ra -- ge.

Qu’en -- tends- je ?

À mes trans -- ports ja -- loux
re -- con -- nais ta ri -- va -- le.
Pour a -- dou -- cir ma pei -- ne sans é -- ga -- le,
c’est sur toi que je vais fai -- re tom -- ber mes coups.

Bar -- ba -- re, a -- chè -- ve ta ven -- gean -- ce,
hâ -- te- toi de frap -- per mon cœur ;
ne res -- pec -- te dans ta fu -- reur,
ni mes pleurs, ni mon in -- no -- cen -- ce.
U -- nique ob -- jet de mes dé -- sirs
cher I -- das, toi pour qui j’au -- rais ai -- mé la vi -- e,
re -- çois a -- vec mon sang, lors -- qu’el -- le m’est ra -- vi -- e,
mes a -- dieux, mon a -- mour, et mes der -- niers sou -- pirs.

Elle aime un autre a -- mant !

Par -- le, ex -- pli -- que tes lar -- mes.

Je tou -- chais au sort le plus doux,
un tendre a -- mant de -- ve -- nait mon é -- poux,
lors -- qu’un bar -- bare en vint trou -- bler les char -- mes ;
il m’en -- lè -- ve, mal -- gré l’ef -- fort de mon a -- mant :
vo -- tre haine à ce prix, est- el -- le lé -- gi -- ti -- me ?

Non, je ne te hais plus.

Ter -- mi -- nez mon tour -- ment,
que la mê -- me fu -- reur con -- tre moi vous a -- ni -- me.

\ifComplet {
  Im -- pi -- toy -- able a -- mour, n’e -- xi -- ge rien de moi,
  si pour me faire ai -- mer il faut com -- mettre un cri -- me ;
  et ne se -- rais- je pas moi- mê -- me la vic -- ti -- me
  d’un in -- grat que je veux ra -- me -- ner sous ma loi.
  Et ne se -- rais- je pas moi- mê -- me la vic -- ti -- me
  d’un in -- grat que je veux ra -- me -- ner sous ma loi !
  C’en est fait, la pi -- tié tri -- om -- phe de la hai -- ne,
  moi- même à vos mal -- heurs je don -- ne des sou -- pirs ;
  c’est trop vous pa -- raître in -- hu -- mai -- ne,
  je vais ser -- vir mes feux, en ser -- vant vos dé -- sirs.
}

Par quel charme ai- je pu cal -- mer vo -- tre co -- lè -- re ?

Ne crai -- gnez rien, je vais vous rendre à votre a -- mant,
et s’il se peut, par mon dé -- gui -- se -- ment,
trom -- per tou -- jours l’in -- grat qui sait me plai -- re.
