\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix"
          #:instrument "[B.C.]")
   (silence #:on-the-fly-markup , #{
\markup\fontsize #-3 \fill-line {
  \column {
    \livretPers Piscaride
    \livretVerse#8 { Pour immoler une victime }
    \livretVerse#10 { Le désespoir me conduit dans ces lieux, }
    \livretVerse#8 { Tu me vois sous ta propre image ; }
    \livretVerse#8 { Mais c’est pour mieux servir ma rage. }
    \livretPers Isménide
    \livretVerse#8 { Qu’entends-je ? }
    \livretPers Pircaride
    \livretVerse#9 { \transparent { Qu’entends-je ? } À mes transports jaloux }
    \livretVerse#6 { Reconnais ta rivale. }
    \livretVerse#10 { Pour adoucir ma peine sans égale, }
    \livretVerse#12 { C’est sur toi que je vais faire tomber mes coups. }
    \livretPers Isménide
    \livretVerse#8 { Barbare, achève ta vengeance, }
    \livretVerse#8 { Hâte-toi de frapper mon cœur ; }
    \livretVerse#8 { Ne respecte dans ta fureur, }
    \livretVerse#8 { Ni mes pleurs, ni mon innocence. }
    \livretVerse#8 { Unique objet de mes désirs }
    \livretVerse#12 { Cher Idas, toi pour qui j’aurais aimé la vie, }
    \livretVerse#12 { Reçois avec mon sang, lorsqu’elle m’est ravie, }
    \livretVerse#12 { Mes adieux, mon amour, & mes derniers soupirs. }
    \livretPersDidas Pircaride à part.
    \livretVerse#12 { Elle aime un autre amant ! }
    \livretDidasPPage à Isménide.
    \livretVerse#12 { \transparent { Elle aime un autre amant ! } Parle, explique tes larmes. }
  }
  \column {
    \livretPers Isménide
    \livretVerse#8 { Je touchais au sort le plus doux, }
    \livretVerse#10 { Un tendre amant devenait mon époux, }
    \livretVerse#10 { Lorsqu’un barbare en vint troubler les charmes ; }
    \livretVerse#12 { Il m’enlève, malgré l’effort de mon amant : }
    \livretVerse#12 { Votre haine à ce prix, est-elle légitime ? }
    \livretPers Pircaride
    \livretVerse#12 { Non, je ne te hais plus. }
    \livretPers Isménide
    \livretVerse#12 { \transparent { Non, je ne te hais plus. } Terminez mon tourment, }
    \livretVerse#12 { Que la même fureur contre moi vous anime. }
    \livretPers Pircaride
    \livretVerse#12 { Impitoyable amour, n’exige rien de moi, }
    \livretVerse#12 { Si pour me faire aimer il faut commettre un crime ; }
    \livretVerse#12 { Et ne serais-je pas moi-même la victime }
    \livretVerse#12 { D’un ingrat que je veux ramener sous ma loi ! }
    \livretVerse#12 { C’en est fait, la pitié triomphe de la haine, }
    \livretVerse#12 { Moi-même à vos malheurs je donne des soupirs ; }
    \livretVerse#8 { C’est trop vous paraître inhumaine, }
    \livretVerse#12 { Je vais servir mes feux, en servant vos désirs. }
    \livretPers Isménide
    \livretVerse#12 { Par quel charme ai-je pu calmer votre colère ? }
    \livretPers Pircaride
    \livretVerse#12 { Ne craignez rien, je vais vous rendre à votre amant, }
    \livretVerse#10 { Et s’il se peut, par mon déguisement, }
    \livretVerse#10 { Tromper toujours l’ingrat qui sait me plaire. }
    
  }
}#}))
