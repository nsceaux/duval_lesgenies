\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "[B.C.]" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2\break s2.*2 s1. s2. \bar "" \break s2. s1*2\pageBreak
        s1 s2. s2 \bar "" \break s4 s2. s1 s4 \bar "" \break s2 s2. s1 s2.*2\pageBreak
        \grace s8 s2.*2 s1 s2.\break s2.*5\break s2.*4 s1\pageBreak
        s2. s1*2 s2 \bar "" \break s2 s1*2\break s2.*3\pageBreak
        s2.*3 s2 \bar "" \break s2 s1 s2.*2\break s2. s1 s2. s1\pageBreak
        \grace s8 s1*2 s2. s2 \bar "" \break s4 s1. s2.*2\break s2.*2 s1.\pageBreak
        s2. s1 s2.*2 s2 \bar "" \break s2 s1*2\break s2.*4\pageBreak
        s1*2 s2.\break s1*2 s2.
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
