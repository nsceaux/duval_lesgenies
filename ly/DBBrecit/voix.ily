\ffclef "vbas-dessus" <>^\markup\character Pircaride
la'8 la'16 la' re'' re'' re'' fa'' sib'8 sib' r16 re'' re'' do'' |
sib'4 sib'8 la' sol'4\trill sol'8 fa' |
mi'8 la'16 sib' do''8 la'16 la' re''8. la'16 |
\appoggiatura la'8 sib'2 sib'4 |
re''2 sib'8 la' sol' fa' mi'4 la' |
re'2 re'4
\ffclef "vdessus" <>^\markup\character Isménide
re''4 fa''2 |
fa''4
\ffclef "vbas-dessus" <>^\markup\character Pircaride
r16 fa' fa' sol' la'8. fa'16 do''8 la'16 la' |
re''8 re''16 re'' si'8\trill si' r16 si' si' si' mi''8 sol'' |
dod''16 dod'' dod'' re'' \appoggiatura re''8 mi''4 mi''8 la'16 la' fa''8 fa''16 re'' |
sol''4 mi''8 fa''16 sol'' dod''8.\trill re''16 |
re''2
\ffclef "vdessus" <>^\markup\character Isménide
re''4 |
fad' fad'8 la' re''16 re'' re'' fad'' |
si'4 si'8 re''16 dod'' si'4 si'16 la' sol' fad' |
mi'8 la'16 la' fad'8 fad' sol' sol'16 la' |
\appoggiatura la'8 si'4 mi''4. si'8 |
\appoggiatura si'8 dod''2 la'4 re''8 mi''16 fad'' |
fad''4( mi''2)\trill |
re''2 r4 |
r4 r8 la' la' re'' |
\appoggiatura dod'' si'4. la'8 sol' fad' |
mi'4\trill la'8. la'16 \appoggiatura sol'8 fad'4 si'8 si' |
sold'8.\trill mi'16 la'8 si' \appoggiatura si' dod''8. re''16 |
dod''4\trill \appoggiatura si'8 la'4 r8 re'' |
\appoggiatura dod''8 si'4. la'8 si' fad' |
\appoggiatura fad' sol'2. |
sold'4 sold'8 sold' lad' si' |
lad'4\trill lad' r8 dod''16 dod'' |
fad''2 r8 lad'16 dod'' |
fad'2 \ficta sold'4 |
lad'8 si' lad'4.\trill si'8 |
si'2
\ffclef "vbas-dessus" <>^\markup\character-text Pircaride à part
r8 si' |
re''2~ re''8 si' si' si' |
sold'8 r^\markup\italic à Isménide mi''4 mi''8 r16 si' |
re''4 re''8 dod'' dod''4\trill dod''8
\ffclef "vdessus" <>^\markup\character Isménide
la'16 la' |
re''4 fad'' si' dod''8 re'' |
sold'4\trill r8 mi' la'4( sold'8)\trill la' |
si' si'16 dod'' re''8 re''16 dod'' dod''4\trill la'8 la'16 la' |
re''8 la' si' si' dod'' re'' re''8.[ dod''16] |
re''2 r8 fad''16 la'' |
re''8 re''16 re'' la'8 si' do'' do''16 do''32 si' |
si'8\trill si'16 si' mi''8 mi''16 mi'' dod''8. la'16 |
re''4. re''8 mi'' fad'' |
mi''2\trill mi''4 |
\ffclef "vbas-dessus" <>^\markup\character Pircaride
fad''8 fad'16 fad' fad'8. la'16 re'4 |
\ffclef "vdessus" <>^\markup\character Isménide
r8 la'16 la' re''8 re''16 fad'' si'8 sol'16 la' si'8 si'16 dod'' |
re''4 re''8 re'' red''4\trill red''8 mi'' |
mi''2 mi''4 |
\ifComplet {
  \ffclef "vbas-dessus" R2. |
  <>^\markup\character Pircaride sol'4 sol'8 sol' la' si' |
  mi'4 r16 mi'' fad'' sol'' red''4.\trill mi''8 |
  \appoggiatura mi''8 fad''2. |
  si'4 si'8 si' mi''4. si'8 |
  \appoggiatura si'8 do''4. la'8 re'' la' si' do'' |
  si'4\trill \appoggiatura la'8 sol'4 sol'' sol''16 fad'' mi'' re'' |
  do''8. do''16 la'8\trill la' la' la' |
  fad'4\trill fad' r8 si'16 dod'' |
  red''4 red''8 si' mi''4 fad''8 sol'' red''4\trill red''8 mi'' |
  \appoggiatura mi''8 fad''2. |
  sol''4 sol''8 fad'' mi'' re'' |
  do''8. do''16 la'8\trill la' la' la' |
  fad'4\trill fad' r8 si'16 dod'' |
  red''4 red''8 si' mi''4 fad''8 sol'' red''!4\trill red''8 mi'' |
  mi''2 r8 mi''16 mi'' |
  sold'8 si'16 si' mi''8. si'16 re''4 re''8 re''16 dod'' |
  dod''4\trill dod'' r8 la' |
  dod''4\trill dod'' dod''8 red'' |
  \appoggiatura red''8 mi''4 r8 dod'' fad''4 mi''8 mi''16 red''! |
  red''?8\trill r16 si' mi''8 fad''16 sol'' dod''8 red''?16 mi'' lad'8\trill lad'16 fad' |
  si'8 dod'' red'' mi'' \appoggiatura mi'' fad''4 dod''8 dod'' |
  red''4 dod''4.\trill si'8 |
  si'8
  \ffclef "vdessus" <>^\markup\character Isménide
}
\ifConcert { R2. r8 }
si'16 si' mi''8 mi''16 mi'' si'8 dod'' |
re''4. re''8 re'' dod'' |
dod''4\trill dod''
\ffclef "vbas-dessus" <>^\markup\character Pircaride
r16 la' si' dod'' |
fad'4 r16 si' si' si' sold'8.\trill sold'16 la'8. si'16 |
\appoggiatura si'8 dod''4. r8 fad''4 fad''8 dod'' |
\appoggiatura dod'' red''4 red'' red''16 red'' mi'' fad'' |
si'4 mi'' dod''8 dod'' red'' mi'' |
la' la' si' dod'' \appoggiatura sold'8 \afterGrace fad'2(\trill mi'8) |
mi'2. |
