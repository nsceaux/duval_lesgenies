\clef "basse" mi2.~ |
mi~ |
mi~ |
mi4 red2 |
mi2 mi,4 |
si,8 si la si fad si |
sol4 fad mi |
si8 si, red4 si, |
mi4 red8 si sol mi |
la2 red8 si, |
mi4 \clef "dessus2" mi''8 mi'' mi'' sol'' |
do''2 la'8 do'' |
fad'4 fad' si' |
mi'4. sol'8 si' sol' |
re''2 si'8 si' |
mi''4 mi'' dod'' |
fad''4 fad'' fad'8 fad' |
si'4 si' dod'' |
re''8 dod'' si' re'' dod'' si' |
lad'2 fad'8 sold' |
lad'4 si' dod'' |
re''2 re'4 |
mi' mi'8 fad' sol' mi' |
fad'2. |
si'2 si'4 |
sol'2 r8 mi' |
si'16 do'' si' la' sol' la' sol' la' si' dod'' si' dod'' |
red''2 si'4 |
mi''2 mi''8 si' |
do''4 la'4.\trill sol'16 la' |
si'2 si'4 |
sol'2 r8 mi' |
si'16 do'' si' la' sol' la' sol' la' si' dod'' si' dod'' |
red'' mi'' red'' dod'' si' dod'' si' dod'' red'' mi'' red'' mi'' |
fad''2 r8 si' |
mi''16 re'' do'' si' do'' si' la' sol' do'' si' la' sol' |
fad' mi' fad' sol' la' fad' sol' la' si' dod'' red'' si' |
mi''2 sol''4 |
red''2 red''8 mi'' |
la'4 si' si |
mi'4 r8 si' sol' mi' |
do'8 re' sol sol' sol' si' |
mi'4 r8 si' sol' mi' |
la4 si2 |
mi'4 \clef "basse" mi4 fad |
\once\set Staff.whichBar = "|"
