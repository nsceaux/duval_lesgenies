<<
  \tag #'dessus1 {
    \clef "dessus" mi'' mi''8 mi'' mi'' sol'' |
    do'' mi'' la'4 do'' |
    fad'8 la' red'4 fad' |
    si si' fad' |
    sol'16 fad' mi' fad' sol' la' sol' la' si' dod'' si' dod'' |
    red''2. |
    mi''4 fad'' sol'' |
    fad''8 fad' si'4 fad' |
    sol'16 mi' sol' mi' si' si' red'' si' mi'' \ficta re'' do'' si' |
    do'' si' la' sol' do'' si' la' sol' fad'8 si'16 si' |
    sol'4. si''8 si'' si'' |
    si''4. la''16 sol'' la''8 la'' |
    la'' sol'' fad'' sol'' la'' fad'' |
    sol''4. sol''8 sol'' si'' |
    la''2 re'''8 re''' |
    si''4 si'' mi''' |
    dod'''8 si'' dod''' re''' mi''' dod''' |
    re'''4 re''' dod''' |
    si''4. si''8 lad'' si'' |
    fad''2 fad''8 fad'' |
    mi'' sol'' fad'' si'' lad''4 |
    si''2 si''8 la'' |
    sol'' la'' sol'' fad'' mi'' re'' |
    re''4( dod''4.)\trill si'8 |
    si'4. si'8 red'' fad'' |
    si''4 r8 si' mi'' sol'' |
    red'' fad'' si' si'' la'' sol'' |
    fad''4. si'8 red'' fad'' |
    sol'' la'' si'' la'' si'' la'' |
    sol''4 do'''8 si'' la'' sol'' |
    fad'' si' red'' fad'' red'' si' |
    mi'' si' mi'' sol'' mi'' sol'' |
    red''8 si' mi'' sol'' fad'' mi'' |
    si''4. la''16 sol'' fad'' sol'' fad'' mi'' |
    red'' mi'' fad'' sol'' la'' si'' la'' sol'' fad'' sol'' la'' fad'' |
    sol''4 \appoggiatura fad''8 mi'' mi'' fad'' sol'' |
    la''4 la''4. sol''16 fad'' |
    sol'' fad'' mi'' red'' mi'' fad'' sol'' la'' si'' si'' la'' sol'' |
    fad''2 fad''8 sol'' |
    do'''16 si'' la'' sol'' sol''4( fad''8.)\trill mi''16 |
    mi'' sol'' fad'' mi'' si'' la'' sol'' fad'' sol'' la'' si'' sol'' |
    do''' si'' la'' do''' si'' la'' sol'' fad'' sol'' fad'' mi'' red'' |
    mi'' fad'' sol'' la'' si'' la'' sol'' fad'' sol'' la'' si'' sol'' |
    do''' si'' la'' sol'' fad''4.\trill mi''8 |
    mi''2. |
  }
  \tag #'dessus2 {
    \clef "dessus" sol'4 sol'8 sol' sol' sol' |
    sol' mi' do''4 la' |
    red' si' si'8 fad' |
    sol'4 fad'8 la' red' fad' |
    si si' si' mi'' sol'' la''16 sol'' |
    fad''4. red''8 mi'' fad'' |
    si' mi'' red'' fad'' si' mi'' |
    red'' si'' fad'' si'' red'' fad'' |
    si'4 si'4. si'8 |
    mi'8. mi'16 la' sol' fad' mi' si'8. si'16 |
    si'4. sol''8 sol'' sol'' |
    sol''4. fad''16 mi'' fad''8 mi'' |
    mi''4 red''8 mi'' fad'' red'' |
    mi''4. re''8 re'' sol'' |
    fad''2 si''8 si'' |
    sold''4 sold'' dod''' |
    lad''8 sold'' lad'' si'' dod''' lad'' |
    si''4 fad'' fad'' |
    fad''8 mi'' re'' fad'' mi'' re'' |
    dod''2 dod''8 dod'' |
    dod''4 re''! mi'' |
    fad''2 fad''4 |
    si' si''8 re''' \ficta dod''' si'' |
    si''4 lad''4. si''8 |
    si''8 si' red'' fad'' si''4 |
    r8 si' mi'' sol'' si''4 |
    fad''8 red'' mi'' sol'' fad'' mi'' |
    si'' si' red'' fad'' si' si'' |
    si'' la'' sol'' fad'' mi'' red'' |
    mi''4 fad''8 sol'' fad'' mi'' |
    red'' fad'' si'' si' fad'' red'' |
    si' mi'' sol'' si'' sol'' mi'' |
    fad''8 red'' si' si'' la'' sol'' |
    fad''4 si'' si'' |
    la''16 si'' la'' sol'' fad'' sol'' fad'' mi'' red'' mi'' fad'' red'' |
    mi''4 mi''16 re'' do'' si' mi'' re'' do'' si' |
    do''8 do'''16 si'' la'' sol'' fad'' mi'' red'' mi'' fad'' red'' |
    mi'' red'' mi'' fad'' sol'' fad'' mi'' red'' mi'' sol'' fad'' mi'' |
    si''4. si''8 la'' sol'' |
    fad'' mi'' red''4.\trill mi''8 |
    mi'' si' sol''16 fad'' mi'' red'' mi'' fad'' sol'' mi'' |
    la'' sol'' fad'' la'' sol'' fad'' sol'' la'' si'' la'' sol'' fad'' |
    sol'' fad'' mi'' fad'' sol'' fad'' mi'' red'' mi'' fad'' sol'' mi'' |
    la'' sol'' fad'' mi'' red''4.\trill mi''8 |
    mi''2. |
  }
  \tag #'dessus3 {
    \clef "dessus2" R2.*10 |
    r4 mi''8 mi'' mi'' sol'' |
    do''2 la'8 do'' |
    fad'4 fad' si' |
    mi'4. sol'8 si' sol' |
    re''2 si'8 si' |
    mi''4 mi'' dod'' |
    fad''4 fad'' fad'8 fad' |
    si'4 si' dod'' |
    re''8 dod'' si' re'' dod'' si' |
    lad'2 fad'8 sold' |
    lad'4 si' dod'' |
    re''2 re'4 |
    mi' mi'8 fad' sol' mi' |
    fad'2. |
    si'2 si'4 |
    sol'2 r8 mi' |
    si'16 do'' si' la' sol' la' sol' la' si' dod'' si' dod'' |
    red''2 si'4 |
    mi''2 mi''8 si' |
    do''4 la'4.\trill sol'16 la' |
    si'2 si'4 |
    sol'2 r8 mi' |
    si'16 do'' si' la' sol' la' sol' la' si' dod'' si' dod'' |
    red'' mi'' red'' dod'' si' dod'' si' dod'' red'' mi'' red'' mi'' |
    fad''2 r8 si' |
    mi''16 re'' do'' si' do'' si' la' sol' do'' si' la' sol' |
    fad' mi' fad' sol' la' fad' sol' la' si' dod'' red'' si' |
    mi''2 sol''4 |
    red''2 red''8 mi'' |
    la'4 si' si |
    mi'4 r8 si' sol' mi' |
    do'8 re' sol sol' sol' si' |
    mi'4 r8 si' sol' mi' |
    la4 si2 |
    mi'2. |
  }
>>
