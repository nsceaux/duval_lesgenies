Vous qui m’o -- bé -- is -- sez, pa -- rais -- sez à mes yeux,
ve -- nez, ve -- nez si -- gna -- ler ma puis -- san -- ce ;
ra -- me -- nez cet ob -- jet dans les ai -- ma -- bles lieux,
où l’a -- mour doit bien -- tôt cou -- ron -- ner sa cons -- tan -- ce ;
par -- tez, vo -- lez, __ ser -- vez ses dé -- sirs a -- mou -- reux.
Par -- tez, vo -- lez, __ vo -- lez, __ ser -- vez ses dé -- sirs a -- mou -- reux.
