\piecePartSpecs
#`((dessus #:score "score-dessus")
   (flute-hautbois #:score-template "score-2dessus"
                   #:instrument "Violons")
   (dessus1 #:score "score-dessus1")
   (dessus2 #:score "score-dessus2")
   (basse #:score-template "score-basse-continue-voix"
          #:instrument
          ,#{ \markup\center-column { [Basses et B.C.] } #})

   (silence #:on-the-fly-markup , #{
\markup\fontsize#-2 \column {
  \livretPers Pircaride
  \livretVerse#12 { Vous qui m’obéissez, paraissez à mes yeux, }
  \livretVerse#8 { Venez signaler ma puissance ; }
  \livretVerse#12 { Ramenez cet objet dans les aimables lieux, }
  \livretVerse#12 { Où l’amour doit bientôt couronner sa constance ; }
  \livretVerse#12 { Partez, volez, servez ses désirs amoureux. }
}#}))
