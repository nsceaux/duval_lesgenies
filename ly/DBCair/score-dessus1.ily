\score {
  \new GrandStaff \with { instrumentName = "Violons" } <<
    \new Staff << \global \keepWithTag#'dessus1 \includeNotes "dessus" >>
    \new Staff \with { \haraKiriFirst } <<
      { s2.*10 s4 <>^"Violons III" }
      \global \keepWithTag#'dessus3 \includeNotes "dessus"
    >>
  >>
  \layout { indent = \largeindent }
}
