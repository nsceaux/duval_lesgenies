\score {
  \new ChoirStaff <<
    \new GrandStaff \with { instrumentName = "Violons" } <<
      \new Staff << \global \keepWithTag#'dessus1 \includeNotes "dessus" >>
      \new Staff << \global \keepWithTag#'dessus2 \includeNotes "dessus" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Pircaride
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*8\pageBreak
        s2.*5 s2 \bar "" \break s4 s2.*5\pageBreak
        s2.*6\break s2.*7\pageBreak
        s2.*4\break s2.*5\pageBreak
        s2.*4
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
