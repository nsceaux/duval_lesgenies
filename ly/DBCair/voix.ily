\clef "vbas-dessus" R2.*10 |
mi''4 mi''8 mi'' mi'' sol'' |
do''2 la'8 do'' |
fad'4 fad' si' |
mi'4. sol'8 si' sol' |
re''2 si'8 si' |
mi''4 mi'' dod'' |
fad'' fad'' fad'8 fad' |
si'4 si' dod'' |
re''8 dod'' si' re'' dod'' si' |
lad'2\trill fad'8 sold' |
lad'4 si' dod'' |
re''2 re'8 re' |
mi'4 mi'8[ fad'] sol'[ mi'] |
fad'2. |
si'2 si'4 |
sol'2 r8 mi' |
si'16[\melisma do'' si' la'] sol'[ la' sol' la'] si'[ dod'' si' dod'']( |
red''2)\melismaEnd si'4 |
mi''2 mi''8 si' |
do''4 la'4.\trill sol'16([ la']) |
si'2 si'4 |
sol'2 r8 mi' |
si'16[\melisma do'' si' la'] sol'[ la' sol' la'] si'[ dod'' si' dod''] |
red''[ mi'' red'' dod''] si'[ dod'' si' dod''] red''[ mi'' red'' mi'']( |
fad''2)\melismaEnd r8 si' |
mi''16[\melisma re'' do'' si'] do''[ si' la' sol'] do''[ si' la' sol'] |
fad'[ mi' fad' sol'] la'[ fad' sol' la'] si'[ dod'' red'' si']( |
mi''2)\melismaEnd sol''4 |
red''2 red''8 mi'' |
la'8[ si'16 do''] si'4. si'8 |
mi'2 r4 |
R2.*4 |
