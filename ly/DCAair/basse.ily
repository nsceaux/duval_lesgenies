\clef "basse" sol2.~ |
sol~ |
sol4 fad2 |
sol4. la8 sol8 fad |
mi2 la la, |
re, re4 |
re fad, sol,8 sol mi4 |
la8 sol fad4 re |
sol8 fad mi re la4 la, |
re2 r4 |
re,2 r4 |
re la, mi8 re do4 |
si, si la |
sold2 mi |
la si4 si, |
mi2.~ |
mi8 re do si, la, la fad4 |
sol2. |
sold4 mi la la8 si do'4 la |
si2. la4 |
sold8 mi la fad si la sol? fad16 mi |
si2 la4 |
sol fad mi2 |
re4 r do2 |
sol,4. la,8 si,2 |
\custosNote mi,8 \stopStaff
