\clef "dessus" R2. |
r4 r r8 do''\doux |
la'2 la'4 |
re'2 re'4 |
re'2. re'4 dod'4.\trill( si16 dod') |
re'2 r8 fad' |
fad'4. la'8 re' re'' sol''8. sol''16 |
mi''8\trill dod'' re''4. re''8 |
re''4 mi''8 fad'' mi''4.\trill re''8 |
re''4. re''8 fad'' la'' |
re'' la' fad'4 r |
r8 fad'16 sold' la'4 sold'8 si' si' la' |
si'4. si'8 red'' fad'' |
si'4 r sold'4\trill( fad'8) sold' |
\appoggiatura sold'8 la'4. mi'8 mi'4( red'8.)\trill mi'16 |
mi'4. mi'8 sol' si' |
mi' mi' fad' sold' la'4. la'8 |
re'4. re''8 sol''8. la''16 |
si''4. si''8 si''4 la''8 sol'' fad''4 fad''8 fad'' |
red''4~ red''16 red'' red'' red'' red''4 fad'' |
si'8 si' mi' la' fad'8 si' si'16 dod'' red'' mi'' |
red''4.\trill fad''8 red'' si' |
mi''2. r8 si' |
si'4 r mi'2~ |
mi'4. mi'8 mi'4.( red'8) |
mi'1 |
