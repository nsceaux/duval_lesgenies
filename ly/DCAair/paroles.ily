El -- le part, et mon cœur n’est point e -- xempt d’a -- lar -- mes !
C’est sous ses traits qu’a -- mour vient flat -- ter mon ar -- deur ;
quel -- le hon -- te ! mes yeux, pour tou -- cher mon vain -- queur,
vous a -- vez be -- soin d’au -- tres char -- mes !
C’est en vain que l’a -- mour veut ras -- su -- rer mon cœur,
je ne sau -- rais cal -- mer l’en -- nui qui me dé -- vo -- re ;
je vais m’of -- frir aux yeux de l’a -- mant que j’a -- do -- re,
j’en -- ten -- drai des sou -- pirs pour un au -- tre que moi !
Il m’ex -- pri -- me -- ra sa ten -- dres -- se,
tan -- dis qu’il me man -- que de foi ;
ô dieux ! il vient, ca -- chons ma honte et ma fai -- bles -- se.
