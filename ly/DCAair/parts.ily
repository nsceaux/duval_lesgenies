\piecePartSpecs
#`((dessus #:instrument "[Violons]"
           #:score-template "score-voix")
   (dessus2 #:instrument "[Violons]"
            #:score-template "score-voix")
   (basse #:score-template "score-basse-continue-voix"
          #:instrument
          ,#{ \markup\center-column { [Basses et B.C.] } #})

   (silence #:on-the-fly-markup , #{
\markup\fontsize#-2 \column {
  \livretPers Pircaride
  \livretVerse#12 { Elle part, & mon cœur n’est point exempt d’alarmes ! }
  \livretVerse#12 { C’est sous ses traits qu’amour vient flatter mon ardeur ; }
  \livretVerse#12 { Quelle honte ! mes yeux, pour toucher mon vainqueur }
  \livretVerse#8 { Vous avez besoin d’autres charmes ! }
  \livretVerse#12 { C’est en vain que l’amour veut rassurer mon cœur, }
  \livretVerse#12 { Je ne saurais calmer l’ennui qui me dévore ; }
  \livretVerse#12 { Je vais m’offrir aux yeux de l’amant que j’adore, }
  \livretVerse#12 { J’entendrai des soupirs pour un autre que moi ! }
  \livretVerse#8 { Il m’exprimera sa tendresse, }
  \livretVerse#8 { Tandis qu’il me manque de foi ; }
  \livretVerse#12 { Ô dieux ! il vient, cachons ma honte & ma faiblesse. }
}#}))
