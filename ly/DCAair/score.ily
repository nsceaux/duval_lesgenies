\score {
  \new ChoirStaff <<
    \new Staff \with { instrumentName = "[Violons]" } <<
      \global \includeNotes "dessus"
    >>
    \new Staff \with {
      instrumentName = \markup\character Pircaride
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*3 s2 \bar "" \break s4 s1. s2. s2. \bar "" \pageBreak
        s4 s2. s1 s2.*2\break s1 s2. s1 s2 \bar "" \pageBreak
        s2 s2. s1 s2.\break s1. s1 s2 \bar "" \pageBreak
        s2 s2. s1*2 s2 \bar "" \break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
