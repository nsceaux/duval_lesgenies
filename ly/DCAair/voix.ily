\clef "vbas-dessus" r4 r si'8. sol'16 |
re''2~ re''8 la'16 si' |
\appoggiatura si'8 do''8. do''16 la'8.\trill la'16 si'8. do''16 |
si'4\trill \appoggiatura la'8 sol'4 si'8 si'16 re'' |
sol'4. sol'8 mi'4 mi'8 mi' la'4 la'8 la' |
fad'2\trill r8 re''16 re'' |
la'8 la' do''4 si'8 si'16 si' mi''8 mi''16 mi'' |
dod''4\trill r8 la'16 la' la'8. fad'16 |
si'4 dod''8. re''16 re''4.( dod''8) |
\appoggiatura dod''8 re''2 r4 |
r r re''8 re'' |
la'8 la'16 si' \appoggiatura si'8 do''4 si'8 si'16 si' mi''8 fad'' |
red''2\trill r4 |
r8 si' si' si' si'4( la'8) si' |
\appoggiatura si'8 do''4. la'8 fad'4 si'8 si'16 fad' |
\appoggiatura fad'8 sol'4 \appoggiatura fad'8 mi'4 r8 sol' |
sol' sol' la' si' \appoggiatura si' do'' la'16 la' re''8 re''16 re'' |
si'4\trill si' r8 si'16 do'' |
re''4 re''8 \appoggiatura do'' si' \appoggiatura si' do''4. do''16 do'' la'4. la'16 la' |
fad'2\trill r4 si'16 si' si' dod'' |
\appoggiatura dod''8 re'' re''16 mi'' dod''8\trill dod''16 fad'' red''8 red''16 red'' mi''8 fad''16 sol'' |
fad''4 fad''2 |
si'2. r8 mi'' |
sold'4 r8 do'' la'4. fad'8 |
si'4 si'8 fad'16 sol' sol'4( fad')\trill |
mi'1 |
