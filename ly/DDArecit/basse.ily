\clef "basse" mi,8 mi' re' do' si la sol fad |
mi fad sol mi do' re' do' si |
la si do' la si si, red fad |
si si, red si, mi mi sol mi |
si re' dod' si lad fad sold lad |
si la sol fad sol fad mi sol |
fad mi re mi fad4 fad, |
si,8 si do' si la sol fad la |
red fad si la sol fad mi sol |
do mi la sol fad mi red fad |
la, do si, mi red fad la fad |
si si, dod red mi red mi fad |
sol la si sol do' re' do' si |
la sol la si do' si do' la |
si la sol la si4 si, |
<< { mi2 r4 } \\ { <>_"[B.C.]" mi,2.~ mi,2 } >> mi4 |
red2. |
mi4. dod8 fad4 fad, |
si,2. si,4 |
la,4. sold,8 fad,4. sold,8 la,4 fad, |
dod4. re8 dod si, |
la, fad, sold, la, si,1 |
\ifComplet {
  mi,2 mi |
  re dod |
  si,2. mi8 re |
  dod4. si,16 la, mi4 mi, |
  la,1 | \allowPageTurn
  dod4 re2 re4 |
  dod2 sold, |
  la,4 la sold |
  la2 fad4 |
  si2 la8 sold fad si |
  mi2. |
  fad4 re mi mi, |
  la,2. |
  dod |
  re2. re4 |
  dod2 fad fad, |
  si,4. la,8 sold,4 mi, |
  la,2 la8 sold fad mi la4. si8 |
  mi2. re8 dod |
  si, si sold mi la4. re8 |
  la,2. | \allowPageTurn
  dod4 si,8 la, si,2 |
  mi, sol4 mi |
  la8 sol fad mi si4. la8 |
  sol2 la4 sol8 fad |
  mi2 mi'8 re' do' si |
  la4 si do'2 |
  si8 do' si la sol4 mi |
  si2 si4 si8 la |
  sold4. mi8 fad4 sold |
  la4 sol fad4. mi8 re4. dod8 |
  re4 re, re mi8 fad |
  sol fad mi re dod4. si,8 |
  fad,4. re8 mi4. re8 dod si, fad fad, |
  si,4 si8 la sol2 |
  sol, sol,8 fad,16 mi, si,4 |
  mi2 mi8 re do si, |
  la,4 si,8 do re4 re, |
  sol,2 sol4 fad8 mi |
  re4 mi fad si, |
  fad, fad8 mi red dod red si, |
  mi re! dod si, fad4 fad, |
  si,2 si,4 la,8 sol, |
  fad,2 mi, |
  si,2. la,4 |
  sol,4 fad,8 mi, si,2 | \allowPageTurn
  mi2. mid4 |
  fad4 mi red8 dod |
  si,4 fad red?8 si, fad, fad16 sold |
  la8 fad si mi la,4 si, |
  mi2 la |
  la2 fad4 |
  si8 mi si,4 si4. si,8 |
  mi4 red8 dod16 si, fad8 fad, |
  si,2.~ | \allowPageTurn
  si,2 red |
  mi2. |
  sold,1 |
  la,2 sold,4 |
  fad, si,2 |
  mi2. | \allowPageTurn
  la4 fad si mi |
  si, red si, |
  mi r mi2 |
  red4 si, mi2 mid |
  fad4 fad8 mi red4 dod8 si, |
  lad,4 fad,8 sold, lad,4 fad, |
  si, si8 la sold mi |
  la sold fad mi si4 si, |
  mi8 mi' re' dod' si la |
  sold4 mi la fad si4. mi8 |
  si,2 si,4 |
  mi8 re dod si, la, si, dod la, |
  si,2. |
  mi, | \allowPageTurn
  mi4 mi8 re do si, |
  la,2. |
  do4 si,8 la, sol, fad, |
  mi,4. mi8 do mi la, la |
  fad2. re4 |
  sol2 fad |
  mi fad4. sol8 re4 fad, |
  sol,8 fad, sol, la, sol, fad, |
}
mi,2 mi4 |
la sol fad |
mi8 fad16 sol fad4. mi8 |
si,8 si la sol fad mi |
red si, red fad si la |
sol4 sol fad |
mi8 fad sol4 mi |
fad8 mi red mi fad si, |
sol fad mi fad sol mi |
la sol fad sol la fad |
si fad red si, si la |
sold fad mi fad sold mi |
la4 mi8 re do si, |
la, sol, la, si, do la, |
si, do si, la, sol, fad, |
mi,2 si,8 si, |
mi4 sol mi |
fad la fad |
sol8 sol, la, si, do la, |
si,4. si,8 dod red |
mi8 fa mi re do si, |
la, sol, la, si, do la, |
si,2. |
mi |
