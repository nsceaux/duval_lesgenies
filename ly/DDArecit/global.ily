\key mi \minor
\digitTime\time 2/2 \midiTempo#160 s1*15 \bar "||"
\key mi \major
\digitTime\time 3/4 \midiTempo#80 s2.*3
\time 4/4 s1
\digitTime\time 2/2 \midiTempo#160 s1
\time 3/2 s1.
\digitTime\time 3/4 \midiTempo#80 \grace s8 s2.
\time 3/2 \midiTempo#160 s1.
\ifComplet {
  \digitTime\time 2/2 s1*2
  \time 4/4 \midiTempo#80 s1*4
  \digitTime\time 2/2 \midiTempo#160 s1
  \digitTime\time 3/4 \midiTempo#80 s2.*2
  \time 4/4 s1
  \digitTime\time 3/4 \grace s8 s2.
  \digitTime\time 2/2 \midiTempo#160 s1
  \digitTime\time 3/4 \midiTempo#80 s2.*2
  \time 4/4 s1
  \time 3/2 \midiTempo#160 s1.
  \digitTime\time 2/2 s1
  \time 3/2 s1.
  \time 4/4 \midiTempo#80 s1
  \digitTime\time 2/2 s1
  \digitTime\time 3/4 \grace s8 s2.
  \time 4/4 s1
  \digitTime\time 2/2 \midiTempo#160 s2 \bar "||"
  \key mi \minor \beginMark "Mineur Air" s2
  \bar ".!:" \grace s8 s1*4 \alternatives s1 s1 s1
  \time 3/2 s1.
  \digitTime\time 2/2 s1*2
  \time 3/2 s1.
  \digitTime\time 2/2 s1
  \time 4/4 \midiTempo#80 s1
  \digitTime\time 2/2 \midiTempo#160 s1*10
  \time 4/4 \midiTempo#80 s1
  \digitTime\time 3/4 s2.
  \time 4/4 s1*2
  \key mi \major s1 \digitTime\time 3/4 s2.
  \time 4/4 s1
  \digitTime\time 3/4 s2.*2
  \digitTime\time 2/2 \midiTempo#160 s1
  \digitTime\time 3/4 \midiTempo#80 \grace s8 s2.
  \digitTime\time 2/2 \midiTempo#160 s1
  \digitTime\time 3/4 \midiTempo#80 s2.*3
  \time 4/4 \grace s8 s1
  \digitTime\time 3/4 \grace s8 s2.
  \digitTime\time 2/2 \midiTempo#160 s1
  \time 3/2 s1.
  \digitTime\time 2/2 s1*2
  \digitTime\time 3/4 \midiTempo#80 s2.
  \digitTime\time 2/2 \midiTempo#160 \grace s8 s1
  \digitTime\time 3/4 \midiTempo#80 \grace s8 s2.
  \time 3/2 \midiTempo#160 s1.
  \digitTime\time 3/4 \midiTempo#80 \grace s8 s2.
  \digitTime\time 2/2 s1
  \digitTime\time 3/4 s2.*2 \bar "||"
  \key mi \minor
  \beginMarkSmall "Mineur" s2.*3
  \time 4/4 \grace s8 s1
  \digitTime\time 2/2 \midiTempo#160 s1*2
  \time 3/2 s1.
  \digitTime\time 3/4 \midiTempo#120 s2.
}
\ifConcert { \digitTime\time 3/4 \midiTempo#120 }
\tempo "Gracieusement" s2.*24 \bar "|."
