Je sens, en vous voy -- ant ac -- croî -- tre mon ar -- deur,
mil -- le feux dé -- vo -- rent mon â -- me ;
vous a -- vez par vos yeux al -- lu -- mé plus de flam -- me
que n’en sau -- rait al -- lu -- mer ma fu -- reur.
\ifComplet {
  Eh bien, cru -- el -- le que vous ê -- tes,
  n’au -- rez- vous point pi -- tié des maux que vous me fai -- tes ?

  Non, rien n’é -- ga -- le ceux que tu me fais souf -- frir :
  sous ce fa -- tal a -- mour tu sais ca -- cher ta hai -- ne,
  hé -- las ! si tu m’ai -- mais, tu fi -- ni -- rais ma pei -- ne,
  mais tu veux me lais -- ser mou -- rir.

  Dieux ! pou -- vez- vous me faire un si san -- glant ou -- tra -- ge !
  Dou -- ter de mon a -- mour, lors -- que je meurs pour vous ;
  qui pour -- rait me por -- ter de plus sen -- si -- bles coups ?
  Mes sou -- pirs, mes trans -- ports, ma lan -- gueur et ma ra -- ge,
  si vous ne les croy -- ez, quel té -- moin croi -- rez- vous ?

  Aime un cœur qui t’a -- dore, et fuis une in -- hu -- mai -- ne,
  fais ton bon -- heur d’ê -- tre cons -- tant ;
  Aime un -tant ;
  dois- je comp -- ter sur un a -- mant
  qui brise u -- ne si bel -- le chaî -- ne,
  dois- je comp -- ter sur un a -- mant
  qui brise u -- ne si bel -- le chaî -- ne.

  Non, __ je ne l’ai -- me -- rai ja -- mais,
  tout vous en don -- ne l’as -- su -- ran -- ce ;
  pour ê -- tre sur de ma cons -- tan -- ce,
  il fal -- lait a -- voir vos at -- trait,
  pour ê -- tre sur de ma cons -- tan -- ce,
  il fal -- lait a -- voir vos at -- trait.

  D’une a -- mante ou -- tra -- gée é -- vi -- tez la van -- gean -- ce.

  Pour dé -- fen -- dre vos jours j’au -- rai plus de puis -- san -- ce ;
  je vous aime, Is -- mé -- nide, au -- tant que je la hais.

  Le per -- fi -- de ! Ai -- mez- moi s’il se peut da -- van -- ta -- ge,
  pour par -- ta -- ger les maux de mon triste es -- cla -- va -- ge.
  Hé -- las !

  Vous sou -- pi -- rez, vos yeux ver -- sent des pleurs,
  ah ! si pour moi, l’a -- mour fai -- sait cou -- ler ces lar -- mes !

  C’est lui qui cau -- se mes a -- lar -- mes.
  Je n’ai pu re -- sis -- ter à ses at -- traits vain -- queurs,
  il tri -- omphe, et tou -- jours sous de fein -- tes ri -- gueurs,
  j’ai vou -- lu ca -- cher ma ten -- dres -- se,
  c’est as -- sez dé -- gui -- ser… c’est pour vous qu’il me bles -- se…

  Que mon sort est heu -- reux !
  Je suis au com -- ble de mes vœux.

  Vous, que ma voix ap -- pel -- le,
  ve -- nez, par vos trans -- ports me mar -- quer vo -- tre zè -- le,
  de ces cli -- mats brû -- lants où s’é -- tend mon pou -- voir,
  ac -- cou -- rez, ve -- nez tous cé -- lé -- brer vo -- tre rei -- ne.
}
Que vos yeux en -- chan -- tés du plai -- sir de la voir,
ap -- plau -- dis -- sent au choix que je fais de sa chaî -- ne.
Que vos yeux en -- chan -- tés du plai -- sir de la voir,
ap -- plau -- dis -- sent au choix que je fais de sa chaî -- ne,
ap -- plau -- dis -- sent au choix que je fais de sa chaî -- ne.
