\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix"
          #:instrument ,#{ \markup\center-column { Toutes les Basses }#})
   (silence #:on-the-fly-markup , #{
\markup\fontsize #-3 \fill-line {
  \column {
    \livretPers Numapire
    \livretVerse#12 { Je sens, en vous voyant accroître mon ardeur, }
    \livretVerse#8 { Mille feux dévorent mon âme ; }
    \livretVerse#12 { Vous avez par vos yeux allumé plus de flamme }
    \livretVerse#10 { Que n’en saurait allumer ma fureur. }
    \livretVerse#8 { Eh bien, cruelle que vous êtes, }
    \livretVerse#12 { N’aurez-vous point pitié des maux que vous me faites ? }
    \livretPers Pircaride
    \livretVerse#12 { Non, rien n’égale ceux que tu me fais souffrir : }
    \livretVerse#12 { Sous ce fatal amour tu sais cacher ta haine, }
    \livretVerse#12 { Hélas ! si tu m’aimais, tu finirais ma peine, }
    \livretVerse#8 { Mais tu veux me laisser mourir. }
    \livretPers Numapire
    \livretVerse#12 { Dieux ! pouvez-vous me faire un si sanglant outrage ! }
    \livretVerse#12 { Douter de mon amour, lorsque je meurs pour vous ; }
    \livretVerse#12 { Qui pourrait me porter de plus sensibles coups ? }
    \livretVerse#12 { Mes soupirs, mes transports, ma langueur & ma rage, }
    \livretVerse#12 { Si vous ne les croyez, quel témoin croirez-vous ? }
    \livretPers Pircaride
    \livretVerse#12 { Aime un cœur qui t’adore, & fuis une inhumaine, }
    \livretVerse#8 { Fais ton bonheur d’être constant ; }
    \livretVerse#8 { Dois-je compter sur un amant }
    \livretVerse#8 { Qui brise une si belle chaîne. }
    \livretPers Numapire
    \livretVerse#8 { Non, je ne l’aimerai jamais, }
    \livretVerse#8 { Tout vous en donne l’assurance ; }
    \livretVerse#8 { Pour être sur de ma constance, }
    \livretVerse#8 { Il fallait avoir vos attrait. }
  }
  \column {
    \livretPers Pircaride
    \livretVerse#12 { D’une amante outragée évitez la vangeance. }
    \livretPers Numapire
    \livretVerse#12 { Pour défendre vos jours j’aurai plus de puissance ; }
    \livretVerse#12 { Je vous aime, Isménide, autant que je la hais. }
    \livretPersDidas Pircaride à part.
    \livretVerse#12 { Le perfide ! aimez-moi s’il se peut davantage, }
    \livretVerse#12 { Pour partager les maux de mon triste esclavage. }
    \livretVerse#12 { Hélas ! }
    \livretPers Numapire
    \livretVerse#12 { \transparent { Hélas ! } Vous soupirez, vos yeux versent des pleurs, }
    \livretVerse#12 { Ah ! si pour moi, l’amour faisait couler ces larmes ! }
    \livretPers Pircaride
    \livretVerse#8 { C’est lui qui cause mes alarmes. }
    \livretVerse#12 { Je n’ai pu resister à ses attraits vainqueurs, }
    \livretVerse#12 { Il triomphe, & toujours sous de feintes rigueurs, }
    \livretVerse#8 { J’ai voulu cacher ma tendresse, }
    \livretVerse#12 { C’est assez déguiser… c’est pour vous qu’il me blesse… }
    \livretPers Numapire
    \livretVerse#6 { Que mon sort est heureux ! }
    \livretVerse#8 { Je suis au comble de mes vœux. }
    \livretPers Numapire
    \livretVerse#6 { Vous, que ma voix appelle, }
    \livretVerse#12 { Venez, par vos transports me marquer votre zèle, }
    \livretVerse#12 { De ces climats brûlants où s’étend mon pouvoir, }
    \livretVerse#12 { Accourez, venez tous célébrer votre reine ; }
    \livretVerse#12 { Que vos yeux enchantés du plaisir de la voir, }
    \livretVerse#12 { Applaudissent au choix que je fais de sa chaîne. }
    
  }
}#}))
