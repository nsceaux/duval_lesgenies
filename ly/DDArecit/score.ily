\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = \markup\center-column { Toutes les Basses } } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion { s1*15\break }
      \origLayout {
        s1*4\break s1*6\break s1*5\break s2.*3 s1\break s1 s1. s2. s2 \bar "" \pageBreak
        s1 s1*2 s2. \bar "" \break s4 s1*2 s4 \bar "" \break s2. s1 s2.*2\pageBreak
        s1 s2. s1\break s2.*2 s1 s2 \bar "" \break s1 s1 s1.\pageBreak
        s1*2 s2. s2 \bar "" \break s2 s1*3\break \grace s8 s1*5\pageBreak
        s1. s1*2 s1 \bar "" \break s2 s1*3\break \grace s8 s1*4 s2 \bar "" \pageBreak
        s2 s1*4\break s1 s2.\break s1*2\pageBreak
        s1 s2. s1\break s2.*2 s1 s2.\break s1 s2.*2\pageBreak
        s2. s1 s2. s1 s1.\break s1*2 s2.\break \grace s8 s1 s2. s1. s2.\pageBreak
        s1 s2.*3\break \grace s8 s2.*2 s1\break s1*2 s1. s2.\pageBreak
        s2.*6 s4 \bar "" \break s2 s2.*6\break \grace s8 s2.*6\pageBreak
        s2.*5
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
