\ffclef "vbasse" R1*15 |
<>^\markup\character Numapire
r4 r r8 si, |
mi4. fad8 sold la |
\appoggiatura la si8. fad16 la4 la8 la16 sold |
sold4\trill r8 si16 mi' dod'8. dod'16 lad8\trill lad16 si |
si2 si4 r8 si16 si |
fad4 fad8 sold la4 la8 si dod'4 dod'8 red' |
\appoggiatura red'8 mi'2 mi'4 |
la4 si8 dod' fad4 fad8 si red4\trill red8 mi |
\ifConcert { mi2 }
\ifComplet {
  mi2. r8 mi |
  si4 re' lad4\trill lad8 lad16 si |
  si4 si8. si16 si si dod' re' sold8.\trill mi16 |
  la4 la8 si16 dod' \afterGrace si2\trill( la8) |
  la4
  \ffclef "vbas-dessus" <>^\markup\character Pircaride
  mi''4 mi''16 re'' dod'' si' la'8. la'16 |
  la'16 sol' sol' fad' fad'4\trill re''8 re''16 re'' fad'8. sol'16 |
  la'4 la' si' si'8 dod''16 re'' |
  dod''4\trill dod''8 r mi''4 |
  dod''\trill r8 dod'' dod'' fad'' |
  red''4 si'8 si'16 dod'' red''8. mi''16 mi''8.[ red''16] |
  \appoggiatura red''8 mi''4 r si'8. mi''16 |
  la'4 si'8 dod'' si'4.\trill la'8 |
  la'4
  \ffclef "vbasse" <>^\markup\character Numapire
  mi'4 mi'16 re' dod' si |
  la4 la8 la16 la si8. dod'16 |
  fad4 fad8 r16 re fad4\trill fad8 fad16 sold |
  la2 lad4 lad8 lad lad4. si8 |
  si4 r8 fad16 fad si4 si8. sold16 |
  dod'2 dod'4 red'8 mi' la4. si8 |
  mi4 r8 si16 si si4 r8 si16 dod' |
  re'4 r8 re'16 mi' dod'4\trill dod'8 re' |
  \appoggiatura re'8 mi'2 mi'4 |
  la8 la16 la si8. dod'16 fad8 fad16 si red8\trill red16 mi |
  mi2
  \ffclef "vbas-dessus" <>^\markup\character Pircaride
  si'4 mi'' |
  \appoggiatura re''8 dod''4 red''8 mi'' red''4.\trill si'8 |
  mi''2 red''4 mi''8 fad'' |
  \appoggiatura fad''8 sol''4 \appoggiatura fad''8 mi''4 si' la'8 si' |
  do''4 si' la'4.\trill si'8 |
  si'2 si'4 mi'' |
  si'2 si'4 si'8 dod'' |
  re''4 si' mi''4. re''8 |
  dod''4.\trill la'8 re''4. dod''8 re'' mi'' fad'' sol'' |
  fad''4\trill \appoggiatura mi''8 re''4 fad'' mi''8 re'' |
  dod''4. fad''8 lad'4.\trill si'8 |
  dod''4. fad''8 sol''4. fad''8 mi'' re'' dod'' fad'' |
  red''4\trill si'
  \ffclef "vbasse" <>^\markup\character Numapire
  mi'2~ |
  mi'4 r sol16 sol la si red8.\trill mi16 |
  mi2 sol4 la8 si |
  \appoggiatura si8 do'4. la8 \appoggiatura la8 si4. do'8 |
  si4\trill \appoggiatura la8 sol4 r8 si si dod' |
  \appoggiatura dod'8 re'4 \appoggiatura dod'!8 si4 lad si |
  dod'2 fad4 fad8 si |
  sold4 lad8 si lad4.\trill si8 |
  si2 r8 fad fad sol |
  la4. fad8 si4 mi |
  fad si, si8 la si red |
  mi4.( fad16[ sol]) fad4.\trill mi8 |
  mi8
  \ffclef "vbas-dessus" <>^\markup\character Pircaride
  mi'16 mi' sold'8 sold'16 mi' si'8 si'16 sold' dod''8 sold'16 dod'' |
  lad'4\trill lad'8
  \ffclef "vbasse" <>^\markup\character Numapire
  fad16 fad si8 si16 dod' |
  red'8 red'16 fad' lad8 lad16 dod' fad4 fad8 la16 si |
  dod'8 red'16 mi' red'8\trill mi' fad4 fad8 fad16 si |
  mi4
  \ffclef "vbas-dessus" <>^\markup\character-text Pircaride à part
  r8 si'16 sold' dod''2 |
  dod''4 r8 la'16 si' dod''8 dod''16 fad'' |
  red''8\trill red''16 mi'' fad''8 fad'' red'' red''16 red'' mi''8. fad''16 |
  lad'8\trill fad'16 fad' si'8 dod''16 red'' dod''8.([\trill si'16]) |
  si'4 fad''2 |
  red''\trill
  \ffclef "vbasse" <>^\markup\character Numapire
  si4 si8 fad |
  \appoggiatura fad8 sold2 r8 si |
  mi'2 si4 dod'8 re' |
  dod'4\trill mi' mi'16 red' dod' si |
  la8. la16 fad8.\trill fad16 sold8. la16 |
  sold4\trill sold
  \ffclef "vbas-dessus" <>^\markup\character Pircaride
  r8 si' |
  \appoggiatura si'8 dod''4. fad''8 red''4 red''8 red''16 mi'' |
  \appoggiatura mi''8 fad''2 fad''4 |
  r4 si'8 la' sold'4 fad'8 mi' |
  si'2 sold'4 sold'8 sold' dod''4 dod'' |
  lad'4\trill fad'8 fad' si'4 dod''8 red'' |
  mi''4 mi''8 red'' dod''4 red''8 mi'' |
  red''4\trill r8 si'16 si' si'8 mi'' |
  \appoggiatura red''8 dod''4 red''8 mi'' mi''4.( red''8) |
  \appoggiatura red''8 mi''2 r8 si'16 dod'' |
  re''4 re''8 mi'' \appoggiatura re''8 dod''4 r8 dod''16 fad'' red''4\trill red''8 mi'' |
  \appoggiatura mi''8 fad''2 fad''8
  \ffclef "vbasse" <>^\markup\character Numapire
  si16 la |
  sold4 la8 si \appoggiatura si dod'4 r16 la si dod' |
  fad4 fad fad8 sold |
  \appoggiatura fad8 mi2. |
  sol4 sol8 sol la si |
  \appoggiatura si8 do'2 do'8 r16 la |
  fad fad fad la red8 si,16 si, mi8 mi16 fad |
  \appoggiatura fad8 sol4 sol mi8 mi16 mi do'8. do'16 |
  do'4 la8 la re'4 re'8 re' |
  si4\trill r8 si16 sol re'4 r8 la16 re' |
  sol4 la8 si do'4 do'8 si si4( la)\trill |
  sol2. |
  r4 r
}
si8 la |
do'4 si la |
sol la4. si8 |
red2\trill red8 mi |
si,2 si,8 si, |
mi4 mi fad |
sol8 fad mi fad sol mi |
la8[\melisma sol fad sol la fad] |
si[ la sol la si sol] |
do'[ si la si do' la]( |
si4)\melismaEnd si si8 si |
mi'4 si re' |
do'2 la8 sol |
\appoggiatura sol8 fad2 fad8 sol |
red2\trill si,8 si, |
mi4 mi fad |
sol8 fad mi fad sol mi |
la8[\melisma sol fad sol la fad] |
si4 la8[ sol fad mi]( |
red4\trill)\melismaEnd si, mi8 fad |
sol4 sol mi |
do'8 si la sol fad mi |
si4( si,2) |
mi2. |
