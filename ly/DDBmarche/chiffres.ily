\setMusic #'rondeau {
  s1 <"">2.\figExtOn <"">4\figExtOff <4+ _->4 <6> <6+>2
  <6>2. <_+>4 s1 <"">2.\figExtOn <"">4\figExtOff <4 3> <7 5> <_+>2 s1
}
\keepWithTag #'() \rondeau
s1 s2 <6 5>4 <4+> <6>4. <6 5>8 <6 4>4 <3> s1
s1 s2 <6 5>4 <4+> <6>4. <6 5>8 <6 4>4 <3> <"">2.\figExtOn <"">4\figExtOff
\rondeau
s1 <6+>2 <6>4 <5/> s2 <_+>4\figExtOn <_+>\figExtOff <6> <6+>8 <_-> <5/>4 <7 _+>
<_->2 <5/>4\figExtOn <5/>\figExtOff s1 <6>2. <6>4 <9> <7 _->2.
s2 <5/>4 <7 _+> <_->2 <5>4 <7 _+> s2 <_+>4\figExtOn <_+>4\figExtOff
<6> <6 5> <7 _+> <_-> <_+>2..\figExtOn <_+>8\figExtOff
s1 <"">2.\figExtOn <"">4\figExtOff <4+ _->4 <6> <6+>2
<6>2. <7 _+>4 s1 <"">2.\figExtOn <"">4\figExtOff <4 3> <7 5> <_+>2 s1
