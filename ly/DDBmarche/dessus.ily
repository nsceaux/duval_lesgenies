\clef "dessus"
\setMusic #'rondeau {
  si'4. si'8 sol'4 mi' |
  si'2 mi''4 sol'' |
  fad'' mi'' red'' mi'' |
  si'8 la' si' do'' si' la' sol' fad' |
  mi'4. si'8 sol'4 mi' |
  si'2 mi''4 sol'' |
  fad''4 mi'' red''8 mi'' fad'' red'' |
  mi''1 |
}
\keepWithTag #'() \rondeau
re''4. re''8 si'4 sol' |
mi''4. do''8 la'4 fad' |
re''8 do'' re'' mi'' re'' do'' si' la' |
si'2\trill sol' |
re''4. re''8 si'4 sol' |
mi''4. do''8 la'4 fad' |
re''8 do'' re'' mi'' re'' do'' si' la' |
sol'1 |
\rondeau
si'4. si'8 do''4 do'' |
sold'2 la'4 si' |
do''2 dod''4. dod''8 |
re''4 mi''8 fa'' mi''4 la'' |
fa'' mi''8 fa'' re''4. re''8 |
mi''4 mi'' si'4. si'8 |
do''2 sol''4. sol''8 |
la'' sol'' fa'' mi'' re''4.\trill do''8 |
do''2 mi''4. mi''8 |
fa''4 fa'' fad''4. fad''8 |
sol''4 sol'' sold''4. sold''8 |
la''4 fad'' red'' mi'' |
fad''2 fad'' |
\rondeau
