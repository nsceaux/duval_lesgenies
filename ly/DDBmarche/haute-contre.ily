\clef "haute-contre"
\setMusic #'rondeau {
  sol'4. sol'8 mi'4 sol' |
  sol'4. si'8 si'4 si' |
  do'' si' la' sol' |
  fad' sol'8 la' sol' fad' mi' red' |
  mi'4. sol'8 mi'4 sol' |
  sol'4. si'8 si'4 si' |
  fad' sol' fad'8 sol' la' fad' |
  sol'1 |
}
\keepWithTag #'() \rondeau
sol'2 r |
sol'2. fad'4 |
sol' re''8 do'' si' la' sol' fad' |
sol'2 r |
sol'4. sol'8 sol'4 re' |
sol'4. sol'8 sol'4 fad' |
sol' re''8 do'' si' la' si' do'' |
si'2\trill~ si'8 do'' si' la' |
\rondeau |
R1 |
fa'4. fa'8 mi'4 mi' |
mi'2 la'4. la'8 |
la'2. la'4 |
la' sol'8 la' fa'4. fa'8 |
mi'2 re'4. re'8 |
do'2 mi'4 do'' |
do''4 do'' si'4.\trill do''8 |
do''2 la'4 la' |
la'2 si'4 si' |
si'2 si'4. si'8 |
do''4 do'' si' si' |
si'4. do''8 si'4 la' |
\rondeau |
