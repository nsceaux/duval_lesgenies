\clef "taille"
\setMusic #'rondeau {
  mi'8 si4 si |
  mi'4. sol'8 mi'4 mi' |
  red' mi' si si |
  si2. la4 |
  sol4. mi'8 si4 si |
  mi'4. sol'8 mi'4 mi' |
  mi'2 si4 si |
  si1 |
}
mi'4. \keepWithTag #'() \rondeau |
si2 r |
do' mi'4 re' |
re'4. sol'8 sol'4 re' |
re'2 r |
si4. si8 re'4 si |
do'4. do'8 mi'4 re' |
re'4. sol'8 sol' la' sol' fad' |
sol'2~ sol'8 red' mi' fad' |
sol'4. \rondeau |
R1 |
re'4. re'8 mi'4 re' |
do'2 mi'4. mi'8 |
re'4 dod'8 re' mi'4 dod' |
re'2 sol4. sol8 |
sol2 sol'4. sol'8 |
sol'2 sol'4. sol'8 |
sol'4 la' sol' sol' |
mi'2\trill sol'4. sol'8 |
fa'2 la'4. la'8 |
sol'2 mi'4. mi'8 |
mi'4 mi' fad' mi' |
red'2 red' |
mi'4. \rondeau |
