\clef "basse" mi'4 |
red'2 red'8 mi' |
do'4 do' la |
si si si8 si |
la4 la la |
sol sol sol |
red2 red4 |
mi2. |
sold4 sold mi |
la2 la8 sol |
fad4 dod la, |
re re re' |
dod'2 re'4 |
fad8 mi re fad mi re |
dod4 si, la, |
re2 re'8 dod' |
si4 lad si |
fad mi re |
dod fad fad, |
si, si, re' |
dod' dod' dod' |
re' re' si |
mi'2 re'4 |
dod'4. re'8 dod'4 |
si lad si |
fad2 fad8 fad |
si2 si8 si |
red4 red si, |
mi mi mi |
mi sold? mi |
fad8 sold lad si dod' lad |
si4 si si, |
mi4. fad8 sol4 |
mi fad fad, |
si,2 <>^"[Basses et] B.C." red'8 red' |
mi'2 mi8 mi |
si2 <>^"[Tous]" si4 |
sol2 fad8 mi |
red4 dod si, |
mi mi mi' |
red'2 red'8 mi' |
do'4 do' la |
si si la |
sol2 sol4 |
red2. |
si4 dod' red' |
mi'2 mi'8 re' |
dod'4 si lad |
si dod' re' |
dod' fad' fad |
si si si |
si si si |
lad lad lad |
si4. si8 si4 |
mi fad4. fad8 |
si2 si8 si |
mi'4 mi' re' |
do' si la |
sold fad mi |
la8 sold la do' si la |
sold fad mi fad sold mi |
la sold la si do' la |
re'4 re' re' |
re'4. mi'8 fa'4 |
re' mi' mi |
la2 r4 | \allowPageTurn
r4 r <>^"[Basses et] B.C." la8 la |
re'4 re re'8 do' |
si4 la sol |
fad mi re |
sol sol, sol |
si,2 do4 |
si, la, sol, |
do2 dod'4 |
re'2 red'4 |
mi'2 r4 |
r r mi8 mi |
la4 si do' |
re' mi' <>^"[Tous]" mi |
la2 fa8 mi |
re4 dod la, |
re re re'4 |
re' dod' la |
re' fa re |
la la, \clef "alto" <>^"[B.C.]" la'8 la' |
la'4 la' la' |
sol' sol' fa' |
mi' la' la |
re' re' re' |
dod' dod' dod' |
re'4. mi'8 fa'4 |
sol' la' la |
re'2 \clef "bass" <>^"[Basses et B.C.]" re'8 re' |
fad4 fad sol |
fad mi re |
sol2 mi4 |
la2 fad4 |
si2 la4 |
sol4. fad8 mi4 |
la, si, <>^"[Tous]" si |
sol2 fad8 mi |
red4 dod si, |
mi mi mi |
fad2 sol4 |
do'2. |
si4 dod' red' |
mi'2 mi'8 re' |
dod'4 si lad |
si dod' re' |
dod' fad' fad |
si8 lad si dod' re' si |
dod' si dod' re' mi' dod' |
re'4 si si, |
sol4. sol8 sol4 |
mi fad fad, |
si,2 si4 |
sol2 fad8 mi |
red4 mi8 fad sol la |
si4 si mi' |
red'2 red'4 |
mi'2. |
mi'4 red' mi' |
si2 si8 fad |
si4 lad si |
dod' si dod' |
red' dod' red' |
mi'4 mi' mi |
si si, si |
sol4. fad8 sol4 |
la si si, |
mi2. |
