\clef "dessus" si''4 |
si''2 si''8 si'' |
si''4 la'' la'' |
fad''\trill \appoggiatura mi''8 red''4 red'' |
fad''2 red''4 |
mi''8 fad'' sol'' la'' si'' sol'' |
fad''4 fad'' fad'' |
sol''2 si'4 |
mi''2 mi''4 |
dod''2\trill la''8 la'' |
la''4 la'' la'' |
la''2 fad''4 |
la''4. sol''8 fad'' sol'' |
la'' sol'' fad'' la'' sol'' fad'' |
mi''4 fad'' sol'' |
fad''2\trill fad''8 mi'' |
re''4 dod'' si' |
lad'\trill fad' si' |
dod'' re'' mi'' |
re''8 dod'' si' dod'' re'' si' |
mi'' re'' dod'' re'' mi'' dod'' |
fad'' mi'' re'' mi'' fad'' re'' |
sol''4 sol'' fad'' |
mi''4. fad''8 mi''4 |
re'' dod'' si' |
fad''2 lad''8 lad'' |
si''2 fad''8 fad'' |
fad''4 fad'' fad'' |
mi'' si' mi'' |
sold'' sold'' sold'' |
fad''2.~ |
fad''2 re''4 |
sol''4. fad''8 mi''4 |
re'' dod''4.\trill si'8 |
si'2 <>^"Violons" fad''8 fad'' |
sol''4 mi'' si'8 mi'' |
red'' dod'' si' dod'' red'' si' |
mi''2 red''8 mi'' |
fad''4 mi'' fad'' |
sol'' \appoggiatura fad''8 mi''4 sol'' |
fad''2\trill fad''8 sol'' |
mi''4\trill mi'' fad'' |
red''\trill red'' si' |
mi''2 si'4 |
fad''2. |
red''4 mi'' fad'' |
sol''2 sol''8 fad'' |
mi''4 re'' dod'' |
fad'' mi'' re'' |
mi'' re'' dod'' |
re''8 dod'' re'' mi'' fad'' re'' |
mi'' re'' mi'' fad'' sol'' mi'' |
fad''2 fad''4 |
re''4.\trill dod''8 re''4 |
mi'' dod''4. fad''8 |
red''2\trill si''8 si'' |
si''4 si'' si'' |
la'' sold''\trill la'' |
si''\trill la'' sold'' |
la''4. la''8 sold'' fad'' |
mi''2.~ |
mi''2 do''4 |
fa''2.~ |
fa''4. mi''8 re''4 |
la'' la'' sold''\trill |
la''2 <>^"Violons" mi''8 mi'' |
la''4 la' la''8 la'' |
fad''2\trill re''8 re'' |
sol''4 fad'' sol'' |
la'' si'' do''' |
si'' sol'' re'' |
sol'' fa'' mi'' |
re'' mi'' fa'' |
mi''8 re'' mi'' fad'' sol'' la'' |
fad'' mi'' fad'' sold'' la'' si'' |
sold''2\trill mi''8 mi'' |
re''4 do'' si' |
do'' mi'' mi'' |
la'' sold''4.\trill la''8 |
la''2 la''8 la'' |
la''4 la'' la'' |
la'' la'' la'8 la' |
fa''4 mi'' la'' |
fa''8 mi'' re'' mi'' fa'' re'' |
mi''4 la' \twoVoices #'(dessus1 dessus2 dessus) <<
  { la''8 la'' |
    sol''4 sol'' fa'' |
    mi'' mi'' fa'' |
    sol'' fa'' mi'' |
    fa''8 mi'' re'' mi'' fa'' re'' |
    sol''4 sol'' mi'' |
    fa''4. sol''8 la''4 |
    sol'' mi''4. la''8 |
    fad''2\trill }
  { fa''8 fa'' |
    mi''4 mi'' re'' |
    dod'' dod'' re'' |
    mi'' re'' dod'' |
    re''8 mi'' fa'' mi'' re'' fa'' |
    mi''4 mi'' mi'' |
    la'4. la'8 la'4 |
    re'' dod''4. re''8 |
    re''2 }
>> <>^"Violons" la'8 la' |
re''4 do'' si' |
la' si' do'' |
si'8 la' si' dod'' re'' mi'' |
dod'' si' dod'' red'' mi'' fad'' |
red''4. si'8 si'4 |
mi''4. red''8 mi''4 |
fad'' fad''4.\trill mi''8 |
mi''2 red''8 mi'' |
fad''4 mi'' fad'' |
sol'' \appoggiatura fad''8 mi''4 do'' |
do''2 si'4 |
fad''2. |
red''4 mi'' fad'' |
sol''2 sol''8 fad'' |
mi''4 re'' dod'' |
fad'' mi'' re'' |
mi'' re'' dod'' |
re''8 dod'' re'' mi'' fad'' re'' |
mi'' re'' mi'' fad'' sol'' mi'' |
fad''4 fad'' fad'' |
re''4.\trill dod''8 re''4 |
mi'' dod''4.\trill si'8 |
si'2 si'4 |
mi''2 red''8 mi'' |
fad''4 sol'' \appoggiatura fad''8 mi''4 |
red''\trill \appoggiatura dod''8 si'4 \ficta do'' |
do''2 si'4 |
sol''2. |
sol'4 fad' mi' |
si'2 si'8 dod'' |
re''4 dod'' red'' |
mi'' red'' mi'' |
fad'' mi'' fad'' |
sol'' \appoggiatura fad''8 mi''4 mi'' |
red'' \appoggiatura dod''8 si'4 si' |
mi''4. red''8 mi''4 |
fad'' red''4.\trill mi''8 |
mi''2. |
