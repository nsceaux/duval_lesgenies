\clef "haute-contre" sol'4 |
fad'2 fad'8 sol' |
mi'4 mi' fad' |
red'4\trill \appoggiatura dod'8 si4 si' |
red''2 si'4 |
si'2 si'4 |
la' la' si' |
si'2 sol'4 |
si'4 si' \appoggiatura la'8 sold'4 |
la'2 mi''8 mi'' |
re''4 mi'' la' |
fad'\trill re' la' |
la'2 la'4 |
la'2. |
mi''4 re'' dod'' |
re''2 fad'8 lad' |
si'4 fad' fad' |
fad' fad' fad' |
mi' re' fad' |
fad'8 mi' re' mi' fad' re' |
sol' fad' mi' fad' sol' mi' |
re' mi' fad' mi' re' fad' |
mi'2 fad'4 |
sol'4. fad'8 sol'4 |
si dod' re' |
dod'2 fad'8 fad' |
fad'2 fad'8 fad' |
si'4 si' la' |
sold' sold' sold' |
sold' dod'' mi'' |
lad' dod'' mi'' |
re'' si' si' |
si'4. si'8 si'4 |
si' lad'4.\trill si'8 |
si'2 si'8 si' |
si'4 sol' sol'8 sol' |
fad' mi' red' mi' fad' red' |
mi'2 si'8 si' |
si'4 dod'' red'' |
mi''4 mi'' si' |
si'2 si'8 si' |
si'4 la' fad' |
fad' fad' \appoggiatura mi'8 red'4 |
mi'2 si'4 |
la'2. |
si'2 si'4 |
si'2 si'8 si' |
lad'4 si' dod'' |
re'' lad' si' |
si'2 lad'4 |
si'8 lad' si' dod'' re'' si' |
dod''2 dod''4 |
dod''2 fad'4 |
fad'4. mi'8 fad'4 |
sol' fad'4. fad'8 |
fad'2 fad'8 fad' |
sold'4 sold' mi' |
mi'2 mi'8 mi' |
mi'4 la' si' |
do''8 si' la' si' do'' re'' |
mi''4 sold' si' |
la'2 la'4 |
la'2.~ |
la'4. la'8 la'4 |
do'' si'4.\trill la'8 |
la'2 r4 |
r r la'8 la' |
la'2 fad'8 fad' |
sol'4 la' si' |
la' sol' fad' |
sol'2 sol'4 |
sol'2 sol'4 |
sol' do'' re'' |
do''8 re'' do'' si' la'4 |
la'8 mi'' re'' do'' si'4 |
si'2 r4 |
r r sold'8 sold' |
la'2 la'4 |
si' si'4.\trill la'8 |
la'2 la'8 la' |
fa'4 mi' dod' |
re' re' la'8 la' |
la'4 la' la' |
la'2 la'4 |
la'4 la' fa''8 fa'' |
mi''4 mi'' re'' |
dod'' dod'' re'' |
mi'' re'' dod'' |
re''8 mi'' fa'' mi'' re'' fa'' |
mi''4 mi'' mi'' |
la'4. la'8 la'4 |
re'' dod''4. re''8 |
re''2 la'8 la' |
la'4 la' sol' |
la' sol' fad' |
sol'8 fad' sol' la' si' dod'' |
la' sol' la' si' dod'' red'' |
si'2 si'4 |
si'2 si'4 |
do''4 si' si' |
si'2 fad'8 fad' |
fad'4 sol' red' |
mi' mi' sol' |
la'2 sol'4 |
la'2. |
si'2 si'4 |
si'2 si'8 si' |
lad'4 si' dod'' |
re'' lad' si' |
si'2 lad'4 |
si'4 si' si' |
lad' lad' dod'' |
si' si' re'' |
si'4. lad'8 si'4 |
dod'' fad' mi' |
red'2\trill si'4 |
si'2 si'8 si' |
si'4 si' \appoggiatura la'8 sol'4 |
fad' fad' la' |
la'2 la'4 |
sol'2. |
si'4 si'8 la' sol' fad' |
fad'2\trill fad'8 fad' |
fad'4 fad' fad' |
sol' la' sol' |
la' si' la' |
sol' sol' si' |
si' si' si' |
si'4. si'8 si'4 |
do'' si' la' |
sol'2. |
