\tag #'vdessus {
  Chan -- tons, cé -- lé -- brons no -- tre rei -- ne,
  por -- tons nos voix jus -- ques aux cieux,
  chan -- tons, chan -- tons, cé -- lé -- brons no -- tre rei -- ne,
  por -- tons nos voix jus -- ques aux cieux,
  le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le le bon -- heur des dieux, __
  le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le le bon -- heur des dieux.
}
\tag #'vhaute-contre {
  Chan -- tons, cé -- lé -- brons no -- tre rei -- ne,
  por -- tons nos voix, por -- tons nos voix,
  chan -- tons, chan -- tons, cé -- lé -- brons no -- tre rei -- ne,
  por -- tons nos voix jus -- ques aux cieux,
  le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le le bon -- heur des dieux,
  é -- ga -- le le bon -- heur des dieux,
  le bon -- heur,
  le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le le bon -- heur des dieux.
}
\tag #'vtaille {
  Chan -- tons, cé -- lé -- brons no -- tre rei -- ne,
  cé -- lé -- brons no -- tre rei -- ne,
  por -- tons nos voix jus -- ques aux cieux,
  cé -- lé -- brons no -- tre rei -- ne,
  por -- tons nos voix jus -- ques aux cieux,
  le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le le bon -- heur des dieux,
  é -- ga -- le le bon -- heur des dieux,
  le bon -- heur,
  le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le le bon -- heur des dieux.
}
\tag #'vbasse {
  Chan -- tons, cé -- lé -- brons no -- tre rei -- ne,
  cé -- lé -- brons no -- tre rei -- ne,
  por -- tons nos voix jus -- ques aux cieux,
  cé -- lé -- brons no -- tre rei -- ne,
  por -- tons nos voix jus -- ques aux cieux,
  le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le le bon -- heur des dieux,
  é -- ga -- le le bon -- heur des dieux,
  le bon -- heur, le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le le bon -- heur des dieux.
}
%%
\tag #'vdessus {
  Chan -- tons, cé -- lé -- brons no -- tre rei -- ne,
  chan -- tons, cé -- lé -- brons no -- tre rei -- ne,
  por -- tons nos voix jus -- ques aux cieux,
  le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le le bon -- heur des dieux,
  le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le le bon -- heur des dieux.
}
\tag #'vhaute-contre {
  Chan -- tons, cé -- lé -- brons no -- tre rei -- ne,
  chan -- tons, cé -- lé -- brons no -- tre rei -- ne,
  por -- tons nos voix jus -- ques aux cieux,
  le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le, é -- ga -- le,
  é -- ga -- le le bon -- heur des dieux,
  le bon -- heur d’un a -- mant,
  le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le le bon -- heur des dieux.
}
\tag #'vtaille {
  Chan -- tons, cé -- lé -- brons no -- tre rei -- ne,
  chan -- tons, cé -- lé -- brons no -- tre rei -- ne,
  por -- tons nos voix jus -- ques aux cieux,
  le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le, é -- ga -- le le bon -- heur des dieux,
  le bon -- heur d’un a -- mant,
  le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le le bon -- heur des dieux.
}
\tag #'vbasse {
  Chan -- tons, cé -- lé -- brons no -- tre rei -- ne,
  chan -- tons, cé -- lé -- brons no -- tre rei -- ne,
  por -- tons nos voix jus -- ques aux cieux,
  le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le, é -- ga -- le,
  é -- ga -- le le bon -- heur des dieux,
  le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le le bon -- heur des dieux.
}
%%
\tag #'(vdessus vbasse) {
  Chan -- tons, cé -- lé -- brons no -- tre rei -- ne.
}
\tag #'vhaute-contre {
  Chan -- tons, cé -- lé -- brons no -- tre rei -- ne.
}
\tag #'vtaille {
  Chan -- tons, cé -- lé -- brons no -- tre rei -- ne.
}
%%
\tag #'(vdessus vdessus2 vhaute-contre) {
  Le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  \ifComplet\tag #'vhaute-contre { é -- ga -- le, }
  é -- ga -- le le bon -- heur des dieux.
}
\ifConcert\tag #'vtaille {
  Le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le,
  é -- ga -- le le bon -- heur des dieux.
}
%%
\tag #'(vdessus vbasse) {
  Chan -- tons, cé -- lé -- brons no -- tre rei -- ne,
  por -- tons nos voix jus -- ques aux cieux,
  le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le le bon -- heur des dieux.
  Chan -- tons, cé -- lé -- brons no -- tre rei -- ne,
  por -- tons nos voix jus -- ques aux cieux,
  le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le, é -- ga -- le le bon -- heur des dieux.
}
\tag #'vhaute-contre {
  Chan -- tons, cé -- lé -- brons no -- tre rei -- ne,
  por -- tons nos voix jus -- ques aux cieux,
  le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le, é -- ga -- le,
  é -- ga -- le le bon -- heur des dieux.
  Chan -- tons, cé -- lé -- brons no -- tre rei -- ne,
  por -- tons nos voix jus -- ques aux cieux,
  le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le, é -- ga -- le le bon -- heur des dieux,
}
\tag #'vtaille {
  Chan -- tons, cé -- lé -- brons no -- tre rei -- ne,
  por -- tons nos voix jus -- ques aux cieux,
  le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le, é -- ga -- le,
  é -- ga -- le le bon -- heur des dieux.
  Chan -- tons, cé -- lé -- brons no -- tre rei -- ne,
  por -- tons nos voix jus -- ques aux cieux,
  le bon -- heur d’un a -- mant qui peut por -- ter sa chaî -- ne,
  é -- ga -- le, é -- ga -- le le bon -- heur des dieux,
}
