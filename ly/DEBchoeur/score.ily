\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with {
        \override VerticalAxisGroup.remove-empty = #(eqv? (ly:get-option 'urtext) #t)
        \override VerticalAxisGroup.remove-first = #(eqv? (ly:get-option 'urtext) #t)
        instrumentName = \markup\center-column { [Violons, Hautbois] }
      } << \global \includeNotes "dessus" >>
      \ifFull\new Staff \with {
        instrumentName = \markup\center-column { [Hautes-contre] }
      } << \global \includeNotes "haute-contre" >>
      \ifFull\new Staff \with { instrumentName = "[Tailles]" } <<
        \global \includeNotes "taille"
      >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \ifComplet\instrumentName "[Dessus]"
        \ifConcert\instrumentName "[Dessus I]"
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \ifComplet\new Staff \with { \haraKiriFirst } \withLyrics <<
        \global \keepWithTag #'vdessus2 \includeNotes "voix"
      >> \keepWithTag #'vdessus2 \includeLyrics "paroles"
      \new Staff \with {
        \override VerticalAxisGroup.remove-empty = #(eqv? (ly:get-option 'urtext) #t)
        \override VerticalAxisGroup.remove-first = #(eqv? (ly:get-option 'urtext) #t)
      } \withLyrics <<
        \ifComplet\instrumentName\markup\center-column { [Hautes-contre] }
        \ifConcert\instrumentName "[Dessus II]"
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \ifFull\new Staff \withLyrics <<
        \ifComplet\instrumentName "[Tailles]"
        \ifConcert\instrumentName\markup\center-column { [Haute- contre] }
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \ifComplet\instrumentName "[Basses]"
        \ifConcert\instrumentName "[Basse]"
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses, Bassons et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \ifComplet\modVersion { s4 s2.*83\break s2.*15\break }
      \origLayout {
        s4 s2.*7\break s2.*7 s2 \bar "" \break s4 s2.*7\pageBreak
        s2.*7\break s2.*7\break s2.*8\pageBreak
        s2.*5\break s2.*7\break s2.*6\pageBreak
        s2.*9\break s2.*7\break s2.*5\pageBreak
        s2.*6\break s2.*7\pageBreak
        s2.*8\break s2.*5\break s2.*7\pageBreak
        s2.*7\break s2.*7
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
