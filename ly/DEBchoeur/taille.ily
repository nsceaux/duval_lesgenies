\clef "taille" mi'4 fad'2 fad'8 mi' |
sol'4 do' do' |
si2 si8 si |
si4 si si |
si si si |
si2 fad'4 |
mi'2. |
mi'4 mi' mi' |
mi'2 dod'8 dod' |
re'4 si dod' |
re' re' re' |
mi'2 re'4 |
re'2. |
dod'4 re' la |
la2 si8 dod' |
re'4 mi' re' |
dod' lad si |
lad si dod' |
si si si' |
si' lad' lad' |
si' si' si' |
si'2 si'4 |
dod''4. si'8 dod''4 |
fad' fad' fad' |
lad'2 dod'8 dod' |
red'2 red'8 red' |
si4 si si |
si mi' si |
si si mi' |
dod' mi' dod' |
si si fad' |
mi'4. re'8 mi'4 |
sol' fad' mi' |
re'2 fad'8 fad' |
mi'2 mi'8 mi' |
fad'4 fad' si |
si2 fad'8 sol' |
fad'4 sol' si |
si si mi' |
fad'2 fad'8 mi' |
sol'4 mi' do' |
si4 si si |
si2 mi'4 |
do'2. |
fad'4 mi' red' |
mi'2 mi'8 fad' |
dod'4 re' mi' |
re' dod' fad' |
sol' fad' fad' |
fad'2 fad'4 |
mi'2 mi'4 |
mi'2 mi'4 |
re'4. mi'8 re'4 |
dod' si4. lad8 |
si2 red'8 red' |
mi'4 mi' mi' |
mi'2 do'8 do' |
si4 fad' sold' |
mi' mi' mi' |
mi' si mi'8 re' |
do' si do' re' mi'4 |
re'2.~ |
re'4. mi'8 fa'4 |
fa' mi' re' |
do'2 r4 |
r r mi'8 mi' |
re'2 re'8 re' |
re'2 re'4 |
re'2 re'4 |
re'2 re'4 |
re'2 do'4 |
re' do' si |
do'8 si do' re' mi'4 |
re'8 do' re' mi' fad'4 |
mi'2 r4 |
r r mi'8 mi' |
mi'2 mi'4 |
fa' mi' mi' |
mi'2 re'8 mi' |
fa'4 la' mi' |
fa' fa' fa'8 fa' |
fa'4 sol' dod' |
re'2 re'4 |
dod'4 dod' la'8 la' |
la'4 la' la' |
sol' sol' fa' |
mi' la' la |
re' re' re' |
dod' dod' dod' |
re'4. mi'8 fa'4 |
sol' la' la |
re'2 re'8 re' |
re'4 re' re' |
re'2 re'4 |
re'2 mi'4 |
mi'2 fad'4 |
fad'2 red'4 |
mi'4. fad'8 sol'4 |
mi' red'4.\trill mi'8 |
mi'2 lad8 lad |
si4 si si |
si si mi' |
red'2 mi'4 |
red'2. |
fad'4 mi' red' |
mi'2 mi'8 fad' |
dod'4 re' mi' |
re' dod' fad' |
sol' fad' fad' |
fad'4 fad' fad' |
mi' mi' mi' |
re' re' si |
si4. dod'8 si4 |
si si lad |
si2 red'4 |
mi'2 fad'8 sol' |
fad'4 mi' si |
si si mi' |
fad'2 fad'4 |
mi'2. |
mi'4 fad' mi' |
red'2\trill re'8 dod' |
si4 mi' red' |
dod' fad' mi' |
red' sol' fad' |
mi' mi' sol' |
fad' \appoggiatura mi'8 red'4 sol' |
sol'4. la'8 sol'4 |
fad' fad'4.\trill mi'8 |
mi'2. |
