<<
  \tag #'vdessus {
    \clef "vdessus" mi'4 |
    si'2 si'8 si' |
    mi''4 mi'' fad'' |
    red''\trill si' si' |
    red''2 si'4 |
    mi''2. |
    fad''4 fad'' fad'' |
    sol''2 si'4 |
    mi''2 mi''4 |
    dod''2\trill la'8 la' |
    re''4 mi'' la'' |
    fad''\trill re'' fad'' |
    la''2 la'4 |
    re''2. |
    mi''4 fad'' sol'' |
    fad''2\trill fad''8 mi'' |
    re''4 dod'' si' |
    lad'\trill fad' si' |
    lad'\trill si' dod'' |
    re''8[\melisma dod'' si' dod'' re'' si'] |
    mi''[ re'' dod'' re'' mi'' dod''] |
    fad''[ mi'' re'' mi'' fad'' re'']( |
    sol''4)\melismaEnd sol'' fad'' |
    mi''4. fad''8 mi''4 |
    re'' dod'' si' |
    fad''2.~ |
    fad''2 fad'8 fad' |
    si'4 si' la' |
    sold' si' mi'' |
    sold' dod'' mi'' |
    lad'8[ si' dod'' re'' mi'' dod'']( |
    re''4) si' re'' |
    sol''4. fad''8 mi''4 |
    re'' dod''4.\trill si'8 |
    si'2 r4 |
    R2. |
    r4 r si' |
    mi''2 red''8 mi'' |
    fad''4 mi'' fad'' |
    sol'' \appoggiatura fad''8 mi''4 sol'' |
    fad''2\trill fad''8 sol'' |
    mi''4\trill mi'' fad'' |
    red''\trill red'' si' |
    mi''2 si'4 |
    fad''2. |
    red''4 mi'' fad'' |
    sol''2 sol''8 fad'' |
    mi''4 re'' dod'' |
    fad'' mi'' re'' |
    mi'' re'' dod'' |
    re''8[\melisma dod'' re'' mi'' fad'' re''] |
    mi''[ re'' mi'' fad'' sol'' mi'']( |
    fad''4)\melismaEnd fad'' fad'' |
    re''4.\trill dod''8 re''4 |
    mi'' dod''4. fad''8 |
    red''2\trill si'8 si' |
    sold'4\trill sold' mi' |
    la' sold'\trill la' |
    si'\trill la' si' |
    do''8[\melisma si' la' si' do'' re''] |
    mi''2.~ |
    mi''4\melismaEnd mi'' do'' |
    fa''2.~ |
    fa''4. mi''8 re''4 |
    do'' si'4.\trill la'8 |
    la'2 r4 |
    R2.*12 |
    r4 r mi'' |
    dod''2\trill la'8 la' |
    fa''4 mi'' la'' |
    fa'' re'' r |
    R2.*2 |
    r4 r la''8 la'' |
    sol''4 sol'' fa'' |
    mi'' mi'' fa'' |
    sol'' fa'' mi'' |
    fa''8[ mi'' re'' mi'' fa'' re'']( |
    sol''4) sol'' mi'' |
    fa''4. sol''8 la''4 |
    sol'' mi''4. la''8 |
    fad''2\trill r4 |
    R2.*6 |
    r4 r si' |
    mi''2 red''8 mi'' |
    fad''4 mi'' fad'' |
    sol'' \appoggiatura fad''8 mi''4 do'' |
    do''2 si'4 |
    fad''2. |
    red''4 mi'' fad'' |
    sol''2 sol''8 fad'' |
    mi''4 re'' dod'' |
    fad'' mi'' re'' |
    mi'' re'' dod'' |
    re''8[\melisma dod'' re'' mi'' fad'' re''] |
    mi''[ re'' mi'' fad'' sol'' mi'']( |
    fad''4)\melismaEnd fad'' fad'' |
    re''4.\trill dod''8 re''4 |
    mi'' dod''4.\trill si'8 |
    si'2 si'4 |
    mi''2 red''8 mi'' |
    fad''4 sol'' \appoggiatura fad''8 mi''4 |
    red''\trill \appoggiatura dod''8 si'4 \ficta do''4 |
    do''2 si'4 |
    sol''2. |
    sol'4 fad' mi' |
    si'2 si'8 dod'' |
    re''4 dod'' red'' |
    mi'' red'' mi'' |
    fad'' mi'' fad'' |
    sol'' \appoggiatura fad''8 mi''4 mi'' |
    red'' \appoggiatura dod''8 si'4 si' |
    mi''4. red''8 mi''4 |
    fad'' red''4.\trill mi''8 |
    mi''2. |
  }
  \tag #'vdessus2 {
    \clef "vbas-dessus" r4 R2.*83 |
    r4 r fa''8 fa'' |
    mi''4 mi'' re'' |
    dod'' dod'' re'' |
    mi'' re'' dod'' |
    re''8[ mi'' fa'' mi'' re'' fa'']( |
    mi''4) mi'' mi'' |
    la'4. la'8 la'4 |
    re'' dod''4. re''8 |
    re''2 r4 |
    R2.*38
  }
  \tag #'vhaute-contre {
    \ifComplet\clef "vhaute-contre"
    \ifConcert\clef "petrucci-c3/treble"
    mi'4 |
    fad'2 fad'8 sol' |
    sol'4 la' la' |
    fad'\trill \appoggiatura mi'8 red'4 red' |
    fad'2 red'4 |
    mi'2 si'4 |
    la'2 si'4 |
    si'2 sol'4 |
    si'2 sold'4 |
    la'2 la'8 la' |
    la'4 la' la' |
    la' la' la' |
    la'2 la'4 |
    la'2. |
    <<
      \ifComplet { mi'4 re' dod' | re'2 }
      \ifConcert { mi''4 re'' dod'' | re''2 }
    >> fad'8 lad' |
    si'4 fad' fad' |
    fad' fad' fad' |
    mi' re' fad' |
    fad' fad' si' |
    si' lad' lad' |
    si' si' si' |
    si'2 si'4 |
    dod''4. si'8 dod''4 |
    fad' fad' fad' |
    lad'2 lad'8 lad' |
    si'2 fad'8 fad' |
    fad'4 fad' fad' |
    mi' sold' sold' |
    sold' sold' sold' |
    fad'2.~ |
    fad'4 fad' si' |
    si'4. si'8 si'4 |
    si' lad'4.\trill si'8 |
    si'2 r4 |
    R2. |
    r4 r <<
      \ifComplet {
        red'4 |
        mi'2 fad'8 sol' |
        fad'4 sol' si |
        si si
      }
      \ifConcert {
        si'4 |
        si'2 si'8 si' |
        si'4 dod'' red'' |
        mi'' mi''
      }
    >> si'4 |
    si'2 si'8 si' |
    si'4 la' fad' |
    fad' fad' red' |
    mi'2 si'4 |
    la'2. |
    si'4 si' si' |
    si'2 si'8 si' |
    lad'4 si' dod'' |
    re'' lad' si' |
    si' si' lad' |
    <<
      \ifComplet {
        fad' fad' fad' |
        mi' mi' mi' |
        dod' dod'
      }
      \ifConcert {
        si'8[ lad' si' dod'' re'' si']( |
        dod''4) dod'' dod'' |
        dod'' dod''
      }
    >> fad'4 |
    fad'4. mi'8 fad'4 |
    sol' fad'4. fad'8 |
    fad'2 fad'8 fad' |
    mi'4 mi' mi' |
    mi'2 mi'8 mi' |
    mi'4 la' sold' |
    la' la' la' |
    si' sold' si' |
    la' la' la' |
    la'2.~ |
    la'4. la'8 la'4 |
    la' la' sold'\trill |
    la'2 r4 |
    R2.*12 |
    r4 r <<
      \ifComplet {
        mi'4 |
        mi'2 re'8 mi' |
        fa'4 la' la' |
        la' la'
      }
      \ifConcert {
        sold'4 |
        la'2 la'8 la' |
        la'4 la' dod'' |
        re'' re''
      }
    >> r4 |
    R2.*2 |
    r4 r <<
      \ifComplet {
        la'8 la' |
        la'4 la' la' |
        sol' sol' fa' |
        mi' la' la |
        re' re' re' |
        dod' dod' dod' |
        re'4. mi'8 fa'4 |
        sol' la' la |
        re'2
      }
      \ifConcert {
        fa''8 fa'' |
        mi''4 mi'' re'' |
        dod'' dod'' re'' |
        mi'' re'' dod'' |
        re''8[ mi'' fa'' mi'' re'' fa'']( |
        mi''4) mi'' mi'' |
        la'4. la'8 la'4 |
        re'' dod''4. re''8 |
        re''2
      }
    >> r4 |
    R2.*6 |
    r4 r si'4 |
    si'2 fad'8 fad' |
    fad'4 sol' red' |
    mi' mi' sol' |
    la'2 sol'4 |
    la'2. |
    si'4 si' si' |
    si'2 si'8 si' |
    lad'4 si' dod'' |
    re'' lad' si' |
    si' si' lad' |
    si'4 si' si' |
    lad' lad' <<
      \ifComplet {
        lad'4 |
        si' si' re' |
        si4. lad8 si4 |
        dod'
      }
      \ifConcert {
        dod''4 |
        si' si' re'' |
        si'4. lad'8 si'4 |
        dod''
      }
    >> fad'4 mi' |
    red'2\trill si'4 |
    si'2 si'8 si' |
    si'4 si' \appoggiatura la'8 sol'4 |
    fad' fad' la' |
    la'2 la'4 |
    sol'2. |
    si'4 si'8[ la'] sol'[ fad'] |
    fad'2\trill fad'8 fad' |
    fad'4 fad' fad' |
    sol' la' sol' |
    la' si' la' |
    sol' sol' <<
      \ifComplet {
        sol'4 |
        fad' \appoggiatura mi'8 red'4 sol' |
        sol'4. la'8 sol'4 |
        fad' fad'4.\trill mi'8 |
        mi'2. |
      }
      \ifConcert {
        si'4 |
        si' si' si' |
        si'4. si'8 si'4 |
        do'' si' la' |
        sol'2. |
      }
    >>
  }
  \tag #'vtaille {
    \clef "vtaille" mi'4 |
    fad'2 fad'8 mi' |
    mi'4 do' do' |
    si si si8 si |
    si4 si si |
    si si si |
    si2 fad'4 |
    mi'2. |
    mi'4 mi' mi' |
    mi'2 dod'8 dod' |
    re'4 la dod' |
    re' re' re' |
    mi'2 re'4 |
    re'2. |
    dod'4 re' la |
    la2 si8 dod' |
    re'4 mi' re' |
    dod' lad si |
    dod' si lad |
    si si fad' |
    mi' mi' mi' |
    re' fad' fad' |
    mi'2 fad'4 |
    sol'4. fad'8 sol'4 |
    si4 dod' re' |
    dod'2 dod'8 dod' |
    red'2 red'8 red' |
    si4 si si |
    si mi' si |
    si si mi' |
    dod'2.( |
    si4) si fad' |
    mi'4. re'8 mi'4 |
    sol' fad' mi' |
    re'2 r4 |
    R2. |
    r4 r <<
      \ifComplet {
        si4 |
        si2 si8 si |
        si4 dod' red' |
        mi' mi'
      }
      \ifConcert {
        red'4 |
        mi'2 fad'8 sol' |
        fad'4 sol' si |
        si si
      }
    >> mi'4 |
    fad'2 fad'8 mi' |
    sol'4 mi' do' |
    si si si |
    si2 mi'4 |
    do'2. |
    fad'4 mi' red' |
    mi'2 mi'8 fad' |
    dod'4 re' mi' |
    re' dod' fad' |
    sol' fad' fad' |
    <<
      \ifComplet {
        si8[ lad si dod' re' si]( |
        dod'4) dod' dod' |
        dod' dod'
      }
      \ifConcert {
        fad' fad' fad' |
        mi' mi' mi' |
        dod' dod'
      }
    >> dod'4 |
    re'4. mi'8 re'4 |
    dod' si4. lad8 |
    si2 red'8 red' |
    mi'4 mi' mi' |
    mi'2 do'8 do' |
    si4 fad' sold' |
    mi' mi' mi' |
    mi' si mi' |
    do' do' mi' |
    re'2.~ |
    re'4. mi'8 fa'4 |
    fa' mi' re' |
    do'2 r4 |
    R2.*12 |
    r4 r <<
      \ifComplet {
        sold4 |
        la2 la8 la |
        la4 la dod' |
        re' re'
      }
      \ifConcert {
        mi'4 |
        mi'2 re'8 mi' |
        fa'4 la la |
        la la
      }
    >> r4 |
    <<
      \ifComplet { R2.*17 | }
      \ifConcert {
        R2.*2 |
        r4 r la'8 la' |
        la'4 la' la' |
        sol' sol' fa' |
        mi' la' la |
        re' re' re' |
        dod' dod' dod' |
        re'4. mi'8 fa'4 |
        sol' la' la |
        re'2 r4 |
        R2.*6 |
      }
    >>
    r4 r red'4 |
    mi'2 lad8 lad |
    si4 si si |
    si si mi' |
    red'2 mi'4 |
    red'2. |
    fad'4 mi' red' |
    mi'2 mi'8 fad' |
    dod'4 re' mi' |
    re' dod' fad' |
    sol' fad' fad' |
    fad' fad' fad' |
    mi' mi' mi' |
    re' re' si |
    si4. dod'8 si4 |
    si si lad |
    si2 red'4 |
    mi'2 fad'8 sol' |
    fad'4 mi' si |
    si si mi' |
    fad'2 fad'4 |
    mi'2. |
    mi'4 fad' mi' |
    red'2\trill re'8 dod' |
    si4 mi' red' |
    dod' fad' mi' |
    red' sol' fad' |
    mi' mi' <<
      \ifComplet {
        si4 |
        si si si |
        si4. si8 si4 |
        do' si la |
        sol2. |
      }
      \ifConcert {
        sol'4 |
        fad'4 \appoggiatura mi'8 red'4 sol' |
        sol'4. la'8 sol'4 |
        fad' fad'4.\trill mi'8 |
        mi'2. |
      }
    >>
  }
  \tag #'vbasse {
    \clef "vbasse" mi'4 |
    red'2 red'8 mi' |
    do'4 do' la |
    si si si8 si |
    la4 la la |
    sol sol sol |
    red2 red4 |
    mi2. |
    sold4 sold mi |
    la2 la8 sol |
    fad4 dod la, |
    re re re' |
    dod'2 re'4 |
    fad2. |
    dod4 si, la, |
    re2 re'8 dod' |
    si4 lad si |
    fad mi re |
    dod fad fad, |
    si, si, re' |
    dod' dod' dod' |
    re' re' si |
    mi'2 re'4 |
    dod'4. re'8 dod'4 |
    si lad si |
    fad2 fad8 fad |
    si2 si8 si |
    red4 red si, |
    mi mi mi |
    mi sold? mi |
    fad8[ sold lad si dod' lad]( |
    si4) si si, |
    mi4. fad8 sol4 |
    mi fad fad, |
    si,2 r4 |
    R2. |
    r4 r si |
    sol2 fad8 mi |
    red4 dod si, |
    mi mi mi' |
    red'2 red'8 mi' |
    do'4 do' la |
    si si la |
    sol2 sol4 |
    red2. |
    si4 dod' red' |
    mi'2 mi'8 re' |
    dod'4 si lad |
    si dod' re' |
    dod' fad' fad |
    si si si |
    si si si |
    lad lad lad |
    si4. si8 si4 |
    mi fad4. fad8 |
    si2 si8 si |
    mi'4 mi' re' |
    do' si la |
    sold fad mi |
    la8[\melisma sold la do' si la] |
    sold[ fad mi fad sold mi] |
    la[ sold la si do' la]( |
    re'4)\melismaEnd re' re' |
    re'4. mi'8 fa'4 |
    re' mi' mi |
    la2 r4 |
    R2.*12 |
    r4 r mi |
    la2 fa8 mi |
    re4 dod la, |
    re re r |
    R2.*17 |
    r4 r si |
    sol2 fad8 mi |
    red4 dod si, |
    mi mi mi |
    fad2 sol4 |
    do'2. |
    si4 dod' red' |
    mi'2 mi'8 re' |
    dod'4 si lad |
    si dod' re' |
    dod' fad' fad |
    si8[\melisma lad si dod' re' si] |
    dod'[ si dod' re' mi' dod'] |
    re'4\melismaEnd si si, |
    sol4. sol8 sol4 |
    mi fad fad, |
    si,2 si4 |
    sol2 fad8 mi |
    red4 mi8[ fad] sol[ la] |
    si4 si mi' |
    red'2 red'4 |
    mi'2. |
    mi'4 red' mi' |
    si2 si8 fad |
    si4 lad si |
    dod' si dod' |
    red' dod' red' |
    mi'4 mi' mi |
    si si, si |
    sol4. fad8 sol4 |
    la si si, |
    mi2. |
  }
>>
