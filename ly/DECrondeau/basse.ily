\clef "basse"
\setMusic #'rondeau {
  mi'2 la |
  si4. la16 sol fad4 la |
  red2 si, |
  mi2.~ mi16 red mi fad |
  sol2 la |
  si red |
  mi4 la, si,2 |
  mi1 |
}
\keepWithTag #'() \rondeau
sol2. fad4 |
sol si fad re |
sol8 la si do' re'4 sol |
fad re re' do' |
si sol do' la |
re' do' si sol |
do la, re re, |
sol, sol8 la si la sol fad |
\rondeau
si2 mi |
fad4 mi re fad |
si, si mi la |
re sol dod mi |
re2 dod |
si,4 re si, sol, |
mi,2 fad, si,1 |
\rondeau
