\setMusic #'rondeau {
  s2 <6 5> <_+> <6\\ 5>4\figExtOn <6\\ >\figExtOff <5/>2 <7 _+>
  <"">2...\figExtOn <"">16\figExtOff <6>2 <6 5> <_+> <7- 5/> <3>4 <6 5> <4> <_+> s1
}
\keepWithTag #'() \rondeau
s2 <2>4 <5/> <"">\figExtOn <"">\figExtOff <5/> <7 5> <"">4.\figExtOn <"">8\figExtOff s2
<6>1 <6>4 <7->2 <7>4 <"">\figExtOn <"">\figExtOff <6>2 <6 5>1 <"">4.\figExtOn <"">8\figExtOff <_+>2
\rondeau
s2 <6+ 5> <_+>4\figExtOn <_+>\figExtOff <6>2 s <7>4 <7 _+> <7> <7> <7>2 <6>2 <6+> s2. <6>4 <6+ 5>2 <_+> <_+>1
\rondeau
