\clef "dessus"
\setMusic #'rondeau {
  mi''4. sol''8 fad''4 la'' |
  red''4. fad''8 la'4 do'' |
  si'4. la'16 sol' fad'4 si' |
  sol'4. fad'8 mi'4 r |
  mi''4. fad''16 sol'' fad''8 sol'' la''4 |
  red''4. mi''16 fad'' la'8 si' do''4 |
  si' la'8 sol' fad'4.\trill mi'8 |
  mi'1 |
}
\keepWithTag #'() \rondeau
sol'4. si'8 la'4 do'' |
si'8 do'' re''4 la'8 si' do''4 |
si' do''8 si' la'4.\trill sol'8 |
la'2 re' |
re''4. fa''8 mi''4 sol'' |
fad'' la'' re'' sol'' |
sol''2 fad''\trill |
sol''2 r |
\rondeau
si'4. re''8 dod'' re'' mi''4 |
lad'8 si' dod''4 fad'2 |
fad'' sol''4 fad''8 mi'' |
fad''4 mi''8 re'' sol''4 fad''8 mi'' |
fad''4 fad'8 sold' lad' si' dod''4 |
fad'2. r8 si' |
dod''2 lad' |
si'1 |
\rondeau
