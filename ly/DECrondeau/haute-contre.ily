\clef "haute-contre"
\setMusic #'rondeau {
  la'4 do'' |
  fad'2. fad'4 |
  fad'4. fad'16 mi' red'2\trill |
  red'2 ( mi'4) r4 |
  si'4. la'16 sol' la'8 si' do''4 |
  fad'4. sol'16 la' fad'8 sol' la'4 |
  sol'4 fad'8 mi' red'4.\trill mi'8 |
  mi'1 |
}
si'4. si'8 \keepWithTag #'() \rondeau
si4. re'8 do'4 la' |
sol'2 la' |
sol'4 la'8 sol' fad'4.\trill sol'8 |
re'1 |
sol'2 sol' |
la'4 fad' sol' si' |
la'2 la'\trill |
sol'2 r8 red' mi' fad' |
sol'4. si'8 \rondeau |
fad'2 sol' |
fad'2. re''4 |
re''2 mi''4 re''8 dod'' |
re''4 dod''8 si' mi''4 re''8 dod'' |
si'4 la'8 sold' fad'2 |
fad'2. mi'4 |
mi'2 dod' |
red'1 |
si'4. si'8 \rondeau |
