\clef "taille"
\setMusic #'rondeau {
  mi'2 |
  fad'4. fad'16 mi' red'4 la |
  si2 si |
  si2. r4 |
  mi'1 |
  si2 do'4 la |
  si do' si2 |
  sol1 |
}
sol'4. mi'8 \keepWithTag #'() \rondeau
re'2. re'4 |
re'2 re' |
re'2. si4 |
la2 re' |
re' mi' |
re'2. re'4 |
mi'2 re'4 do' |
si si'8 la' sol' fad' mi' red' |
mi'4. mi'8 \rondeau |
re'4. fad'8 mi' fad' sol'4 |
dod'2 si |
si4 re'2 dod'4~ |
dod' si2 si4~ |
si la8 si dod' re' mi'4 |
mi' re'2 si4 |
si2 fad' |
fad'1 |
sol'4. mi'8 \rondeau |
