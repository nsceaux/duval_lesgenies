\clef "basse" r2 |
r mi8 fad sol la |
si4 la sol8 fad sol mi |
la4 si do'8 la si si, |
mi4 si, r2 |
r mi8 fad sol la |
si4 la sol8 fad sol mi |
la4 si do'2 |
si2 r |
r sol8 la si do' |
re'4 do' si la |
sol8 si la sol re'4 re |
sol2 sol, |
sol4 fad mi fad |
sol2 mi |
do'4 la si si, |
mi2
