\clef "dessus" mi'8 fad' sol' la' |
si'4 mi''8 red'' mi''4. fad''8 |
red''4\trill si' mi''8 red'' mi'' si' |
do''4 si' la'8 do'' si' la' |
sol'4\trill fad' mi'8 fad' sol' la' |
si'4 mi''8 red'' mi''4. fad''8 |
red''2\trillSug mi''8 red'' mi'' si' |
do''4 si' la'4.(\trill sol'16 la') |
si'2 sol'8 la' si' do'' |
re''4 sol''8 fad'' sol''4 sol' |
re'' re' sol'8 la' si' do'' |
re'' sol'' fad'' sol'' la'4.\trill sol'8 |
sol'2 si'4 do''8 si' |
mi''4 fad'' sol'' la'' |
si''2 si'4 do''8 si' |
mi'' sol'' fad'' mi'' red''4.\trill mi''8 |
mi''2
