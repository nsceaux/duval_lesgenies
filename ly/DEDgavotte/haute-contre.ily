\clef "haute-contre" r2 |
r si'8 la' sol' fad' |
fad'2 si'4 si' |
fad' sol' sol'8 la' sol' fad' |
mi'4\trill red' r2 |
r si'8 la' sol' fad' |
fad'2 si'4 si' |
fad'4 sol' fad'4.(\trill mi'16 fad') |
fad'2 r |
r re''8 do'' si' la' |
la'2 sol'4 fad' |
sol' re' sol' fad'\trill |
sol'2 sol'4 la'8 sol' |
si'2 si'4 si' |
si'2 sol'4 la'8 sol' |
sol'4 la'8 sol' fad'4.\trill mi'8 |
mi'2
