\clef "taille" r2 |
mi8 fad sol la sol la si do' |
si2 mi'4 mi' |
mi'2 mi'4 si |
si2 r |
mi8 fad sol la sol la si do' |
si2 mi'4 mi' |
mi'2 mi' |
red'\trill r |
sol8 la si do' si do' re' mi' |
re'2 re'4 re' |
si\trill do' re' do' |
si2\trill re' |
mi'4 red' mi' red' |
mi'2 mi' |
mi'4 do' si si |
si2
