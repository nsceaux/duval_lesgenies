\clef "basse" mi8 fad sold la |
si4 sold dod' la |
si la sold fad8 mi |
red4 mi red mi |
si8 la sold fad mi fad sold la |
si4 sold dod' la |
si la sold fad8 mi |
red4 dod8 si, mi,4 fad, |
si,2 si4 mi |
fad mi red mi |
si,2 si4 mi |
fad mi red mi |
si4. sold8 la sold fad mi |
red4. si,8 dod4 red |
mi fad8 sold la sold fad mi |
red2 si, |
mi4. la,8 si,2 |
mi
