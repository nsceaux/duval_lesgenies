\clef "dessus" sold''4 la''8 sold'' |
fad''4 si'' la''8 sold'' fad'' mi'' |
red''4 si' mi'' red''8 mi'' |
fad''4 sold''8 mi'' la''4 sold'' |
fad'' si' sold'' la''8 sold'' |
fad''4 si'' la''8 sold'' fad'' mi'' |
red''4 si' mi'' red''8 mi'' |
fad'' sold'' lad'' si'' dod'''4 lad'' |
si''2 fad''4 sold''8 mi'' |
la''4 sold''8 fad'' si''4 mi'' |
fad'' si' fad'' sold''8 mi'' |
la''4 sold''8 fad'' si''4 mi'' |
red''4. si'8 dod''4 la' |
si' dod''8 red'' mi''4 fad'' |
sold'' la''8 si'' dod'''4 la'' |
si'' la''8 sold'' la'' fad'' sold'' la'' |
sold'' fad'' mi'' fad'' fad''4.\trill mi''8 |
mi''2
