\clef "haute-contre" si'4 si' |
si'2 mi'4 fad' |
fad'2 sold'4 fad'8 sold' |
la'4 sold' fad' mi' |
red'2\trill si'4 si' |
si'2 mi'4 fad' |
fad'2 sold'4 fad'8 sold' |
fad'4 mi'2 mi'4 |
red'2\trill red''4 si'8 sold' |
fad'2 fad'4 sold' |
fad'2 red''4 si'8 sold' |
fad'2 fad'4 sold' |
fad'2 fad'4 fad' |
fad' sold'8 fad' mi'4 si' |
si' la'8 sold' fad'4 fad' |
fad' fad'8 mi' fad' la' sold' fad' |
mi'4 mi'2 red'4\trill |
mi'2
