\clef "taille" mi'4 mi' |
red'\trill mi' dod' dod' |
red'2 si4 si |
si2 si4 si |
si2 mi'4 mi' |
red'\trill mi' dod' dod' |
red'2 si4 si |
si dod' sold fad |
fad2 fad'4 mi' |
red'2 fad'4 mi' |
red'2\trill fad'4 mi' |
red'2 fad'4 mi' |
fad'2 dod'4 dod' |
si2 si4 si |
si2 la4 dod' |
si si red' si |
si4. dod'8 si4 la |
sold2\trill
