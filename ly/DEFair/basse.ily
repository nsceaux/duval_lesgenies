\clef "basse" mi,4 |
mi,2 mi4 |
mi2 red4 |
mi4~ \tuplet 3/2 { mi8( sol fad) } \tuplet 3/2 { mi( fad sol) } |
fad4 si si, |
mi mi, r |
r r mi |
la re' re |
sol8 la sol fad sol mi |
la si la sol fad mi |
si4. la8 sol fad |
mi4 mi, r |
r r r8 mi |
la4 re' re |
sol8 la sol fad sol mi |
la si la sol fad mi |
si do' si la sol4 |
sol, fad, sol, |
re8 re' do' si la sol |
do4 do' la |
re'8 do' si la sol fa |
mi re do4. do'8 |
si la si sol do' re' |
mi' do' re'4 re |
sol4 sol fad |
mi8 fad sol mi si, la, |
sol,4 fad, mi, |
si, si si |
si8 la sol fad mi4 |
la sol fad |
si2 la4 |
sol red mi |
do'8 si la4 sol |
la si si, |
\tuplet 3/2 { mi8( si, sol,) } mi, sol, si, mi |
\tuplet 3/2 { red( do si,) } \ru#2 \tuplet 3/2 { mi( fad sol) } |
\tuplet 3/2 { la( si la) } \tuplet 3/2 { sol( la sol) } \tuplet 3/2 { fad( sol la) } |
\tuplet 3/2 { si si, si } la2 |
\tuplet 3/2 { sol8 fad mi } \tuplet 3/2 { red dod si, } \tuplet 3/2 { mi red mi } |
\tuplet 3/2 { do' si la } si4 si, |
mi8 fad sol mi si, sol, |
re4 re re |
re8 re' do' si la sol |
\tuplet 3/2 { fad8 mi re } fad8 sol la la, |
re4 re' dod' |
re' do' si |
\tuplet 3/2 { do'8 re' do' } \tuplet 3/2 { si do' si } \tuplet 3/2 { la si la } |
\tuplet 3/2 { sol( re si,) } \tuplet 3/2 { sol,( la, si,) } \tuplet 3/2 { do( si, la,) } |
sol,8 re sol la \tuplet 3/2 { si la sol } |
\tuplet 3/2 { do'( si la) } \tuplet 3/2 { si( do' si) } \tuplet 3/2 { la( si la) } |
\tuplet 3/2 { sol( la sol) } \tuplet 3/2 { fad( mi re) } \tuplet 3/2 { sol( la si) } |
\tuplet 3/2 { do'( re' do') } \tuplet 3/2 { si( do' si) } \tuplet 3/2 { la( si la) } |
\tuplet 3/2 { sol( la sol) } \tuplet 3/2 { fa( sol fa) } mi8 re |
do sol la si do' re' |
mi' do' re'4 re |
sol8 la sol fad mi4 |
mi,8 mi red4 mi |
