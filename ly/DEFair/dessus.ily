\clef "dessus" r8 mi' |
si'4 la' sol' |
do''8 si' la' si' \tuplet 3/2 { do''8( si' la') } |
\tuplet 3/2 { sol'8( si' mi') } \tuplet 3/2 { si'( mi'' red'') } \tuplet 3/2 { sol''( fad'' mi'') } |
\tuplet 3/2 { la''( sol'' la'') } \tuplet 3/2 { fad''( si'' la'') } \tuplet 3/2 { si''( fad'' si'') } |
\tuplet 3/2 { sol''( la'' sol'') } \tuplet 3/2 {  mi''( si' sol') } mi'4 |
R2.*4 |
r4 r r8 si' |
\tuplet 3/2 { mi''8( sol'' mi'') } \tuplet 3/2 { si'8( mi'' si') } \tuplet 3/2 { sol'( si' sol') } |
mi'4 si4. si'8 |
la'4 la' la'\trill |
sol'4. re''8 re'' sol'' |
mi''4. mi''8 fad'' sol'' |
fad''4\trill sol''8 fad'' sol'' la'' |
\tuplet 3/2 { si''8( do''' si'') } \tuplet 3/2 { la''8( si'' la'') } \tuplet 3/2 { sol''( la'' sol'') } |
fad''4.\trill re''8 re'' sol'' |
mi''4. mi''8 mi'' la'' |
\tuplet 3/2 { fad''8( sol'' la'') } \tuplet 3/2 { re''8( mi'' fad'') } \tuplet 3/2 { sol''( la'' si'') } |
\tuplet 3/2 { do'''( do'' re'') } \tuplet 3/2 { mi''( fad'' sol'') } \tuplet 3/2 { fad''( sol'' la'') } |
re''4. sol''8 sol''4~ |
sol'' fad''4.\trill sol''8 |
sol''8 sol' \tuplet 3/2 { si'8( do'' si') } red''8 si' |
mi''4 si' r8 fad'' |
sol''4 red'' mi'' |
red'' si' r |
r r8 red'' \tuplet 3/2 { sol''8( fad'' sol'') } |
mi''4 r8 mi'' \tuplet 3/2 { la''8( sol'' la'') } |
fad''4 r8 fad'' si'' si'' |
si''4 la'' sol'' |
fad''4. fad''8 \tuplet 3/2 { sol''8( fad'' mi'') } |
mi''4( red''4.)\trill mi''8 |
mi''4. si'8 sol' mi' |
\tuplet 3/2 { si'8( la' si') } \tuplet 3/2 { sol'( la' si') } \tuplet 3/2 { sol'( la' si') } |
\tuplet 3/2 { dod''( re'' mi''8) } \tuplet 3/2 { dod''( si' dod'') } \tuplet 3/2 { la'( si' dod'') } |
\tuplet 3/2 { red''( mi'' fad'') } \tuplet 3/2 { red''( dod'' red'') } \tuplet 3/2 { si'( dod'' red'') } |
\tuplet 3/2 { mi''( red'' mi'') } \tuplet 3/2 { fad''( mi'' fad'') } \tuplet 3/2 { sol''( fad'' sol'') } |
\tuplet 3/2 { la''( sol'' fad'') } fad''4.\trill mi''8 |
mi''2 r4 |
r r sol'' |
\tuplet 3/2 { fad''8( mi'' re'') } fad''8 sol'' la'' la' |
re''4. re''8 re'' dod'' |
\tuplet 3/2 { re''8 fad'' re'' } la' re'' \tuplet 3/2 { mi'' fad'' sol'' } |
fad''4 \appoggiatura mi''8 re''4 r |
R2. |
r4 sol''8 re'' \tuplet 3/2 { mi''8 re'' do'' } |
re''4 si'8 do'' \tuplet 3/2 { re''8( do'' re'') } |
sol'4. sol'8 do''4 |
\tuplet 3/2 { si'8( do'' si') } \tuplet 3/2 { la'( fad' la') } re'8 re'' |
sol'4. sol'8 do''4 |
si'8 do'' \tuplet 3/2 { re''8( do'' re'') } \tuplet 3/2 { sol'( la' si') } |
\tuplet 3/2 { do'' mi'' re'' } \tuplet 3/2 { do'' sol'' fa'' } mi''8 re'' |
\tuplet 3/2 { do''8( si' do'') } la'8 re'' \tuplet 3/2 { do''( re'' do'') } |
si'4\trill sol' r8 si' |
\tuplet 3/2 { mi''8( sol'' mi'') } \tuplet 3/2 { si'( mi'' si') } \tuplet 3/2 { sol'( si' sol') } |
