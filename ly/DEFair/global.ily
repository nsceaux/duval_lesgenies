\key mi \minor \midiTempo#120
\digitTime\time 3/4 \partial 4 s4 s2.*11 \bar "||"
\segnoMark s2.*28 s8 \endMark "[Fin.]" s8 s2 s2.*16 \bar "|."
\endMark\markup { Comme ci-devant jusqu’au mot fin \raise#1 \musicglyph#"scripts.segno" }

