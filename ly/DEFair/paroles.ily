L’a -- mour a be -- soin de vos char -- mes
pour se ren -- dre vic -- to -- ri -- eux,
l’a -- mour a be -- soin de vos char -- mes
pour se ren -- dre vic -- to -- ri -- eux,
il tri -- om -- phe plus par vos yeux
qu’il ne tri -- om -- phe par ses ar -- mes,
il tri -- om -- phe plus par vos yeux
qu’il ne tri -- om -- phe par ses ar -- mes.
Lors -- que vous sou -- met -- tez un cœur
l’a -- mour est fier de sa vic -- toi -- re,
il ne comp -- te pour rien sa gloi -- re,
il ne comp -- te pour rien sa gloi -- re,
quand lui seul en est le vain -- queur.
L’a-
