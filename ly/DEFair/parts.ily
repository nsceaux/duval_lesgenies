\piecePartSpecs
#`((dessus #:instrument "[Violons]")
   (dessus2 #:instrument "[Violons]")
   (basse #:score-template "score-basse-continue-voix"
          #:instrument
          ,#{ \markup\center-column { [Basses et B.C.] } #})

   (silence #:on-the-fly-markup , #{
\markup\fontsize#-2 \column {
  \livretPers Une Africaine
  \livretVerse#8 { L’amour a besoin de vos charmes }
  \livretVerse#8 { Pour se rendre victorieux, }
  \livretVerse#8 { Il triomphe plus par vos yeux }
  \livretVerse#8 { Qu’il ne triomphe par ses armes. }
  \livretVerse#8 { Lorsque vous soumettez un cœur }
  \livretVerse#8 { L’amour est fier de sa victoire, }
  \livretVerse#8 { Il ne compte pour rien sa gloire, }
  \livretVerse#8 { Quand lui seul en est le vainqueur. }
  \livretVerse#8 { L’amour a besoin, &c. }
}#}))
