\score {
  \new ChoirStaff <<
    \new Staff \with { instrumentName = "[Violons]" } <<
      \global \includeNotes "dessus"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column\smallCaps { [Une Africaine] }
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s2.*6\break s2.*6\pageBreak
        s2.*5 s2 \bar "" \break s4 s2.*5\pageBreak
        s2.*6\break s2.*6\pageBreak
        s2.*6\break s2.*6\pageBreak
        s2.*6\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
