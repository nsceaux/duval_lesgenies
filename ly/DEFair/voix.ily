\clef "vdessus" r4 |
R2.*4 |
r4 r r8 mi' |
si'2 la'8 sol' |
do''[ si'] la'[ sol'] la'[ fad'] |
si'4 \appoggiatura la'8 sol'4 si'8 mi'' |
dod''4\trill dod''8 dod'' red'' mi'' |
red''2\trill r4 |
r r r8 mi' |
si'2 la'8 sol' |
do''([ si']) la'([ si']) do''([ la']) |
si'4 \appoggiatura la'8 sol'4 si'8 mi'' |
dod''4\trill dod''8 dod'' red'' mi'' |
red''4\trill sol'8 la' si' do'' |
re''4 do'' si' |
la'4.\trill re''8 do'' si' |
\tuplet 3/2 { do''8([\melisma si' do'']) } mi''8*2/3([ re'' mi'']) do''([ re'' mi'']) |
la'([ si' do'']) re''([ do'' re'']) si'([ do'' re'']) |
sol'([ la' si']) do''([ si' la']) re''([ mi'' fad''])( |
sol''4.)\melismaEnd re''8 mi''16([ re''8.]) |
do''8([ si']) \afterGrace la'2\trill( sol'8) |
sol'2 r4 |
r sol'8 si' red'' si' |
mi''4 fad''4. sol''8 |
fad''4.\trill sol''8 fad'' mi'' |
\tuplet 3/2 { red''8([\melisma mi'' fad'']) } si'8*2/3([ la' si']) sol'([ la' si']) |
dod''([ re'' mi'']) dod''([ si' dod'']) la'([ si' dod'']) |
red''([ mi'' fad'']) red''([ dod'' red'']) si'([ dod'' red'']) |
mi''([ red'' mi'']) fad''([ mi'' fad'']) sol''([ fad'' sol''])( |
la''4.)\melismaEnd red''8 mi''4 |
fad'' \afterGrace fad''2\trill( mi''8) |
mi''2 r4 |
R2.*5 |
r4 r sol'' |
fad''8 mi'' re'' do'' si' do'' |
la'4.\trill la'8 si' do'' |
re''4. mi''8 fad'' sol'' |
fad''4\trill \appoggiatura mi''8 re''4 r |
r r sol''8 re'' |
mi''4 fa''8 mi'' re'' do'' |
re''4 si'\trill r |
r4 r sol''8 re'' |
mi''4 fa''8 mi'' re'' do'' |
re''4\melisma do''8([ re'']) \tuplet 3/2 { si'8([ do'' re'']) } |
mi''4 fa''8([ mi'']) \tuplet 3/2 { re''([ mi'' do''])( } |
re''4)\melismaEnd si' do''8 re'' |
mi''2~ mi''8 fad'' |
sol''2 sol''8 fad'' |
\appoggiatura fad''8 sol''2 r4 |
r r r8 mi' |
