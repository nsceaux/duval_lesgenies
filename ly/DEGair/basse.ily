\clef "basse" do4 do' si sol8. fa16 |
mi4 do si, sol,8. fa,16 |
mi,8. do,16 do'8. fa16 sol4 sol, |
do do' si8. do'16 re'8. re16 |
sol4 do' si sol8. fa16 |
mi4 do si, sol,8. fa,16 |
mi,8. do,16 do'8. si16 do'8. la16 do'8. re'16 |
mi'4 do'8. si16 do'8. la16 mi'8. mi16 |
la4 mi fa8. mi16 re8. do16 |
la8. sol16 fa8. mi16 re8. do16 si,8. do16 |
sol4 do' si sol8. fa16 |
mi8. do16 do'8. fa16 sol4 sol, |
do
%%
do'4 fa sol |
do do' fa8. sol16 lab8. fa16 |
sol4 r sol, r |
sol r sol, r |
sol do' fa sol |
do do' fa sol8. sol,16 |
do4 r8 mib' mib'8. mib16 sol8. mib16 |
sib4 r8 sib sib8. sib,16 re8. sib,16 |
mib4 r re r |
sib, do lab, sib, |
mib, do' fa sol |
do do' fa8. sol16 lab8. fa16 |
sol4 r sol, r |
sol r sol, r |
sol do' fa sol |
lab do' fa sol8. sol,16 |
do4
