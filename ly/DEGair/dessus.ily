\clef "dessus" r4 mi''8. do''16 sol''8. sol'16 si'8. sol'16 |
do''8. do'16 mi''8. do''16 sol''8. sol'16 si'8. sol'16 |
do''4 mi''8. la''16 sol''8. fa''16 mi''8. re''16 |
mi''8. do''16 mi''8. la''16 sol''8. la''16 re''8. fad''16 |
sol''8. sol'16 mi''8. do''16 sol''8. sol'16 si'8. sol'16 |
do''8. do'16 mi''8. do''16 sol''8. sol'16 si'8. sol'16 |
do''4 mi''8. fa''16 mi''8. fa''16 mi''8. la''16 |
sold''4\trill mi''8. fa''16 mi''8. la''16 si''8. sold''16 |
la''8. la'16 do'''8. si''16 la''8. sol''16 fa''8. mi''16 |
fa''8. mi''16 la''8. sol''16 fa''8. mi''16 re''8. do''16 |
si'8. sol'16 mi''8. do''16 sol''8. sol'16 si'8. sol'16 |
do''8. do'16 mi''8. do''16 sol''8. sol'16 si'8. sol'16 |
do''4
%%
mib''8. do''16 lab''8. sol''16 fa''8. sol''16 |
mib''8. do''16 mib''8. do''16 lab''8. sol''16 fa''8. lab''16 |
sol''4 re''8. mib''16 re''8. sol''16 fa''8. mib''16 |
re''8. sol'16 re''8. mib''16 re''8. sol''16 fa''8. mib''16 |
re''4 mib''8. do''16 lab''8. sol''16 fa''8. sol''16 |
mib''8. do''16 mib''8. do''16 lab''8. sol''16 fa''8. sol''16 |
do''4 sol''8. mib''16 sib''8. sol''16 mib''8. sol''16 |
fa''4 re''8. fa''16 sib''8. fa''16 re''8. fa''16 |
sib'4 sol''8. mib''16 lab''8. sol''16 fa''8. lab''16 |
re''8. sib'16 mib''8. fa''16 fa''4.\trill mib''8 |
mib''4 mib''8. do''16 lab''8. sol''16 fa''8. sol''16 |
mib''8. do''16 mib''8. do''16 lab''8. sol''16 fa''8. lab''16 |
sol''4 re''8. mib''16 re''8. sol''16 fa''8. mib''16 |
re''8. sol'16 re''8. mib''16 re''8. sol''16 fa''8. mib''16 |
re''4 mib''8. do''16 lab''8. sol''16 fa''8. sol''16 |
mib''8. do''16 mib''8. do''16 lab''8. sol''16 fa''8. sol''16 |
do''4
