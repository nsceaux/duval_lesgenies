\digitTime\time 2/2 \midiTempo#120
\key do \major
\beginMarkSmall\markup { \concat { 1 \super er } air }
s4 \segnoMark s2. s1*11 s8 \endMark "[Fin.]" s \bar "||"
\key sol \minor
\beginMarkSmall\markup { \concat { 2 \super e } air }
s2. s1*15 s4 \bar "|."
\endMark\markup { au premier \raise#1 \musicglyph#"scripts.segno" }
