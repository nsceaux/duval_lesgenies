\clef "haute-contre" mi' sol' sol' sol' |
sol' do'' re'' sol' |
sol'8. mi'16 sol'8. fa'16 mi'8. re'16 do'8. si16 |
do'4 sol'8. fad'16 sol'8. sol'16 sol'8. re'16 |
si4\trill sol' sol' sol' |
sol' do'' re'' sol' |
sol'8. mi'16 sol'4 la' sol'8. fa'16 |
mi'4 la' la' sold'8. si'16 |
la'4 sol' do'' re'' |
do'' do'' si'8. sol'16 sol'4 |
sol' do'' re'' sol' |
sol'8. mi'16 sol'8. la'16 sol'4 fa' |
mi'4\trill do'' do'' si'\trill |
do'' do'' do'' do'' |
si'\trill si'8. do''16 si'8. mib''16 re''8. do''16 |
si'4\trill si'8. do''16 si'8. mib''16 re''8. do''16 |
si'4\trill do'' do'' si'\trill |
do'' do'' do'' si'\trill |
do'' r8 sib'!8 sib'4 sib' |
sib' r8 sib' re''4 sib' |
sol'\trill r fa' r |
lab' sol' do'' sib' |
sol'\trill do'' do'' si'\trill |
do'' do'' do'' do'' |
si'\trill si'8. do''16 si'8. mib''16 re''8. do''16 |
si'4\trill si'8. do''16 si'8. mib''16 re''8. do''16 |
si'4\trill do'' do'' si'\trill |
do'' do'' do'' si'\trill |
do''
