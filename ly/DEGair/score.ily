\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { [Violons et Hautbois] }
    } << \global \includeNotes "dessus" >>
    \ifFull <<
      \new Staff \with {
        instrumentName = \markup\center-column { [Hautes-contre] }
      } << \global \includeNotes "haute-contre" >>
      \new Staff \with {
        instrumentName = "[Tailles]"
      } << \global \includeNotes "taille" >>
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses, Bassons et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*5\break s1*5\break s1*5\pageBreak
        s1*5\break s1*5\break s1*3
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
