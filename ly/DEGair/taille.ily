\clef "taille" sol4 mi' re' re' |
do' sol' sol' re' |
do' do'8. re'16 mi'4 sol |
sol do'8. re'16 re'8. do'16 si8. la16 |
sol4 mi' re' re' |
do' sol' sol' re' |
do' mi'8. re'16 do'4 mi'8. fa'16 |
mi'4 do'8. re'16 mi'8. do'16 mi'8. re'16 |
do'4 do' re'8. mi'16 fa'8. sol'16 |
fa'8. sol'16 la'8. mi'16 fa'8. sol'16 re'8. mi'16 |
re'4 sol' sol' re' |
do'8. sol'16 sol'8. fa'16 mi'4 re' |
do' sol'8. mib'16 re'8. re'16 re'8. sol16 |
sol4 sol'8. mib'16 re'4 re' |
re' sol8. sol'16 sol'4 sol' |
sol' sol8. sol'16 sol'4 sol' |
sol'4 sol'8. mib'16 re'8. re'16 re'8. sol16 |
sol4 sol'8. mib'16 re'4 re' |
mib' r8 sol' sol'8. mib'16 mib'8. mib'16 |
re'4 r8 re' fa'8. fa'16 fa'8. re'16 |
mib'4 r fa' r |
fa' mib'2 re'4\trill |
mib' sol'8. mib'16 re'8. re'16 re'8. sol16 |
sol4 sol'8. mib'16 re'4 re' |
re'4 sol8. sol'16 sol'4 sol' |
sol' sol8. sol'16 sol'4 sol' |
sol' sol'8. mib'16 re'8. re'16 re'8. re'16 |
do'4 sol'8. mib'16 re'4 re' |
mib'
