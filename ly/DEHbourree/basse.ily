\clef "basse" do2 mi4 do |
sol mi2 fa8 mi |
re4 do si, sol, |
do2 do, |
do mi4 do |
sol sol, do la, |
re si,8 do re4 re, |
sol,1 |
sol,2 sol |
sol2. fad4 sol2 sol,4 sol |
sol2. fad4 |
sol8 la si sol do'4 do |
do2. si,4 |
do2. do'4 |
la sol la fa |
sol2 sol, do2. do4 |
do2. si,4 |
do2. do'4 |
la sol la fa |
sol2 sol, |
do1 |
