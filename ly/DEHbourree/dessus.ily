\clef "dessus"
do'''4 sol''2 do'''4 |
si'' sol''2 la''8 sol'' |
fa''4 mi'' re'' sol'' |
mi'' do'' mi'' sol'' |
do''' sol''2 do'''4 |
si'' sol''2 la''8 sol'' |
fad''4 sol'' re'' fad'' |
sol''2 sol' |
sol''4 sol'2 re''4 |
mi'' la'2 re''8 do'' |
si'4 sol' si' re'' |
mi'' la'2 re''8 do'' |
si'4 sol' sol'' la''8 sol'' |
fa''4 re''2 fa''4~ |
fa'' sol''8 fa'' mi''4 do''~ |
do'' mi''2 fa''8 mi'' |
re''4 do'' re'' si' |
do''2 sol''4 la''8 sol'' |
fa''4 re''2 fa''4~ |
fa'' sol''8 fa'' mi''4 do''~ |
do'' mi''2 fa''8 mi'' |
re''4 do'' re'' si' |
do''1 |
