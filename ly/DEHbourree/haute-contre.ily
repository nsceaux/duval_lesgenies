\clef "haute-contre" mi''4 do''2 mi''4 |
re'' do''2 do''4 |
sol' sol' sol' sol' |
sol' sol' do'' mi'' |
mi'' do''2 mi''4 |
re'' re'' do'' do''8 si' |
la'4 re''8 do'' si'4 la' |
sol'1 |
sol'2 si |
do'2. re'4 |
re' re' sol' si |
do'2. re'4 |
re'2 mi'4 mi' |
fa'2. re'4 |
mi' mi'8 fa' sol'4 sol' |
sol'2. re'4 |
re' mi' fa' re' |
mi'2 mi'4 mi' |
fa'2. re'4 |
mi' mi'8 fa' sol'4 sol' |
sol'2. re'4 |
re' mi' fa'2 |
mi'1\trill |
