\clef "taille" sol'2 sol'4 sol' |
sol'2. la4 |
si do' re' si |
do' mi' do' sol |
sol'2 sol'4 sol' |
sol' fa' mi' mi' |
fad' re'2 do'4 |
si1\trill |
si2\trill si |
la2. la4 |
sol si re' si |
la2. la4 |
si8 do' re' si do'4 do' |
re'2. re'4 |
do'2. mi'4 |
mi'2. la4 |
si do' si sol |
sol2 do'4 do' |
re'2. re'4 |
do'2. mi'4 |
mi'2. la4 |
si do' si sol |
sol1 |
