\clef "basse" do4 do' fa2 |
sib mib |
lab re |
sol4 do si, do |
sol sol,8 la, si,4 sol, |
do do' fa2 |
sib mib |
lab2 re |
sol8 fa mib4 re do |
sol1 |
sol2 mib4 mib' |
re'2. sib4 |
mib'2 mib4 mib' |
re'2. sib4 |
mib'2 mib4 mib'8 re' |
do'4 re' mib' do' |
re' do' sib la |
sol fad sol la |
sib do' re' re |
sol2 mib |
sib si |
do' do |
lab4 sol lab fa |
sol2 sol, |
do mib |
sib si |
do' do |
lab4 sol lab fa |
sol2 sol, |
do1 |
