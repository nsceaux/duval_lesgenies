\clef "dessus" mib''4 sol''2 fa''8 mib'' |
re''4 fa''2 mib''8 re'' |
do''4 mib''2 re''8 do'' |
si'4 do'' re'' mib'' |
re''8 do'' si' la' sol' la' si' sol' |
mib''4 sol''2 fa''8 mib'' |
re''4 fa''2 mib''8 re'' |
do''4 mib''2 re''8 do'' |
si'4 do'' re'' mib'' |
re''1\trill |
re''2 mib''4 sol'' |
sib''4 lab''8 sol'' fa''4 sib'' |
sol'' mib''2 sol''4 |
sib'' lab''8 sol'' fa''4 sib'' |
sol'' sol''2 do'''8 sib'' |
lab''4 sib'' lab'' sol'' |
fad'' re'' mi'' fad'' |
sol'' la'' sib'' do''' |
re''' do'''8 sib'' la''4\trill sol'' |
sol''2 sol''4 lab''8 sol'' |
fa''4 re''2 fa''4~ |
fa'' sol''8 fa'' mib''4 do''~ |
do'' mib''2 fa''8 mib'' |
re''4\trill do'' re'' si' |
do''2 sol''4 lab''8 sol'' |
fa''4 re''2 fa''4~ |
fa'' sol''8 fa'' mib''4 do''~ |
do'' mib''2 fa''8 mib'' |
re''4\trill do'' re'' si' |
do''1 |
