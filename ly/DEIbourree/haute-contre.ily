\clef "haute-contre" sol'2 lab' |
fa' sol' |
mib' fa' |
re'4 sol' sol' sol' |
sol'2. sol'4 |
sol'2 lab' |
fa' sol' |
mib' fa' |
re'4 sol' sol' sol' |
sol'1 |
sol'2 sib'4 sib' |
sib'2 sib' |
sib' sib'4 sib' |
sib'2 sib' |
sib'2. mib''4 |
mib'' re'' do'' mib'' |
re'' fad' sol' la' |
sib' do'' sib' la' |
sol' sol'2 fad'4\trill |
sol'2 mib''4 fa''8 mib'' |
re''2. re''4~ |
re'' mib''8 re'' do''4 sol'~ |
sol' do''2 re''8 do'' |
si'4\trill sol'2 fa'4 |
mib'2 mib''4 fa''8 mib'' |
re''2. re''4~ |
re'' mib''8 re'' do''4 sol'~ |
sol' do''2 re''8 do'' |
si'4\trill sol'2 fa'4 |
mib'1 |
