\clef "taille" mib' mib'2 re'8 do' |
re'4 re'2 do'8 sib |
do'4 do'2 sib8 lab |
sol4 mib' re' do' |
si8 la si do' re' mib' fa' re' |
mib'4 mib'2 re'8 do' |
re'4 re'2 do'8 sib |
do'4 do'2 sib8 lab |
sol4 do' si do' |
si1\trill |
si2\trill sol'4 sol' |
fa' re'8 mib' fa' mib' re' fa' |
mib'2 sol'4 sol' |
fa' re'8 mib' fa' mib' re' fa' |
mib'2. sol'4 |
sol'2. do''4 |
la' la' sol' re' |
re' re' re' re' |
re' mib' re' do' |
sib2 sib'~ |
sib' sol'~ |
sol' mib'~ |
mib'2. lab4 |
sol mib' re' sol |
sol2 sib'~ |
sib' sol'~ |
sol' mib'~ |
mib'2. lab4 |
sol mib' re' sol |
sol1 |
