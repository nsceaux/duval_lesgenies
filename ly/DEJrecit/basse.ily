\clef "basse" la2 sold4 mi |
la2. fad4 |
mi2 mi4 re8 dod |
si,2. |
dod2 mid4 dod |
fad2 re'4 si8 dod' |
fad2. mi4 |
re2. |
mi2 la8 sol fad mi |
re2. |
mi2~ mi8 re |
dod8. re16 mi4 mi, |
<>^"[Basses et B.C.]" la,8 la fad la re re, |
la, mi la sol la fad |
si4 si4. sol8 |
re' re re' si dod' re' |
mi'2 mi'8 re' |
dod'4 si4. la8 |
sold4. mi8 fad sold |
la4 la4. dod8 |
fad4 re4. mi8 |
la,2 la4~ |
la mi8 fad sol4 |
la si dod' |
re' re' re8 re |
sol4 sol mi |
si4. la8 sol4 |
fad mi4. re8 |
la2 fad8 fad |
si2 dod'8 re' |
mi'4 mi' dod' |
fad'4. fad8 si4 |
sol4 la4. la8 |
re4 dod4. re8 |
la sol la si dod' la |
re'4 re re |
sol4. fad8 mi4 |
si4. la8 sol4 |
dod'8 la dod' la dod' la |
re' sol la4 la, |
re4. \clef "dessus2" <>^"Violons [et clavecin]" re''8 fa''4 r8 fa'' |
dod''4. dod''8 re''4 re' |
la'2 la'4 fa'8 fa' |
do''4. re''8 mi''4 mi''8 do'' |
fa''2. la'8 la' |
re''4. sib'8 do''4 do''8 do' |
fa'4 r8 fa' fa'8 sol' la' fa' |
do''4 r8 la' re'' do'' si' la' |
mi''4 mi''8 re'' do''4 la' |
fa''4 fa''8 re'' mi''2 |
la'4 r8 la' re'' dod'' re'' mi'' |
fa''4 r8 re'' sol'' fa'' mi'' re'' |
dod''4 la'8 la' re''4 re''8 mi'' |
fa''2~ fa''4. dod''8 |
re''4 sib'8 sol' la'2 |
re'2. \allowPageTurn
\clef "vbasse" <>^"[B.C]" re4 |
re dod4. si,8 |
la,4 sold, la, re, |
mi,8 mi sold,2 |
la, dod |
re1~ |
re |
fad2 sol16 fad mi re |
la,1~ |
la,2 dod, |
re,2. |
re4. do8 si, la, |
sol,2. r16 sol sol sol |
sold8 mi la8.*5/6 la,32 la, la, re16 re re re fad,8. re,16 |
sol,4. fad,8 mi, re, |
la,2. sol,4 |
fad, fad8 re la4. re8 |
la,4~ la,32 la sol fad mi re dod si, la,4.*13/12 la,32 la, la, |
\once\set Staff.whichBar = "|"
