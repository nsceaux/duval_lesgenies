\clef "dessus" R1*3 R2. R1*3 R2. R1 R2.*3 | \allowPageTurn
<<
  \tag #'dessus1 {
    << R2. { s2 s8 <>^\markup\whiteout "Violons" } >>
    r4 r r8 la''\doux |
    fad'' mi'' re'' fad'' si'' si'' |
    la'' sol'' fad'' re''' re''' dod''' |
    si'' la'' sold'' la'' si''4 mi''8 mi'' re'' mi'' re'' dod'' |
    si'4. mi''8 mi'' mi'' |
    mi'' fad'' mi'' re'' dod'' mi'' |
    re''4. dod''8 re'' si' |
    dod''4 la' r |
    r sol''4. fad''8 |
    mi'' la'' la'' si'' sol'' la'' |
    fad'' mi'' fad'' sol'' la''4 |
    re''8 re'' mi'' fad'' sol'' mi'' |
    fad''4. si'8 dod''4 |
    re'' dod''4. re''8 |
    dod''4 mi''8 mi'' la'' la'' |
    fad'' fad'' si'' si'' si'' la'' |
    sold'' sold'' sold'' sold'' dod''' dod''' |
    la'' dod'' fad'' la'' fad'' re'' |
    sol''8 fad'' mi'' fad'' sol'' la'' |
    fad'' la''\fort sol'' fad'' mi'' re'' |
    dod'' re'' dod'' si' la' sol' |
    fad' la' fad' sol' la' fad' |
    si' la' sol' la' si' dod'' |
    re'' dod'' si' dod'' re'' si' |
    mi'' sol'' fad'' mi'' la''4~ |
    la''8 sol''16 fad'' mi''4.\trill re''8 |
    re''4. <>^"Violons I" la''8\doux la'' sol'' fa'' la'' |
    sol'' fa'' mi'' sol'' fa'' mi'' re'' mi'' |
    dod'' la' dod'' re'' mi''4 la''8 la'' |
    sol'' sol'' sol'' sol'' do''' sib'' sib'' do''' |
    la'' fa'' do'' fa'' la'' do''' do''' do''' |
    do''' la'' sib'' re''' sol'' sol'' la'' sib'' |
    la'' fa'' fa'' do'' la'4 r8 la'' |
    sol'' do''' do''' do''' la'' la'' re''' do''' |
    si'' mi'' sold'' mi'' la'' mi''' mi''' mi''' |
    do''' do''' do''' re''' si'' si'' dod''' re''' |
    dod'''4\trill r r r8 la' |
    re''4 r r8 re'' mi'' fa'' |
    sol'' mi'' mi'' re''16 dod'' re''8 fa'' fa'' sol'' |
    la'' la'' si'' dod''' re'''4. mi'''8 |
    la''4 re'''8 re''' re'''4 dod'''\trill |
    re'''2.
  }
  \tag #'dessus2 {
    r4 r r8 <>^\markup\whiteout "Violons" re''\doux |
    dod'' si' la' mi'' fad'' fad'' |
    re'' dod'' si' re'' sol'' sol'' |
    fad'' mi'' re'' si'' si'' la'' |
    sold'' fad'' mi'' fad'' sold'' mi'' |
    la''4 sold''4.\trill la''8 |
    mi'' mi'' re'' dod'' si' re'' |
    dod'' re'' dod'' si' la' dod'' |
    si'4. la'8 si' sold' |
    la'4 mi' r |
    r mi''4. re''8 |
    dod''4 re'' mi'' |
    la'8 sol' la' si' do'' re'' |
    si' si' dod'' re'' mi'' dod'' |
    re''4. re''8 mi''4 |
    fad'' sol''4. fad''8 |
    mi''4 dod''8 dod'' fad'' fad'' |
    re'' re'' re'' re'' mi'' fad'' |
    si' si' si' si' mi'' mi'' |
    dod'' la' re'' fad'' re'' si' |
    mi'' re'' dod'' re'' mi'' dod'' |
    re''4. la''8\fort sol'' fad'' |
    mi'' fad'' mi'' re'' mi'' dod'' |
    re'' dod'' re'' mi'' fad''4 |
    re''8 dod'' si'4 r8 sol'' |
    fad'' mi'' re''4 r8 si'' |
    la'' si'' la'' sol'' fad'' mi'' |
    fad'' mi''16 re'' dod''4. re''8 |
    re''4. <>^"Violons II" re''8 re'' la' re'' fa'' |
    mi'' re'' dod'' mi'' re'' mi'' fa'' sol'' |
    mi'' la'' mi'' re'' dod''4 fa''8 fa'' |
    mi'' mi'' mi'' fa'' sol'' sol'' sol'' mi'' |
    fa'' la'' do''' la'' fa'' la'' fa'' do'' |
    fa'' fa'' fa'' fa'' fa'' fa'' fa'' mi'' |
    fa'' do'' la' do'' fa'4 r8 fa'' |
    mi'' mi'' la'' la'' fad'' fad'' sold'' la'' |
    sold'' si'' si'' sold'' mi'' do''' do''' do''' |
    la'' la'' la'' la'' la'' la'' la'' sold'' |
    la'' mi'' la'' sol'' fa'' mi'' re'' dod'' |
    re'' la'' re''' do''' sib'' la'' sol'' fa'' |
    mi'' sol'' sol'' fa''16 mi'' fa''8 la'' re''' dod''' re'''4. dod'''16 si'' la''4. sol''8 |
    \appoggiatura sol''8 fa''4. mi''8 mi''4.\trill re''8 |
    re''2.
  }
  \tag #'dessus3 {
    \clef "dessus2" R2.*28 |
    r4 r8 <>^"Violons III" re''8 fa''4 r8 fa'' |
    dod''4. dod''8 re''4 re' |
    la'2 la'4 fa'8 fa' |
    do''4. re''8 mi''4 mi''8 do'' |
    fa''2. la'8 la' |
    re''4. sib'8 do''4 do''8 do' |
    fa'4 r8 fa' fa'8 sol' la' fa' |
    do''4 r8 la' re'' do'' si' la' |
    mi''4 mi''8 re'' do''4 la' |
    fa''4 fa''8 re'' mi''2 |
    la'4 r8 la' re'' dod'' re'' mi'' |
    fa''4 r8 re'' sol'' fa'' mi'' re'' |
    dod''4 la'8 la' re''4 re''8 mi'' |
    fa''2~ fa''4. dod''8 |
    re''4 sib'8 sol' la'2 |
    re'2.
  }
>> \allowPageTurn r4 |
R2. R1 R2. R1 R1*2 R2. R1 R1 R2.*2 R1 R1 R2. R1 R1*2
