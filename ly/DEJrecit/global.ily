\key la \major
\time 4/4 \midiTempo#80 s1
\digitTime\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 \grace s8 s2.
\digitTime\time 2/2 \midiTempo#160 s1*2 s2 \midiTempo#80 s2
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 \grace s8 s2.*3 \bar "||"
\key re \major
\midiTempo#160 s2.*21 \tempo "Très vite" s2.*7 \bar "||"
\key la \minor
\beginMarkSmall "Mineur" \digitTime\time 2/2 \midiTempo#160 s1*15 s2. \bar "||"
\key re \major \midiTempo#80 s4
\digitTime\time 3/4 s2.
\digitTime\time 2/2 s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 2/2 \midiTempo#160 s1*2
\digitTime\time 3/4 \midiTempo#80 s2.
\time 4/4 s1
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 s2.*2
\digitTime\time 2/2 s1
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 2/2 \midiTempo#160 s1*2 \bar "|."
