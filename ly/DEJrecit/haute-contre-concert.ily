\clef "dessus" R1*3 R2. R1*3 R2. R1 R2.*3 | \allowPageTurn
r4 r r8 re''\doux |
dod'' si' la' mi'' fad'' fad'' |
re'' dod'' si' re'' sol'' sol'' |
fad'' mi'' re'' si'' si'' la'' |
sold'' fad'' mi'' fad'' sold'' mi'' |
la''4 sold''4.\trill la''8 |
mi'' mi'' re'' dod'' si' re'' |
dod'' re'' dod'' si' la' dod'' |
si'4. la'8 si' sold' |
la'4 mi' r |
r mi''4. re''8 |
dod''4 re'' mi'' |
la'8 sol' la' si' do'' re'' |
si' si' dod'' re'' mi'' dod'' |
re''4. re''8 mi''4 |
fad'' sol''4. fad''8 |
mi''4 dod''8 dod'' fad'' fad'' |
re'' re'' re'' re'' mi'' fad'' |
si' si' si' si' mi'' mi'' |
dod'' la' re'' fad'' re'' si' |
mi'' re'' dod'' re'' mi'' dod'' |
re''4. la''8\fort sol'' fad'' |
mi'' fad'' mi'' re'' mi'' dod'' |
re'' dod'' re'' mi'' fad''4 |
re''8 dod'' si'4 r8 sol'' |
fad'' mi'' re''4 r8 si'' |
la'' si'' la'' sol'' fad'' mi'' |
fad'' mi''16 re'' dod''4. re''8 |
re''4. re''8 fa''4 r8 fa'' |
dod''4. dod''8 re''4 re' |
la'2 la'4 fa'8 fa' |
do''4. re''8 mi''4 mi''8 do'' |
fa''2. la'8 la' |
re''4. sib'8 do''4 do''8 do' |
fa'4 r8 fa' fa'8 sol' la' fa' |
do''4 r8 la' re'' do'' si' la' |
mi''4 mi''8 re'' do''4 la' |
    fa''4 fa''8 re'' mi''2 |
la'4 r8 la' re'' dod'' re'' mi'' |
fa''4 r8 re'' sol'' fa'' mi'' re'' |
dod''4 la'8 la' re''4 re''8 mi'' |
fa''2~ fa''4. dod''8 |
re''4 sib'8 sol' la'2 |
re'2. \allowPageTurn r4 |
R2. R1 R2. R1 R1*2 R2. R1 R1 R2.*2 R1 R1 R2. R1 R1*2
