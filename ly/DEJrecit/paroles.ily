Fi -- nis -- sez ces con -- certs, votre hom -- ma -- ge m’of -- fen -- se.

Qu’en -- tends-je, ô ciel !

Re -- con -- nais- moi :
en é -- loi -- gnant l’ob -- jet dont tu sui -- vais la loi,
sous ses traits em -- prun -- tés j’ai rem -- pli ma ven -- gean -- ce.

Is -- mé -- ni -- de, grands dieux !

Tu ne la ver -- ras plus.
Au -- près de ton ri -- val qu’elle aime,
el -- le goûte un bon -- heur ex -- trê -- me,
et laisse à ton a -- mour des re -- grets su -- per -- flus.

\tag #'recit {
  Sui -- vons la fu -- reur qui me gui -- de,
  al -- lons pu -- nir et l’a -- mante et l’a -- mant,
  al -- lons pu -- nir et l’a -- mante et l’a -- mant ;
  ah ! que ne puis-je aus -- si, per -- fi -- de,
  t’im -- mo -- ler à ma rage en cet af -- freux mo -- ment,
  t’im -- mo -- ler, t’im -- mo -- ler à ma rage en cet af -- freux mo -- ment.
  
  I -- ci je bra -- ve ta ven -- gean -- ce,
  mon pou -- voir é -- ga -- le le tien,
  mon pou -- voir é -- ga -- le le tien ;
  je vais de ces a -- mants ser -- rer le doux li -- en,
  et c’est moi qui prend leur dé -- fen -- se.
  Je vais de ces a -- mants ser -- rer le doux li -- en,
  et c’est moi, et c’est moi qui prend leur dé -- fen -- se.
}
La per -- fi -- de tri -- omphe, et mal -- gré moi je sens
les a -- mou -- reux trans -- ports de la plus vi -- ve flam -- me ;
el -- le pro -- tè -- ge ces a -- mants !
Où suis- je ? quelle hor -- reur s’em -- pa -- re de mon â -- me !
Je ne puis me ven -- ger, que je suis mal -- heu -- reux !
Du moins, si je ne puis ex -- er -- cer ma van -- gean -- ce,
dé -- trui -- sons ce pa -- lais, té -- moin de mon of -- fen -- se ;
que ne puis- je pé -- rir pour é -- tein -- dre mes feux.
