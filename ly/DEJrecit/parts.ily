\piecePartSpecs
#`((dessus #:score "score-dessus")
   (flute-hautbois #:score "score-vents")
   (dessus1 #:score "score-dessus1")
   (dessus2 #:score "score-dessus2")
   (basse #:score-template "score-basse-continue-voix"
          #:instrument "[B.C.]"
          #:music ,#{ s1*3 s2. s1*3 s2. s1 s2.*3 s2.\break s2.*27 s1*15 s2.\break #})

   (silence #:on-the-fly-markup , #{
\markup\fontsize#-2 \fill-line {
  \column {
    \livretPersDidas Pircaride à sa suite
    \livretVerse#12 { Finissez ces concerts, votre hommage m’offense. }
    \livretPers Numapire
    \livretVerse#8 { Qu’entends-je, ô ciel ! }
    \livretPers Pircaride
    \livretVerse#8 { \transparent { Qu’entends-je, ô ciel ! } Reconnais-moi : }
    \livretVerse#12 { En éloignant l’objet dont tu suivais la loi, }
    \livretVerse#12 { Sous ses traits empruntés j’ai rempli ma vengeance. }
    \livretPers Numapire
    \livretVerse#12 { Isménide, grands dieux ! }
    \livretPers Pircaride
    \livretVerse#12 { \transparent { Isménide, grands dieux ! } Tu ne la verras plus. }
    \livretVerse#8 { Auprès de ton rival qu’elle aime, }
    \livretVerse#8 { Elle goûte un bonheur extrême, }
    \livretVerse#12 { Et laisse à ton amour des regrets superflus. }
    \livretPers Numapire
    \livretVerse#8 { Suivons la fureur qui me guide, }
  }
  \column {
    \livretVerse#10 { Allons punir & l’amante & l’amant ; }
    \livretVerse#8 { Ah ! que ne puis-je aussi, perfide, }
    \livretVerse#12 { T’immoler à ma rage en cet affreux moment. }
    \livretPersDidas Pircaride sur un char de feu
    \livretVerse#8 { Ici je brave ta vengeance, }
    \livretVerse#8 { Mon pouvoir égale le tien ; }
    \livretVerse#12 { Je vais de ces amants serrer le doux lien, }
    \livretVerse#8 { Et c’est moi qui prend leur défense. }
    \livretPers Numapire
    \livretVerse#12 { La perfide triomphe, & malgré moi je sens }
    \livretVerse#12 { Les amoureux transports de la plus vive flamme ; }
    \livretVerse#8 { Elle protège ces amants ! }
    \livretVerse#12 { Où suis-je ? quelle horreur s’empare de mon âme ! }
    \livretVerse#12 { Je ne puis me venger, que je suis malheureux ! }
    \livretVerse#12 { Du moins, si je ne puis exercer ma vangeance, }
    \livretVerse#12 { Détruisons ce palais, témoin de mon offense ; }
    \livretVerse#12 { Que ne puis-je périr pour éteindre mes feux. }
  }
}#}))
