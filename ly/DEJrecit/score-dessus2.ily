\score {
  <<
    \new Staff \with {
      \remove "Page_turn_engraver" \tinyStaff \haraKiriFirst
    } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
      \new Staff <<
        \global \keepWithTag #'dessus3 \includeNotes "dessus"
        { s1*3 s2. s1*3 s2. s1 s2.*3 s2.*28 \break
          s1*15 s2.\break }
      >>
    >>
  >>
  \layout { }
}