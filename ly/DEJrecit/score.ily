\score {
  \new ChoirStaff <<
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
      \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'recit \includeNotes "voix"
    >> \keepWithTag #'recit \includeLyrics "paroles"
    \new Staff \with { instrumentName = "[B.C.]" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion {
        s1*3 s2. s1*3 s2. s1 s2.*3\break
        s2.*28 \break
        s1*15 s2.\break
      }
      \origLayout {
        s1*2 s2 \bar "" \break s2 s2. s1 s2 \bar "" \break s2 s1 s2.\pageBreak
        s1 s2.*2\break s2.*5\pageBreak
        s2.*7\break s2.*9\pageBreak
        s2.*8\break s1*6 s2 \bar "" \pageBreak
        s2 s1*4\break s1*4 s2. \pageBreak
        s4 s2. s1\break s2. s1*2\break s1 s2. s1 s2 \bar "" \pageBreak
        s2 s2.*2 s1 s2 \bar "" \break s2 s2. s1\break s1*2\pageBreak
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
