\ffclef "vbas-dessus" <>^\markup\character Pircaride
r8 la'16 la' dod''8 dod''16 la' mi''8 si'16 dod'' re''8 re''16 dod'' |
dod''4\trill dod''8
\ffclef "vbasse" <>^\markup\character Numapire
la8 dod'4 la |
mi'
\ffclef "vbas-dessus" <>^\markup\character Pircaride
r16 si' sold' si' mi' si' si' si' si'8. dod''16 |
\appoggiatura dod''8 re''4. dod''8 si'16 la' sold' fad' |
mid'4\trill sold'8 sold' dod''4 dod''8 sold' |
la'4 dod''8 dod'' fad''4 fad''8 mid'' |
\appoggiatura mid'' fad''2 fad''8
\ffclef "vbasse" <>^\markup\character Numapire
la16 la lad8 lad16 si |
si4
\ffclef "vbas-dessus" <>^\markup\character Pircaride
r16 si' si' si' dod''8 re'' |
sold'8 r16 si' mi''4 dod''8 dod''16 dod'' re''8. mi''16 |
\appoggiatura mi''8 fad'' fad''16 mi'' re''4 re''16 dod'' si' la' |
sold'8 sold'16 mi' si'4 si'8 dod''16 re'' |
mi''8 mi''16 fad'' si'4\trill si'8 dod'' |
la'4 r
<<
  \tag #'basse { r4 R2.*27 R1*15 r2 r4 }
  \tag #'recit {
    \ffclef "vbasse" <>^\markup\character Numapire
    r8 re |
    la4 la4. fad8 |
    si4 si4. sol8 |
    re'4 re'8 si dod' re' |
    mi'2 mi'8 re' |
    dod'4 si4. la8 |
    sold4. mi8 fad sold |
    la4 la4. dod8 |
    fad4 re4. mi8 |
    la,2 la4~ |
    la mi8 fad sol4 |
    la si dod' |
    re' re' re8 re |
    sol4 sol mi |
    si4. la8 sol4 |
    fad mi4. re8 |
    la2 fad8 fad |
    si2 dod'8 re' |
    mi'4 mi' dod' |
    fad'4. fad8 si4 |
    sol4 la4. la8 |
    re4 r r |
    R2.*6 |
    \ffclef "vbas-dessus" <>^\markup\character Pircaride
    r4 r8 re'' fa''4 r8 fa'' |
    dod''4. dod''8 re''4 re' |
    la'2 la'4 fa'8 fa' |
    do''4. re''8 mi''4 mi''8 do'' |
    fa''2. la'8 la' |
    re''4. sib'8 do''4 do''8 do' |
    fa'4 r8 fa' fa'8 sol' la' fa' |
    do''4 r8 la' re'' do'' si' la' |
    mi''4 mi''8 re'' do''4 la' |
    fa''4 fa''8 re'' mi''2 |
    la'4 r8 la' re'' dod'' re'' mi'' |
    fa''4 r8 re'' sol'' fa'' mi'' re'' |
    dod''4 la'8 la' re''4 re''8 mi'' |
    fa''2~ fa''4. dod''8 |
    re''4 sib'8 sol' la'2 |
    re'2.
  }
>>
\ffclef "vbasse" <>^\markup\character Numapire
r8 re'16 re' |
fad8 fad16 re la la la si dod'8. re'16 |
mi'4 si8 si16 si dod'4 re' |
sold\trill si8 si16 si dod'8. re'16 |
dod'8\trill dod' r16 mi mi mi sol4 sol8 sol16 fad |
fad2 r4 re' |
fad2 fad4 r8 fad16 re |
la4. re'8 si16 si dod' re' |
dod'4\trill dod' r8 mi16 mi mi8 fad16 sol |
dod4 r8 la16 mi sol4 sol8 fad |
fad2\trill r8 re |
la4 fad sol8 la |
si4 si8 dod' re'4 re'8 si |
mi'4 mi'8 dod'16 dod' fad'8 fad16 re la8. fad16 |
si4. si8 dod' re' |
dod'4\trill dod' r8 la16 la dod8 dod16 mi |
<< la,4 \new Voice { \voiceTwo fad, } >> la8 re' dod'4\trill dod'8 re' |
\appoggiatura re'8 mi'2 r |
