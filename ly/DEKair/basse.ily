\clef "basse" re4 re'8 |
la16 sol fad sol la fad |
si sol si sol si sol |
re' la fad la re mi |
fad mi re mi fad re |
la sol fad sol la fad |
si si mi' mi mi mi |
la la, dod mi dod la, |
mi, mi sold si sold mi |
la la, dod mi dod la, |
mi mi, mi, mi, mi, mi, |
\mergeDifferentlyDottedOn
<<
  { la, la dod' la mi mi |
    dod16 la, dod mi sol mi |
    la, mi, la, dod mi dod | } \\
  { la,4. | la, | la, | }
>>
la16 mi dod mi la, dod |
re la, fad, la, re, re |
sol la sol fad mi re |
la la, la, la, la, la, |
re4 re'16 re |
la la dod' la fad la |
si si re' si sol si |
re' la fad la re mi |
fad re fad la re' re |
la mi dod la, mi la |
re fad mi mi, sold, mi, |
la, la dod' mi' dod' la |
mi' mi sold si sold mi |
la la, dod mi dod la, |
mi mi mi mi, mi, mi, |
la, la mi la dod mi |
la, dod mi sol mi sol |
dod mi la dod' mi' dod' |
la mi dod mi la, dod |
re, re la, re fad re |
si si dod' la dod' re' |
la la la la, la, la, |
re4 si,8 |
mi16 re mi fad sol mi |
fad mi fad sol fad mi |
re mi fad mi re dod |
si,8. fad16 sold lad |
si dod' re' dod' si la |
sold la sold fad mid fad |
dod si, dod re dod si, |
la, si, dod si, la, sold, |
fad, dod red mid fad sold |
la sold fad sold la si |
dod' dod mid sold mid dod |
fad fad, la, dod la, fad, |
si, dod re dod si, re |
dod re dod si, la, sold, |
la, si, la, sold, fad, mid, |
fad,8. fad16 sold la |
si la sold la si sold |
dod' dod mid dod fad la |
sold si la sold fad si, |
dod si, dod re dod dod, |
fad, fad, fad, fad, fad, fad, |
fad, fad fad fad fad fad, |
si, si fad re si, re |
mi8 dod fad |
si,8 si16 la sol fad |
mi sol fad mi fad fad, |
si, si, re fad si si |
la sol la si la si |
sol la sol fad sol mi |
red dod si, dod red si, |
mi red mi fad sol mi |
la si la sol la fad |
si8 si si |
si,16 si, red si, fad si, |
si si red' fad' red' fad' |
la red' fad' red' la red' |
sol si fad si mi si |
red dod si, dod red mi |
la, mi la si do' la |
si si, red fad si si, |
si, si, si, si, si, si, |
mi mi mi mi do la, |
re si, re fa re si, |
mi mi, sold, si, sold, mi, |
la, la, dod mi dod la, |
re fa re si, re si, |
mi mi, sold, si, mi mi, |
la, la do' mi' do' mi' |
sold fad sold si sold la |
mi si, sold, si, mi, mi |
do mi la do si, la, |
fa re fa la fa re |
mi fa mi re do si, |
la, do la, do mi la |
fa re mi mi, mi, mi, |
la,4 la16 si |
dod' si la si dod' la |
re' mi' re' do' sib la |
sol mi la la, dod la, |
re4 re8 |
la dod' la |
re' dod' re' |
la la la16 la |
dod8 dod8. re16 |
la,4 re'8 |
la16 si la sol la fad |
si dod' si la si sol |
re' la fad la re8 |
fad fad re |
sol sol mi |
la la fad16 fad |
si8 si8. sol16 |
re'4.~ |
re'4 re'8 |
re'8. re'16 re' re |
sol4 sol16 mi |
si4 mi'16 mi |
la8 si8. dod'16 |
re'4.~ |
re'8 fad re |
re re8. re16 |
la4.~ |
la~ |
la8 la la |
la dod'8. mi'16 |
sol4.~ |
sol8 fad re |
la dod'8. mi'16 |
fad4.~ |
fad8. fad16 re8 |
si dod'8. re'16 |
la8( la,4) |
re4. |
