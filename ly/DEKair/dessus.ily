\clef "dessus" r8 r <>\doux
<<
  \tag #'dessus1 {
    fad''8 |
    mi''16 mi'' la'' la'' la'' la'' |
    fad'' si'' si'' si'' si'' si'' |
    la'' la'' la'' la'' la'' la'' |
    la'' la'' la'' la'' la'' la'' |
    mi'' la'' la'' la'' la'' la'' |
    la''8 sold''16 si'' la'' sold'' |
    la'' la'' la'' la'' la'' la'' |
    sold'' si'' mi'' sold'' si' mi'' |
    dod'' dod'' mi'' mi'' mi'' dod'' |
    si' si' si' dod'' re'' mi'' |
    dod'' mi'' la'' mi'' dod'' mi'' |
    la' mi'' la'' dod''' mi''' dod''' |
    la'' dod''' la'' mi'' dod'' la' |
    dod'' mi'' sol'' sol'' sol'' sol'' |
    fad'' re'' la' re'' fad'' la'' |
    re''8 sol''16 la'' sol'' fad'' |
    mi'' re'' mi'' fad'' sol'' la'' |
    fad''4
  }
  \tag #'dessus2 {
    re''8 |
    dod''16 dod'' dod'' fad'' fad'' fad'' |
    re'' re'' re'' sol'' sol'' sol'' |
    fad'' fad'' fad'' fad'' fad'' fad'' |
    re'' re'' re'' re'' re'' re'' |
    dod'' dod'' dod'' dod'' dod'' dod'' |
    re''8 si'16 re'' dod'' si' |
    dod'' mi'' mi'' mi'' mi'' dod'' |
    si' sold' si' sold' mi' sold' |
    la' dod'' la' mi' la' la' |
    sold' sold' sold' la' si' sold' |
    la' la' la' la' la' dod'' |
    mi'' mi'' mi'' mi'' mi'' mi'' |
    mi'' la'' mi'' dod'' la' dod'' |
    mi'' sol'' mi'' mi'' dod'' mi'' |
    la' fad' re' fad' la' fad' |
    sol' si' mi'' re'' mi'' fad'' |
    sol'' fad'' mi''8.\trill re''16 |
    re''4 \startHaraKiri
  }
>> fad''16 re'' |
mi'' mi'' la'' mi'' fad'' la'' |
re'' re'' re'' re'' sol'' sol'' |
fad'' re'' la' re'' fad'' sol'' |
la'' la'' fad'' re'' la' re'' |
dod'' la' dod'' mi'' dod'' la' |
re'' re'' re'' si' mi'' mi'' |
mi'' mi' la' dod'' mi'' la'' |
sold'' si'' sold'' mi'' si' mi'' |
dod'' mi' la' dod'' mi'' dod'' |
si' mi'' mi'' re'' dod'' si' |
dod'' mi' la' dod'' la' dod'' |
mi'' la' dod'' mi'' dod'' mi'' |
la'' dod''' la'' mi'' dod'' la' |
dod'' mi'' sol'' mi'' dod'' mi'' |
fad'' re'' fad'' la'' re''' la'' |
fad'' re'' la' mi' la' re'' |
mi'' re'' mi'' fad'' sol'' mi'' |
fad''8 re'' <>\doux <<
  \tag #'dessus1 {
    fad''8 |
    sol''16 la'' sol'' fad'' mi'' re'' |
    dod'' dod'' dod'' dod'' dod'' dod'' |
    re'' re'' re'' dod'' re'' mi'' |
    fad'' fad' sold' lad' si' dod'' |
    re'' dod'' si' la' sold' fad' |
    si'8. dod''16 si' la' |
    sold' sold' sold' sold' sold' sold' |
    la' sold' fad' sold' la' si' |
    dod''8. dod''16 fad'' mid'' |
    fad'' sold'' la'' sold'' fad'' sold'' |
    mid'' dod'' dod'' dod'' sold' dod'' |
    la' fad'' fad'' fad'' dod'' fad'' |
    re'' dod'' si' dod'' re'' si' |
    sold'8. mid''16 fad'' sold'' |
    dod'' re'' dod'' si' la' sold' |
    la' la'' la'' la'' la'' la'' |
    fad'' si'' si'' si'' si'' si'' |
    sold'' mid'' sold'' mid'' dod'' dod'' |
    re'' re'' dod'' dod'' fad'' fad'' |
    fad''8 fad'' mid'' |
    fad''16
  }
  \tag #'dessus2 {
    \stopHaraKiri re''8 |
    mi''16 fad'' mi'' re'' dod'' si' |
    fad'' fad'' fad'' fad'' sold'' lad'' |
    si'' si' si' lad' si' dod'' |
    re''8. dod''16 re'' mi'' |
    fad'' fad'' fad'' fad'' sold'' la'' |
    mid''8. fad''16 sold'' la'' |
    dod'' dod'' mid'' sold'' mid'' dod'' |
    fad'' sold'' la'' sold'' fad'' mid'' |
    fad''8. dod''16 dod'' si' |
    la' si' dod'' si' la' si' |
    sold' sold'' sold'' mid'' sold'' mid'' |
    dod'' dod'' dod'' dod'' la' dod'' |
    fad' la' sold' la' si' sold' |
    mid'8. sold'16 la' si' |
    la' sold' fad' mid' fad' sold' |
    la' dod'' dod'' dod'' dod'' fad'' |
    red'' red'' red'' red'' red'' sold'' |
    mid'' dod'' dod'' dod'' la' fad' |
    si' sold' la' si' la' sold' |
    la'8 sold'4\trill |
    fad'16 \startHaraKiri
  }
>> fad''16\fort lad'' dod''' lad'' fad'' |
mi'' fad'' lad'' dod''' lad'' mi'' |
re'' si' re'' fad'' si''8 |
si''4 lad''8 |
si''16 fad'' re'' fad'' si' re'' |
mi'' mi'' mi'' dod'' fad'' dod'' |
re'' re'' si' si' fad' fad'' |
red'' fad'' la'' fad'' red'' fad'' |
si' sol'' si'' sol'' mi'' sol'' |
la'' do''' si'' la'' sol'' fad'' |
sol'' si' mi'' sol'' mi'' sol'' |
do'' mi'' do'' mi'' la' do'' |
fad' fad'' red'' fad'' si' red'' |
fad' fad' si' red'' si' red'' |
fad'' si' red'' fad'' si'' si'' |
si'' fad'' si'' fad'' red'' si' |
mi'' si' fad'' si' sol'' si' |
la'' fad'' red'' la'' la'' sol'' |
fad'' mi'' fad'' sol'' la'' fad'' |
red'' red'' fad'' red'' si' mi'' |
mi'' si'' la'' si'' fad'' si'' |
sold''8\trill mi'' <>\doux <<
  \tag #'dessus1 {
    la''16 mi'' |
    fa'' fa'' fa'' fa'' fa'' fa'' |
    mi'' sold'' si'' sold'' mi'' sold'' |
    la'' dod'' mi'' dod'' la' mi'' |
  }
  \tag #'dessus2 {
    \stopHaraKiri do''16 do'' |
    re'' re'' re'' re'' re'' re'' |
    si' mi'' mi'' mi'' si' mi'' |
    dod'' la' la' la' la' mi'' | \startHaraKiri
  }
>>
fa''16 la'' fa'' la'' re'' fa'' |
si' mi'' si' mi'' sold' si' |
do'' mi'' la'' do''' la'' la' |
mi'' si' mi'' sold' si' mi' |
mi' si' mi'' si' sold' mi' |
la' do'' mi'' la' si' do'' |
re'' fa'' re'' fa'' si' re'' |
sold' si' la' sold' la' si' |
do'' mi'' do'' la' do'' mi'' |
la'' si'' sold''8. la''16 |
la''16 la' dod'' mi'' dod'' la' |
sol' la' dod'' mi'' dod'' sol' |
fa' re' fa' la' re''8~ |
re'' dod''8.\trill re''16 |
re''16 la' re'' fa'' re'' fa'' |
mi'' dod'' la' dod'' mi'' sol'' |
fad'' re'' mi'' dod'' re'' si' |
dod'' mi'' dod'' la' dod'' mi'' |
sol'' mi'' dod'' sol'' sol'' fad'' |
mi''4\trill <<
  \tag #'dessus1 {
    fad''8 |
    mi''16 mi'' mi'' mi'' la'' la'' |
    fad'' fad'' fad'' fad'' si'' si'' |
    la'' la'' fad'' fad'' re'' re'' |
  }
  \tag #'dessus2 {
    \stopHaraKiri re''8 |
    dod''16 dod'' dod'' dod'' dod'' fad'' |
    re'' re'' re'' re'' re'' sol'' |
    fad'' fad'' re'' re'' la' re'' | \startHaraKiri
  }
>>
la'16 la' re'' la' fad' la' |
si' re'' sol'' si'' sol'' si'' |
mi'' mi'' la'' dod''' la'' dod''' |
fad'' re'' re'' si' re'' sol'' |
fad'' la'' fad'' la'' re''' la'' |
fad'' la'' fad'' re'' la' la' |
la' fad' re' fad' la' re'' |
re'' re'' si' re'' re'' sol'' |
fad'' red'' fad'' red'' si' mi'' |
mi'' mi'' mi'' re'' dod'' mi'' |
la' la' re'' fad'' la'' la'' |
la'' fad'' la'' fad'' re'' la' |
fad' fad'' fad'' la'' fad'' re'' |
dod'' la' dod'' mi'' la'' mi'' |
dod'' dod'' dod'' la' dod'' mi'' |
sol'' mi'' dod'' sol'' sol'' fad'' |
mi'' dod'' la' dod'' mi'' la'' |
la'' la' la' dod'' dod'' mi'' |
sol'' mi'' la'' re''' la'' la'' |
la'' dod''' la'' mi'' mi'' mi'' |
mi'' mi'' dod'' mi'' la' dod'' |
re'' la' fad' re' fad' la' |
re'' si' mi'' re'' dod'' mi'' |
fad''32 la'' sol'' fad'' mi''4\trill |
re''4. |
