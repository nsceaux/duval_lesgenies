\clef "haute-contre" R4.*18 |
r8 r la'16 la' |
la' la' la' la' la' la' |
fad' fad' fad' fad' si' si' |
la' fad' re' fad' la' la' |
la' la' la' la' la' fad' |
mi' mi' la' dod'' la' la' |
la' la' sold' sold' si' si' |
la' dod' mi' la' la' la' |
si' si' si' si' si' si' |
la' dod' mi' la' dod'' mi' |
mi' mi' mi' mi' mi' mi' |
mi' dod' mi' la' mi' mi' |
dod' mi' la' dod'' la' mi' |
mi' la' mi' dod' la mi' |
mi' mi' la' la' mi' mi' |
re' fad' la' re'' la' la' |
la' la' sol' sol' sol' fad' |
sol' fad' mi' re' dod' mi' |
re'4 r8 |
R4.*20 | \allowPageTurn
r16 dod'\fort fad' lad' fad' dod' |
lad dod' fad' lad' fad' dod' |
fad'16 fad' si' re'' re''8 |
dod''4 dod''8 |
re''16 re'' si' si' sol' sol' |
sol' sol' sol' mi' dod'' lad' |
si' si' fad' fad' re' re' |
fad' fad' fad' fad' fad' fad' |
mi' mi' sol' mi' si mi' |
fad' la' sol' fad' mi' red' |
mi' mi' sol' si' sol' si' |
la' do'' la' do'' fad' mi' |
red' si' fad' si' red' fad' |
fad' fad' fad' si' fad' si' |
red' fad' si' red'' red'' red'' |
red'' red'' red'' red'' si' fad' |
sol' si' sol' si' sol' si' |
fad' mi' red' mi' fad' sol' |
la' sol' fad' mi' fad' fad' |
fad' fad' la' fad' red' mi' |
fad' fad' fad' fad' fad' red' |
mi'4 r8 |
R4.*2 |
r8 r la'16 la' |
la' fa' la' fa' fa' re' |
mi' sold' mi' sold' si mi' |
mi' mi' mi' la' mi' la' |
si' sold' si' mi' sold' si |
si mi' mi' sold' mi' si |
mi' la' do'' la' sold' la' |
si' re'' si' re'' la' la' |
si' sold' la' si' mi' mi' |
mi' la' mi' mi' la' do'' |
re'' re'' si' si' si' dod''! |
dod'' mi' la' dod'' la' mi' |
sol' sol' sol' sol' sol' sol' |
la' la' la' la' la' la' |
mi'8 mi' mi' |
fa'16 fa' la' re'' la' re'' |
dod'' la' mi' la' dod'' mi'' |
re'' la' dod'' la' fad' re' |
la' dod'' la' mi' la' dod'' |
mi'' dod'' la' mi'' mi'' re'' |
dod''4\trill r8 |
R4.*2 |
r8 r fad'16 fad' |
re' re' la' fad' re' fad' |
sol' sol' si' re'' si' si' |
dod'' la' dod'' mi'' dod'' mi'' |
re'' si' si' si' si' re'' |
re'' re'' re'' re'' la' fad' |
la' re'' la' fad' fad' fad' |
fad' re' re' re' fad' la' |
si' si' sol' si' si' si' |
si' fad' si' fad' sol' sol' |
sol' sol' sol' fad' mi' sol' |
fad' re' fad' la' re'' re'' |
re'' la' re'' la' fad' re' |
re' la' la' re'' la' fad' |
mi' la' mi' la' dod'' la' |
mi' la' la' mi' la' dod'' |
mi'' dod'' la' mi'' mi'' re'' |
dod'' la' mi' la' dod'' mi'' |
dod'' mi' mi' la' la' dod'' |
mi'' dod'' re'' la' re'' re'' |
dod'' mi'' dod'' la' la' la' |
dod'' dod'' la' dod'' mi' la' |
la' fad' re' la re' fad' |
fad' re' dod' re' mi' la' |
la' la' sol' sol' sol' sol' |
fad'4.\trill |
