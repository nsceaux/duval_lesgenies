\tag #'numapire {
  Ser -- vez les trans -- ports de ma ra -- ge,
  ra -- va -- gez ce sé -- jour, qu’il per -- de ses at -- traits ;
  que le feu dé -- vo -- rant le con -- sume à ja -- mais,
  et qu’il n’offre aux re -- gards,
  et qu’il n’offre aux re -- gards qu’une ef -- fray -- ante i -- ma -- ge.

  Ser -- vez les trans -- ports de ma ra -- ge,
  ra -- va -- gez ce sé -- jour, qu’il per -- de ses at -- traits ;
  que le feu dé -- vo -- rant le con -- sume à ja -- mais,
  et qu’il n’offre aux re -- gards,
  et qu’il n’offre aux re -- gards qu’une ef -- fray -- ante i -- ma -- ge.

  Ser -- vez les trans -- ports de ma ra -- ge,
  ra -- va -- gez, __  ra -- va -- gez __ ce sé -- jour, qu’il per -- de ses at -- traits ;
  que le feu dé -- vo -- rant le con -- sume à ja -- mais,
  et qu’il n’offre aux re -- gards __ qu’une ef -- fray -- ante i -- ma -- ge.

  Ser -- vez les trans -- ports de ma ra -- ge,
  ra -- va -- gez ce sé -- jour, qu’il per -- de ses at -- traits ;
  que le feu dé -- vo -- rant le con -- sume à ja -- mais,
  que le feu dé -- vo -- rant le con -- sume à ja -- mais,
  à ja -- mais, __ à ja -- mais.
  
  Ser -- vez les trans -- ports de ma ra -- ge,
  ser -- vez les trans -- ports de ma ra -- ge,
  ra -- va -- gez ce sé -- jour, qu’il per -- de ses at -- traits ;
  que le feu dé -- vo -- rant le con -- sume à ja -- mais,
  ra -- va -- gez __ ce sé -- jour, qu’il per -- de ses at -- traits,
  ra -- va -- gez, ra -- va -- gez ce sé -- jour,
  ser -- vez les trans -- ports de ma ra -- ge,
  ra -- va -- gez __ ce sé -- jour, __ qu’il per -- de ses at -- traits ;
  que le feu dé -- vo -- rant __ le con -- sume à ja -- mais, __
  et qu’il n’offre aux re -- gards, __
  et qu’il n’offre aux re -- gards __ qu’une ef -- fray -- ante i -- ma -- ge.
}
\tag #'vdessus {
  Ser -- vons les trans -- ports de sa ra -- ge,
  ra -- va -- geons se sé -- jour, qu’il per -- de ses at -- traits ;
  que le feu dé -- vo -- rant le con -- sume à ja -- mais,
  et qu’il n’offre aux re -- gards,
  et qu’il n’offre aux re -- gards qu’une ef -- fray -- ante i -- ma -- ge.

  Ser -- vons les trans -- ports de sa ra -- ge,
  ra -- va -- geons se sé -- jour, qu’il per -- de ses at -- traits ;
  que le feu dé -- vo -- rant le con -- sume à ja -- mais, __
  et qu’il n’offre aux re -- gards qu’une ef -- fray -- ante i -- ma -- ge.

  Ser -- vons les trans -- ports de sa ra -- ge,
  ra -- va -- geons se sé -- jour, qu’il per -- de ses at -- traits ;
  que le feu dé -- vo -- rant le con -- sume à ja -- mais.

  Ser -- vons les trans -- ports de sa ra -- ge,
  ra -- va -- geons se sé -- jour.

  Ser -- vons les trans -- ports de sa ra -- ge,
  ra -- va -- geons se sé -- jour, qu’il per -- de ses at -- traits,
  qu’il per -- de ses at -- traits ;
  que le feu dé -- vo -- rant __ le con -- sume à ja -- mais,
  le con -- sume à ja -- mais,
  le con -- sume à ja -- mais, __
  et qu’il n’offre aux re -- gards qu’une ef -- fray -- ante i -- ma -- ge.
}
\tag #'vhaute-contre {
  Ser -- vons les trans -- ports de sa ra -- ge,
  ra -- va -- geons se sé -- jour, qu’il per -- de ses at -- traits ;
  que le feu dé -- vo -- rant le con -- sume à ja -- mais,
  et qu’il n’offre aux re -- gards,
  et qu’il n’offre aux re -- gards qu’une ef -- fray -- ante i -- ma -- ge.

  Ser -- vons les trans -- ports de sa ra -- ge,
  ra -- va -- geons se sé -- jour, qu’il per -- de ses at -- traits ;
  que le feu,
  que le feu dé -- vo -- rant,
  que le feu dé -- vo -- rant le con -- sume à ja -- mais,
  à ja -- mais, __ à ja -- mais.

  Ser -- vons les trans -- ports de sa ra -- ge,
  ra -- va -- geons se sé -- jour, qu’il per -- de ses at -- traits ;
  que le feu dé -- vo -- rant le con -- sume à ja -- mais.

  Ser -- vons les trans -- ports de sa ra -- ge,
  ra -- va -- geons se sé -- jour.

  Ser -- vons les trans -- ports de sa ra -- ge,
  ra -- va -- geons se sé -- jour, qu’il per -- de ses at -- traits,
  qu’il per -- de ses at -- traits ;
  que le feu dé -- vo -- rant le con -- sume à ja -- mais,
  et qu’il n’offre aux re -- gards,
  et qu’il n’offre aux re -- gards,
  et qu’il n’offre aux re -- gards,
  aux re -- gards qu’une ef -- fray -- ante i -- ma -- ge.
}
\tag #'vtaille {
  Ser -- vons les trans -- ports de sa ra -- ge,
  ra -- va -- geons se sé -- jour, qu’il per -- de ses at -- traits ;
  que le feu dé -- vo -- rant le con -- sume à ja -- mais,
  et qu’il n’offre aux re -- gards,
  et qu’il n’offre aux re -- gards qu’une ef -- fray -- ante i -- ma -- ge.

  Ser -- vons les trans -- ports de sa ra -- ge,
  ra -- va -- geons se sé -- jour, qu’il per -- de ses at -- traits ;
  que le feu,
  que le feu dé -- vo -- rant,
  que le feu dé -- vo -- rant le con -- sume à ja -- mais,
  à ja -- mais, à ja -- mais, à ja -- mais.

  Ser -- vons les trans -- ports de sa ra -- ge,
  ra -- va -- geons se sé -- jour, qu’il per -- de ses at -- traits ;
  que le feu dé -- vo -- rant le con -- sume à ja -- mais.

  Ser -- vons les trans -- ports de sa ra -- ge,
  ra -- va -- geons se sé -- jour.

  Ser -- vons les trans -- ports de sa ra -- ge,
  ra -- va -- geons se sé -- jour, qu’il per -- de ses at -- traits,
  qu’il per -- de ses at -- traits ;
  que le feu dé -- vo -- rant le con -- sume à ja -- mais,
  et qu’il n’offre aux re -- gards,
  et qu’il n’offre aux re -- gards,
  et qu’il n’offre aux re -- gards,
  aux re -- gards qu’une ef -- fray -- ante i -- ma -- ge.
}
\tag #'vbasse {
  Ser -- vons les trans -- ports de sa ra -- ge,
  ra -- va -- geons se sé -- jour, qu’il per -- de ses at -- traits ;
  que le feu dé -- vo -- rant le con -- sume à ja -- mais,
  et qu’il n’offre aux re -- gards,
  et qu’il n’offre aux re -- gards qu’une ef -- fray -- ante i -- ma -- ge.

  Ser -- vons les trans -- ports de sa ra -- ge,
  ra -- va -- geons se sé -- jour, qu’il per -- de ses at -- traits ;
  que le feu dé -- vo -- rant le con -- sume à ja -- mais,
  que le feu dé -- vo -- rant le con -- sume à ja -- mais,
  à ja -- mais, __  à ja -- mais.

  Ser -- vons les trans -- ports de sa ra -- ge,
  ra -- va -- geons se sé -- jour, qu’il per -- de ses at -- traits ;
  que le feu dé -- vo -- rant le con -- sume à ja -- mais.

  Ser -- vons les trans -- ports de sa ra -- ge,
  ra -- va -- geons se sé -- jour.

  Ser -- vons les trans -- ports de sa ra -- ge,
  ra -- va -- geons se sé -- jour, __
  qu’il per -- de ses at -- traits ;
  que le feu dé -- vo -- rant, dé -- vo -- rant __ le con -- sume à ja -- mais, __
  et qu’il n’offre aux re -- gards, __
  et qu’il n’offre aux re -- gards __ qu’une ef -- fray -- ante i -- ma -- ge.
}
