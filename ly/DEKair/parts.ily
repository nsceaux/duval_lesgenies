\piecePartSpecs
#`((dessus #:score-template "score-2dessus"
           #:instrument "Violons"
           #:music ,#{ s4.*19\break s4.*17\break s4.*22\break s4.*20\break s4.*23\break #})
   (dessus1 #:instrument "Violons")
   (dessus2 #:instrument "Violons")
   (dessus2-haute-contre #:notes "dessus2-haute-contre")
   (taille)
   (parties)
   (basse #:instrument
          ,#{\markup\center-column { [Basses et B.C.] }#})
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
