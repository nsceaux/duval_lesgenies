\score {
  \new StaffGroupNoBar \with { \haraKiri } <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { instrumentName = "Violons" } <<
        \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
        \new Staff \with { \haraKiriFirst } <<
          \global \keepWithTag #'dessus2 \includeNotes "dessus"
        >>
      >>
      \ifComplet\ifFull <<
        \new Staff \with {
          instrumentName = \markup\center-column { [Hautes-contre] }
        } << \global \includeNotes "haute-contre" >>
        \new Staff \with { instrumentName = "[Tailles]" } <<
          \global \includeNotes "taille"
        >>
      >>
    >>
    \ifComplet\new ChoirStaff <<
      \new Staff = "vdessus" \with { instrumentName = "[Dessus]" } \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \ifFull\new Staff \with {
        instrumentName = \markup\center-column { [Hautes-contre] }
      } \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \ifFull\new Staff \with { instrumentName = "[Tailles]" } \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \with { instrumentName = "[Basses]" } \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with { instrumentName = \markup\character Numapire } \withLyrics <<
      \global \keepWithTag #'numapire \includeNotes "voix"
    >> \keepWithTag #'numapire \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4.*6\break s4.*6\pageBreak
        s4.*5\break s4.*6\pageBreak
        s4.*6\break s4.*5\pageBreak
        s4.*6\break s4.*7\pageBreak
        s4.*7\break s4.*7\pageBreak
        s4.*6\break s4.*6\pageBreak
        s4.*7\break s4.*7\pageBreak
        s4.*6\break s4.*7\pageBreak
        s4.*6\break s4.*6\pageBreak
        s4.*6\break s4.*4\pageBreak
        s4.*4\break s4.*4
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
