\clef "taille" R4.*18 |
r8 r re'16 re' |
dod' dod' dod' dod' re' re' |
re' re' re' re' re' re' |
re' re' re' re' fad' mi' |
re' fad' re' la fad la |
la dod' mi' la' mi' dod' |
re' re' mi' mi' sold' sold' |
la' la' la' la' mi' mi' |
mi' sold' sold' sold' sold' sold' |
la' mi' mi' mi' dod' dod' |
la la la la la sold |
la la dod' mi' dod' la |
la la la la la la |
la mi' dod' la dod' dod' |
la la dod' dod' la la |
la la re' fad' la' fad' |
fad' fad' mi' mi' mi' re' |
la si dod' re' mi' dod' |
re'4 r8 |
R4.*20 |
r16 lad\fort lad lad lad lad |
lad lad' lad' lad' lad' lad' |
si' fad' re' si fad fad |
sol8 sol' fad' |
fad'16 fad' fad' re' re' si |
dod' dod' si si lad fad |
fad fad fad si si si |
si si fad' red' si si |
si si mi' si sol si |
si si si si si si |
si si si mi' si mi' |
fad' la' fad' la' fad' fad' |
fad' red' si red' fad' si |
red' red' fad' fad' red' fad' |
si red' fad' si' fad' fad' |
fad' fad' fad' fad' fad' red' |
mi' mi' red' red' mi' mi' |
fad' mi' red' mi' fad' sol' |
la' sol' fad' mi' mi' mi' |
si si red' red' red' si |
si si si' si' si' fad' |
mi'4 r8 |
R4.*2 |
r8 r dod'16 dod' |
si si si si si si |
sold si mi' si si sold |
la do' mi' mi' mi' mi' |
mi' mi' mi' mi' mi' mi' |
sold si si mi' si sold |
la la la mi' mi' mi' |
la' la' la' la' re' si |
mi' mi' mi' mi' mi' sold |
la la la la la la |
fa' fa' mi' mi' mi' mi' |
mi' dod' mi' la' mi' dod' |
mi' re' dod' re' mi' dod' |
fa'16 sol' fa' mi' re' do' |
sib8 la la |
la16 la la la la' la' |
la' la' mi' mi' la' la' |
la' fad' mi' mi' fad' fad' |
mi' mi' mi' dod' dod' la |
la la' la' la' la' la' |
la'4 r8 |
R4.*2 |
r8 r re'16 re' |
re' re' la re' la re' |
re' si re' sol' mi' sol' |
mi' mi' mi' la' mi' la' |
la' fad' fad' re' fad' si' |
la' fad' la' fad' fad' re' |
re' fad' re' la re' re' |
re' la fad la re' fad' |
sol' sol' re' sol' re' mi' |
red' si red' red' mi' re' |
dod' dod' si si si dod' |
re' re' re' re' fad' fad' |
fad' re' fad' re' la fad |
la re' re' fad' re' la |
la dod' la dod' mi' dod' |
la mi' mi' dod' mi' la' |
mi' la' mi' dod' dod' re' |
mi' mi' dod' mi' la' dod'' |
la' dod' dod' mi' mi' la' |
dod'' la' la' la' fad' fad' |
mi' la' mi' dod' dod' mi' |
la' la' la' la' la' la' |
la la la la la re' |
re' re' sol' sol' sol' re' |
re' re' re' re' dod' dod' |
re'4. |
