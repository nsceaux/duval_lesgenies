<<
  \tag #'numapire {
    \clef "vbasse" r8 r re' |
    la la fad |
    si si sol |
    re' re' re16 mi |
    fad8 fad8. re16 |
    la4 la8 |
    si8. si16 dod' re' |
    dod'4\trill dod'16 la |
    mi'8 mi'8. mi'16 |
    mi'8 dod' la |
    mi mi8. mi16 |
    mi4 mi16 mi |
    sol8 mi8. sol16 |
    dod4 mi16 mi |
    la8 la8. la16 |
    la4 fad16 fad |
    si8 dod'8. re'16 |
    la4. |
    re4 \ifComplet <>^\markup\italic { avec le chœur } re'8 |
    la la fad |
    si si sol |
    re' re' re16 mi |
    fad8 fad8. re16 |
    la4 la8 |
    si8. si16 dod' re' |
    dod'4\trill dod'16 la |
    mi'8 mi'8. mi'16 |
    mi'8 dod' la |
    mi8 mi8. mi16 |
    la,4 mi16 mi |
    sol8 mi8. sol16 |
    dod4 mi16 mi |
    la8 la8. la16 |
    la4 fad16 fad |
    si8 dod'8. re'16 |
    la8( la,4) |
    re4 re'8 |
    dod' dod' mi' |
    lad lad dod' |
    fad8 fad fad16 mi |
    re16[ mi fad mi re dod]( |
    si,4) re'16 dod' |
    si[ dod' si la sold fad]( |
    mid8) mid8. sold16 |
    dod4 dod8 |
    fad8. sold16 la si |
    dod'4.~ |
    dod'4 dod16 dod |
    fad8 fad8. dod16 |
    re8 re si, |
    dod dod8. dod16 |
    dod4 dod16 dod |
    fad8 sold8. la16 |
    si4.~ |
    si8. sold16 la8 |
    mid fad8. si,16 |
    dod4. |
    fad8 r r |
    R4.*2 |
    r8 r fad |
    si8 si sol |
    mi fad fad, |
    si, si, si16 si |
    la8 la8. la16 |
    sol4 sol8 |
    red8. red16 red red |
    mi8 mi mi |
    la8 la8. fad16 |
    si8 si si |
    red' red'8. red'16 |
    red'8 si si |
    la la8. la16 |
    sol8 fad mi |
    red red8. mi16 |
    do'8 do'8. la16 |
    si4.~ |
    si8 si8. si,16 |
    mi4 mi'8 |
    si si re' |
    sold sold si |
    mi mi la8 |
    re re si, |
    mi mi mi |
    la, la, la16 la |
    sold8 sold8. la16 |
    mi4 re8 |
    do8. do16 si, la, |
    fa4 fa16 re |
    mi8 fad8. sold16 |
    la8 la la |
    re mi8. mi16 |
    la4 la16 si |
    dod'16[\melisma si la si dod' la] |
    re'[ mi' re' do' sib la]( |
    sol8)\melismaEnd la8. la16 |
    re4 re8 |
    la8. la16 si dod' |
    re'4 re16 re |
    la4 la16 la |
    dod8 dod8. re16 la,4 re'8 |
    la la fad |
    si si sol |
    re' re' re16 mi |
    fad[\melisma mi re mi fad re] |
    sol[ fad mi fad sol mi] |
    la[ sol fad sol la fad] |
    si[ la sol la si sol]( |
    re'8)\melismaEnd re'8. re'16 |
    re'4.~ |
    re'4 re8 |
    sol8. sol16 sol mi |
    si4 mi'16 re'! |
    dod'8 re'8. mi'16 |
    fad'4.~ |
    fad'~ |
    fad'8 fad re |
    la8 la8. la16 |
    la4.~ |
    la8 la la |
    la dod'8. mi'16 |
    sol4.~ |
    sol8 fad re |
    la dod'8. mi'16 |
    fad4.~ |
    fad8. fad16 re8 |
    si dod'8. re'16 |
    la8( la,4) |
    re4. |
  }
  \tag #'vdessus {
    \clef "vdessus" R4.*18 |
    r8 r fad'' |
    mi'' mi'' fad'' |
    re'' re'' sol'' |
    fad'' fad'' fad''16 sol'' |
    la''8 la'8. re''16 |
    dod''4\trill la'8 |
    re''8. si'16 mi'' mi'' |
    mi''4 mi''16 la'' |
    sold''8\trill mi''8. si'16 |
    dod''8 la' dod'' |
    si' dod''8. re''16 |
    dod''4\trill dod''16 dod'' |
    mi''8 sol''8. mi''16 |
    la''4 la'16 la' |
    dod''8 mi'' dod'' |
    fad''4 fad''16 la'' |
    re''8 la'8. re''16 |
    re''4( dod''8) |
    re''4 r8 |
    R4.*23 |
    r8 r fad'' |
    re'' re'' si' |
    mi'' dod'' fad'' |
    re'' si' si'16 si' |
    red''8 si'8. red''16 |
    mi''4 mi''8 |
    la''8. la''16 sol'' fad'' |
    sol''8 sol'' mi'' |
    do'' do''8. la'16 |
    fad'8 fad' fad' |
    si' si'8. si'16 |
    si'4.~ |
    si'~ |
    si'8 fad'' sol'' |
    la'' la''8. sol''16 |
    fad''8. sol''16 la''8 |
    red''8 si'8. mi''16 |
    mi''4( red''8) |
    mi''8 r r |
    R4.*2 |
    r8 r mi'' |
    fa'' fa'' re'' |
    si' mi'' si' |
    do'' la' do''16 la' |
    mi''8 mi''8. mi''16 |
    mi''4 mi'8 |
    la'8. la'16 si' do'' |
    re''4 si'16 re'' |
    sold'8 la'8. si'16 |
    do''8 mi'' mi'' |
    la'' sold''8. la''16 |
    la''4 r8 |
    R4.*3 |
    r8 r fa'' |
    mi'' la'' sol'' |
    fad'' mi'' re'' |
    dod'' la' mi''16 fad'' |
    sol''8 sol''8. fad''16 |
    mi''4\trill r8 |
    R4.*2 |
    r8 r re'' |
    la' la' fad' |
    si' si' sol' |
    dod'' dod'' dod''16 fad'' |
    re''8 re''8. sol''16 |
    fad''4 fad''8 |
    fad''8. fad''16 fad'' re'' |
    la''4 la''8 |
    re''8. re''16 re'' sol'' |
    fad''4 r8 |
    r r la''16 la' |
    re''8 fad''8. la''16 |
    la''4.~ |
    la''8 re'' re'' |
    dod'' dod''8. dod''16 |
    dod''8 mi'' mi'' |
    sol'' sol''8. fad''16 |
    mi''8 la'' la'' |
    la'' la''8. la''16 |
    la''4.~ |
    la''8 mi'' mi'' |
    mi'' mi''8. dod''16 |
    re''8. re''16 re''8 |
    re'' mi''8. fad''16 |
    fad''8( mi''4) |
    re''4. |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" R4.*18 |
    r8 r la' |
    la' la' la' |
    fad' fad' re' |
    re' re' la'16 la' |
    la'8 la'8. fad'16 |
    mi'4\trill la'8 |
    la' sold' sold'16 sold' |
    la'4 la'16 la' |
    si'8 sold'8. sold'16 |
    la'8 mi' mi' |
    mi' mi'8. mi'16 |
    mi'4 mi'16 mi' |
    dod'8 dod'8. dod'16 |
    mi'4 mi'16 mi' |
    mi'8 la' mi' |
    re'4 la'16 la' |
    la'8 sol'8. fad'16 |
    fad'4( mi'8) |
    fad'4 r8 |
    R4.*23 |
    r8 r fad' |
    fad' fad' sol' |
    sol' mi' fad' |
    fad' fad' re'16 re' |
    fad'8 fad'8. fad'16 |
    mi'4 mi'8 |
    fad'8. fad'16 mi' red' |
    mi'8 sol' sol' |
    fad' fad' fad' fad'4. |
    fad'4 fad'8 |
    fad' red' red' |
    fad' fad'8. fad'16 |
    mi'8 si' si' |
    si' fad'8. mi'16 |
    mi'8 la'8. fad'16 |
    fad'4.~ |
    fad'8 fad'8. la'16 |
    sold'4\trill r8 |
    R4.*2 |
    r8 r la' |
    la' la' la' |
    sold' sold' sold' |
    la' mi' mi'16 mi' |
    mi'8 mi'8. la'16 |
    sold'4\trill mi'8 |
    mi'8. mi'16 mi' mi' |
    si4 re'16 si |
    si8 mi'8. mi'16 |
    mi'8 mi' mi' |
    fa' mi'8. mi'16 |
    mi'4 r8 |
    R4.*3 |
    r8 r re' |
    dod' mi' mi' |
    fad' la' la' |
    la' la' la'16 la' |
    la'8 la'8. la'16 |
    la'4 r8 |
    R4.*2 |
    r8 r fad' |
    re' re' re' |
    re' re' mi' |
    mi' mi' mi'16 mi' |
    fad'8 fad'8. re'16 |
    la'4 la'8 |
    la'8. la'16 la' la' |
    la'4 fad'8 |
    sol'8. sol'16 re' mi' |
    red'4\trill r8 |
    R4. |
    r8 r la'16 la' |
    fad'8\trill la'8. la'16 |
    la'8 fad' fad' |
    mi' mi'8. mi'16 |
    mi'8 la' la' |
    mi' mi'8. re'16 |
    mi'8 mi' mi' |
    mi' mi'8. mi'16 |
    mi'8 re' fad' |
    mi' la'8. la'16 |
    la'8 la'8. la'16 |
    la'8. la'16 la'8 |
    la'8 sol'8. la'16 |
    la'8( sol'4) |
    fad'4.\trill |
  }
  \tag #'vtaille {
    \clef "vtaille" R4.*18 |
    r8 r re' |
    dod' dod' re' |
    re' re' si |
    la la fad'16 mi' |
    re'8 re'8. la16 |
    la4 dod'8 |
    re' mi' si16 si |
    la4 mi'16 mi' |
    mi'8 mi'8. mi'16 |
    mi'8 mi' dod' |
    la8 la8. sold16 |
    la4 la16 la |
    la8 la8. la16 |
    la4 dod'16 dod' |
    la8 dod' la |
    la4 re'16 fad' |
    fad'8 mi'8. re'16 |
    la4. |
    la4 r8 |
    R4.*23 |
    r8 r dod' |
    si re' re' |
    dod' si lad |
    si si si16 si |
    si8 si8. si16 |
    si4 si8 |
    si8. si16 si si |
    si8 si si |
    mi' mi' mi' |
    red'4.\trill |
    red'4 red'8 |
    red' si si |
    red' red'8. red'16 |
    mi'8 red' mi' |
    fad' si8. si16 |
    la8 mi'8. mi'16 |
    fad'8 red'8. si16 |
    si8 si8. si16 |
    si4 r8 |
    R4.*2 |
    r8 r dod' |
    si si fa' |
    mi' si mi' |
    mi' do' la16 do' |
    si8 si8. do'16 |
    si4 sold8 |
    la8. la16 sold la |
    la4 la16 la |
    sold8 do'8. si16 |
    la8 do' do' |
    re' si8. dod'!16 |
    dod'4 r8 |
    R4.*3 |
    r8 r la |
    la mi' dod' |
    re' mi' fad' |
    mi' dod' dod'16 re' |
    mi'8 mi'8. re'16 |
    dod'4\trill r8 |
    R4.*2 |
    r8 r la |
    re' re' la |
    si si si |
    dod' dod' dod'16 dod' |
    re'8 re'8. si16 |
    re'4 re'8 |
    re'8. re'16 re' fad' |
    fad'4 re'8 |
    si8. si16 si si |
    si4 r8 |
    R4. |
    r8 r re'16 fad' |
    re'8 re'8. fad'16 |
    re'8 la la |
    la la8. la16 |
    la8 dod' dod' |
    dod' dod'8. re'16 |
    dod'8 mi' mi' |
    dod' dod'8. dod'16 |
    dod'8 la re' |
    dod' mi'8. dod'16 |
    dod'8 dod'8. mi'16 |
    re'8. re'16 fad'8 |
    fad' mi'8. re'16 |
    re'4( dod'8) |
    re'4. |
  }
  \tag #'vbasse {
    \clef "vbasse" R4.*18 |
    r8 r re'8 |
    la la fad |
    si si sol |
    re' re' re16 mi |
    fad8 fad8. re16 |
    la4 la8 |
    si8. si16 dod' re' |
    dod'4\trill dod'16 la |
    mi'8 mi'8. mi'16 |
    mi'8 dod' la |
    mi8 mi8. mi16 |
    la,4 mi16 mi |
    sol8 mi8. sol16 |
    dod4 mi16 mi |
    la8 la8. la16 |
    la4 fad16 fad |
    si8 dod'8. re'16 |
    la8( la,4) |
    re4 r8 |
    R4.*23 |
    r8 r fad |
    si8 si sol |
    mi fad fad, |
    si, si, si16 si |
    la8 la8. la16 |
    sol4 sol8 |
    red8. red16 red red |
    mi8 mi mi |
    la8 la8. fad16 |
    si8 si si |
    red' red'8. red'16 |
    red'8 si si |
    la la8. la16 |
    sol8 fad mi |
    red red8. mi16 |
    do'8 do'8. la16 |
    si4.~ |
    si8 si8. si,16 |
    mi4 r8 |
    R4.*2 |
    r8 r la8 |
    re re si, |
    mi mi mi |
    la, la, la16 la |
    sold8 sold8. la16 |
    mi4 re8 |
    do8. do16 si, la, |
    fa4 fa16 re |
    mi8 fad8. sold16 |
    la8 la la |
    re mi8. mi16 |
    la4 r8 |
    R4.*3 |
    r8 r re |
    la dod' la |
    re' dod' re' |
    la la la16 la |
    dod8 dod8. re16 |
    la,4 r8 |
    R4.*2 |
    r8 r re |
    fad fad re |
    sol sol mi |
    la la fad16 fad |
    si8 si8. sol16 |
    re'4.~ |
    re'4 re'8 |
    re'8. re'16 re' re |
    sol4 sol16 mi |
    si4 mi'16 mi |
    la8 si8. dod'16 |
    re'4.~ |
    re'8 fad re |
    re re8. re16 |
    la4.~ |
    la~ |
    la8 la la |
    la dod'8. mi'16 |
    sol4.~ |
    sol8 fad re |
    la dod'8. mi'16 |
    fad4.~ |
    fad8. fad16 re8 |
    si dod'8. re'16 |
    la8( la,4) |
    re4. |
  }
>>
