\clef "basse" re4 re'8 |
dod'4 la8 |
re'4 re8 |
la4 sol8 |
fad fad16 mi re8 |
la4. |
la,8 dod la, |
re4. |
re8 fad re |
sol4 r8 |
fad4 r8 |
dod8 re16 mi fad8 |
re mi mi, |
la,4. |
la4. |
dod4 la,8 |
dod4 la,8 |
re re, re |
fad4 re8 |
sol sol, sol |
sold4 mi8 |
la la, la |
dod'4 la8 |
re' fad re |
la dod la, |
re fad re |
sol4 r8 |
la4 r8 |
si4 r8 |
dod'4 r8 |
re' re dod |
re sol4 |
fad8 mi la |
re sol, la, |
re, re16 mi fad8 |
sol,8 sol16 la si8 |
fad,16 sol, la,4 |
re,4. |
