\clef "dessus" re''8 fad'' re'' |
la'' mi''16 fad'' sol''8 |
fad'' mi'' re'' |
dod'' re'' mi'' |
la' re''16 mi'' fad''8 |
mi'' dod'' la' |
la'' mi''16 fad'' sol''8 |
fad'' re'' re' |
re''' fad''16 sol'' la''8 |
si'' mi''16 fad'' sol''8 |
la'' re''16 mi'' fad''8 |
sol'' fad''16 sol'' la''8 |
la'' sold''4 |
la''4. |
mi''8 dod'' la' |
mi'' la'' mi'' |
la'' mi''16 fad'' sol''8 |
fad'' re''' la'' |
re''' la''16 si'' do'''8 |
si'' re'' si' |
mi'' si'16 dod'' re''8 |
dod'' la'' mi'' |
la'' mi''16 fad'' sol''8 |
fad'' fad''16 sol'' la''8 |
mi'' mi''16 fad'' sol''8 |
fad''8 re'''16 dod''' si'' la'' |
si'' la'' sol'' fad'' mi'' re'' |
dod''' si'' la'' sol'' fad'' mi'' |
re''' dod''' si'' la'' sol'' fad'' |
mi''' re''' dod''' si'' la'' sol'' |
fad'' la'' re'''8 la'' |
fad''16 re'' si''8 sol'' |
la''16 fad'' sol''8 mi'' |
fad''16 re'' mi''8 dod'' |
re''4.~ |
re''~ |
re''8. re''16 dod''8 |
re''4. |
