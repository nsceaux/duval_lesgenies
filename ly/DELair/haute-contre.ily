\clef "haute-contre" fad'8 la' fad' |
mi' sol'16 fad' mi'8 |
la' la' la' |
la' si' dod'' |
re'' re''16 dod'' re''8 |
dod'' la' mi' |
mi' dod'16 re' mi'8 |
re'4. |
fad'8 la'16 sol' fad'8 |
sol' sol'16 fad' mi'8 |
fad' fad'16 mi' re'8 |
mi'8 la'16 sol' fad'8 |
fad' mi'4 |
dod'4.\trill |
dod''8 la' la' |
la'4 la'8 |
la' sol'16 fad' mi'8 |
re' fad' fad' |
la'4 fad'8 |
sol'4 sol'8 |
si' si'16 la' sold'8 |
la' dod'' la' |
la' sol'16 fad' mi'8 |
re' la'16 sol' fad'8 |
mi' sol'16 fad' mi'8 |
re' la' si'16 dod'' |
re'' dod'' si' la' sol' fad' |
mi'' re'' dod'' si' la' sol' |
fad'' mi'' re'' dod'' si' la' |
sol'' fad'' mi'' re'' dod'' si' |
la'4 la'8 |
la' sol' re'' |
re''4 dod''8 |
re''4 la'8 |
la' fad'16 sol' la'8 |
si' si'16 la' sol'8 |
la'16 sol' fad'8 mi' |
re'4. |
