\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { Violons }
    } << \global \includeNotes "dessus" >>
    \ifFull <<
      \new Staff \with {
        instrumentName = \markup\center-column { [Hautes-contre] }
      } << \global \includeNotes "haute-contre" >>
      \new Staff \with {
        instrumentName = "[Tailles]"
      } << \global \includeNotes "taille" >>
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4.*11\break s4.*12\break s4.*9\break s4.*6
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
