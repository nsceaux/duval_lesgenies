\clef "taille" la4 la'8 |
la'4 dod'8 |
re' mi' fad' |
mi' la la |
la4 la8 |
la mi'16 re' dod' si |
dod'8 la dod' |
re'4. |
la4 re'8 |
re' mi'16 re' dod'8 |
re'4 la8 |
la4 la8 |
si si4 |
la4. |
mi'4 mi'8 |
mi'4 dod'8 |
mi' mi'16 re' dod'8 |
re' la re' |
re'4 re'8 |
re' si re' |
si mi' si |
la mi' dod' |
mi' mi'16 re' dod'8 |
re'4 re'8 |
dod' la dod' |
re' re'16 mi' fad'8 |
re''4 r8 |
dod''4 r8 |
si'4 r8 |
la'4 r8 |
la'8 fad' mi' |
fad' re' si |
la si la |
la sol mi |
fad4 fad'8 |
mi'4 mi'8 |
re' la4 |
fad4.\trill |
