\clef "basse" sol,4 sol sol sol |
sol2 fad4 re |
sol sol fad fad |
mi mi la la |
re re mi si, |
do2 re4 re, |
sol, sol sol sol |
sol fa fa fa |
fa2 mi |
re4 mi8 fa sol4 sol, |
do do' do' do' |
do'2 si4 sol |
do' mi' do' la |
fa4. re8 sol4. re8 |
mi4. do8 fa4. do8 |
re4. si,8 mi4. si,8 |
do4 la, re re, |
sol, la, si, do |
re2 sol4. re8 |
mi4 do la4. mi8 |
fad4 re mi si, |
do la, re re, |
sol,1 |
