\clef "dessus"
<<
  \tag #'dessus1 {
    r4 si' si' si' |
    la' si'8 do'' re''4 do'' |
    si'8 re'' do'' si' la' do'' si' la' |
    sol' la' sol' fad' mi' la' sol' la' |
    fad'4 re' sol' sol' |
    sol'2 fad'4.\trill sol'8 |
    sol'2 re'' |
    do''1 |
    si'2 do''~ |
    do'' si' |
    do''4 mi'' mi'' mi'' |
    re'' mi''8 fa'' sol''4 fa'' |
    mi''2 mi''~ |
    mi'' re''~ |
    re'' do''~ |
    do'' si' |
    la' la'4.\trill sol'8 |
    sol'2 sol'' |
    fad'' fa''4. fa''8 |
    fa''4 mi'' mi''4. mi''8 |
    \afterGrace mi''4(\trill re''8) re''4 sol''2~ |
    sol'' fad''\trill |
    sol''1 |
  }
  \tag #'dessus2 {
    r4 re'' re'' re'' |
    do'' re''8 mi'' la'4 la' |
    re' re' re''2~ |
    re'' do''~ |
    do'' si'~ |
    si' la'4.\trill sol'8 |
    sol'2 si' |
    la' re''~ |
    re'' sol'' |
    fa''4 mi'' re'' sol'' |
    mi'' sol'' sol'' sol'' |
    fa'' sol''8 la'' re''4 re'' |
    sol'2 do''~ |
    do'' si'~ |
    si' la'~ |
    la' sol'~ |
    sol' fad'4.\trill sol'8 |
    sol'4 re'' re'' do''8 si' |
    la'4 re' re''2~ |
    re''4 sol' do''2~ |
    do'' si' |
    la'8 si' do'' si' la'4.\trill sol'8 |
    sol'1 |
  }
>>
