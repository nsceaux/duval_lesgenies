\score {
  \new StaffGroup <<
    \new GrandStaff \with {
      instrumentName = \markup\center-column { [Violons et Hautbois] }
    } <<
      \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
      \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses, Bassons et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*9\pageBreak
        s1*10\break s1*4
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
