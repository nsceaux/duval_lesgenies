\clef "basse" r4 |
sol2. fad4 |
sol2. |
sol2 fa4 |
mi2 dod re4 sib |
la8 sol fa sol la4 la, |
re,2 r4 |
r sib2 |
sib,4. do8 re mib |
fa2. | \allowPageTurn
r4 r mib8 re |
do4 la, sib, |
fa,2. |
sib, | \allowPageTurn
r4 r8 sib la sol |
fad2 sol4 |
re2 do4 |
sib,2 do8 re |
mib2 do8 la, |
fad, sol, re4 re, |
sol,2 r4 |
<>^"[Basses et B.C.]"
r r r8 sol |
fad4 \appoggiatura mi8 re4 r8 sol |
do4 re re, |
sol8 la si la sol fad |
mi fad sol4 mi |
la4. fad8 si4 |
sol la la, |
re2. |
re'4 fad sol |
re2.~ |
re~ |
re~ |
re4 do4. si,8 |
la,4 la8 sol la si |
do'2. |
la4 si4. dod'8 |
re'2 do'8 si |
do'4 re' re |
mi8 do re4 re, |
sol,2. |
