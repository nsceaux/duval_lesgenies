\clef "dessus" r4 R1 R2.*2 R1. R1 R2.*14 | \allowPageTurn
r4 r <>^"[Violons]" r8 re'' |
re'' si' sol' si' re'' sol' |
la' re' fad' la' re' re'' |
do'' mi'' re'' do'' si' la' |
si'4\trill sol' r8 re'' |
sol'' la'' si'' la'' sol'' fad'' |
mi'' mi'' la''4. la''8 |
la''( sol''16\trill fad'') mi''4.\trill re''8 |
re''8 re' la' do'' si' sol' |
la'4 re''4. sol''8 |
fad''4. sol''8 la'' si'' |
do''' si'' la'' fad'' sol''4 |
fad''8\trill re'' do'' si' la' sol' |
fad'4 fad'4. sold'8 |
la' si' do'' re'' do'' si' |
la'4. la''8 sol'' fad'' |
mi'' re'' mi'' dod'' re'' mi'' |
fad'' sol'' fad'' sol'' la'' si'' |
la'' si''16 do''' la''4.\trill sol''8 |
sol''16 fad'' mi'' fad'' fad''4.\trill sol''8 |
sol''2. |
