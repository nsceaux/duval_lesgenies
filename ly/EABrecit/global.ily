\key re \minor
\once\omit Staff.TimeSignature
\time 2/2 \partial 4 s4
\digitTime\time 2/2 \midiTempo#132 s1
\digitTime\time 3/4 s2.*2
\time 3/2 s1.
\digitTime\time 2/2 s1
\digitTime\time 3/4 s2.*14 \bar "||"
\key sol \major \beginMarkSmall "Majeur"
s2.*21 \bar "|."
