Le ciel a fi -- xé mon em -- pi -- re
en -- tre les cieux & les mers,
je règne en sou -- ve -- rain dans l’es -- pa -- ce des airs,
mais l’u -- ni -- que bien où j’as -- pi -- re
c’est de char -- mer l’ob -- jet dont je por -- te les fers,
mais l’u -- ni -- que bien où j’as -- pi -- re
c’est de char -- mer l’ob -- jet dont je por -- te les fers.
Ces lieux sont or -- nés pour lui plai -- re
A -- mour, A -- mour, se -- con -- de mes dé -- sirs ;
si cet ob -- jet char -- mant de -- mande un cœur sin -- cè -- re,
fi -- xe mes vœux, 
fi -- xe mes vœux, __ fais du -- rer mes plai -- sirs.