\piecePartSpecs
#`((dessus #:score-template "score-voix")
   (dessus2 #:score-template "score-voix")
   (basse #:score-template "score-basse-continue-voix"
          #:instrument "[B.C.]")
   
   (silence #:on-the-fly-markup , #{
\markup\fontsize#-2 \column {
  \livretPers Un Sylphe
  \livretVerse#8 { Le sort a fixé mon empire }
  \livretVerse#7 { Entre les cieux & les mers, }
  \livretVerse#12 { Je règne en souverain dans l’espace des airs, }
  \livretVerse#8 { Mais l’unique bien où j’aspire }
  \livretVerse#12 { C’est de charmer l’objet dont je porte les fers. }
  \livretVerse#8 { Ces lieux sont ornés pour lui plaire }
  \livretVerse#8 { Amour, seconde mes désirs ; }
  \livretVerse#12 { Si cet objet charmant demande un cœur sincère, }
  \livretVerse#10 { Fixe mes vœux, fais durer mes plaisirs. }
}#}))
