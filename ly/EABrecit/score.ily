\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiriFirst } << \global \includeNotes "dessus" >>
    \new Staff \with { instrumentName = \markup\character Un Sylphe } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = \markup\center-column { [B.C.] } } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion { s4 s1 s2.*2 s1. s1 s2.*14\break }
      \origLayout {
        s4 s1 s2.\pageBreak
        s2. s1. s1 s2.\break s2.*6\break s2.*7\pageBreak
        s2.*8\break s2.*7\pageBreak
        s2.*6
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
