\clef "vhaute-contre" r8 re' |
sol'4 re'8 mib' do'4\trill la8 re' |
sib2\trill \appoggiatura la8 sol4 |
sol4 sib re' |
sol' fa'8 sol' la'4 r8 sol' fa'4 mi'8 re'16 mi' |
dod'4 re'8 sol la4 la8 la |
re'2. |
sib4. do'8 re' mib' |
fa'2 fa'8 sib |
la2\trill \appoggiatura sol8 fa4 |
do' mib'8 re' do' sib |
mib'2 re'8 mib' |
fa'2 fa'8 fa |
sib2. |
re'4. re'8 do' sib |
la2 sib8 \appoggiatura la sol |
fad2\trill fad4 |
sol4 sib8 sol mib' re' |
sol'2 mib'8 do' |
re'2 do'8 re' |
sol2 r4 |
r r r8 re' |
re'2 si8 sol |
la4 si8([ la]) sol([ fad]) |
sol4 sol r8 si |
mi'2 r8 si |
dod'4 r8 la re'4~ |
re'16 dod'[ si dod'] dod'4.\trill re'8 |
re'2. |
r8 fad la do' si sol |
la4. si8 do'4 |
la re' sol |
la2 fad4\trill |
re' la4. si8 |
\appoggiatura si8 do'2. |
mi'4 fad'4. sol'8 |
la'2.~ |
la'2 fad'8 re' |
mi'16[ fad' mi' fad'] fad'4.\trill sol'8 |
sol'2. |
R2. |
