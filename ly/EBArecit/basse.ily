\clef "basse" R2.*2 |
r8 re fad la fad re |
si la sol fad mi fad16 re |
dod2 la,4 |
re re' do' |
si2 r8 sol |
do'2 r8 la |
re'2 r8 re |
sol fad sol la si sol |
la sol la si dod' la |
re' sol la4 la, |
re8 re fad la fad re |
sol, sol si re' si sol |
mi' re' do' si la si16 sol |
fad2. |
sol8 fad sol la si sol |
la4 la, la8 sol |
fad mi re mi fad re |
sol sol, si, re si, sol, |
do si, do re mi do |
re4 re' do' |
si8 la sol la si sol |
do' la re'4 re |
\ifComplet {
  << { s4 <>^"[B.C.]" } sol2.~ >> | \allowPageTurn
  sol2. fad4 |
  sol2 sol fad8 sol la la, |
  re2.~ |
  re | \allowPageTurn
  re'4 do'8 si la sol |
  fad4 sol8 la si sol |
  do'2 si4 |
  mi' do' re' |
  sol2. | \allowPageTurn
  sol8 fad sol sib la sol |
  fad2 re4 |
  sol8 re sol, fad sol la |
  sib la sol la sib do' |
  re'4. sol8 fa mib |
  re4 r8 fa mib re |
  do4 fa fa, |
  sib,2 sib,4 |
  fa4 mib8 re do sib, |
  la, sib, fa4 fa, |
  sib,8 re' do' sib la sol |
  fad mi re mi fad re |
  sol la sib do' sib la |
  sib sol do4 mib |
  re8 mib re do sib, la, |
  sol, sol sol la sib sol |
  do' sib la4. sol8 |
  fad4 sol4. la8 |
  sib8 re' do' sib la sol |
  fad2 re4 |
  sol, la, sib, |
  do re re, |
  sol,8 sol sol la sib sol |
  do' sib do' re' mib' do' |
  re'4 do'8 sib la sol |
  fad4 sol sol, |
  re2 sib4 |
  la8 fa sib, do re mib |
  fa4 fa fa |
  fa4. mib8 re do |
  re mib fa4 fa, |
  sib,2 sib8 la |
  sol4 fa mib |
  re2 re'4 |
  do' sib la |
  sib la8 sol fad4 |
  sol mib'8 re' do' la |
  sib do' re'4 re |
  sol4 <>^"[Basses et B.C.]" re8 sol,8. sol16 la8 |
  si8. do'16 si8 la4 sol8 |
  fad8. mi16 re8 sol8. si16 la8 |
  sol4 fad8 mi4. |
  re8. la,16 fad,8 re,8. re'16 do'8 |
  si8. do'16 si8 la4 sol8 |
  fad8. mi16 re8 sol4 sol8 |
  fad4 sol8 la4 la,8 |
  re8. mi16 fad8 sol si, sol, |
  do8. do'16 si8 la8. sol16 fa8 |
  mi4 re8 do8. la,16 si,8 |
  do8. si,16 la,8 re8. mi16 fa8 |
  mi8. mi'16 re'8 do'8. si16 la8 |
  re8. mi16 fa8 re mi mi, |
  la,8. la16 sol8 fad4 sol8 |
  do4. dod |
  re8. la16 si do' re'8 fad re |
  sol4 la8 si la sol |
  re16 re' do' si la sol fad4 sol8 |
  do4. dod |
  re8. la16 si do' re'8 si sol |
  do4 la,8 re4 do8 |
  si,8. si16 la sol fad4 fa8 |
  mi4 si,8 do re re, |
  sol,8. sol16 la8  \allowPageTurn sib4 la8 |
  sol sib sol do'4 la8 |
  re'8. fad16 la8 re8. sib16 sol8 |
  do'4 re'8 mib'4 mib'8 |
  re'8 re do' sib4 la8 |
  sol16 la sib8 do' re' la do' |
  sib8. la16 sol fa mib4. |
  re8. re'16 do'8 sib la fa |
  sib4 mib8 sib la sib |
  fa fa, fa mib mib, mib |
  re sib, sib si sol si |
  do' sol mib do8. do'16 re'8 |
  \clef "tenor" mib'8. fa'16 sol'8 mib'8. re'16 do'8 |
  \clef "bass" fa'8. fa16 la fa sib4 sib8 |
  la4 sib8 do'4 do8 |
  fa fa, mib re sib, re |
  mib8. fa16 sol8 mib8. re16 do8 |
  fa8. fa'16 mib'8 re'8. do'16 sib8 |
  mib'4. mib' |
  re'8. do'16 sib8 la4 lab8 |
  sol4 re8 mib fa fa, |
  sib,8. sib16 la8 sol sol, sol |
  re4 sib,8 mib do re |
  sol,2 \allowPageTurn
}
\ifConcert { sol2 }
<>^"B.C." sol4 |
sol do' dod' |
re'8. do'16 si8 sol la re16 sol, la,4 |
re re'8 do' si la |
sol2 la8 si |
do' si la2 |
sol4 sol, sol |
fad4. mi8 re8 fa |
mi do re4 re, |
sol,2. | \allowPageTurn
sol4 sol8 re'4 do'8 |
si4 sol8 re8. mi16 fad8 |
sol8. la16 si la sol fad mi fad sol mi |
la4 re'8 si16 sol la8 la, |
re re' fad sol si, mi |
do re mi re16 re' do' si la sol |
fad8 re fad sol16 re si, re sol,8 |
sol8 si sol re'4 re8 |
sol16 si la sol fad mi re8 fad re |
sol16 la si do' re' sol re8 re, re |
sol si sol do8. re16 mi do |
re8 re' do' si8. la16 sol fad |
mi8 mi4 re8. mi16 fad re |
sol8 do' la re'16 re si, do re si, |
mi re do re mi do re4 re8 |
sol,8 si16 do' si la sol8 la la, |
re16 la, fad, la, re,8 re8 re' do' |
si16 la sol fad mi re do re mi re do si, |
la,4 la,8 re16 la re' mi' re' do' |
si la sol fad mi re sol mi la8 la, |
re16 mi fad mi re do si,8 re si, |
mi sol mi do mi do |
la, do la, re fad re |
sol si sol do'8. si16 la8 |
re fad re re, fad, re, |
re4 re'8 dod'8. si16 la8 |
re4 re'8 re' re' la |
re' re' re la, la, la |
dod'16 si la si dod' la re'8 do' si |
mi16 re do re mi do re8 re,4 | \allowPageTurn
sol,4. sol8 do' la |
re' la fad4 re4 |
sol8 mi la fad si sol la la, |
re4 fad re |
sol8 la si do' si la |
sol4 sol, sol |
fad4. mi8 re do |
si,4 la, sol, |
do re re, |
\ifComplet { sol,2. | }
\ifConcert { sol,4. la,8 sol, fad, | \once\set Staff.whichBar = "|" }
