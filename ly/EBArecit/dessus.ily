\clef "dessus"
<<
  \tag #'dessus1 {
    <>^"Violons" r8 sol' si' re'' si' sol' |
    mi'' re'' do'' si' la' si'16 sol' |
    fad'4 fad'4. la'8 |
    re'2 r4 |
    r8 la' dod'' mi'' dod'' la' |
    fad''16 re'' mi'' fad'' fad''4.(\trill mi''16 fad'') |
    sol''4 re'' r |
    <>^"Flûtes" r8 sol'' mi'' sol'' do'' mi'' |
    <>^"Violons" la'8 re'' fad'' la'' fad'' re'' |
    si'' la'' sol'' fad'' mi'' fad''16 re'' |
    dod''8 re'' mi'' fad'' sol'' la'' |
    fad'' sol'' mi''4.\trill re''8 |
    re''4 <>^"Flûtes" la' do'' |
    si' sol' sol'' |
    sol'' sol''4.(\trill fad''16 sol'') |
    la''2 la''4 |
    <>^"Violons" re''8 re''' si'' re''' sol'' si'' |
    mi'' re'' mi'' fad'' sol'' mi'' |
    la''4 <>^"Flûtes" la' do'' |
    do'' si' r8 <>^"Violons" re'' |
    re''8 mi''16 re'' do''8 si' la' si'16 sol' |
    fad'8 mi' fad' sol' la' sol'16 fad' |
    sol'4 re' r8 si' |
    do'' re''16 mi'' la'4.\trill sol'8 |
    sol'2
  }
  \tag #'dessus2 {
    <>^"Flûtes" sol'2. |
    r8 sol'' mi'' sol'' do'' mi'' |
    la'2 la'4 |
    r8 re'' si' re'' sol' si' |
    mi' sol'' mi'' sol'' mi'' dod'' |
    la'4 la'4.(\trill sol'16 la') |
    si'8 sol' si' re'' si' sol' |
    mi'' re'' do'' si' la' si'16 sol' |
    fad'4 r8 <>^\markup\whiteout "Flûtes" la'8 re'' re'' |
    re'' re''' si'' re''' sol'' si'' |
    mi'' re'' dod'' re'' mi'' dod'' |
    re'' mi'' dod''4. re''8 |
    re''4 re'' re'' |
    re'' si' re'' |
    do''8 re'' mi'' re'' do'' si' |
    la' <>^"Violons" re'' fad'' la'' fad'' re'' |
    si'' la'' sol'' fad'' mi'' fad''16 re'' |
    dod''8 si' dod'' re'' mi'' dod'' |
    re''4 <>^"Flûtes" fad' la' |
    la' re' r8 si' |
    si' do''16 re'' mi''8 re'' do'' re''16 si' |
    la'8 sol' la' si' do'' si'16 la' |
    re''2~ re''8 mi''16 fa'' |
    mi''8 fad''16 sol'' fad''4.\trill sol''8 |
    sol''2 \startHaraKiri
  }
>> r4 | \allowPageTurn
\ifComplet {
  R1\allowPageTurn R1.\allowPageTurn R2.*7\allowPageTurn
  \ru#38 { R2. \allowPageTurn } |
  r2*3/4 r |
  r r8 r16 fad'' sol''8 |
  la''16 sol'' fad'' sol'' la''8 re''8. si'16 do''8 |
  re''16 do'' si' dod'' re''8 re''4 dod''8 |
  re''4. r8 r16 sol' la'8 |
  si'16 la' sol' la' si'8 fad'8. fad''16 sol''8 |
  la''16 sol'' fad'' sol'' la''8 re''8. re''16 mi''8 |
  fad''8. la''16 sol'' fad'' mi''8. fad''16 sol''8 |
  fad''8. mi''16 re'' do'' si' sol' re' sol' si' re'' |
  sol' fad' mi' fad' sold' mi' la' mi' la' do'' re'' fa'' |
  si' mi'' sold'' si'' sold'' mi'' la'' la' do'' mi'' sold' si' |
  mi' la' do'' mi'' la'' mi'' re'' do'' si' do'' re'' si' |
  sold' mi' sold' si' sold' mi' la' si' do'' re'' mi'' la'' |
  si'8. do''16 la'8 la'4 sold'8 |
  la'4. r8 r16 la' si'8 |
  do'' mi'' la' mi'4 mi''8 |
  la'4 la'8 fad'4\trill r8 |
  r2*3/4 r8 r16 re'' sol''8 |
  fad''8. re''16 do'' si' la'8. la'16 si'8 |
  do''8 mi'' la' mi' mi''4 |
  la'4 fad'8\trill r r16 re' sol'8 |
  sol'8 mi' la' fad'8. sol'16 la'8 |
  si'8. re''16 do'' si' la'8. la'16 re''8 |
  sol'4 re''8 do''8. si'16 \tuplet 3/2 { la'([ si' do'']) } |
  si'8\trill sol' r r r16 sol'' fad''8 |
  sol''8. re''16 sib' re'' sol'4 do''8 |
  la'4.\trill fad'8. re''16 re''8 |
  do''4 sib'8 la'8. sol'16 la'8 |
  fad'4.\trill re'8 sol'16 fad'! sol' la' |
  sib' do'' re''8 do''16 sib' la'4 la'8 |
  sib'8. sib'16 do'' re'' sol' la' la'8.\trill sol'16 |
  fad'8. la'16 sib' do'' re''8 do''16 re'' mib'' do'' |
  re''4 mib''8 re'' do'' sib' |
  la'16 sol' fa' sol' la' sib' do'' sib' la' sib' do'' la' |
  sib' fa' sol' la' sib' do'' re'' do'' re'' mib'' fa'' re'' |
  mib''8. sol''16 la'' si'' do'''8. do'''16 si''8 |
  do'''4 do''8 mib''8. fa''16 sol''8 |
  do''16 sib' do'' re'' mib'' fa'' re''8. fa''16 sol''8 |
  la''8. do'''16 sib'' la'' sol'' fa'' sol'' la'' sib'' do''' |
  la'' sol'' fa'' sol'' la'' fa'' sib''8. la''16 sol'' fa'' |
  sol'' fa'' mib'' re'' do'' sib' mib''8. fa''16 sol''8 |
  do''16 sib' do'' re'' mib'' do'' fa'' fa' sol' la' sib' la' |
  sol' sol' la' sib' do'' sib' la' sol' la' sib' do'' la' |
  sib' la' sib' do'' re'' mib'' do''4 fa''8 |
  sib'4 fa''8 mib''8. re''16 \tuplet 3/2 { do''([ re'' mib'']) } |
  re''8. fa'16 sol' la' sib'8. sib'16 do''8 |
  la'8. la'16 sib'8 sol'8. sol'16 fad'8 |
  sol'2 \allowPageTurn r4 |
}
R2. R1 R2.*7 R2.*30 R2.*2 R1 R2.*7 |
