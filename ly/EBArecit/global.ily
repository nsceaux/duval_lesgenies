\beginMarkSmall "Prélude gai sans vitesse"
\key sol \major
\digitTime\time 3/4 \midiTempo#120 s2.*24
\ifComplet {
  \digitTime\time 3/4 \midiTempo#80 s2.
  \digitTime\time 2/2 \midiTempo#160 s1
  \time 3/2 s1.
  \digitTime\time 3/4 \midiTempo#120 s2.*7 \bar "||"
  \key re \minor s2.*38 \bar "||"
  \key sol \major \time 6/8 \midiTempo#80 s2.*24 s4. \bar "||"
  \key re \minor s4. s2.*22
  \digitTime\time 3/4 s2 \bar "||"
  \key sol \major \midiTempo#80 s4 s2.
}
\ifConcert { \digitTime\time 3/4 s2 \bar "||" s4 s2. }
\time 4/4 s1
\digitTime\time 3/4 \midiTempo#120 s2.*7
\time 6/8 \midiTempo#80 s2.*30
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\digitTime\time 3/4 \midiTempo#120 s2.*7 \bar "|."
