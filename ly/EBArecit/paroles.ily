\tag #'(voix1 basse) {
  \ifComplet {
    Ne dis -- si -- mu -- lez point, vo -- tre cœur est vo -- la -- ge,
    vous ne vi -- vez plus sous ma loi.
    
    Lors -- que vous me man -- quez de foi,
    vous of -- fen -- se -- riez- vous quand mon cœur se dé -- ga -- ge ?
    
    Non, je ne croy -- ais pas que dans le mê -- me jour
    qu’un ai -- ma -- ble nœud nous en -- ga -- ge,
    qu’en m’ap -- pre -- nant à con -- naî -- tre l’a -- mour,
    vous m’ap -- pren -- driez à de -- ve -- nir vo -- la -- ge ;
    non, je ne croy -- ais pas que dans le mê -- me jour
    qu’un ai -- ma -- ble nœud nous en -- ga -- ge,
    qu’en m’ap -- pre -- nant à con -- naî -- tre l’a -- mour,
    vous m’ap -- pren -- driez à de -- ve -- nir __ vo -- la -- ge.
    
    Vous de -- vez ren -- dre grâce à ma lé -- gè -- re -- té,
    est- il un plus grand a -- van -- ta -- ge ?
    Des dou -- ceurs de l’a -- mour vous sa -- vez faire u -- sa -- ge,
    en con -- ser -- vant la li -- ber -- té.
    Des dou -- ceurs de l’a -- mour vous sa -- vez faire u -- sa -- ge,
    en con -- ser -- vant la li -- ber -- té.
    
    L’a -- mour bril -- le de moins de char -- mes,
    vous sa -- vez tou -- cher tous les cœur ;
    sous vos lois il n’est point d’a -- lar -- mes,
    on ne goû -- te que des dou -- ceurs.
    Vous dé -- sar -- mez le plus re -- bè -- le,
    il est con -- traint à s’en -- flam -- mer,
    il est con -- traint à s’en -- flam -- mer,
    si vous n’é -- tiez point in -- fi -- dè -- le
    on vou -- drait tou -- jours vous ai -- mer.
    Si vous n’é -- tiez point in -- fi -- dè -- le
    on vou -- drait tou -- jours vous ai -- mer,
    on vou -- drait tou -- jours vous ai -- mer.
    
    Un a -- mant tel que vous en -- chan -- te,
    vous ai -- mez sans ê -- tre ja -- loux :
    vous n’e -- xi -- gez point d’une a -- man -- te,
    de ne sou -- pi -- rer que pour vous.
    Vous ê -- tes dans vo -- tre ten -- dres -- se
    com -- plai -- sant, sin -- cère et dis -- cret ;
    si mon cœur a de la fai -- bles -- se,
    vous sa -- vez gar -- der le se -- cret.
    Si mon cœur a de la fai -- bles -- se,
    vous sa -- vez gar -- der le se -- cret,
    vous sa -- vez gar -- der le se -- cret,
    vous sa -- vez gar -- der le se -- cret.
  }
  Je sens que mon a -- mour au -- rait é -- té fi -- dè -- le,
  si le votre eut é -- té cons -- tant.
  
  Sans le plai -- sir d’u -- ne flam -- me nou -- vel -- le,
  j’ai -- me -- rais en -- cor mon a -- mant.
}
\tag #'(voix1 basse) {
  Lan -- ce tes traits,
  lan -- ce tes traits, rem -- por -- te la vic -- toi -- re,
  a -- mour, tri -- om -- phe de mon cœur ;
  lan -- ce tes traits, lan -- ce tes traits,
  lan -- ce, lan -- ce tes traits, rem -- por -- te la vic -- toi -- re,
  tri -- om -- phe de mon cœur ;
  non, non, non, non, tu n’as ja -- mais tant de gloi -- re
  que dans une in -- cons -- tante ar -- deur,
  lan -- ce, lan -- ce tes traits,
  lan -- ce tes traits,
  lan -- ce, lan -- ce tes traits, a -- mour, rem -- por -- te la vic -- toi -- re,
  rem -- por -- te la vic -- toi -- re,
  a -- mour, tri -- om -- phe, tri -- om -- phe de mon cœur.
}
\tag #'voix2 {
  Lan -- ce tes traits, rem -- por -- te, rem -- por -- te la vic -- toi -- re,
  a -- mour, tri -- om -- phe de mon cœur ;
  lan -- ce, lan -- ce tes traits,
  lan -- ce, lan -- ce tes traits, rem -- por -- te la vic -- toi -- re,
  tri -- om -- phe, tri -- om -- phe de mon cœur ;
  non, non, non, non, tu n’as ja -- mais,
  tu n’as ja -- mais tant de gloi -- re,
  non, tu n’as ja -- mais tant de gloi -- re,
  que dans une in -- cons -- tante ar -- deur,
  lan -- ce, lan -- ce tes traits,
  a -- mour, rem -- por -- te, rem -- por -- te la vic -- toi -- re,
  rem -- por -- te la vic -- toi -- re,
  a -- mour, tri -- om -- phe de mon cœur.
}
\tag #'(voix1 basse) {
  Je vois ma nou -- vel -- le con -- quê -- te.
}
\tag #'(voix2 basse) {
  La mien -- ne doit se rendre au mi -- lieu de la fê -- te.
}
\tag #'(voix1 basse) {
  Al -- lons pré -- pa -- rer des jeux,
  al -- lons pré -- pa -- rer des jeux
  di -- gnes de nos soins a -- mou -- reux.
}
\tag #'voix2 {
  Al -- lons pré -- pa -- rer des jeux __
  di -- gnes de nos soins a -- mou -- reux.
}
