\piecePartSpecs
#`((dessus #:score-template "score-2dessus-voix"
           #:instrument ,#{\markup\center-column { Violons Flûtes }#})
   (dessus2 #:score-template "score-2dessus-voix"
            #:instrument ,#{\markup\center-column { Violons Flûtes }#})
   (basse #:score-template "score-basse-continue-voix"
          #:instrument ,#{\markup\center-column { [Basses et B.C.] }#})
   
   (silence #:on-the-fly-markup , #{
\markup\fontsize#-2 \fill-line {
  \column {
    \livretPers Le Sylphe
    \livretVerse#12 { Ne dissimulez point, votre cœur est volage, }
    \livretVerse#8 { Vous ne vivez plus sous ma loi. }
    \livretPers La Sylphide
    \livretVerse#8 { Lorsque vous me manquez de foi, }
    \livretVerse#12 { Vous offenseriez-vous quand mon cœur se dégage ? }
    \livretPers Le Sylphe
    \livretVerse#12 { Non, je ne croyais pas que dans le même jour }
    \livretVerse#8 { Qu’un aimable nœud nous engage, }
    \livretVerse#10 { Qu’en m’apprenant à connaître l’amour, }
    \livretVerse#10 { Vous m’apprendriez à devenir volage. }
    \livretPers La Sylphide
    \livretVerse#12 { Vous devez rendre grâce à ma légèreté, }
    \livretVerse#8 { Est-il un plus grand avantage ? }
    \livretVerse#12 { Des douceurs de l’amour vous savez faire usage }
    \livretVerse#8 { En conservant la liberté. }
    \livretPers Le Sylphe
    \livretVerse#8 { L’amour brille de moins de charmes, }
    \livretVerse#8 { Vous savez toucher tous les cœur ; }
    \livretVerse#8 { Sous vos lois il n’est point d’alarmes, }
    \livretVerse#8 { On ne goûte que des douceurs. }
    \livretVerse#8 { Vous désarmez le plus rebèle, }
    \livretVerse#8 { Il est contraint à s’enflammer, }
    \livretVerse#8 { Si vous n’étiez point infidèle }
    \livretVerse#8 { On voudrait toujours vous aimer. }
  }
  \column {
    \livretPers La Sylphide
    \livretVerse#8 { Un amant tel que vous enchante, }
    \livretVerse#8 { Vous aimez sans être jaloux : }
    \livretVerse#8 { Vous n’exigez point d’une amante, }
    \livretVerse#8 { De ne soupirer que pour vous. }
    \livretVerse#8 { Vous êtes dans votre tendresse }
    \livretVerse#8 { Complaisant, sincère & discret ; }
    \livretVerse#8 { Si mon cœur a de la faiblesse, }
    \livretVerse#8 { Vous savez garder le secret. }
    \livretPers Le Sylphe
    \livretVerse#12 { Je sens que mon amour aurait été fidèle, }
    \livretVerse#8 { Si le votre eut été constant. }
    \livretPers La Sylphide
    \livretVerse#10 { Sans le plaisir d’une flamme nouvelle, }
    \livretVerse#8 { J’aimerais encor mon amant. }
    \livretPers Ensemble
    \livretVerse#10 { Lance tes traits, remporte la victoire, }
    \livretVerse#8 { Amour, triomphe de mon cœur ; }
    \livretVerse#8 { Non, tu n’as jamais tant de gloire }
    \livretVerse#8 { Que dans une inconstante ardeur. }
    \livretPers La Sylphide
    \livretVerse#8 { Je vois ma nouvelle conquête. }
    \livretPers Le Sylphe
    \livretVerse#12 { La mienne doit se rendre au milieu de la fête. }
    \livretPers Ensemble
    \livretVerse#7 { Allons préparer des jeux }
    \livretVerse#8 { Dignes de nos soins amoureux. }
  }
}#}))
