\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new GrandStaff \with {
      instrumentName = \markup\center-column { Violons Flûtes }
    } <<
      \new Staff << \global \keepWithTag#'dessus1 \includeNotes "dessus" >>
      \new Staff << \global \keepWithTag#'dessus2 \includeNotes "dessus" >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag#'voix1 \includeNotes "voix"
    >> \keepWithTag#'voix1 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag#'voix2 \includeNotes "voix"
    >> \keepWithTag#'voix2 \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \ifConcert { s2.*24 s2 \break s4 s2. s1 s2.*7\break }
      \origLayout {
        s2.*8\pageBreak
        s2.*9\break s2.*7\pageBreak
        s2. s1 s1.\break s2.*5\break s2.*4 s4. \bar "" \pageBreak
        s4. s2.*5\break s2.*5\break s2.*5\pageBreak
        s2.*5\break s2.*5\break s2.*5\pageBreak
        s2.*5 s4. \bar "" \break s4. s2.*3 s4. \bar "" \break s4. s2.*4\pageBreak
        s2.*5\break s2.*5\pageBreak
        s2.*5\break s2.*4 s4. \bar "" \pageBreak
        s4. s2.*3\break s2.*4 s4. \bar "" \pageBreak
        s4. s2.*4\break s2.*5\pageBreak
        s2.*3 s2 \bar "" \break s2 s2.*4 s2 \bar "" \pageBreak
        s4 s2.*4 s4. \bar "" \break s4. s2.*4\pageBreak
        s2.*4 s4. \bar "" \break s4. s2.*3\pageBreak
        s2.*3\break s2.*4 s4. \bar "" \pageBreak
        s4. s2.*4\break s2.*4 s2 \bar "" \pageBreak
        s4 s1 s2. s2 \bar "" \break s4 s2.*5
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
