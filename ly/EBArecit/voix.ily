<<
  \tag #'(voix1 basse) {
    \clef "vhaute-contre" R2.*24 |
    \ifComplet {
      <>^\markup\character Un Sylphe r8 re' re' re' mi' fad' |
      sol'4 sol'8 si do'4 la8 re' |
      si4\trill si r sol16 la si dod' re'4 re'8 dod' |
      re'2. |
      \ffclef "vdessus" <>^\markup\character La Sylphide
      re''4 do''8 si' la' si' |
      \appoggiatura sol'8 fad'2 sol'4 |
      la'8 re'' do'' si' la' sol' |
      mi''4 fad'' sol'' |
      do''8([ si']) la'([ si']) do''([ la']) |
      si'2 \appoggiatura la'8 sol'4 |
      \ffclef "vhaute-contre" <>^\markup\character Le Sylphe
      re'4 sib8 do' re' mib' |
      \appoggiatura re'8 do'\trill sib la sib do' re' |
      sib4\trill \appoggiatura la8 sol la sib do' |
      re'4 re' sol' |
      fad'\trill \appoggiatura mi'8 re' sib re' mib' |
      fa'4 do'4. re'8 |
      mib'4 re'4.\trill do'8 |
      re'4. re'8 mib' fa' |
      la4.\trill sib8 do' re' |
      mib' re' do'4.(\trill sib8) |
      sib2. |
      re'4 la8 do' sib la |
      sib do' re' mib' re' do' |
      re'4 do'8 sib la sib |
      fad2\trill sol8 la |
      sib4 sol8 re' mi' fa' |
      mi'4 fad'4. sol'8 |
      do'4 sib4.\trill la8 |
      re'2. |
      la4. sib8 do'4 |
      sib8 re' la fad sol4~ sol8 la \afterGrace la2\trill( sol8) |
      sol8
      \ffclef "vdessus" <>^\markup\character La Sylphide
      sib'8 sib' do'' re'' sib' |
      mib'' re'' do'' sib' la' sol' |
      fad'4\trill la'8 sib' do'' re'' |
      mib''4 re'' sol'' |
      fad''4\trill \appoggiatura mi''8 re''4 re''8 sib' |
      fa''2 mib''8 re'' |
      do'' fa'' mib'' re'' do'' sib' |
      do''4 la'8\trill fa' sol' la' |
      sib' do'' do''4.\trill sib'8 |
      sib'2 re''8 do'' |
      sib'4 do''8([ sib']) la'([ sol']) |
      re''2 re''8 mi'' |
      fad''4 sol''8[ fad''] mi''[ re''] |
      sol''2 re''4 |
      sib'8[ la'] do''[ re''] mib''[ do''] |
      re''8 sol'' la'4.\trill sol'8 |
      sol'4.
      \ffclef "vhaute-contre" <>^\markup\character Le Sylphe
      r8 r16 si do'8 |
      re'4 sol8 do'8. do'16 do'8 |
      do'4.\trill si8. re'16 do'8 |
      si4 la8 sol8. fad16 sol8 |
      la4. r8 r16 si do'8 |
      re'4 sol8 do'8. do'16 do'8 |
      do'4.\trill si8. si16 dod'8 |
      re'4 mi'8 dod'8.\trill si16 dod'8 |
      re'4. re'8. re'16 re'8 |
      mi4. do'8. si16 la8 |
      sold4 mi8 mi'8. la'16 mi'8 |
      mi'4. fa'8. mi'16 re'8 |
      mi'4. mi'8. la'16 mi'8 |
      fa'4. do'8.\trill si16 la8 |
      la4. re'8. do'16 si8 |
      la8 do'4 \appoggiatura si8 la4\trill si8 |
      fad4.\trill \appoggiatura mi8 re8. la16 re'8 |
      si4 do'8 re'8. do'16 si8 |
      la4.\trill re'8. do'16 si8 |
      la8 do'4 \appoggiatura si8 la4 si8 |
      fad4\trill \appoggiatura mi8 re r r16 re' si8 |
      mi'4 do'8 la8. si16 do'8 |
      re'4.~ re'8. re'16 sol8 |
      do'4 si8 la8.\trill sol16 fad8 |
      sol4.
      \ffclef "vdessus" <>^\markup\character La Sylphide
      r8 r16 sol'16 la'8 |
      sib'8 re''8. sol''16 mi''4\trill la''8 |
      fad''4.\trill \appoggiatura mi''8 re''8. sol''16 \ficta fa''8 |
      mib''4 re''8 do''8.\trill sib'16 do''8 |
      re''4.~ re''8 sol'16 la' sib' do'' |
      re''8 sol''8. mi''16 fad''4.\trill |
      \appoggiatura mi''8 re'' sol''16 fa'' mib'' re'' do''8. sib'16 do''8 |
      re''4.~ re''8 r r16 fa'' |
      fa''8. sib'16 sol''8 fa'' mib'' re'' |
      do''4. do''8. re''16 mib''8 |
      fa''4 sib'8 sol'8. sol'16 sol'8 |
      sol'4.~ sol'8. mib''16 fa''8 |
      sol''4 sol'8 do''8. re''16 mib''8 |
      la'4\trill \appoggiatura sol'8 fa' r r16 re'' mi''8 |
      fa''4 sol''8 mi''8.\trill re''16 mi''8 |
      fa''4.~ fa''8. fa''16 sib'8 |
      sol'4 mib''8 do''8. re''16 mib''8 |
      la'4. \appoggiatura sol'8 fa'8. fa''16 re''8 |
      sol''4 mib''8 do''8. re''16 mib''8 |
      fa''4.~ fa''8. fa''16 sib'8 |
      mib''4 re''8 do''8. sib'16 la'8 |
      sib'4.~ sib'8. re''16 mi''8 |
      fad''4 sol''8 sib'8.\trill la'16 sol'8 |
      sol'2 \ffclef "vhaute-contre"
    }
    \ifConcert { r4 r }
    <>^\markup\character Le Sylphe
    re'4 |
    si16\trill si do' re' \appoggiatura re'8 mi'16 mi' mi' mi' fad'8. sol'16 |
    fad'4\trill \appoggiatura mi'8 re' sol'16 fad' mi'8 fad'16 sol' dod'8.\trill re'16 |
    re'4
    \ffclef "vdessus" <>^\markup\character La Sylphide
    la'4 si'8 do'' |
    si'2\trill do''8 re'' |
    mi''[ re''] do''[ re''] mi''[ do''] |
    re''2 si'4\trill |
    r re''8. do''16 si'8 la' |
    do''8[ si'16 la'] la'4.\trill sol'8 |
    sol'2. |
    si'16[ do'' re'' mi'' fad'' sol'']( fad''8.)\trill mi''16 fad''8 |
    sol''4. fad''16[ sol'' la'' sol'' fad'' mi'']( |
    re''8.) do''16 re''8 si'4 mi''8 |
    dod''8.\trill si'16 la'8 re'' re''8.([ dod''16]) |
    re''4 r16 la' si'4 r16 sol' |
    do''8. si'16 la' sol' la'4.\trill |
    la''16[ sol'' fad'' mi'' re'' do'']( si'8.)\trill la'16 sol'8 |
    re''4. la'8. si'16 do''8 |
    si'4. re''4 re''8 |
    si'16[ do'' re'' mi'' fad'' sol'']( fad''8.)\trill mi''16 fad''8 |
    sol''4 re''8 mi''8. re''16 do'' si' |
    la'[ sol' la' si' do'' la']( si'8) sol' r16 re'' |
    mi''8. mi''16 fad'' sol'' fad''4\trill re''8 |
    si' mi''4 re''8. do''16 si' la' |
    sol'8 do'' si' la'16[ sol' la' si' do'' la']( |
    si'8) sol'16 la' si' do'' re'' mi'' dod''8.\trill re''16 |
    re''4. re''16[ do'' si' la' sol' fad']( |
    sol'4) sol'8 mi''16[ re'' do'' si' la' sol']( |
    do''8.) si'16 do''8 la'4.\trill |
    re''16[ do'' si' la' sol' fad']( sol'8.) mi'16 la'8 |
    fad'4. r2*3/4 |
    mi''16[ re'' do'' si' la' sol']( do''4) do''8 |
    mi'8. fad'16 sol'8 fad'4\trill r16 re'' |
    si'4\trill re''8 mi''8. mi''16 fad'' sol'' |
    fad''[\melisma mi'' re'' do'' si' la'] do''[ si' la' si' sol' la'] |
    fad'8\trill\melismaEnd \appoggiatura mi'8 re' re'' mi''8. mi''16 fad'' sol'' |
    fad''[\trill mi'' re'' mi'' fad'' sol'']( la''4.)~ |
    la'' la''4 r16 mi'' |
    mi''4 r16 mi'' fad''8 mi''8.\trill re''16 |
    sol''8. fad''16[ mi'' fad''] fad''4\trill sol''8 |
  }
  \tag #'voix2 {
    \clef "vhaute-contre" R2.*24
    \ifComplet {
      R2. R1 R1. R2.*7 R2.*38 R2.*24 r4 r8
      r4 r8 R2.*22
    }
    r4 r r4 R2. R1 R2.*7 R2. |
    re'16[ mi' fad' sol' la' si']( la'8.)\trill sol'16 la'8 |
    si'4 sol'8 sol' si' sol' |
    mi'8.\trill mi'16 fad'8 sol' \afterGrace mi'4(\trill re'8) |
    re'4 r16 re' re'4 r16 si |
    mi'8. mi'16 fad' sol' fad'4.\trill |
    fad'16[ sol' la' sol' fad' mi']( re'4) si8\trill |
    si16[ do' re' mi' fad' sol']( fad'8.)\trill mi'16 fad'8 |
    sol'4. fad'16[ sol' la' sol' fad' mi']( |
    re'4) re'8 re'8. do'16 re'8 |
    si4\trill si8 do'8. si16 la sol |
    re'[ mi' fad' sol' la' fad']( sol'8) re' r16 sol' |
    sol'8 sol' r16 mi' la'8.[ sol'16 fad' la']( |
    sol'8.) mi'16 fad' sol' fad'4\trill re'8 |
    si \afterGrace mi'4( { fad'16[ sol']) } fad'8.\trill re'16 mi' fad' |
    sol'8. sol'16 sol' la' si'8 mi'8. la'16 |
    fad'4\trill \appoggiatura mi'8 re' r2*3/4 |
    re'8. re'16 do' si do'8 do' re' |
    mi'16[ re' do' si la sol]( fad4)\trill \appoggiatura mi8 re |
    si8 si16 si dod' re' re'8( dod'8.)\trill re'16 |
    re'4. re'16[ do' si la sol fad]( |
    sol4) sol8 mi'16[ re' do' si la sol]( |
    do'8.) si16 do'8 la4\trill r16 fad' |
    sol'4 si8 do' la mi' |
    la'8. la'16 la' la' la'4.~ |
    la' la'8 r mi' |
    la'8. la'16 la' la' fad'[\melisma mi' re' mi' fad' sol'] |
    fad'[ mi' re' mi' fad' sol'] la'4.~ |
    la'8\melismaEnd la' r16 la' la'4 r16 re' |
    mi'4 la8 la4\trill sol8 |
  }
>>
<<
  \tag #'(voix1 basse) {
    sol''8 r16 sol' re''8 re''16 si' mi''8 fad''16 sol'' |
    fad''4\trill \appoggiatura mi''8 re''4
    <<
      \tag #'basse { s4 s1 s2 \ffclef "vdessus" }
      \tag #'voix1 { r4 R1 r4 r }
    >> <>^\markup\character La Sylphide r8 la' |
    si'4 sol'8 la' si' do'' |
    re''2 r8 mi'' |
    fad''4 re''8 mi'' fad'' re'' |
    sol''4 fad''8 fad'' sol'' re'' |
    re''8([ do''16\trill si']) la'4.\trill sol'8 |
    sol'2. |
  }
  \tag #'(voix2 basse) {
    <<
      \tag #'basse { s2. s2 \ffclef "vhaute-contre" }
      \tag #'voix2 { sol4 r r | r4 r }
    >> <>^\markup\character Le Sylphe r8 re' |
    si16 si dod' re' dod'8\trill la16 la re'8 mi'16 fad' \afterGrace mi'4(\trill re'8) |
    re'2 \tag #'voix2 {
      r4 |
      r r r8 la |
      si4 sol8 la si dod' |
      re'2.~ |
      re'4 do'8 re' si si |
      mi'[ fad'16 sol'] fad'4.\trill sol'8 |
      sol'2. |
    }
  }
>>
