<<
  \tag #'(dessus1 dessus2) {
    \clef "dessus" R2.*17 R1*2 R2.*7 \allowPageTurn r4 r
  }
  \tag #'basse {
    \clef "quinte" <>^"Toutes les basses" sol'4. fad'8 sol'4 |
    do' fa'2 |
    fa2. |
    mib'4. re'8 mib'4 |
    la re'2 |
    re2. |
    sib4. la8 sib4 |
    mib sol2 |
    \clef "bass" sol,2. |
    la4. sib8 do'4 |
    fad la2 |
    la,4 sib8 la sib4 |
    sol re' re |
    sol2. |
    <>^"[B.C.]" sol,2 sol4~ |
    sol la8 sib16 sol re'8 do' |
    sib4 sol4. sol16 la |
    sib4 la8 sib fa4 do8 do'16 re' |
    mib'8 do' do re mib2 |
    re8 re' do' sib la sol |
    fad mi re mi fad re |
    sol2. |
    dod |
    re4 sol2 |
    sol4 fa4. mi8 |
    fa sol la4 la, |
    re2
  }
>>
<<
  \tag #'dessus1 {
    <>^"Flûtes" sol''4 |
    fad''8\trill mi'' re'' do'' sib' do'' |
    la' sol' la' sib' do'' la' |
    sib'4 sib''8 la'' sol'' fad'' |
    sol'' re'' sol'' fad'' sol'' la'' |
    sib''4 la''8 sol'' fad'' la'' |
    sol''4 re''16 re'' mi'' fad'' sol'' fad'' sol'' la'' |
    sib''8 do''' do'''4.(\trill sib''16 do''') |
    re'''8 la'' la''4.(\trill sol''16 la'') |
    sib''4. la''8 sol''\trill fad'' |
    sib''4. la''8 sol''\trill fad'' |
    sol'' sib'' la'' sol'' fad'' la'' |
    sol''4 re'' re'' |
    fa''4. fa''8 sol'' re'' |
    mib''4~ mib''16 re'' do'' sib' la' do'' sib' do'' |
    re''4~ re''16 do'' sib' la' sol' sib' la' sib' |
    do''4. do'''8 sib'' la'' |
    sib''4 fa''4. sib''8 |
    la''4\trill r8 fa'' sol'' re'' |
    mib'' sol'' do''' sib'' la'' fa'' |
    sib''4 la''4.\trill sib''8 |
    sib''8 fa'' sib' fa'' sib'' la'' |
    sol'' sib'' la'' sol'' fad'' sol''16 la'' |
    sib''4 si''4.(\trill la''16 si'') |
    do'''2 do'''4 |
    dod'''8 \ficta si'' la''16 si''! la'' si'' dod''' re''' dod''' re''' |
    mi'''8 re''' dod''' re''' mi''' dod''' |
    re'''2 re'''8 do''' |
    sib'' la'' sol'' sol'' do''' sib'' |
    la'' sol'' fad'' sol'' fad'' sol''16 la'' |
    sib''8 la'' la'' sol'' fad''(\trill mi''16 fad'') |
    sol''8 la'' sib''4. sib''8 |
    sib''4 la''8 sol'' do''' sib'' |
    la'' sib'' la''4.\trill sol''8 |
    sol''8 sol' la' sib' do'' re'' mib''4. re''8 sol''4~ |
    sol''16 fad'' mi'' fad'' fad''4.\trill sol''8 |
    sol''2
  }
  \tag #'(dessus2 basse) {
    \tag #'basse { \clef "dessus" <>^"Violons [et clavecin]" }
    \tag #'dessus2 <>^\markup\whiteout Violons
    sol'4 |
    re'' fad' sol' |
    re' re'' do'' |
    sib'8 do'' re''4 re' |
    sol'2. |
    sol'4 fad'4. re'8 |
    sol'8 la' sib' la' sol' fa' |
    mib'4 mib'2 |
    re'4 <<
      \tag #'dessus2 { r4 r | R2.*4 | r4 <>^\markup\whiteout [Violons] }
      \tag #'basse {
        \clef "basse" <>^"[Basses et B.C.]" re'8 mib' re' do' |
        sib do' re'4 re |
        sol,8 re sol fad sol la |
        sib4 fad re |
        sol8 sol fa mib re do |
        sib,4 <>^\markup\whiteout "[Violons et clavecin]"
      }
    >> \clef "dessus2" sib'8 lab' sol' fa' |
    mib' re' do'4 fa'8 mib' |
    re' do' sib4 mib'8 re' |
    do'4 fa'2 |
    sib4. sib'8 la' sol' |
    fa'4 sib'8 lab' sol' fa' |
    mib'2. |
    re'8 mib' fa'2 |
    sib r4 | \allowPageTurn
    R2. |
    r4 sol' sol |
    do'2. |
    r4 la' la |
    dod''8 si' la' si' dod'' la' |
    re''4 re'8 do' sib la |
    sol4 r r |
    r re' do' |
    sib do' re' |
    sol4. <<
      \tag #'dessus2 { r8 r4 | R2.*2 | r4 <>^"Violons" }
      \tag #'basse {
        \clef "basse" <>^"[Basses et B.C.]" sol8 la sib |
        do' sib do' re' mib' do' |
        re' sol re4 re, |
        sol, \clef "dessus2" <>^"Violons [et clavecin]"
      }
    >> sol'8 fa' mib' re' |
    do'' sib' do'' re'' mib''4 |
    do'' re'' re' |
    sol'2
  }
>>
