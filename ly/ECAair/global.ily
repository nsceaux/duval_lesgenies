\key re \minor \beginMarkSmall "Prélude"
\digitTime\time 3/4 \midiTempo#160 s2.*14 \midiTempo#80 s2.*3
\time 4/4 s1
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#120 \grace s8 s2.*45 \bar "|."
