C’est i -- ci que l’a -- mour va m’of -- frir des hom -- ma -- ges,
qui vont fai -- re bril -- ler __ le pou -- voir de ses traits ;
sous ce dé -- gui -- se -- ment re -- dou -- blent mes at -- traits,
je vais trom -- per des cœurs vo -- la -- ges.
A -- mour, sous tes ai -- ma -- bles lois,
tu sou -- mets à ja -- mais mon â -- me ;
A -- mour, sous tes ai -- ma -- bles lois,
tu sou -- mets à ja -- mais mon â -- me ;
per -- mets que pour ta gloire __ et l’hon -- neur de mon choix,
je puis -- se feindre une a -- mou -- reu -- se flam -- me,
per -- mets que pour ta gloire __ et l’hon -- neur de mon choix,
je puis -- se feindre une a -- mou -- reu -- se flam -- me,
je puis -- se feindre une a -- mou -- reu -- se flam -- me.
