\piecePartSpecs
#`((dessus #:score-template "score-2dessus-voix"
           #:notes "dessus-basse")
   (flute-hautbois #:score-template "score-voix"
                   #:notes "dessus-basse"
                   #:tag-notes dessus1)
   (dessus1 #:score-template "score-voix"
            #:notes "dessus-basse"
            #:tag-notes dessus2)
   (dessus2 #:score-template "score-voix"
            #:notes "dessus-basse"
            #:tag-notes dessus2)
   (basse #:score-template "score-basse-continue-voix"
          #:notes "dessus-basse"
          #:tag-notes basse
          #:instrument ,#{\markup\center-column { [Basses et B.C.] }#})
   (silence #:on-the-fly-markup , #{
\markup\fontsize#-2 \column {
  \livretPers Florise
  \livretVerse#12 { C’est ici que l’amour va m’offrir des hommages, }
  \livretVerse#12 { Qui vont faire briller le pouvoir de ses traits ; }
  \livretVerse#12 { Sous ce déguisement redoublent mes attraits, }
  \livretVerse#8 { Je vais tromper des cœurs volages. }
  \livretVerse#8 { Amour, sous tes aimables lois, }
  \livretVerse#8 { Tu soumets à jamais mon âme ; }
  \livretVerse#12 { Permets que pour ta gloire & l’honneur de mon choix, }
  \livretVerse#10 { Je puisse feindre une amoureuse flamme. }
}#}))
