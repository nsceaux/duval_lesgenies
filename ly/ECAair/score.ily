\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus-basse" >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses et B.C.] }
    } <<
      \global \keepWithTag #'basse \includeNotes "dessus-basse"
      \includeFigures "chiffres"
      \modVersion { s2.*17 s1*2 s2.*7\break }
      \origLayout {
        s2.*12\break s2.*5 s4 \bar "" \break s2. s1 s2.*2\pageBreak
        \grace s8 s2.*5 s2 \bar "" \break s4 s2.*5\pageBreak
        s2.*5\break s2.*5\pageBreak
        s2.*5\break s2.*6\pageBreak
        s2.*5\break \grace s8 s2.*6
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
