\clef "vdessus" R2.*14 |
r4 r <>^\markup\character Florise sol'8 sol' |
sib' sib'16 sol' mib''8 re''16 sol'' fad''8\trill \ficta mi''16 fad'' |
\appoggiatura fad''8 sol''4 sol'' r8 sib'16 do'' |
re''8 re''16 sib' fa''8[\melisma mib''16 re''] do'' [ re'' do'' re'' ] mib''[ fa'' mib'' fa'']( |
sol''4)\melismaEnd mib''8 re'' do''4\trill sib'8 do'' |
\appoggiatura do''8 re''2. |
la'4 la'8 la' re'' la' |
\appoggiatura la'8 sib'2 r8 re'' |
sol''4. sol''8 fa'' mi'' |
\appoggiatura mi'' fa''4 r8 mi'' fa'' sol'' |
dod''8. la'16 re''4. dod''8 |
re''16[\melisma mi'' re'' mi''] \afterGrace mi''2\trill( re''8)\melismaEnd |
re''2 r4 |
r r r8 sol'' |
fad''2.\trill |
sol''4 re''8 do'' sib' la' |
sol'2 sib'8 do'' |
re''4 do''8 sib' la' re'' |
sib'4\trill \appoggiatura la'8 sol'4 r |
r r r8 sol'' |
fad''2.\trill |
sol''4 re''8 do'' sib' la' |
sol'2 sib'8 do'' |
re''4 do''8 sib' la' re'' |
sib'4\trill \appoggiatura la'8 sol'4 sib' |
re''4.\trill re''8 mib'' fa'' |
sol''4~ sol''16[\melisma fa'' mib'' re''] do''[ mib'' re'' mib''] |
fa''4~ fa''16[ mib'' re'' do''] sib'[ re'' do'' re''] |
mib''2\melismaEnd re''8 do'' |
re''2 do''8 sib' |
do''4 r8 re'' mib'' fa'' |
sol''4. do''8 fa'' la' |
sib' do'' do''4.(\trill sib'8) |
sib'2 r4 |
r r re'' |
sol''4. re''8 mi'' fa'' |
mi''[\melisma re''] do''16[ re'' do'' re''] mi''8[ fa''16 sol'']( |
la''2)\melismaEnd mi''8 fad'' sol''4 sol''4.*5/6 \ficta fad''16[ mi'' fad''] |
fad''4.\trill la'8 sib' do'' |
re''4. sib'8 mib'' re'' |
do'' sib' la'8.[\melisma sib'16] la'8[ sib'16 do''] |
re''4. mib''16[ re''] do''[ mib'' re'' do''] |
sib'4\trill\melismaEnd \appoggiatura la'8 sol' sib' do'' re'' |
\appoggiatura re'' mib''4. re''8 sol'' \ficta mi'' |
fad'' sol'' sol''4.\melisma fad''8\melismaEnd |
sol''4 r r |
R2.*3 |
