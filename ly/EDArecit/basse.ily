\clef "basse"
\ifComplet {
  sol4 |
  sol8. do16 re8 sol, fad,4 sol,8 fad,16 mi, |
  re,4 re sol mi |
  la fad re |
  sol, la,2 |
  re8 re' do' si la sol |
  fad mi re mi fad re |
  sol4 fad mi |
  re re, la, |
  re4. dod8 re mi |
  fad4 sol8 fa mi re |
  do sol do' si la sol |
  re'4 re2 |
  sol8 re' do' si la sol |
  fad mi re mi fad re |
  sol4 fad4. mi8 |
  re4 re, la, |
  re4. dod8 re mi |
  fad2 sol8 si la sol |
  do'4~ do'16 si la sol re'4 re |
  sol8. si16 la8 sol8. do16 re8 |
  sol,4. r2*3/4 | \allowPageTurn
  r8 r16 re' do'8 si8. sol16 do'8 |
  si8. do'16 la8 si8. la16 sol8 |
  si8. do'16 la8 si8. la16 sol8 |
  do'4 re'8 mi'4 do'8 |
  re' fad re re' fad re |
  sol16 fad mi8 re la la,4 |
  re8. fad16 re8 sol8. do16 re8 |
  sol,4. r2*3/4 | \allowPageTurn
  r8 r16 re' do'8 si8. sol16 do'8 |
  si8. do'16 la8 si8. la16 sol8 |
  si8. do'16 la8 si8. la16 sol8 |
  do'4 re'8 mi'4 do'8 |
  re' fad re re' fad re |
  sol16 fad mi8 re la la,4 |
  re8 re' do' si la sol |
  fad mi re mi fad re |
  sol4 fad mi |
  re8 mi fad re sol fad |
  sol si la sol fad mi |
  fad re sol4 sol, |
  re8 re' do' si la sol |
  do4 dod2 |
  re4 re' do' |
  si8 sol do'2 |
  si4 do' re' re |
  sol4. mi8 fa4 re |
  mi dod re4~ re16 do si, la, |
  mi4 re do8 la, |
  re4. mi8 fa4 re mi mi, |
  la,8 la16 sol fad2 |
  sol fad8 mi |
  re4 re, re8 do |
  si, sol, la,2 |
  re4 si,2 |
  mi4 fad4. sol8 |
  fad re sol la si4 |
  do'4 re' re |
  sol8 fad sol la sol fad |
  mi2.~ | \allowPageTurn
  mi |
  mi4 red mi |
  la,2 si,4 |
  mi2 r4 |
  r r mi8 fad |
  sol4 fad mi |
  re8 mi re dod re si, |
  fad4 mi re8 si, |
  fad4 mi re8 si, |
  sol mi fad4 fad, |
  si, si8 la sol fad |
  si,2 sol4 |
  sol4. fad8 mi2 |
  do la, |
  si,4 si la |
  sol2 fad8 mi |
  la si do'2 |
  si8 la sol4 fad8 mi |
  la si do'2 |
  si8 la sol4 fad8 mi |
  red mi16 re do2 |
  si,4 si8 do' si la |
  sold fad mi fad sold mi |
  la2 la,4 |
  la4. si8 la sol la fad |
  si fad si, si dod' red' |
  mi'4 mi si |
  mi' mi la |
  sol8 la si4 si, |
  mi2 la4 fad |
  si la sol8 fad |
  mi2 red4 mi |
  la, si,2 |
  mi mi,4 |
  la, la red |
  mi4. red8 mi re |
  do4 do2\trill |
  si,4 si la |
}
\ifConcert { mi,2. | }
sol8 si la sol fad mi |
red2. |
mi8 red mi fad sol mi |
la2 si4 |
mi2. | \allowPageTurn
red |
mi4~ mi8. mi16 fad sol la si |
do'4 si la |
sol2. |
si,2 mi8 do |
re4 re, r |
re4 do8 si, la, sol, |
do,4 re,2 |
sol,4 si4. si8 |
fad4 re si,~ |
si, la,2 |
la2.~ |
la~ | \allowPageTurn
la2 sol4 |
la8 si do'4 la |
si2 la4 |
sol4~ sol16 mi fad sol la si do' re' |
mi'4. mi'8 mi'4 |
la2. |
la |
si |
si la |
sol2 sol4 |
la si si, |
mi4. si8 dod' red' |
mi'4. re'8 dod' si |
lad2 la4~ |
la si si, |
mi2. |
