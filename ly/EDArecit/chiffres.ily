\ifComplet {
  s4 s8. <6 5>16 s4 <6 5/> s8 <6>16 <6+> s4 <7>2 <7>4 <_+> <6>2 <6 5>4 <4> <_+> <"">8*5\figExtOn
  <"">8 <5/>8*5 <5/>8\figExtOff s4 <6> <6+> s2 <_+>4 <"">2\figExtOn <"">8\figExtOff <6> <5/>2 <6>8 <6 _-> <"">4.\figExtOn <"">8\figExtOff <6>4
  <6 4>4 <3>2 <"">4.\figExtOn <"">8 <6> <6> <5/>2 <5/>4\figExtOff s4 <6>4. <6+>8 s2 <_+>4 <"">2\figExtOn <"">8\figExtOff <6> <5/>2 s8 <6> <6>4
  s4. <6>8 <4>4 <3> s4 <6>8 s8. <6 5> s2. s4. <6>4 <6 5>8 <6>8.\figExtOn <6>16\figExtOff <6>8 <6>8.\figExtOn <6>\figExtOff <6>8.\figExtOn <6>16\figExtOff <6>8 <6>4\figExtOn <6>8\figExtOff
  s4 <6 4>8 <4 3>4 <6 5>8 <"">2\figExtOn <"">8\figExtOff <7> <9> <6+>4 <6 4>8 <_+>4 s4. s8. <6 5> s2.
  s4. <6>4 <6 5>8 <6>4 <6>8 <6>4\figExtOn <6>8\figExtOff <6>4 <6>8 <6>4\figExtOn <6>8\figExtOff
  s4 <6 4>8 <4 3>4 <7 5>8 <"">4\figExtOn <"">8\figExtOff <"">8\figExtOn <"">\figExtOff <7> <9>4 <6+>8 <6 4> <_+>4 <"">8*5\figExtOn <"">8
  <5/>8*5 <5/>8\figExtOff s4 <6> <7>8 <6+> <"">2\figExtOn <"">4\figExtOff
  s8 <6> <6>4\figExtOn <6>8\figExtOff <6> <5/>4 <"">\figExtOn <"">\figExtOff <"">2\figExtOn <"">8\figExtOff <7->
  s4 <5/>2 <"">4\figExtOn <"">2\figExtOff <6>4 <3> <4+> <6> <9>8 <8> <4>4 <3>
  s4. <6>8 s4 <6 5 _-> <_+> <6>
  <3->4. <6+>8 <_+>4\figExtOn <_+>\figExtOff <6> <9 7 _->2 <4 3>4 <7 5 _-> <4> <_+>
  s4 <6>2 s <6>8 <6+>
  s2. <6>8 <6 5> <_+>4 <7> s <6>2 <6>4 <5/>2 <6>4 <"">8\figExtOn <"">\figExtOff <6>4
  <6 5>4 <4> <3> <"">4.\figExtOn <"">8\figExtOff <6> <6\\ > s2 s8 <7+>
  <3>2. <2>4 <7 5/>2 <6 5> <7 _+>4
  s2.*2 <4+ 3>4 <6 4> <6+ 4+> <6>2\figExtOn <6>4
  <_+> <_+> <6>8 <6> <_+>4 <_+>\figExtOff <6>4 <4+ 3>8 <6+ 5> <_+>2
  <_+>2 <6>8 <6+> <_+>2 <5>4 s4 <6>8 <6+> s2 <4+ 3> <6 5> <_+>2\figExtOn <_+>4\figExtOff
  <6>2 <6+>4 <6 5>8 <_+> <7>4 <6> <_+>4 <6> <6+> <6 5>8 <_+> <4 3>2 <_+>4 <6> <6+> <7- 5>8 <5> <4 3>2
  <_+>8*5\figExtOn <_+>8 <6>8*5 <6>8 <9 _+>4 <8 _+>2\figExtOff
  <6 5 _+>2.\figExtOn <6>4 <_+>2 <_+>4\figExtOff s2 <_+>4
  s2 <_+>8 <4+> <6> <6 5> <_+>4 <7> <_+>4 <7> <_+> <7> <_+>\figExtOn <_+>\figExtOff <6>8 <6+>
  s2 <7 5/> <_+>4 <_+>2 <_+> <7>4 s2 <5/>4
  <"">2\figExtOn <"">4\figExtOff <7>4 <6>2 <_+>2.
}
\ifConcert s2.
<6>4.\figExtOn <6>8\figExtOff <6+>4 <6>2 <5/>4 <"">2\figExtOn <"">4\figExtOff <6 5>2
s4 s2. <7 5/> s <4+ 2+>4 <_+>\figExtOn <_+>\figExtOff <6>2. <6>2 s8 <6 5>
s2. s4. <6>8 <6>4 <6 5> <4> <3> s2. <_+>4 <6>2 <2+>4 <4+>2
<4+>2. s s2 <6>4 <6 5>8 <_+> <4+ 3>4 <6 5> <_+>2\figExtOn <_+>4
<6>8*5 <6>8\figExtOff s2. <6 5> <6 5> <_+>4. <6 4>
<_+>2. <4+> <6> <6 4+>8 <7 5> <6 4>4 <_+> <"">8*5\figExtOn <"">8\figExtOff
<"">8*5\figExtOn <"">8\figExtOff <7 5/>2 <4+>4
<7 5> <6 4> <7 _+>
