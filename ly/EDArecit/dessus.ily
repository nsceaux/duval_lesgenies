\clef "dessus" r4 R1*2 R2.*15 R1*2 |
r8 r16 <>^"Violons" re''\doux do''8 si'8. do''16 la'8 |
si'8. si'16 la'8 sol'8. fad'16 mi'8 |
re'8. re''16 mi'' fad'' sol''4 sol''8 |
sol''8. la''16 fad''8 sol''8. la''16 si''8 |
sol''8. la''16 fad''8 sol''8. la''16 si''8 |
sol''8 do''' si'' la''4 sol''8 |
fad''8. sol''16 la''8 fad''8. sol''16 la''8 |
re''8 sol'' fad'' fad''( mi''8.\trill re''16) |
re''8. re''16 do''8 si'8. do''16 la'8 |
si'8. si'16 la'8 sol'8. fad'16 mi'8 |
re'8. re''16 mi'' fad'' sol''4 sol''8 |
sol''8. la''16 fad''8 sol''8. la''16 si''8 |
sol''8. la''16 fad''8 sol''8. la''16 si''8 |
sol'' do''' si'' la''4 sol''8 |
fad''8. sol''16 la''8 fad''8. sol''16 la''8 |
re'' sol'' fad'' fad'' mi''8.\trill re''16 |
re''2 r4 |
\ru#9 { R2.\allowPageTurn } \ru#3 { R1\allowPageTurn } R2.\allowPageTurn
R1.\allowPageTurn \ru#8 { R2.\allowPageTurn } |
r4 r <>^"Flûtes et violons" mi''8 red'' |
mi''4 si''8 la'' sol'' fad'' |
sol''4 \appoggiatura fad''8 mi''4 sol''8 mi'' |
la''2 sol''4 |
fad''8 la'' \appoggiatura sol'' fad''4 \appoggiatura mi''8 red''4 |
mi'' si' sol''8 fad'' |
mi'' re'' dod'' si' mi'' re'' |
dod''4 si' lad' |
si' fad'4. si'8 |
lad'4. lad'8 si' dod''16 re'' |
dod''8.( fad'16) fad'4 fad''8 si'' |
si''4 lad''4.\trill si''8 |
si''4 si''4. la''8 |
si''2 r4 |
R1*2\allowPageTurn R2.*3\allowPageTurn |
si'2 la'8 si'16 sol' do''8 si' la'4.(\trill sol'16 la') |
si'4 mi'' red''8 mi'' |
fad'' sol'' la'' sol'' fad'' mi'' |
red''2\trill r4 |
r r si''8 si'' |
mi'' re'' mi'' fad'' mi'' re'' |
dod''2 r4 la'' |
\appoggiatura sol''8 fad''2 fad''8 sol'' |
sol''2( fad''8)\trill mi''16 red'' |
mi''8 fad'' sol'' la'' la''8(\trill sol''16 la'') |
si''8 la''16 sol'' fad''4.\trill mi''8 |
mi''4 r r2 |
R2. R1 \ru#41 { R2.\allowPageTurn } |
