\key sol \major
\ifComplet {
  \once\omit Staff.TimeSignature
  \digitTime\time 3/4 \partial 4 \midiTempo#80 s4
  \time 4/4 s1
  \digitTime\time 2/2 \midiTempo#160 s1
  \digitTime\time 3/4 s2.*15
  \digitTime\time 2/2 s1*2
  \time 6/8 \midiTempo#80 s2.*16
  \digitTime\time 3/4 \midiTempo#160 s2.*10
  \digitTime\time 2/2 \midiTempo#80 s1*3
  \digitTime\time 3/4 s2.
  \time 3/2 \midiTempo#160 \grace s8 s1.
  \digitTime\time 3/4 \midiTempo#80 s2.*9
  \beginMarkSmall "Air" s2. \bar ".|:" s2.*10 \alternatives s2. s2.
  \digitTime\time 2/2 \midiTempo#160 s1*2
  \digitTime\time 3/4 \midiTempo#80 s2.*3
  \digitTime\time 3/4 \grace s8 s2.*7
  \digitTime\time 2/2 \midiTempo#160 s1
  \digitTime\time 3/4 \midiTempo#80 \grace s8 s2.*4
  \time 4/4 \grace s8 s1
  \digitTime\time 3/4 s2.
  \digitTime\time 2/2 \midiTempo#160 s1
  \digitTime\time 3/4 \midiTempo#80 s2.*5 s2
}
\ifConcert { \digitTime\time 3/4 }
\beginMarkSmall "Duo" \midiTempo#120 \ifConcert s2 s4 s2.*35 \bar "|."
