\tag #'(voix1 basse) {
  \ifComplet {
    Bel -- le Nymphe, à l’é -- clat dont bril -- lent vos beaux yeux,
    que de cœurs vont ren -- dre les ar -- mes !
    Non, non, du dieux d’a -- mour les traits vic -- to -- ri -- eux,
    sont moins à crain -- dre que vos char -- mes.
    Non, non, du dieux d’a -- mour les traits vic -- to -- ri -- eux,
    sont moins à crain -- dre que vos char -- mes.
    
    D’u -- ne fou -- le d’a -- mants qui vo -- le sur mes pas
    je ne crains point le lan -- ga -- ge ;
    d’u -- ne fou -- le d’a -- mants qui vo -- le sur mes pas
    je ne crains point le lan -- ga -- ge ;
    il est un a -- mant dont l’hom -- ma -- ge
    au -- rait pour moi des ap -- pas.
    Il est un a -- mant dont l’hom -- ma -- ge
    au -- rait pour moi des ap -- pas
    
    Et quel est cet a -- mant ? ah ! que je porte en -- vi -- e
    au sort dont vous flat -- ter son cœur ;
    le plus doux ins -- tant de ma vi -- e,
    se -- rait mar -- qué par ce bon -- heur,
    le plus doux ins -- tant de ma vi -- e,
    se -- rait mar -- qué par ce bon -- heur.
    
    La lan -- gueur des a -- mants sans ces -- se me fait ri -- re :
    ils m’a -- dres -- sent leurs vœux, je fo -- lâ -- tre,
    je fo -- lâ -- tre tou -- jours ;
    la lan- jours ;
    quand je suis près de vous, je sens que je sou -- pi -- re,
    que me de -- man -- dent les a -- mours ?
    que me de -- man -- dent les a -- mours ?
    Quand je suis près de vous, je sens que je sou -- pi -- re,
    que me de -- man -- dent les a -- mours ?
    
    Ah ! c’en est trop Nym -- phe char -- man -- te,
    un a -- veu si flat -- teur paie as -- sez mes sou -- pirs.
    
    Que no -- tre ten -- dres -- se s’aug -- men -- te
    par l’es -- poir de mil -- le plai -- sirs.
  }
  For -- mons u -- ne chaî -- ne si bel -- le
  au mi -- lieu des ris et des jeux :
  vole __ a -- mour, vo -- le, viens nous rendre heu -- reux,
  c’est la cons -- tan -- ce qui t’ap -- pel -- le.
  Vole a -- mour, vo -- le, viens nous rendre heu -- reux,
  vole a -- mour, vo -- le, vole __ a -- mour, viens nous rendre heu -- reux,
  c’est la cons -- tan -- ce qui t’ap -- pel -- le,
  c’est la cons -- tan -- ce qui t’ap -- pel -- le.
}
\tag #'voix2 {
  For -- mons u -- ne chaî -- ne si bel -- le
  au mi -- lieu des ris et des jeux :
  vole a -- mour, viens nous rendre heu -- reux,
  c’est la cons -- tan -- ce qui t’ap -- pel -- le.
  Vole a -- mour, vo -- le, viens nous rendre heu -- reux,
  vole __ a -- mour, vole __ a -- mour, viens nous rendre heu -- reux,
  c’est la cons -- tan -- ce qui t’ap -- pel -- le,
  c’est la cons -- tan -- ce qui t’ap -- pel -- le.
}
