\piecePartSpecs
#`((dessus #:score-template "score-voix")
   (dessus2 #:score-template "score-voix")
   (basse #:score-template "score-basse-continue-voix"
          #:instrument "[B.C.]")
   (silence #:on-the-fly-markup , #{
\markup\fontsize#-2 \fill-line {
  \column {
    \livretPers Florise
    \livretVerse#12 { Belle Nymphe, à l’éclat dont brillent vos beaux yeux, }
    \livretVerse#8 { Que de cœurs vont rendre les armes ! }
    \livretVerse#12 { Non, non, du dieux d’amour les traits victorieux, }
    \livretVerse#8 { Sont moins à craindre que vos charmes. }
    \livretPers La Sylphide
    \livretVerse#12 { D’une foule d’amants qui vole sur mes pas }
    \livretVerse#7 { Je ne crains point le langage ; }
    \livretVerse#8 { Il est un amant dont l’hommage }
    \livretVerse#7 { Aurait pour moi des appas. }
    \livretPers Florise
    \livretVerse#12 { Et quel est cet amant ? ah ! que je porte envie }
    \livretVerse#8 { Au sort dont vous flatter son cœur ; }
    \livretVerse#8 { Le plus doux instant de ma vie, }
    \livretVerse#8 { Serait marqué par ce bonheur. }
  }
  \column {
    \livretPers La Sylphide
    \livretVerse#12 { La langueur des amants sans cesse me fait rire : }
    \livretVerse#12 { Ils m’adressent leurs vœux, je folâtre toujours ; }
    \livretVerse#12 { Quand je suis près de vous, je sens que je soupire, }
    \livretVerse#8 { Que me demandent les amours ? }
    \livretPers Florise
    \livretVerse#8 { Ah ! c’en est trop Nymphe charmante, }
    \livretVerse#12 { Un aveu si flatteur paie assez mes soupirs. }
    \livretPers La Sylphide
    \livretVerse#8 { Que notre tendresse s’augmente }
    \livretVerse#8 { Par l’espoir de mille plaisirs. }
    \livretPers Ensemble
    \livretVerse#8 { Formons une chaîne si belle }
    \livretVerse#8 { Au milieu des ris & des jeux : }
    \livretVerse#8 { Vole amour, viens nous rendre heureux, }
    \livretVerse#8 { C’est la constance qui t’appelle. }
  }
}#}))
