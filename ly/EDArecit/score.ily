\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \ifComplet\new Staff << \global \includeNotes "dessus" >>
    \new Staff \withLyrics <<
      \ifConcert\instrumentName\markup\character La Sylphide
      \global \keepWithTag#'voix1 \includeNotes "voix"
    >> \keepWithTag#'voix1 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \ifConcert\instrumentName\markup\character Florise
      \global \keepWithTag#'voix2 \includeNotes "voix"
    >> \keepWithTag#'voix2 \includeLyrics "paroles"
    \new Staff \with { instrumentName = "[B.C.]" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s1*2 s2.*2 s2 \bar "" \break s4 s2.*6\break s2.*6 s1\pageBreak
        s1 s2.*5\break s2.*5\pageBreak
        s2.*6 s2 \bar "" \break s4 s2.*6\pageBreak
        s2.*3 s1*2 s2 \bar "" \break s2 s2. s1. s2.*2\break s2.*5\pageBreak
        s2.*6\break s2.*7\pageBreak
        s2.*2 s1*2 s2.\break s2.*6\pageBreak
        s2.*3 s1 s2.*2\break s2.*2 s1 s2.\pageBreak
        s1 s2.*3\break \grace s8 s2.*6 s2 \bar "" \pageBreak
        s4 s2.*6\break s2.*6\pageBreak
        s2.*5\break s2.*5\pageBreak
        s2.*7\break s2.*2
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
