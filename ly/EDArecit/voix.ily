<<
  \tag #'(voix1 basse) {
    \ifComplet {
      \ffclef "vdessus" <>^\markup\character Florise
      sol''8 sol'' |
      re'' si'16 mi'' la'8 si' do''4 si'8 la'16\trill sol' |
      la'4 re''8 do'' si'4 mi'' |
      dod'' la'4. re''8 |
      re''2( dod''4) |
      re''2 r8 la' |
      do''2. |
      si'4 la'4.\trill sol'8 |
      re''2 r8 mi'' |
      fad''4.\trill mi''8 fad'' sol'' |
      do''4 si' do''8 re'' |
      mi''4 mi'' fad''8 sol'' |
      sol''2( fad''4) |
      sol''2 r8 la' |
      do''2. |
      si'4 la'4.\trill sol'8 |
      re''2 r8 mi'' |
      fad''4.\trill mi''8 fad'' sol'' |
      do''2 si'4 do''8 re'' |
      mi''4 mi''8 fad''16 sol'' sol''4.( fad''8) |
      sol''4. r2*3/4 |
      \ffclef "vdessus" <>^\markup\character La Sylphide
      r8 r16 re''16 do''8 si'8. la'16 sol'8 |
      re''4.~ re''4 mi''8 |
      re''4.~ re''8.[\melisma mi''16 fad'' sol''] |
      re''4.~ re''8.[ mi''16 fad'' sol'']( |
      mi''4)\melismaEnd re''8 do''4 si'8 |
      la'4.\trill la'8. si'16 do''8 |
      si'8. dod''16 re''8 re''4( dod''8) |
      \appoggiatura dod''8 re''4 r8 r2*3/4 |
      r8 r16 re'' do''8 si'8. la'16 sol'8 |
      re''4.~ re''4 mi''8 |
      re''4.~ re''8.[\melisma mi''16 fad'' sol''] |
      re''4.~ re''8.[ mi''16 fad'' sol'']( |
      mi''4)\melismaEnd re''8 do''4 si'8 |
      la'4.\trill la'8. si'16 do''8 |
      si'8. dod''16 re''8 re''4( dod''8) |
      re''2 r8 re' |
      la'2 si'8 do'' |
      si'4 la'4.\trill sol'8 |
      re''2 si'4\trill |
      r8 re'' do''[ si'] la'[ sol'] |
      do''4 si'4.\trill do''8 |
      la'8 la' re''4 mi''8 fa'' |
      mi''4 fad''4. sol''8 |
      fad''2\trill \appoggiatura mi''8 re''4 |
      r8 sol'' \appoggiatura fad''8 mi''4 la' |
      re''4.( do''16[\trill si']) la'4.\trill sol'8 |
      sol'4
      \ffclef "vdessus" <>^\markup\character Florise
      si'8. do''16 la'4\trill sold'8 la' |
      sold'4\trill mi'' r fa''16 mi'' re'' do'' |
      si'4\trill si' r8 mi'' |
      \appoggiatura mi''8 fa''2 re''4 si'8 do'' si'4.\trill la'8 |
      la'4 re''8. do''16 si'8 la' |
      si'2 la'8 sol' |
      la'4 fad'\trill r16 la' si' do'' |
      re''8 mi'' dod''4.\trill re''8 |
      re''4 sol''8. fad''16 mi''8 re'' |
      do''4 la'4.\trill sol'8 |
      re''4 si'\trill r16 re'' sol'' re'' |
      mi''8 la' la'4.\trill sol'8 |
      sol'2.
      \ffclef "vdessus" r4 r <>^\markup\character La Sylphide
      mi''8 red'' |
      mi''4 si'4. la'8 |
      do''2 si'4 |
      la'8 do'' \appoggiatura si' la'4 \grace sol'8( \afterGrace fad'4 sol'8) |
      sol'4 \appoggiatura fad'8 mi'4 r |
      r r sol''8 fad'' |
      mi''4 re'' dod'' |
      fad''2 si'8 re'' |
      dod''4 \appoggiatura fad'8 fad'4 r |
      r r si'8 re'' |
      dod''[ re''16 mi''] dod''4.\trill si'8 |
      si'2 mi''8 red'' |
      si'2 si'8 sol' |
      re''4 mi''8 fad'' \appoggiatura fad'' sol''4 r8 si' |
      fad''4. sol''8 fad''4. mi''8 |
      red''2\trill \appoggiatura dod''8 si'4 |
      si'2 la'8 sol' |
      do'' si' \grace si'8( la'4.\trill sol'16) la' |
      \appoggiatura la'8 si'2 r4 |
      R2. |
      si'2 la'8 sol' |
      do'' si' \grace si'8( la'4.\trill sol'16) la' |
      si'2 si'8 dod'' |
      re''4 si'4.\trill mi''8 |
      dod''2\trill r8 dod'' |
      fad''4. sol''8 fad''4. \appoggiatura red''8 mi'' |
      red''2\trill \appoggiatura dod''8 si'4 |
      R2. |
      si'2 dod''8 red'' |
      mi''4. mi''8 mi'' red'' |
      \appoggiatura red''8 mi''4
      \ffclef "vdessus" <>^\markup\character Florise
      mi''8 si'16 re'' dod''4 fad''8 dod''16 fad'' |
      red''4\trill red'' r8 si'16 la' |
      sol'4 fad'8 mi' do''4 si'8 mi'' |
      dod''8[ red''16 mi''] red''4.\trill mi''8 |
      mi''4
      \ffclef "vdessus" <>^\markup\character La Sylphide
      si'16 si' do'' re'' sold'8 la'16 si' |
      \appoggiatura si'8 do''4 do'' r8 la'16 si' |
      \appoggiatura la'8 sol'4.\trill fad'8 sol'4~ |
      sol'16[ la' sol' la'] la'4.\trill si'8 |
      si'2
    }
    \ifConcert { \ffclef "vdessus" r4 r }
    si'4 |
    mi''2 red''8 mi'' |
    fad''2\trill mi''8 fad'' |
    sol''4 \appoggiatura fad''8 mi''4 si'8 la' |
    do''2 r8 si' |
    si'4 \appoggiatura la'8 sol'4.\trill fad'8 |
    fad'4\trill r8 r16 si'[ dod'' red'' mi'' fad'']( |
    sol''4.) sol''8 sol''4 fad''8[ mi'' red'' mi'' fad'' mi''16 red'']( |
    mi''2) si'4 |
    re''4. do''8 si' do'' |
    la'4.\trill la'8 si' do'' |
    re''4. mi''8 fad'' sol'' |
    sol''2( fad''4) |
    \appoggiatura fad''8 sol''4 si'4. re''8 |
    dod''4 fad''8[\melisma sol'' fad'' sol''] |
    mi''8.[ fad''16 red''8. mi''16 red''8. mi''16] |
    red''[ mi'' red'' mi''] red''4\trill~ red''16[ mi'' red'' mi'']( |
    fad''2)\melismaEnd si'4 |
    R2. |
    mi''8 red'' mi''4. fad''8 |
    red''4\trill si'4. si'8 |
    si'2. |
    mi''4 mi''8 r16 mi'[\melisma fad' sol' la' si'] |
    do''2~ do''8\melismaEnd do'' |
    do''2. |
    fad''4. sol''8 fad'' mi'' |
    red''2. |
    si'2 dod''8 red'' |
    mi''2 mi''4 |
    fad''8 sol'' sol''4( fad'')\trill |
    mi''2. |
    sol''2 sol''8 sol'' |
    sol''2 fad''4 |
    red''8 mi'' mi''4.( red''8) |
    mi''2. |
  }
  \tag #'voix2 {
    \clef "vdessus"
    \ifComplet {
      r4 R1*2 R2.*15 R1*2 R2.*26 R1*3 R2. R1. R2.*22
      R1*2 R2.*10 R1 R2.*4 R1 R2. R1 R2.*7 |
      <>^\markup\character Florise
    }
    \ifConcert R2.*2
    r4 r si' |
    mi''2 red''8 mi'' |
    fad''2\trill mi''8 fad'' |
    sol''4 \appoggiatura fad''8 mi''4 si'8 la' |
    do''2 r8 si' |
    si'4 mi''4. fad''8 |
    red''4\trill fad''4. fad''8 |
    si'2. si'4. la'8 sol' la' |
    fad'4.\trill re''8 re'' sol'' |
    fad''4. re''8 do'' si' |
    si'4( la'2\trill) |
    sol'4 r r |
    r si'4. re''8 |
    dod''4 fad''8.[\melisma sol''16 fad''8. sol''16] |
    fad''16[ sol'' fad'' sol''] fad''4\trill~ fad''16[ sol'' fad'' mi''] |
    red''[ mi'' red'' dod''] si'[ dod'' si' dod''] red''[ mi'' red'' mi'']( |
    fad''2)\melismaEnd si'4 |
    do''8 si' la'4. sol'8 |
    fad'4~ fad'8 r16 si'[\melisma dod'' red'' mi'' fad''] |
    sol''2~ sol''8\melismaEnd sol'' |
    sol''2. |
    r4 r8 r16 la'[ si' do'' re'' mi'']( |
    fad''4.) fad''8 fad''4 |
    red''4. mi''8 fad'' sol'' |
    fad''2. |
    red''2 mi''8 fad'' |
    sol''2 sol''4 |
    red''8 mi'' mi''4.( red''8) |
    mi''2. |
    mi''2 mi''8 mi'' |
    mi''2 red''4 |
    fad''8 sol'' sol''4( fad'')\trill |
    mi''2. |
  }
>>
