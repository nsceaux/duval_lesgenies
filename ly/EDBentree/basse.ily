\clef "basse" si,2 r4 |
R2. |
r4 r fad8 fad |
lad4 lad fad |
si si la |
sol4. fad8 mi4 |
red2. |
mi2 si8 la sol4 sol2 |
fad4 fad, fad8 fad |
lad4 lad fad |
si si, r |
fad2 fad8 fad |
si4 si, si8 la |
sol4. sol8 fad4 |
mi8 si, sol,4 si, |
mi mi, mi8 mi |
la4 la, la8 sol |
fad4. fad8 mi4 |
re8 la, fad,4 la, |
re re, fad |
sol mi la |
re2 re'8 re' |
dod'4 fad'8 mi' re' dod' |
re'4 si si8 si |
si4 lad fad |
si la sol |
fad fad, fad8 fad |
mi4 fad8 mi re dod |
re4 mi8 re dod si, |
sol4 fad8 mi re4 |
mi fad fad, |
si,2
