\clef "dessus" r4 r si'8 si' |
re''4 re'' si' |
fad'' fad' fad''8 fad'' |
mi''4 fad''8 mi'' re'' dod'' |
re''4 si' red'' |
mi''4. red''8 mi''4 |
fad''4. mi''8 fad''4 |
sol''4. la''8 fad''4~ |
fad''8 sol'' mi''4.\trill fad''8 |
fad''2 r4 |
R2. |
r4 r si'8 si' |
fad''2 fad''8 fad'' |
red''4 red'' si' |
mi''4 mi'8 mi'' fad''4 |
sol'' mi'' si' |
mi'2 mi''8 mi'' |
dod''4 dod'' la' |
re'' re'8 re'' mi''4 |
fad'' re'' la' |
re'2 re''8 re'' |
mi'' re'' mi'' fad'' sol'' mi'' |
fad''4 fad' fad''8 fad'' |
lad''4 lad'' fad'' |
si'' si' re''8 re'' |
dod''4 fad''8 mi'' re'' dod'' |
re''4 dod'' si' |
lad' fad''4. fad''8 |
fad''2.~ |
fad''8 mi'' sol'' fad'' mi'' re'' |
dod''4. si'16 lad' si'4 |
dod'' dod''4. si'8 |
si'2
