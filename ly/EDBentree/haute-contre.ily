\clef "haute-contre" R2.*2 |
r4 r dod''8 dod'' |
dod''4 re''8 dod'' si' lad' |
si'2 si'4 |
si'2 si'4 |
si'2 si'4 |
si'4. dod''8 si'4~ |
si' si'2 |
lad'\trill r4 |
R2.*2 |
lad'2\trill lad'8 lad' |
si'4 si' si' |
si'4. sol'8 la'4 |
sol' si'8 la' sol' fad' |
sol'2 sol'8 sol' |
la'4 la' la' |
la'4. fad'8 sol'4 |
fad' la'8 sol' fad' mi' |
fad'2 la'8 la' |
sol' fad' sol' fad' mi' dod' |
re'2 si'8 si' |
dod''4 dod'' si'8 lad' |
si'4 fad' si'8 si' |
dod''4. dod''8 si' lad' |
si'4 dod'' mi' |
fad'2 lad'8 lad' |
lad'4. dod''8 si' lad' |
si'2 lad'4 |
lad'2 si'4 |
si'2 lad'4\trill |
si'2
