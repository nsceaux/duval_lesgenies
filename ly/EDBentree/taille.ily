\clef "taille" R2.*2 |
r4 r lad8 lad |
lad4 fad' fad' |
fad'2 fad'4 |
sol'4. la'8 sol'4 |
fad'2. |
mi'4 fad'8 mi' re' dod' |
re'4 dod'2 |
dod'2 r4 |
R2.*2 |
dod'2 fad'8 fad' |
fad'4 fad' red' |
mi'2 red'4 |
mi' mi' red' |
mi'2 mi'8 mi' |
mi'4 mi' dod' |
re'2 dod'4 |
re' re' dod' |
re'2 re'4 |
si2 la4 |
la2 fad'8 fad' |
mi'4 fad' fad' |
fad' re' fad'8 fad' |
mi'4 fad' fad' |
fad'2 si4 |
dod'2 dod'8 dod' |
dod'2 fad'8 fad' |
fad' sol' mi' fad' sol'4 |
mi' fad'8 sol' fad'4 |
sol' fad' mi' |
re'2
