\clef "basse" r4 |
R2. |
r4 r fad |
si2 si8 si |
la4 si8 la sol fad |
sol4 sol mi |
fad2.~ |
fad |
fad,4. fad8 fad fad |
si4. si8 si si |
dod'2 dod'8 dod' |
re'4 re'8 dod' re' si |
mi'4 mi' mi8 fad |
sol4 sol8 fad sol mi |
fad4 fad,2 |
si,2 r4 |
r4 r si8 si |
mi'4 mi mi'8 re' |
dod' re' dod' si dod' la |
re'4 re' re8 re |
mi4 mi, mi8 fad |
sol4 sol mi |
fad2. |
fad4 fad, fad |
si2 si8 si |
sold4 sold mi |
la2 la8 sol |
fad sol fad mi re mi |
dod4 dod la |
re'2 re8 re |
mi4 mi mi |
fad2 fad8 re |
sol2 mi4 |
si dod' re' |
la4 la,2 |
re2 \clef "alto" re'8 re' |
la'4 sol' fad' |
mi'2 \clef "bass" mi4 |
sol2 mi4 |
si2 si8 dod' |
re'4 dod' si |
mi2 mi8 fad |
sol4 mi8 fad sol mi |
fad2.~ |
fad~ |
fad~ |
fad~ |
fad~ |
fad8 mi fad sol la fad |
si4 si si8 si |
la4 si8 la sol fad |
sol2 sol8 sol |
fad4 si si, |
mi2 mi8 mi |
fad2 fad8 fad |
sol2 mi4 |
la la fad |
si si si |
si2.~ |
si~ |
si~ |
si2 si8 si |
la2 la4 |
sol fad mi |
si4 si,2 |
mi2 \clef "alto" mi'4 |
la'2 la4 |
re'2 \clef "bass" re8 re |
mi4 sol mi |
fad2 fad8 fad |
lad,4 lad,4. lad,8 |
si,4 si, si |
mi'2 mi4 |
la2 la8 fad |
si2 si4 |
la sold fad |
dod'2 dod'4 |
dod dod4. dod8 |
fad4. fad8 fad fad |
sold2 sold8 sold |
la4 la8 si dod' la |
re'4. re'8 re'4 |
si dod' dod |
fad4. <>^"Bassons" fad8 mi re |
dod4 fad fad, |
si, si8 la sol fad |
sol2 mi4 |
fad2 <>^"[Basses, Bassons et B.C.]" fad4 |
si2 r4 |
r r si |
mi'2 mi4 |
la2 r4 |
r r la |
re'2 \clef "alto" re'8 re' |
la'4 sol' fad' |
mi'2 \clef "bass" mi8 fad |
sol4 sol mi |
si2 r4 |
si,2 si,4 |
mi,2 mi8 fad |
sol4 sol mi |
fad r r |
r4 r8 si, si, si, |
fad4. fad8 fad fad |
si2 si8 si |
dod'2. |
re'8 dod' si re' dod' si |
lad sold fad sold lad fad |
si lad si re' dod' mi' |
re' dod' si la sol fad |
mi re mi fad sol mi |
fad4 fad8 fad fad fad |
si4. si8 si si |
re'4 re'4. re'8 |
re'2. |
lad4. si8 sol4 |
mi fad2 |
si,2. |
