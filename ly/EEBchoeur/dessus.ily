\clef "dessus" si'4 |
fad''2 fad''8 fad'' |
mi''4 fad''8 mi'' re'' dod'' |
re''2 fad''8 fad'' |
fad'' mi'' re'' dod'' si' la' |
sol'4 sol' sol' |
fad'4. fad'8 fad' fad' |
si'4. si'8 si' si' |
dod''2 dod''8 dod'' |
re''4 re''8 dod'' re'' si' |
mi'' fad'' mi'' re'' mi'' dod'' |
fad'' sol'' fad'' mi'' fad'' re'' |
sol'' la'' sol'' fad'' mi'' re'' |
mi''4. re''8 dod''4 |
re'' re''( dod'')\trill |
si'2 fad''8 fad'' |
si''4 si' si''8 si'' |
sold'' la'' sold'' fad'' sold'' mi'' |
la''4 la' la''8 la'' |
fad''8 sol'' fad'' mi'' fad'' re'' |
sol'' la'' sol'' fad'' mi'' re'' |
dod'' re'' mi'' re'' dod'' si' |
lad'4\trill fad' dod'' |
fad''2 fad''8 fad'' red''4 red'' si' |
mi''2 sold''8 sold'' |
la''2 la''4 |
la''2.~ |
la''\trill~ |
la''4.\trill la''8 sol'' fad'' |
mi''2\trill sol''8 mi'' |
la''2 la'8 la' |
si'4 sol'8 la' si' dod'' |
re''4. mi''8 fad''4~ |
fad''8 sol'' fad''4( mi'')\trill |
re''2 \twoVoices #'(dessus1 dessus2 dessus) <<
  { fad''8 fad'' | mi''4\trill mi'' fad'' | sol''2 }
  { re''8 re'' | dod''4\trill dod'' red'' | mi''2 }
>> \tag #'dessus <>^"[unis]" mi''8 mi'' |
si'4 si' dod'' |
re''2 re''8 mi'' |
fad''2 fad''8 re'' |
sol''2 r8 fad'' |
mi'' fad'' mi'' re'' dod'' si' |
lad'4.\trill fad'8 fad' fad' |
si'4. si'8 lad' si' |
dod''2 si'8 dod'' |
re''4 si'8 dod'' re'' mi'' |
fad'' sol'' fad'' mi'' fad'' re'' |
mi'' fad'' mi'' re'' mi'' dod'' |
re''4 si' si''8 si'' |
si''4 si'' si'' |
si''2 si''8 si'' |
la''4 sol'' fad'' |
sol''2 do''8 do'' |
do''2 do''8 re'' |
si'2 mi''4 |
dod''4 red'' mi'' |
red''2\trill si'4 |
si'2.~ |
si'4 si' si' |
si'2.~ |
si'2 si'8 si' |
fad''2 red''4 |
mi'' fad'' sol'' |
sol''( fad''2)\trill |
mi''2 \twoVoices #'(dessus1 dessus2 dessus) <<
  { sol''4 | sol''( fad'')\trill mi'' | fad''2 }
  { mi''4 | mi''( re'')\trill dod'' | re''2 }
>> \tag #'dessus <>^"[unis]" fad''8 fad'' |
sol'' fad'' mi'' re'' dod'' si' |
lad'2\trill dod''4 |
fad''2 dod''4 |
re''2 re''8 re'' |
si'4 si'4. mi''8 |
dod''2 dod''8 fad'' |
red''2\trill red''4 |
red'' mid'' fad'' |
mid''2 \appoggiatura red''8 dod''4 |
si' la' sold' |
la'4. la'8 la' la' |
si'2 si'8 si' |
dod''4 dod''8 red'' mid'' dod'' |
fad''4. fad''8 fad''4 |
fad'' fad''4. mid''8 |
fad''2 <>^"Hautbois" \twoVoices #'(dessus1 dessus2 dessus) <<
  { dod''8 re'' |
    mi''4 re'' dod'' |
    re'' mi'' fad'' |
    fad'' mi'' re'' |
    dod''4\trill fad' }
  { lad'8 si' |
    dod''4 si' lad' |
    si' dod'' re'' |
    re'' dod'' si' |
    lad'\trill fad' }
>> <>^"[Tous]" fad'' |
fad''2 r4 |
r r fad'' |
mi''2 mi''4 |
mi''2 r4 |
r r mi'' |
re''2 \twoVoices #'(dessus1 dessus2 dessus) <<
  { fad''8 fad'' | mi''4 mi'' fad'' | sol''2 }
  { re''8 re'' | dod''4 dod'' red'' | mi''2 }
>> \tag #'dessus <>^"[unis]" mi''8 mi'' |
si'4 si' dod'' |
re''2 re''8 mi'' |
fad''2 fad''8 re'' |
sol''2 r8 fad'' |
mi'' fad'' mi'' re'' dod'' si' |
lad'4.\trill fad'8 fad' fad' |
si'4. si'8 si' si' |
dod''2 dod''8 dod'' |
re''4 si'8 dod'' re'' si' |
mi'' re'' dod'' re'' mi'' dod'' |
fad''4. fad''8 mi'' re'' |
dod'' si' lad' si' dod'' lad' |
si'4 si' mi'' |
re''8 mi'' re'' dod'' si'4~ |
si' dod''8 re'' dod'' si' |
lad'4 fad' fad''8 mi'' |
re''4 si'8 dod'' re'' mi'' |
fad'' mi'' fad'' sol'' mi'' sol'' |
fad'' mi'' fad'' sol'' mi'' sol'' |
fad''4. fad''8 mi''4 |
re'' re''( dod'')\trill |
si'2. |
