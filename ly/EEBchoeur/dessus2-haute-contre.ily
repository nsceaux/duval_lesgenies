\clef "haute-contre" r4 |
R2. |
r4 r lad' |
si'2 re''8 re'' |
dod''4 dod'' re'' |
si' si' dod'' |
lad'2\trill dod'4 |
re'2 re'4 |
dod'4. la'8 la' la' |
si'4. si'8 si' si' |
si'4( lad') lad'8 lad' |
si'4 si' si' |
si'8 dod'' si' la' sol' fad' |
mi'4. fad'8 mi'4 |
re' fad' mi' |
re'2 r4 |
r r red''8 red'' |
mi'' fad'' mi'' re'' mi'' mi'' |
mi''2 la'4 |
la'8 si' la' sol' fad'4~ |
fad' mi'8 fad' sol'4~ |
sol'2 sol'4 |
fad'2. |
r4 r lad' |
si'2 si'4 |
si'2 mi''8 mi'' |
dod''2 dod''4 |
re'' re'' re'' |
mi''2 mi''4 |
fad''4. fad''8 mi'' re'' |
dod''4\trill sol' sol' |
fad'2 fad'8 fad' |
sol'4 sol'8 fad' sol' la' |
si'4. la'8 la'4~ |
la'8 si' la'4 sol' |
fad'2\trill re''8 re'' |
dod''4\trill dod'' red'' |
mi''2 si'4 |
si'2 si'4 |
si'2 si'4 |
si' dod'' re'' |
mi''2 r8 la' |
sol' la' sol' fad' mi' re' |
dod'4.\trill fad'8 fad' fad' |
re'4. fad'8 fad' si' |
lad'2\trill si'8 lad' |
si'4 si' si' |
lad'2\trill \appoggiatura sol'8 fad'4 |
dod''8 re'' dod'' si' dod'' lad' |
si'4 si' si'8 dod'' |
red''4 red'' red'' |
mi''2 mi''8 mi'' |
dod''4 si' si' |
si'2 sol'8 sol' |
la'2 la'8 la' |
si'2 sol'4 |
la'4 si' dod'' |
si' si' sol' |
la' sol' fad' |
sol' la' si' |
si' la' sol' |
fad'2\trill fad'8 fad' |
fad'2 si'4 |
sol' la' si' |
si'( la'2) |
sol'2 mi'4 |
la'2 la4 |
re'2 re''8 re'' |
re''4 dod'' dod'' |
dod''2 fad'4 |
fad'2 fad'4 |
fad'2 si'4 |
si'2 sold'4 |
la'2 la'8 la' |
fad'2 si'4 |
si' si' la' |
sold'2 sold'4 |
sold' fad' mid' |
fad'4. fad'8 fad' fad' |
mid'2\trill sold'8 sold' |
fad'4 fad' la' |
la'4. la'8 la'4 |
sold' la'4. sold'8 |
fad'2 r4 |
R2.*3 |
r4 r mi'' |
red''2\trill r4 |
r4 r si' |
sold'2\trill sold'4 |
la'2 r4 |
r r la' |
la'2 re''8 re'' |
dod''4 dod'' red'' |
mi''2 si'8 si' |
si'4 si' si' |
si'2 r4 |
r r si'8 si' |
si'2 r8 la' |
sol' la' sol' fad' mi' re' |
dod'4.\trill dod'8 dod' dod' |
re'4. fad'8 fad' si' |
lad'2\trill lad'8 lad' |
si'4 si' si'~ |
si' lad' lad' |
si' fad' fad' |
fad'2. |
re''8 dod'' si'4 dod''8 lad' |
si'2 fad'4 |
sol'2 sol'4 |
fad'4. lad'8 lad' lad' |
si'4. si'8 si' si' |
si'2 si'8 si' |
si'2. |
dod''4. si'8 si'4 |
si' si'( lad')\trill |
si'2. |
