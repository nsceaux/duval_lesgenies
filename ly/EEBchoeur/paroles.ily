\tag #'vdessus {
  Chan -- tons, ne son -- geons qu’aux plai -- sirs,
  pro -- fi -- tons de l’â -- ge de grâ -- ces,
  pour mieux ré -- pondre à nos dé -- sirs,
  les a -- mours vo -- lent sur nos tra -- ces.

  Chan -- tons, ne son -- geons qu’aux plai -- sirs,
  pro -- fi -- tons de l’â -- ge de grâ -- ces,
  pour mieux ré -- pondre à nos dé -- sirs,
  les a -- mours, les a -- mours vo -- lent sur nos tra -- ces,
  ne son -- geons qu’aux plai -- sirs,
  ne son -- geons qu’aux plai -- sirs,
  pro -- fi -- tons, pro -- fi -- tons de l’â -- ge de grâ -- ces,
  pour mieux ré -- pondre à nos dé -- sirs,
  les a -- mours vo -- lent,
  ne son -- geons qu’aux plai -- sirs,
  ne son -- geons qu’aux plai -- sirs,
  pro -- fi -- tons, pro -- fi -- tons de l’â -- ge de grâ -- ces,
  chan -- tons, chan -- tons, ne son -- geons qu’aux plai -- sirs,
  pro -- fi -- tons de l’â -- ge de grâ -- ces,
  chan -- tons, chan -- tons, ne son -- geons qu’aux plai -- sirs,
  chan -- tons, chan -- tons, ne son -- geons qu’aux plai -- sirs,
  pro -- fi -- tons de l’â -- ge de grâ -- ces,
  pour mieux ré -- pondre à nos dé -- sirs,
  les a -- mours vo -- lent sur nos tra -- ces.

  Chan -- tons, ne son -- geons qu’aux plai -- sirs,
  pro -- fi -- tons de l’â -- ge de grâ -- ces,
  ne son -- geons qu’aux plai -- sirs,
  ne son -- geons qu’aux plai -- sirs,
  pro -- fi -- tons, pro -- fi -- tons de l’â -- ge de grâ -- ces,
  pour mieux ré -- pondre à nos dé -- sirs,
  les a -- mours vo -- lent,
  les a -- mours vo -- lent sur nos tra -- ces.
}
\tag #'vhaute-contre {
  Chan -- tons, ne son -- geons qu’aux plai -- sirs,
  qu’aux plai -- sirs,
  chan -- tons, chan -- tons,
  pour mieux ré -- pondre à nos dé -- sirs,
  les a -- mours vo -- lent, vo -- lent sur nos tra -- ces.

  \ifComplet {
    Chan -- tons, pro -- fi -- tons des plai -- sirs,
    chan -- tons pro -- fi -- tons des plai -- sirs,
    chan -- tons,
  }
  \ifConcert {
    Chan -- tons, chan -- tons,
    pro -- fi -- tons de l’â -- ge de grâ -- ces,
    chan -- tons, chan -- tons,
  }
  les a -- mours vo -- lent sur nos tra -- ces,

  ne son -- geons qu’aux plai -- sirs,

  chan -- tons, chan -- tons,
  \ifConcert { pro -- fi -- tons } de l’â -- ge de grâ -- ces,
  pour mieux ré -- pondre à nos dé -- sirs,

  les a -- mours vo -- lent, vo -- lent, vo -- lent,
  \ifComplet { vo -- lent, }
  ne son -- geons qu’aux plai -- sirs,
  ne son -- geons qu’aux plai -- sirs,
  pro -- fi -- tons, pro -- fi -- tons de l’â -- ge de grâ -- ces,
  chan -- tons, chan -- tons, ne son -- geons qu’aux plai -- sirs,
  pro -- fi -- tons de l’â -- ge de grâ -- ces,

  chan -- tons, chan -- tons,

  ne son -- geons qu’aux plai -- sirs,
  chan -- tons, chan -- tons,
  pro -- fi -- tons de l’â -- ge de grâ -- ces,
  pour mieux ré -- pondre à nos dé -- sirs,
  les a -- mours vo -- lent,
  vo -- lent sur nos tra -- ces.
  
  Chan -- tons, ne son -- geons qu’aux plai -- sirs,
  chan -- tons, chan -- tons,

  ne son -- geons qu’aux plai -- sirs,

  ne son -- geons qu’aux plai -- sirs,
  pro -- fi -- tons de l’â -- ge de grâ -- ces,
  pour mieux ré -- pondre à nos dé -- sirs,
  les a -- mours __
  \ifComplet { les a -- mours }
  \ifConcert { vo -- lent, vo -- lent, vo -- lent, }
  vo -- lent, vo -- lent, vo -- lent,
  pour mieux ré -- pondre à nos dé -- sirs,
  les a -- mours \ifComplet { vo -- lent, } vo -- lent sur nos tra -- ces.
}

\tag #'vtaille {
  Chan -- tons, ne son -- geons qu’aux plai -- sirs,
  qu’aux plai -- sirs,
  chan -- tons, chan -- tons,
  pour mieux ré -- pondre à nos dé -- sirs,
  les a -- mours vo -- lent, vo -- lent,
  les a -- mours vo -- lent sur nos tra -- ces.

  \ifComplet {
    Chan -- tons, chan -- tons,
    pro -- fi -- tons,
    chan -- tons, chan -- tons, chan -- tons, chan -- tons,
  }
  \ifConcert {
    Chan -- tons, ne son -- geons qu’aux plai -- sirs,
    chan -- tons, chan -- tons, chan -- tons, chan -- tons,
  }
  pro -- fi -- tons, pro -- fi -- tons de l’â -- ge de grâ -- ces,

  \ifConcert { ne son -- geons qu’aux plai -- sirs, }

  chan -- tons, chan -- tons,
  \ifComplet { pro -- fi -- tons, }
  les a -- mours vo -- lent, vo -- lent,
  les a -- mours vo -- lent, vo -- lent,

  les a -- mours vo -- lent, vo -- lent, vo -- lent,
  \ifConcert { vo -- lent, }
  ne son -- geons qu’aux plai -- sirs,
  ne son -- geons qu’aux plai -- sirs,
  pro -- fi -- tons, pro -- fi -- tons de l’â -- ge de grâ -- ces,
  chan -- tons, chan -- tons,
  ne son -- geons qu’aux plai -- sirs,
  pro -- fi -- tons de l’â -- ge de grâ -- ces,

  \ifConcert { chan -- tons, chan -- tons, }
  
  ne son -- geons qu’aux plai -- sirs.
  
  Chan -- tons, chan -- tons, chan -- tons,
  pro -- fi -- tons de l’â -- ge de grâ -- ces,
  pour mieux ré -- pondre à nos dé -- sirs,
  les a -- mours vo -- lent,
  vo -- lent sur nos tra -- ces.
  
  Chan -- tons, chan -- tons,
  pro -- fi -- tons de l’â -- ge de grâ -- ces,

  \ifConcert { ne son -- geons qu’aux plai -- sirs, }

  ne son -- geons qu’aux plai -- sirs,
  pro -- fi -- tons,
  ne son -- geons qu’aux plai -- sirs,
  pour mieux ré -- pondre à nos dé -- sirs,
  à nos dé -- sirs,
  les a -- mours vo -- lent,
  vo -- lent, vo -- lent, \ifComplet { vo -- lent, }
  pour mieux ré -- pondre à nos dé -- sirs,
  les a -- mours \ifComplet { vo -- lent, } vo -- lent sur nos tra -- ces.  
}
\tag #'vbasse {
  Chan -- tons, ne son -- geons qu’aux plai -- sirs,
  qu’aux plai -- sirs,
  pour mieux ré -- pondre à nos dé -- sirs,
  les a -- mours vo -- lent,
  les a -- mours vo -- lent sur nos tra -- ces.

  Chan -- tons, ne son -- geons qu’aux plai -- sirs,
  Chan -- tons, ne son -- geons qu’aux plai -- sirs,
  pro -- fi -- tons de l’â -- ge de grâ -- ces,
  chan -- tons, chan -- tons,
  les a -- mours vo -- lent,
  ne son -- geons qu’aux plai -- sirs,
  ne son -- geons qu’aux plai -- sirs,
  pro -- fi -- tons, pro -- fi -- tons de l’â -- ge de grâ -- ces,
  chan -- tons, __
  pro -- fi -- tons de l’â -- ge de grâ -- ces,
  ne son -- geons qu’aux plai -- sirs.

  Chan -- tons, chan -- tons,
  pro -- fi -- tons de l’â -- ge de grâ -- ces,
  pour mieux ré -- pondre à nos dé -- sirs,
  les a -- mours vo -- lent sur nos tra -- ces.

  Chan -- tons, chan -- tons, chan -- tons,
  chan -- tons, ne son -- geons qu’aux plai -- sirs,
  ne son -- geons qu’aux plai -- sirs,
  pour mieux ré -- pondre à nos dé -- sirs,
  les a -- mours vo -- lent,
  pour mieux ré -- pondre à nos dé -- sirs,
  les a -- mours vo -- lent sur nos tra -- ces.  
}
