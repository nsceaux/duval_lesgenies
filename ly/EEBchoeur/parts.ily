\piecePartSpecs
#`((dessus #:instrument ,#{\markup\center-column { [Violons et Hautbois] }#})
   (dessus1 #:instrument ,#{\markup\center-column { [Violons et Hautbois] }#})
   (dessus2 #:instrument ,#{\markup\center-column { [Violons et Hautbois] }#})
   (parties)
   (dessus2-haute-contre #:notes "dessus2-haute-contre")
   (taille)
   (basse #:instrument
          ,#{ \markup\center-column { [Basses, Bassons et B.C.] } #})

   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
