\clef "taille" r4 |
R2. |
r4 r fad' |
fad'2 si'8 si' |
dod''4 fad' fad' |
fad' mi' mi' |
\appoggiatura re'8 dod'2 lad4 |
si2 si4 |
la!4. fad'8 fad' fad' |
fad'4. fad'8 fad' fad' |
mi'2 mi'8 mi' |
re'4 fad' fad' |
mi'2 si8 si |
si4 si8 la si4 |
si( lad2)\trill |
si2 r4 |
r r fad'8 fad' |
mi'4 si mi' |
mi'8 fad' mi' re' mi' dod' |
re'2 re'4~ |
re' mi' si |
si2 mi'8 re' |
dod'2. |
r4 r fad' |
fad'2 red'8 red' |
mi'4 si' si' |
dod''2 dod''8 si' |
la' si' la' sol' fad' mi' |
dod'2\trill dod''4 |
re'' la' la' |
sol' dod' dod' |
re'2 re'8 re' |
re'2 dod'4 |
si mi' re' |
re'( dod'2)\trill |
re'2 re'8 re' |
la'4 sol' fad' |
mi'2 sol'4 |
mi'2 sol'4 |
fad'2 fad'4 |
fad'2 si4 |
si2 si8 si |
si4 si si |
dod' dod' lad8 lad |
si4 re' re' |
dod' dod' re'8 dod' |
si4 re' re' |
dod'2\trill re'4 |
r fad' fad' |
fad' fad' red'8 mi' |
fad'4 fad' fad' |
mi'2 mi'8 mi' |
mi'4 mi' red' |
mi'2 mi'8 mi' |
re'!2 re'8 re' |
re'2 re'4 |
dod'4 si la |
si si si |
red' mi' red' |
mi' fad' sol' |
red' red' mi' |
red'2\trill red'8 red' |
red'2 fad'4 |
mi' red' mi' |
mi'2( red'4)\trill |
mi'2 mi'4 |
la'2 la4 |
re'2 fad'8 fad' |
fad' la' sol' fad' mi' re' |
dod'2\trill dod'4 |
dod'2 fad'4 |
fad'2 fad'4 |
sold'2 mi'4 |
mi'2 dod'8 dod' |
si2 fad'4 |
fad' mid' dod' |
dod'2 dod'4 |
mid' fad' dod' |
dod'4. dod'8 dod' dod' |
si2 si8 si |
la4 dod' dod' |
re'4. re'8 re'4 |
re' dod'2 |
lad2\trill r4 |
R2.*3 |
r4 r dod' |
si2 r4 |
r4 r red' |
mi'2 si4 |
dod'2 r4 |
r r sol' |
fad'2\trill re'8 re' |
la'4 sol' fad' |
mi'2 sol'8 fad' |
mi'4 mi' sol' |
fad'2 r4 |
r r re'8 fad' |
mi'2 si8 si |
si4 si dod' |
dod'4. lad8 lad lad |
si4. re'8 re' re' |
dod'4. dod'8 dod' fad' |
fad'8 mi' re' mi' fad' re' |
mi' fad' mi' fad' sol' mi' |
fad'2 fad'4 |
fad'2.~ |
fad'2 fad'4 |
fad' re'8 mi' fad'4~ |
fad' mi' mi' |
dod'4. dod'8 dod' dod' |
re'4. fad'8 fad' fad' |
fad'2 sol'8 mi' |
fad'2. |
fad'4. fad'8 sol'4 |
sol' fad'2 |
re'2. |
