<<
  \tag #'vdessus {
    \clef "vdessus" si'4 |
    fad''2 fad''8 fad'' |
    mi''4 fad''8[ mi''] re''[ dod''] |
    re''2 re''8 re'' |
    dod''2 re''4 |
    si' si' dod'' |
    lad' lad'8 fad' fad' fad' |
    si'4. si'8 si' si' |
    dod''2 dod''8 dod'' |
    re''4 re''8[\melisma dod'' re'' si'] |
    mi''[ fad'' mi'' re'' mi'' dod''] |
    fad''[ sol'' fad'' mi'' fad'' re''] |
    sol''[ la'' sol'' fad'' mi'' re'']( |
    mi''4.)\melismaEnd re''8 dod''4 |
    re''4 re''( dod'')\trill |
    si'2 r4 |
    R2.*6 |
    r4 r dod'' |
    fad''2 fad''8 fad'' |
    red''4 red'' si' |
    mi''2 mi''8 mi'' |
    dod''2 la'4 |
    re'' re'' re'' |
    mi'' mi''8 mi'' mi'' mi'' |
    fad''4. fad''8 fad'' fad'' |
    sol''2 sol''8 mi'' |
    la''2 la'8 la' |
    si'4 sol'8[ la' si' dod'']( |
    re''4.) mi''8 fad''4~ |
    fad''8 sol'' fad''4( mi'')\trill |
    re''2 <<
      \ifComplet <<
        { \voiceOne fad''8 fad'' |
          mi''4\trill mi'' fad'' |
          sol''2 \oneVoice }
        \new Voice \with { autoBeaming = ##f } {
          \voiceTwo re''8 re'' |
          dod''4\trill dod'' red'' |
          mi''2
        }
      >>
      \ifConcert {
        fad''8 fad'' |
        mi''4\trill mi'' fad'' |
        sol''2
      }
    >> mi''8 mi'' |
    si'4 si' dod'' |
    re''2 re''8 mi'' |
    fad''2 fad''8 re'' |
    sol''2 r8 fad'' |
    mi''[ fad''] mi''[ re''] dod''[ si'] |
    lad'4\trill lad'8 fad' fad' fad' |
    si'4. si'8 lad' si' |
    dod''2 si'8 dod'' |
    re''4 si'8[\melisma dod'' re'' mi''] |
    fad''[ sol'' fad'' mi'' fad'' re''] |
    mi''[ fad'' mi'' re'' mi'' dod'']( |
    re''4)\melismaEnd si' si'8 dod'' |
    red''4 red'' si' |
    mi''2 mi''8 mi'' |
    la''4 sol'' fad'' |
    sol''2 do''8 do'' |
    do''2 do''8 re'' |
    si'2 mi''4 |
    dod''4 red'' mi'' |
    red'' red'' si' |
    la'( sol') fad' |
    sol' la' si' |
    si' la' sol' |
    fad'2 si'8 si' |
    fad''2 red''4 |
    mi'' fad'' sol'' |
    sol''( fad''2)\trill |
    mi''2 <<
      \ifComplet <<
        { \voiceOne sol''4 |
          sol''( fad'')\trill mi'' |
          fad''2 \oneVoice }
        \new Voice \with { autoBeaming = ##f } {
          \voiceTwo mi''4 |
          mi''( re'')\trill dod'' |
          re''2
        }
      >>
      \ifConcert {
        sol''4 |
        sol''( fad'')\trill mi'' |
        fad''2
      }
    >> fad''8 fad'' |
    sol''[ fad''] mi''[ re''] dod''[ si'] |
    lad'2\trill dod''4 |
    fad''2 dod''4 |
    re''2 re''8 re'' |
    si'4 si'4. mi''8 |
    dod''2 dod''8 fad'' |
    red''2\trill red''4 |
    red'' mid'' fad'' |
    mid''2 \appoggiatura red''8 dod''4 |
    si'4 la' sold' |
    la'4. la'8 la' la' |
    si'2 si'8 si' |
    dod''4 dod''8[ red'' mid'' dod'']( |
    fad''4.) fad''8 fad''4 |
    fad''4 fad''4.( mid''8) |
    fad''2 r4 |
    R2.*3 |
    r4 r fad'' |
    red''2\trill red''8 mi'' |
    fad''4 mi'' red'' |
    mi''2 mi''8 mi'' |
    dod''2 re''4 |
    mi'' re'' dod'' |
    re'' re'' <<
      \ifComplet <<
        { \voiceOne fad''8 fad'' |
          mi''4 mi'' fad'' |
          sol''2 \oneVoice }
        \new Voice \with { autoBeaming = ##f } {
          \voiceTwo re''8 re'' |
          dod''4 dod'' red'' |
          mi''2
        }
      >>
      \ifConcert {
        fad''8 fad'' |
        mi''4 mi'' fad'' |
        sol''2
      }
    >> mi''8 mi'' |
    si'4 si' dod'' |
    re''2 re''8 mi'' |
    fad''2 fad''8 re'' |
    sol''2 r8 fad'' |
    mi''[ fad''] mi''[ re''] dod''[ si'] |
    lad'4\trill lad'8 fad' fad' fad' |
    si'4. si'8 si' si' |
    dod''2 dod''8 dod'' |
    re''4 si'8[\melisma dod'' re'' si'] |
    mi''[ re'' dod'' re'' mi'' dod''] |
    fad''2.~ |
    fad''~ |
    fad''~ |
    fad''~ |
    fad''4 mi''8[ re'' dod'' si']( |
    lad'4)\melismaEnd fad' fad''8 mi'' |
    re''4 si'8[\melisma dod'' re'' mi''] |
    fad''[ mi'' fad'' sol'' mi'' sol''] |
    fad''[ mi'' fad'' sol'' mi'' sol'']( |
    fad''4.)\melismaEnd fad''8 mi''4 |
    re'' re''( dod'')\trill |
    si'2. |
  }
  \tag #'vhaute-contre {
    \ifComplet\clef "vhaute-contre"
    \ifConcert\clef "petrucci-c3/treble"
    r4 |
    R2. |
    r4 r <<
      \ifComplet {
        fad'4 |
        fad'2 fad'8 fad' |
        fad'4 fad' fad' |
        fad'
      }
      \ifConcert {
        lad'4 |
        si'2 si'8 si' |
        dod''4 fad' fad' |
        fad'
      }
    >> mi'4 mi' |
    \appoggiatura re'8 dod'2 dod'4 |
    re'2 re'4 |
    dod'4. la'8 la' la' |
    si'4. si'8 si' si' |
    si'4( lad') lad'8 lad' |
    si'4 si' si' |
    si'8[ dod'' si' la' sol' fad']( |
    mi'4.) fad'8 mi'4 |
    re' fad'( mi') |
    re'2 r4 |
    R2.*7 |
    r4 r <<
      \ifComplet {
        fad'4 |
        fad'2 red'8 red' |
        mi'4 mi' mi' |
        mi'2 la'4 |
        la' la' la' |
        la' la' la' |
        la'2 la'4 |
        sol'2 r4 |
      }
      \ifConcert {
        lad'4 |
        si'2 si'4 |
        si'2 sold'8 sold' |
        la'2 la'4 |
        la' la' la' |
        la' la' dod'' |
        re''2 re''4 |
        dod''2.\trill |
      }
    >>
    r4 r fad'8 fad' |
    sol'4 sol'8[ fad' sol' la']( |
    si'4.) la'8 la'4~ |
    la'8 si' la'4( sol') |
    fad'2\trill
    <<
      \ifComplet {
        re'8 re' |
        la'4 sol' fad' |
        mi'2
      }
      \ifConcert {
        re''8 re'' |
        dod''4\trill dod'' red'' |
        mi''2
      }
    >> sol'4 |
    mi'2 sol'4 |
    fad'2 r4 |
    <<
      \ifComplet { R2. | r4 r }
      \ifConcert {
        r4 r si'8 si' |
        si'2
      }
    >> r8 la' |
    sol'[ la'] sol'[ fad'] mi'[ re'] |
    dod'4 dod'8 fad' fad' fad' |
    re'4. fad'8 fad' si' |
    lad'2\trill <<
      \ifComplet {
        re'8 dod' |
        si4 re' re' |
        dod'2\trill re'4 |
        r4 fad' fad' |
        fad' fad' red'8 mi' |
        fad'4 fad' fad' |
        mi'2 mi'8 mi' |
        mi'4 mi' red' |
        mi'2 mi'8 mi' |
        re'!2 re'8 re' |
        re'2 re'4 |
        dod'4 si la |
        si si
      }
      \ifConcert {
        si'8 lad' |
        si'4 si'4 si' |
        lad'2\trill \appoggiatura sol'8 fad'4 |
        dod''8[ re'' dod'' si' dod'' lad']( |
        si'4) si' si'8 si' |
        si'4 si' si' |
        si'2 si'8 si' |
        dod''4 si' si' |
        si'2 sol'8 sol' |
        la'2 la'8 la' |
        si'2 sol'4 |
        la'4 si' dod'' |
        si' si'
      }
    >> sol'4 |
    fad'( mi') red' |
    mi' fad' sol' |
    fad' fad' mi' |
    red'2\trill si'8 si' |
    si'2 si'4 |
    sol' la' si' |
    si'( la'2) |
    sol'2 <<
      \ifComplet {
        mi'4 |
        la'2 la4 |
        re'2 fad'8 fad' |
        fad'4 sol'8[ fad'] mi'[ re'] |
        dod'2\trill
      }
      \ifConcert {
        mi''4 |
        mi''( re'')\trill dod'' |
        re''2 re''8 re'' |
        re''4 dod'' dod'' |
        dod''2
      }
    >> r4 |
    R2. |
    r4 r si' |
    si'2 sold'4 |
    la'2 la'8 la' |
    fad'2 si'4 |
    si' si' la' |
    sold'2 sold'4 |
    sold' fad' mid' |
    fad'4. fad'8 fad' fad' |
    mid'2\trill sold'8 sold' |
    fad'4 fad' la' |
    la'4. la'8 la'4 |
    sold' la'4.( sold'8) |
    fad'2 r4 |
    R2.*3 |
    r4 r lad'4 |
    fad'2\trill fad'8 sol' |
    la'4 sol' fad' |
    sold'2 si'4 |
    la'2 r4 |
    r r la' |
    la'2 <<
      \ifComplet {
        re'8 re' |
        la'4 sol' fad' |
        mi'2
      }
      \ifConcert {
        re''8 re'' |
        dod''4 dod'' red'' |
        mi''2
      }
    >>
    si'8 si' |
    si'4 si' si' |
    si'2 r4 |
    r r <<
      \ifComplet { re'8 fad' | mi'2 }
      \ifConcert { si'8 si' | si'2 }
    >> r8 la' |
    sol'[ la'] sol'[ fad'] mi'[ re'] |
    dod'4\trill dod'8 dod' dod' dod' |
    re'4. fad'8 fad' si' |
    lad'2 lad'8 lad' |
    si'2.~ |
    si'4 lad' lad' |
    <<
      \ifComplet {
        si'2. |
        dod'2 dod'4 |
        re'8[ dod']( si4) dod' |
        re'8[ mi' re' dod']( re'4)~ |
        re' dod' r |
      }
      \ifConcert {
        si'8[ dod'']( re''4) mi''8[ re''] |
        dod''2 dod''4 |
        re''8[ dod'']( si'4) dod''8[ lad'] |
        si'2 si'4 |
        sol'2 sol'4 |
      }
    >>
    r4 r8 lad' lad' lad' |
    si'4. si'8 si' si' |
    si'2 si'8 si' |
    <<
      \ifComplet {
        si'4 fad' fad' |
        fad'4. fad'8 sol'4 |
        sol' fad'2 |
        re'2. |
      }
      \ifConcert {
        si'2. |
        dod''4. si'8 si'4 |
        si' si'( lad')\trill |
        si'2. |
      }
    >>
  }
  \tag #'vtaille {
    \clef "vtaille" r4 |
    R2. |
    r4 r <<
      \ifComplet {
        lad4 |
        si2 si8 si |
        dod'4 re'8[ dod'] si[ la] |
        sol4
      }
      \ifConcert {
        fad'4 |
        fad'2 fad'8 fad' |
        fad'[ mi'] re'[ dod'] si[ la] |
        sol4
      }
    >> sol4 sol |
    fad2 lad4 |
    si2 si4 |
    la!4. fad'8 fad' fad' |
    fad'4. fad'8 fad' fad' |
    mi'2 mi'8 mi' |
    re'4 fad' fad' |
    mi'4 mi' si8 si |
    si4 si8 la si si |
    si4( lad2)\trill |
    si2 r4 |
    R2.*7 |
    r4 r <<
      \ifComplet {
        lad4 |
        si2 si4 |
        si2 sold8 sold |
        la2 dod'4 |
        re'2 re'4 |
        dod'2\trill dod'4 |
        re'2 re'4 |
        dod'\trill
      }
      \ifConcert {
        fad'4 |
        fad'2 red'8 red' |
        mi'4 mi' mi' |
        mi'2 dod'4 |
        re'2 re'4 |
        dod'2\trill la4 |
        la2 la4 |
        sol
      }
    >> dod'4 dod' |
    re'2 re'8 re' |
    re'2 dod'4 |
    si mi' re' |
    re'( dod'2)\trill |
    re'2 <<
      \ifComplet { r4 | R2. | r4 r }
      \ifConcert {
        re'8 re' |
        la'4 sol' fad' |
        mi'2
      }
    >> si4 |
    si2 si4 |
    si2 r4 |
    <<
      \ifComplet { r4 r si8 si | si2 }
      \ifConcert { R2. | r4 r }
    >> si8 si |
    si4 si si |
    dod' dod' lad8 lad |
    si4 re' re' |
    dod' dod' <<
      \ifComplet {
        si8 lad |
        si4 si4 si |
        lad2\trill \appoggiatura sol8 fad4 |
        dod'8[ re' dod' si dod' lad]( |
        si4) si si8 si |
        si4 si si |
        si2 si8 si |
        dod'4 si si |
        si2 sol8 sol |
        la2 la8 la |
        si2 sol4 |
        la4 si dod' |
        si si
      }
      \ifConcert {
        re'8 dod' |
        si4 re' re' |
        dod'2\trill re'4 |
        r4 fad' fad' |
        fad' fad' red'8 mi' |
        fad'4 fad' fad' |
        mi'2 mi'8 mi' |
        mi'4 mi' red' |
        mi'2 mi'8 mi' |
        re'!2 re'8 re' |
        re'2 re'4 |
        dod'4 si la |
        si si
      }
    >> si4 |
    red'( mi') si |
    si fad' mi' |
    red' red' mi' |
    si2 red'8 red' |
    red'2 fad'4 |
    mi' red' mi' |
    mi'2( red'4)\trill |
    mi'2 <<
      \ifComplet {
        r4 |
        R2. |
        r4 r re'8 re' |
        re'4 dod' dod' |
        dod'2
      }
      \ifConcert {
        mi'4 |
        la'2 la4 |
        re'2 fad'8 fad' |
        fad'4 sol'8[ fad'] mi'[ re'] |
        dod'2\trill
      }
    >> r4 |
    r r fad' |
    fad'2 fad'4 |
    sold'2 mi'4 |
    mi'2 dod'8 dod' |
    si2 fad'4 |
    fad' mid' dod' |
    dod'2 dod'4 |
    mid' fad' dod' |
    dod'4. dod'8 dod' dod' |
    si2 si8 si |
    la4 dod' dod' |
    re'4. re'8 re'4 |
    re' dod'2 |
    lad2\trill r4 |
    R2.*3 |
    r4 r dod'4 |
    si2 r4 |
    r4 r fad' |
    mi'2 sold'8 sold' |
    mi'2 fad'4 |
    sol' fad' mi' |
    fad' fad' <<
      \ifComplet { r4 | R2. | r4 r }
      \ifConcert {
        re'8 re' |
        la'4 sol' fad' |
        mi'2
      }
    >> sol'8 fad' |
    mi'4 mi' sol' |
    fad'2 r4 |
    r r <<
      \ifComplet { si8 si | si2 }
      \ifConcert { re'8 fad' | mi'2 }
    >> si8 si |
    si4 si dod' |
    dod' r8 lad lad lad |
    si4. re'8 re' re' |
    dod'4. dod'8 dod' fad' |
    fad'2 re'8 re' |
    mi'4 mi'8[ fad' sol' mi']( |
    fad'2) fad'4 |
    dod'8[ si lad si dod' lad]( |
    <<
      \ifComplet {
        si2) lad4 |
        si2 si4 |
        sol2 sol4 |
      }
      \ifConcert {
        si4) si r |
        re'8[ mi' re' dod']( re'4)~ |
        re' dod' r |
      }
    >>
    r4 r8 fad' fad' fad' |
    fad'4. re'8 re' re' |
    re'2 sol'8 mi' |
    <<
      \ifComplet {
        fad'4 re' re' |
        dod'4. si8 si4 |
        si si( lad)\trill |
        si2. |
      }
      \ifConcert {
        fad'2. |
        fad'4. fad'8 sol'4 |
        sol' fad'2 |
        re'2. |
      }
    >>
  }
  \tag #'vbasse {
    \clef "vbasse" r4 |
    R2. |
    r4 r fad |
    si2 si8 si |
    la4 si8[ la] sol[ fad] |
    sol4 sol mi |
    fad2 r4 |
    R2. |
    r4 r8 fad fad fad |
    si4. si8 si si |
    dod'2 dod'8 dod' |
    re'4 re'8[ dod' re' si]( |
    mi'4) mi' mi8 fad |
    sol4 sol8 fad sol mi |
    fad4( fad,2) |
    si,2 r4 |
    R2.*7 |
    r4 r fad |
    si2 si8 si |
    sold4 sold mi |
    la2 r4 |
    R2. |
    r4 r la |
    re'2 re8 re |
    mi4 mi mi |
    fad2 fad8 re |
    sol2 mi4 |
    si dod' re' |
    la4( la,2) |
    re2 r4 |
    R2. |
    r4 r mi |
    sol2 mi4 |
    si2 r4 |
    R2. |
    r4 r mi8 fad |
    sol4 mi8[\melisma fad sol mi] |
    fad2.~ |
    fad~ |
    fad~ |
    fad~ |
    fad~ |
    fad8[ mi fad sol la fad]( |
    si4)\melismaEnd si si8 si |
    la4 si8[ la] sol[ fad] |
    sol2 sol8 sol |
    fad4 si si, |
    mi2 mi8 mi |
    fad2 fad8 fad |
    sol2 mi4 |
    la la fad |
    si si si |
    si2.~ |
    si~ |
    si~ |
    si2 si8 si |
    la2 la4 |
    sol fad mi |
    si4( si,2) |
    mi2 r4 |
    R2. |
    r4 r re8 re |
    mi4 sol mi |
    fad2 r4 |
    R2. |
    r4 r si |
    mi'2 mi4 |
    la2 la8 fad |
    si2 si4 |
    la sold fad |
    dod'2 dod'4 |
    dod dod4. dod8 |
    fad4. fad8 fad fad |
    sold2 sold8 sold |
    la4 la8[ si dod' la]( |
    re'4.) re'8 re'4 |
    si dod'( dod) |
    fad2 r4 |
    R2.*3 |
    r4 r fad |
    si2 r4 |
    r r si |
    mi'2 mi4 |
    la2 r4 |
    r r la |
    re'2 r4 |
    R2. |
    r4 r mi8 fad |
    sol4 sol mi |
    si2 r4 |
    R2. |
    r4 r mi8 fad |
    sol4 sol mi |
    fad r r |
    r4 r8 si, si, si, |
    fad4. fad8 fad fad |
    si2 si8 si |
    dod'2. |
    re'8[\melisma dod' si re' dod' si] |
    lad[ sold fad sold lad fad] |
    si[ lad si re' dod' mi'] |
    re'[ dod' si la sol fad] |
    mi[ re mi fad sol mi]( |
    fad4)\melismaEnd fad8 fad fad fad |
    si4. si8 si si |
    re'4 re'4. re'8 |
    re'2. |
    lad4. si8 sol4 |
    mi fad2 |
    si,2. |
  }
>>
