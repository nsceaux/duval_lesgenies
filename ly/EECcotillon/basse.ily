\clef "basse" si2 |
si mi |
si, mi |
si, lad, |
si, si |
si mi |
si, mi |
si, si |
sold4 lad8 si dod'4 dod |
fad2 lad4 fad |
si fad lad, fad, |
si, si8 la sold4 mi |
fad red sold mid |
fad8 mid fad sold lad4 fad |
si red' lad fad |
si la sold mi |
fad si fad fad, |
si,2
