\clef "dessus" si'4 fad'' |
fad'' red'' sold'' fad''8 mi'' |
fad''4 red'' sold'' fad''8 mi'' |
fad''4 mi''8 red'' mi'' dod'' red'' mi'' |
red''4 si'2 fad''4 |
fad'' mi''8 red'' sold''4 fad''8 mi'' |
fad''4 mi''8 red'' sold''4 fad''8 mi'' |
fad''4 sold''8 lad'' si''4 si' |
red''2 mid'' |
fad'' dod''4 mi'' |
red'' fad'' dod'' mi'' |
red'' fad'' si' mi''8 red'' |
dod''4 fad'' si' dod'' |
lad' fad' dod'' mi'' |
red'' fad'' dod'' mi'' |
red'' fad'' si' mi''8 red'' |
dod''4 si' dod'' lad' |
si'2
