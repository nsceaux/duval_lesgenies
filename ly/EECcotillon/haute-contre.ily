\clef "haute-contre" fad'4 red'' |
red''8 dod'' si' lad' si'4 si' |
si'2 mi''4 red''8 dod'' |
red''4 dod''8 si' dod'' lad' si' dod'' |
si'4 fad'2 red''4 |
red'' dod''8 si' si'4 si'8 dod'' |
red''4 dod''8 si' si'4 si'8 dod'' |
si'2. si'4 |
si'2 si' |
lad'\trill fad'4 fad' |
fad'2 fad'4 fad' |
fad' red' mi' sold' |
lad' lad' si' sold' |
lad'2 fad'4 fad' |
fad'2 fad'4 fad' |
fad' red' mi'8 fad' sold' fad' |
mi' sold' fad' mi' red'4 mi' |
red'2\trill
