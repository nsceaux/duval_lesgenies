\clef "taille" red'2 |
red'4 fad' mi' red'8 dod' |
red'4 si si si |
si dod'8 red' dod'4 fad |
fad red'2 red'4 |
red' mi'8 fad' mi'4 red'8 dod' |
red'4 mi'8 fad' mi'4 red'8 dod' |
red'4 mi'8 fad' fad'4 fad' |
fad'2 mid'4 dod' |
dod'2 mi'4 dod' |
si red' dod' dod' |
si red'8 dod' si4 dod' |
dod' red' red' dod' |
dod'2 mi'4 dod' |
si fad'2 dod'4 |
si2 mi'8 red' dod'4 |
lad4 si2 fad4 |
fad2
