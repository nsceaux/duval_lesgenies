\clef "basse" si4 lad8 si |
fad2 lad, |
si,4 re8 dod si,4 lad,8 si, |
fad, sold, la, si, dod4 dod, |
fad,2 fad4 mi8 re |
dod si, lad, dod si,2 |
fad, fad4 mi8 re |
dod mi re dod re4 lad, |
si,8 dod re mi re dod si, mi |
fad4 sol8 mi fad4 fad, |
si,2
