\clef "dessus" re''4 dod''8 si' |
dod''4 fad'' mi''8 re'' dod'' mi'' |
re''4 si' re'' dod''8 si' |
dod''4 fad''8 la' sold'4.\trill fad'8 |
fad'2 dod''4 dod''8 re'' |
mi''4 dod'' fad'' si' |
dod'' fad' dod'' dod''8 re'' |
mi'' dod'' fad'' lad' si' re'' dod'' mi'' |
re'' dod'' fad'' lad' si' dod'' re'' mi'' |
lad'4 si'8 dod'' dod''4. si'8 |
si'2
