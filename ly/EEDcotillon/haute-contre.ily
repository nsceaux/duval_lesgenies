\clef "haute-contre" fad'4 fad' |
fad'2 fad'4 fad' |
fad'2 fad'4 fad' |
fad'2 fad'4 mid'\trill |
fad'2 lad'4 lad'8 si' |
dod''4 mi' re' fad' |
fad'2 lad'4 lad'8 si' |
dod''4 fad' fad' fad' |
fad' fad'8 mi' re' mi' fad' sol' |
fad'4 si'2 lad'4\trill |
si'2
