\clef "taille" si dod'8 re' |
lad4 lad8 si dod' re' mi' dod' |
si2 si4 dod'8 re' |
lad si dod' re' dod'4 si |
lad2\trill fad4 fad |
lad8 si dod' lad si4 si |
lad2\trill fad4 fad |
lad8 sold fad mi fad4 mi'8 dod' |
re' mi' fad' sol' fad' mi' re' dod' |
dod'4 mi'8 sol' fad'4 mi' |
re'2
