\clef "basse" si4 |
lad la sold sol |
fad2 r |
re'2 do'4 |
si dod' re' |
fad4. fad8 mi re |
la4 sol fad2 |
r8 sol la si mi2 |
la4. si8 dod'4 |
re' dod' si |
la sol fad2 |
r8 sol la si mi2 |
la4 sol8 fad mi dod |
re4 la la, |
re2. |
