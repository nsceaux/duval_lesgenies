\key si \minor
\once\omit Staff.TimeSignature \time 2/2 \partial 4 \midiTempo#80 s4
\time 4/4 s1*2
\digitTime\time 3/4 s2.*3
\digitTime\time 2/2 \midiTempo#160 s1*2
\digitTime\time 3/4 \midiTempo#80 s2.*2
\digitTime\time 2/2 \midiTempo#160 s1*2
\digitTime\time 3/4 \midiTempo#80 s2.*3 \bar "|."
