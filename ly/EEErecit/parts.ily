\piecePartSpecs
#`((basse #:instrument "[B.C.]"
          #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup , #{

\markup\fontsize#-2 \column {
  \livretPersDidas Le Sylphe à sa suite.
  \livretVerse#12 { Ce lieu va recevoir la beauté qui m’engage, }
  \livretVerse#10 { Vous, qui sous d’aimables déguisements }
  \livretVerse#8 { Venez lui rendre votre hommage, }
  \livretVerse#10 { Formez des jeux & des concerts charmants. }
}
       #}))
