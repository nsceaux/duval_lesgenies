\score {
  \new ChoirStaff <<
    \new Staff \with { instrumentName = \markup\character Le Sylphe } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "[B.C.]" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s4 s1*2 s2.\break \grace s8 s2.*2 s1*2 s2.\break s2. s1*2 s2.*3 }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
