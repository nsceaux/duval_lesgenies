\clef "vhaute-contre" r8 si |
fad'4 dod'8 re'16 dod' si8 dod'16 re' dod'8 re'16 si |
lad4\trill fad re' \ficta la8 re'16 mi' |
fad'4 mi'8 re' mi' fad' |
\appoggiatura fad'8 sol'4. mi'8 fad' sol' |
la'4. re'8 dod' re' |
mi'4 la r8 re' mi' fad' |
si2 r8 mi' fad' sol' |
dod'4.\trill re'8 mi'4 |
fad' sol'8[ fad'] mi'[ re'] |
la'2 r8 re' mi' fad' |
si2 r8 mi' fad' sol' |
dod'4.\trill re'8 mi'4 |
fad' mi'4.\trill re'8 |
re'2. |
