\clef "basse" re'4 dod' si |
dod' si lad |
si sol mi |
mi8 dod fad mi re dod |
si,4 dod re |
mi2. |
fad4 sol2 |
fad2. |
R2. |
re4 re4. mi8 |
fad4 sol2 |
la sol4 |
fad sol la |
si dod' re' |
sol la la, |
re mi fad |
si, si la |
sol2 fad8 mi |
re4 lad, si, |
fad, fad mi |
re8 dod si, dod re si, |
mi2 fad4 |
lad,2. |
si,4 la, sol, |
fad,8 fad re fad dod fad |
si,4 dod re |
mi fad fad, |
si,2. |
