\clef "dessus"
<<
  \tag #'dessus1 {
    re''4 mi'' fad'' |
    mi''4. mi''8 fad'' mi'' |
    re''4 si' dod''8 re'' |
    lad' dod'' lad' dod'' fad'4 |
    fad''8 la'' sol'' la'' fad'' sol'' |
    mi'' sol'' mi'' sol'' mi'' sol'' |
    re'' dod'' re'' si' mi'' sol'' |
    dod''2. |
    R2. |
    la''4 la''4. sol''8 |
    fad''4 mi''8 re'' sol'' fad'' |
    mi'' fad'' mi'' re'' dod''4 |
    fad'' mi'' mi'' |
    re'' la'' la'' |
    si''4 la''8 si'' la'' sol'' fad''2. |
    la''4 sol'' fad'' |
    sol''8 la'' si'' lad'' si'' dod''' |
    re''' si'' dod'''4. re'''8 |
    lad'' sold'' lad'' si'' dod''' lad'' |
    si''2 si''4 si''8 la'' sol'' fad'' mi'' re'' |
    dod''2 dod''4 |
    fad' fad' si' |
    lad'8 dod'' si' re'' lad' dod'' |
    si' fad'' dod'' fad'' re'' si'' |
    sol'' si'' mi'' sol'' dod'' fad'' |
    \appoggiatura mi''8 re''2. |
  }
  \tag #'dessus2 {
    si'4 dod'' re'' |
    dod''4. dod''8 dod'' dod'' |
    fad'4 sol'4. sol'8 |
    fad'8 lad' dod'' lad' fad' lad' |
    si'4 lad'8 dod'' si' re'' |
    dod'' si' dod'' mi'' dod''4 |
    si'8 lad' si' re'' dod'' si' |
    lad'2. |
    R2. |
    re''4 fad''4. mi''8 |
    re''4 si' si'' |
    la''8 la'' sol'' fad'' mi''4 |
    re''8 dod'' si' si' la' sol' |
    fad'4 mi' re' |
    re''4 dod''4.\trill re''8 |
    re''2. |
    red''4 mi'' fad'' |
    si'2 si'8 lad' |
    si'4 sol''8 fad'' mi'' re'' |
    dod'' si' dod'' re'' mi'' dod'' |
    re''2 re''4 |
    re'' dod'' si' |
    mi'' lad' dod'' |
    si' fad'' mi'' |
    fad'' fad' fad' |
    si'8 re'' lad' dod'' si'4 |
    si' lad'4.\trill si'8 |
    si'2. |
  }
>>
