\piecePartSpecs
#`((dessus #:instrument "[Violons]" #:score-template "score-2dessus")
   (dessus1 #:instrument "[Violons]")
   (dessus2 #:instrument "[Violons]")
   (basse #:score-template "score-basse-continue-voix"
          #:instrument
          , #{ \markup\center-column { [Basses et B.C.] } #})
   (silence #:on-the-fly-markup , #{
\markup\fontsize#-2 \column {
  \livretPers Le Sylphe
  \livretVerse#10 { Que de son nom ce séjour retentisse, }
  \livretVerse#8 { Applaudissez à mon ardeur ; }
  \livretVerse#10 { Qu’à mes transports votre zèle s’unisse, }
  \livretVerse#8 { Ne songeons qu’à touchez son cœur. }
}#}))
