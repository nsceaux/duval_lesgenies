\score {
  \new ChoirStaff <<
    \new GrandStaff \with {
      instrumentName = \markup\center-column { [Violons] }
    } <<
      \new Staff << \global \keepWithTag#'dessus1 \includeNotes "dessus" >>
      \new Staff << \global \keepWithTag#'dessus2 \includeNotes "dessus" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Le Sylphe
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*7\break s2.*7\pageBreak
        s2.*6\break s2.*8\pageBreak
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
