\clef "vhaute-contre" fad'4 sol'8[ fad'] mi'[ re'] |
mi'4 fad'8[ mi'] re'[ dod'] |
re'4 mi'8[ re'] dod'[ si] |
dod'2 lad4 |
re' mi' fad' |
sol'2. |
fad'4 fad' mi' |
fad'2. |
re'4 re' mi' |
fad'2 fad'8 sol' |
la'4 sol'8[ fad'] mi'[ re'] |
dod'2\trill \appoggiatura si8 la4 |
r si dod' |
re' mi' fad' |
fad'4( mi'4.)\trill re'8 |
re'2. |
fad'4 mi' red' |
mi'2 re'8 dod' |
fad'4 mi'8[ re'] dod'[ si] |
dod'2 lad4 |
r fad' re' |
sol' mi' dod' |
fad' dod' mi' |
re'( dod')\trill si |
dod' re' mi' |
re' mi' fad' |
re'( dod'4.)\trill si8 |
si2. |
