\clef "basse" re'4 mi'8 re' dod' si |
dod'4 re'8 dod' si lad |
si4 si8 dod' re' mi' |
fad'2 fad'4 |
si si si |
la2. |
sol4 sol sol |
fad2. |
re4 re re |
re' re' re' |
dod' dod'4. re'8 |
la2 la4 |
r4 fad re |
si dod' re' |
la2 la4 |
re2. |
fad'4 fad' fad' |
mi' mi' mi' |
re' re' re' |
dod'2 dod'4 |
r dod' dod' |
si si si |
la mid fad |
dod'2 dod4 |
fad2. |
\clef "alto" fad'4 fad' fad' |
mi' re' dod' |
si sol' mi' |
fad'2 fad'4 |
si si si |
la sol fad |
mi do' la |
si2 si4 |
\clef "bass" mi'4 mi' mi |
do'2. |
sol4 la si |
mi2. |
la4 la la |
re' la re' |
re' re' re' |
dod'2 dod'4 |
re'4 re' re' |
dod'2. |
re'4 re' re' |
dod'4. dod'8 dod' re' |
la2. |
R2. |
r4 la fad |
si dod' re' |
la2 la4 |
re2 dod4 |
si,8 dod re4 fad, |
sol,2 la,4 |
re2 re'4 |
dod'2 re'4 |
sol2. |
fad4 mi re |
sol, la,2 |
\clef "alto" si4 si si |
sol' mi' fad' |
si si si |
fad'2 fad'4 |
\clef "bass" si4 si si |
lad2. |
si4 sol sol |
fad2. |
\clef "alto" si4 si si |
sol' mi' fad' |
si si si |
fad2 fad4 |
\clef "bass" R2. |
r4 si si |
mi fad fad |
si2 si,4 |
fad fad fad |
si sol mi |
fad2 fad4 |
si, si lad |
si sol mi |
fad4. mi8 re dod |
si,4 re' dod' |
si si si |
lad2 lad4 |
lad lad fad |
si sol mi |
fad2 fad4 |
si,2. |
