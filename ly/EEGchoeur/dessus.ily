\clef "dessus" fad''4 sol''8( fad'') mi''( re'') |
mi''4 fad''8( mi'') re''( dod'') |
re''4 fad'' re'' |
dod''2 dod''4 |
re'' re'' mi'' |
fad''2. |
fad''4 fad'' mi'' |
fad''2. |
la''4 la'' fad'' |
re'' re'' re'' |
la' la'4. re''8 |
dod''2 \appoggiatura si'8 la'4 |
r fad'' la'' |
re'' mi'' fad'' |
fad''( mi''4.)\trill re''8 |
re''2. |
R2. |
fad''4 fad'' fad'' |
fad'' sold'' fad'' |
mid'' mid'' mid'' |
mid''2\trill mid''4 |
r mid'' dod'' |
la'' sold'' fad'' |
fad''2 mid''4 |
fad''2. |
\twoVoices #'(un deux tous) <<
  { dod''4 dod'' re'' |
    dod'' re'' mi'' |
    fad'' mi'' re'' |
    dod''2\trill dod''4 |
    red'' red'' mi'' |
    red'' mi'' fad'' |
    sol'' fad'' mi'' |
    red''2 red''4 }
  { lad'4 lad' si' |
    lad' si' dod'' |
    re'' dod'' si' |
    lad'2\trill lad'4 |
    fad' fad' sol' |
    fad' sol' la' |
    si' la' sol' |
    fad'2 fad'4 }
>>
<>^"Tous" mi''4 mi'' mi'' |
la''2. |
sol''4 fad''4.\trill mi''8 |
mi''2. |
sol''4 fad'' mi'' |
fad'' mi'' re'' |
sol'' sol'' mi'' |
la''2 la''4 |
fad''8 mi'' re'' mi'' fad'' re'' |
mi'' re'' mi'' fad'' sol'' mi'' |
fad'' sol'' la'' sol'' fad'' la'' |
sol''2 fad''4 |
mi''4.\trill la''8 la'' sol'' |
fad'' mi'' re'' mi'' fad'' re'' |
mi''4 la'' la'' |
la'' sol'' fad'' |
fad''( mi''4.)\trill re''8 |
re''8 <>^"Violons" fad'' sol''4 la'' |
re'' la' re'' |
mi''8 fad'' sol''4 mi'' |
fad'' mi''8 fad'' re''4 |
la'' sol''8 fad'' mi'' re'' |
si''4 la''8 sol'' fad'' mi'' |
la''4 dod'' re'' |
mi'' mi''4.\trill re''8 |
\twoVoices #'(un deux tous) <<
  { re''8 fad'' sol'' fad'' mi'' re'' |
    mi''4 fad''8 mi'' re'' dod'' |
    re''4 mi''8 re'' dod'' si' |
    dod''2\trill lad'4 | }
  { re''4 mi''8 re'' dod'' si' |
    dod''4 re''8 dod'' si' lad' |
    si'4 dod''8 si' lad' sold' |
    lad'2 fad'4 | }
>>
fad''4 fad'' re'' |
sol''2. |
fad''4 fad'' mi'' |
fad''2. |
\twoVoices #'(un deux tous) <<
  { fad''4 sol''8 fad'' mi'' re'' |
    mi''4 fad''8 mi'' re'' dod'' |
    re''4 mi''8 re'' dod'' si' |
    dod''2 lad'!4 | }
  { re''4 mi''8 re'' dod'' si' |
    dod''4 re''8 dod'' si' lad' |
    si'4 dod''8 si' lad' sold' |
    lad'2 fad'4 | }
>>
<>^"Tous" r4 fad'' mi'' |
re'' mi'' fad'' |
sol'' dod'' mi'' |
re''( dod'') si' |
dod'' re'' mi'' |
re'' mi'' sol'' |
re''( dod''4.)\trill si'8 |
si'4 <>^"Violons" fad''4 mi'' |
re'' dod'' si' |
lad' si'8 dod'' re'' mi'' |
fad''4 fad'' mi'' |
re'' mi'' fad'' |
sol''( fad'')\trill mi'' |
fad'' dod'' mi'' |
re'' mi'' sol'' |
re''( dod''4.)\trill si'8 |
si'2. |
