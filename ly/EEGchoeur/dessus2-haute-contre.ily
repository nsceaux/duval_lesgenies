\clef "haute-contre" si'4 si' si' |
lad' lad' fad' |
fad' fad' si' |
lad'2\trill lad'4 |
fad' fad' sol' |
fad'2. |
re'4 re' mi' |
dod'2. |
fad'4 fad' la' |
la' la' la' |
la' la'4. la'8 |
la'2 la'4 |
r la' la' |
la' sol' fad'8 mi' |
re'2 dod'4 |
re'2. |
R2. |
lad'4 lad' lad' |
si' si' si' |
sold' sold' sold' |
sold'2 sold'4 |
r sold' sold' |
fad' sold' la' |
la'2 sold'4 |
fad'2. |
lad'4 lad' si' |
lad' si' dod'' |
re'' dod'' si' |
lad'2\trill lad'4 |
fad' fad' sol' |
fad' sol' la' |
si' la' sol' |
fad'2 fad'4 |
sol'4 sol' sol' |
fad'2. |
mi'4 mi' red' |
sol'2. |
mi'4 fad' la' |
la' la' la' |
sol' sol' sol' |
sol'2 sol'4 |
r fad' la' |
sol' sol' mi' |
re' fad' la' |
la'2 la'4 |
la'2. |
R2. |
r4 mi' mi' |
fad' mi' re' |
re'2 dod'4 |
re' si' la' |
sol' fad' la' |
si'2 la'4 |
la' sol'8 la' fad'4 |
la'2 la'4 |
re''2 la'4 |
la' la' la' |
si' la' la |
si4 mi''8 re'' dod'' si' |
dod''4 re''8 dod'' si' lad' |
si'4 dod''8 si' lad' sold' |
lad'2 fad'4 |
re''4 re'' re'' |
dod''2. |
re''4 si' si' |
lad'2.\trill |
re''4 mi''8 re'' dod'' si' |
dod''4 re''8 dod'' si' lad' |
si'4 dod''8 si' lad' sold' |
lad'2 fad'4 |
R2. |
r4 sol' fad' |
mi' dod' dod' |
re'( mi') fad' |
fad' fad' fad' |
fad' mi' mi' |
re'2 fad'8 mi' |
re'4 re'' dod'' |
re'' sol' sol' |
fad'4. lad'8 si' dod'' |
re''4 si' sol' |
fad' sol' fad' |
mi'2 sol'4 |
fad' fad' fad' |
fad' mi' mi' |
re'2 fad'8 mi' |
re'2. |
