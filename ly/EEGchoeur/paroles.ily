\tag #'vdessus {
  Que de son nom ce sé -- jour re -- ten -- tis -- se,
  ap -- plau -- dis -- sons à son ar -- deur ;
  qu’à ses trans -- ports no -- tre zè -- le s’u -- nis -- se,
  ne son -- geons qu’à tou -- cher son cœur.

  Que de son nom ce sé -- jour re -- ten -- tis -- se,
  ne son -- geons qu’à tou -- cher son cœur.

  Que de son nom ce sé -- jour re -- ten -- tis -- se,
  que de son nom ce sé -- jour re -- ten -- tis -- se,
  ap -- plau -- dis -- sons à son ar -- deur ;
  qu’à ses trans -- ports no -- tre zè -- le s’u -- nis -- se,
  ne son -- geons, ne son -- geons qu’à tou -- cher son cœur,
  ne son -- geons, ne son -- geons qu’à tou -- cher son cœur.

  Que de son nom ce sé -- jour re -- ten -- tis -- se,
  ap -- plau -- dis -- sons à son ar -- deur ;
  que de son nom ce sé -- jour re -- ten -- tis -- se,
  ne son -- geons, ne son -- geons qu’à tou -- cher son cœur,
  ne son -- geons qu’à tou -- cher son cœur.

  Ne son -- geons qu’à tou -- cher son cœur,
  ne son -- geons qu’à tou -- cher son cœur.
}
\tag #'vhaute-contre {
  Que de son nom ce sé -- jour re -- ten -- tis -- se,
  ap -- plau -- dis -- sons à son ar -- deur ;
  qu’à ses trans -- ports no -- tre zè -- le s’u -- nis -- se,
  ne son -- geons qu’à tou -- cher son cœur.

  Que de son nom ce sé -- jour re -- ten -- tis -- se,
  ne son -- geons qu’à tou -- cher son cœur.

  Que de son nom ce sé -- jour re -- ten -- tis -- se,

  ap -- plau -- dis -- sons à son ar -- deur ;
  qu’à ses trans -- ports no -- tre zè -- le s’u -- nis -- se,
  ne son -- geons, ne son -- geons qu’à tou -- cher son cœur,
  ne son -- geons qu’à tou -- cher son cœur.

  Que de son nom ce sé -- jour re -- ten -- tis -- se,
  ap -- plau -- dis -- sons à son ar -- deur ;

  Ne son -- geons qu’à tou -- cher son cœur,
  ne son -- geons qu’à tou -- cher son cœur.

  Ne son -- geons qu’à tou -- cher son cœur,
  ne son -- geons qu’à tou -- cher son cœur.
}
\tag #'vtaille {
  Que de son nom ce sé -- jour re -- ten -- tis -- se,
  ap -- plau -- dis -- sons à son ar -- deur ;
  qu’à ses trans -- ports no -- tre zè -- le s’u -- nis -- se,
  ne son -- geons qu’à tou -- cher son cœur.

  Que de son nom ce sé -- jour re -- ten -- tis -- se,
  ne son -- geons qu’à tou -- cher son cœur.

  Que de son nom ce sé -- jour re -- ten -- tis -- se,

  ap -- plau -- dis -- sons à son ar -- deur ;
  qu’à ses trans -- ports no -- tre zè -- le s’u -- nis -- se,
  ap -- plau -- dis -- sons, ap -- plau -- dis -- sons à son ar -- deur ;
  ne son -- geons qu’à tou -- cher son cœur.

  Ap -- plau -- dis -- sons à son ar -- deur ;

  Que de son nom ce sé -- jour re -- ten -- tis -- se,

  Ne son -- geons qu’à tou -- cher son cœur,
  ne son -- geons qu’à tou -- cher son cœur.

  Ne son -- geons qu’à tou -- cher son cœur,
  ne son -- geons qu’à tou -- cher son cœur.
}
\tag #'vbasse {
  Que de son nom ce sé -- jour re -- ten -- tis -- se,
  ap -- plau -- dis -- sons à son ar -- deur ;
  qu’à ses trans -- ports no -- tre zè -- le s’u -- nis -- se,
  ne son -- geons qu’à tou -- cher son cœur.

  Que de son nom ce sé -- jour re -- ten -- tis -- se,
  ne son -- geons, ne son -- geons qu’à tou -- cher son cœur.

  Ap -- plau -- dis -- sons à son ar -- deur ;
  qu’à ses trans -- ports no -- tre zè -- le s’u -- nis -- se,
  ap -- plau -- dis -- sons, ap -- plau -- dis -- sons à son ar -- deur ;
  ne son -- geons qu’à tou -- cher son cœur.

  Ap -- plau -- dis -- sons à son ar -- deur ;

  Ne son -- geons qu’à tou -- cher son cœur,
  ne son -- geons qu’à tou -- cher son cœur.

  Ne son -- geons qu’à tou -- cher son cœur,
  ne son -- geons qu’à tou -- cher son cœur.
}
