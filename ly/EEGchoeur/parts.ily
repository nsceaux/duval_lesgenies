\piecePartSpecs
#`((dessus #:tag-notes tous
           #:instrument ,#{\markup\center-column { [Violons et Hautbois] }#})
   (dessus1 #:tag-notes un
            #:instrument ,#{\markup\center-column { [Violons et Hautbois] }#})
   (dessus2 #:tag-notes deux
            #:instrument ,#{\markup\center-column { [Violons et Hautbois] }#})
   (parties)
   (dessus2-haute-contre #:notes "dessus2-haute-contre"
                         #:indent 0)
   (taille #:indent 0)
   (basse #:instrument
          , #{ \markup\center-column { [Basses, Bassons et B.C.] } #})
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
