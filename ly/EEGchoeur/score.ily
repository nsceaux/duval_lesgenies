\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with {
        \override VerticalAxisGroup.remove-empty = #(eqv? (ly:get-option 'urtext) #t)
        \override VerticalAxisGroup.remove-first = #(eqv? (ly:get-option 'urtext) #t)
        instrumentName = \markup\center-column { [Violons, Hautbois] }
      } << \global \keepWithTag #'tous \includeNotes "dessus" >>
      \ifFull\new Staff \with {
        instrumentName = \markup\center-column { [Hautes-contre] }
      } << \global \includeNotes "haute-contre" >>
      \ifFull\new Staff \with { instrumentName = "[Tailles]" } <<
        \global \includeNotes "taille"
      >>
    >>
    \new ChoirStaff <<
      \new Staff = "vdessus" \with { instrumentName = "[Dessus]" } \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \with {
        \override VerticalAxisGroup.remove-empty = #(eqv? (ly:get-option 'urtext) #t)
        \override VerticalAxisGroup.remove-first = #(eqv? (ly:get-option 'urtext) #t)
        instrumentName = \markup\center-column { [Hautes-contre] }
      } \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \with {
        \override VerticalAxisGroup.remove-empty = #(eqv? (ly:get-option 'urtext) #t)
        \override VerticalAxisGroup.remove-first = #(eqv? (ly:get-option 'urtext) #t)
        instrumentName = "[Tailles]"
      } \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \with { instrumentName = "[Basses]" } \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses, Bassons et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*7\break s2.*7\break s2.*7\pageBreak
        s2.*6\break s2.*7\break s2.*7\pageBreak
        s2.*7\break s2.*9\break s2.*7\pageBreak
        s2.*7\break s2.*7\break s2.*9
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
