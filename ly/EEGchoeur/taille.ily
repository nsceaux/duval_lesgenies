\clef "taille" re'4 dod'8( re') mi'( fad') |
mi'4 re'8( mi') fad'4 |
re' re'8( mi') fad'4 |
fad'2 fad'4 |
si si mi' |
dod'2. |
si4 si si |
lad2.\trill |
re'4 re' re' |
fad' fad' fad' |
mi' mi'4. fad'8 |
mi'2 \appoggiatura re'8 dod'4 |
r re' fad' |
fad' mi' re' |
la2 sol4 |
fad2.\trill |
R2. |
dod'4 dod' dod' |
re' re' re' |
mid' dod' dod' |
dod'2 dod'4 |
r dod' dod' |
dod' dod' dod' |
dod'2 si4 |
lad2.\trill |
fad'4 fad' fad' |
mi' re' dod' |
si sol' mi' |
fad'2 fad'4 |
si4 si si |
la sol fad |
mi do' la |
si2 si4 |
mi'4 mi' mi' |
red'2. |
si4 do' si |
si2. |
dod'4 re' dod' |
re' dod' fad' |
mi' mi' mi' |
mi'2 mi'4 |
fad' fad' fad' |
mi'2. |
la4 re' fad' |
mi'4. mi'8 mi' re' |
dod'2.\trill |
R2. |
r4 dod' dod' |
re' mi' fad' |
la'2 sol'4 |
fad'8 la' sol' fad' mi' dod' |
re' mi' fad'4 re' |
re'2 dod'4 |
re'2 re'4 |
mi'2 fad'4 |
fad' fad'8 mi' re' dod' |
re'4 mi' fad' |
re'2 dod'4 |
si si si |
sol' mi' fad' |
si si si |
fad'2 fad'4 |
fad' fad' fad' |
mi'2. |
re'4 re' mi' |
dod'2. |
si4 si si |
sol' mi' fad' |
si si si |
fad2 fad4 |
R2. |
r4 si re' |
si lad lad |
si2 si4 |
lad si dod' |
si si si |
si4( lad4.)\trill si8 |
si4 fad' fad' |
fad' si si |
dod' re'8 dod' si lad |
si4 re' mi' |
fad' mi' re' |
dod'2 dod'4 |
dod' dod' lad |
si si si |
si( lad4.)\trill si8 |
si2. |
