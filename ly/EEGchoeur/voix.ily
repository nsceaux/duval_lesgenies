<<
  \tag #'vdessus {
    \clef "vdessus"
    fad''4 sol''8([ fad'']) mi''([ re'']) |
    mi''4 fad''8([ mi'']) re''([ dod'']) |
    re''4 fad'' re'' |
    dod''2 dod''4 |
    re'' re'' mi'' |
    fad''2. |
    fad''4 fad'' mi'' |
    fad''2. |
    la''4 la'' fad'' |
    re'' re'' re'' |
    la' la'4. re''8 |
    dod''2 \appoggiatura si'8 la'4 |
    r fad'' la'' |
    re'' mi'' fad'' |
    fad''( mi''4.)\trill re''8 |
    re''2. |
    R2. |
    fad''4 fad'' fad'' |
    fad'' sold'' fad'' |
    mid'' mid'' mid'' |
    mid''2\trill mid''4 |
    r mid'' dod'' |
    la'' sold'' fad'' |
    fad''2 mid''4 |
    fad''2. |
    <<
      { \voiceOne dod''4 dod'' re'' |
        dod'' re'' mi'' |
        fad'' mi'' re'' |
        dod''2\trill dod''4 |
        red'' red'' mi'' |
        red'' mi'' fad'' |
        sol'' fad'' mi'' |
        red''2 red''4 \oneVoice }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo lad'4 lad' si' |
        lad' si' dod'' |
        re'' dod'' si' |
        lad'2\trill lad'4 |
        fad' fad' sol' |
        fad' sol' la' |
        si' la' sol' |
        fad'2 fad'4
      }
    >>
    <>^"Tous" mi''4 mi'' mi'' |
    la''2. |
    sol''4 fad''4.\trill mi''8 |
    mi''2. |
    sol''4 fad'' mi'' |
    fad'' mi'' re'' |
    sol'' sol'' mi'' |
    la''2 la''4 |
    r4 re'' fad'' |
    mi'' mi'' sol'' |
    fad'' la'' re'' |
    sol''2 fad''4 |
    mi''2.\trill |
    r4 re'' fad'' |
    mi'' mi'' la'' |
    re'' mi'' fad'' |
    fad''( mi''4.)\trill re''8 |
    re''2 r4 |
    R2.*7 |
    <<
      { \voiceOne fad''4 sol''8[ fad''] mi''[ re''] |
        mi''4 fad''8[ mi''] re''[ dod''] |
        re''4 mi''8[ re''] dod''[ si'] |
        dod''2\trill lad'!4 | \oneVoice }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo re''4 mi''8[ re''] dod''[ si'] |
        dod''4 re''8[ dod''] si'[ lad'] |
        si'4 dod''8[ si'] lad'[ sold'] |
        lad'2 fad'4 |
      }
    >>
    fad''4 fad'' re'' |
    sol''2. |
    fad''4 fad'' mi'' |
    fad''2. |
    <<
      { \voiceOne fad''4 sol''8[ fad''] mi''[ re''] |
        mi''4 fad''8[ mi''] re''[ dod''] |
        re''4 mi''8[ re''] dod''[ si'] |
        dod''2 lad'!4 | \oneVoice }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo re''4 mi''8[ re''] dod''[ si'] |
        dod''4 re''8[ dod''] si'[ lad'] |
        si'4 dod''8[ si'] lad'[ sold'] |
        lad'2 fad'4 |
      }
    >>
    <>^"Tous" r4 fad'' mi'' |
    re'' mi'' fad'' |
    sol'' dod'' mi'' |
    re''( dod'') si' |
    dod'' re'' mi'' |
    re'' mi'' sol'' |
    re''( dod''4.)\trill si'8 |
    si'2 r4 |
    R2.*2 |
    r4 fad'' mi'' |
    re'' mi'' fad'' |
    sol''( fad'')\trill mi'' |
    fad'' dod'' mi'' |
    re'' mi'' sol'' |
    re''( dod''4.)\trill si'8 |
    si'2. |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" si'4 si' si' |
    lad' lad' fad' |
    fad' fad' si' |
    lad'2\trill lad'4 |
    fad' fad' sol' |
    fad'2. |
    re'4 re' mi' |
    dod'2. |
    fad'4 fad' la' |
    la' la' la' |
    la' la'4. la'8 |
    la'2 la'4 |
    r la' la' |
    la' sol' fad'8[ mi'] |
    re'2 dod'4 |
    re'2. |
    R2. |
    lad'4 lad' lad' |
    si' si' si' |
    sold' sold' sold' |
    sold'2 sold'4 |
    r sold' sold' |
    fad' sold' la' |
    la'2 sold'4 |
    fad'2. |
    fad'4 fad' fad' |
    mi' re' dod' |
    si sol' mi' |
    fad'2 fad'4 |
    R2.*4 |
    sol'4 sol' sol' |
    fad'2. |
    mi'4 mi' red' |
    sol'2. |
    mi'4 fad' la' |
    la' la' la' |
    sol' sol' sol' |
    sol'2 sol'4 |
    r fad' la' |
    sol' sol' mi' |
    re' fad' la' |
    la'2 la'4 |
    la'2. |
    R2. |
    r4 mi' mi' |
    la' sol' fad' |
    la'2 sol'4 |
    fad'2\trill r4 |
    R2.*7 |
    si4 si si |
    sol' mi' fad' |
    si si si |
    fad'2 fad'4 |
    fad'4 fad' fad' |
    mi'2. |
    re'4 re' mi' |
    dod'2. |
    R2.*5 |
    r4 sol' fad' |
    mi' dod' dod' |
    re'( mi') fad' |
    fad' fad' fad' |
    fad' mi' mi' |
    re'2 fad'8[ mi'] |
    re'2 r4 |
    R2.*2 |
    r4 si' sol' |
    fad' sol' fad' |
    mi'2 sol'4 |
    fad' fad' fad' |
    fad' mi' mi' |
    re'2 fad'8[ mi'] |
    re'2. |
  }
  \tag #'vtaille {
    \clef "vtaille" re'4 dod'8([ re']) mi'([ fad']) |
    mi'4 re'8([ mi']) fad'4 |
    re' re'8([ mi']) fad'4 |
    fad'2 fad'4 |
    si si mi' |
    dod'2. |
    si4 si si |
    lad2.\trill |
    re'4 re' re' |
    fad' fad' fad' |
    mi' mi'4. fad'8 |
    mi'2 \appoggiatura re'8 dod'4 |
    r re' fad' |
    fad' mi' re' |
    la2 sol4 |
    fad2.\trill |
    R2. |
    dod'4 dod' dod' |
    re' re' re' |
    mid' dod' dod' |
    dod'2 dod'4 |
    r dod' dod' |
    dod' dod' dod' |
    dod'2 si4 |
    lad2.\trill |
    R2.*4 |
    si4 si si |
    la sol fad |
    mi do' la |
    si2 si4 |
    mi'4 mi' mi' |
    red'2. |
    si4 do' si |
    si2. |
    dod'4 re' dod' |
    re' dod' fad' |
    mi' mi' mi' |
    mi'2 mi'4 |
    fad' fad' fad' |
    mi'2. |
    la4 re' fad' |
    mi'4. mi'8 mi' re' |
    dod'2.\trill |
    R2. |
    r4 dod' dod' |
    fad' mi' re' |
    re'2 dod'4 |
    re'2 r4 |
    R2.*11 |
    re'4 re' re' |
    dod'2. |
    re'4 re' si |
    lad2.\trill |
    si4 si si |
    sol' mi' fad' |
    si si si |
    fad2 fad4 |
    R2. |
    r4 si re' |
    si lad lad |
    si2 si4 |
    lad si dod' |
    si si si |
    si4( lad4.)\trill si8 |
    si2 r4 |
    R2.*2 |
    r4 re' mi' |
    fad' mi' re' |
    dod'2 dod'4 |
    dod' dod' lad |
    si si si |
    si( lad4.)\trill si8 |
    si2. |
  }
  \tag #'vbasse {
    \clef "vbasse" re'4 mi'8([ re']) dod'([ si]) |
    dod'4 re'8([ dod']) si([ lad]) |
    si4 si8([ dod']) re'([ mi']) |
    fad'2 fad'4 |
    si si si |
    la2. |
    sol4 sol sol |
    fad2. |
    re4 re re |
    re' re' re' |
    dod' dod'4. re'8 |
    la2 la4 |
    r4 fad re |
    si dod' re' |
    la2 la4 |
    re2. |
    fad'4 fad' fad' |
    mi' mi' mi' |
    re' re' re' |
    dod'2 dod'4 |
    r dod' dod' |
    si si si |
    la mid fad |
    dod'2 dod4 |
    fad2. |
    R2.*8 |
    mi'4 mi' mi |
    do'2. |
    sol4 la si |
    mi2. |
    la4 la la |
    re' la re' |
    re' re' re' |
    dod'2 dod'4 |
    re'4 re' re' |
    dod'2. |
    re'4 re' re' |
    dod'4. dod'8 dod' re' |
    la2. |
    R2. |
    r4 la fad |
    si dod' re' |
    la2 la4 |
    re2 r4 |
    R2.*11 |
    si4 si si |
    lad2. |
    si4 sol sol |
    fad2. |
    R2.*5 |
    r4 si si |
    mi fad fad |
    si2 si,4 |
    fad fad fad |
    si sol mi |
    fad2 fad4 |
    si,2 r4 |
    R2.*2 |
    r4 re' dod' |
    si si si |
    lad2 lad4 |
    lad lad fad |
    si sol mi |
    fad2 fad4 |
    si,2. |
  }
>>
