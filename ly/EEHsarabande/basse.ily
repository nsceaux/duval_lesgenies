\clef "basse"
\setMusic #'rondeau {
  si,4 si8 lad si dod' |
  re' re si, dod red si, |
  mi sol lad, dod fad, lad, |
  si, lad, si, re lad, dod |
  sol, mi fad, re mi, dod |
  re, fad16 mi re8 dod re dod16 si, |
  lad,8 dod si, si, re si, |
  fad,4. fad8 fad mi |
  re si, si lad si dod' |
  re' re si, dod red si, |
  mi sol lad, dod fad, lad, |
  si, lad, si, re lad, dod |
  sol, mi fad, re mi, dod |
  re, fad16 mi re8 dod re mi |
  fad4 fad,2 |
  si,2. |
}
\keepWithTag #'() \rondeau
re4 mi2 |
fad4. mi8 re4 |
sol8 mi la la, dod la, |
re la, si, dod re mi |
fad re sol mi la fad |
si, sol, dod la, re re, |
sol, mi, la,4 la,, |
re8 dod re mi fad fad, |
\rondeau
r8 si, re si, re si, |
dod si, la, sold, fad,4 |
dod8 dod' si la sold fad |
mid red dod red mid dod |
fad fad, si,4 re |
mid,8 dod, dod4. si,8 |
la, si, dod4 dod, |
fad,8 dod fad mi re dod |
\rondeau
