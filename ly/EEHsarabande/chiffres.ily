\setMusic #'rondeau {
  s2. <6>4 <_+>4.\figExtOn <_+>8\figExtOff <6 5>4 <6 5/>4.\figExtOn <6>8
  <"">4. <"">8\figExtOff <6>4 <6> <6 4> <6 4+>
  <6>8.\figExtOn <6>16\figExtOff s8 <6+> <6>4 <6>8*5\figExtOn <6>8\figExtOff
  <_+>8*5\figExtOn <_+>8\figExtOff
  <6>8*5\figExtOn <6>8\figExtOff <6>4 <_+>4.\figExtOn <_+>8\figExtOff
  <6 5>4 <6 5/> <7_+>8\figExtOn <7> <"">4. <"">8\figExtOff <6>4 <6> <6 4> <6 4+>
  <6>8.\figExtOn <6>16\figExtOff s8 <6\\ > <6>4 <4> <_+>2 s2.
}
\keepWithTag #'() \rondeau
<5>4 <6 5>2 <6>\figExtOn <6>4\figExtOff <6 5>4 <"">4.\figExtOn <"">8\figExtOff
<"">2\figExtOn <"">4\figExtOff <6>8 <7> s <7> s <7> s <7> s <7> s4 <9 7>8 <7> <4>4 <3> s2.
\rondeau
s4 <6 4+>4.\figExtOn <6>8 <_+> <_+>\figExtOff <6>2 <_+>2\figExtOn <_+>4 <6>2 <6>4\figExtOff s2 <4+ 3>4 <6> <6 4>4. <4+>8 <6>4 <_+> <7> <_+>2\figExtOn <_+>4\figExtOff
\rondeau
