\clef "dessus" \tag#'dessus2 \startHaraKiri
\setMusic #'rondeau {
  r8 fad' si' dod'' re'' mi'' |
  fad''2~ fad''8 mi''16 fad'' |
  sol''4 fad'' mi'' |
  re''2\trill dod''4 |
  mi'' re'' dod'' |
  si'4.\trill lad'8 si'4 |
  dod'' re'' \appoggiatura dod''8 si'4 |
  dod''4 si'8 lad' sold' lad' |
  fad'4 si'8 dod'' re'' mi'' |
  fad''2~ fad''8 mi''16 fad'' |
  sol''8 la''16 sol'' fad''8 sol''16 fad'' mi''8 fad''16 mi'' |
  re''2\trill dod''4 |
  mi'' re'' dod'' |
  si'4.\trill lad'8 si'4~ |
  si'8 dod'' dod''4.\trill si'8 |
  si'2. |
}
\keepWithTag #'() \rondeau
re''4 dod'' si' |
la'4. sol'8 fad'4 |
sol' \appoggiatura fad'8 mi'4. la'8 |
fad'2\trill \appoggiatura mi'8 re'4 |
la'4 si' dod'' |
re'' mi'' fad''4~ |
fad''8 sol'' mi''4.\trill re''8 |
re''2. |
\rondeau
<<
  \tag #'(dessus1 flute) {
    <>^"Flûte[s]" fad''4 si''4. la''8 |
    sold''4 dod'''4. fad''8 |
    mid''4. fad''8 sold''4 |
    dod''2. |
    la'4 re''4. si'8 |
    dod''4 fad''8 red'' \ficta sold'' mid'' |
    fad''8. sold''16 sold''4.\trill fad''8 |
    fad''2.
  }
  \tag #'(dessus2 violon) {
    <>^\markup\whiteout [Violons]
    \tag #'dessus2 \stopHaraKiri si'8 fad'' sold'' la'' sold'' fad'' |
    mid''4 la''4. la''8 |
    sold''4. fad''8 mid'' fad'' |
    sold''2. |
    dod''4 fad'4. sold'8 |
    la' si' la' fad' si' sold' |
    dod'' fad'' mid''4.\trill fad''8 |
    fad''2. | \tag #'dessus2 \startHaraKiri
  }
>>
\rondeau
