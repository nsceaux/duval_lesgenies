\clef "haute-contre"
\setMusic #'rondeau {
  fad'2 si'4~ |
  si'8 fad' red' mi' fad' sol'16 fad' |
  mi'2 dod'4 |
  si2 dod'4 |
  si2 lad4 |
  si8 re'16 mi' fad'4~ fad'8 mi'16 re' |
  dod'4 fad' fad' |
  fad'2. |
  fad'2 si'4~ |
  si'8 fad' red' mi' fad' sol'16 fad' |
  mi'2 dod'4 |
  si2 dod'4 |
  si2 lad4 |
  si8 re'16 mi' fad'4 si'~ |
  si' si' lad'\trill |
  si'2. |
}
\keepWithTag #'() \rondeau
la'4 sol' dod' |
re'4. mi'8 fad'4 |
mi' \appoggiatura re'8 dod'4. dod'8 |
re'2. |
re'2 dod'4 |
fad' mi' re'~ |
re'8 mi' dod'4.\trill re'8 |
re'2. |
\rondeau |
R2.*8 |
\rondeau |
