\piecePartSpecs
#`((dessus #:score-template "score-2dessus"
           #:music ,#{ s2.*40\break s2.*8\break #}
           #:instrument
           ,#{\markup\center-column { [Violons et Flûtes] }#})
   (flute-hautbois #:tag-notes flute)
   (dessus1 #:tag-notes violon)
   (dessus2 #:tag-notes violon)
   (parties)
   (dessus2-haute-contre)
   (taille)
   (basse #:instrument
          , #{ \markup\center-column { [Basses et B.C.] } #})
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
