\score {
  \new StaffGroup \with { \haraKiri } <<
    \new GrandStaff \with {
      instrumentName = \markup\center-column { [Violons et Flûtes] }
    } <<
      \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
      \new Staff \with { \haraKiriFirst } <<
        \global \keepWithTag #'dessus2 \includeNotes "dessus"
      >>
    >>
    \ifFull <<
      \new Staff \with {
        instrumentName = \markup\center-column { [Hautes-contre] }
      } << \global \includeNotes "haute-contre" >>
      \new Staff \with {
        instrumentName = "[Tailles]"
      } << \global \includeNotes "taille" >>
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion { s2.*16\break s2.*8\break s2.*16\break s2.*8\break }
      \origLayout {
        s2.*8\break s2.*8\break s2.*8\break s2.*8\break s2.*8\pageBreak
        s2.*8\break s2.*8\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}