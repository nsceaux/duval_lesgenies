\clef "taille"
\setMusic #'rondeau {
  re' re'8 dod' fad' mi' |
  re'4 fad si~ |
  si dod' fad |
  fad2. |
  mi4 fad fad |
  fad2. |
  fad8 mi re4 si |
  lad2.\trill |
  si4 re'8 dod' fad' mi' |
  re'4 fad si~ |
  si dod' fad |
  fad2. |
  mi4 fad fad |
  fad fad8 mi fad sol |
  fad2 fad4 |
  re2. |
}
\keepWithTag #'() \rondeau
fad4 sol sol |
la2 la4 |
si8 sol la2 |
la2. |
re'4 si la |
si sol la |
si la sol |
fad2.\trill |
\rondeau |
R2.*8 |
\rondeau |
