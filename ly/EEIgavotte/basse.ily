\clef "basse" sol4 sol |
re si do'8 si la do' |
si4 la8 sol fad4 re |
sol do re re, |
sol,2 r2 |
R1*3 |
r2 r |
R1*10 |
r2