\clef "dessus" si'4 do''8 si' |
la'4 re'' mi'' fad'' |
sol'' fad''8 mi'' re''4 do'' |
si' mi'' re''8 do'' si' la' |
si'4 sol' <<
  \tag #'(hautbois1 hautbois2 hautbois) {
    <>^\markup\whiteout [Hautbois]
    si' do''8 si' |
    la'4 re'' mi'' fad'' |
    sol'' fad''8 mi'' re''4 do'' |
    si'8 la' si' do'' dod''4.\trill re''8 |
    re''2
  }
  \tag #'violon {
    <>^\markup\whiteout Violons sol'4 sol' |
    re' si' do''8 si' la' do'' |
    si'4 la'8 sol' fad'4 re' |
    sol' mi' la' la |
    re'2
  }
>>
<<
  \twoVoices #'(hautbois1 hautbois2 hautbois) <<
    { la''4 si''8 sol'' |
      la''4 si''8 do''' si''4 la''8 sol'' |
      la'' sol'' fad'' mi'' re''4 sol''8 re'' |
      mi''4 re'' si' sol''8 fad'' |
      sol''4 re'' sol'8 la' si' do'' |
      re''4 do''8 si' mi'' re'' do'' si' |
      la'4 re' la'' si''8 sol'' |
      la''4 si''8 do''' si''4 la''8 sol'' |
      la'' sol'' fad'' mi'' re''4 sol''8 re'' |
      mi''4 re'' si' sol''8 re'' |
      mi'' re'' do'' si' la'4.\trill sol'8 |
      sol'2 }
    { fad''4 sol''8 mi'' |
      fad''4 sol''8 la'' sol''4 fad''8 mi'' |
      fad'' mi'' re'' do'' si'4 si' |
      do'' si' re'' re''8 do'' |
      si'4 sol'4. fad'8 sol' la' |
      si'4 la'8 sol' do'' si' la' sol' |
      fad'4 re' fad'' sol''8 mi'' |
      fad''4 sol''8 la'' sol''4 fad''8 mi'' |
      fad'' mi'' re'' do'' si'4 si' |
      do'' si' re'' si' |
      do''8 si' la' sol' fad'4.\trill sol'8 |
      sol'2 }
    { <>^\markup\whiteout [Hautbois] }
  >>
  \tag #'violon {
    <>^\markup\whiteout [Violons] re''4 sol' |
    re' re'' sol'' sol' |
    re'' re' sol' sol' |
    do' sol si'8 do'' si' la' |
    sol'4 sol si8 do' si la |
    sol4 sol' do'8 re' mi' do' |
    re'2 re''4 sol' |
    re' re'' sol'' sol' |
    re'' re' sol' sol' |
    do' sol sol' sol' |
    do' do' re' re' |
    sol2
  }
>>
