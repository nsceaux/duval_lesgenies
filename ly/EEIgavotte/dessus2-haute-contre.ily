\clef "haute-contre" sol' la'8 sol' |
fad'4 sol'2 la'4 |
sol' la'2 fad'4 |
sol' sol' sol'8 la' sol' fad' |
sol'2 sol'4 sol' |
re' si' do''8 si' la' do'' |
si'4 la'8 sol' fad'4 re' |
sol' mi' la' la |
re'2 re''4 sol' |
re' re'' sol'' sol' |
re'' re' sol' sol' |
do' sol si'8 do'' si' la' |
sol'4 sol si8 do' si la |
sol4 sol' do'8 re' mi' do' |
re'2 re''4 sol' |
re' re'' sol'' sol' |
re'' re' sol' sol' |
do' sol sol' sol' |
do' do' re' re' |
sol2
