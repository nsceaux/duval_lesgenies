\piecePartSpecs
#`((dessus #:score "score-dessus")
   (dessus1 #:tag-notes violon)
   (dessus2 #:tag-notes violon)
   (flute-hautbois #:tag-notes hautbois #:instrument "Hautbois")
   (parties)
   (dessus2-haute-contre #:notes "dessus2-haute-contre")
   (taille)
   (basse #:instrument
          , #{ \markup\center-column { [Basses, Bassons et B.C.] } #})
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
