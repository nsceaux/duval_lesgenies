\score {
  \new StaffGroup <<
    \new GrandStaff \with {
      instrumentName = "[Hautbois]"
      \haraKiriFirst
    } <<
      \new Staff << \global \keepWithTag #'hautbois1 \includeNotes "dessus" >>
      \new Staff <<
        \global \keepWithTag #'hautbois2 \includeNotes "dessus"
        { \startHaraKiri s2 s1*7 s2\break \stopHaraKiri }
      >>
    >>
    \new Staff \with { instrumentName = "[Violons]" } <<
      \global \keepWithTag #'violon \includeNotes "dessus"
    >>
  >>
  \layout { indent = \largeindent }
}
