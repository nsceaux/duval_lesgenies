\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = "[Hautbois]"
      \haraKiriFirst
    } << \global \keepWithTag #'hautbois2 \includeNotes "dessus" >>
    \new Staff \with { instrumentName = "[Violons]" } <<
      \global \keepWithTag #'violon \includeNotes "dessus"
    >>
  >>
  \layout { indent = \largeindent }
}
