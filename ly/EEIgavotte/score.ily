\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = "[Hautbois]"
      shortInstrumentName = "Htb."
    } << \global \keepWithTag #'hautbois \includeNotes "dessus" >>
    \new Staff \with {
      instrumentName = "[Violons]"
      shortInstrumentName = "Vln."
    } <<
      \global \keepWithTag #'violon \includeNotes "dessus"
    >>
    \ifFull <<
      \new Staff \with {
        instrumentName = \markup\center-column { [Hautes-contre] }
        shortInstrumentName = "H-c."
        \haraKiri
      } << \global \includeNotes "haute-contre" >>
      \new Staff \with {
        instrumentName = "[Tailles]"
        shortInstrumentName = "T."
        \haraKiri
      } << \global \includeNotes "taille" >>
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses, Bassons et B.C.] }
      shortInstrumentName = \markup\center-column { B. Bn. B.C. }
      \haraKiri
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion { s2 s1*7 s2\break }
      \origLayout { s2 s1*7 s2\pageBreak s2 s1*5\break }
    >>
  >>
  \layout {
    indent = \largeindent
    short-indent = 8\mm
  }
  \midi { }
}
