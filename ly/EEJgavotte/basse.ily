\clef "basse" sol4 sol, |
do re mib2 |
re2 r |
R1 |
r2 sol4 sol, |
do re mib2 |
re r |
R1 |
r2 sib4 fa |
sib, sib la sib |
fa mib re4 do8 sib, |
re4 do8 sib, re4 mib |
fa8 sol la sib do'4 do8 re |
mib fa sol la sib mib fa fa, |
sib, do sib, la, sol,4 sol, |
do re mib2 |
re4 re'8 do' sib la sol4 |
re, re8 do sib,4 fad, |
sol,2 r |
R1*3 |
r2 sib8 do' la sib |
do' re' sib do' la sib sol la |
fad4 re r2 |
R1 |
r2 sib8 do' la sib |
do' re' sib do' la sib sol la |
fad4 re r2 |
R1 |
r2 sib4 fa |
sib, sib la sib |
fa2 r |
R1 |
r2 do'4 do8 re |
mib fa sol la sib mib fa fa, |
sib,2 sol4 sol, |
do re mib2 |
re r2 |
R1 |
r2 sol4 sol, |
do re mib2 |
re r |
R1 |
r2
