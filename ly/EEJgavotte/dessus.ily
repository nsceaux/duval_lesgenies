\clef "dessus" re''4 re'' |
mib'' re'' do''8\trill sib' do''4 |
<<
  \tag #'dessus1 {
    re''2 sol''8 la'' sib''4 |
    la'' sol''8 fad'' sol''4 la'' |
    re'' re'
  }
  \tag #'dessus2 {
    re'' \origVersion\clef "dessus2" re''8 do'' sib' la' sol'4 |
    re' re''8 do'' sib'4 fad' |
    sol'8 la' fad' la' \origVersion\clef "dessus"
  }
>> re''4 re'' |
mib'' re'' do''8\trill sib' do''4 |
<<
  \tag #'dessus1 {
    re''2 sol''8 la'' sib''4 |
    la'' sol''8 fa'' mi''4.\trill re''8 |
    re''2
  }
  \tag #'dessus2 {
    re''4 \origVersion\clef "dessus2" re''8 do'' sib' la' sol'4 |
    re'' sol' la' la |
    re'2 \origVersion\clef "dessus"
  }
>> fa''4 fa'' |
fa'' re'' mib'' re'' |
do'' fa' fa''8 sol'' la'' sib'' |
fa'' sol'' la'' sib'' fa''4 mib''8 re'' |
do''2\trill do''8 re'' mib'' fa'' |
sol'' la'' sib'' mib'' re''4 do''\trill |
sib'2 re''4 re'' |
mib'' re'' do''8\trill sib' do''4 |
re''2 sol''8 la'' sib''4 |
la'' sol''8 fad'' sol''4 la'' |
re'' re' <<
  \tag #'dessus1 {
    re''4 re'' |
    mib'' re'' do''8 sib' do''4 |
    re''2 sol''4 la''8 sib'' |
    la'' fad'' sol'' la'' la''4.\trill sol''8 |
    sol''4 sol'
  }
  \tag #'dessus2 {
    \origVersion\clef "dessus2" sol'4 sol |
    do' re' mib'2 |
    re'4 re''8 do'' sib' la' sol'4 |
    mib'' do'' re'' re' |
    sol'2 \origVersion\clef "dessus"
  }
>> re''8 mib'' do'' re'' |
mib'' fa'' re'' mib'' do'' re'' sib' do'' |
re''4 <<
  \tag #'dessus1 {
    re'4 sol''8 la'' sib'' sol'' |
    la''4 sib''8 la'' sol'' fad'' sol'' la'' |
    re''4 re'
  }
  \tag #'dessus2 {
    \origVersion\clef "dessus2" re'8 do' sib8 fad'' sol'' mi'' |
    fad'' re'' sol' la' sib' la' sib' do'' |
    sib'4 la' \origVersion\clef "dessus"
  }
>> re''8 mib'' do'' re'' |
mib'' fa'' re'' mib'' do'' re'' sib' do'' |
<<
  \tag #'dessus1 {
    re''2 sol''8 la'' sib'' sol'' |
    la'' sib'' sol'' la'' mi''4.\trill re''8 |
    re''2
  }
  \tag #'dessus2 {
    re''4 \origVersion\clef "dessus2" re'8 do' sib8 fad'' sol'' mi'' |
    fa''! fa'' mi'' re'' dod''4.\trill re''8 |
    re''2 \origVersion\clef "dessus"
  }
>> fa''4 fa'' |
fa''8 mib'' re'' do'' mib'' fa'' re'' mib'' |
do''4 <<
  \tag #'dessus1 {
    fa'4 fa''8 sol'' la'' sib'' |
    fa'' sol'' la'' sib'' fa''8 sol''16 fa'' mib''8 re'' |
    do''2\trill
  }
  \tag #'dessus2 {
    \origVersion\clef "dessus2" fa''8 mib'' re''4 do''8 sib' |
    re''4 do''8 sib' re'4 mib' |
    fa'8 sol' la' sib' \origVersion\clef "dessus"
  }
>> do''8 re'' mib'' fa'' |
sol'' la'' sib'' mib'' re'' mib'' do'' re'' |
sib'2 re''4 re'' |
mib'' re'' do''8\trill sib' do''4 |
<<
  \tag #'dessus1 {
    re''2 sol''8 la'' sib'' sol'' |
    la'' sib'' sol'' la'' fad'' sol'' la''4 |
    re'' re'
  }
  \tag #'dessus2 {
    re''4 \origVersion\clef "dessus2" re''8 do'' sib'8 la' sol'4 |
    fad' sol' re'8 mi' fad' re' |
    sol'4 sol' \origVersion\clef "dessus"
  }
>> re''8 mib'' do'' re'' |
mib'' fa'' re'' mib'' do'' re'' mib'' do'' |
<<
  \tag #'dessus1 {
    re''2 sol''8 la'' sib'' do''' |
    la'' fad'' sol'' la'' la''4.\trill sol''8 |
    sol''2
  }
  \tag #'dessus2 {
    re''4 \origVersion\clef "dessus2" re''8 do'' sib' la' sol'4 |
    mib''4 sib'8 do'' re''4 re' |
    sol'2
  }
>>
