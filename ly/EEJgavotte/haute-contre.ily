\clef "haute-contre" sol'4 sol' |
sol' fad' sol'2 |
la' r |
R1 |
r2 sol'4 sol' |
sol' fad' sol'2 |
la' r |
R1 |
r2 sib'4 la' |
sib' fa' fa' fa' |
fa'2 sib' |
sib' sib'4 do''8 sib' |
la'2\trill sol'4 sol' |
sol'4. do''8 sib'4 la'\trill |
sib'2 sol'4 sol' |
sol' fad' sol'2 |
la' sol'4 sol' |
fad' sol'8 la' sol'4 re' |
re'2 r |
R1*3 |
r2 fa'4 fa' |
sol'2 fad'4 sol' |
la'2 r |
R1 |
r2 fa'4 fa' |
sol'2 fad'4 sol' |
la'2 r |
R1 |
r2 fa''4 mib'' |
re''8 do'' sib' la' do'' re'' sib' do'' |
la'2\trill r |
R1 |
r2 sol'4 sol' |
sol'4. do''8 sib' do'' la' sib' |
sib'2 sol'4 sol' |
sol' fad' sol'2 |
la' r |
R1 |
r2 sol'4 sol' |
sol' fad' sol'2 |
la' r |
R1 |
r2
