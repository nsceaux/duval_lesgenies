\piecePartSpecs
#`((dessus #:score "score-dessus")
   (flute-hautbois #:instrument "Flûtes"
                   #:tag-notes dessus1)
   (dessus1 #:tag-notes dessus2)
   (dessus2 #:tag-notes dessus2)
   (parties)
   (dessus2-haute-contre)
   (taille)
   (basse #:instrument
          , #{ \markup\center-column { [Basses et B.C.] } #})
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
