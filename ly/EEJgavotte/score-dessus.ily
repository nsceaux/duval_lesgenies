\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Flûtes" } <<
      \global \keepWithTag #'dessus1 \includeNotes "dessus"
    >>
    \new Staff \with { instrumentName = "Violons" } <<
      \global \keepWithTag #'dessus2 \includeNotes "dessus"
    >>
  >>
  \layout { indent = \largeindent }
}
