\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Flûtes" } <<
      \global \keepWithTag #'dessus1 \includeNotes "dessus"
    >>
    \new Staff \with { instrumentName = "Violons" } <<
      \global \keepWithTag #'dessus2 \includeNotes "dessus"
    >>
    \ifFull <<
      \new Staff \with {
        instrumentName = \markup\center-column { [Hautes-contre] }
      } << \global \includeNotes "haute-contre" >>
      \new Staff \with {
        instrumentName = "[Tailles]"
      } << \global \includeNotes "taille" >>
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses, Bassons et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2 s1*7\break s1*8\break s1*8\pageBreak
        s1*6 s2 \bar "" \break s2 s1*7\break s1*6 s2
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
