\clef "taille" sib4 sib |
la la sol2 |
fad\trill r |
R1 |
r2 sib4 sib |
la la sol2 |
fad\trill r |
R1 |
r2 re'4 do' |
re' sib do' sib |
la2\trill fa'4 mib'8 re' |
fa'4 mib'8 re' fa'4 fa' |
fa'2 mib'8 fa' mib' re' |
do'4. fa'8 fa' sol' fa' mib' |
re'2\trill sib4 sib |
la la sol2 |
fad\trill re'4 re' |
re'2 re'4 do' |
sib2 r |
R1*3 |
r2 sib4 do' |
do' re' re' re' |
re'2 r |
R1 |
r2 sib4 do' |
do' re' re' re' |
re'2 r |
R1 |
r2 re'4 do' |
sib fa' fa' fa' |
fa'2 r |
R1 |
r2 mib'4 mib'8 re' |
do'2 fa'8 mib' mib'4 |
re'2\trill sib4 sib |
la la sol2 |
fad\trill r |
R1 |
r2 sib4 sib |
la la sol2 |
fad\trill r |
R1 |
r2
