\clef "basse" re2. dod4 |
re2. |
re4. do8 sib, sol, la, sol, |
fa,4. fa8 mi re |
dod2 la,4 |
re2 mi4 |
fa sol2 |
la la4 |
dod2 re4 |
sol2 la4 |
sib sol la |
re2 sol4 |
dod2. |
re |
sib4 la8 sol fa sol |
la4 la,2 |
re2 re'8 do' |
sib4. la8 sol16 fa mi re |
dod8 la, re,4 re2~ | \allowPageTurn
re dod4 |
re2 fa8 re mi sold |
la re mi4 mi, |
la,8 la sol fa16 mi re4 fad |
sol si, do do'8 sib |
la fa sol4 sol, |
do1~ do4 fa8 re |
mi4 re8 do si, la, |
mi re mi fa mi re |
do4 re8 do si, la, |
sold,4. mi8 la si |
do'4. do'8 si la |
mi'2 mi'4 |
re'2. | \allowPageTurn
do'4 re' mi' mi |
la,2 re |
mi2 r8 re |
do la, r8 la4 r8 |
si4 r8 do'4 la8 |
si4 sol8 la si si, |
mi4 mi,8 mi, mi re |
do si, la, sold,4 mi,8 |
la,8. mi16 fad sold la8 la, si, |
do8 dod4 re4. |
red4 si,8 mi4 re?8 |
do si, la, sold,4 mi,8 |
la,8. mi16 la si do'8. si16 do' la |
re'8. do'16 si la mi'4 mi8 |
la la, la16 si do'4 do8 |
re mi fa re mi mi, |
la,2 \allowPageTurn la4 |
sold2 mi4 |
la8 sold la la, la4 |
sold8 fad sold la si4 |
dod'8 re' dod' si dod' la |
si la si4 si, |
mi2 mi4 |
mi2.~ |
mi~ |
mi~ | \allowPageTurn
mi2 dod8 la, |
re la, re,4 re' |
dod'2 la4 |
re8 dod si, sold, la, re |
mi4 mi,2 |
la,8 mi fad sold la do' la do' |
sold mi fad sold la la, sol? sol, |
fa mi re do sol sol, si, sol, |
do4 re8 do si,4 do8 re |
do si, la, la sold4 mi |
la8 la, do' si la sol fa mi |
re4 mi fa2 |
mi8 re do si, la, do' la do' |
mi2 mi'4 re' |
do'8 re' mi' do' si la si sol |
do' sol do do' si la si sol |
do' sol do do' si la si sol |
do'4 do2 do8 re |
mi8 do fa re sol4 sol, |
do8 do mi sol do'4 si |
la8 do' si la sold mi la la, |
do la, re do si,4 do8 re |
do si, re do si,4 mi |
la,8 mi fad sold la mi do si, |
la,4 fa8 re mi4 mi, |
la, la8 si do'4 do8 si, |
la,4 fa8 re mi4 mi, |
la,2 \allowPageTurn la4 la |
re'8 si dod' la si sold la la, |
mi2. re4 |
dod8 si, dod la, si,2 |
mi8 fad sold mi la4 la |
re'8 si dod' la si sold la la, |
mi2. re4 |
dod8 si, dod la, si,2 |
mi dod4 si,8 la, |
re si, mi dod fad mi fad re |
mi4 re8 dod si,4 la, |
sold,2. mi,4 |
la,8 si, dod la, mi fad mi re |
dod mi re dod si,4 la, |
mi8 fad sold mi la si dod' la |
re' dod' si la sold4 la8 re |
mi re dod re mi4 mi, |
la,2. | \allowPageTurn
la16 sol fad mi re2 |
mi4 sold, la,2 |
si,4 si la |
sold8 la si4 si, |
mi1 | \allowPageTurn
la4 fad fadd2 |
sold1~ | \allowPageTurn
sold2 sid |
dod'2 dod4 |
fad4. sold8 la fad |
sold dod sold,2 |
dod dod'4 |
dod' dod red |
mi red dod |
sold2 fad4 |
mi mid2 |
fad8 sold la2 |
sold4 sold, r |
r4 r r8 mi |
la sold la si dod' la |
si4 red si, |
mi8 fad sold la si mi |
si,4. si8 mi' mi |
la2 lad4 |
si2 la4 |
sold8 fad mi4 si, |
mi,2 mi4 |
la2 lad4 |
si8 mi si,2 |
mi,4. \allowPageTurn mi |
mi si4 si8 |
dod' mi' red' dod' sold mi |
la fad mi red mi la, |
si,4 fad8 si4 si,8 |
mi4 mi,8 mi red dod |
fad4 r8 si,4 r8 |
mi,4 mi8 la16 sold fad8 mi |
si si, si, si sold4 |
la8 la, la, la fad16 sold la fad |
si4.~ si |
si, si, |
si si |
si, si, |
si si,4 si8 |
sold dod' la fad si sold |
la16 sold fad8 mi si si,4 |
mi4.
