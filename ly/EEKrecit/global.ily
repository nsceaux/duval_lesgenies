\key do \major
\once\omit Staff.TimeSignature
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 \midiTempo#160 s2.*13 \midiTempo#80 s2.*2
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\time 3/2 \midiTempo#160 s1.
\digitTime\time 3/4 \midiTempo#80 s2.*7
\digitTime\time 2/2 s1
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 6/8 s2.*13
\digitTime\time 3/4 \midiTempo#160 s2 \bar "||"
\beginMarkSmall "Air gai" \key la \major
\bar ".!:" s4 s2.*5 s2 \bar ":|." s4 s2.*8
\digitTime\time 2/2 s2 \bar "||"
\beginMarkSmall "Air" \key la \minor s2
\bar ".|:" s1*6 \alternatives s1 s1 s1*13 s2 \bar "||"
\beginMarkSmall "Air" \key la \major s2 s1*16
\digitTime\time 3/4 \midiTempo#80 s2.*2
\time 4/4 s1
\digitTime\time 3/4 s2.*2
\time 4/4 \grace s8 s1
\digitTime\time 2/2 \midiTempo#160 s1 \midiTempo#80 s1*2
\digitTime\time 3/4 \midiTempo#120 \grace s8 s2.*21
\time 6/8 s4. \bar "||"
\beginMarkSmall "Duo" \key mi \major s4. s2.*16 s4. \bar "|."
