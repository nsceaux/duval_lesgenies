\tag #'(voix1 basse) {
  Vous ne pa -- rais -- sez point cher ob -- jet que j’a -- do -- re,
  quel -- que ri -- val ja -- loux re -- tien -- drait- il vos pas ?
  Sans vous, ce beau sé -- jour est pour moi sans ap -- pas.
  Ve -- nez cal -- mer le feu qui me dé -- vo -- re.
  Ve -- nez cal -- mer le feu qui me dé -- vo -- re.
  
  Et quelle est la beau -- té qui cau -- se vos sou -- pirs ?
  
  Je l’ai vue un mo -- ment, mo -- ment trop re -- dou -- ta -- ble
  pour la per -- te d’un cœur qu’a -- mu -- saient les plai -- sirs !
  Sans fi -- xer mon a -- mour, les plus ten -- dres dé -- sir
  sem -- blaient me rendre heu -- reux près d’un ob -- jet ai -- ma -- ble.
  Mais, hé -- las ! de -- puis cet ins -- tant
  les soins m’ac -- com -- pa -- gnent sans ces -- se,
  et j’é -- prou -- ve dans ma ten -- dres -- se,
  que mon plai -- sir est mon tour -- ment.
  Flo -- ri -- se cau -- se mon mar -- ty -- re.
  
  Je la con -- nais. Cet -- te jeu -- ne beau -- té
  n’ai -- me pas un cœur qui sou -- pi -- re ;
  l’a -- mant qui fo -- lâ -- tre l’at -- ti -- re,
  et l’a -- mant qui se plaint est tou -- jours re -- bu -- té,
  l’a -- mant qui fo -- lâ -- tre l’at -- ti -- re,
  et l’a -- mant qui se plaint est tou -- jours re -- bu -- té,
  et l’a -- mant qui se plaint est tou -- jours re -- bu -- té.
  
  Je sais ac -- com -- mo -- der ma chaî -- ne
  aux ca -- pri -- ces d’un cœur dont je suis en -- chan -- té ;
  et pour vain -- cre sa cru -- au -- té,
  je ne comp -- te pour rien la pei -- ne,
  et pour vain -- cre sa cru -- au -- té,
  je ne comp -- te pour rien la pei -- ne.
  
  Elle aime un cœur cons -- tant ;
  quel -- que fois un vo -- la -- ge
  pour le plai -- sir du chan -- ge -- ment :
  Elle -ment :
  pour vous faire à son ba -- di -- na -- ge,
  ê -- tes- vous l’un et l’autre a -- mant ?
  Pour vous faire à son ba -- di -- na -- ge,
  ê -- tes- vous l’un et l’autre a -- mant,
  ê -- tes- vous l’un et l’autre a -- mant ?
  
  L’in -- cons -- tance est mon par -- ta -- ge,
  je ne suis cons -- tant qu’à re -- gret ;
  l’in -- cons -- tance est mon par -- ta -- ge,
  je ne suis cons -- tant qu’à re -- gret ;
  mais pour char -- mez un bel ob -- jet,
  la cons -- tance est mon tendre hom -- ma -- ge,
  mais pour char -- mez un bel ob -- jet,
  la cons -- tance est mon tendre hom -- ma -- ge.
  
  Vous ê -- tes ce qu’il faut pour plaire à ses beaux yeux,
  mais de son cœur el -- le n’est plus maî -- tres -- se ;
  et son a -- mant est dans ces lieux.
  
  Ah ! de quel coup mor -- tel frap -- pez- vous ma ten -- dres -- se !
  
  Do -- rante ap -- pro -- chez- vous, digne ob -- jet de mes vœux,
  Flo -- ri -- se veut vous rendre heu -- reux.
}

\tag #'(voix1 voix2 basse) { Ô ciel ! }

\tag #'(voix1 basse) {
  Je vous ai trom -- pé l’un et l’au -- tre,
  mais c’est pour mieux ser -- rer vos __ nœuds ;
  ai -- mez, que votre a -- mour puisse i -- mi -- ter le nô -- tre,
  ja -- mais rien n’é -- tein -- dra vos feux.
  Ai -- mez, ai -- mez, que votre a -- mour puisse i -- mi -- ter le nô -- tre,
  ja -- mais rien n’é -- tein -- dra vos feux.
}
\tag #'(voix1 basse) {
  Sui -- vons cet e -- xem -- ple sans pei -- ne,
  ai -- mons pour ne ja -- mais chan -- ger ;
  le plai -- sir,
  le plai -- sir de se dé -- ga -- ger
  ne vaut pas le plai -- sir de re -- pren -- dre sa chaî -- ne.
  Le plai -- sir, __
  Le plai -- sir de se dé -- ga -- ger
  ne vaut pas le plai -- sir de re -- pren -- dre sa chaî -- ne,
  ne vaut pas le plai -- sir de re -- pren -- dre sa chaî -- ne.
}
\tag #'voix2 {
  Sui -- vons cet e -- xem -- ple sans pei -- ne,
  ai -- mons pour ne ja -- mais chan -- ger ; __
  \tag #'(voix1 basse) { le plai -- sir, }
  le plai -- sir de se dé -- ga -- ger
  ne vaut pas le plai -- sir de re -- pren -- dre sa chaî -- ne.
  Le plai -- sir, __
  Le plai -- sir de se dé -- ga -- ger
  ne vaut pas le plai -- sir,
  ne vaut pas le plai -- sir de re -- pren -- dre sa chaî -- ne,
  ne vaut pas le plai -- sir de re -- pren -- dre sa chaî -- ne.
}
