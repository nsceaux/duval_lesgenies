\piecePartSpecs
#`((basse #:score-template "score-basse-continue-voix"
          #:instrument "[B.C.]")
   (silence #:on-the-fly-markup , #{
\markup\fontsize#-3 \fill-line {
  \column {
    \livretPers Le Sylphe
    \livretVerse#12 { Vous ne paraissez point cher objet que j’adore, }
    \livretVerse#12 { Quelque rival jaloux retiendrait-il vos pas ? }
    \livretVerse#12 { Sans vous, ce beau séjour est pour moi sans appas. }
    \livretVerse#10 { Venez calmer le feu qui me dévore. }
    \livretPersDidas Florise masquée.
    \livretVerse#12 { Et quelle est la beauté qui cause vos soupirs ? }
    \livretPers Le Sylphe
    \livretVerse#12 { Je l’ai vue un moment, moment trop redoutable }
    \livretVerse#12 { Pour la perte d’un cœur qu’amusaient les plaisirs ! }
    \livretVerse#12 { Sans fixer mon amour, les plus tendres désir }
    \livretVerse#12 { Semblaient me rendre heureux près d’un objet aimable. }
    \livretVerse#8 { Mais, hélas ! depuis cet instant }
    \livretVerse#8 { Les soins m’accompagnent sans cesse, }
    \livretVerse#8 { Et j’éprouve dans ma tendresse, }
    \livretVerse#8 { Que mon plaisir est mon tourment. }
    \livretVerse#8 { Florise cause mon martyre. }
    \livretPers Florise
    \livretVerse#10 { Je la connais. Cette jeune beauté }
    \livretVerse#8 { N’aime pas un cœur qui soupire ; }
    \livretVerse#8 { L’amant qui folâtre l’attire, }
    \livretVerse#12 { Et l’amant qui se plaint est toujours rebuté. }
    \livretPers Le Sylphe
    \livretVerse#8 { Je sais accommoder ma chaîne }
    \livretVerse#12 { Aux caprices d’un cœur dont je suis enchanté ; }
    \livretVerse#8 { Et pour vaincre sa cruauté, }
    \livretVerse#8 { Je ne compte pour rien la peine. }
    \livretPers Florise
    \livretVerse#6 { Elle aime un cœur constant ; }
    \livretVerse#6 { Quelque fois un volage }
  }
  \column {
    \livretVerse#8 { Pour le plaisir du changement : }
    \livretVerse#8 { Pour vous faire à son badinage, }
    \livretVerse#8 { Êtes-vous l’un & l’autre amant ? }
    \livretPers Le Sylphe
    \livretVerse#7 { L’inconstance est mon partage, }
    \livretVerse#8 { Je ne suis constant qu’à regret ; }
    \livretVerse#8 { Mais pour charmez un bel objet, }
    \livretVerse#8 { La constance est mon tendre hommage. }
    \livretPers Florise
    \livretVerse#12 { Vous êtes ce qu’il faut pour plaire à ses beaux yeux, }
    \livretVerse#10 { Mais de son cœur elle n’est plus maîtresse ; }
    \livretVerse#8 { Et son amant est dans ces lieux. }
    \livretPers Le Sylphe
    \livretVerse#12 { Ah ! de quel coup mortel frappez-vous ma tendresse ! }
    \livretPersDidas Florise, se démasquant, & parlant à un masque du bal.
    \livretVerse#12 { Dorante approchez-vous, digne objet de mes vœux, }
    \livretVerse#8 { Florise veut vous rendre heureux. }
    \livretPers Le Sylphe et la Sylphide
    \livretVerse#10 { Ô ciel ! }
    \livretPers Florise
    \livretVerse#10 { \transparent { Ô ciel ! } Je vous ai trompé l’un & l’autre, }
    \livretVerse#8 { Mais c’est pour mieux serrer vos nœuds ; }
    \livretVerse#12 { Aimez, que votre amour puisse imiter le nôtre, }
    \livretVerse#8 { Jamais rien n’éteindra vos feux. }
    \livretPers Le Sylphe et la Sylphide
    \livretVerse#8 { Suivons cet exemple sans peine, }
    \livretVerse#8 { Aimons pour ne jamais changer ; }
    \livretVerse#8 { Le plaisir de se dégager }
    \livretVerse#12 { Ne vaut pas le plaisir de reprendre sa chaîne. }
  }
}#}))
