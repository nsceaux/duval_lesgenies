\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix1 \includeNotes "voix"
    >> \keepWithTag #'voix1 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix2 \includeNotes "voix"
    >> \keepWithTag #'voix2 \includeLyrics "paroles"
    \new Staff \with { instrumentName = "[B.C.]" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1 s2. s2. \bar "" \break s4 s2.*5\break s2.*7\pageBreak
        s2.*3 s1\break s2. s1 s2.\break s1 s2 \bar "" \pageBreak
        \grace s8 s2 s2. s1. s2.\break s2.*5\break s2. s1*2\pageBreak
        s2.*4\break s2.*4\break s2.*3\pageBreak
        s2.*3\break s2.*5\break s2.*5\pageBreak
        s2.*5 s1\break s1*5\break s1*7\pageBreak
        s1*6\break s1*4\break s1*5\pageBreak
        s1*5\break s1*4\break s1*2 s2.*2\pageBreak
        s1\break s2.*2 s1\break s1*3\pageBreak
        \grace s8 s2.*5\break s2.*5\pageBreak
        s2.*3 s4. \bar "" \break s4. s2.*3 s2 \bar "" \break s4 s2.*3 s4. \bar "" \pageBreak
        s4. s2.*4\break s2.*4\pageBreak
        s2.*4\break s2.*4
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
