<<
  \tag #'(voix1 basse) {
    \ffclef "vhaute-contre" <>^\markup\character Le Sylphe
    r8 re' re'16 re' mi' fa' mi'8\trill mi'16 fa' sol'8 fa'16 mi' |
    fa'2 \appoggiatura mi'8 re'4 |
    la'8 sol'16 fa' mi'8. re'16 sol'8. fa'16 mi'16 re' dod' mi' |
    la2 la'4 |
    \appoggiatura la'8 sol'4.\trill fa'8 sol' \appoggiatura fa' mi' |
    \appoggiatura mi' fa'4. fa'8 sol'4 |
    la' \appoggiatura sib'8 la'4 \appoggiatura sol'8 fa'4 |
    mi'2 r8 mi' |
    mi'2 fa'4 |
    \appoggiatura fa'8 mi'2 fa'4 |
    sol'4. fa'8 mi' la' |
    fa'4\trill \appoggiatura mi'8 re'4 r8 sib' |
    sib'2 la'4 |
    la'2 fa'4 |
    sol'4. dod'8 re' mi' |
    fa'4( mi'2)\trill |
    re'2
    \ffclef "vdessus" <>^\markup\character-text Florise masquée
    fa''8 mi'' |
    re'' mi''16 fa'' sol''8. fa''16 mi'' re'' dod'' re'' |
    la'4
    \ffclef "vhaute-contre" <>^\markup\character Le Sylphe
    fa'8 sol' la'4 la'8 la' |
    re' sib' sol'4\trill sol'8 fa'16 mi' |
    fa'4 \appoggiatura mi'8 re' fa'16 mi' re'8 re'16 do' si8 mi'16 si |
    do'4 si4.\trill la8 | \override Staff.AccidentalSuggestion.avoid-slur = #'outside
    la mi'16 fa' dod'8 re'16 mi' \appoggiatura mi'8 fa' la16 si \appoggiatura si8 \ficta do' do'16 re' |
    si8.\trill re'16 sol' fa' mi' re' \appoggiatura re'8 mi'4 mi'8 fa'16 sol' |
    do'8 re' mi'4( re')\trill |
    do'4 mi'8. re'16 do'4. re'8 mi'4 la8 si |
    sold2\trill r8 la |
    si2 do'8 re' |
    mi'4 fa'8[ mi'] re'[ do'] |
    re'4 si\trill r8 do'16 re' |
    mi'4 mi'8 fad' sold' la' |
    sold'2\trill \appoggiatura fad'8 mi'4 |
    mi'2 fad'8 sold' |
    la'4.*5/6 \ficta sold'16[ fad' sold'!] sold'4.\trill la'8 |
    la'4 r16 mi' do' la fa'4 mi'8 re'16 do' |
    si4\trill si
    \ffclef "vdessus" <>^\markup\character Florise
    r16 do'' re'' mi'' |
    la'8 r mi''16 re'' do''8 re''16[ do''] si'[ la'] |
    re''8 mi''16[ re''] do''[ si'] mi''4 fad''8 |
    red''8\trill si'8. mi''16 mi''4( red''8) |
    mi''4.~ mi''4 r16 mi' |
    la'8 sold'8.\trill la'16 si'[ la'] si'[ do''] re''[ si'] |
    do''4. \appoggiatura si'8 la'8. do''16 re''8 |
    mi''8. la'16 sol'8 fad'8. sold'16 la'8 |
    si'8. do''16 \appoggiatura si'8 la' sold'4 r16 mi' |
    la'8 sold'8. la'16 si'[ la'] si'[ do''] re''[ si'] |
    do''8 \appoggiatura si' la' do''16 re'' mi''8 mi''8. mi''16 |
    mi''4 re''16 mi'' do''8.\trill si'16 la'8 |
    la'4 do''16 re'' mi''8 mi'' mi'' |
    mi''4 re''16 mi'' do''8.\trill si'16 la'8 |
    la'2
    \ffclef "vhaute-contre" <>^\markup\character Le Sylphe
    dod'4 |
    si8 la si dod' re' mi' |
    dod'4\trill \appoggiatura si8 la4 dod'8 re' |
    mi'4. fad'8 sold'4 |
    la'2 sold'8 fad' |
    sold'4 fad'4.\trill mi'8 |
    mi'2 si8 dod' |
    re'4 dod'8 si dod' re' |
    \appoggiatura re' mi'2 re'8 dod' |
    si4 dod'8 re' dod' la |
    si4 sold\trill mi'8 dod' |
    fad'4 fad'8 mi' fad' sold' |
    la'2 mi'8 dod' |
    fad'4 re'8 si dod' re' |
    dod'4( si2)\trill |
    la2.
    \ffclef "vdessus" <>^\markup\character Florise
    r8 la'' |
    mi''4 re'' do'' si' |
    la' fa''8 mi'' re''4 do''8 si' |
    mi''4\melisma fa''8[ mi''] re''4 do''8[ si'] |
    mi''4 fa''8[ mi''] re''[ do'' si' mi'']( |
    do''4)\melismaEnd \appoggiatura si'8 la'4 do'' re''8 mi'' |
    fa''4 mi'' re''4.\trill mi''8 |
    mi''2. r8 la'' |
    mi''2 do''4 re'' |
    mi'' mi''8 fa'' sol''4 re'' |
    mi''8[\melisma fa'' mi'' fa''] sol''4 re'' |
    mi''8[ fa'' mi'' fa''] sol''[ fa'' mi'' re'']( |
    mi''2)\trill\melismaEnd \appoggiatura re''8 do''4 mi''8 fa'' |
    sol''4 fa''8 mi'' re''4.\trill do''8 |
    do''2 mi''4 re'' |
    do'' re''8 do'' si'4 la' |
    mi''4\melisma fa''8[ mi''] re''4 do''8[ si'] |
    mi''4 fa''8[ mi'' re'' do'' si' mi'']( |
    do''2)\trill\melismaEnd la'4 do''8 re'' |
    mi''4 re''8 do'' si'4.\trill la'8 |
    la'2 r4 la'8 si' |
    do''4 re''8 mi'' si'4.\trill la'8 |
    la'2
    \ffclef "vhaute-contre" <>^\markup\character Le Sylphe
    dod'4 la |
    fad' mi' re' dod' |
    si mi mi'8 fad' sold' mi' |
    la'2 fad'4.\trill mi'8 |
    mi'2 dod'4 la |
    fad' mi' re' dod' |
    si mi mi'8 fad' sold' mi' |
    la'2 fad'4.\trill mi'8 |
    mi'2 mi'4 re'8 dod' |
    fad'4 mi' re' dod' |
    si2.\trill dod'8 re' |
    mi'2 re'8 dod' si mi' |
    dod'4\trill \appoggiatura si8 la4 si dod'8 re' |
    mi'4 fad' sold' la' |
    si'2 r4 la'8 mi' |
    fad'2 re'8 si dod' re' |
    dod'2( si)\trill |
    la2
    \ffclef "vdessus" <>^\markup\character Florise
    r8 mi'' |
    dod''16\trill dod'' re'' mi'' \appoggiatura mi''8 fad'' mi'' re''16 dod'' si' la' |
    sold'4\trill si'8 dod''16 re'' dod''8.\trill dod''16 dod'' dod'' red'' mi'' |
    red''4\trill red'' si'8 dod''16 red'' |
    mi''4. mi''8 mi'' red'' |
    \appoggiatura red''8 mi''4
    \ffclef "vhaute-contre" <>^\markup\character Le Sylphe
    sold'4~ sold'8 mi'16 mi' sold'8 si' |
    mi'4 la'8 fad' red'4\trill red'8 red' |
    sid4\trill sid8
    \ffclef "vdessus" <>^\markup\character Florise
    red''8 sold''8. sid'16 dod''8 red'' |
    sold'4 r8 red''16 mi'' fad''4 mi''8. red''16 |
    \appoggiatura red''8 mi''2 r8 sold'' |
    sold''2 fad''4 |
    red''8 mi'' red''4. dod''8 |
    dod''
    \ffclef "vdessus" <>^\markup\character La Sylph.
    dod''8 mid''2 |
    \ffclef "vdessus" <>^\markup\character Florise
    r4 mi''!8 re'' dod'' sid' |
    dod''4 red''4. mi''8 |
    red''2 sold'4 |
    dod'' si'8 la' sold' fad' |
    la'4 fad'4.(\trill mi'16[ fad']) |
    sold'2 r8 mi' |
    sold'4. sold'8 la' si' |
    dod'' si' dod'' red'' mi'' fad'' |
    red''4 \appoggiatura dod''8 si'4 r8 fad'' |
    sold'' fad'' mi'' fad'' red''8.\trill mi''16 |
    \appoggiatura mi''8 fad''2 r8 sold'' |
    sold''2 r8 fad'' |
    fad''4. si'8 dod'' red'' |
    mi'' red'' mi'' sold'' fad'' sold'' |
    sold''4 \appoggiatura fad''8 mi''4 r8 sold'' |
    dod''2 fad''4 |
    red''8 mi'' red''4.\trill mi''8 |
    mi''4.
    \ffclef "vdessus" r8 r <>^\markup\character La Sylphide
    mi''8 |
    sold'' fad'' mi'' red'' dod'' si' |
    mi''4. mi''4 red''8 |
    dod'' red'' mi'' fad'' mi'' fad'' |
    red'' si' dod'' red'' si' la' |
    sold'4 sold'8 sold' lad' si' |
    lad'\trill si' dod'' red'' mi'' fad'' |
    sold'' mi'' sold'' dod'' red'' mi'' |
    red''4.\trill \appoggiatura dod''8 si' mi'' red'' |
    dod''4.~ dod''8 fad'' mi'' |
    red''4 red''8 red'' dod'' red'' |
    si' dod'' red'' mi'' red'' dod'' |
    red'' dod'' si' sold'' fad'' mi'' |
    fad''8[\melisma sold'' la''] fad''16[ sold'' la'' sold'' fad'' mi'']( |
    fad''4.)\melismaEnd si'8 fad'' red'' |
    sold'' mi'' dod'' fad'' red'' si' |
    dod'' red'' mi'' mi''4( red''8) |
    mi''4.
  }
  \tag #'voix2 {
    \clef "vhaute-contre" R1 R2. R1 R2.*13 R2.*2 R1 R2. R1 R2. R1*2
    R2. R1. R2.*7 R1 R1 R2. R2.*13 r4 r r4 R2.*5 r4 r r4 R2.*8 r2 r2
    R1*6 R1 R1 R1*13 r2 r2 R1*16 R2.*2 R1 R2.*2 R1 R1 R1*2 R2.*3 |
    r8 <>^\markup\character Le Sylphe mid' sold'2 |
    R2.*17 |
    r4 r8 r r <>^\markup\character Le Sylphe sold' |
    si' la' sold' fad' sold' la' |
    sold'4.\trill sold'4 si'8 |
    mi' la' sold' fad' sold' la' |
    fad'4.~ fad'8. fad'16 red'8 |
    mi'4 sold'8 mi' fad' sold' |
    dod' red' mi' fad' sold' la' |
    si' sold' si' mi' fad' sold' |
    fad'4.\trill fad'8 sold' fad' |
    mi'4.~ mi'8 la' sold' |
    fad'4 fad'8 fad' mi' fad' |
    red' mi' fad' sold' fad' mi' |
    fad' sold' la' mi' fad' sold' |
    la' sold' fad' la'16[ sold'] fad'[ mi'] red'[ dod'] |
    red'4. red'8 si si |
    mi' mi' mi' red' fad' sold' |
    mi' la' sold' sold'4( fad'8)\trill |
    mi'4.
  }
>>
