\clef "dessus"
<<
  \tag #'dessus1 {
    r4 sol' si' re'' |
    sol'' fad''8 mi'' re'' do'' si' la' |
    si'4~ si'16 re'' do'' si' la'4~ la'16 do'' si' la' |
    si'4 sol' si' re'' |
    sol''4. la''16 sol'' sol''4.(\trill fad''16 sol'') |
    la''8 re'' fad'' sol'' la'' si'' do''' la'' |
    si'' re'' mi'' fad'' sol'' la'' si'' sol'' |
    la''4 sol''8 fad'' mi'' re'' dod'' si' |
    la''4 sol''8 fad'' mi'' re'' dod'' la' |
    la''4. si''16 la'' re'''8 la'' fad'' re'' |
    la''4. si''16 la'' la''4.\trill si''16 la'' |
    re'''4 dod'''8 si'' la'' sol'' fad'' mi'' |
    re'''4 dod'''8 si'' la'' sol'' fad'' re'' |
    re'''4. mi'''16 re''' re'''4.\trill dod'''8 |
    re'''4~ re'''16 re'' mi'' re'' sol''4~ sol''16 fad'' sol'' si'' |
    la''4~ la''16 re'' la'' do''' si''4~ si''16 re''' do''' si'' |
    la''8 si'' sol'' la'' la''4.\trill sol''8 |
    sol''2. fad''8 mi'' |
    re'' do'' si' la' sol'\fort re'' sol'' si'' |
    la'' re'' fad'' la'' re''' re'' sol'' si'' |
    re'''4 dod'''8 si'' la'' sol'' fad'' mi'' |
    re'''4 dod'''8 si'' la'' sol'' fad'' mi'' |
    re''4 sol''2 fad''8 mi'' |
    re'' do'' si' sol' sol'' si'' la'' sol'' |
    fad'' re'' sol'' la'' si''4~ si''16 re''' do''' si'' |
    la''4~ la''16 do''' si'' la'' si''4~ si''16 re''' do''' si'' |
    la''4~ la''16 do''' si'' la'' si''8 sol'' re'' do'' |
    si' mi'' re'' mi'' dod'' re'' mi'' dod'' |
    re''4~ re''16 fad'' mi'' re'' dod''4~ dod''16 re'' mi'' dod'' |
    re''4~ re''16 re'' mi'' fad'' sol''8 la'' fad'' sol'' |
    la'' si'' la'' re''' dod'''4.\trill re'''8 |
    re'''4 dod'''8 si'' la'' sol'' fad'' mi'' |
    re'''4 dod'''8 si'' la'' sol'' fad'' re'' |
    re'''4~ re'''16 re'' mi'' re'' re''4\doux~ re''8 mi''16 re'' |
    re''1\trill~ |
    re''2. sol''4 |
    fad''2 la''~ |
    la''4 sib''8 la'' sol'' fa'' mi'' re'' |
    dod''4 re''8 mi'' \appoggiatura fa''8 mi''4.\trill re''8 |
    re''4 dod''8 si' la' sol' fad' mi' |
    re''4 dod''8 si' la' sol' fad' mi' |
    re''4~ re''8 mi''16 re'' re''4~ re''16 mi'' re'' mi'' |
    fad''4 mi''8 re'' dod'' si' la' sol' |
    fad'4. sol'8 la' si' do'' la' |
    si'4. do''16 si' si'4.\trill do''16 si' |
    si'2\trill \appoggiatura la'8 sol'4. sol''8 |
    sol''4 fad''8 mi'' re'' do'' si' la' |
    sol''4 fad''8 mi'' re'' do'' si' sol' |
    sol''4~ sol''8 la''16 sol'' sol''4\trill~ sol''16 fad'' sol'' la'' |
    si''4 la''8 sol'' fad'' mi'' re'' do'' |
    si'4. do''8 re'' mi'' fa'' sol'' |
    mi''4. fa''16 mi'' mi''4. fa''16 mi'' |
    mi''4.\trill re''16 do'' si'4 sol'8 la' |
    si'4 do''16 si' la' si' do''4 re''16 do'' si' do'' |
    re''4 mi''16 re'' do'' re'' mi''4 dod'' |
    re''4. mi''8 re'' do'' si' la' |
    si'2.\fort~ si'16 re'' do'' si' |
    la'2.~ la'16 do'' si' la' |
    sol'2.\doux~ sol'16 si' la' sol' |
    fad'2.~ fad'16 la' sol' fad' |
    \once\tieDashed sol'4~ sol'16 si' la' sol' fad'4~ fad'16 la' sol' fad' |
    sol'4 sol''8 fad'' mi'' la'' la'' la'' |
    la''4 sol''8 fad'' mi'' re'' dod'' si' |
    la'' si'' la'' sol'' fad'' mi'' re'' mi'' |
    dod''2\trill r |
    R1 |
    r2 re''4. fa''8 |
    mi''4. fad''8 sol'' mi'' fad'' sol'' |
    la'' fad'' sol'' la'' re''4 sol'' |
    fad''2\trill la''8\fort sol'' fad'' re'' |
    si'' la'' sol'' re'' la'' sol'' fad'' re'' |
    do''' re''' si'' do''' la''4 re'' |
    R1 |
    re''4 mi''8 fad'' sol'' fad'' sol'' la'' |
    fad'' re''' do''' si'' la'' si'' sol'' la'' |
    fad'' sol'' la'' re'' do''' re''' si'' do''' |
    la''4 si'8 do'' re'' mi'' fad'' sol'' |
    la''2 si''\trill |
    do'''1~ |
    do'''4. si''8 la'' do''' si'' la'' |
    si''2\trill sol''4. si''8 |
    si''4 la''8 sol'' fad'' mi'' re'' do'' |
    si'4. do''8 re'' mi'' fa'' sol'' |
    mi''2\trill \appoggiatura re''8 do''4 r8 si' |
    la'4~ la'16 re'' do'' si' la'4~ la'16 do'' si' la' |
    sol'4~ sol'16 si' la' sol' fad'4.(\trill mi'16 fad') |
    sol'2 r |
    R1*2 |
    r4 re''8 mi'' fad''4.\trill sol''8 |
    fad''4~ fad''16 mi'' re'' fad'' mi''8. re''16 dod''8 mi'' |
    re''2 la''4\fort~ la''16 si'' do'' si'' |
    la''8 re''' do''' si'' la'' sol'' fad'' mi'' |
    re''2 re'' |
    re''1 |
    sol''4~ sol''16 si'' la'' sol'' fad''4~ fad''16 la'' sol'' fad'' |
    sol''8 si'' la'' sol'' fad'' mi'' re'' do'' |
    si' do'' si' la' sol' si' la' si' |
    do''4. re''16 mi'' la'4 r8 re'' |
    si'4~ si'16 re'' do'' si' la'4~ la'16 do'' si' la' |
    si'4 sol' si' re'' |
    sol''4 fad''8 mi'' re'' do'' si' la' |
    sol''4 fad''8 mi'' re'' do'' si' sol' |
    sol''2 sol''4.\trill fad''8 |
    sol''1 |
  }
  \tag #'dessus2 {
    sol'2. sol'8 la' |
    si'4 la'8 sol' fad'4 re' |
    sol' si do' re' |
    sol2 sol'4 fad' |
    mi'1 |
    re'4 re''8 mi'' re''4 do'' |
    si'4. la'8 sol'4 sol' |
    fad'2 sol' |
    la'2. sol'4 |
    fad' la' fad' re' |
    re'' re' re'' dod'' |
    si'2 dod'' |
    re''2.( do''!4) |
    si'4 sol' la'2 |
    re'4 re'' si' sol' |
    re''4 fad'' sol'' sol' |
    do''8 re'' mi'' do'' re''4 re' |
    sol'1 |
    r4 sol'8 la' si'4 sol' |
    fad' re' fad'' sol'' |
    si'2 dod'' |
    re''2 re' |
    sol'4 do' re'2 |
    sol' mi' |
    re'4 si'8 la' sol' si' la' sol' |
    re'' mi'' fad'' re'' sol''4 sol' |
    re'' do'' si'4. la'8 |
    sol' la' si' sol' la'4 sol' |
    fad' re' la' la |
    fad' re'' dod'' re'' |
    re'8 sol' fad' sol' la'4 la |
    re' re'' dod''2 |
    re''2. do''!4 |
    si' re'' si' sol' |
    si' la'8 sol' fad'4 re' |
    sol'4 la'8 si' la'4 sol' |
    re'2. re''4 |
    sol'1~ |
    sol'4 fa'8 sol' la'4 la |
    re'2 dod' |
    re'2. do'4 |
    si sol8 la si4 sol |
    re'1 |
    re' |
    sol8 sol'' fad'' mi'' re'' do'' si' la' |
    sol''4 fad''8 mi'' re'' do'' si' la' |
    sol'2 fad' |
    sol'2. fa'4 |
    mi' do'8 re' mi'4 do' |
    sol'2 r |
    sol' r4 r8 sol' |
    do''4 si'8 la' sol' fa' mi' re' |
    do' re' mi' fa' sol'4 r |
    R1*2 |
    re''2 fad' |
    sol'2. sol''4 |
    sol''2 fad'' |
    sol''2. sol'4 |
    re'2. r4 |
    r sol' re'2 |
    sol'4 mi' la' re'' |
    la'2 r |
    r4 la' re'' re' |
    la'2 r4 re'' |
    sol'' sol' do''4 si'8 la' |
    sol'4. la'8 si'4 sol' |
    do'4. re'8 mi'2 |
    fad' sol'4 sol |
    re'4 re''8 mi'' fad''4 re'' |
    sol''4 sol' do''8 la' re'' si' |
    mi'' fad'' sol'' do'' re''4 do'' |
    si' la'8 sol' fad'4 re' |
    sol'8 la' sol' fad' mi'4 la' |
    re' r re' r |
    r re'' fad' sol' |
    re' r r2 |
    r4 re''8 do'' si' la' si' sol' |
    re'1~ |
    re'4 re'8 mi' fad'4 re' |
    sol'2 r |
    R1 |
    r2 r4 sol' |
    do'' do'8 re' mi'4 do' |
    re'2. re'4 |
    sol2 r4 re' |
    sol'2. sol'8 la' |
    si'4 do''16 si' la' si' do''4 re''16 do'' si' do'' |
    re''4 do'' si'8 la' si' sol' |
    re''4 re' r re''8 dod'' |
    re''4 si' sol' la' |
    re'2. re''4 |
    re''2. re'4 |
    sol'2.~ sol'16 si' la' sol' |
    fad'2.~ fad'16 la' sol' fad' |
    sol'8 la' si' do'' re'' mi'' fad'' re'' |
    sol''2 r |
    r r4 r8 sol' |
    mi'4 do' re' si |
    mi' do' re'2 |
    sol2. sol'8 la' |
    si'4 la'8 sol' fad'2 |
    sol'2. fa'4 |
    mi' do' re'2 |
    sol'1 |
  }
>>
