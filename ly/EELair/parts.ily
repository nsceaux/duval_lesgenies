\piecePartSpecs
#`((dessus #:score "score-dessus")
   (flute-hautbois #:tag-notes dessus1
                   #:instrument "Violons")
   (dessus1 #:instrument "Violons")
   (dessus2 #:instrument "Violons")
   
   (silence #:on-the-fly-markup , #{
\markup\fontsize#-2 \column {
  \livretPers Florise
  \livretVerse#8 { Triomphe, fais voler tes traits, }
  \livretVerse#8 { Tendre amour, règne dans nos fêtes ; }
  \livretVerse#8 { Fais ta gloire de nos défaites, }
  \livretVerse#8 { Mais laisse-nous aimer en paix. }
}#}))
