\markup\small { [Seconds violons pages suivantes.] }
\score {
  \new Staff \with {
    instrumentName = \markup\center-column { Premiers Violons }
  } << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
  \layout { indent = \largeindent }
}
\pageBreak
\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
      \global \includeNotes "voix"
    >> { \set fontSize = -2 \includeLyrics "paroles" }
    \new Staff \with {
      instrumentName = \markup\center-column { Seconds Violons }
    } << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
  >>
  \layout { indent = \largeindent }
}
