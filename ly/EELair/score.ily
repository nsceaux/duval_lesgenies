\score {
  \new ChoirStaff <<
    \new Staff \with { instrumentName = \markup\character Florise } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new GrandStaff \with { instrumentName = "Violons" } <<
      \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
      \new Staff <<
        \global \keepWithTag #'dessus2 \includeNotes "dessus"
        \origLayout {
          s1*8\break s1*7\pageBreak
          s1*6\break s1*4\pageBreak
          s1*5\break s1*5\pageBreak
          s1*5\break s1*5\pageBreak
          s1*5\break s1*5\pageBreak
          s1*6\break s1*4 s2 \bar "" \pageBreak
          s2 s1*4\break s1*6\pageBreak
          s1*6\break s1*5\pageBreak
          s1*7\break s1*7\pageBreak
          s1*4
        }
      >>
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
