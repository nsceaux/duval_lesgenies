\clef "vdessus" R1*16 |
r2 r4 r8 sol' |
si'4~ si'16[\melisma re'' do'' si'] la'4~ la'16[ do'' si' la']( |
si'4)\melismaEnd sol'2 r4 |
R1*2 |
r2 r4 r8 sol'8 |
si'4~ si'16[\melisma re'' do'' si'] la'4~ la'16[ do'' si' la']( |
si'4)\melismaEnd sol'2 si'8 dod'' |
re''2 re'' |
re''1~ |
re''2. sol''8 fad'' |
sol''2 mi''8[\melisma fad'' sol'' mi''] |
fad''4~ fad''16[ la'' sol'' fad''] mi''4~ mi''16[ sol'' fad'' mi''] |
fad''4~ fad''16[ la'' sol'' fad''] mi''8\melismaEnd fad'' re'' mi'' |
fad''2( mi'')\trill |
re''4 r r2 |
R1 |
r2 sol''4 sol'' |
sol''4\melisma fad''8[ mi''] re''[ do'' si' la']( |
si'4)\melismaEnd do''8 re'' do''4 si' |
la' re' fa''2~ |
fa''4 sol''8 fa'' mi''[ re''] dod''[ si'] |
la'2 dod''\trill |
re''1~ |
re''2 r |
r r4 r8 re'' |
re''4\melisma dod''8[ si'] la'[ sol' fad' mi'] |
re''4 dod''8[ si'] la'[ sol' fad' re'] |
re''2. re''8[ mi''16 re'']( |
re''2)\trill\melismaEnd re'' |
R1*2 |
r2 r4 r8 sol'' |
sol''4\melisma fad''8[ mi''] re''[ do'' si' la'] |
sol''4 fad''8[ mi''] re''[ do'' si' sol'] |
sol''2~ sol''\trill\melismaEnd |
sol''4 r r si'8 do'' |
re''4\melisma mi''16[ re'' do'' re''] mi''4 fad''16[ mi'' re'' mi''] |
fad''4 sol''16[ fad'' mi'' fad''] sol''4.*5/6\melismaEnd fad''16([ mi'' fad'']) |
fad''2\trill r |
R1*2 |
si'2.~ si'16[\melisma re'' do'' si'] |
la'2.~ la'16[ do'' si' la'] |
si'4.*5/6 re''16[ do'' si'] la'4~ la'16[ do'' si' la']( |
si'4.)\melismaEnd si'8 dod''4 re'' |
dod''4\trill \appoggiatura si'8 la'4 r2 |
r re''4 fad'' |
mi''4~ mi''16[\melisma sol'' fad'' mi''] fad''4~ fad''16[ la'' sol'' fad'']( |
sol''4)\melismaEnd re'' mi''4 re''8 do'' |
re''4 si' re''2~ |
re'' do''8 si' la' sol' |
do''2 si' |
la'\trill r |
R1*2 |
sol''4 fad''8 mi'' re''[ do''] si'[ la'] |
si'2 dod''\trill |
re''1~ |
re''2. r4 |
r sol'8 la' si'[ do''] re''[ mi''] |
fad''2 sol''\trill |
la''1~ |
la''2 r |
r r4 r8 sol'' |
sol''4\melisma fad''8[ mi''] re''[ do'' si' la'] |
sol''4 fad''8[ mi''] re''[ do'' si' sol'] |
sol''1\melismaEnd |
fad''2\trill r4 r8 sol' |
si'4~ si'16[\melisma re'' do'' si'] la'4~ la'16[ do'' si' la']( |
si'2)\trill\melismaEnd \appoggiatura la'8 sol'4 si'8 do'' |
re''4\melisma mi''16[ re'' do'' re''] mi''4 fad''16[ mi'' re'' mi'']( |
fad''2)\melismaEnd sol'' |
re'' re''4. mi''8 |
re''2 sol''4~ sol''16[\melisma fad'' mi'' fad''] |
fad''2\trill\melismaEnd \appoggiatura mi''8 re''4 r |
R1 |
si'2.~ si'16[\melisma re'' do'' si'] |
la'2.~ la'16[ do'' si' la'] |
si'4\melismaEnd \appoggiatura la'8 sol'4 r2 |
r8 sol'[\melisma la' si'] do''[ re'' mi'' fad''] |
sol''1~ |
sol''4.\melismaEnd mi''8 fad''4 sol'' |
sol''2.~ sol''8[ fad''] |
\appoggiatura fad''8 sol''2 r |
R1*4 |
