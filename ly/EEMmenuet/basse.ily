\clef "basse" sol,2. |
sol2 fad4 |
sol do mi |
re re do |
si, si, la, |
sol,2 fad,4 |
sol, fad, sol, |
re,2. |
re,4 re' do' |
si2. |
si, |
si |
do'4 do' si |
la2. |
la, |
la |
si4 si sol |
do2. |
do'2 si4 |
la4 fad sol |
re' re' do' |
si2. |
si, |
si |
do'4 do' si |
la2. |
la, |
la |
si4 si sol |
do2. |
do'2 si4 |
do' re' re |
sol re'4 do' |
sol2. |
