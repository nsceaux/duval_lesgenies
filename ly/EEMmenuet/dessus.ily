\clef "dessus" sol'4 sol'8 fad' sol' la' |
si'4 re'' do'' |
si' do''8 si' la' sol' |
la'4 sol'8 fad' mi' re' |
sol'4 sol'8 fad' sol' la' |
si'4 re'' do'' |
si' do''8 si' la' sol' |
la'2.\trill |
la'\trill |
re''4 re'' re'' |
re'' si'8 do'' re''4 |
re''8 do'' si' do'' re''4 |
mi'2. |
do''4 do'' do'' |
do''4 la'8 si' do''4 |
do''8 si' la' si' do''4 |
re'2. |
si'4 do''8 si' la' sol' |
la'4 fad' sol' |
do'' do'' si' |
la'8 sol' la' si' do'' la' |
re''4 re'' re'' |
re'' si'8 do'' re''4 |
re''8 do'' si' do'' re''4 |
mi'2. |
do''4 do'' do'' |
do'' la'8 si' do''4 |
do''8 si' la' si' do''4 |
re'2. |
si'4 do''8 si' la' sol' |
la'4 re' sol' |
sol' fad'8 sol' la' fad' |
sol'2. |
sol' |
