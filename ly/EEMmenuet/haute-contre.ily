\clef "haute-contre" re'2. |
re'2 re'4 |
re'8 sol' la' sol' fad' mi' |
fad'4 re' re' |
re' re' do' |
si re' re' |
re'2 re'8 sol' |
fad'2.\trill |
fad'\trill |
sol'2. |
sol'4 sol'8 la' si'4 |
re'2 sol'4 |
sol'2. |
sol' |
la'4 do'8 re' mi'4 |
mi'8 re' do' re' mi'4 |
do'2. |
mi'2 mi'4 |
mi'2 re'4 |
la' la' sol' |
fad'8 mi' fad' sol' la' fad' |
sol'2. |
sol'4 sol'8 la' si'4 |
re'2 sol'4 |
sol'2. |
sol' |
la'4 do'8 re' mi'4 |
mi'8 re' do' re' mi'4 |
do'2. |
mi'2 mi'4 |
fad'2 sol'4 |
la' si' do'' |
si'2.\trill |
si'\trill |
