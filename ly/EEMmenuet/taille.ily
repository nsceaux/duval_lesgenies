\clef "taille" si si8 la si do' |
re'4 si la |
sol2 sol4 |
fad sol8 la sol fad |
sol4 sol8 la si do' |
re'4 si la |
sol la si |
la2. |
la4 re'2 |
re'2. |
re'2 sol'4 |
fad'8 mi' re' mi' fad' mi' |
re'2 re'4 |
mi' mi' mi' |
mi'8 re' do' si la4 |
la2 la'4 |
la'2. |
sol'8 fad' mi' re' do' si |
do'4 la si |
do' re' re' |
re'2 re'4 |
re'2. |
re'2 sol'4 |
fad'8 mi' re' mi' fad' mi' |
re'2 re'4 |
mi'4 mi' mi' |
mi'8 re' do' si la4 |
la2 la'4 |
la'2. |
sol'8 fad' mi' re' do' si |
la2 re'4 |
mi' re' re' |
re'2. |
re' |
