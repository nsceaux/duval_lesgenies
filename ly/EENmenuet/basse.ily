\clef "basse" sol,4 sol fad |
sol sol, sol8 la |
sib2 do'4 |
re' re'8 do' sib la |
sol4 do'8 sib la sol |
fa4 sib8 la sol fa |
mib4 mib2\trill |
re2. |
sib2 la4 |
sib sib, sib |
la sol2 |
fa4 fa'8 mib' re' do' |
sib4 mib'8 re' do' sib |
la fa sib la sol sib |
mib do fa4 fa, |
sib,8 sib la sol fa mib |
re4 fa fa, |
do mib mib, |
sib, fa fa, |
do mib mib, |
sib, do2 |
re sol,4 |
do, re,2 |
sol,2. |
