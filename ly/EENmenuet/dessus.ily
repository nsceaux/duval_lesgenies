\clef "dessus" sol'8 la' sib' do'' re''4 |
sib' sol'8 la' sib' do'' |
re''4 sol''4. la''8 |
fad''2. |
sol''8 fa'' mib'' re'' do'' la' |
fa'' mib'' re'' do'' sib' sol' |
mib'' re'' do''4.\trill( sib'16 do'') |
re''2. |
sib'8 do'' re'' mib'' fa''4 |
re''4 sib'8 do'' re'' mib'' |
fa''4 sib''4. do'''8 |
la''2. |
sib''8 la'' sol'' fa'' mib'' do'' |
fa'' mib'' re'' do'' sib' sol' |
mib''4 do''4.\trill sib'8 |
sib'2. |
re''4 la'8 sib' do''4 |
mib'' sol'8 la' sib'4 |
re''4 la'8 sib' do''4 |
mib'' sol'8 la' sib'4 |
re'' do''8 sib' la' sol' |
fad' sol' la' re' sib'4 |
la' la'4.\trill sol'8 |
sol'2. |
