\clef "haute-contre" re'2 re'4 |
re'2 sol'4 |
sol' re' la' |
la'2 sol'8 la' |
sib'8 la' sol'4. sol'8 |
la' sol' fa'4. fa'8 |
sol' fa' sol'2 |
fad'2.\trill |
fa'2 fa'4 |
fa'2 re''4 |
do'' re''4. do''8 |
do''2 sib'8 do'' |
re'' do'' sib'4. sib'8 |
do''4 fa'4. fa'8 |
sol' mib' fa'4 mib' |
re'2. |
sib'4 la'8 sol' la'4 |
sol' sol'8 fa' sol'4 |
fa' fa'8 sol' la'4 |
sol' sol'8 fa' sol'4 |
fa' la'8 sol' fa' mib' |
re'4 la' sol'~ |
sol' sol' fad'\trill |
sol'2. |
