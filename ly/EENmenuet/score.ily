\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { [Violons et Hautbois] }
    } << \global \includeNotes "dessus" >>
    \ifFull <<
      \new Staff \with {
        instrumentName = \markup\center-column { [Hautes-contre] }
      } << \global \includeNotes "haute-contre" >>
      \new Staff \with {
        instrumentName = "[Tailles]"
      } << \global \includeNotes "taille" >>
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { [Basses, Bassons et B.C.] }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s2.*9\break s2.*9\pageBreak }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
