\clef "taille" sib8 do' re'4 la |
sol sib8 do' re'4 |
re'2 mib'4 |
re' fad'8 mi'! re' do' |
sib4 sib4. sib8 |
la4 la4. la8 |
sib4 sol do' |
la2. |
re'8 mib' fa'4 do' |
sib re'8 mib' fa'4 |
fa' fa'2 |
fa'4 la'8 sol' fa' mib' |
re'4 re'4. re'8 |
do'4 re'4. re'8 |
do'4 sib la\trill |
sib4. sib8 la sol |
fa4 do' fa' |
mib' sib mib' |
re' do' fa' |
mib' sib mib' |
fa' mib' la |
la8 sib do'4 re' |
mib' re' do' |
sib2. |
