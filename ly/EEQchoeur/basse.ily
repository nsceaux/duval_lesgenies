\clef "basse" r4 |
R2. |
r4 r fad |
si2 si8 si |
la4 si8 la sol fad |
sol4 sol mi |
fad2.~ |
fad |
fad,4. fad8 fad fad |
si4. si8 si si |
dod'2 dod'8 dod' |
re'4 re'8 dod' re' si |
mi'4 mi' mi8 fad |
sol4 sol8 fad sol mi |
fad4 fad,2 |
si,2. |
