\clef "dessus" si'4 |
fad''2 fad''8 fad'' |
mi''4 fad''8 mi'' re'' dod'' |
re''2 fad''8 fad'' |
fad'' mi'' re'' dod'' si' la' |
sol'4 sol' sol' |
fad'4. fad'8 fad' fad' |
si'4. si'8 si' si' |
dod''2 dod''8 dod'' |
re''4 re''8 dod'' re'' si' |
mi'' fad'' mi'' re'' mi'' dod'' |
fad'' sol'' fad'' mi'' fad'' re'' |
sol'' la'' sol'' fad'' mi'' re'' |
mi''4. re''8 dod''4 |
re'' re''( dod'')\trill |
si'2. |

