\clef "haute-contre" r4 |
R2. |
r4 r lad' |
si'2 re''8 re'' |
dod''4 dod'' re'' |
si' si' dod'' |
lad'2\trill dod'4 |
re'2 re'4 |
dod'4. la'8 la' la' |
si'4. si'8 si' si' |
si'4( lad') lad'8 lad' |
si'4 si' si' |
si'8 dod'' si' la' sol' fad' |
mi'4. fad'8 mi'4 |
re' fad' mi' |
re'2. |
