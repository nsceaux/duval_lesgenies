\tag #'vdessus {
  Chan -- tons, ne son -- geons qu’aux plai -- sirs,
  pro -- fi -- tons de l’â -- ge des grâ -- ces,
  pour mieux ré -- pondre à nos dé -- sirs,
  les a -- mours vo -- lent sur nos tra -- ces.
}
\tag #'vhaute-contre {
  Chan -- tons, ne son -- geons qu’aux plai -- sirs,
  qu’aux plai -- sirs,
  chan -- tons, chan -- tons,
  pour mieux ré -- pondre à nos dé -- sirs,
  les a -- mours vo -- lent, vo -- lent sur nos tra -- ces.
}
\tag #'vtaille {
  Chan -- tons, ne son -- geons qu’aux plai -- sirs,
  qu’aux plai -- sirs,
  chan -- tons, chan -- tons,
  pour mieux ré -- pondre à nos dé -- sirs,
  les a -- mours vo -- lent, vo -- lent,
  les a -- mours vo -- lent sur nos tra -- ces.
}
\tag #'vbasse {
  Chan -- tons, ne son -- geons qu’aux plai -- sirs,
  qu’aux plai -- sirs,
  pour mieux ré -- pondre à nos dé -- sirs,
  les a -- mours vo -- lent,
  les a -- mours vo -- lent sur nos tra -- ces.
}
