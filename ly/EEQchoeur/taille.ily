\clef "taille" r4 |
R2. |
r4 r fad' |
fad'2 si'8 si' |
dod''4 fad' fad' |
fad' mi' mi' |
\appoggiatura re'8 dod'2 lad4 |
si2 si4 |
la!4. fad'8 fad' fad' |
fad'4. fad'8 fad' fad' |
mi'2 mi'8 mi' |
re'4 fad' fad' |
mi'2 si8 si |
si4 si8 la si4 |
si( lad2)\trill |
si2. |
