<<
  \tag #'vdessus {
    \clef "vdessus" si'4 |
    fad''2 fad''8 fad'' |
    mi''4 fad''8[ mi''] re''[ dod''] |
    re''2 re''8 re'' |
    dod''2 re''4 |
    si' si' dod'' |
    lad' lad'8 fad' fad' fad' |
    si'4. si'8 si' si' |
    dod''2 dod''8 dod'' |
    re''4 re''8[\melisma dod'' re'' si'] |
    mi''[ fad'' mi'' re'' mi'' dod''] |
    fad''[ sol'' fad'' mi'' fad'' re''] |
    sol''[ la'' sol'' fad'' mi'' re'']( |
    mi''4.)\melismaEnd re''8 dod''4 |
    re''4 re''( dod'')\trill |
    si'2. |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" r4 |
    R2. |
    r4 r fad'4 |
    fad'2 fad'8 fad' |
    fad'4 fad' fad' |
    fad' mi'4 mi' |
    \appoggiatura re'8 dod'2 dod'4 |
    re'2 re'4 |
    dod'4. la'8 la' la' |
    si'4. si'8 si' si' |
    si'4( lad') lad'8 lad' |
    si'4 si' si' |
    si'8[ dod'' si' la' sol' fad']( |
    mi'4.) fad'8 mi'4 |
    re' fad'( mi') |
    re'2. |
  }
  \tag #'vtaille {
    \clef "vtaille" r4 |
    R2. |
    r4 r lad4 |
    si2 si8 si |
    dod'4 re'8[ dod'] si[ la] |
    sol4 sol4 sol |
    fad2 lad4 |
    si2 si4 |
    la!4. fad'8 fad' fad' |
    fad'4. fad'8 fad' fad' |
    mi'2 mi'8 mi' |
    re'4 fad' fad' |
    mi'4 mi' si8 si |
    si4 si8 la si si |
    si4( lad2)\trill |
    si2. |
  }
  \tag #'vbasse {
    \clef "vbasse" r4 |
    R2. |
    r4 r fad |
    si2 si8 si |
    la4 si8[ la] sol[ fad] |
    sol4 sol mi |
    fad2 r4 |
    R2. |
    r4 r8 fad fad fad |
    si4. si8 si si |
    dod'2 dod'8 dod' |
    re'4 re'8[ dod' re' si]( |
    mi'4) mi' mi8 fad |
    sol4 sol8 fad sol mi |
    fad4( fad,2) |
    si,2. |
  }
>>
