\version "2.23.10"
\include "common.ily"

\paper {
  %% de la place sous la basse pour pouvoir noter les chiffrages
  score-markup-spacing.padding = #4
  system-system-spacing.padding = #4
  last-bottom-spacing.padding = #4
}
\layout {
  \context {
    \Lyrics
    %% de la place sous les paroles
    \override VerticalAxisGroup.nonstaff-unrelatedstaff-spacing.padding = #3
  }
}

%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header {
    title = \markup\center-column {
      LES GÉNIES
      \fontsize#-5 OU LES CARACTÈRES DE L’AMOUR
    }
  }
  \markup\null
}
%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \markuplist
  \abs-fontsize-lines#8
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 2)
  \table-of-contents
}
%% Livret
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \include "livret/livret.ily"
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \actn "Prologue"
  \sceneDescription\markup\wordwrap-center { Le théatre représente un désert. }
  \scene "Scène Première" "Scène I : Zoroastre."
  \sceneDescription\markup\wordwrap-center { Zoroastre. }
  %% 0-1
  \pieceTocFootnote Ouverture \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "AAAouverture"
}
\bookpart {
  %% 0-2
  \pieceToc\markup\wordwrap {
    Récit. Zoroastre : \italic { Il est temps que mon Art instruise les Mortels }
  }
  \includeScore "AABrecit"
  %% 0-3
  \pieceTocFootnote\markup\wordwrap {
    Air. Zoroastre : \italic { Esprits soumis à mes commandements }
  } \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "AACair"
}
\bookpart {
  \scene "Scène II" "Scène II : Zoroastre, chœur."
  \sceneDescription\markup\wordwrap-center { Zoroastre, et les génies. }
  %% 0-4
  \pieceTocFootnote\markup\wordwrap {
    Air, chœur. Zoroastre : \italic { Que la terre, le feu, que l’onde, que les airs }
  } \markup\wordwrap {
    Parties de dessus, hautes-contre et tailles de violons, et de
    hautes-contre et tailles chantantes recomposées par B. Dratwicki.
  }
  \includeScore "ABAair"
  \includeScore "ABBchoeur"
}
\bookpart {
  \paper { systems-per-page = 4 }
  %% 0-5
  \pieceTocFootnote "Air pour les génies" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons
    recomposées par B. Dratwicki.
  }
  \includeScore "ABCdanse"
}
\bookpart {
  \paper { systems-per-page = 4 }
  %% 0-6
  \pieceToc\markup\wordwrap {
    Air. Zoroastre :
    \italic { Quels bruits ! quels doux accords ! quelle clarté nouvelle ! }
  }
  \markup\italic {
    On entend une douce harmonie qui annonce la descente de l’Amour.
  } \noPageBreak
  \includeScore "ABDair"
}
\bookpart {
  \scene "Scène III" "Scène III : L’Amour, Zoroastre."
  \sceneDescription\markup\wordwrap-center {
    L’Amour, Zoroastre, et les génies.
  }
  %% 0-7
  \pieceToc\markup\wordwrap {
    Air. L’Amour, Zoroastre : \italic { Tout obéit, tout s’éveille à ta voix ! }
  }
  \includeScore "ACAair"
  %% 0-8
  \pieceToc\markup\wordwrap {
    Air. L’Amour : \italic { Accourez jeux charmants, volez tendres amours }
  }
  \includeScore "ACBair"
}
\bookpart {
  \scene "Scène IV" "Scène IV : L’Amour, Zoroastre, chœur."
  \sceneDescription\markup\wordwrap-center {
    L’Amour, Zoroastre, et les génies, Troupe de Plaisirs & de Jeux.
  }
  %% 0-9
  \pieceTocFootnote "Air pour les Plaisirs" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "ACCplaisirs"
  %% 0-10
  \pieceTocFootnote "Première bourrée en rondeau" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "ACDbouree"
  %% 0-11
  \pieceTocFootnote "Deuxième bourrée" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "ACEbouree"
}
\bookpart {
  %% 0-12
  \pieceTocFootnote "Sarabande" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "ACFsarabande"
  %% 0-13
  \pieceToc\markup\wordwrap {
    Air. L’Amour : \italic { Aimez-tous, cédez à l’Amour }
  }
  \includeScore "ADAair"
}
\bookpart {
  %% 0-14
  \pieceTocFootnote "Premier menuet" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "ADBmenuet"
  %% 0-15
  \pieceToc "Deuxième Menuet"
  \includeScore "ADCmenuet"
}
\bookpart {
  %% 0-16
  \pieceTocFootnote\markup\wordwrap {
    Chœur : \italic { Du doux bruit de nos chants que ces lieux retentissent }
  } \markup\wordwrap {
    Parties de dessus, hautes-contre et tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "ADEchoeur"
}
\bookpart {
  %% 0-17
  \pieceToc "Ouverture"
  \reIncludeScore "AAAouverture" "ADFouverture"
  \actEnd "Fin du Prologue"
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \act "Acte Premier"
  \sceneDescription\markup\center-column {
    \larger Les Nymphes ou l’amour indiscret.
    \wordwrap-center {
      Le théatre représente un agréable jardin sur le bord de la mer.
    }
  }
  \scene "Scène Première" "Scène I : Léandre, Zerbin."
  \sceneDescription\markup\wordwrap-center { Léandre, Zerbin. }
  %% 1-1
  \pieceToc "Ritournelle"
  \includeScore "BAAritournelle"
  %% 1-2
  \pieceToc\markup\wordwrap {
    Récit, air. Léandre, Zerbin :
    \italic { Viens être le témoin du bonheur qui m’enchante }
  }
  \includeScore "BABrecit"
  \includeScore "BACair"
  \includeScore "BADrecit"
}
\bookpart {
  \scene "Scène II" "Scène II : Lucile, Zerbin."
  \sceneDescription\markup\wordwrap-center { Lucile, Zerbin. }
  %% 1-3
  \pieceToc\markup\wordwrap {
    Air, récit. Lucile, Zerbin :
    \italic { Asile des plaisirs, beau lieu rempli de charmes }
  }
  \includeScore "BBAair"
  \includeScore "BBBrecit"
}
\bookpart {
  \paper { systems-per-page = 4 }
  %% 1-4
  \pieceToc\markup\wordwrap {
    Air, récit. Lucile, Zerbin : \italic { Venez, juste Dépit, venez à mon secours }
  }
  \includeScore "BBCair"
  \includeScore "BBDrecit"
}
\bookpart {
  \scene "Scène III" "Scène III : Léandre."
  \sceneDescription\markup\wordwrap-center { Léandre. }
  %% 1-5
  \pieceToc\markup\wordwrap {
    Air. Léandre : \italic { Reviens cher objet de mes vœux }
  }
  \includeScore "BCAair"
}
\bookpart {
  \scene "Scène IV" "Scène IV : Léandre, une nymphe, chœur."
  \sceneDescription\markup\center-column {
    \justify {
      On entend une douce harmonie ; la nymphe paraît sur une conque marine,
      suivie de sa cour.
    }
    \wordwrap-center {
      Léandre, la principale Nymphe, et sa suite.
    }
  }
  %% 1-6
  \pieceToc "[Entrée]"
  \includeScore "BDAentree"
}
\bookpart {
  %% 1-7
  \pieceToc\markup\wordwrap {
    Air, duo, récit. Léandre, la principale Nymphe :
    \italic { Qu’éloigné de votre présence }
  }
  \includeScore "BDBduo"
}
\bookpart {
  %% 1-8
  \pieceToc\markup\wordwrap {
    Air, chœur. La principale Nymphe : \italic { Chantez dans ce riant boccage }
  }
  \includeScore "BDCair"
  \includeScore "BDDchoeur"
}
\bookpart {
  %% 1-9
  \pieceTocFootnote "Passacaille" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "BDEpassacaille"
}
\bookpart {
  %% 1-10
  \pieceTocFootnote "Premier passepied" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "BDFpassepied"
  %% 1-11
  \pieceToc "Deuxième passepied"
  \includeScore "BDGpassepied"
}
\bookpart {
  \paper { systems-per-page = 5 }
  %% 1-12
  \pieceToc\markup\wordwrap {
    Air. La principale Nymphe : \italic { Amour, tu réponds à nos vœux }
  }
  \includeScore "BDHair"
}
\bookpart {
  %% 1-13
  \pieceTocFootnote "Premier passepied" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \reIncludeScore "BDFpassepied" "BDIpassepied"
  %% 1-14
  \pieceToc "Deuxième passepied"
  \reIncludeScore "BDGpassepied" "BDJpassepied"
}
\bookpart {
  %% 1-15
  \pieceToc\markup\wordwrap {
    Air, chœur. Un Nymphe : \italic { Rions, chantons sous cet ombrage }
  }
  \includeScore "BDKchoeur"
}
\bookpart {
  %% 1-16
  \pieceTocFootnote "Premier tambourin" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "BDLtambourin"
  %% 1-17
  \pieceTocFootnote "Deuxième tambourin" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "BDMtambourin"
  %% 1-18
  \pieceToc\markup\wordwrap {
    Récit. La principale Nymphe, Léandre : \italic { Tout prévient ici vos désirs }
  }
  \includeScore "BDNrecit"
}
\bookpart {
  \scene "Scène V" "Scène V : La nymphe, Léandre, Lucile, Zerbin."
  \sceneDescription\markup\wordwrap-center {
    La principale Nymphe, et sa suite, Léandre, Lucile, Zerbin.
  }
  %% 1-19
  \pieceToc\markup\wordwrap {
    Récit. \italic { Poursuis, ingrat, poursuis volage, amant sans foi }
  }
  \includeScore "BEArecit"
  %% 1-20
  \pieceToc\markup\wordwrap {
    Air. Lucile : \italic { Venez tyrans des airs, aquilons furieux }
  }
  \includeScore "BEBair"
}
\bookpart {
  %% 1-21
  \pieceToc "Entr’acte"
  \reIncludeScore "ADBmenuet" "BEDmenuet"
  \reIncludeScore "ADCmenuet" "BEEmenuet"
  \actEnd "Fin du Premier Acte"
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \act "Acte Deuxième"
  \sceneDescription\markup\center-column {
    \larger Les Gnomes ou l’amour ambitieux.
    \wordwrap-center {
      Le théatre représente une solitude bornée par un bosquet.
    }
  }
  \scene "Scène Première" "Scène I : Zaïre, Zamide."
  \sceneDescription\markup\wordwrap-center { Zaïre, Zamide. }
  %% 2-1
  \pieceToc\markup\wordwrap {
    Air, récit. Zaïre, Zamide : \italic { Douce erreur, aimable chimère }
  }
  \includeScore "CAAair"
}
\bookpart {
  \paper { systems-per-page = 4 }
  \scene "Scène II" "Scène II : Zaïre."
  \sceneDescription\markup\wordwrap-center { Zaïre seule. }
  %% 2-2
  \pieceToc\markup\wordwrap {
    Air. Zaïre : \italic { Je cède à ta voix qui m’appelle }
  }
  \includeScore "CBAair"
}
\bookpart {
  \scene "Scène III" "Scène III : Adolphe, Zaïre."
  \sceneDescription\markup\wordwrap-center { Un gnome sous le nom d’Adolphe, Zaïre. }
  %% 2-3
  \pieceToc\markup\wordwrap {
    Récit, air. Adolphe, Zaïre : \italic { Vous voyez à vos pieds l’amant le plus fidèle }
  }
  \includeScore "CCArecit"
}
\bookpart {
  \scene "Scène IV" "Scène IV : Adolphe, Zaïre, chœur."
  \sceneDescription\markup\center-column {
    \justify {
      L’on voit paraître un superbe palais. Une troupe de gnomes sous la
      forme de divers peuples orientaux se préparent pour la fête.
    }
    \wordwrap-center {
      Adolphe, Zaïre, troupe de gnomes sous la forme
      de divers peuples orientaux.
    }
  }
  %% 2-4
  \pieceTocFootnote "Marche" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons
    recomposées par Camille Delaforge.
  }
  \includeScore "CDAmarche"
}
\bookpart {
  \paper { min-systems-per-page = 4 }
  %% 2-5
  \pieceToc\markup\wordwrap {
    Récit, air. Zaïre, Adolphe :
    \italic { Que tout ce que je vois rend mon âme interdite }
  }
  \includeScore "CDBrecit"
}
\bookpart {
  %% 2-6
  \pieceTocFootnote\markup\wordwrap {
    Chœur : \italic { Régnez dans nos climats, jouïssez de la gloire }
  } \markup\wordwrap {
    Parties de dessus, hautes-contre et tailles de violons, et de
    hautes-contre et tailles chantantes recomposées par B. Dratwicki.
  }
  \includeScore "CDCchoeur"
}
\bookpart {
  %% 2-7
  \pieceTocFootnote "Rondeau" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "CDDrondeau"
}
\bookpart {
  %% 2-8
  \pieceToc\markup\wordwrap {
    Air. Un Indien : \italic { Recevez l’éclatant hommage }
  }
  \includeScore "CDEair"
}
\bookpart {
  %% 2-9
  \pieceTocFootnote "Loure" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "CDFloure"
}
\bookpart {
  %% 2-10
  \pieceTocFootnote "Première gavotte" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "CDGgavotte"
}
\bookpart {
  %% 2-11
  \pieceTocFootnote "Deuxième gavotte" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "CDHgavotte"
}
\bookpart {
  \paper { systems-per-page = 5 }
  %% 2-12
  \pieceToc\markup\wordwrap {
    Air. Un Indien : \italic { Dans nos climats }
  }
  \includeScore "CDIair"
}
\bookpart {
  %% 2-13
  \pieceTocFootnote "Deuxième gavotte" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \reIncludeScore "CDHgavotte" "CDJgavotte"
}
\bookpart {
  %% 2-14
  \pieceTocFootnote\markup\wordwrap {
    Chœur : \italic { Régnez dans nos climats, jouïssez de la gloire }
  } \markup\wordwrap {
    Parties de dessus, hautes-contre et tailles de violons, et de
    hautes-contre et tailles chantantes recomposées par B. Dratwicki.
  }
  \reIncludeScore "CDCchoeur" "CDKchoeur"
}
\bookpart {
  %% 2-15
  \pieceTocFootnote "Entr’acte" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \reIncludeScore "CDGgavotte" "CDLgavotte"
  \reIncludeScore "CDHgavotte" "CDMgavotte"
  \actEnd "Fin du Deuxième Acte"
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \act "Acte Troisième"
  \sceneDescription\markup\center-column {
    \larger Les Salamandres ou l’amour violent.
    \wordwrap-center {
      Le théatre représente le palais de Numapire.
    }
  }
  \scene "Scène Première" "Scène I : Isménide."
  \sceneDescription\markup\wordwrap-center { Isménide. }
  %% 3-1
  \pieceToc\markup\wordwrap {
    Air. Isménide : \italic { Tyran d’un cœur fidèle et tendre }
  }
  \includeScore "DAAair"
  \sceneDescription\markup\wordwrap-center\italic {
    Pircaride sous les traits d'Ismé[ni]de, paraît sur un char de feu
    un poignard à la main.
  }
  %% 3-2
  \pieceTocFootnote\markup\wordwrap {
    Récit. Isménide : \italic { Que vois-je ? quel objet se présente à mes yeux ? }
  } \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "DABrecit"
}
\bookpart {
  \scene "Scène II" "Scène II : Pircaride, Isménide."
  \sceneDescription\markup\center-column {
    Pircaride, Isménide.
    Pircaride sort de son char.
  }
  %% 3-3
  \pieceTocFootnote "[Ritournelle]" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "DBAritournelle"
  %% 3-4
  \pieceToc\markup\wordwrap {
    Récit. Pircaride, Isménide : \italic { Pour immoler une victime }
  }
  \includeScore "DBBrecit"
}
\bookpart {
  %% 3-5
  \pieceToc\markup\wordwrap {
    Air. Pircaride : \italic { Vous qui m’obéissez, paraissez à mes yeux }
  }
  \includeScore "DBCair"
  \sceneDescription\markup\wordwrap-center {
    Isménide est enlevée par les génies sur un char.
  }
}
\bookpart {
  \scene "Scène III" "Scène III : Pircaride."
  \sceneDescription\markup\wordwrap-center {
    Pircaride sous les traits d’Isménide.
  }
  %% 3-6
  \pieceToc\markup\wordwrap {
    Air. Pircaride : \italic { Elle part, et mon cœur n’est point exempt d’alarmes }
  }
  \includeScore "DCAair"
  
  \scene "Scène IV" "Scène IV : Numapire, Pircaride."
  \sceneDescription\markup\wordwrap-center {
    Numapire, Pircaride sous les traits d’Isménide.
  }
  %% 3-7
  \pieceToc\markup\wordwrap {
    Récit. Numapire, Pircaride :
    \italic { Je sens, en vous voyant, accroître mon ardeur }
  }
  \includeScore "DDArecit"
}
\bookpart {
  \scene "Scène V" "Scène V : Numapire, Pircaride, chœurs."
  \sceneDescription\markup\wordwrap-center {
    Numapire, Pircaride, troupe de salamandres sous la forme
    de divers peuples d’Afrique.
  }
  %% 3-8
  \pieceTocFootnote "Marche" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "DDBmarche"
}
\bookpart {
  \paper { systems-per-page = 2 }
  %% 3-9
  \pieceTocFootnote\markup\wordwrap {
    Chœur. \italic { Chantons, célébrons notre reine }
  } \markup\wordwrap {
    Parties de dessus, hautes-contre et tailles de violons et de
    hautes-contre et tailles chantantes recomposées par B. Dratwicki.
  }
  \includeScore "DEBchoeur"
}
\bookpart {
  %% 3-10
  \pieceTocFootnote "Rondeau" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "DECrondeau"
  %% 3-11
  \pieceTocFootnote "Première gavotte" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "DEDgavotte"
  %% 3-12
  \pieceTocFootnote "Deuxième gavotte" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "DEEgavotte"
}
\bookpart {
  %% 3-13
  \pieceToc\markup\wordwrap {
    Air. Une Africaine : \italic { L’amour a besoin de vos charmes }
  }
  \includeScore "DEFair"
}
\bookpart {
  \paper { systems-per-page = 4 }
  %% 3-14
  \pieceTocFootnote "Air" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "DEGair"
}
\bookpart {
  %% 3-15
  \pieceTocFootnote "Première bourrée" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "DEHbourree"
  %% 3-16
  \pieceTocFootnote "Deuxième bourrée" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "DEIbourree"
}
\bookpart {
  %% 3-17
  \pieceToc\markup\wordwrap {
    Récit, air. Pircaride, Numapire :
    \italic { Finissez ces concerts, votre hommage m’offense }
  }
  \includeScore "DEJrecit"
  %% 3-18
  \pieceToc\markup\wordwrap {
    Air et chœur. Numapire : \italic { Servez les transports de ma rage }
  }
  \includeScore "DEKair"
}
\bookpart {
  %% 3-19
  \pieceTocFootnote "Air" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "DELair"
}
\bookpart {
  %% 3-20
  \pieceTocFootnote "Entr’acte" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \reIncludeScore "BDLtambourin" "DEMtambourin"
  \reIncludeScore "BDMtambourin" "DENtambourin"
  \actEnd "Fin du Troisième Acte"
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \act "Acte Quatrième"
  \sceneDescription\markup\center-column {
    \larger Les Sylphes ou l’amour léger.
    \wordwrap-center {
      Le théatre représente un lieu préparé pour y donner une fête galante.
    }
  }
  \scene "Scène Première" "Scène I : Un Sylphe."
  \sceneDescription\markup\wordwrap-center { Un Sylphe }
  %% 4-1
  \pieceToc "Ritournelle"
  \includeScore "EAAritournelle"
}
\bookpart {
  \paper { page-count = 1 }
  %% 4-2
  \pieceToc\markup\wordwrap {
    Récit et air. Un Sylphe : \italic { Le ciel a fixé mon empire }
  }
  \includeScore "EABrecit"
}
\bookpart {
  \scene "Scène II" "Scène II : Le Sylphe, une Sylphide."
  \sceneDescription\markup\wordwrap-center { Un Sylphe, une Sylphide. }
  %% 4-3
  \pieceToc\markup\wordwrap {
    Prélude, récit, air. Un Sylphe, une Sylphide :
    \italic { Ne dissimulez point, votre cœur est volage }
  }
  \includeScore "EBArecit"
}
\bookpart {
  \paper { page-count = 2 }
  \scene "Scène III" "Scène III : Florise."
  \sceneDescription\markup\wordwrap-center {
    Florise déguisée en cavalier, un masque à la main.
  }
  %% 4-4
  \pieceToc\markup\wordwrap {
    Récit, air. Florise : \italic { C’est ici que l’amour va m’offrir des hommages }
  }
  \includeScore "ECAair"
}
\bookpart {
  \scene "Scène IV" "Scène IV : Florise, la Sylphide."
  \sceneDescription\markup\wordwrap-center { Florise, la Sylphide. }
  %% 4-5
  \pieceToc\markup\wordwrap {
    Récit, air. Florise, la Sylphide :
    \italic { Belle Nymphe, à l’éclat dont brillent vos beaux yeux }
  }
  \includeScore "EDArecit"
}
\bookpart {
  \scene "Scène V" "Scène V : Le Sylphe, la Sylphide, Florise, chœur."
  \sceneDescription\markup\wordwrap-center {
    Le Sylphe, la Sylphide, Florise, troupes de Sylphes & de Sylphides,
    sous divers déguisements.
  }
  %% 4-6
  \pieceTocFootnote "Entrée des masques" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "EDBentree"
}
\bookpart {
  %% 4-7
  \pieceTocFootnote\markup\wordwrap {
    Chœur. \italic { Chantons, ne songeons qu’aux plaisirs }
  } \markup\wordwrap {
    Parties de dessus, hautes-contre et tailles de violons, et de
    hautes-contre et tailles chantantes recomposées par B. Dratwicki.
  }
  \includeScore "EEBchoeur"
}
\bookpart {
  %% 4-8
  \pieceTocFootnote "Premier cotillon" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "EECcotillon"
  %% 4-9
  \pieceTocFootnote "Deuxième cotillon" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées
    par B. Dratwicki.
  }
  \includeScore "EEDcotillon"
  %% 4-10
  \pieceToc\markup\wordwrap {
    Récit. Le Sylphe : \italic { Ce lieu va recevoir la beauté qui m’engage }
  }
  \includeScore "EEErecit"
}
\bookpart {
  %% 4-11
  \pieceTocFootnote\markup\wordwrap {
    Air, Chœur. Le Sylphe : \italic { Que de son nom ce séjour retentisse }
  } \markup\wordwrap {
    Parties de dessus, hautes-contre et tailles de violons, et de
    hautes-contre et tailles chantantes recomposées par B. Dratwicki.
  }
  \includeScore "EEFair"
  \includeScore "EEGchoeur"
}
\bookpart {
  %% 4-12
  \pieceTocFootnote "Sarabande" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "EEHsarabande"
}
\bookpart {
  %% 4-13
  \pieceTocFootnote "Première gavotte" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "EEIgavotte"
  %% 4-14
  \pieceTocFootnote "Deuxième gavotte" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "EEJgavotte"
}
\bookpart {
  %% 4-15
  \pieceToc\markup\wordwrap {
    Récit, air. Le Sylphe, Florise, La Sylphide :
    \italic { Vous ne paraissez point cher objet que j’adore }
  }
  \includeScore "EEKrecit"
}
\bookpart {
  %% 4-16
  \pieceToc\markup\wordwrap {
    Air. Florise : \italic { Triomphe, fais voler tes traits }
  }
  \includeScore "EELair"
}
\bookpart {
  %% 4-17
  \pieceTocFootnote "Premier menuet" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "EEMmenuet"
}
\bookpart {
  \paper { system-count = 4 }
  %% 4-18
  \pieceTocFootnote "Deuxième menuet" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \includeScore "EENmenuet"
}
\bookpart {
  %% 4-19
  \pieceTocFootnote "Première gavotte" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \reIncludeScore "EEIgavotte" "EEOgavotte"
  %% 4-20
  \pieceTocFootnote "Deuxième gavotte" \markup\wordwrap {
    Parties de hautes-contre et de tailles de violons recomposées par
    B. Dratwicki.
  }
  \reIncludeScore "EEJgavotte" "EEPgavotte"
}
\bookpart {
  %% 4-21
  \pieceTocFootnote\markup\wordwrap {
    Chœur. \italic { Chantons, ne songeons qu’aux plaisirs }
  } \markup\wordwrap {
    Parties de dessus, hautes-contre et tailles de violons, et de
    hautes-contre et tailles chantantes recomposées par B. Dratwicki.
  }
  \includeScore "EEQchoeur"
  \actEnd "Fin du Quatrième et Dernier Acte"
}
