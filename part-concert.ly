\version "2.19.83"
\include "common.ily"

\paper {
  %% plus d'espace entre titre et musique
  markup-system-spacing.padding = #2.5 % default: 0.5
  system-system-spacing.padding = #2.5 % default: 1
  %% de la place entre dernier system et copyright
  last-bottom-spacing.padding = #3
}
\layout {
  \context {
    \Lyrics
    %% de la place sous les paroles (basse continue)
    \override VerticalAxisGroup.nonstaff-unrelatedstaff-spacing.padding = #3
  }
}

%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header {
    title = \markup\center-column {
      LES GÉNIES
      \fontsize#-5 OU LES CARACTÈRES DE L’AMOUR
    }
    editions = \markup\center-column {
      Il Caravaggio
      Camille Delaforge  
    }
  }
  \markup\null
}
%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \markuplist
  \abs-fontsize-lines #7
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 2)
  \table-of-contents
}

%% Musique
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Prologue
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\actn "Prologue"
\sceneDescription\markup\wordwrap-center {
  Le Théatre représente un désert.
}
\scene "Scène Première" "Scène I"
\sceneDescription\markup\wordwrap-center { Zoroastre. }
%% 0-1
\pieceToc "Ouverture"
\includeScore "AAAouverture"
%% 0-2
\pieceToc\markup\wordwrap {
  Récit. Zoroastre : \italic { Il est temps que mon Art instruise les Mortels }
}
\includeScore "AABrecit"
%% 0-3
\pieceToc\markup\wordwrap {
  Air. Zoroastre : \italic { Esprits soumis à mes commandements }
}
\includeScore "AACair"

\scene "Scène II" "Scène II"
\sceneDescription\markup\wordwrap-center {
  Zoroastre, et les génies.
}
%% 0-4
\pieceToc\markup\wordwrap {
  Air. Zoroastre : \italic { Que la Terre, le feu, que l’onde, que les airs }
}
\includeScore "ABAair"
%% 0-5
\pieceToc\markup\wordwrap { Air pour les génies }
\includeScore "ABCdanse"
%% 0-6
\pieceToc\markup\wordwrap {
  Air. Zoroastre : \italic { Quels bruits ! quels doux accords ! quelle clarté nouvelle ! }
}
\markup\italic {
  On entend une douce harmonie qui annonce la descente de l’Amour.
}\noPageBreak
\includeScore "ABDair"

\scene "Scène III" "Scène III"
\sceneDescription\markup\wordwrap-center {
  L’Amour, Zoroastre, et les génies.
}
%% 0-7
\pieceToc\markup\wordwrap {
  Air, récit. L’Amour, Zoroastre : \italic { Tout obéit, tout s’éveille à ta voix ! }
}
\includeScore "ACAair"
%% 0-8
\pieceToc\markup\wordwrap {
  Air. L’Amour : \italic { 
    Accourez jeux charmants, volez tendres amours
  }
}
\includeScore "ACBair"

\scene "Scène IV" "Scène IV"
\sceneDescription\markup\wordwrap-center {
  L’Amour, Zoroastre, et les génies, Troupe de Plaisirs & de Jeux.
}
%% 0-9
\pieceToc "Sarabande"
\includeScore "ACFsarabande"
%% 0-10
\pieceToc\markup\wordwrap {
  Chœur : \italic { Du doux bruit de nos chants que ces lieux retentissent }
}
\includeScore "ADEchoeur"
%% 0-11
\pieceToc "Ouverture"
\reIncludeScore "AAAouverture" "ADFouverture"
\actEnd "Fin du Prologue"


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ACTE I
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newBookPart#'()
\act "Acte Premier"
\sceneDescription\markup\center-column {
  \larger Les Nymphes ou l’amour indiscret.
  \wordwrap-center {
    Le Théatre représente un agréable jardin sur le bord de la mer.
  }
}
\scene "Scène Première" "Scène I"
\sceneDescription\markup\wordwrap-center { Léandre. }
%% 1-1
\pieceToc "Ritournelle"
\includeScore "BAAritournelle"
%% 1-2
\pieceToc\markup\wordwrap {
  Récit, air. Léandre : \italic { Viens être le témoin du bonheur qui m’enchante }
}
\includeScore "BABrecit"
\includeScore "BADrecit"
\newBookPart #'(flute-hautbois)

\scene "Scène II" "Scène II"
\sceneDescription\markup\wordwrap-center { Lucile, Zerbin. }
%% 1-3
\pieceToc\markup\wordwrap {
  Air, récit. Lucile, Zerbin : \italic { Asile des plaisirs, beau lieu rempli de charmes }
}
\includeScore "BBAair"
\includeScore "BBBrecit"
%% 1-4
\pieceToc\markup\wordwrap {
  Air, récit. Lucile, Zerbin : \italic { Venez, juste Dépit, venez à mon secours }
}
\includeScore "BBCair"
\includeScore "BBDrecit"

\scene "Scène III" "Scène III"
\sceneDescription\markup\wordwrap-center { Léandre. }
%% 1-5
\pieceToc\markup\wordwrap {
  Air. Léandre : \italic { Reviens cher objet de mes vœux }
}
\includeScore "BCAair"
\newBookPart #'(flute-hautbois)

\scene "Scène IV" "Scène IV"
\sceneDescription\markup\center-column {
  \justify {
    On entend une douce harmonie ; la nymphe paraît sur une conque marine,
    suivie de sa cour.
  }
  \wordwrap-center {
    Léandre, la principale Nymphe, et sa suite.
  }
}
%% 1-6
\pieceToc "[Entrée]"
\includeScore "BDAentree"
%% 1-7
\pieceToc\markup\wordwrap {
  Duo. Léandre, la principale Nymphe :
  \italic { Amour, viens nous unir de tes plus douces chaînes }
}
\includeScore "BDBduo"
%% 1-8
\pieceToc "Passacaille"
\includeScore "BDEpassacaille"
%% 1-9
\pieceToc\markup\wordwrap {
  Air. La principale Nymphe : \italic { Amour, tu réponds à nos vœux }
}
\includeScore "BDHair"
%% 1-10
\pieceToc "Premier Tambourin"
\includeScore "BDLtambourin"
%% 1-11
\pieceToc "Deuxième Tambourin"
\includeScore "BDMtambourin"
%% 1-12
\pieceToc\markup\wordwrap {
  Récit. La principale Nymphe, Léandre : \italic { Tout prévient ici vos désirs }
}
\includeScore "BDNrecit"

\scene "Scène V" "Scène V"
\sceneDescription\markup\wordwrap-center {
  La principale Nymphe, et sa suite, Léandre, Lucile, Zerbin.
}
%% 1-13
\pieceToc\markup\wordwrap {
  Récit. \italic { Poursuis, ingrat, poursuis volage, amant sans foi }
}
\includeScore "BEArecit"
%% 1-14
\pieceToc\markup\wordwrap {
  Air. Lucile : \italic { Venez tyrans des airs, aquilons furieux }
}
\includeScore "BEBair"
\actEnd "Fin du Premier Acte"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Acte II
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newBookPart#'()
\act "Acte Deuxième"
\sceneDescription\markup\center-column {
  \larger Les Gnomes ou l’amour ambitieux.
  \wordwrap-center {
    Le Théatre représente une solitude bornée par un bosquet.
  }
}
\scene "Scène Première" "Scène I"
\sceneDescription\markup\wordwrap-center { Zaïre, Zamide. }
%% 2-1
\pieceToc\markup\wordwrap {
  Air, récit. Zaïre, Zamide : \italic { Douce erreur, aimable chimère }
}
\includeScore "CAAair"

\scene "Scène II" "Scène II"
\sceneDescription\markup\wordwrap-center { Zaïre seule. }
%% 2-2
\pieceToc\markup\wordwrap {
  Air. Zaïre : \italic { Je cède à ta voix qui m’appelle }
}
\includeScore "CBAair"

\scene "Scène III" "Scène III"
\sceneDescription\markup\wordwrap-center { Un gnome sous le nom d’Adolphe, Zaïre. }
%% 2-3
\pieceToc\markup\wordwrap {
  Récit, air. Adolphe, Zaïre : \italic { Vous voyez à vos pieds l’amant le plus fidèle }
}
\includeScore "CCArecit"
\newBookPart #'(flute-hautbois)

\scene "Scène IV" "Scène IV"
\sceneDescription\markup\center-column {
  \justify {
    L’on voit paraître un superbe palais. Une troupe de gnomes sous la
  forme de divers peuples orientaux se préparent pour la fête.
  }
  \wordwrap-center {
    Adolphe, Zaïre, troupe de gnomes sous la forme
    de divers peuples orientaux.
  }
}
%% 2-4
\pieceToc "Marche"
\includeScore "CDAmarche"
%% 2-5
\pieceToc\markup\wordwrap {
  Récit, air. Adolphe, Zaïre :
  \italic { Rassurez-vous, dissipez votre effroi }
}
\includeScore "CDBrecit"
\newBookPart #'(flute-hautbois)
%% 2-6
\pieceToc\markup\wordwrap {
  Chœur : \italic { Régnez dans nos climats, jouïssez de la gloire }
}
\includeScore "CDCchoeur"
%% 2-7
\pieceToc "Rondeau"
\includeScore "CDDrondeau"
%% 2-8
\pieceToc\markup\wordwrap {
  Air. Un Indien : \italic { Recevez l’éclatant hommage }
}
\includeScore "CDEair"
\actEnd "Fin du Deuxième Acte"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Acte III
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newBookPart#'()
\partBlankPageBreak#'(basse)
\act "Acte Troisième"
\sceneDescription\markup\center-column {
  \larger Les Salamandres ou l’amour violent.
  \wordwrap-center {
    Le Théatre représente le Palais de Numapire.
  }
}
\scene "Scène Première" "Scène I"
\sceneDescription\markup\wordwrap-center { Isménide. }
%% 3-1
\pieceToc\markup\wordwrap {
  Air. Isménide : \italic { Tyran d’xun cœur fidèle et tendre }
}
\includeScore "DAAair"
\sceneDescription\markup\wordwrap-center\italic {
  Pircaride sous les traits d'Ismé[ni]de, paraît sur un char de feu
  un poignard à la main.
}
%% 3-2
\pieceToc\markup\wordwrap {
  Récit. Isménide :
  \italic { Que vois-je ? quel objet se présente à mes yeux ? }
}
\includeScore "DABrecit"

\scene "Scène II" "Scène II"
\sceneDescription\markup\center-column {
  Pircaride, Isménide.
  Pircaride sort de son char.
}
%% 3-3
\pieceToc "[Ritournelle]"
\includeScore "DBAritournelle"
%% 3-4
\pieceToc\markup\wordwrap {
  Récit. Pircaride, Isménide : \italic { Pour immoler une victime }
}
\includeScore "DBBrecit"
%% 3-5
\pieceToc\markup\wordwrap {
  Air. Pircaride : \italic { Vous qui m’obéissez, paraissez à mes yeux }
}
\includeScore "DBCair"
\sceneDescription\markup\wordwrap-center {
  Isménide est enlevée par les génies sur un char.
}

\scene "Scène III" "Scène III"
\sceneDescription\markup\wordwrap-center {
  Pircaride sous les traits d’Isménide.
}
%% 3-6
\pieceToc\markup\wordwrap {
  Air. Pircaride : \italic { Elle part, et mon cœur n’est point exempt d’alarmes }
}
\includeScore "DCAair"
\newBookPart #'(flute-hautbois)

\scene "Scène IV" "Scène IV"
\sceneDescription\markup\wordwrap-center {
  Numapire, Pircaride sous les traits d’Isménide.
}
%% 3-7
\pieceToc\markup\wordwrap {
  Récit, air. Numapire :
  \italic { Je sens, en vous voyant, accroître mon ardeur }
}
\includeScore "DDArecit"

\scene "Scène V" "Scène V"
\sceneDescription\markup\wordwrap-center {
  Numapire, Pircaride, troupe de salamandres sous la forme
  de divers peuples d’Afrique.
}
%% 3-8
\pieceToc\markup\wordwrap {
  Chœur. \italic { Chantons, célébrons notre reine }
}
\includeScore "DEBchoeur"
%% 3-9
\pieceToc "Rondeau"
\includeScore "DECrondeau"
%% 3-10
\pieceToc\markup\wordwrap {
  Air. Une Africaine : \italic { L’amour a besoin de vos charmes }
}
\includeScore "DEFair"
%% 3-11
\pieceToc "Première bourrée"
\includeScore "DEHbourree"
%% 3-12
\pieceToc "Deuxième bourrée"
\includeScore "DEIbourree"
%% 3-13
\pieceToc\markup\wordwrap {
  Récit, air. Pircaride, Numapire :
  \italic { Finissez ces concerts, votre hommage m’offense }
}
\includeScore "DEJrecit"
%% 3-17
\pieceToc\markup\wordwrap {
  Air. Numapire : \italic { Servez les transports de ma rage }
}
\includeScore "DEKair"
%% 3-18
\pieceToc "Entr’acte"
\reIncludeScore "BDLtambourin" "DEMtambourin"
\reIncludeScore "BDMtambourin" "DENtambourin"
\actEnd "Fin du Troisième Acte"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Acte IV
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newBookPart#'()
\act "Acte Quatrième"
\sceneDescription\markup\center-column {
  \larger Les Sylphes ou l’amour léger.
  \wordwrap-center {
    Le Théatre représente un lieu préparé pour y donner une fête galante.
  }
}
\scene "Scène Première" "Scène I"
\sceneDescription\markup\wordwrap-center { Un Sylphe }
%% 4-1
\pieceToc "Ritournelle"
\includeScore "EAAritournelle"
%% 4-2
\pieceToc\markup\wordwrap {
  Récit et air. Un Sylphe : \italic { Le ciel a fixé mon empire }
}
\includeScore "EABrecit"

\scene "Scène II" "Scène II"
\sceneDescription\markup\wordwrap-center { Un Sylphe, une Sylphide. }
%% 4-3
\pieceToc\markup\wordwrap {
  Prélude, récit, air. Un Sylphe, une Sylphide :
  \italic { Ne dissimulez point, votre cœur est volage }
}
\includeScore "EBArecit"

\scene "Scène III" "Scène III"
\sceneDescription\markup\wordwrap-center { Florise, la Sylphide. }
%% 4-4
\pieceToc\markup\wordwrap {
  Duo. La Sylphide, Florise : \italic { Formons une chaîne si belle }
}
\includeScore "EDArecit"
\newBookPart #'(violon1 violon2 haute-contre taille)
\scene "Scène IV" "Scène IV"
\sceneDescription\markup\wordwrap-center {
  Le Sylphe, la Sylphide, Florise, troupes de Sylphes & de Sylphides,
  sous divers déguisements.
}
%% 4-5
\pieceToc\markup\wordwrap {
  Air. La Sylphide : \italic { Triomphe, fais voler tes traits }
}
\includeScore "EELair"
\newBookPart #'(flute-hautbois)
%% 4-6
\pieceToc\markup\wordwrap {
  Chœur. \italic { Chantons, ne songeons qu’aux plaisirs }
}
\includeScore "EEBchoeur"
\actEnd "Fin du Quatrième et Dernier Acte"
