\version "2.23.10"
\include "common.ily"

\paper {
  %% plus d'espace entre titre et musique
  markup-system-spacing.padding = #2.5 % default: 0.5
  system-system-spacing.padding = #2.5 % default: 1
  %% de la place entre dernier system et copyright
  last-bottom-spacing.padding = #3
}
\layout {
  \context {
    \Lyrics
    %% de la place sous les paroles (basse continue)
    \override VerticalAxisGroup.nonstaff-unrelatedstaff-spacing.padding = #3
  }
}

%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header {
    title = \markup\center-column {
      LES GÉNIES
      \fontsize#-5 OU LES CARACTÈRES DE L’AMOUR
    }
  }
  \markup\null
  \partBlankPageBreak#'(dessus1 dessus2 parties)
}
%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \markuplist
  \abs-fontsize-lines#8
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 2)
  \table-of-contents
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\actn "Prologue"
\sceneDescription\markup\wordwrap-center { Le théatre représente un désert. }
\scene "Scène Première" "Scène I : Zoroastre."
\sceneDescription\markup\wordwrap-center { Zoroastre. }
%% 0-1
\pieceToc Ouverture
\includeScore "AAAouverture"
%% 0-2
\pieceToc\markup\wordwrap {
  Récit. Zoroastre : \italic { Il est temps que mon Art instruise les Mortels }
}
\includeScore "AABrecit"
%% 0-3
\pieceToc\markup\wordwrap {
  Air. Zoroastre : \italic { Esprits soumis à mes commandements }
}
\includeScore "AACair"
\scene "Scène II" "Scène II : Zoroastre, chœur."
\sceneDescription\markup\wordwrap-center { Zoroastre, et les génies. }
%% 0-4
\pieceToc\markup\wordwrap {
  Air, chœur. Zoroastre : \italic { Que la terre, le feu, que l’onde, que les airs }
}
\includeScore "ABAair" \partNoPageTurn#'(basse)
\includeScore "ABBchoeur"
%% 0-5
\pieceToc "Air pour les génies"
\includeScore "ABCdanse"
%% 0-6
\pieceToc\markup\wordwrap {
  Air. Zoroastre :
  \italic { Quels bruits ! quels doux accords ! quelle clarté nouvelle ! }
}
\markup\italic {
  On entend une douce harmonie qui annonce la descente de l’Amour.
} \noPageBreak
\includeScore "ABDair"
\scene "Scène III" "Scène III : L’Amour, Zoroastre."
\sceneDescription\markup\wordwrap-center {
  L’Amour, Zoroastre, et les génies.
}
%% 0-7
\pieceToc\markup\wordwrap {
  Air. L’Amour, Zoroastre : \italic { Tout obéit, tout s’éveille à ta voix ! }
}
\includeScore "ACAair"
%% 0-8
\pieceToc\markup\wordwrap {
  Air. L’Amour : \italic { Accourez jeux charmants, volez tendres amours }
}
\includeScore "ACBair"
\scene "Scène IV" "Scène IV : L’Amour, Zoroastre, chœur."
\sceneDescription\markup\wordwrap-center {
  L’Amour, Zoroastre, et les génies, Troupe de Plaisirs & de Jeux.
}
%% 0-9
\pieceToc "Air pour les Plaisirs"
\includeScore "ACCplaisirs"
%% 0-10
\pieceToc "Première bourrée en rondeau"
\includeScore "ACDbouree"
%% 0-11
\pieceToc "Deuxième bourrée"
\includeScore "ACEbouree"
%% 0-12
\pieceToc "Sarabande"
\includeScore "ACFsarabande"
%% 0-13
\pieceToc\markup\wordwrap {
  Air. L’Amour : \italic { Aimez-tous, cédez à l’Amour }
}
\includeScore "ADAair"
%% 0-14
\pieceToc "Premier menuet"
\includeScore "ADBmenuet"
%% 0-15
\pieceToc "Deuxième Menuet"
\includeScore "ADCmenuet"
\partPageTurn#'(parties)
%% 0-16
\pieceToc\markup\wordwrap {
  Chœur : \italic { Du doux bruit de nos chants que ces lieux retentissent }
}
\includeScore "ADEchoeur"
%% 0-17
\pieceToc "Ouverture"
\reIncludeScore "AAAouverture" "ADFouverture"
\actEnd "Fin du Prologue"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newBookPart#'()
\act "Acte Premier"
\sceneDescription\markup\center-column {
  \larger Les Nymphes ou l’amour indiscret.
  \wordwrap-center {
    Le théatre représente un agréable jardin sur le bord de la mer.
  }
}
\scene "Scène Première" "Scène I : Léandre, Zerbin."
\sceneDescription\markup\wordwrap-center { Léandre, Zerbin. }
%% 1-1
\pieceToc "Ritournelle"
\includeScore "BAAritournelle"
%% 1-2
\pieceToc\markup\wordwrap {
  Récit, air. Léandre, Zerbin :
  \italic { Viens être le témoin du bonheur qui m’enchante }
}
\includeScore "BABrecit"
\newBookPart#'(dessus1 dessus2 flute-hautbois)
\includeScore "BACair"
\includeScore "BADrecit"
\newBookPart#'(dessus1 dessus2)
\scene "Scène II" "Scène II : Lucile, Zerbin."
\sceneDescription\markup\wordwrap-center { Lucile, Zerbin. }
%% 1-3
\pieceToc\markup\wordwrap {
  Air, récit. Lucile, Zerbin :
  \italic { Asile des plaisirs, beau lieu rempli de charmes }
}
\includeScore "BBAair"
\includeScore "BBBrecit"
%% 1-4
\pieceToc\markup\wordwrap {
  Air, récit. Lucile, Zerbin : \italic { Venez, juste Dépit, venez à mon secours }
}
\includeScore "BBCair"
\includeScore "BBDrecit"
\newBookPart#'(dessus1 dessus2)
\scene "Scène III" "Scène III : Léandre."
\sceneDescription\markup\wordwrap-center { Léandre. }
%% 1-5
\pieceToc\markup\wordwrap {
  Air. Léandre : \italic { Reviens cher objet de mes vœux }
}
\includeScore "BCAair"
\newBookPart#'(dessus1 dessus2)
\scene "Scène IV" "Scène IV : Léandre, une nymphe, chœur."
\sceneDescription\markup\center-column {
  \justify {
    On entend une douce harmonie ; la nymphe paraît sur une conque marine,
    suivie de sa cour.
  }
  \wordwrap-center {
    Léandre, la principale Nymphe, et sa suite.
  }
}
%% 1-6
\pieceToc "[Entrée]"
\includeScore "BDAentree"
%% 1-7
\pieceToc\markup\wordwrap {
  Air, duo, récit. Léandre, la principale Nymphe :
  \italic { Qu’éloigné de votre présence }
}
\includeScore "BDBduo"
\newBookPart#'(dessus1 dessus2)
%% 1-8
\pieceToc\markup\wordwrap {
  Air, chœur. La principale Nymphe : \italic { Chantez dans ce riant boccage }
}
\includeScore "BDCair"
\includeScore "BDDchoeur"
\newBookPart#'(parties)
%% 1-9
\pieceToc "Passacaille"
\includeScore "BDEpassacaille"
%% 1-10
\pieceToc "Premier passepied"
\includeScore "BDFpassepied"
%% 1-11
\pieceToc "Deuxième passepied"
\includeScore "BDGpassepied"
\newBookPart#'(parties)
%% 1-12
\pieceToc\markup\wordwrap {
  Air. La principale Nymphe : \italic { Amour, tu réponds à nos vœux }
}
\includeScore "BDHair"
%% 1-13
\pieceToc "Premier passepied"
\reIncludeScore "BDFpassepied" "BDIpassepied"
%% 1-14
\pieceToc "Deuxième passepied"
\reIncludeScore "BDGpassepied" "BDJpassepied"
%% 1-15
\pieceToc\markup\wordwrap {
  Air, chœur. Un Nymphe : \italic { Rions, chantons sous cet ombrage }
}
\includeScore "BDKchoeur"
\partPageTurn#'(parties)
%% 1-16
\pieceToc "Premier tambourin"
\includeScore "BDLtambourin"
%% 1-17
\pieceToc "Deuxième tambourin"
\includeScore "BDMtambourin"
\newBookPart#'(parties)
%% 1-18
\pieceToc\markup\wordwrap {
  Récit. La principale Nymphe, Léandre : \italic { Tout prévient ici vos désirs }
}
\includeScore "BDNrecit"
\scene "Scène V" "Scène V : La nymphe, Léandre, Lucile, Zerbin."
\sceneDescription\markup\wordwrap-center {
  La principale Nymphe, et sa suite, Léandre, Lucile, Zerbin.
}
%% 1-19
\pieceToc\markup\wordwrap {
  Récit. \italic { Poursuis, ingrat, poursuis volage, amant sans foi }
}
\includeScore "BEArecit"
\newBookPart#'(dessus1 dessus2)
\partNoPageTurn#'(basse)
%% 1-20
\pieceToc\markup\wordwrap {
  Air. Lucile : \italic { Venez tyrans des airs, aquilons furieux }
}
\includeScore "BEBair"
%% 1-21
\pieceToc "Entr’acte"
\reIncludeScore "ADBmenuet" "BEDmenuet"
\reIncludeScore "ADCmenuet" "BEEmenuet"
\actEnd "Fin du Premier Acte"
\partBlankPageBreak#'(basse)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newBookPart#'()
\act "Acte Deuxième"
\sceneDescription\markup\center-column {
  \larger Les Gnomes ou l’amour ambitieux.
  \wordwrap-center {
    Le théatre représente une solitude bornée par un bosquet.
  }
}
\scene "Scène Première" "Scène I : Zaïre, Zamide."
\sceneDescription\markup\wordwrap-center { Zaïre, Zamide. }
%% 2-1
\pieceToc\markup\wordwrap {
  Air, récit. Zaïre, Zamide : \italic { Douce erreur, aimable chimère }
}
\includeScore "CAAair"
\scene "Scène II" "Scène II : Zaïre."
\sceneDescription\markup\wordwrap-center { Zaïre seule. }
%% 2-2
\pieceToc\markup\wordwrap {
  Air. Zaïre : \italic { Je cède à ta voix qui m’appelle }
}
\includeScore "CBAair"
\scene "Scène III" "Scène III : Adolphe, Zaïre."
\sceneDescription\markup\wordwrap-center { Un gnome sous le nom d’Adolphe, Zaïre. }
%% 2-3
\pieceToc\markup\wordwrap {
  Récit, air. Adolphe, Zaïre : \italic { Vous voyez à vos pieds l’amant le plus fidèle }
}
\includeScore "CCArecit"
\scene "Scène IV" "Scène IV : Adolphe, Zaïre, chœur."
\sceneDescription\markup\center-column {
  \justify {
    L’on voit paraître un superbe palais. Une troupe de gnomes sous la
    forme de divers peuples orientaux se préparent pour la fête.
  }
  \wordwrap-center {
    Adolphe, Zaïre, troupe de gnomes sous la forme
    de divers peuples orientaux.
  }
}
%% 2-4
\pieceToc "Marche"
\includeScore "CDAmarche"
\partNoPageTurn#'(parties)
%% 2-5
\pieceToc\markup\wordwrap {
  Récit, air. Zaïre, Adolphe :
  \italic { Que tout ce que je vois rend mon âme interdite }
}
\includeScore "CDBrecit"
\newBookPart#'(parties)
%% 2-6
\pieceToc\markup\wordwrap {
  Chœur : \italic { Régnez dans nos climats, jouïssez de la gloire }
}
\includeScore "CDCchoeur"
%% 2-7
\pieceToc "Rondeau"
\includeScore "CDDrondeau"
%% 2-8
\pieceToc\markup\wordwrap {
  Air. Un Indien : \italic { Recevez l’éclatant hommage }
}
\includeScore "CDEair"
%% 2-9
\pieceToc "Loure"
\includeScore "CDFloure"
%% 2-10
\pieceToc "Première gavotte"
\includeScore "CDGgavotte"
%% 2-11
\pieceToc "Deuxième gavotte"
\includeScore "CDHgavotte"
%% 2-12
\pieceToc\markup\wordwrap {
  Air. Un Indien : \italic { Dans nos climats }
}
\includeScore "CDIair"
%% 2-13
\pieceToc "Deuxième gavotte"
\reIncludeScore "CDHgavotte" "CDJgavotte"
%% 2-14
\pieceToc\markup\wordwrap {
  Chœur : \italic { Régnez dans nos climats, jouïssez de la gloire }
}
\reIncludeScore "CDCchoeur" "CDKchoeur"
%% 2-15
\pieceToc "Entr’acte"
\reIncludeScore "CDGgavotte" "CDLgavotte"
\reIncludeScore "CDHgavotte" "CDMgavotte"
\actEnd "Fin du Deuxième Acte"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newBookPart#'()
\act "Acte Troisième"
\sceneDescription\markup\center-column {
  \larger Les Salamandres ou l’amour violent.
  \wordwrap-center {
    Le théatre représente le palais de Numapire.
  }
}
\scene "Scène Première" "Scène I : Isménide."
\sceneDescription\markup\wordwrap-center { Isménide. }
%% 3-1
\pieceToc\markup\wordwrap {
  Air. Isménide : \italic { Tyran d’un cœur fidèle et tendre }
}
\includeScore "DAAair"
\sceneDescription\markup\wordwrap-center\italic {
  Pircaride sous les traits d'Ismé[ni]de, paraît sur un char de feu
  un poignard à la main.
}
%% 3-2
\pieceToc\markup\wordwrap {
  Récit. Isménide : \italic { Que vois-je ? quel objet se présente à mes yeux ? }
}
\includeScore "DABrecit"
\newBookPart#'(parties)
\scene "Scène II" "Scène II : Pircaride, Isménide."
\sceneDescription\markup\center-column {
  Pircaride, Isménide.
  Pircaride sort de son char.
}
%% 3-3
\pieceToc "[Ritournelle]"
\includeScore "DBAritournelle"
\newBookPart#'(parties)
%% 3-4
\pieceToc\markup\wordwrap {
  Récit. Pircaride, Isménide : \italic { Pour immoler une victime }
}
\includeScore "DBBrecit"
\newBookPart#'(basse)
%% 3-5
\pieceToc\markup\wordwrap {
  Air. Pircaride : \italic { Vous qui m’obéissez, paraissez à mes yeux }
}
\includeScore "DBCair"
\sceneDescription\markup\wordwrap-center {
  Isménide est enlevée par les génies sur un char.
}
\scene "Scène III" "Scène III : Pircaride."
\sceneDescription\markup\wordwrap-center {
  Pircaride sous les traits d’Isménide.
}
%% 3-6
\pieceToc\markup\wordwrap {
  Air. Pircaride : \italic { Elle part, et mon cœur n’est point exempt d’alarmes }
}
\includeScore "DCAair"
\scene "Scène IV" "Scène IV : Numapire, Pircaride."
\sceneDescription\markup\wordwrap-center {
  Numapire, Pircaride sous les traits d’Isménide.
}
%% 3-7
\pieceToc\markup\wordwrap {
  Récit. Numapire, Pircaride :
  \italic { Je sens, en vous voyant, accroître mon ardeur }
}
\includeScore "DDArecit"
\newBookPart#'(dessus1 dessus2 flute-hautbois)
\scene "Scène V" "Scène V : Numapire, Pircaride, chœurs."
\sceneDescription\markup\wordwrap-center {
  Numapire, Pircaride, troupe de salamandres sous la forme
  de divers peuples d’Afrique.
}
%% 3-8
\pieceToc "Marche"
\includeScore "DDBmarche"
%% 3-9
\pieceToc\markup\wordwrap {
  Chœur. \italic { Chantons, célébrons notre reine }
}
\includeScore "DEBchoeur"
%% 3-10
\pieceToc "Rondeau"
\includeScore "DECrondeau"
%% 3-11
\pieceToc "Première gavotte"
\includeScore "DEDgavotte"
%% 3-12
\pieceToc "Deuxième gavotte"
\includeScore "DEEgavotte"
%% 3-13
\pieceToc\markup\wordwrap {
  Air. Une Africaine : \italic { L’amour a besoin de vos charmes }
}
\includeScore "DEFair"
%% 3-14
\pieceToc "Air"
\includeScore "DEGair"
%% 3-15
\pieceToc "Première bourrée"
\includeScore "DEHbourree"
%% 3-16
\pieceToc "Deuxième bourrée"
\includeScore "DEIbourree"
%% 3-17
\pieceToc\markup\wordwrap {
  Récit, air. Pircaride, Numapire :
  \italic { Finissez ces concerts, votre hommage m’offense }
}
\includeScore "DEJrecit"
%% 3-18
\pieceToc\markup\wordwrap {
  Air et chœur. Numapire : \italic { Servez les transports de ma rage }
}
\includeScore "DEKair"
%% 3-19
\pieceToc "Air"
\includeScore "DELair"
%% 3-20
\pieceToc "Entr’acte"
\reIncludeScore "BDLtambourin" "DEMtambourin"
\reIncludeScore "BDMtambourin" "DENtambourin"
\actEnd "Fin du Troisième Acte"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newBookPart#'()
\act "Acte Quatrième"
\sceneDescription\markup\center-column {
  \larger Les Sylphes ou l’amour léger.
  \wordwrap-center {
    Le théatre représente un lieu préparé pour y donner une fête galante.
  }
}
\scene "Scène Première" "Scène I : Un Sylphe."
\sceneDescription\markup\wordwrap-center { Un Sylphe }
%% 4-1
\pieceToc "Ritournelle"
\includeScore "EAAritournelle"
%% 4-2
\pieceToc\markup\wordwrap {
  Récit et air. Un Sylphe : \italic { Le ciel a fixé mon empire }
}
\includeScore "EABrecit"
\scene "Scène II" "Scène II : Le Sylphe, une Sylphide."
\sceneDescription\markup\wordwrap-center { Un Sylphe, une Sylphide. }
%% 4-3
\pieceToc\markup\wordwrap {
  Prélude, récit, air. Un Sylphe, une Sylphide :
  \italic { Ne dissimulez point, votre cœur est volage }
}
\includeScore "EBArecit"
\scene "Scène III" "Scène III : Florise."
\sceneDescription\markup\wordwrap-center {
  Florise déguisée en cavalier, un masque à la main.
}
%% 4-4
\pieceToc\markup\wordwrap {
  Récit, air. Florise : \italic { C’est ici que l’amour va m’offrir des hommages }
}
\includeScore "ECAair"
\scene "Scène IV" "Scène IV : Florise, la Sylphide."
\sceneDescription\markup\wordwrap-center { Florise, la Sylphide. }
%% 4-5
\pieceToc\markup\wordwrap {
  Récit, air. Florise, la Sylphide :
  \italic { Belle Nymphe, à l’éclat dont brillent vos beaux yeux }
}
\includeScore "EDArecit"
\scene "Scène V" "Scène V : Le Sylphe, la Sylphide, Florise, chœur."
\sceneDescription\markup\wordwrap-center {
  Le Sylphe, la Sylphide, Florise, troupes de Sylphes & de Sylphides,
  sous divers déguisements.
}
%% 4-6
\pieceToc "Entrée des masques"
\includeScore "EDBentree"
%% 4-7
\pieceToc\markup\wordwrap {
  Chœur. \italic { Chantons, ne songeons qu’aux plaisirs }
}
\includeScore "EEBchoeur"
%% 4-8
\pieceToc "Premier cotillon"
\includeScore "EECcotillon"
%% 4-9
\pieceToc "Deuxième cotillon"
\includeScore "EEDcotillon"
%% 4-10
\pieceToc\markup\wordwrap {
  Récit. Le Sylphe : \italic { Ce lieu va recevoir la beauté qui m’engage }
}
\includeScore "EEErecit"
%% 4-11
\pieceToc\markup\wordwrap {
  Air, Chœur. Le Sylphe : \italic { Que de son nom ce séjour retentisse }
}
\includeScore "EEFair"
\includeScore "EEGchoeur"
%% 4-12
\pieceToc "Sarabande"
\includeScore "EEHsarabande"
%% 4-13
\pieceToc "Première gavotte"
\includeScore "EEIgavotte"
%% 4-14
\pieceToc "Deuxième gavotte"
\includeScore "EEJgavotte"
%% 4-15
\pieceToc\markup\wordwrap {
  Récit, air. Le Sylphe, Florise, La Sylphide :
  \italic { Vous ne paraissez point cher objet que j’adore }
}
\includeScore "EEKrecit"
%% 4-16
\pieceToc\markup\wordwrap {
  Air. Florise : \italic { Triomphe, fais voler tes traits }
}
\includeScore "EELair"
%% 4-17
\pieceToc "Premier menuet"
\includeScore "EEMmenuet"
%% 4-18
\pieceToc "Deuxième menuet"
\includeScore "EENmenuet"
%% 4-19
\pieceToc "Première gavotte"
\reIncludeScore "EEIgavotte" "EEOgavotte"
%% 4-20
\pieceToc "Deuxième gavotte"
\reIncludeScore "EEJgavotte" "EEPgavotte"
%% 4-21
\pieceToc\markup\wordwrap {
  Chœur. \italic { Chantons, ne songeons qu’aux plaisirs }
}
\includeScore "EEQchoeur"
\actEnd "Fin du Quatrième et Dernier Acte"
