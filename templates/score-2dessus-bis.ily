\score {
  \new GrandStaff <<
    \new Staff <<
      \keepWithTag #(*tag-global*) \global
      \includeNotes "dessus1"
      \clef #(*clef*)
      $(if (*instrument-name*)
        (make-music 'ContextSpeccedMusic
                    'context-type 'GrandStaff
                    'element (make-music 'PropertySet
                               'value (make-large-markup (*instrument-name*))
                               'symbol 'instrumentName))
        (make-music 'Music))
      $(or (*score-extra-music*) (make-music 'Music))
    >>
    \new Staff \with { \haraKiriFirst } <<
      \keepWithTag #(*tag-global*) \global
      \includeNotes "dessus2"
    >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
    ragged-last = #(*score-ragged*)
  }
}