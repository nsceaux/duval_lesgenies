\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \with {
      \remove "Page_turn_engraver"
      \tinyStaff
    } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
      $(or (*score-extra-music*) (make-music 'Music))
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new GrandStaff <<
      \new Staff <<
        \keepWithTag #(*tag-global*) \global
        \keepWithTag #'dessus1 \includeNotes #(*note-filename*)
        \clef #(*clef*)
        $(if (*instrument-name*)
          (make-music 'ContextSpeccedMusic
                      'context-type 'GrandStaff
                      'element (make-music 'PropertySet
                                 'value (make-large-markup (*instrument-name*))
                                 'symbol 'instrumentName))
          (make-music 'Music))
        $(or (*score-extra-music*) (make-music 'Music))
      >>
      \new Staff \with { \haraKiriFirst } <<
        \keepWithTag #(*tag-global*) \global
        \keepWithTag #'dessus2 \includeNotes #(*note-filename*)
      >>
    >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
    ragged-last = #(*score-ragged*)
  }
}