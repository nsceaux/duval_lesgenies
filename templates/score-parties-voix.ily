\score {
  \new StaffGroupNoBar <<
    \new Staff \with {
      \remove "Page_turn_engraver"
      \tinyStaff
      \haraKiriFirst
    } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new StaffGroupNoBracket \with { \haraKiriFirst } <<
      \new Staff <<
        \keepWithTag #(*tag-global*) \global
        \includeNotes "haute-contre"
        $(or (*score-extra-music*) (make-music 'Music))
      >>
      \new Staff <<
        \keepWithTag #(*tag-global*) \global
        \includeNotes "taille"
      >>
    >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
    ragged-last = #(*score-ragged*)
  }
}
