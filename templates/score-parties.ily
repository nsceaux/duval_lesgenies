\score {
  \new StaffGroup <<
    \new Staff <<
      \keepWithTag #(*tag-global*) \global
      \includeNotes "haute-contre"
      $(or (*score-extra-music*) (make-music 'Music))
    >>
    \new Staff \with { \haraKiri } <<
      \keepWithTag #(*tag-global*) \global
      \includeNotes "taille"
    >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
    ragged-last = #(*score-ragged*)
  }
}
